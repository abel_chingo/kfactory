// Creación del módulo
var angularRoutingApp = angular.module('angularRoutingApp', ['ngRoute']);
// Configuración de las rutas
angularRoutingApp.config(function($routeProvider) {

	$routeProvider
		.when('/', {
			templateUrl	: 'app/estudio/views/index-principal.html',
			controller 	: 'estudioController'
		})
		.when('/estudios', {
			templateUrl	: 'app/estudio/views/index-principal.html',
			controller 	: 'estudioController'
		})
		.when('/estudios/ver/:id', {
			templateUrl	: 'app/estudio/views/index-ver.html',
			controller 	: 'estudioController'
		})
        .when('/estudios/editar/:id', {
			templateUrl	: 'app/estudio/views/index-editar.html',
			controller 	: 'estudioController'
		}) 
        .when('/estudios/listar/', {
			templateUrl	: 'app/estudio/views/index-listar.html',
			controller 	: 'estudioController'
		})
        .when('/estudios/agregar', {
			templateUrl	: 'app/estudio/views/index-nuevo.html',
			controller 	: 'estudioControllerNuevo'
		})
		.when('/multimedia', {
			templateUrl	: 'app/multimedia/views/index-lista.html',
			controller 	: 'estudioController'
		})
		.when('/multimedia/agregar', {
			templateUrl	: 'app/multimedia/views/index-nuevo.html',
			controller 	: 'estudioControllerNuevo'
		})
        .when('/multimedia/editar/:id', {
			templateUrl	: 'app/multimedia/views/index-editar.html',
			controller 	: 'estudioController'
		})
		.when('/servicios', {
			templateUrl	: 'app/servicios/views/index-ver.html',
			controller 	: 'estudioController'
		})
		.when('/servicios/agregar', {
			templateUrl	: 'app/servicios/views/index-nuevo.html',
			controller 	: 'estudioControllerNuevo'
		})
        .when('/servicios/editar/:id', {
			templateUrl	: 'app/servicios/views/index-editar.html',
			controller 	: 'estudioController'
		}) 
		.when('/otro/nuevo', {
			templateUrl	: 'app/otro/views/index-nuevo.html',
			controller 	: 'estudioController'
		})
		.otherwise({
			redirectTo: '/'
		});
});

angularRoutingApp.controller('mainController', function($scope, $http, $rootScope, $window) {
	$scope.message = 'Hola, Mundo!!';
	$http.get('../biblioteca/getVarsGlobales')
		.success(function(resp){
			if(resp.code=='ok'){
				$rootScope._sysUsuario_ = resp.data.usuario;
				$rootScope._sysUrlStatic_=resp.data.url_static;
				$rootScope._sysUrlBase_=resp.data.url_base;
				if(resp.data.usuario!= null && resp.data.usuario.length==0){
					$window.location.href = resp.data.url_base;
				}
			} else {
					//$window.location.href = '../';
			}
		}).error(function(){
			console.log('error al cargar datos');
		});
});

angularRoutingApp.controller('estudioController', function($scope,$rootScope,$http,$routeParams,$filter,$location) {
	$scope.message= 'estudio';
	var id=$routeParams.id;
	$scope.bib_estudio=[];
	$scope.bib_autor=[];
    $scope.selectEstudios=[];
	$scope.tipo;
	$scope.estudio;
    $scope.tipo=0;


   $scope.cargarEstudios = function (){
		$http.get('../bib_tipo/xTipo')
		.success(function(resp){
			if(resp.code=='ok'){
				$scope.tipo=resp.data;
				console.log($scope.tipo);
			} else {
				console.log('error al cargar datos: '+resp.msj);
			}
		}).error(function(){
			console.log('error al cargar datos');
		});
	}

    $scope.init = function (){
		$scope.cargarEstudios();
	}

	$scope.init();
    
    
    
   $http.get('../bib_estudio/xEstudio')
		.success(function(resp){
			if(resp.code=='ok'){
				$scope.estudio=resp.data;
			} else {
				console.log('error al cargar datos estudio: '+resp.msj);
			}
		}).error(function(){
			console.log('error al cargar datos');
	});

    $scope.selecionarTipo = function(id){
		console.log(id);
		$scope.tipo=id;
		if($scope.tipo===0){
			$scope.selectEstudios=[];
			$scope.cargarEstudios();
		}else{
			if ($scope.estudio!=undefined) {
				for (var i = $scope.estudio.length - 1; i >= 0; i--) {
					if($scope.estudio[i].id_tipo ===$scope.tipo){
						$scope.selectEstudios.push($scope.estudio[i]);
					}
				}
			}
		}
	}

	$http.get('../bib_tipo_registro/xRegistro')
		.success(function(resp){
			if(resp.code=='ok'){
				$scope.registro=resp.data;
				//console.log($scope.registro);
			} else {
				console.log('error al cargar datos registro: '+resp.msj);
			}
		}).error(function(){
			console.log('error al cargar datos registro');
	});

    $scope.eliminar=function(id){  
        $http.get('../bib_estudio/xEliminarEstudio/?id_estudio='+id)
		.success(function(resp){
            console.log(id);
            console.log(resp);
			if(resp.code=='ok'){
				$scope.bib_estudio=resp.data;
				//console.log($scope.bib_estudio);
			} else {
				console.log('error al borrar datos estudio: '+resp.msj);
			}
		}).error(function(){
			console.log('error al borrar datos estudio');
		});
        
    }
    
    $scope.verEstudio = function(id){
		console.log("Entre "+id);
		$location.path('/estudios/ver/'+id);	
	}
});


angularRoutingApp.controller('estudioControllerNuevo', function($scope,$rootScope,$http,$routeParams,$filter,$location) {
	var id=$routeParams.id;
	var dni;
	$scope.bib_estudio=[];
	$scope.bib_autor=[];
    $scope.selectEstudios=[];
    $scope.bib_categoria=[];
    $scope.bib_subcategoria=[];
	$scope.tipo;
	$scope.estudio;
	$scope.editorial;
	$scope.idioma;
	$scope.pais;
    $scope.tipo=0;
	$scope.categoria;
    $scope.subcategoria;
    $scope.setting;
    $scope.registro;
    $scope.autor;
	$scope.detalle;
	$scope.bib_email=[];

	$http.get('../bib_setting/xSetting')
		.success(function(resp){
			if(resp.code=='ok'){
				$scope.setting=resp.data;
				console.log($scope.setting);
			} else {
				console.log('error al cargar datos setting: '+resp.msj);
			}
		}).error(function(){
			console.log('error al cargar datos');
	});

	$http.get('../bib_tipo_registro/xRegistro')
		.success(function(resp){
			if(resp.code=='ok'){
				$scope.registro=resp.data;
			} else {
				console.log('error al cargar datos registro: '+resp.msj);
			}
		}).error(function(){
			console.log('error al cargar datos registro');
	});

	$http.get('../bib_editorial/xEditorial')
		.success(function(resp){
			if(resp.code=='ok'){
				$scope.editorial=resp.data;
			} else {
				console.log('error al cargar datos de editorial: '+resp.msj);
			}
		}).error(function(){
			console.log('error al cargar datos de xeditorial:');
	});

	$http.get('../bib_tipo/xTipo')
		.success(function(resp){
			if(resp.code=='ok'){
				$scope.tipo=resp.data;
			} else {
				console.log('error al cargar datos tipo: '+resp.msj);
			}
		}).error(function(){
			console.log('error al cargar datos tipo');
	});

    $http.get('../bib_subcategoria/xSubcategoria')
		.success(function(resp){
			if(resp.code=='ok'){
				$scope.subcategoria=resp.data;
				if(resp.data!=null)
				for (var i = $scope.subcategoria.length - 1; i >= 0; i--) {
					$scope.subcategoria[i].estado=false;
				};
			} else {
				console.log('error al cargar datos de subcategorias: '+resp.msj);
			}
		}).error(function(){
			console.log('error al cargar datos');
	});

	$http.get('../bib_autor/xAutor')
		.success(function(resp){
			if(resp.code=='ok'){
				$scope.autor=resp.data;
				if($scope.autor!=null)
				for (var i = $scope.autor.length - 1; i >= 0; i--) {
					$scope.autor[i].estado=false;
				};
			} else {
				console.log('error al cargar datos de autor: '+resp.msj);
			}
		}).error(function(){
			console.log('error al cargar datos');
	});
	
	$scope.guardarEstudios = function(){
		var data={
				'paginas':$scope.estudio.paginas,
				'codigo_tipo':$scope.estudio.codigo_tipo,
                'codigo':$scope.estudio.codigo,
				'nombre':$scope.estudio.nombre,
				'nombre_abreviado':$scope.estudio.nombre_abreviado,
				'precio': $scope.estudio.precio,
				'edicion':$scope.estudio.edicion,
				'fec_publicacion':$filter('date')($scope.estudio.fecha,'yyyy-MM-dd'),
				'fec_creacion':new Date(),
				'fec_modificacion':'',
				'foto': $scope.estudio.foto,
				'archivo': $scope.estudio.archivo,
				'link': $scope.estudio.link,
				'lugar': $scope.estudio.lugar,
				'condicion': $scope.estudio.condicion,
				'resumen':$scope.estudio.resumen,
				'id_idioma': $scope.estudio.id_idioma,
				'id_tipo':$scope.estudio.id_tipo,
				'id_detalle': $scope.estudio.id_detalle,
				'id_setting': $scope.estudio.id_setting,
				'id_editorial':$scope.estudio.id_editorial,
				'autor': $scope.autor,
				'detalle': $scope.subcategoria,
        };
			$http.post($rootScope._sysUrlBase_+'/bib_estudio/xRegistrarEstudio/', data)
				.success(function(resp){
					if(resp.code=='ok'){
						$scope.bib_estudio=resp.data;
						$location.path('/estudios');
					}else{
						console.log('hay error guardarEstudios: '+resp.msj);
					}
				}).error(function(){
					console.log('error al guardar datos: '+resp.msj);
				});
	}

	if (id!=undefined) {
        $http.get('../bib_estudio/xGetEstudio/?id='+id)
            .success(function(resp){
                if(resp.code=='ok'){
                    $scope.bib_estudio=resp.data;
                }else{
                    console.log('hay error!'+resp.msj);
                }
            }).error(function(){
                console.log('error al cargar datos estudio (xGetEstudio): '+resp.msj);
            });
    }

	$scope.editarEstudios = function(){
		var data={
				'id':id,
				'paginas':$scope.bib_estudio.paginas,
				'codigo_tipo':$scope.bib_estudio.codigo_tipo,
                'codigo':$scope.bib_estudio.codigo,
				'nombre':$scope.bib_estudio.nombre,
				'nombre_abreviado':$scope.bib_estudio.nombre_abreviado,
				'precio': $scope.bib_estudio.precio,
				'edicion':$scope.bib_estudio.edicion,
				'fec_publicacion':$filter('date')($scope.bib_estudio.fecha,'yyyy-MM-dd'),
				'fec_creacion':'',
				'fec_modificacion':new Date(),
				'foto': $scope.bib_estudio.foto,
				'archivo': $scope.bib_estudio.archivo,
				'link': $scope.bib_estudio.link,
				'lugar': $scope.bib_estudio.lugar,
				'condicion': $scope.bib_estudio.condicion,
				'resumen':$scope.bib_estudio.resumen,
				'id_idioma': $scope.bib_estudio.id_idioma,
				'id_tipo':$scope.bib_estudio.id_tipo,
				'id_detalle': $scope.bib_estudio.id_detalle,
				'id_setting': $scope.bib_estudio.id_setting,
				'id_editorial':$scope.bib_estudio.id_editorial,
				'autor': $scope.autor,
				'detalle': $scope.subcategoria,
        };
			//console.log($scope.estudio);
			//console.log($scope.autor);
			//console.log(data);
			$http.post($rootScope._sysUrlBase_+'/bib_estudio/xRegistrarEstudio/', data)
				.success(function(resp){
					if(resp.code=='ok'){
						//console.log(resp.data);
						$scope.bib_estudio=resp.data;
					}else{
						console.log('hay error guardarEstudios: '+resp.msj);
					}
				}).error(function(){
					console.log('error al guardar datos: '+resp.msj);
				});
	}

    $http.get('../Apps_countries/xPais')
		.success(function(resp){
			if(resp.code=='ok'){
				$scope.pais=resp.data;
				if(resp.data!=null)
				for (var i = $scope.pais.length - 1; i >= 0; i--) {
					$scope.pais[i].estado=false;
				};
			} else {
				console.log('error al cargar datos de pais: '+resp.msj);
			}
		}).error(function(){
			console.log('error al cargar datos de pais:');
	});

	$http.get('../bib_idioma/xIdioma')
		.success(function(resp){
			if(resp.code=='ok'){
				$scope.idioma=resp.data;
			} else {
				console.log('error al cargar datos de idioma: '+resp.msj);
			}
		}).error(function(){
			console.log('error al cargar datos de idioma:');
	});

	$http.get('../bib_categoria/xCategoria')
		.success(function(resp){
			if(resp.code=='ok'){
				$scope.categoria=resp.data;
			} else {
				console.log('error al cargar datos de categoria: '+resp.msj);
			}
		}).error(function(){
			console.log('error al cargar datos de categoria:');
	});

    $scope.agregarAutor = function(){
		var data={
				'nombre':$scope.autor.nombre,
                'ap_paterno':$scope.autor.ap_paterno,
                'ap_materno':$scope.autor.ap_materno,
				'id_pais':$scope.autor.id_pais,
        };
			console.log($scope.autor);
			console.log(data);
		$http.post($rootScope._sysUrlBase_+'/bib_autor/xRegistrarAutor/', data)
			.success(function(resp){
				if(resp.code=='ok'){
					$scope.bib_autor=resp.data;
				}else{
					console.log('hay error'+resp.msj);
				}
			$http.get('../bib_autor/xAutor')
				.success(function(resp){
					if(resp.code=='ok'){
						$scope.autor=resp.data;
						for (var i = $scope.autor.length - 1; i >= 0; i--) {
							$scope.autor[i].estado=false;
						};
					} else {
						console.log('error al cargar datos de autor: '+resp.msj);
					}
				}).error(function(){
					console.log('error al cargar datos');
				});
			}).error(function(){
				console.log('error al cargar datos: '+resp.msj);
			});

        $scope.autor.nombre=''
        $scope.autor.ap_paterno=''
        $scope.autor.ap_materno=''
	}
	
	$scope.agregarCategoria = function(){
		var data={
				'descripcion':$scope.categoria.descripcion,
        };
			console.log($scope.autor);
			console.log(data);
			$http.post($rootScope._sysUrlBase_+'/bib_categoria/xRegistrarCategoria/', data)
				.success(function(resp){
					if(resp.code=='ok'){
						$scope.bib_categoria=resp.data;
						alert('Categoría Registrada');
					}else{
						console.log('la categoría no se agrego'+resp.msj);
					}
					$http.get('../bib_categoria/xCategoria')
					.success(function(resp){
						if(resp.code=='ok'){
							$scope.categoria=resp.data;
						} else {
							console.log('error al cargar datos de categoria: '+resp.msj);
						}
					}).error(function(){
						console.log('error al cargar datos de categoria:');
					});
			}).error(function(){
					console.log('error al cargar datos: '+resp.msj);
			});
        	$scope.categoria.descripcion=''
	}

	$scope.agregarSubCategoria = function(){
		var data={
				'descripcion':$scope.subcategoria.descripcion,
        };
		
		$http.post($rootScope._sysUrlBase_+'/bib_subcategoria/xRegistrarSubCategoria/', data)
				.success(function(resp){
					if(resp.code=='ok'){
						$scope.bib_subcategoria=resp.data;
					}else{
						console.log('hay error'+resp.msj);
					}

			$http.get('../bib_subcategoria/xSubcategoria')
			.success(function(resp){
				if(resp.code=='ok'){
					$scope.subcategoria=resp.data;
					for (var i = $scope.subcategoria.length - 1; i >= 0; i--) {
						$scope.subcategoria[i].estado=false;
					};
				} else {
					console.log('error al cargar datos de subcategorias: '+resp.msj);
				}
			}).error(function(){
				console.log('error al cargar datos');
			});
			
			}).error(function(){
				console.log('error al cargar datos: '+resp.msj);
			});
     	   $scope.subcategoria.descripcion=''
	}

	$scope.enviarCorreo=function(){
		var data={
			'paraemail':[{email:$scope.email.destinatario,nombre:''}],
			'asunto':$scope.email.asunto,
			'msje':$scope.email.mensaje,
			'adjuntos':[{tipo:'png', src: $rootScope._sysUrlBase_+'/static/libreria/image/'+$scope.estudio.foto}],
		};

		$http.post($rootScope._sysUrlBase_+'/sendemail/enviar_phpmailer_angular/', data)
				.success(function(resp){
					if(resp.code=='ok'){
						$scope.bib_email=resp.data;
						alert('Correo enviado');
						console.log($scope.bib_email);
					}else{
						console.log('xError al enviar correo: '+resp.msj);
					}
			}).error(function(){
					console.log('Error al enviar correo'+resp.msj)
			});
        	$scope.email.asunto=''
        	$scope.email.destinatario=''
        	$scope.email.mensaje=''
	}

	//==============================PAGINACION=================================
	 $scope.pager = {};
     $scope.pages = [];
	 $scope.setPage= function(page){

            if (page < 1 || page > $scope.pager.totalPages) {
                return;
            }
            // get pager object from service
             $scope.GetPager($scope.selectEstudios.length, page);
             $scope.pager.pages=$scope.pages;

            // get current page of items
           $scope.items = $scope.selectEstudios.slice($scope.pager.startIndex, $scope.pager.endIndex + 1);
           //$scope.items = $scope.selectLibros.slice(1, 10);        
           
        }

        // service implementation
        $scope.GetPager=function(totalItems, currentPage, pageSize) {
            // default to first page
            currentPage = currentPage || 1;

            // default page size is 10
            pageSize = pageSize || 10;

            // calculate total pages
            var totalPages = Math.ceil(totalItems / pageSize);

            var startPage, endPage;
            if (totalPages <= 10) {
                // less than 10 total pages so show all
                startPage = 1;
                endPage = totalPages;
            } else {
                // more than 10 total pages so calculate start and end pages
                if (currentPage <= 6) {
                    startPage = 1;
                    endPage = 10;
                } else if (currentPage + 4 >= totalPages) {
                    startPage = totalPages - 9;
                    endPage = totalPages;
                } else {
                    startPage = currentPage - 5;
                    endPage = currentPage + 4;
                }
            }
            
            // calculate start and end item indexes
            var startIndex = (currentPage - 1) * pageSize;
            var endIndex = Math.min(startIndex + pageSize - 1, totalItems - 1);

            // create an array of pages to ng-repeat in the pager control
            //var pages = _.range(startPage, endPage + 1);
            var max = (totalItems / 10)+1 ;
            for (var i=startIndex+1; i<(max); i++) {
		      $scope.pages.push(i);
		    }

            $scope.pager.currentPage = currentPage;
            $scope.pager.totalPages = totalPages;

            $scope.pager.startIndex = startIndex;
            $scope.pager.endIndex = endIndex;
        }

	//=========================================================================
});