<?php
 /**
 * @autor		Generador Abel Chingo Tello , ACHT
 * @fecha		12-07-2017
 * @copyright	Copyright (C) 12-07-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_datos::DatBib_detalle_estudio_editorial', RUTA_BASE, 'sys_datos');
JrCargador::clase('sys_negocio::NegTools', RUTA_BASE, 'sys_negocio');
class NegBib_detalle_estudio_editorial 
{
	protected $id_detalle;
	protected $id_editorial;
	protected $id_estudio;
	
	protected $dataBib_detalle_estudio_editorial;
	protected $oDatBib_detalle_estudio_editorial;	

	public function __construct()
	{
		$this->oDatBib_detalle_estudio_editorial = new DatBib_detalle_estudio_editorial;
	}

	public function __get($prop)
	{
		$metodo = 'get' . ucfirst($prop);
		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}

	public function __set($prop, $valor)
	{
		$this->set($prop, $valor);
	}

	private function prop__($prop, $valor)
	{
		if(is_array($prop)) {
			foreach($prop as $prop_ => $valor) {
				$this->set($prop_, $valor);
			}
		}
		
		$this->set($prop, $valor);
	}
	public function get($prop)
	{
		$metodo = 'get' . ucfirst($prop);		
		if(method_exists($this, $metodo)) {
			return $this->$metodo();
		} else {
			return $this->$prop;
		}
	}
	
	public function set($prop, $valor)
	{
		$metodo = 'set' . ucfirst($prop);
		if(method_exists($this, $metodo)) {
			$this->$metodo($valor);
		} else {
			$this->$prop = $valor;
		}
	}

	public function setLimite($desde, $desplazamiento)
	{
		try {
			$this->limite_desde = $desde;
			$this->limite_desplazamiento = $desplazamiento;
			
			$this->oDatBib_detalle_estudio_editorial->setLimite($this->limite_desde, $this->limite_desplazamiento);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}////////// Fin - Metodos magicos //////////

	public function getNumRegistros($filtros = array())
	{
		try {
			return $this->oDatBib_detalle_estudio_editorial->getNumRegistros($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function buscar($filtros = array())
	{
		try {
			return $this->oDatBib_detalle_estudio_editorial->buscar($filtros);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function listar()
	{
		try {
			return $this->oDatBib_detalle_estudio_editorial->listarall();
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function getXid()
	{
		try {
			return $this->oDatBib_detalle_estudio_editorial->get($this->id_detalle);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function agregar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('bib_detalle_estudio_editorial', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			//$this->oDatBib_detalle_estudio_editorial->iniciarTransaccion('neg_i_Bib_detalle_estudio_editorial');
			$this->id_detalle = $this->oDatBib_detalle_estudio_editorial->insertar($this->id_editorial,$this->id_estudio);
			//$this->oDatBib_detalle_estudio_editorial->terminarTransaccion('neg_i_Bib_detalle_estudio_editorial');	
			return $this->id_detalle;
		} catch(Exception $e) {	
		   //$this->oDatBib_detalle_estudio_editorial->cancelarTransaccion('neg_i_Bib_detalle_estudio_editorial');		
			throw new Exception($e->getMessage());
		}
	}

	public function editar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('bib_detalle_estudio_editorial', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/			
			return $this->oDatBib_detalle_estudio_editorial->actualizar($this->id_detalle,$this->id_editorial,$this->id_estudio);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	
	public function eliminar()
	{
		try {
			/*if(!NegSesion::tiene_acceso('Bib_detalle_estudio_editorial', 'delete')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			return $this->oDatBib_detalle_estudio_editorial->eliminar($this->id_detalle);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}
	}

	public function setId_detalle($pk){
		try {
			$this->dataBib_detalle_estudio_editorial = $this->oDatBib_detalle_estudio_editorial->get($pk);
			if(empty($this->dataBib_detalle_estudio_editorial)) {
				throw new Exception(JrTexto::_("Bib_detalle_estudio_editorial").' '.JrTexto::_("not registered"));
			}
			$this->id_detalle = $this->dataBib_detalle_estudio_editorial["id_detalle"];
			$this->id_editorial = $this->dataBib_detalle_estudio_editorial["id_editorial"];
			$this->id_estudio = $this->dataBib_detalle_estudio_editorial["id_estudio"];
						//falta campos
		} catch(Exception $e) {			
			throw new Exception($e->getMessage());
		}
	}

	public function setCampo($pk, $propiedad, $valor){
		try {
			/*if(!NegSesion::tiene_acceso('bib_detalle_estudio_editorial', 'editar')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->dataBib_detalle_estudio_editorial = $this->oDatBib_detalle_estudio_editorial->get($pk);
			if(empty($this->dataBib_detalle_estudio_editorial)) {
				throw new Exception(JrTexto::_("Bib_detalle_estudio_editorial").' '.JrTexto::_("not registered"));
			}

			return $this->oDatBib_detalle_estudio_editorial->set($pk, $propiedad, $valor);
		} catch(Exception $e) {
			throw new Exception($e->getMessage());
		}

	}
	
	
		
}