<?php
set_time_limit(0);
require 'class.PHPWebSocket.php';
$userchat=array();
function wsOnMessage($clientID, $message, $messageLength,$binary){
	global $Server;
	global $userchat;
	$ip = long2ip($Server->wsClients[$clientID][6]);
	$msj=null;
	if ($messageLength == 0) {
		$Server->wsClose($clientID);
		return;
	}else{
		$msj=json_decode($message,true);
	}
	$user=empty($msj["useremit"])?('Anonimo'.$clientID):@$msj["useremit"];
	$useremitdatos=empty($msj["useremitdatos"])?'':@$msj["useremitdatos"];
	if(!array_key_exists('user'.$clientID,$userchat)){
		$userchat=array_merge($userchat,array('user'.$clientID=>array('user'=>$user,'useremitdatos'=>$useremitdatos,'ip'=>$ip,'id'=>$clientID)));
	}else{
		$userchat['user'.$clientID]=array('user'=>$user,'useremitdatos'=>$useremitdatos,'ip'=>$ip,'id'=>$clientID);
	}
	$msj["useremit"]=@$userchat["user".$clientID];
	unset($msj["useremitdatos"]);
	$data=$msj;
	
	if(!empty($msj["para"])){
	    if(sizeof($userchat)>1)
		foreach($userchat as $curuser)
			if('chat_'.$curuser['user']== $msj["para"])	$Server->wsSend($curuser['id'], json_encode($data));
	}elseif(@$msj["accion"]=='listarconectados'){
		@$data["usuarios"]=$userchat;
		$Server->wsSend($clientID, json_encode($data));
	}else{
		if(sizeof($Server->wsClients)>1)
		foreach($Server->wsClients as $id => $client)
			if($id != $clientID) $Server->wsSend($id, json_encode($data));
	}
}
function wsOnOpen($clientID)// when a client connects
{
	global $Server;
	$ip = long2ip($Server->wsClients[$clientID][6]);
	foreach ($Server->wsClients as $id => $client){//Send a join notice to everyone but the person who joined
		$data["mensaje"]="$clientID ($ip) conectado.";
		if($id!= $clientID)$Server->wsSend($id, json_encode($data));
	}
}
function wsOnClose($clientID, $status){// when a client closes or lost connection
	global $Server;
	$ip = long2ip( $Server->wsClients[$clientID][6]);		
	foreach ( $Server->wsClients as $id => $client){
		$data["mensaje"]="$clientID ($ip) desconectado.";
		if($id!= $clientID)$Server->wsSend($id, json_encode($data));
	} //Send a user left notice to everyone in the room
		
}
//start the server
$Server = new PHPWebSocket();
$Server->bind('message', 'wsOnMessage');
$Server->bind('open', 'wsOnOpen');
$Server->bind('close', 'wsOnClose');
//$ip=gethostbyname($_SERVER['SERVER_NAME']); //optiene la ip
//$host=gethostbyaddr($ip); //optiene nombre segun una ip
$ip='192.168.11.55';
$Server->wsStartServer($ip, 999);
?>