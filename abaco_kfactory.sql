-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 09-08-2017 a las 17:23:30
-- Versión del servidor: 5.7.14
-- Versión de PHP: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `abaco_kfactory`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `actividades`
--

CREATE TABLE `actividades` (
  `idactividad` int(11) NOT NULL,
  `nivel` int(11) NOT NULL,
  `unidad` int(11) NOT NULL,
  `sesion` int(11) NOT NULL,
  `metodologia` int(11) NOT NULL,
  `habilidad` varchar(16) DEFAULT NULL,
  `titulo` varchar(250) DEFAULT NULL,
  `descripcion` text,
  `estado` char(1) NOT NULL DEFAULT '1' COMMENT 'activo o inactivo',
  `url` varchar(100) DEFAULT NULL,
  `idioma` char(2) DEFAULT 'EN',
  `idpersonal` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `actividad_alumno`
--

CREATE TABLE `actividad_alumno` (
  `idactalumno` int(11) NOT NULL,
  `iddetalleactividad` int(11) NOT NULL,
  `idalumno` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `porcentajeprogreso` float NOT NULL,
  `habilidades` varchar(32) NOT NULL,
  `estado` char(2) NOT NULL COMMENT 'termino, pendiente'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `actividad_detalle`
--

CREATE TABLE `actividad_detalle` (
  `iddetalle` int(11) NOT NULL,
  `idactividad` int(11) NOT NULL,
  `pregunta` varchar(200) COLLATE utf8_spanish_ci DEFAULT NULL,
  `orden` int(11) NOT NULL DEFAULT '1',
  `url` varchar(200) COLLATE utf8_spanish_ci DEFAULT NULL,
  `tipo` varchar(100) COLLATE utf8_spanish_ci NOT NULL COMMENT 'video. Arratre',
  `tipo_desarrollo` varchar(100) COLLATE utf8_spanish_ci NOT NULL COMMENT 'c=captivate, D=codigo, M=mixto',
  `tipo_actividad` char(1) COLLATE utf8_spanish_ci NOT NULL COMMENT 'Examen,Actividad',
  `idhabilidad` varchar(16) COLLATE utf8_spanish_ci DEFAULT NULL,
  `texto` text COLLATE utf8_spanish_ci,
  `texto_edit` text COLLATE utf8_spanish_ci,
  `titulo` varchar(250) COLLATE utf8_spanish_ci DEFAULT NULL,
  `descripcion` text COLLATE utf8_spanish_ci,
  `idpersonal` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `alumno`
--

CREATE TABLE `alumno` (
  `dni` int(11) NOT NULL,
  `ape_paterno` varchar(50) NOT NULL,
  `ape_materno` varchar(50) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `fechanac` date DEFAULT NULL,
  `sexo` char(1) NOT NULL,
  `estado_civil` char(1) NOT NULL,
  `ubigeo` char(8) NOT NULL,
  `urbanizacion` varchar(100) NOT NULL,
  `direccion` varchar(100) NOT NULL,
  `telefono` varchar(15) NOT NULL,
  `celular` varchar(15) NOT NULL,
  `email` varchar(75) NOT NULL,
  `idugel` char(4) NOT NULL,
  `regusuario` int(11) NOT NULL,
  `regfecha` date NOT NULL,
  `usuario` varchar(35) NOT NULL,
  `clave` varchar(35) NOT NULL,
  `token` varchar(35) NOT NULL,
  `foto` varchar(100) NOT NULL,
  `estado` tinyint(1) NOT NULL,
  `situacion` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `alumno`
--

INSERT INTO `alumno` (`dni`, `ape_paterno`, `ape_materno`, `nombre`, `fechanac`, `sexo`, `estado_civil`, `ubigeo`, `urbanizacion`, `direccion`, `telefono`, `celular`, `email`, `idugel`, `regusuario`, `regfecha`, `usuario`, `clave`, `token`, `foto`, `estado`, `situacion`) VALUES
(12345678, 'Chingo', 'Tello', 'Abel', '1986-10-27', 'M', 'V', '', '', '', '', '', 'abel_chingo@hotmail.com', '', 0, '2016-11-11', 'alumno', '81dc9bdb52d04dc20036dbd8313ed055', '', '', 1, 1),
(72042592, 'Figueroa', 'Piscoya', 'Eder', '1992-12-21', '', '', '', '', '', '', '', 'edermax1992@gmail.com', '', 0, '2016-11-12', 'efigueroap', '81dc9bdb52d04dc20036dbd8313ed055', '', '', 1, 1),
(87654321, 'muñoz', 'MEZA', 'evelio', '2016-11-01', 'M', 'S', '00000001', 'BELLIDO', 'MEXICO', '251478', '979222660', 'CORREO', '0001', 1, '2016-11-01', 'emunoz', 'c4ca4238a0b923820dcc509a6f75849b', '43371672', '', 1, 1),
(11111111, 'Peña', '-', 'Enny', NULL, '', '', '', '', '', '', '', 'email', '', 0, '2016-11-16', 'enny', '0192023a7bbd73250516f069df18b500', '', '', 1, 1),
(55555555, 'Castro', '-', 'Castro', '2017-02-01', 'M', 'S', '', '', '', '', '', 'docente3@hotmail.com', '', 0, '2017-02-09', 'mcastro', '0192023a7bbd73250516f069df18b500', '8be2efedf58bc5ca5ef33aec86a7a217', '', 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `apps_countries`
--

CREATE TABLE `apps_countries` (
  `id_pais` int(11) NOT NULL,
  `country_code` varchar(2) NOT NULL,
  `country_name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `apps_countries`
--

INSERT INTO `apps_countries` (`id_pais`, `country_code`, `country_name`) VALUES
(1, 'AF', 'Afghanistan'),
(2, 'AL', 'Albania'),
(3, 'DZ', 'Algeria'),
(4, 'DS', 'American Samoa'),
(5, 'AD', 'Andorra'),
(6, 'AO', 'Angola'),
(7, 'AI', 'Anguilla'),
(8, 'AQ', 'Antarctica'),
(9, 'AG', 'Antigua and Barbuda'),
(10, 'AR', 'Argentina'),
(11, 'AM', 'Armenia'),
(12, 'AW', 'Aruba'),
(13, 'AU', 'Australia'),
(14, 'AT', 'Austria'),
(15, 'AZ', 'Azerbaijan'),
(16, 'BS', 'Bahamas'),
(17, 'BH', 'Bahrain'),
(18, 'BD', 'Bangladesh'),
(19, 'BB', 'Barbados'),
(20, 'BY', 'Belarus'),
(21, 'BE', 'Belgium'),
(22, 'BZ', 'Belize'),
(23, 'BJ', 'Benin'),
(24, 'BM', 'Bermuda'),
(25, 'BT', 'Bhutan'),
(26, 'BO', 'Bolivia'),
(27, 'BA', 'Bosnia and Herzegovina'),
(28, 'BW', 'Botswana'),
(29, 'BV', 'Bouvet Island'),
(30, 'BR', 'Brazil'),
(31, 'IO', 'British Indian Ocean Territory'),
(32, 'BN', 'Brunei Darussalam'),
(33, 'BG', 'Bulgaria'),
(34, 'BF', 'Burkina Faso'),
(35, 'BI', 'Burundi'),
(36, 'KH', 'Cambodia'),
(37, 'CM', 'Cameroon'),
(38, 'CA', 'Canada'),
(39, 'CV', 'Cape Verde'),
(40, 'KY', 'Cayman Islands'),
(41, 'CF', 'Central African Republic'),
(42, 'TD', 'Chad'),
(43, 'CL', 'Chile'),
(44, 'CN', 'China'),
(45, 'CX', 'Christmas Island'),
(46, 'CC', 'Cocos (Keeling) Islands'),
(47, 'CO', 'Colombia'),
(48, 'KM', 'Comoros'),
(49, 'CG', 'Congo'),
(50, 'CK', 'Cook Islands'),
(51, 'CR', 'Costa Rica'),
(52, 'HR', 'Croatia (Hrvatska)'),
(53, 'CU', 'Cuba'),
(54, 'CY', 'Cyprus'),
(55, 'CZ', 'Czech Republic'),
(56, 'DK', 'Denmark'),
(57, 'DJ', 'Djibouti'),
(58, 'DM', 'Dominica'),
(59, 'DO', 'Dominican Republic'),
(60, 'TP', 'East Timor'),
(61, 'EC', 'Ecuador'),
(62, 'EG', 'Egypt'),
(63, 'SV', 'El Salvador'),
(64, 'GQ', 'Equatorial Guinea'),
(65, 'ER', 'Eritrea'),
(66, 'EE', 'Estonia'),
(67, 'ET', 'Ethiopia'),
(68, 'FK', 'Falkland Islands (Malvinas)'),
(69, 'FO', 'Faroe Islands'),
(70, 'FJ', 'Fiji'),
(71, 'FI', 'Finland'),
(72, 'FR', 'France'),
(73, 'FX', 'France, Metropolitan'),
(74, 'GF', 'French Guiana'),
(75, 'PF', 'French Polynesia'),
(76, 'TF', 'French Southern Territories'),
(77, 'GA', 'Gabon'),
(78, 'GM', 'Gambia'),
(79, 'GE', 'Georgia'),
(80, 'DE', 'Germany'),
(81, 'GH', 'Ghana'),
(82, 'GI', 'Gibraltar'),
(83, 'GK', 'Guernsey'),
(84, 'GR', 'Greece'),
(85, 'GL', 'Greenland'),
(86, 'GD', 'Grenada'),
(87, 'GP', 'Guadeloupe'),
(88, 'GU', 'Guam'),
(89, 'GT', 'Guatemala'),
(90, 'GN', 'Guinea'),
(91, 'GW', 'Guinea-Bissau'),
(92, 'GY', 'Guyana'),
(93, 'HT', 'Haiti'),
(94, 'HM', 'Heard and Mc Donald Islands'),
(95, 'HN', 'Honduras'),
(96, 'HK', 'Hong Kong'),
(97, 'HU', 'Hungary'),
(98, 'IS', 'Iceland'),
(99, 'IN', 'India'),
(100, 'IM', 'Isle of Man'),
(101, 'ID', 'Indonesia'),
(102, 'IR', 'Iran (Islamic Republic of)'),
(103, 'IQ', 'Iraq'),
(104, 'IE', 'Ireland'),
(105, 'IL', 'Israel'),
(106, 'IT', 'Italy'),
(107, 'CI', 'Ivory Coast'),
(108, 'JE', 'Jersey'),
(109, 'JM', 'Jamaica'),
(110, 'JP', 'Japan'),
(111, 'JO', 'Jordan'),
(112, 'KZ', 'Kazakhstan'),
(113, 'KE', 'Kenya'),
(114, 'KI', 'Kiribati'),
(115, 'KP', 'Korea, Democratic People\'s Republic of'),
(116, 'KR', 'Korea, Republic of'),
(117, 'XK', 'Kosovo'),
(118, 'KW', 'Kuwait'),
(119, 'KG', 'Kyrgyzstan'),
(120, 'LA', 'Lao People\'s Democratic Republic'),
(121, 'LV', 'Latvia'),
(122, 'LB', 'Lebanon'),
(123, 'LS', 'Lesotho'),
(124, 'LR', 'Liberia'),
(125, 'LY', 'Libyan Arab Jamahiriya'),
(126, 'LI', 'Liechtenstein'),
(127, 'LT', 'Lithuania'),
(128, 'LU', 'Luxembourg'),
(129, 'MO', 'Macau'),
(130, 'MK', 'Macedonia'),
(131, 'MG', 'Madagascar'),
(132, 'MW', 'Malawi'),
(133, 'MY', 'Malaysia'),
(134, 'MV', 'Maldives'),
(135, 'ML', 'Mali'),
(136, 'MT', 'Malta'),
(137, 'MH', 'Marshall Islands'),
(138, 'MQ', 'Martinique'),
(139, 'MR', 'Mauritania'),
(140, 'MU', 'Mauritius'),
(141, 'TY', 'Mayotte'),
(142, 'MX', 'Mexico'),
(143, 'FM', 'Micronesia, Federated States of'),
(144, 'MD', 'Moldova, Republic of'),
(145, 'MC', 'Monaco'),
(146, 'MN', 'Mongolia'),
(147, 'ME', 'Montenegro'),
(148, 'MS', 'Montserrat'),
(149, 'MA', 'Morocco'),
(150, 'MZ', 'Mozambique'),
(151, 'MM', 'Myanmar'),
(152, 'NA', 'Namibia'),
(153, 'NR', 'Nauru'),
(154, 'NP', 'Nepal'),
(155, 'NL', 'Netherlands'),
(156, 'AN', 'Netherlands Antilles'),
(157, 'NC', 'New Caledonia'),
(158, 'NZ', 'New Zealand'),
(159, 'NI', 'Nicaragua'),
(160, 'NE', 'Niger'),
(161, 'NG', 'Nigeria'),
(162, 'NU', 'Niue'),
(163, 'NF', 'Norfolk Island'),
(164, 'MP', 'Northern Mariana Islands'),
(165, 'NO', 'Norway'),
(166, 'OM', 'Oman'),
(167, 'PK', 'Pakistan'),
(168, 'PW', 'Palau'),
(169, 'PS', 'Palestine'),
(170, 'PA', 'Panama'),
(171, 'PG', 'Papua New Guinea'),
(172, 'PY', 'Paraguay'),
(173, 'PE', 'Peru'),
(174, 'PH', 'Philippines'),
(175, 'PN', 'Pitcairn'),
(176, 'PL', 'Poland'),
(177, 'PT', 'Portugal'),
(178, 'PR', 'Puerto Rico'),
(179, 'QA', 'Qatar'),
(180, 'RE', 'Reunion'),
(181, 'RO', 'Romania'),
(182, 'RU', 'Russian Federation'),
(183, 'RW', 'Rwanda'),
(184, 'KN', 'Saint Kitts and Nevis'),
(185, 'LC', 'Saint Lucia'),
(186, 'VC', 'Saint Vincent and the Grenadines'),
(187, 'WS', 'Samoa'),
(188, 'SM', 'San Marino'),
(189, 'ST', 'Sao Tome and Principe'),
(190, 'SA', 'Saudi Arabia'),
(191, 'SN', 'Senegal'),
(192, 'RS', 'Serbia'),
(193, 'SC', 'Seychelles'),
(194, 'SL', 'Sierra Leone'),
(195, 'SG', 'Singapore'),
(196, 'SK', 'Slovakia'),
(197, 'SI', 'Slovenia'),
(198, 'SB', 'Solomon Islands'),
(199, 'SO', 'Somalia'),
(200, 'ZA', 'South Africa'),
(201, 'GS', 'South Georgia South Sandwich Islands'),
(202, 'ES', 'Spain'),
(203, 'LK', 'Sri Lanka'),
(204, 'SH', 'St. Helena'),
(205, 'PM', 'St. Pierre and Miquelon'),
(206, 'SD', 'Sudan'),
(207, 'SR', 'Suriname'),
(208, 'SJ', 'Svalbard and Jan Mayen Islands'),
(209, 'SZ', 'Swaziland'),
(210, 'SE', 'Sweden'),
(211, 'CH', 'Switzerland'),
(212, 'SY', 'Syrian Arab Republic'),
(213, 'TW', 'Taiwan'),
(214, 'TJ', 'Tajikistan'),
(215, 'TZ', 'Tanzania, United Republic of'),
(216, 'TH', 'Thailand'),
(217, 'TG', 'Togo'),
(218, 'TK', 'Tokelau'),
(219, 'TO', 'Tonga'),
(220, 'TT', 'Trinidad and Tobago'),
(221, 'TN', 'Tunisia'),
(222, 'TR', 'Turkey'),
(223, 'TM', 'Turkmenistan'),
(224, 'TC', 'Turks and Caicos Islands'),
(225, 'TV', 'Tuvalu'),
(226, 'UG', 'Uganda'),
(227, 'UA', 'Ukraine'),
(228, 'AE', 'United Arab Emirates'),
(229, 'GB', 'United Kingdom'),
(230, 'US', 'United States'),
(231, 'UM', 'United States minor outlying islands'),
(232, 'UY', 'Uruguay'),
(233, 'UZ', 'Uzbekistan'),
(234, 'VU', 'Vanuatu'),
(235, 'VA', 'Vatican City State'),
(236, 'VE', 'Venezuela'),
(237, 'VN', 'Vietnam'),
(238, 'VG', 'Virgin Islands (British)'),
(239, 'VI', 'Virgin Islands (U.S.)'),
(240, 'WF', 'Wallis and Futuna Islands'),
(241, 'EH', 'Western Sahara'),
(242, 'YE', 'Yemen'),
(243, 'ZR', 'Zaire'),
(244, 'ZM', 'Zambia'),
(245, 'ZW', 'Zimbabwe');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `asistencia`
--

CREATE TABLE `asistencia` (
  `idasistencia` int(11) NOT NULL,
  `idgrupo` char(9) COLLATE utf8_spanish_ci NOT NULL,
  `idalumno` char(8) COLLATE utf8_spanish_ci NOT NULL,
  `fecha` date NOT NULL,
  `estado` char(1) COLLATE utf8_spanish_ci NOT NULL,
  `observacion` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL,
  `regusuario` varchar(15) COLLATE utf8_spanish_ci DEFAULT NULL,
  `regfecha` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `aulasvirtuales`
--

CREATE TABLE `aulasvirtuales` (
  `aulaid` int(11) NOT NULL,
  `idnivel` int(11) NOT NULL,
  `idunidad` int(11) NOT NULL,
  `idactividad` int(11) NOT NULL,
  `fecha_inicio` datetime NOT NULL,
  `fecha_final` datetime NOT NULL,
  `titulo` varchar(300) COLLATE utf8_spanish_ci NOT NULL,
  `descripcion` text COLLATE utf8_spanish_ci,
  `moderadores` text COLLATE utf8_spanish_ci NOT NULL,
  `estado` varchar(2) COLLATE utf8_spanish_ci NOT NULL,
  `video` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `chat` text COLLATE utf8_spanish_ci,
  `notas` text COLLATE utf8_spanish_ci,
  `dirigidoa` varchar(2) COLLATE utf8_spanish_ci NOT NULL,
  `estudiantes` text COLLATE utf8_spanish_ci NOT NULL,
  `dni` int(8) NOT NULL,
  `fecha_creado` datetime NOT NULL,
  `portada` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `filtroestudiantes` text COLLATE utf8_spanish_ci,
  `nparticipantes` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `aulasvirtuales`
--

INSERT INTO `aulasvirtuales` (`aulaid`, `idnivel`, `idunidad`, `idactividad`, `fecha_inicio`, `fecha_final`, `titulo`, `descripcion`, `moderadores`, `estado`, `video`, `chat`, `notas`, `dirigidoa`, `estudiantes`, `dni`, `fecha_creado`, `portada`, `filtroestudiantes`, `nparticipantes`) VALUES
(1, 1, 6, 18, '2017-06-09 00:06:00', '2017-06-10 00:06:00', 'Presentación de modulo de clases', 'Aquí explicaremos de las herramientas que se han programado para esta sesión ', '{43831104}', 'I', '120170609105209_1_video.webm', '', '', 'P', '', 43831104, '2017-06-09 22:20:00', '__xRUTABASEx__/static/media/image/N1-U1-A1-20170323063813.jpg', '', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `aulavirtualinvitados`
--

CREATE TABLE `aulavirtualinvitados` (
  `idinvitado` int(11) NOT NULL,
  `idaula` int(11) NOT NULL,
  `dni` int(11) DEFAULT NULL,
  `email` varchar(150) COLLATE utf8_spanish2_ci NOT NULL,
  `asistio` int(11) DEFAULT NULL,
  `como` varchar(2) COLLATE utf8_spanish2_ci NOT NULL,
  `usuario` varchar(150) COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish2_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `biblioteca`
--

CREATE TABLE `biblioteca` (
  `idbiblioteca` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `link` varchar(150) NOT NULL,
  `tipo` varchar(35) NOT NULL,
  `idpersonal` int(11) NOT NULL DEFAULT '0',
  `idnivel` int(11) NOT NULL,
  `idunidad` int(11) NOT NULL,
  `idleccion` int(11) NOT NULL,
  `fechareg` date DEFAULT NULL,
  `estado` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bib_autor`
--

CREATE TABLE `bib_autor` (
  `id_autor` int(11) NOT NULL,
  `nombre` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `ap_paterno` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `ap_materno` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `id_pais` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bib_categoria`
--

CREATE TABLE `bib_categoria` (
  `id_categoria` int(11) NOT NULL,
  `descripcion` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bib_detalle_estudio_editorial`
--

CREATE TABLE `bib_detalle_estudio_editorial` (
  `id_detalle` int(11) NOT NULL,
  `id_editorial` int(11) NOT NULL,
  `id_estudio` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bib_detalle_subcategoria_estudio`
--

CREATE TABLE `bib_detalle_subcategoria_estudio` (
  `id_detalle` int(11) NOT NULL,
  `id_subcategoria` int(11) NOT NULL,
  `id_estudio` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bib_detalle_usuario_estudio`
--

CREATE TABLE `bib_detalle_usuario_estudio` (
  `id_detalle` int(11) NOT NULL,
  `id_personal` int(11) NOT NULL,
  `id_estudio` int(11) NOT NULL,
  `tipo` char(1) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bib_det_estudio_autor`
--

CREATE TABLE `bib_det_estudio_autor` (
  `id_detalle` int(11) NOT NULL,
  `id_estudio` int(11) NOT NULL,
  `id_autor` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bib_editorial`
--

CREATE TABLE `bib_editorial` (
  `id_editorial` int(11) NOT NULL,
  `nombre` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bib_estudio`
--

CREATE TABLE `bib_estudio` (
  `id_estudio` int(11) NOT NULL,
  `paginas` int(11) DEFAULT NULL,
  `codigo_tipo` int(11) DEFAULT NULL,
  `codigo` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `nombre` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `nombre_abreviado` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `precio` double DEFAULT NULL,
  `edicion` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `fec_publicacion` date DEFAULT NULL,
  `fec_creacion` date DEFAULT NULL,
  `fec_modificacion` date DEFAULT NULL,
  `foto` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `archivo` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `link` varchar(500) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `lugar` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `condicion` varchar(15) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `resumen` varchar(500) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_idioma` varchar(7) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `id_detalle` int(11) DEFAULT NULL COMMENT 'tabla detalle_grados_areas',
  `id_setting` int(11) NOT NULL COMMENT 'tabla bib_setting',
  `id_tipo` int(11) NOT NULL,
  `id_editorial` int(11) NOT NULL,
  `id_autor` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bib_historial`
--

CREATE TABLE `bib_historial` (
  `id_historial` int(11) NOT NULL,
  `id_personal` int(11) NOT NULL,
  `id_estudio` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bib_idioma`
--

CREATE TABLE `bib_idioma` (
  `id_idioma` int(11) NOT NULL,
  `descripcion` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `bib_idioma`
--

INSERT INTO `bib_idioma` (`id_idioma`, `descripcion`) VALUES
(1, 'español'),
(2, 'ingles');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bib_portada`
--

CREATE TABLE `bib_portada` (
  `id_portada` int(11) NOT NULL,
  `foto` varchar(255) NOT NULL,
  `estado` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `bib_portada`
--

INSERT INTO `bib_portada` (`id_portada`, `foto`, `estado`) VALUES
(1, '20170801095319_biblioteca.png', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bib_recursos`
--

CREATE TABLE `bib_recursos` (
  `id_recursos` int(11) NOT NULL,
  `id_personal` int(11) NOT NULL,
  `titulo` varchar(250) COLLATE utf8_spanish_ci DEFAULT NULL,
  `texto` text COLLATE utf8_spanish_ci,
  `caratula` text COLLATE utf8_spanish_ci,
  `orden` tinyint(4) NOT NULL,
  `tipo` char(1) COLLATE utf8_spanish_ci NOT NULL COMMENT 'V=video,P=pdf,I=imagen,A=audios',
  `estado` char(1) COLLATE utf8_spanish_ci NOT NULL COMMENT 'S=subido, B=biblioteca',
  `publicado` tinyint(4) NOT NULL DEFAULT '0' COMMENT '1=si, 0=no',
  `compartido` tinyint(4) NOT NULL DEFAULT '0' COMMENT '1=si,0=no'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bib_setting`
--

CREATE TABLE `bib_setting` (
  `id_setting` int(11) NOT NULL,
  `cantidad_descarga` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bib_subcategoria`
--

CREATE TABLE `bib_subcategoria` (
  `id_subcategoria` int(11) NOT NULL,
  `id_categoria` int(11) NOT NULL,
  `descripcion` varchar(20) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bib_tipo`
--

CREATE TABLE `bib_tipo` (
  `id_tipo` int(11) NOT NULL,
  `nombre` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `filtro` int(11) NOT NULL,
  `extension` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `id_registro` int(11) NOT NULL,
  `foto` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bib_tipo_registro`
--

CREATE TABLE `bib_tipo_registro` (
  `id_registro` int(11) NOT NULL,
  `nombre` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `configuracion`
--

CREATE TABLE `configuracion` (
  `nivel` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `ventana` varchar(20) COLLATE utf8_spanish_ci NOT NULL COMMENT 'L=look,P=practice,D=dobyyourself',
  `atributo` char(1) COLLATE utf8_spanish_ci NOT NULL DEFAULT '' COMMENT 'Peso, Intentos',
  `valor` varchar(100) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `configuracion`
--

INSERT INTO `configuracion` (`nivel`, `ventana`, `atributo`, `valor`) VALUES
('A1', 'L', 'P', '10'),
('A1', 'P', 'P', '10'),
('A1', 'D', 'P', '10'),
('A1', 'D', 'I', '3'),
('A2', 'D', 'I', '3'),
('B1', 'D', 'I', '3'),
('B2', 'D', 'I', '3'),
('C1', 'D', 'I', '3');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `configuracion_docente`
--

CREATE TABLE `configuracion_docente` (
  `idconfigdocente` int(11) NOT NULL,
  `iddocente` int(11) NOT NULL,
  `idrol` int(11) NOT NULL,
  `idnivel` int(11) DEFAULT NULL,
  `idunidad` int(11) DEFAULT NULL,
  `idactividad` int(11) DEFAULT NULL,
  `tiempototal` time DEFAULT NULL,
  `tiempoactividades` time DEFAULT NULL,
  `tiempoteacherresrc` time DEFAULT NULL,
  `tiempogames` time DEFAULT NULL,
  `tiempoexamenes` time DEFAULT NULL,
  `escalas` text COLLATE utf8_spanish_ci NOT NULL COMMENT 'JSON'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `configuracion_docente`
--

INSERT INTO `configuracion_docente` (`idconfigdocente`, `iddocente`, `idrol`, `idnivel`, `idunidad`, `idactividad`, `tiempototal`, `tiempoactividades`, `tiempoteacherresrc`, `tiempogames`, `tiempoexamenes`, `escalas`) VALUES
(5, 22222222, 2, 1, 14, 60, '08:38:00', '08:00:00', '00:30:00', '00:08:00', '00:00:00', '[{"nombre":"esc1","minimo":0,"maximo":33.33},{"nombre":"esc2","minimo":33.33,"maximo":66.67},{"nombre":"esc3","minimo":66.67,"maximo":100}]'),
(1, 22222222, 2, 1, 6, 18, '05:30:00', '05:00:00', '00:15:00', '00:15:00', '00:00:00', '[{"nombre":"En inicio","minimo":0,"maximo":25},{"nombre":"En proceso","minimo":25,"maximo":50},{"nombre":"En proceso","minimo":50,"maximo":75},{"nombre":"Logrado","minimo":75,"maximo":100}]'),
(2, 22222222, 2, 1, 6, 55, '05:25:00', '05:00:00', '00:15:00', '00:10:00', '00:00:00', '[{"nombre":"esc1","minimo":0,"maximo":33.33},{"nombre":"esc2","minimo":33.33,"maximo":66.67},{"nombre":"esc3","minimo":66.67,"maximo":100}]'),
(3, 22222222, 2, 1, 6, 56, '05:25:00', '05:00:00', '00:15:00', '00:10:00', '00:00:00', '[{"nombre":"esc1","minimo":0,"maximo":33.33},{"nombre":"esc2","minimo":33.33,"maximo":66.67},{"nombre":"esc3","minimo":66.67,"maximo":100}]'),
(4, 22222222, 2, 1, 6, 57, '05:25:00', '05:00:00', '00:15:00', '00:10:00', '00:00:00', '[{"nombre":"esc1","minimo":0,"maximo":33.33},{"nombre":"esc2","minimo":33.33,"maximo":66.67},{"nombre":"esc3","minimo":66.67,"maximo":100}]'),
(6, 22222222, 2, 1, 7, 21, '08:38:00', '08:00:00', '00:30:00', '00:08:00', '00:00:00', '[{"nombre":"esc1","minimo":0,"maximo":33.33},{"nombre":"esc2","minimo":33.33,"maximo":66.67},{"nombre":"esc3","minimo":66.67,"maximo":100}]'),
(7, 22222222, 2, 1, 14, 61, '08:38:00', '08:00:00', '00:30:00', '00:08:00', '00:00:00', '[{"nombre":"esc1","minimo":0,"maximo":33.33},{"nombre":"esc2","minimo":33.33,"maximo":66.67},{"nombre":"esc3","minimo":66.67,"maximo":100}]'),
(8, 22222222, 2, 1, 9, 27, '08:38:00', '08:00:00', '00:30:00', '00:08:00', '00:00:00', '[{"nombre":"esc1","minimo":0,"maximo":33.33},{"nombre":"esc2","minimo":33.33,"maximo":66.67},{"nombre":"esc3","minimo":66.67,"maximo":100}]'),
(9, 22222222, 2, 1, 9, 58, '08:38:00', '08:00:00', '00:30:00', '00:08:00', '00:00:00', '[{"nombre":"esc1","minimo":0,"maximo":33.33},{"nombre":"esc2","minimo":33.33,"maximo":66.67},{"nombre":"esc3","minimo":66.67,"maximo":100}]'),
(10, 22222222, 2, 1, 7, 22, '08:38:00', '08:00:00', '00:30:00', '00:08:00', '00:00:00', '[{"nombre":"esc1","minimo":0,"maximo":33.33},{"nombre":"esc2","minimo":33.33,"maximo":66.67},{"nombre":"esc3","minimo":66.67,"maximo":100}]'),
(11, 22222222, 2, 1, 10, 30, '08:38:00', '08:00:00', '00:30:00', '00:08:00', '00:00:00', '[{"nombre":"esc1","minimo":0,"maximo":33.33},{"nombre":"esc2","minimo":33.33,"maximo":66.67},{"nombre":"esc3","minimo":66.67,"maximo":100}]'),
(12, 22222222, 2, 1, 7, 23, '08:38:00', '08:00:00', '00:30:00', '00:08:00', '00:00:00', '[{"nombre":"esc1","minimo":0,"maximo":33.33},{"nombre":"esc2","minimo":33.33,"maximo":66.67},{"nombre":"esc3","minimo":66.67,"maximo":100}]'),
(13, 22222222, 2, 1, 8, 24, '08:38:00', '08:00:00', '00:30:00', '00:08:00', '00:00:00', '[{"nombre":"esc1","minimo":0,"maximo":33.33},{"nombre":"esc2","minimo":33.33,"maximo":66.67},{"nombre":"esc3","minimo":66.67,"maximo":100}]'),
(14, 22222222, 2, 1, 8, 25, '08:38:00', '08:00:00', '00:30:00', '00:08:00', '00:00:00', '[{"nombre":"esc1","minimo":0,"maximo":33.33},{"nombre":"esc2","minimo":33.33,"maximo":66.67},{"nombre":"esc3","minimo":66.67,"maximo":100}]'),
(15, 22222222, 2, 1, 10, 31, '08:38:00', '08:00:00', '00:30:00', '00:08:00', '00:00:00', '[{"nombre":"esc1","minimo":0,"maximo":33.33},{"nombre":"esc2","minimo":33.33,"maximo":66.67},{"nombre":"esc3","minimo":66.67,"maximo":100}]'),
(16, 22222222, 2, 1, 8, 26, '08:38:00', '08:00:00', '00:30:00', '00:08:00', '00:00:00', '[{"nombre":"esc1","minimo":0,"maximo":33.33},{"nombre":"esc2","minimo":33.33,"maximo":66.67},{"nombre":"esc3","minimo":66.67,"maximo":100}]'),
(17, 22222222, 2, 1, 10, 32, '08:38:00', '08:00:00', '00:30:00', '00:08:00', '00:00:00', '[{"nombre":"esc1","minimo":0,"maximo":33.33},{"nombre":"esc2","minimo":33.33,"maximo":66.67},{"nombre":"esc3","minimo":66.67,"maximo":100}]'),
(18, 22222222, 2, 1, 10, 59, '08:38:00', '08:00:00', '00:30:00', '00:08:00', '00:00:00', '[{"nombre":"esc1","minimo":0,"maximo":33.33},{"nombre":"esc2","minimo":33.33,"maximo":66.67},{"nombre":"esc3","minimo":66.67,"maximo":100}]'),
(19, 22222222, 2, 1, 9, 28, '08:38:00', '08:00:00', '00:30:00', '00:08:00', '00:00:00', '[{"nombre":"esc1","minimo":0,"maximo":33.33},{"nombre":"esc2","minimo":33.33,"maximo":66.67},{"nombre":"esc3","minimo":66.67,"maximo":100}]'),
(20, 22222222, 2, 1, 9, 29, '08:38:00', '08:00:00', '00:30:00', '00:08:00', '00:00:00', '[{"nombre":"esc1","minimo":0,"maximo":33.33},{"nombre":"esc2","minimo":33.33,"maximo":66.67},{"nombre":"esc3","minimo":66.67,"maximo":100}]'),
(21, 22222222, 2, 1, 11, 33, '08:38:00', '08:00:00', '00:30:00', '00:08:00', '00:00:00', '[{"nombre":"esc1","minimo":0,"maximo":33.33},{"nombre":"esc2","minimo":33.33,"maximo":66.67},{"nombre":"esc3","minimo":66.67,"maximo":100}]'),
(22, 22222222, 2, 1, 11, 34, '08:38:00', '08:00:00', '00:30:00', '00:08:00', '00:00:00', '[{"nombre":"esc1","minimo":0,"maximo":33.33},{"nombre":"esc2","minimo":33.33,"maximo":66.67},{"nombre":"esc3","minimo":66.67,"maximo":100}]'),
(23, 22222222, 2, 1, 11, 35, '08:38:00', '08:00:00', '00:30:00', '00:08:00', '00:00:00', '[{"nombre":"esc1","minimo":0,"maximo":33.33},{"nombre":"esc2","minimo":33.33,"maximo":66.67},{"nombre":"esc3","minimo":66.67,"maximo":100}]'),
(24, 22222222, 2, 1, 12, 36, '08:38:00', '08:00:00', '00:30:00', '00:08:00', '00:00:00', '[{"nombre":"esc1","minimo":0,"maximo":33.33},{"nombre":"esc2","minimo":33.33,"maximo":66.67},{"nombre":"esc3","minimo":66.67,"maximo":100}]'),
(25, 22222222, 2, 1, 12, 37, '08:38:00', '08:00:00', '00:30:00', '00:08:00', '00:00:00', '[{"nombre":"esc1","minimo":0,"maximo":33.33},{"nombre":"esc2","minimo":33.33,"maximo":66.67},{"nombre":"esc3","minimo":66.67,"maximo":100}]'),
(26, 22222222, 2, 1, 12, 38, '08:38:00', '08:00:00', '00:30:00', '00:08:00', '00:00:00', '[{"nombre":"esc1","minimo":0,"maximo":33.33},{"nombre":"esc2","minimo":33.33,"maximo":66.67},{"nombre":"esc3","minimo":66.67,"maximo":100}]'),
(27, 22222222, 2, 1, 13, 39, '08:38:00', '08:00:00', '00:30:00', '00:08:00', '00:00:00', '[{"nombre":"esc1","minimo":0,"maximo":33.33},{"nombre":"esc2","minimo":33.33,"maximo":66.67},{"nombre":"esc3","minimo":66.67,"maximo":100}]'),
(28, 22222222, 2, 1, 13, 40, '08:38:00', '08:00:00', '00:30:00', '00:08:00', '00:00:00', '[{"nombre":"esc1","minimo":0,"maximo":33.33},{"nombre":"esc2","minimo":33.33,"maximo":66.67},{"nombre":"esc3","minimo":66.67,"maximo":100}]'),
(29, 22222222, 2, 1, 14, 42, '08:38:00', '08:00:00', '00:30:00', '00:08:00', '00:00:00', '[{"nombre":"esc1","minimo":0,"maximo":33.33},{"nombre":"esc2","minimo":33.33,"maximo":66.67},{"nombre":"esc3","minimo":66.67,"maximo":100}]'),
(30, 22222222, 2, 1, 13, 41, '08:38:00', '08:00:00', '00:30:00', '00:08:00', '00:00:00', '[{"nombre":"esc1","minimo":0,"maximo":33.33},{"nombre":"esc2","minimo":33.33,"maximo":66.67},{"nombre":"esc3","minimo":66.67,"maximo":100}]'),
(31, 22222222, 2, 1, 14, 43, '08:38:00', '08:00:00', '00:30:00', '00:08:00', '00:00:00', '[{"nombre":"esc1","minimo":0,"maximo":33.33},{"nombre":"esc2","minimo":33.33,"maximo":66.67},{"nombre":"esc3","minimo":66.67,"maximo":100}]'),
(32, 22222222, 2, 1, 14, 44, '08:38:00', '08:00:00', '00:30:00', '00:08:00', '00:00:00', '[{"nombre":"esc1","minimo":0,"maximo":33.33},{"nombre":"esc2","minimo":33.33,"maximo":66.67},{"nombre":"esc3","minimo":66.67,"maximo":100}]'),
(33, 22222222, 2, 1, 15, 45, '08:38:00', '08:00:00', '00:30:00', '00:08:00', '00:00:00', '[{"nombre":"esc1","minimo":0,"maximo":33.33},{"nombre":"esc2","minimo":33.33,"maximo":66.67},{"nombre":"esc3","minimo":66.67,"maximo":100}]'),
(34, 22222222, 2, 1, 15, 46, '08:38:00', '08:00:00', '00:30:00', '00:08:00', '00:00:00', '[{"nombre":"esc1","minimo":0,"maximo":33.33},{"nombre":"esc2","minimo":33.33,"maximo":66.67},{"nombre":"esc3","minimo":66.67,"maximo":100}]'),
(35, 22222222, 2, 1, 15, 47, '08:38:00', '08:00:00', '00:30:00', '00:08:00', '00:00:00', '[{"nombre":"esc1","minimo":0,"maximo":33.33},{"nombre":"esc2","minimo":33.33,"maximo":66.67},{"nombre":"esc3","minimo":66.67,"maximo":100}]'),
(36, 22222222, 2, 1, 16, 48, '08:38:00', '08:00:00', '00:30:00', '00:08:00', '00:00:00', '[{"nombre":"esc1","minimo":0,"maximo":33.33},{"nombre":"esc2","minimo":33.33,"maximo":66.67},{"nombre":"esc3","minimo":66.67,"maximo":100}]'),
(37, 22222222, 2, 1, 16, 49, '08:38:00', '08:00:00', '00:30:00', '00:08:00', '00:00:00', '[{"nombre":"esc1","minimo":0,"maximo":33.33},{"nombre":"esc2","minimo":33.33,"maximo":66.67},{"nombre":"esc3","minimo":66.67,"maximo":100}]'),
(38, 22222222, 2, 1, 16, 50, '08:38:00', '08:00:00', '00:30:00', '00:08:00', '00:00:00', '[{"nombre":"esc1","minimo":0,"maximo":33.33},{"nombre":"esc2","minimo":33.33,"maximo":66.67},{"nombre":"esc3","minimo":66.67,"maximo":100}]'),
(39, 22222222, 2, 1, 17, 51, '08:38:00', '08:00:00', '00:30:00', '00:08:00', '00:00:00', '[{"nombre":"esc1","minimo":0,"maximo":33.33},{"nombre":"esc2","minimo":33.33,"maximo":66.67},{"nombre":"esc3","minimo":66.67,"maximo":100}]'),
(40, 22222222, 2, 1, 17, 52, '08:38:00', '08:00:00', '00:30:00', '00:08:00', '00:00:00', '[{"nombre":"esc1","minimo":0,"maximo":33.33},{"nombre":"esc2","minimo":33.33,"maximo":66.67},{"nombre":"esc3","minimo":66.67,"maximo":100}]'),
(41, 22222222, 2, 1, 17, 53, '08:38:00', '08:00:00', '00:30:00', '00:08:00', '00:00:00', '[{"nombre":"esc1","minimo":0,"maximo":33.33},{"nombre":"esc2","minimo":33.33,"maximo":66.67},{"nombre":"esc3","minimo":66.67,"maximo":100}]');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `dcn`
--

CREATE TABLE `dcn` (
  `iddcn` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle_matricula_alumno`
--

CREATE TABLE `detalle_matricula_alumno` (
  `iddetalle` int(11) NOT NULL,
  `idmatricula` int(11) NOT NULL,
  `idejercicio` int(11) NOT NULL,
  `orden` int(11) NOT NULL,
  `estado` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `diccionario`
--

CREATE TABLE `diccionario` (
  `id` int(11) NOT NULL,
  `palabra` varchar(25) NOT NULL,
  `tipo` varchar(20) NOT NULL,
  `definicion` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `examenes`
--

CREATE TABLE `examenes` (
  `idexamen` int(11) NOT NULL,
  `idnivel` int(11) NOT NULL,
  `idunidad` int(11) NOT NULL,
  `idactividad` int(11) NOT NULL,
  `titulo` varchar(250) COLLATE utf8_spanish_ci NOT NULL,
  `descripcion` text COLLATE utf8_spanish_ci NOT NULL,
  `portada` varchar(150) COLLATE utf8_spanish_ci NOT NULL,
  `fuente` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `fuentesize` int(11) NOT NULL,
  `tipo` varchar(100) COLLATE utf8_spanish_ci NOT NULL COMMENT 'I=inicio,P=intermedio,F=final',
  `grupo` varchar(110) COLLATE utf8_spanish_ci NOT NULL,
  `aleatorio` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1 aletaroio',
  `calificacion_por` char(1) COLLATE utf8_spanish_ci NOT NULL COMMENT 'E=examen, P=pregunta',
  `calificacion_en` char(1) COLLATE utf8_spanish_ci NOT NULL COMMENT 'N=puntaje,P=porcentaje, A=alfanumerico',
  `calificacion_total` text COLLATE utf8_spanish_ci NOT NULL,
  `calificacion_min` int(11) DEFAULT '0',
  `tiempo_por` char(1) COLLATE utf8_spanish_ci NOT NULL COMMENT 'E=examen, P=Preguntas',
  `tiempo_total` time NOT NULL,
  `estado` tinyint(1) NOT NULL,
  `idpersonal` int(11) NOT NULL,
  `fecharegistro` date NOT NULL,
  `nintento` tinyint(4) DEFAULT '1',
  `calificacion` char(1) COLLATE utf8_spanish_ci DEFAULT 'U' COMMENT 'U=ultima nota, M=mejor nota'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `examenes_preguntas`
--

CREATE TABLE `examenes_preguntas` (
  `idpregunta` int(11) NOT NULL,
  `idexamen` int(11) NOT NULL,
  `pregunta` varchar(250) COLLATE utf8_spanish_ci NOT NULL,
  `descripcion` text COLLATE utf8_spanish_ci NOT NULL,
  `ejercicio` text COLLATE utf8_spanish_ci NOT NULL,
  `idpadre` int(11) NOT NULL DEFAULT '0',
  `tiempo` time NOT NULL,
  `puntaje` int(11) NOT NULL,
  `idpersonal` int(11) NOT NULL,
  `fecharegistro` date NOT NULL,
  `template` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `habilidades` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `idcontenedor` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `examen_alumno`
--

CREATE TABLE `examen_alumno` (
  `idexaalumno` int(11) NOT NULL,
  `idexamen` int(11) NOT NULL,
  `idalumno` int(11) NOT NULL,
  `preguntas` text COLLATE utf8_spanish_ci NOT NULL,
  `resultado` varchar(250) COLLATE utf8_spanish_ci NOT NULL,
  `puntajehabilidad` varchar(250) COLLATE utf8_spanish_ci NOT NULL,
  `puntaje` double NOT NULL,
  `resultadojson` varchar(250) COLLATE utf8_spanish_ci NOT NULL,
  `tiempoduracion` time NOT NULL,
  `fecha` date NOT NULL,
  `intento` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `examen_tipo`
--

CREATE TABLE `examen_tipo` (
  `idtipo` smallint(6) NOT NULL,
  `tipo` varchar(150) COLLATE utf8_spanish_ci NOT NULL,
  `estado` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `examen_tipo`
--

INSERT INTO `examen_tipo` (`idtipo`, `tipo`, `estado`) VALUES
(1, 'Induction', 1),
(2, 'Start of unit', 1),
(3, 'Finish unit', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `foros`
--

CREATE TABLE `foros` (
  `idforo` int(11) NOT NULL,
  `titulo` varchar(100) NOT NULL,
  `texto` text NOT NULL,
  `estado` int(11) NOT NULL,
  `reg_usuario` int(11) NOT NULL,
  `rol` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `grupos`
--

CREATE TABLE `grupos` (
  `idgrupo` char(9) COLLATE utf8_spanish_ci NOT NULL,
  `iddocente` char(8) COLLATE utf8_spanish_ci NOT NULL,
  `idlocal` char(4) COLLATE utf8_spanish_ci NOT NULL,
  `idambiente` char(5) COLLATE utf8_spanish_ci NOT NULL,
  `idasistente` char(8) COLLATE utf8_spanish_ci NOT NULL,
  `fechainicio` date NOT NULL,
  `fechafin` date DEFAULT NULL,
  `dias` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL,
  `horas` int(11) NOT NULL,
  `horainicio` time NOT NULL,
  `horafin` time NOT NULL,
  `valor` double NOT NULL,
  `valor_asi` double NOT NULL,
  `regusuario` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `regfecha` date NOT NULL,
  `matriculados` int(11) NOT NULL,
  `nivel` int(11) DEFAULT '1',
  `codigo` varchar(9) COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `grupos`
--

INSERT INTO `grupos` (`idgrupo`, `iddocente`, `idlocal`, `idambiente`, `idasistente`, `fechainicio`, `fechafin`, `dias`, `horas`, `horainicio`, `horafin`, `valor`, `valor_asi`, `regusuario`, `regfecha`, `matriculados`, `nivel`, `codigo`) VALUES
('1', '22222222', '1', '1', '99999999', '2017-03-01', '2017-12-15', 'M,V', 4, '08:00:00', '12:00:00', 20, 11, '72042592', '2017-04-20', 10, 1, NULL),
('2', '22222222', '1', '2', '99999999', '2017-03-01', '2017-12-15', 'L,J', 2, '08:00:00', '10:00:00', 20, 11, '72042592', '2017-04-20', 10, 1, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `grupo_matricula`
--

CREATE TABLE `grupo_matricula` (
  `idgrupo_matricula` int(11) NOT NULL,
  `idalumno` char(8) COLLATE utf8_spanish_ci NOT NULL,
  `idgrupo` char(9) COLLATE utf8_spanish_ci DEFAULT NULL,
  `fechamatricula` date NOT NULL,
  `estado` char(1) COLLATE utf8_spanish_ci NOT NULL,
  `regusuario` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `regfecha` date NOT NULL,
  `subgrupos` text COLLATE utf8_spanish_ci
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `grupo_matricula`
--

INSERT INTO `grupo_matricula` (`idgrupo_matricula`, `idalumno`, `idgrupo`, `fechamatricula`, `estado`, `regusuario`, `regfecha`, `subgrupos`) VALUES
(1, '72042592', '1', '2017-04-03', '1', '72042592', '2017-04-20', NULL),
(2, '12345678', '1', '2017-03-06', '1', '72042592', '2017-04-20', NULL),
(3, '87654321', '1', '2017-03-30', '1', '72042592', '2017-04-20', NULL),
(4, '33333333', '2', '2017-03-08', '1', '72042592', '2017-04-20', NULL),
(5, '44444444', '2', '2017-02-28', '1', '72042592', '2017-04-20', NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `herramientas`
--

CREATE TABLE `herramientas` (
  `idtool` int(11) NOT NULL,
  `idnivel` int(11) NOT NULL,
  `idunidad` int(11) NOT NULL,
  `idactividad` int(11) NOT NULL,
  `idpersonal` int(11) NOT NULL,
  `titulo` varchar(300) DEFAULT NULL,
  `descripcion` text,
  `texto` text NOT NULL,
  `orden` tinyint(4) NOT NULL,
  `tool` char(1) NOT NULL COMMENT 'V=vocabulario,L=pdf,P=pdf,G=Game,R=recursoteachear,A=audiorecord'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `herramientas`
--

INSERT INTO `herramientas` (`idtool`, `idnivel`, `idunidad`, `idactividad`, `idpersonal`, `titulo`, `descripcion`, `texto`, `orden`, `tool`) VALUES
(6, 1, 6, 34, 22222222, NULL, NULL, '__xRUTABASEx__/static/media/pdf/N1-U1-A1-20161226110841.pdf', 1, 'P'),
(3, 1, 6, 34, 22222222, NULL, NULL, '<p>First interest link:<a href="/" target="_blank" title="English">Learn E</a>114525</p>', 1, 'L'),
(7, 1, 6, 34, 22222222, NULL, NULL, '__xRUTABASEx__/static/media/audio/N1-U1-A1-20161214104826.mp3', 1, 'A'),
(8, 1, 6, 34, 438311047, NULL, NULL, '<p>asasa</p>', 0, 'R'),
(9, 1, 6, 34, 99999999, NULL, NULL, '<input type="hidden" name="idgui" id="idgui" value="58ab83c86bb82">\n<input type="hidden" name="orden[-1][1]" value="1">\n<input type="hidden" name="det_tipo[-1][1]" value="V">\n<input type="hidden" name="tipo_desarrollo[-1][1]" value="D">\n<input type="hidden" name="tipo_actividad[-1][1]" value="A">\n\n<div class="plantilla plantilla-crucigrama" data-idgui="58ab83c86bb82">\n  <div class="row">\n    <div class="col-xs-12 pull-right text-right btns-zone nopreview">\n        <a class="btn btn-primary save-crossword">\n            <i class="fa fa-floppy-o"></i> \n            save crossword        </a>\n    </div>\n    <div class="col-md-12">\n      <div id="crossword58ab83c86bb82"><table class="crossword">\n<tbody><tr>\n<td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="" style="background:url(\'http://edutec1.com/static/libs/crusigrama/numbers/1.png\') no-repeat">\n<input data-valn="1" value="" data-row="0" data-col="9" data-val="H" class="tvalcrusigrama ok" style=" " readonly="readonly">\n</td><td class="">\n<input data-valn="1" value="" data-row="0" data-col="10" data-val="i" class="tvalcrusigrama" style=" ">\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td></tr>\n<tr>\n<td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="" style="background:url(\'http://edutec1.com/static/libs/crusigrama/numbers/2.png\') no-repeat">\n<input data-valn="2" value="" data-row="1" data-col="3" data-val="G" class="tvalcrusigrama" style=" ">\n</td><td class="">\n<input data-valn="2" value="" data-row="1" data-col="4" data-val="o" class="tvalcrusigrama" style=" ">\n</td><td class="">\n<input data-valn="2" value="" data-row="1" data-col="5" data-val="o" class="tvalcrusigrama" style=" ">\n</td><td class="">\n<input data-valn="2" value="" data-row="1" data-col="6" data-val="d" class="tvalcrusigrama" style=" ">\n</td><td class="">\n<input data-valn="2" value="" data-row="1" data-col="7" data-val="b" class="tvalcrusigrama" style=" ">\n</td><td class="">\n<input data-valn="2" value="" data-row="1" data-col="8" data-val="y" class="tvalcrusigrama" style=" ">\n</td><td class="">\n<input data-valn="2" value="" data-row="1" data-col="9" data-val="e" class="tvalcrusigrama ok" style=" " readonly="readonly">\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td></tr>\n<tr>\n<td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="">\n<input data-valn="2" value="" data-row="2" data-col="3" data-val="o" class="tvalcrusigrama" style=" ">\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="">\n<input data-valn="2" value="" data-row="2" data-col="9" data-val="l" class="tvalcrusigrama ok" style=" " readonly="readonly">\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td></tr>\n<tr>\n<td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="">\n<input data-valn="2" value="" data-row="3" data-col="3" data-val="o" class="tvalcrusigrama" style=" ">\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="">\n<input data-valn="2" value="" data-row="3" data-col="9" data-val="l" class="tvalcrusigrama ok" style=" " readonly="readonly">\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td></tr>\n<tr>\n<td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="">\n<input data-valn="2" value="" data-row="4" data-col="3" data-val="d" class="tvalcrusigrama" style=" ">\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="" style="background:url(\'http://edutec1.com/static/libs/crusigrama/numbers/3.png\') no-repeat">\n<input data-valn="3" value="" data-row="4" data-col="6" data-val="G" class="tvalcrusigrama" style=" ">\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="">\n<input data-valn="3" value="" data-row="4" data-col="9" data-val="o" class="tvalcrusigrama ok" style=" " readonly="readonly">\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="" style="background:url(\'http://edutec1.com/static/libs/crusigrama/numbers/4.png\') no-repeat">\n<input data-valn="4" value="" data-row="4" data-col="14" data-val="G" class="tvalcrusigrama" style=" ">\n</td><td class="no-border">\n&nbsp;\n</td></tr>\n<tr>\n<td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="">\n<input data-valn="4" value="" data-row="5" data-col="6" data-val="o" class="tvalcrusigrama" style=" ">\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="">\n<input data-valn="4" value="" data-row="5" data-col="14" data-val="o" class="tvalcrusigrama" style=" ">\n</td><td class="no-border">\n&nbsp;\n</td></tr>\n<tr>\n<td class="" style="background:url(\'http://edutec1.com/static/libs/crusigrama/numbers/5.png\') no-repeat">\n<input data-valn="5" value="" data-row="6" data-col="0" data-val="N" class="tvalcrusigrama" style=" ">\n</td><td class="">\n<input data-valn="5" value="" data-row="6" data-col="1" data-val="i" class="tvalcrusigrama" style=" ">\n</td><td class="">\n<input data-valn="5" value="" data-row="6" data-col="2" data-val="c" class="tvalcrusigrama" style=" ">\n</td><td class="">\n<input data-valn="5" value="" data-row="6" data-col="3" data-val="e" class="tvalcrusigrama" style=" ">\n</td><td class="">\n&nbsp;\n</td><td class="">\n<input data-valn="5" value="" data-row="6" data-col="5" data-val="t" class="tvalcrusigrama" style=" ">\n</td><td class="">\n<input data-valn="5" value="" data-row="6" data-col="6" data-val="o" class="tvalcrusigrama" style=" ">\n</td><td class="">\n&nbsp;\n</td><td class="">\n<input data-valn="5" value="" data-row="6" data-col="8" data-val="m" class="tvalcrusigrama" style=" ">\n</td><td class="">\n<input data-valn="5" value="" data-row="6" data-col="9" data-val="e" class="tvalcrusigrama" style=" ">\n</td><td class="">\n<input data-valn="5" value="" data-row="6" data-col="10" data-val="e" class="tvalcrusigrama" style=" ">\n</td><td class="">\n<input data-valn="5" value="" data-row="6" data-col="11" data-val="t" class="tvalcrusigrama" style=" ">\n</td><td class="">\n&nbsp;\n</td><td class="">\n<input data-valn="5" value="" data-row="6" data-col="13" data-val="y" class="tvalcrusigrama" style=" ">\n</td><td class="">\n<input data-valn="5" value="" data-row="6" data-col="14" data-val="o" class="tvalcrusigrama" style=" ">\n</td><td class="">\n<input data-valn="5" value="" data-row="6" data-col="15" data-val="u" class="tvalcrusigrama" style=" ">\n</td></tr>\n<tr>\n<td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="">\n<input data-valn="5" value="" data-row="7" data-col="3" data-val="v" class="tvalcrusigrama" style=" ">\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="">\n<input data-valn="5" value="" data-row="7" data-col="6" data-val="d" class="tvalcrusigrama" style=" ">\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="">\n<input data-valn="5" value="" data-row="7" data-col="14" data-val="d" class="tvalcrusigrama" style=" ">\n</td><td class="no-border">\n&nbsp;\n</td></tr>\n<tr>\n<td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="">\n<input data-valn="5" value="" data-row="8" data-col="3" data-val="e" class="tvalcrusigrama" style=" ">\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td></tr>\n<tr>\n<td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="">\n<input data-valn="5" value="" data-row="9" data-col="3" data-val="n" class="tvalcrusigrama" style=" ">\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="">\n<input data-valn="5" value="" data-row="9" data-col="6" data-val="n" class="tvalcrusigrama" style=" ">\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="">\n<input data-valn="5" value="" data-row="9" data-col="14" data-val="m" class="tvalcrusigrama" style=" ">\n</td><td class="no-border">\n&nbsp;\n</td></tr>\n<tr>\n<td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="">\n<input data-valn="5" value="" data-row="10" data-col="3" data-val="i" class="tvalcrusigrama" style=" ">\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="">\n<input data-valn="5" value="" data-row="10" data-col="6" data-val="i" class="tvalcrusigrama" style=" ">\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="">\n<input data-valn="5" value="" data-row="10" data-col="14" data-val="o" class="tvalcrusigrama" style=" ">\n</td><td class="no-border">\n&nbsp;\n</td></tr>\n<tr>\n<td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="">\n<input data-valn="5" value="" data-row="11" data-col="3" data-val="n" class="tvalcrusigrama" style=" ">\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="">\n<input data-valn="5" value="" data-row="11" data-col="6" data-val="g" class="tvalcrusigrama" style=" ">\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="">\n<input data-valn="5" value="" data-row="11" data-col="14" data-val="r" class="tvalcrusigrama" style=" ">\n</td><td class="no-border">\n&nbsp;\n</td></tr>\n<tr>\n<td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="">\n<input data-valn="5" value="" data-row="12" data-col="3" data-val="g" class="tvalcrusigrama" style=" ">\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="">\n<input data-valn="5" value="" data-row="12" data-col="6" data-val="h" class="tvalcrusigrama" style=" ">\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="">\n<input data-valn="5" value="" data-row="12" data-col="14" data-val="n" class="tvalcrusigrama" style=" ">\n</td><td class="no-border">\n&nbsp;\n</td></tr>\n<tr>\n<td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="">\n<input data-valn="5" value="" data-row="13" data-col="6" data-val="t" class="tvalcrusigrama" style=" ">\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="">\n<input data-valn="5" value="" data-row="13" data-col="14" data-val="i" class="tvalcrusigrama" style=" ">\n</td><td class="no-border">\n&nbsp;\n</td></tr>\n<tr>\n<td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="">\n<input data-valn="5" value="" data-row="14" data-col="14" data-val="n" class="tvalcrusigrama" style=" ">\n</td><td class="no-border">\n&nbsp;\n</td></tr>\n<tr>\n<td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="">\n<input data-valn="5" value="" data-row="15" data-col="14" data-val="g" class="tvalcrusigrama" style=" ">\n</td><td class="no-border">\n&nbsp;\n</td></tr>\n</tbody></table></div>\n      <div class="col-xs-12 col-sm-12 col-md-12 ">\n              <table class="nopreview table table-striped table-responsive" id="alternacru58ab83c86bb82">               \n                <tbody>\n                  <tr>\n                    <td><div class="addtextvideo txtquestion58ab83c86bb82" data-initial_val="Used to express a greeting, answer a telephone, or attract attention.">Used to express a greeting, answer a telephone, or attract attention.</div></td>\n                    <td class="addtextvideo nopreview txtresponse58ab83c86bb82" data-initial_val="Hello">Hello</td>\n                    <td class="nopreview"><span class="removedialog tooltip" title="Remove question"><i class="fa fa-trash"></i> </span></td>\n                  </tr>\n                  \n                  <tr class="hidden" id="addpregunta58ab83c86bb82">\n                    <td><div class="addtextvideo txtquestion58ab83c86bb82">Add Question or reference</div></td>\n                    <td class="addtextvideo nopreview txtresponse58ab83c86bb82">Response</td>\n                    <td class="nopreview"><span class="removedialog tooltip" title="Remove question"><i class="fa fa-trash"></i> </span></td>\n                  </tr>                                \n                <tr>\n                    <td><div class="addtextvideo txtquestion58ab83c86bb82" data-initial_val="Used as an exclamation of greeting">Used as an exclamation of greeting</div></td>\n                    <td class="addtextvideo nopreview txtresponse58ab83c86bb82" data-initial_val="Hi">Hi</td>\n                    <td class="nopreview"><span class="removedialog tooltip" title="Remove question"><i class="fa fa-trash"></i> </span></td>\n                  </tr><tr>\n                    <td><div class="addtextvideo txtquestion58ab83c86bb82" data-initial_val="A polite formula used on being introduced to someone">A polite formula used on being introduced to someone</div></td>\n                    <td class="addtextvideo nopreview txtresponse58ab83c86bb82" data-initial_val="Nice to meet you">Nice to meet you</td>\n                    <td class="nopreview"><span class="removedialog tooltip" title="Remove question"><i class="fa fa-trash"></i> </span></td>\n                  </tr><tr>\n                    <td><div class="addtextvideo txtquestion58ab83c86bb82" data-initial_val="A conventional expression at meeting or parting in the morning.">A conventional expression at meeting or parting in the morning.</div></td>\n                    <td class="addtextvideo nopreview txtresponse58ab83c86bb82" data-initial_val="Good morning">Good morning</td>\n                    <td class="nopreview"><span class="removedialog tooltip" title="Remove question"><i class="fa fa-trash"></i> </span></td>\n                  </tr><tr>\n                    <td><div class="addtextvideo txtquestion58ab83c86bb82" data-initial_val="A conventional expression used at meeting or parting in the evening.">A conventional expression used at meeting or parting in the evening.</div></td>\n                    <td class="addtextvideo nopreview txtresponse58ab83c86bb82" data-initial_val="Good evening">Good evening</td>\n                    <td class="nopreview"><span class="removedialog tooltip" title="Remove question"><i class="fa fa-trash"></i> </span></td>\n                  </tr><tr>\n                    <td><div class="addtextvideo txtquestion58ab83c86bb82" data-initial_val="An expression of farewell used in parting at nighttime or when going to sleep.">An expression of farewell used in parting at nighttime or when going to sleep.</div></td>\n                    <td class="addtextvideo nopreview txtresponse58ab83c86bb82" data-initial_val="Good night">Good night</td>\n                    <td class="nopreview"><span class="removedialog tooltip" title="Remove question"><i class="fa fa-trash"></i> </span></td>\n                  </tr><tr>\n                    <td><div class="addtextvideo txtquestion58ab83c86bb82" data-initial_val="Used when someone leaves.">Used when someone leaves.</div></td>\n                    <td class="addtextvideo nopreview txtresponse58ab83c86bb82" data-initial_val="Goodbye">Goodbye</td>\n                    <td class="nopreview"><span class="removedialog tooltip" title="Remove question"><i class="fa fa-trash"></i> </span></td>\n                  </tr></tbody>              \n              </table>\n              <div class="text-center nopreview">\n                <span class="btn btn-xs btn-primary addclone58ab83c86bb82 nopreview tooltip" data-source="#addpregunta58ab83c86bb82" data-donde="#alternacru58ab83c86bb82" title="Add more Question or reference">\n                  <i class="fa fa-plus-square"></i>\n                </span> \n\n                <span class="btn btn-xs btn-primary btngeneratecrusigrama nopreview tooltip" data-donde="#txtdialogocole" title="Generate crossword">\n                  <i class="fa fa-gamepad"></i>\n                </span>\n              </div>\n              <table class="table table-responsive" id="tbopcion58ab83c86bb82">\n                  <thead>\n                      <tr>\n                          <th class="text-center">Horizontal</th>\n                          <th class="text-center">Vertical</th>\n                      </tr>\n                  </thead>\n                  <tbody>\n                      <tr>\n                          <td><ul id="across58ab83c86bb82"><li class="crusigramalista" data-valn="1" data-orient="row" data-val="HI"><strong>1.</strong> Used as an exclamation of greeting</li>\n<li class="crusigramalista" data-valn="2" data-orient="row" data-val="GOODBYE"><strong>2.</strong> Used when someone leaves.</li>\n<li class="crusigramalista" data-valn="5" data-orient="row" data-val="NICE TO MEET YOU"><strong>5.</strong> A polite formula used on being introduced to someone</li></ul></td>\n                          <td><ul id="down58ab83c86bb82"><li class="crusigramalista" data-valn="1" data-orient="col" data-val="HELLO" style="color: rgb(0, 128, 0); text-decoration: line-through;"><strong>1.</strong> Used to express a greeting, answer a telephone, or attract attention.</li>\n<li class="crusigramalista" data-valn="2" data-orient="col" data-val="GOOD EVENING"><strong>2.</strong> A conventional expression used at meeting or parting in the evening.</li>\n<li class="crusigramalista" data-valn="3" data-orient="col" data-val="GOOD NIGHT"><strong>3.</strong> An expression of farewell used in parting at nighttime or when going to sleep.</li>\n<li class="crusigramalista" data-valn="4" data-orient="col" data-val="GOOD MORNING"><strong>4.</strong> A conventional expression at meeting or parting in the morning.</li></ul></td>\n                      </tr>\n                  </tbody>\n              </table>\n            </div>      \n    </div>\n  </div>\n</div>\n\n<script type="text/javascript">\n$(\'.tooltip\').tooltip();\n\n$(document).ready(function(){\n  $(\'#alternacru58ab83c86bb82\').on(\'click\',\'.removedialog\',function(){\n    $(this).parent().parent(\'tr\').remove();\n  });\n\n  $(\'.addclone58ab83c86bb82\').click(function(){\n    var de=$(this).attr(\'data-source\');\n    var adonde=$(this).attr(\'data-donde\');\n    var clon=$(de).clone();\n    $(adonde).append(\'<tr>\'+clon.html()+\'</tr>\');\n    $(\'.tooltip\').tooltip();  \n  });\n\n  $(\'.btngeneratecrusigrama\').click(function(){\n      var question=$(\'.txtquestion58ab83c86bb82\');\n      var response=$(\'.txtresponse58ab83c86bb82\');\n      var datosp=[], datosf=[];\n      response.each(function(){\n        if(!$(this).parent().hasClass(\'hidden\')){\n          txt=$(this).text();\n          datosp.push(txt.trim());\n        }\n      });\n      question.each(function(){\n       if(!$(this).parent().parent().hasClass(\'hidden\')){\n          txt=$(this).text();\n          datosf.push(txt.trim());\n        }\n      });      \n      var datos=[{palabras:datosp,frase:datosf}];\n      $(\'#tbopcion58ab83c86bb82\').removeClass(\'hidden\');\n      cargarcrucigrama(\'58ab83c86bb82\',datos);\n\n      $(\'.btns-zone\').show(\'fast\');\n  });\n\n  $(\'#alternacru58ab83c86bb82\')\n      .on(\'click\',\'.addtextvideo\', function(e){\n        if( $(\'.preview\').is(\':visible\') ){ //si btn.preview es Visible:estamos en edicion\n          var in_=$(this);\n          var texto_inicial = $(this).text().trim();\n          $(this).attr(\'data-initial_val\', texto_inicial);\n          in_.html(\'<input type="text" required="required" class="form-control" id="inputEditTexto" value="\'+ texto_inicial +\'">\');\n          if($(this).hasClass("addtime")){\n            $(\'input#inputEditTexto\',in_).addClass(\'addtime\');\n          }\n          $(\'input#inputEditTexto\',in_).focus().select();\n        }\n      })\n      .on(\'blur\',\'input#inputEditTexto\',function(){\n        var dataesc=$(this).attr(\'data-esc\'); \n        var vini=$(this).parent().attr(\'data-initial_val\');\n        var vin=$(this).val();\n        if(dataesc) _intxt=vini;\n        else _intxt=vin;\n        $(this).removeAttr(\'data-esc\');\n        $(this).parent().attr(\'data-initial_val\',_intxt); \n        if($(this).hasClass("addtime")){   \n          var time=_intxt.split(":");\n          var h=0,m=0,s=0;\n          if(time.length==3){h=parseInt(time[0]);m=parseInt(time[1]);s=parseInt(time[2])}\n          if(time.length==2){m=parseInt(time[0]);s=parseInt(time[1])}\n          if(time.length==1){s=parseInt(time[0])}\n          s=(h*3600)+(m*60)+s;\n          var parent = $(this).parent().parent().parent().parent().attr(\'data-time\',s);\n        }\n        $(this).parent().text(_intxt);\n      })\n      .on(\'keypress\',\'input#inputEditTexto\', function(e){      \n        if(e.which == 13){ //Enter pressed\n          e.preventDefault();          \n          $(this).trigger(\'blur\');\n        } \n      }).on(\'keyup\',\'input#inputEditTexto\', function(e){      \n        if(e.which == 27){ //Enter esc\n          $(this).attr(\'data-esc\',"1");\n          $(this).trigger(\'blur\');\n        }\n      });\n\n  $(\'#crossword58ab83c86bb82\').on(\'keydown\',\'input.tvalcrusigrama\',function(ev){\n    if($(this).hasClass(\'ok\')) return false;\n    $(this).val(\'\');\n  });\n\n  $(\'#crossword58ab83c86bb82\').on(\'keyup\',\'input.tvalcrusigrama\',function(){\n    val =$(this).data(\'val\');\n    val2=$(this).val();\n    var tvalrow=[\'--\'];\n    $(\'#across58ab83c86bb82\').find(\'li[data-orient="row"]\').each(function(){\n      var rval=$(this).data(\'val\');\n      tvalrow.push(rval.toUpperCase());\n    });\n    var tvalcol=new Array();\n    $(\'#down58ab83c86bb82\').find(\'li[data-orient="col"]\').each(function(){\n      var rval=$(this).data(\'val\');\n      tvalcol.push(rval.toUpperCase());\n    });\n    if(val.trim().toUpperCase()==val2.trim().toUpperCase()){\n      $(this).addClass(\'ok\').removeClass(\'error\').attr(\'readonly\',\'true\');\n      var r=$(this).data(\'row\');\n      var c=$(this).data(\'col\');\n      var todosrow=$(\'#crossword58ab83c86bb82\').find(\'input.tvalcrusigrama.ok[data-row="\'+r+\'"]\');\n      var palabra=new Array();\n      if(todosrow.length>1){        \n        $.each(todosrow,function(){         \n          var c=$(this).data(\'col\');\n          var v_=$(this).data(\'val\');\n          palabra[c]=v_;\n        });\n        txt=palabra.join("").toUpperCase();\n        if(tvalrow.indexOf(txt)>-1){\n          $(\'#across58ab83c86bb82\').find(\'li[data-val="\'+txt+\'"]\').css({color:\'green\',\'text-decoration\':\'line-through\'});\n        }\n      }\n      //var palabra=[];\n      var todoscol=$(\'#crossword58ab83c86bb82\').find(\'input.tvalcrusigrama.ok[data-col="\'+c+\'"]\');\n      var palabra=new Array();\n      if(todoscol.length>1){\n        $.each(todoscol,function(){         \n          var c=$(this).data(\'row\');\n          var v_=$(this).data(\'val\');\n          palabra[c]=v_;\n        });\n        txt=palabra.join("").toUpperCase();\n        if(tvalcol.indexOf(txt)>-1){\n          $(\'#down58ab83c86bb82\').find(\'li[data-val="\'+txt+\'"]\').css({color:\'green\',\'text-decoration\':\'line-through\'});\n        }\n      }      \n    }else\n      $(this).addClass(\'error\').removeClass(\'ok\');\n      \n  });\n\n  $(\'.save-crossword\').click(function(e) {\n    e.preventDefault();\n    saveGame();\n  });\n\n  var iniciarCrucigrama = function(){\n    var valRol = $(\'#hRol\').val();\n    if( valRol!=\'\' && valRol!=undefined ){\n        if(valRol != 1){\n            $(\'.plantilla-crucigrama\').find(\'.nopreview\').remove();\n        }\n    }\n  };\n\n  iniciarCrucigrama();\n\n});\n\n</script>', 0, 'G'),
(10, 1, 6, 18, 99999999, 'Greetings and farewells', 'Search for the greetings and farewells words', '<link rel="stylesheet" href="__xRUTABASEx__/frontend/plantillas/tema1/css/actividad_sopaletras.css">\n<input type="hidden" name="idgui" id="idgui" value="58acd4c50fca0">\n<div class="plantilla plantilla-sopaletras" data-idgui="58acd4c50fca0">\n    <div class="row nopreview" id="pnl-edit" style="display: none;">\n        <div class="col-xs-12 btns-zone">\n            <a class="btn btn-info add-word-game" data-clonefrom="#to-clone" data-cloneto="#list-words-game">\n                <i class="fa fa-plus-circle"></i> \n                add word            </a>\n            <div class="col-xs-12 col-sm-6 col-md-4 item-word-game hidden" id="to-clone">\n                <div class="form-group has-feedback">\n                    <input type="text" class="form-control word-game" placeholder="write a word" maxlength="18">\n                    <span class="glyphicon glyphicon-trash form-control-feedback" title="remove word"></span>\n                </div>\n            </div>\n        </div>\n\n        <div class="col-xs-12" id="list-words-game">\n            <div class="col-xs-12 col-sm-6 col-md-4 item-word-game">\n                <div class="form-group has-feedback">\n                    <input type="text" class="form-control word-game" placeholder="write a word" maxlength="18" autofocus="" value="Hello">\n                    <span class="glyphicon glyphicon-trash form-control-feedback" data-tooltip="tooltip" title="remove word"></span>\n                </div>\n            </div>\n\n        <div class="col-xs-12 col-sm-6 col-md-4 item-word-game">\n                <div class="form-group has-feedback">\n                    <input type="text" class="form-control word-game" placeholder="write a word" maxlength="18" id="input-1487721682369" value="goodmorning">\n                    <span class="glyphicon glyphicon-trash form-control-feedback" title="remove word" data-tooltip="tooltip"></span>\n                </div>\n            </div><div class="col-xs-12 col-sm-6 col-md-4 item-word-game">\n                <div class="form-group has-feedback">\n                    <input type="text" class="form-control word-game" placeholder="write a word" maxlength="18" id="input-1487721845031" value="goodbye">\n                    <span class="glyphicon glyphicon-trash form-control-feedback" title="remove word" data-tooltip="tooltip"></span>\n                </div>\n            </div><div class="col-xs-12 col-sm-6 col-md-4 item-word-game">\n                <div class="form-group has-feedback">\n                    <input type="text" class="form-control word-game" placeholder="write a word" maxlength="18" id="input-1487721849080" value="goodnight">\n                    <span class="glyphicon glyphicon-trash form-control-feedback" title="remove word" data-tooltip="tooltip"></span>\n                </div>\n            </div><div class="col-xs-12 col-sm-6 col-md-4 item-word-game">\n                <div class="form-group has-feedback">\n                    <input type="text" class="form-control word-game" placeholder="write a word" maxlength="18" id="input-1487721857065" value="goodevening">\n                    <span class="glyphicon glyphicon-trash form-control-feedback" title="remove word" data-tooltip="tooltip"></span>\n                </div>\n            </div><div class="col-xs-12 col-sm-6 col-md-4 item-word-game">\n                <div class="form-group has-feedback">\n                    <input type="text" class="form-control word-game" placeholder="write a word" maxlength="18" id="input-1487721862863" value="goodafternoon">\n                    <span class="glyphicon glyphicon-trash form-control-feedback" title="remove word" data-tooltip="tooltip"></span>\n                </div>\n            </div></div>\n\n        <div class="col-xs-12 text-right">\n            <a class="btn btn-primary create-word-search">\n                <i class="fa fa-gamepad"></i>\n                create word search            </a>\n        </div>\n    </div>\n\n    <div class="row" id="findword-game" style="display: block;">\n        <div class="col-xs-12 pull-right text-right btns-zone nopreview">\n            <a class="btn btn-success edit-wordfind">\n                <i class="fa fa-pencil-square-o"></i>\n                Edit word search            </a>\n            <a class="btn btn-primary save-wordfind">\n                <i class="fa fa-floppy-o"></i> \n                Save word search            </a>\n        </div>\n\n        <div class="col-xs-12" style="text-align: center;">\n            <div id="puzzle"><div><button class="puzzleSquare" x="0" y="0">c</button><button class="puzzleSquare" x="1" y="0">j</button><button class="puzzleSquare" x="2" y="0">u</button><button class="puzzleSquare" x="3" y="0">f</button><button class="puzzleSquare" x="4" y="0">f</button><button class="puzzleSquare" x="5" y="0">v</button><button class="puzzleSquare" x="6" y="0">o</button><button class="puzzleSquare" x="7" y="0">y</button><button class="puzzleSquare" x="8" y="0">r</button><button class="puzzleSquare" x="9" y="0">b</button><button class="puzzleSquare" x="10" y="0">y</button><button class="puzzleSquare" x="11" y="0">g</button><button class="puzzleSquare" x="12" y="0">g</button></div><div><button class="puzzleSquare" x="0" y="1">h</button><button class="puzzleSquare" x="1" y="1">i</button><button class="puzzleSquare" x="2" y="1">g</button><button class="puzzleSquare" x="3" y="1">n</button><button class="puzzleSquare" x="4" y="1">i</button><button class="puzzleSquare" x="5" y="1">n</button><button class="puzzleSquare" x="6" y="1">r</button><button class="puzzleSquare" x="7" y="1">o</button><button class="puzzleSquare" x="8" y="1">m</button><button class="puzzleSquare" x="9" y="1">d</button><button class="puzzleSquare" x="10" y="1">o</button><button class="puzzleSquare" x="11" y="1">o</button><button class="puzzleSquare" x="12" y="1">g</button></div><div><button class="puzzleSquare" x="0" y="2">f</button><button class="puzzleSquare" x="1" y="2">k</button><button class="puzzleSquare" x="2" y="2">u</button><button class="puzzleSquare" x="3" y="2">i</button><button class="puzzleSquare" x="4" y="2">i</button><button class="puzzleSquare" x="5" y="2">h</button><button class="puzzleSquare" x="6" y="2">m</button><button class="puzzleSquare" x="7" y="2">l</button><button class="puzzleSquare" x="8" y="2">g</button><button class="puzzleSquare" x="9" y="2">o</button><button class="puzzleSquare" x="10" y="2">e</button><button class="puzzleSquare" x="11" y="2">o</button><button class="puzzleSquare" x="12" y="2">o</button></div><div><button class="puzzleSquare" x="0" y="3">h</button><button class="puzzleSquare" x="1" y="3">a</button><button class="puzzleSquare" x="2" y="3">k</button><button class="puzzleSquare" x="3" y="3">h</button><button class="puzzleSquare" x="4" y="3">i</button><button class="puzzleSquare" x="5" y="3">n</button><button class="puzzleSquare" x="6" y="3">d</button><button class="puzzleSquare" x="7" y="3">l</button><button class="puzzleSquare" x="8" y="3">d</button><button class="puzzleSquare" x="9" y="3">e</button><button class="puzzleSquare" x="10" y="3">o</button><button class="puzzleSquare" x="11" y="3">d</button><button class="puzzleSquare" x="12" y="3">o</button></div><div><button class="puzzleSquare" x="0" y="4">g</button><button class="puzzleSquare" x="1" y="4">p</button><button class="puzzleSquare" x="2" y="4">n</button><button class="puzzleSquare" x="3" y="4">r</button><button class="puzzleSquare" x="4" y="4">h</button><button class="puzzleSquare" x="5" y="4">i</button><button class="puzzleSquare" x="6" y="4">u</button><button class="puzzleSquare" x="7" y="4">e</button><button class="puzzleSquare" x="8" y="4">b</button><button class="puzzleSquare" x="9" y="4">d</button><button class="puzzleSquare" x="10" y="4">n</button><button class="puzzleSquare" x="11" y="4">a</button><button class="puzzleSquare" x="12" y="4">d</button></div><div><button class="puzzleSquare" x="0" y="5">w</button><button class="puzzleSquare" x="1" y="5">k</button><button class="puzzleSquare" x="2" y="5">o</button><button class="puzzleSquare" x="3" y="5">t</button><button class="puzzleSquare" x="4" y="5">s</button><button class="puzzleSquare" x="5" y="5">w</button><button class="puzzleSquare" x="6" y="5">v</button><button class="puzzleSquare" x="7" y="5">H</button><button class="puzzleSquare" x="8" y="5">n</button><button class="puzzleSquare" x="9" y="5">a</button><button class="puzzleSquare" x="10" y="5">b</button><button class="puzzleSquare" x="11" y="5">f</button><button class="puzzleSquare" x="12" y="5">b</button></div><div><button class="puzzleSquare" x="0" y="6">g</button><button class="puzzleSquare" x="1" y="6">e</button><button class="puzzleSquare" x="2" y="6">t</button><button class="puzzleSquare" x="3" y="6">s</button><button class="puzzleSquare" x="4" y="6">p</button><button class="puzzleSquare" x="5" y="6">e</button><button class="puzzleSquare" x="6" y="6">f</button><button class="puzzleSquare" x="7" y="6">i</button><button class="puzzleSquare" x="8" y="6">g</button><button class="puzzleSquare" x="9" y="6">l</button><button class="puzzleSquare" x="10" y="6">u</button><button class="puzzleSquare" x="11" y="6">t</button><button class="puzzleSquare" x="12" y="6">y</button></div><div><button class="puzzleSquare" x="0" y="7">c</button><button class="puzzleSquare" x="1" y="7">d</button><button class="puzzleSquare" x="2" y="7">y</button><button class="puzzleSquare" x="3" y="7">t</button><button class="puzzleSquare" x="4" y="7">n</button><button class="puzzleSquare" x="5" y="7">r</button><button class="puzzleSquare" x="6" y="7">g</button><button class="puzzleSquare" x="7" y="7">e</button><button class="puzzleSquare" x="8" y="7">s</button><button class="puzzleSquare" x="9" y="7">w</button><button class="puzzleSquare" x="10" y="7">g</button><button class="puzzleSquare" x="11" y="7">e</button><button class="puzzleSquare" x="12" y="7">e</button></div><div><button class="puzzleSquare" x="0" y="8">f</button><button class="puzzleSquare" x="1" y="8">t</button><button class="puzzleSquare" x="2" y="8">u</button><button class="puzzleSquare" x="3" y="8">i</button><button class="puzzleSquare" x="4" y="8">o</button><button class="puzzleSquare" x="5" y="8">h</button><button class="puzzleSquare" x="6" y="8">p</button><button class="puzzleSquare" x="7" y="8">v</button><button class="puzzleSquare" x="8" y="8">r</button><button class="puzzleSquare" x="9" y="8">o</button><button class="puzzleSquare" x="10" y="8">k</button><button class="puzzleSquare" x="11" y="8">r</button><button class="puzzleSquare" x="12" y="8">k</button></div><div><button class="puzzleSquare" x="0" y="9">o</button><button class="puzzleSquare" x="1" y="9">u</button><button class="puzzleSquare" x="2" y="9">n</button><button class="puzzleSquare" x="3" y="9">r</button><button class="puzzleSquare" x="4" y="9">t</button><button class="puzzleSquare" x="5" y="9">j</button><button class="puzzleSquare" x="6" y="9">o</button><button class="puzzleSquare" x="7" y="9">k</button><button class="puzzleSquare" x="8" y="9">c</button><button class="puzzleSquare" x="9" y="9">o</button><button class="puzzleSquare" x="10" y="9">w</button><button class="puzzleSquare" x="11" y="9">n</button><button class="puzzleSquare" x="12" y="9">k</button></div><div><button class="puzzleSquare" x="0" y="10">l</button><button class="puzzleSquare" x="1" y="10">g</button><button class="puzzleSquare" x="2" y="10">u</button><button class="puzzleSquare" x="3" y="10">n</button><button class="puzzleSquare" x="4" y="10">u</button><button class="puzzleSquare" x="5" y="10">p</button><button class="puzzleSquare" x="6" y="10">g</button><button class="puzzleSquare" x="7" y="10">a</button><button class="puzzleSquare" x="8" y="10">y</button><button class="puzzleSquare" x="9" y="10">m</button><button class="puzzleSquare" x="10" y="10">c</button><button class="puzzleSquare" x="11" y="10">o</button><button class="puzzleSquare" x="12" y="10">n</button></div><div><button class="puzzleSquare" x="0" y="11">l</button><button class="puzzleSquare" x="1" y="11">m</button><button class="puzzleSquare" x="2" y="11">h</button><button class="puzzleSquare" x="3" y="11">s</button><button class="puzzleSquare" x="4" y="11">k</button><button class="puzzleSquare" x="5" y="11">m</button><button class="puzzleSquare" x="6" y="11">n</button><button class="puzzleSquare" x="7" y="11">h</button><button class="puzzleSquare" x="8" y="11">f</button><button class="puzzleSquare" x="9" y="11">g</button><button class="puzzleSquare" x="10" y="11">r</button><button class="puzzleSquare" x="11" y="11">o</button><button class="puzzleSquare" x="12" y="11">f</button></div><div><button class="puzzleSquare" x="0" y="12">c</button><button class="puzzleSquare" x="1" y="12">g</button><button class="puzzleSquare" x="2" y="12">j</button><button class="puzzleSquare" x="3" y="12">r</button><button class="puzzleSquare" x="4" y="12">d</button><button class="puzzleSquare" x="5" y="12">j</button><button class="puzzleSquare" x="6" y="12">j</button><button class="puzzleSquare" x="7" y="12">l</button><button class="puzzleSquare" x="8" y="12">b</button><button class="puzzleSquare" x="9" y="12">n</button><button class="puzzleSquare" x="10" y="12">k</button><button class="puzzleSquare" x="11" y="12">n</button><button class="puzzleSquare" x="12" y="12">n</button></div></div>\n        </div>\n        <div class="col-xs-12">\n            <div id="words"><ul><li class="word Hello">Hello</li><li class="word goodafternoon">goodafternoon</li><li class="word goodbye">goodbye</li><li class="word goodevening">goodevening</li><li class="word goodmorning">goodmorning</li><li class="word goodnight">goodnight</li></ul></div>\n        </div>\n    </div>\n</div>\n\n<script>\n\n$("*[data-tooltip=\\"tooltip\\"]").tooltip();\n\nvar inputsValidos = function(){\n    var rspta = true;\n    var $contenInputs = $("#list-words-game");\n    $contenInputs.find("input.word-game").each(function() {\n        var valor = $(this).val();\n        if(valor.length<=1){\n            $(this).parents(".form-group").addClass("has-error");\n        } else {\n            $(this).parents(".form-group").removeClass("has-error");\n        }\n    });\n    $contenInputs.find("input.word-game").each(function() {\n        var valor = $(this).val();\n        if(valor.length<=1){\n            rspta = false;\n            return false;\n        }\n    });\n    return rspta;\n};\n\nvar obtenerPalabras = function(){\n    var palabras = [];\n\n    var $contenInputs = $("#list-words-game");\n    $contenInputs.find("input.word-game").each(function() {\n        var valor = $(this).val();\n        $(this).attr("value", valor);\n        palabras.push(valor);\n    });\n\n    return palabras;\n};\n\n$("#list-words-game")\n    .on("keydown", "input.word-game", function(e){\n        if (e.which === 32)\n            return false;\n        if( $(this).parents(".form-group").hasClass("has-error") ) inputsValidos();\n    })\n    .on("change", "input.word-game", function(e){\n        this.value = this.value.replace(/\\s/g, "");\n    })\n    .on("click", "span.form-control-feedback", function(e) {\n        e.preventDefault();\n        $(this).parents(".item-word-game").remove();\n    });;\n\n$(".btn.add-word-game").click(function(e) {\n    e.preventDefault();\n    var id_cloneFrom = $(this).data("clonefrom");\n    var id_cloneTo = $(this).data("cloneto");\n    var now = Date.now();\n    var newWord_html = $(id_cloneFrom).clone(true, true);\n\n    newWord_html.removeClass("hidden");\n    newWord_html.removeAttr("id");\n    newWord_html.find("input.word-game").attr("id", "input-"+now);\n    newWord_html.find("span.form-control-feedback").attr("data-tooltip", "tooltip");\n    $(id_cloneTo).append(newWord_html);\n    \n    $("#input-"+now).focus();\n    $("*[data-tooltip=\\"tooltip\\"]").tooltip();\n});\n\n$(".btn.create-word-search").click(function(e) {\n    e.preventDefault();\n    if( inputsValidos() ){\n        var idPuzzle = "#puzzle";\n        var idWords = "#words";\n        var words = obtenerPalabras();\n        var gamePuzzle = wordfindgame.create(words, idPuzzle, idWords);\n\n        $("#pnl-edit").hide();\n        $("#findword-game").show("fast");\n    } else {\n        $("#list-words-game").find(".has-error").first().find("input.word-game").focus();\n        mostrar_notificacion("Attention","words must have more than one letter", "warning");\n    }\n});\n\n$(".edit-wordfind").click(function(e) {\n    e.preventDefault();\n\n    $("#pnl-edit").show("fast");\n    $("#findword-game").hide();\n\n    $("#puzzle").html("");\n    $("#words").html("");\n});\n\n$(".btn.save-wordfind").click(function(e) {\n    e.preventDefault();\n    msjes = {\n        \'attention\' : \'Attention\',\n        \'guardado_correcto\' : \'Game saved successfully\'\n    }\n    saveGame(msjes);\n});\n\nvar iniciarSopaLetras = function(){\n    var arrWords=[];\n    if( $(\'#words\').html()!=\'\' ){\n        $(\'#words\').find(\'li.word\').each(function() {\n            var word = $(this).text();\n            arrWords.push(word);\n        });\n\n        var gamePuzzle = wordfindgame.create(arrWords, \'#puzzle\', \'#words\');\n    }\n\n    var valRol = $(\'#hRol\').val();\n    if( valRol!=\'\' && valRol!=undefined ){\n        if(valRol != 1){\n            $(\'.plantilla-sopaletras\').find(\'.nopreview\').remove();\n        }\n    }\n};\n\n$(document).ready(function() {\n    iniciarSopaLetras();\n});\n\n</script>', 0, 'G');
INSERT INTO `herramientas` (`idtool`, `idnivel`, `idunidad`, `idactividad`, `idpersonal`, `titulo`, `descripcion`, `texto`, `orden`, `tool`) VALUES
(11, 1, 6, 55, 66666666, 'Spelling names!', 'Find some names in the word puzzle.', '<link rel="stylesheet" href="__xRUTABASEx__/frontend/plantillas/tema1/css/actividad_sopaletras.css">\n<input type="hidden" name="idgui" id="idgui" value="58b61a345aab9">\n<div class="plantilla plantilla-sopaletras" data-idgui="58b61a345aab9">\n    <div class="row nopreview" id="pnl-edit" style="display: none;">\n        <div class="col-xs-12 btns-zone">\n            <a class="btn btn-info add-word-game" data-clonefrom="#to-clone" data-cloneto="#list-words-game">\n                <i class="fa fa-plus-circle"></i> \n                add word            </a>\n            <div class="col-xs-12 col-sm-6 col-md-4 item-word-game hidden" id="to-clone">\n                <div class="form-group has-feedback">\n                    <input type="text" class="form-control word-game" placeholder="write a word" maxlength="18">\n                    <span class="glyphicon glyphicon-trash form-control-feedback" title="remove word"></span>\n                </div>\n            </div>\n        </div>\n\n        <div class="col-xs-12" id="list-words-game">\n            <div class="col-xs-12 col-sm-6 col-md-4 item-word-game">\n                <div class="form-group has-feedback">\n                    <input type="text" class="form-control word-game" placeholder="write a word" maxlength="18" autofocus="" value="Mary">\n                    <span class="glyphicon glyphicon-trash form-control-feedback" data-tooltip="tooltip" title="remove word"></span>\n                </div>\n            </div>\n\n        <div class="col-xs-12 col-sm-6 col-md-4 item-word-game">\n                <div class="form-group has-feedback">\n                    <input type="text" class="form-control word-game" placeholder="write a word" maxlength="18" id="input-1488329304829" value="Michael">\n                    <span class="glyphicon glyphicon-trash form-control-feedback" title="remove word" data-tooltip="tooltip"></span>\n                </div>\n            </div><div class="col-xs-12 col-sm-6 col-md-4 item-word-game">\n                <div class="form-group has-feedback">\n                    <input type="text" class="form-control word-game" placeholder="write a word" maxlength="18" id="input-1488329310709" value="Rosemary">\n                    <span class="glyphicon glyphicon-trash form-control-feedback" title="remove word" data-tooltip="tooltip"></span>\n                </div>\n            </div><div class="col-xs-12 col-sm-6 col-md-4 item-word-game">\n                <div class="form-group has-feedback">\n                    <input type="text" class="form-control word-game" placeholder="write a word" maxlength="18" id="input-1488329319431" value="Chelsea">\n                    <span class="glyphicon glyphicon-trash form-control-feedback" title="remove word" data-tooltip="tooltip"></span>\n                </div>\n            </div><div class="col-xs-12 col-sm-6 col-md-4 item-word-game">\n                <div class="form-group has-feedback">\n                    <input type="text" class="form-control word-game" placeholder="write a word" maxlength="18" id="input-1488329328741" value="Demetria">\n                    <span class="glyphicon glyphicon-trash form-control-feedback" title="remove word" data-tooltip="tooltip"></span>\n                </div>\n            </div><div class="col-xs-12 col-sm-6 col-md-4 item-word-game">\n                <div class="form-group has-feedback">\n                    <input type="text" class="form-control word-game" placeholder="write a word" maxlength="18" id="input-1488329332957" value="Susan">\n                    <span class="glyphicon glyphicon-trash form-control-feedback" title="remove word" data-tooltip="tooltip"></span>\n                </div>\n            </div><div class="col-xs-12 col-sm-6 col-md-4 item-word-game">\n                <div class="form-group has-feedback">\n                    <input type="text" class="form-control word-game" placeholder="write a word" maxlength="18" id="input-1488329337469" value="Jason">\n                    <span class="glyphicon glyphicon-trash form-control-feedback" title="remove word" data-tooltip="tooltip"></span>\n                </div>\n            </div><div class="col-xs-12 col-sm-6 col-md-4 item-word-game">\n                <div class="form-group has-feedback">\n                    <input type="text" class="form-control word-game" placeholder="write a word" maxlength="18" id="input-1488329346454" value="Robert">\n                    <span class="glyphicon glyphicon-trash form-control-feedback" title="remove word" data-tooltip="tooltip"></span>\n                </div>\n            </div><div class="col-xs-12 col-sm-6 col-md-4 item-word-game">\n                <div class="form-group has-feedback">\n                    <input type="text" class="form-control word-game" placeholder="write a word" maxlength="18" id="input-1488329351629" value="Karina">\n                    <span class="glyphicon glyphicon-trash form-control-feedback" title="remove word" data-tooltip="tooltip"></span>\n                </div>\n            </div><div class="col-xs-12 col-sm-6 col-md-4 item-word-game">\n                <div class="form-group has-feedback">\n                    <input type="text" class="form-control word-game" placeholder="write a word" maxlength="18" id="input-1488329363261" value="Maite">\n                    <span class="glyphicon glyphicon-trash form-control-feedback" title="remove word" data-tooltip="tooltip"></span>\n                </div>\n            </div><div class="col-xs-12 col-sm-6 col-md-4 item-word-game">\n                <div class="form-group has-feedback">\n                    <input type="text" class="form-control word-game" placeholder="write a word" maxlength="18" id="input-1488329367446" value="Jay">\n                    <span class="glyphicon glyphicon-trash form-control-feedback" title="remove word" data-tooltip="tooltip"></span>\n                </div>\n            </div><div class="col-xs-12 col-sm-6 col-md-4 item-word-game">\n                <div class="form-group has-feedback">\n                    <input type="text" class="form-control word-game" placeholder="write a word" maxlength="18" id="input-1488329372471" value="Ghia">\n                    <span class="glyphicon glyphicon-trash form-control-feedback" title="remove word" data-tooltip="tooltip"></span>\n                </div>\n            </div></div>\n\n        <div class="col-xs-12 text-right">\n            <a class="btn btn-primary create-word-search">\n                <i class="fa fa-gamepad"></i>\n                create word search            </a>\n        </div>\n    </div>\n\n    <div class="row" id="findword-game">\n        <div class="col-xs-12 pull-right text-right btns-zone nopreview">\n            <a class="btn btn-success edit-wordfind">\n                <i class="fa fa-pencil-square-o"></i>\n                Edit word search            </a>\n            <a class="btn btn-primary save-wordfind">\n                <i class="fa fa-floppy-o"></i> \n                Save word search            </a>\n        </div>\n\n        <div class="col-xs-12" style="text-align: center;">\n            <div id="puzzle"><div><button class="puzzleSquare" x="0" y="0">R</button><button class="puzzleSquare" x="1" y="0">K</button><button class="puzzleSquare" x="2" y="0">a</button><button class="puzzleSquare" x="3" y="0">r</button><button class="puzzleSquare" x="4" y="0">i</button><button class="puzzleSquare" x="5" y="0">n</button><button class="puzzleSquare" x="6" y="0">a</button><button class="puzzleSquare" x="7" y="0">v</button><button class="puzzleSquare" x="8" y="0">G</button></div><div><button class="puzzleSquare" x="0" y="1">h</button><button class="puzzleSquare" x="1" y="1">o</button><button class="puzzleSquare" x="2" y="1">v</button><button class="puzzleSquare" x="3" y="1">y</button><button class="puzzleSquare" x="4" y="1">n</button><button class="puzzleSquare" x="5" y="1">o</button><button class="puzzleSquare" x="6" y="1">k</button><button class="puzzleSquare" x="7" y="1">h</button><button class="puzzleSquare" x="8" y="1">y</button></div><div><button class="puzzleSquare" x="0" y="2">u</button><button class="puzzleSquare" x="1" y="2">u</button><button class="puzzleSquare" x="2" y="2">b</button><button class="puzzleSquare" x="3" y="2">a</button><button class="puzzleSquare" x="4" y="2">r</button><button class="puzzleSquare" x="5" y="2">s</button><button class="puzzleSquare" x="6" y="2">i</button><button class="puzzleSquare" x="7" y="2">f</button><button class="puzzleSquare" x="8" y="2">a</button></div><div><button class="puzzleSquare" x="0" y="3">R</button><button class="puzzleSquare" x="1" y="3">o</button><button class="puzzleSquare" x="2" y="3">s</button><button class="puzzleSquare" x="3" y="3">e</button><button class="puzzleSquare" x="4" y="3">m</button><button class="puzzleSquare" x="5" y="3">a</button><button class="puzzleSquare" x="6" y="3">r</button><button class="puzzleSquare" x="7" y="3">y</button><button class="puzzleSquare" x="8" y="3">J</button></div><div><button class="puzzleSquare" x="0" y="4">s</button><button class="puzzleSquare" x="1" y="4">u</button><button class="puzzleSquare" x="2" y="4">e</button><button class="puzzleSquare" x="3" y="4">s</button><button class="puzzleSquare" x="4" y="4">r</button><button class="puzzleSquare" x="5" y="4">J</button><button class="puzzleSquare" x="6" y="4">M</button><button class="puzzleSquare" x="7" y="4">w</button><button class="puzzleSquare" x="8" y="4">i</button></div><div><button class="puzzleSquare" x="0" y="5">S</button><button class="puzzleSquare" x="1" y="5">l</button><button class="puzzleSquare" x="2" y="5">s</button><button class="puzzleSquare" x="3" y="5">l</button><button class="puzzleSquare" x="4" y="5">e</button><button class="puzzleSquare" x="5" y="5">t</button><button class="puzzleSquare" x="6" y="5">i</button><button class="puzzleSquare" x="7" y="5">a</button><button class="puzzleSquare" x="8" y="5">M</button></div><div><button class="puzzleSquare" x="0" y="6">D</button><button class="puzzleSquare" x="1" y="6">e</button><button class="puzzleSquare" x="2" y="6">m</button><button class="puzzleSquare" x="3" y="6">e</button><button class="puzzleSquare" x="4" y="6">t</button><button class="puzzleSquare" x="5" y="6">r</button><button class="puzzleSquare" x="6" y="6">i</button><button class="puzzleSquare" x="7" y="6">a</button><button class="puzzleSquare" x="8" y="6">h</button></div><div><button class="puzzleSquare" x="0" y="7">M</button><button class="puzzleSquare" x="1" y="7">i</button><button class="puzzleSquare" x="2" y="7">c</button><button class="puzzleSquare" x="3" y="7">h</button><button class="puzzleSquare" x="4" y="7">a</button><button class="puzzleSquare" x="5" y="7">e</button><button class="puzzleSquare" x="6" y="7">l</button><button class="puzzleSquare" x="7" y="7">g</button><button class="puzzleSquare" x="8" y="7">s</button></div><div><button class="puzzleSquare" x="0" y="8">k</button><button class="puzzleSquare" x="1" y="8">o</button><button class="puzzleSquare" x="2" y="8">v</button><button class="puzzleSquare" x="3" y="8">C</button><button class="puzzleSquare" x="4" y="8">f</button><button class="puzzleSquare" x="5" y="8">d</button><button class="puzzleSquare" x="6" y="8">c</button><button class="puzzleSquare" x="7" y="8">e</button><button class="puzzleSquare" x="8" y="8">m</button></div></div>\n        </div>\n        <div class="col-xs-12">\n            <div id="words"><ul><li class="word Chelsea">Chelsea</li><li class="word Demetria">Demetria</li><li class="word Ghia">Ghia</li><li class="word Jason">Jason</li><li class="word Jay">Jay</li><li class="word Karina">Karina</li><li class="word Maite">Maite</li><li class="word Mary">Mary</li><li class="word Michael">Michael</li><li class="word Robert">Robert</li><li class="word Rosemary">Rosemary</li><li class="word Susan">Susan</li></ul></div>\n        </div>\n    </div>\n</div>\n\n<script>\n\n$("*[data-tooltip=\\"tooltip\\"]").tooltip();\n\nvar inputsValidos = function(){\n    var rspta = true;\n    var $contenInputs = $("#list-words-game");\n    $contenInputs.find("input.word-game").each(function() {\n        var valor = $(this).val();\n        if(valor.length<=1){\n            $(this).parents(".form-group").addClass("has-error");\n        } else {\n            $(this).parents(".form-group").removeClass("has-error");\n        }\n    });\n    $contenInputs.find("input.word-game").each(function() {\n        var valor = $(this).val();\n        if(valor.length<=1){\n            rspta = false;\n            return false;\n        }\n    });\n    return rspta;\n};\n\nvar obtenerPalabras = function(){\n    var palabras = [];\n\n    var $contenInputs = $("#list-words-game");\n    $contenInputs.find("input.word-game").each(function() {\n        var valor = $(this).val();\n        $(this).attr("value", valor);\n        palabras.push(valor);\n    });\n\n    return palabras;\n};\n\n$("#list-words-game")\n    .on("keydown", "input.word-game", function(e){\n        if (e.which === 32)\n            return false;\n        if( $(this).parents(".form-group").hasClass("has-error") ) inputsValidos();\n    })\n    .on("change", "input.word-game", function(e){\n        this.value = this.value.replace(/\\s/g, "");\n    })\n    .on("click", "span.form-control-feedback", function(e) {\n        e.preventDefault();\n        $(this).parents(".item-word-game").remove();\n    });;\n\n$(".btn.add-word-game").click(function(e) {\n    e.preventDefault();\n    var id_cloneFrom = $(this).data("clonefrom");\n    var id_cloneTo = $(this).data("cloneto");\n    var now = Date.now();\n    var newWord_html = $(id_cloneFrom).clone(true, true);\n\n    newWord_html.removeClass("hidden");\n    newWord_html.removeAttr("id");\n    newWord_html.find("input.word-game").attr("id", "input-"+now);\n    newWord_html.find("span.form-control-feedback").attr("data-tooltip", "tooltip");\n    $(id_cloneTo).append(newWord_html);\n    \n    $("#input-"+now).focus();\n    $("*[data-tooltip=\\"tooltip\\"]").tooltip();\n});\n\n$(".btn.create-word-search").click(function(e) {\n    e.preventDefault();\n    if( inputsValidos() ){\n        var idPuzzle = "#puzzle";\n        var idWords = "#words";\n        var words = obtenerPalabras();\n        var gamePuzzle = wordfindgame.create(words, idPuzzle, idWords);\n\n        $("#pnl-edit").hide();\n        $("#findword-game").show("fast");\n    } else {\n        $("#list-words-game").find(".has-error").first().find("input.word-game").focus();\n        mostrar_notificacion("Attention","words must have more than one letter", "warning");\n    }\n});\n\n$(".edit-wordfind").click(function(e) {\n    e.preventDefault();\n\n    $("#pnl-edit").show("fast");\n    $("#findword-game").hide();\n\n    $("#puzzle").html("");\n    $("#words").html("");\n});\n\n$(".btn.save-wordfind").click(function(e) {\n    e.preventDefault();\n    msjes = {\n        \'attention\' : \'Attention\',\n        \'guardado_correcto\' : \'Game saved successfully\'\n    }\n    saveGame(msjes);\n});\n\nvar iniciarSopaLetras = function(){\n    var arrWords=[];\n    if( $(\'#words\').html()!=\'\' ){\n        $(\'#words\').find(\'li.word\').each(function() {\n            var word = $(this).text();\n            arrWords.push(word);\n        });\n\n        var gamePuzzle = wordfindgame.create(arrWords, \'#puzzle\', \'#words\');\n    }\n\n    var valRol = $(\'#hRol\').val();\n    if( valRol!=\'\' && valRol!=undefined ){\n        if(valRol != 1){\n            $(\'.plantilla-sopaletras\').find(\'.nopreview\').remove();\n        }\n    }\n};\n\n$(document).ready(function() {\n    iniciarSopaLetras();\n});\n\n</script>', 0, 'G'),
(12, 1, 6, 56, 66666666, 'Tell me about you!', 'click here to add description', '<link rel="stylesheet" href="__xRUTABASEx__/frontend/plantillas/tema1/css/actividad_rompecabezas.css">\n<input type="hidden" name="idgui" id="idgui" value="58b61b2a77e1d">\n<div class="plantilla plantilla-rompecabezas" data-idgui="58b61b2a77e1d">\n    <div class="row botones-puzzle nopreview">\n        <div class="col-xs-12 col-sm-10 pull-right text-right">\n            <a class="btn btn-success edit-puzzle">\n                <i class="fa fa-pencil-square-o"></i> \n                edit puzzle            </a>\n            <a class="btn btn-primary save-puzzle">\n                <i class="fa fa-floppy-o"></i> \n                save puzzle            </a>\n        </div>\n    </div>\n    <br>\n    <div class="row">\n        <div class="col-xs-10 video-loader nopreview" style="display: none;">\n            <div class="btnselectedfile btn btn-primary nopreview" data-tipo="image" data-url=".img_158b61b2a77e1d">\n                <i class="fa fa-image"></i> \n                Select image            </div>\n            <div class="btnselectedfile btn btn-orange nopreview" data-tipo="audio" data-url=".audioimg_158b61b2a77e1d">\n                <i class="fa fa-music"></i> \n                Select audio\n            </div>\n        </div>\n        <div class="col-xs-12 puzzle-started" id="puzzle-containment">              \n            <div class="col-xs-6 text-center">\n                <span class="snappuzzle-wrap"><img class="img_158b61b2a77e1d img-responsive valvideo" src="__xRUTABASEx__/static/media/image/N1-U1-A1-20170222105540.jpg" style="display: inline-block;" data-nombre-file="140221edujobinterview-stock.jpg" data-width="376" data-height="250"><div class="snappuzzle-slot sp_1488329715165 ui-droppable" style="width: 125.333px; height: 83.3333px; left: 0px; top: 0px;"></div><div class="snappuzzle-slot sp_1488329715165 ui-droppable" style="width: 125.333px; height: 83.3333px; left: 125.333px; top: 0px;"></div><div class="snappuzzle-slot sp_1488329715165 ui-droppable" style="width: 125.333px; height: 83.3333px; left: 250.667px; top: 0px;"></div><div class="snappuzzle-slot sp_1488329715165 ui-droppable" style="width: 125.333px; height: 83.3333px; left: 0px; top: 83.3333px;"></div><div class="snappuzzle-slot sp_1488329715165 ui-droppable" style="width: 125.333px; height: 83.3333px; left: 125.333px; top: 83.3333px;"></div><div class="snappuzzle-slot sp_1488329715165 ui-droppable" style="width: 125.333px; height: 83.3333px; left: 250.667px; top: 83.3333px;"></div><div class="snappuzzle-slot sp_1488329715165 ui-droppable" style="width: 125.333px; height: 83.3333px; left: 0px; top: 166.667px;"></div><div class="snappuzzle-slot sp_1488329715165 ui-droppable" style="width: 125.333px; height: 83.3333px; left: 125.333px; top: 166.667px;"></div><div class="snappuzzle-slot sp_1488329715165 ui-droppable" style="width: 125.333px; height: 83.3333px; left: 250.667px; top: 166.667px;"></div></span>\n                <img src="__xRUTABASEx__/static/media/web/noimage.png" class="img-responsive img-thumbnail embed-responsive-item" style="display: none;">\n            </div>\n            <div class="col-xs-6">\n                <div id="pile" style="height: 250px;" class="snappuzzle-pile"><div class="snappuzzle-piece sp_1488329715165 ui-draggable ui-draggable-handle" style="width: 125.333px; height: 83.3333px; position: absolute; left: 20px; top: 81px; z-index: 9; background-image: url(&quot;__xRUTABASEx__/static/media/image/N1-U1-A1-20170222105540.jpg&quot;); background-position: 0px 0px; background-size: 376px;"></div><div class="snappuzzle-piece sp_1488329715165 ui-draggable ui-draggable-handle" style="width: 125.333px; height: 83.3333px; position: absolute; left: 110px; top: 55px; z-index: 6; background-image: url(&quot;__xRUTABASEx__/static/media/image/N1-U1-A1-20170222105540.jpg&quot;); background-position: -125.333px 0px; background-size: 376px;"></div><div class="snappuzzle-piece sp_1488329715165 ui-draggable ui-draggable-handle" style="width: 125.333px; height: 83.3333px; position: absolute; left: 55px; top: 64px; z-index: 3; background-image: url(&quot;__xRUTABASEx__/static/media/image/N1-U1-A1-20170222105540.jpg&quot;); background-position: -250.667px 0px; background-size: 376px;"></div><div class="snappuzzle-piece sp_1488329715165 ui-draggable ui-draggable-handle" style="width: 125.333px; height: 83.3333px; position: absolute; left: 61px; top: 41px; z-index: 3; background-image: url(&quot;__xRUTABASEx__/static/media/image/N1-U1-A1-20170222105540.jpg&quot;); background-position: 0px -83.3333px; background-size: 376px;"></div><div class="snappuzzle-piece sp_1488329715165 ui-draggable ui-draggable-handle" style="width: 125.333px; height: 83.3333px; position: absolute; left: 75px; top: 43px; z-index: 8; background-image: url(&quot;__xRUTABASEx__/static/media/image/N1-U1-A1-20170222105540.jpg&quot;); background-position: -125.333px -83.3333px; background-size: 376px;"></div><div class="snappuzzle-piece sp_1488329715165 ui-draggable ui-draggable-handle" style="width: 125.333px; height: 83.3333px; position: absolute; left: 109px; top: 80px; z-index: 5; background-image: url(&quot;__xRUTABASEx__/static/media/image/N1-U1-A1-20170222105540.jpg&quot;); background-position: -250.667px -83.3333px; background-size: 376px;"></div><div class="snappuzzle-piece sp_1488329715165 ui-draggable ui-draggable-handle" style="width: 125.333px; height: 83.3333px; position: absolute; left: 67px; top: 25px; z-index: 4; background-image: url(&quot;__xRUTABASEx__/static/media/image/N1-U1-A1-20170222105540.jpg&quot;); background-position: 0px -166.667px; background-size: 376px;"></div><div class="snappuzzle-piece sp_1488329715165 ui-draggable ui-draggable-handle" style="width: 125.333px; height: 83.3333px; position: absolute; left: 3px; top: 39px; z-index: 2; background-image: url(&quot;__xRUTABASEx__/static/media/image/N1-U1-A1-20170222105540.jpg&quot;); background-position: -125.333px -166.667px; background-size: 376px;"></div><div class="snappuzzle-piece sp_1488329715165 ui-draggable ui-draggable-handle" style="width: 125.333px; height: 83.3333px; position: absolute; left: 39px; top: 14px; z-index: 8; background-image: url(&quot;__xRUTABASEx__/static/media/image/N1-U1-A1-20170222105540.jpg&quot;); background-position: -250.667px -166.667px; background-size: 376px;"></div></div>\n            </div>\n            <div id="panel_finish" class="col-xs-6" style="display: none;">\n                <h2 class="palabra-respuesta">Personal information</h2>\n                <a href="#" class="btn btn-default start-puzzle" data-grid="3"><i class="fa fa-undo"></i> restart</a>\n            </div>\n        </div>\n    </div>\n    <br>\n    <div class="row rspta-nivel-contenedor nopreview" style="display: none;">\n        <div class="col-xs-12 col-sm-6">\n            <div class="form-group palabra-rspta">\n                <label>what is in the image?</label>\n                <input type="text" name="txtRespuesta" id="txtRespuesta" class="form-control" value="Personal information">\n                <span class="glyphicon glyphicon-warning-sign form-control-feedback hidden"></span>\n            </div>\n        </div>\n        <div class="col-xs-12 col-sm-6 nivel-dificultad">\n            <label>choose difficulty: </label>\n            <div class="btn-group btn-group-justified" role="group">\n                <div class="btn-group" role="group">\n                    <a class="btn btn-default start-puzzle" data-grid="3"><i class="fa fa-star"></i></a>\n                </div>\n                <div class="btn-group" role="group">\n                    <a class="btn btn-default start-puzzle" data-grid="4"><i class="fa fa-star"></i><i class="fa fa-star"></i></a>\n                </div>\n                <div class="btn-group" role="group">\n                    <a class="btn btn-default start-puzzle" data-grid="6"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></a>\n                </div>\n                <div class="btn-group" role="group">\n                    <a class="btn btn-default start-puzzle" data-grid="10"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></a>\n                </div>\n            </div>\n        </div>\n\n    </div>\n    <audio src="" controls="true" class="audioimg_158b61b2a77e1d hidden" style="display: none !important;"></audio>\n</div>\n<script type="text/javascript" src="__xRUTABASEx__/static/tema/js/jquery-ui.min.js"></script>\n<script type="text/javascript">\n\n$(\'.plantilla-rompecabezas .btnselectedfile\').click(function(e){     \n    var txt="Selected or upload ";\n    selectedfile(e,this,txt);\n});\n\n$(\'.img_158b61b2a77e1d\').load(function() {\n    var src = $(this).attr(\'src\');\n    if(src!=\'\'){\n        $(\'.palabra-rspta input[disabled="disabled"]\').removeAttr(\'disabled\');\n        $(\'.palabra-rspta input\').focus();\n        $(\'.nivel-dificultad a.disabled\').removeClass(\'disabled\');\n    }\n});\n\nvar reproducirAudio = function(){\n    var $audio = $(\'.audioimg_158b61b2a77e1d\');\n    var data_audio = $audio.data(\'audio\');\n    if(data_audio!=\'\' && data_audio!=undefined){\n        var src_audio = $audio.attr(\'src\', \'__xRUTABASEx__/static/media/audio/\'+data_audio );\n        $audio.trigger(\'play\');\n    }\n};\n\nvar start_puzzle = function(puzzle_container, pnl_finish, img_src, id_pila, grid){\n    $(pnl_finish).hide();\n    $(puzzle_container).addClass(\'puzzle-started\');\n\n    $(img_src).snapPuzzle({\n        rows: grid, columns: grid,\n        pile: id_pila,\n        containment: puzzle_container,\n        onComplete: function(){\n            $(img_src).fadeOut(150).fadeIn();\n            $(pnl_finish).show();\n            $(id_pila).hide();\n            reproducirAudio();\n        }\n    });\n    $(id_pila).height( $(img_src).height() );\n};\n\nvar actualizarPanelFinish = function(id_pnlFinish, cant_grid){\n    var $pnlFinish = $(id_pnlFinish);\n    var rspta = $(\'#txtRespuesta\').val();\n    $pnlFinish.find(\'.palabra-respuesta\').text(rspta);\n    $pnlFinish.find(\'a.start-puzzle\').attr(\'data-grid\', cant_grid);\n};\n\nvar validarInput = function(){\n    var $input = $(\'input#txtRespuesta\');\n    if($input.length>0){ /* en vista de edicion (Admin) */\n        if($input.val()!=\'\' && $input.val()!=undefined ){\n            if( $input.parent().hasClass(\'has-warning\') ){\n                $input.parent().removeClass(\'has-warning has-feedback\');\n                $input.siblings(\'span.glyphicon\').addClass(\'hidden\');\n            }\n            $input.attr(\'value\', $input.val());\n            var rspta = true;\n        } else {\n            $input.parent().addClass(\'has-warning has-feedback\');\n            $input.siblings(\'span.glyphicon\').removeClass(\'hidden\');\n            $input.focus();\n            var rspta = false;\n        }\n    } else { /* en vista de (Alumno) */\n        var rspta = true;\n    }\n    return rspta;\n};\n\n$(\'.start-puzzle\').click(function(e) {\n    e.preventDefault();\n    if( validarInput() ){\n        var container    = \'#puzzle-containment\';\n        var id_img       = \'.img_158b61b2a77e1d\';\n        var id_pnlFinish = \'#panel_finish\';\n        var id_pila      = \'#pile\';\n        var cant_grid    = $(this).data(\'grid\');\n\n        if( $(container).hasClass(\'puzzle-started\') ){\n            $(id_img).snapPuzzle(\'destroy\');\n            $(container).removeClass(\'puzzle-started\');\n        }\n        \n        $(id_img).attr(\'data-width\', $(id_img).width());\n        $(id_img).attr(\'data-height\', $(id_img).height());\n\n        start_puzzle(container, id_pnlFinish, id_img, id_pila, cant_grid);\n\n        $(\'.plantilla-rompecabezas .botones-puzzle\').show(\'fast\');\n        $(\'.plantilla-rompecabezas .video-loader\').hide(\'fast\');\n        $(id_pila).show();\n        $(this).parents(\'.rspta-nivel-contenedor\').hide();\n        actualizarPanelFinish(id_pnlFinish, cant_grid);\n\n    }\n});\n\n$(\'.btn.edit-puzzle\').click(function(e) {\n    e.preventDefault();\n    var container    = \'#puzzle-containment\';\n    var id_img       = \'.img_158b61b2a77e1d\';\n    var id_pila      = \'#pile\';\n    var id_pnlFinish = \'#panel_finish\';\n\n    if( $(container).hasClass(\'puzzle-started\') ){\n        $(id_img).snapPuzzle(\'destroy\');\n        $(container).removeClass(\'puzzle-started\');\n\n        $(\'.plantilla-rompecabezas .botones-puzzle\').hide(\'fast\');\n        $(\'.plantilla-rompecabezas .video-loader\').show(\'fast\');\n        $(id_pila).hide();\n        $(\'.rspta-nivel-contenedor\').show(\'fast\');\n        $(id_pnlFinish).hide();\n    }\n});\n\n$(\'#txtRespuesta\').keyup(function(e) {\n    validarInput();\n});\n\n$(window).resize(function(){\n    var container    = \'#puzzle-containment\';\n    var id_img       = \'.img_158b61b2a77e1d\';\n    var id_pila      = \'#pile\';\n\n    if( $(container).hasClass(\'puzzle-started\') ){\n        $(id_pila).height($(id_img).height());\n        $(id_img).snapPuzzle(\'refresh\');\n    }\n});\n\n$(\'h2.palabra-respuesta\').click(function(e) {\n    reproducirAudio();\n});\n\n$(\'.btn.save-puzzle\').click(function(e) {\n    e.preventDefault();\n    msjes = {\n        \'attention\' : \'Attention\',\n        \'guardado_correcto\' : \'Game saved successfully\'\n    };\n    saveGame(msjes);\n});\n\nvar iniciarRompecabezas = function(){\n    var img_src = $(\'.img_158b61b2a77e1d\').attr(\'src\');\n    if(img_src.length>0){\n        var container    = \'#puzzle-containment\';\n        var img          = \'.img_158b61b2a77e1d\';\n        var id_pnlFinish = \'#panel_finish\';\n        var id_pila      = \'#pile\';\n        var cant_grid    = $(id_pnlFinish).find(\'a.start-puzzle\').data(\'grid\');\n\n        $(id_pila).html(\'\');\n        $(id_pila).removeClass(\'snappuzzle-pile\');\n\n        $(img).siblings(\'.snappuzzle-slot\').remove();\n        $(img).unwrap();\n\n        $(\'#puzzle-containment\').removeClass(\'puzzle-started\');\n\n        //$(\'#panel_finish a.btn.start-puzzle\').trigger(\'click\');\n        $(id_pila).height($(img).data(\'height\'));\n        start_puzzle( container, id_pnlFinish, img, id_pila, cant_grid )\n\n    }\n\n    var valRol = $(\'#hRol\').val();\n    if( valRol!=\'\' && valRol!=undefined ){\n        if(valRol != 1){\n            $(\'.plantilla-rompecabezas\').find(\'.nopreview\').remove();\n        }\n    }\n\n};\n\n$(document).ready(function() {\n    iniciarRompecabezas();\n});\n\n</script>', 0, 'G'),
(13, 1, 6, 18, 99999999, 'Greetings', '', '<input type="hidden" name="idgui" id="idgui" value="58b8247b9fe64">\n<input type="hidden" name="orden[1][1]" value="1">\n<input type="hidden" name="det_tipo[1][1]" value="V">\n<input type="hidden" name="tipo_desarrollo[1][1]" value="D">\n<input type="hidden" name="tipo_actividad[1][1]" value="A">\n<div class="plantilla plantilla-crucigrama" data-idgui="58b8247b9fe64">\n  <div class="row">\n    <div class="col-xs-12 pull-right text-right btns-zone nopreview" style="z-index: 9;">\n        <a class="btn btn-primary save-crossword">\n            <i class="fa fa-floppy-o"></i> \n            save crossword        </a>\n    </div>\n    <div class="col-md-12">\n      <div id="crossword58b8247b9fe64"><table class="crossword">\n<tbody><tr>\n<td class="no-border">\n&nbsp;\n</td><td class="" style="background:url(\'__xRUTABASEx__/static/libs/crusigrama/numbers/1.png\') no-repeat">\n<input data-valn="1" value="" data-row="0" data-col="1" data-val="b" class="tvalcrusigrama" style=" ">\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td></tr>\n<tr>\n<td class="no-border">\n&nbsp;\n</td><td class="">\n<input data-valn="1" value="" data-row="1" data-col="1" data-val="y" class="tvalcrusigrama" style=" ">\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="" style="background:url(\'__xRUTABASEx__/static/libs/crusigrama/numbers/2.png\') no-repeat">\n<input data-valn="2" value="" data-row="1" data-col="4" data-val="G" class="tvalcrusigrama" style=" ">\n</td></tr>\n<tr>\n<td class="" style="background:url(\'__xRUTABASEx__/static/libs/crusigrama/numbers/3.png\') no-repeat">\n<input data-valn="3" value="" data-row="2" data-col="0" data-val="H" class="tvalcrusigrama" style=" ">\n</td><td class="">\n<input data-valn="3" value="" data-row="2" data-col="1" data-val="e" class="tvalcrusigrama" style=" ">\n</td><td class="">\n<input data-valn="3" value="" data-row="2" data-col="2" data-val="l" class="tvalcrusigrama" style=" ">\n</td><td class="">\n<input data-valn="3" value="" data-row="2" data-col="3" data-val="l" class="tvalcrusigrama" style=" ">\n</td><td class="">\n<input data-valn="3" value="" data-row="2" data-col="4" data-val="o" class="tvalcrusigrama" style=" ">\n</td></tr>\n<tr>\n<td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="">\n<input data-valn="3" value="" data-row="3" data-col="4" data-val="o" class="tvalcrusigrama" style=" ">\n</td></tr>\n<tr>\n<td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="">\n<input data-valn="3" value="" data-row="4" data-col="4" data-val="d" class="tvalcrusigrama" style=" ">\n</td></tr>\n</tbody></table></div>\n      <div class="col-xs-12 col-sm-12 col-md-12 ">\n              <table class="nopreview table table-striped table-responsive" id="alternacru58b8247b9fe64">               \n                <tbody>\n                  <tr>\n                    <td><div class="addtextvideo txtquestion58b8247b9fe64" data-initial_val="Hello">Hello</div></td>\n                    <td class="addtextvideo nopreview txtresponse58b8247b9fe64" data-initial_val="Hello">Hello</td>\n                    <td class="nopreview"><span class="removedialog tooltip" title="" data-original-title="Remove question"><i class="fa fa-trash"></i> </span></td>\n                  </tr>\n                  <tr>\n                    <td><div class="addtextvideo txtquestion58b8247b9fe64" data-initial_val="hi">hi</div></td>\n                    <td class="addtextvideo nopreview txtresponse58b8247b9fe64" data-initial_val="Good">Good</td>\n                    <td class="nopreview  "><span class="removedialog tooltip" title="" data-original-title="Remove question"><i class="fa fa-trash"></i> </span></td>\n                  </tr>\n                  <tr class="hidden" id="addpregunta58b8247b9fe64">\n                    <td><div class="addtextvideo txtquestion58b8247b9fe64">Add Question or reference</div></td>\n                    <td class="addtextvideo nopreview txtresponse58b8247b9fe64">Response</td>\n                    <td class="nopreview"><span class="removedialog tooltip" title="" data-original-title="Remove question"><i class="fa fa-trash"></i> </span></td>\n                  </tr>                                \n                <tr>\n                    <td><div class="addtextvideo txtquestion58b8247b9fe64" data-initial_val="bye">bye</div></td>\n                    <td class="addtextvideo nopreview txtresponse58b8247b9fe64" data-initial_val="bye">bye</td>\n                    <td class="nopreview"><span class="removedialog tooltip" title="" data-original-title="Remove question"><i class="fa fa-trash"></i> </span></td>\n                  </tr></tbody>              \n              </table>\n              <div class="text-center nopreview">\n                <span class="btn btn-xs btn-primary addclone58b8247b9fe64 nopreview tooltip" data-source="#addpregunta58b8247b9fe64" data-donde="#alternacru58b8247b9fe64" title="" data-original-title="Add more Question or reference">\n                  <i class="fa fa-plus-square"></i>\n                </span> \n                <span class="btn btn-xs btn-primary btngeneratecrusigrama nopreview tooltip" data-donde="#txtdialogocole" title="" data-original-title="Generate crossword">\n                  <i class="fa fa-gamepad"></i>\n                </span>\n              </div>\n              <table class="table table-responsive" id="tbopcion58b8247b9fe64">\n                  <thead>\n                      <tr>\n                          <th class="text-center">Horizontal</th>\n                          <th class="text-center">Vertical</th>\n                      </tr>\n                  </thead>\n                  <tbody>\n                      <tr>\n                          <td><ul id="across58b8247b9fe64"><li class="crusigramalista" data-valn="3" data-orient="row" data-val="HELLO"><strong>3.</strong> Hello</li></ul></td>\n                          <td><ul id="down58b8247b9fe64"><li class="crusigramalista" data-valn="1" data-orient="col" data-val="BYE"><strong>1.</strong> bye</li>\n<li class="crusigramalista" data-valn="2" data-orient="col" data-val="GOOD"><strong>2.</strong> hi</li></ul></td>\n                      </tr>\n                  </tbody>\n              </table>\n            </div>      \n    </div>\n  </div>\n</div> \n\n<script type="text/javascript">\n$(\'.tooltip\').tooltip();\n$(document).ready(function(){\n  $(\'#alternacru58b8247b9fe64\').on(\'click\',\'.removedialog\',function(){\n    $(this).parent().parent(\'tr\').remove();\n  });\n\n  $(\'.addclone58b8247b9fe64\').click(function(){\n    var de=$(this).attr(\'data-source\');\n    var adonde=$(this).attr(\'data-donde\');\n    var clon=$(de).clone();\n    $(adonde).append(\'<tr>\'+clon.html()+\'</tr>\');\n    $(\'.tooltip\').tooltip();  \n  });\n\n  $(\'.btngeneratecrusigrama\').click(function(){\n      var question=$(\'.txtquestion58b8247b9fe64\');\n      var response=$(\'.txtresponse58b8247b9fe64\');\n      var datosp=[], datosf=[];\n      response.each(function(){\n        if(!$(this).parent().hasClass(\'hidden\')){\n          txt=$(this).text();\n          datosp.push(txt.trim());\n        }\n      });\n      question.each(function(){\n       if(!$(this).parent().parent().hasClass(\'hidden\')){\n          txt=$(this).text();\n          datosf.push(txt.trim());\n        }\n      });      \n      var datos=[{palabras:datosp,frase:datosf}];\n      $(\'#tbopcion58b8247b9fe64\').removeClass(\'hidden\');\n      cargarcrucigrama(\'58b8247b9fe64\',datos);\n      $(\'.btns-zone\').show(\'fast\');\n  });\n\n  $(\'#alternacru58b8247b9fe64\')\n      .on(\'click\',\'.addtextvideo\', function(e){\n       // if( $(\'.preview\').is(\':visible\') ){ //si btn.preview es Visible:estamos en edicion\n          var in_=$(this);\n          var texto_inicial = $(this).text().trim();\n          $(this).attr(\'data-initial_val\', texto_inicial);\n          in_.html(\'<input type="text" required="required" class="form-control" id="inputEditTexto" value="\'+ texto_inicial +\'">\');\n          if($(this).hasClass("addtime")){\n            $(\'input#inputEditTexto\',in_).addClass(\'addtime\');\n          }\n          $(\'input#inputEditTexto\',in_).focus().select();\n        //}\n      })\n      .on(\'blur\',\'input#inputEditTexto\',function(){\n        var dataesc=$(this).attr(\'data-esc\');\n        var vini=$(this).parent().attr(\'data-initial_val\');\n        var vin=$(this).val();\n        if(dataesc) _intxt=vini;\n        else _intxt=vin;\n        $(this).removeAttr(\'data-esc\');\n        $(this).parent().attr(\'data-initial_val\',_intxt); \n        if($(this).hasClass("addtime")){   \n          var time=_intxt.split(":");\n          var h=0,m=0,s=0;\n          if(time.length==3){h=parseInt(time[0]);m=parseInt(time[1]);s=parseInt(time[2])}\n          if(time.length==2){m=parseInt(time[0]);s=parseInt(time[1])}\n          if(time.length==1){s=parseInt(time[0])}\n          s=(h*3600)+(m*60)+s;\n          var parent = $(this).parent().parent().parent().parent().attr(\'data-time\',s);\n        }\n        $(this).parent().text(_intxt);\n      })\n      .on(\'keypress\',\'input#inputEditTexto\', function(e){      \n        if(e.which == 13){ //Enter pressed\n          e.preventDefault();          \n          $(this).trigger(\'blur\');\n        } \n      }).on(\'keyup\',\'input#inputEditTexto\', function(e){      \n        if(e.which == 27){ //Enter esc\n          $(this).attr(\'data-esc\',"1");\n          $(this).trigger(\'blur\');\n        }\n      });\n\n  $(\'#crossword58b8247b9fe64\').on(\'keydown\',\'input.tvalcrusigrama\',function(ev){\n    if($(this).hasClass(\'ok\')) return false;\n    $(this).val(\'\');\n  });\n\n  $(\'#crossword58b8247b9fe64\').on(\'keyup\',\'input.tvalcrusigrama\',function(){\n    val =$(this).data(\'val\');\n    val2=$(this).val();\n    var tvalrow=[\'--\'];\n    $(\'#across58b8247b9fe64\').find(\'li[data-orient="row"]\').each(function(){\n      var rval=$(this).data(\'val\');\n      tvalrow.push(rval.toUpperCase());\n    });\n    var tvalcol=new Array();\n    $(\'#down58b8247b9fe64\').find(\'li[data-orient="col"]\').each(function(){\n      var rval=$(this).data(\'val\');\n      tvalcol.push(rval.toUpperCase());\n    });\n    if(val.trim().toUpperCase()==val2.trim().toUpperCase()){\n      $(this).addClass(\'ok\').removeClass(\'error\').attr(\'readonly\',\'true\');\n      var r=$(this).data(\'row\');\n      var c=$(this).data(\'col\');\n      var todosrow=$(\'#crossword58b8247b9fe64\').find(\'input.tvalcrusigrama.ok[data-row="\'+r+\'"]\');\n      var palabra=new Array();\n      if(todosrow.length>1){\n        $.each(todosrow,function(){\n          var c=$(this).data(\'col\');\n          var v_=$(this).data(\'val\');\n          palabra[c]=v_;\n        });\n        txt=palabra.join("").toUpperCase();\n        if(tvalrow.indexOf(txt)>-1){\n          $(\'#across58b8247b9fe64\').find(\'li[data-val="\'+txt+\'"]\').css({color:\'green\',\'text-decoration\':\'line-through\'});\n        }\n      }\n      //var palabra=[];\n      var todoscol=$(\'#crossword58b8247b9fe64\').find(\'input.tvalcrusigrama.ok[data-col="\'+c+\'"]\');\n      var palabra=new Array();\n      if(todoscol.length>1){\n        $.each(todoscol,function(){         \n          var c=$(this).data(\'row\');\n          var v_=$(this).data(\'val\');\n          palabra[c]=v_;\n        });\n        txt=palabra.join("").toUpperCase();\n        if(tvalcol.indexOf(txt)>-1){\n          $(\'#down58b8247b9fe64\').find(\'li[data-val="\'+txt+\'"]\').css({color:\'green\',\'text-decoration\':\'line-through\'});\n        }\n      }      \n    }else\n      $(this).addClass(\'error\').removeClass(\'ok\');\n      \n  });\n\n  $(\'.save-crossword\').click(function(e) {\n    e.preventDefault();\n    msjes = {\n        \'attention\' : \'Attention\',\n        \'guardado_correcto\' : \'Game saved successfully\'\n    };\n    var data={\n      idgame:$(\'#idgame\').val(),\n      nivel:\'1\',\n      unidad:\'6\',\n      actividad:\'18\',\n      texto:$(\'.games-main\').html(),\n      titulo:$(\'.titulo-game\').val(),\n      descripcion:$(\'.descripcion-game\').val()\n    }\n    var idgame=saveGame(data,msjes);\n    $(\'#idgame\').val(idgame);\n  });\n\n  var iniciarCrucigrama = function(){\n    var valRol = $(\'#hRol\').val();\n    if( valRol!=\'\' && valRol!=undefined ){\n      if(valRol != 1){\n        $(\'.plantilla-crucigrama\').find(\'.nopreview\').remove();\n      }\n    }\n  };\n\n  iniciarCrucigrama();\n\n});\n\n</script>', 0, 'G');
INSERT INTO `herramientas` (`idtool`, `idnivel`, `idunidad`, `idactividad`, `idpersonal`, `titulo`, `descripcion`, `texto`, `orden`, `tool`) VALUES
(14, 1, 6, 18, 99999999, 'Greetings', '', '<input type="hidden" name="idgui" id="idgui" value="58b8247b9fe64">\n<input type="hidden" name="orden[1][1]" value="1">\n<input type="hidden" name="det_tipo[1][1]" value="V">\n<input type="hidden" name="tipo_desarrollo[1][1]" value="D">\n<input type="hidden" name="tipo_actividad[1][1]" value="A">\n<div class="plantilla plantilla-crucigrama" data-idgui="58b8247b9fe64">\n  <div class="row">\n    <div class="col-xs-12 pull-right text-right btns-zone nopreview" style="z-index: 9;">\n        <a class="btn btn-primary save-crossword">\n            <i class="fa fa-floppy-o"></i> \n            save crossword        </a>\n    </div>\n    <div class="col-md-12">\n      <div id="crossword58b8247b9fe64"><table class="crossword">\n<tbody><tr>\n<td class="no-border">\n&nbsp;\n</td><td class="" style="background:url(\'__xRUTABASEx__/static/libs/crusigrama/numbers/1.png\') no-repeat">\n<input data-valn="1" value="" data-row="0" data-col="1" data-val="b" class="tvalcrusigrama" style=" ">\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td></tr>\n<tr>\n<td class="no-border">\n&nbsp;\n</td><td class="">\n<input data-valn="1" value="" data-row="1" data-col="1" data-val="y" class="tvalcrusigrama" style=" ">\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="" style="background:url(\'__xRUTABASEx__/static/libs/crusigrama/numbers/2.png\') no-repeat">\n<input data-valn="2" value="" data-row="1" data-col="4" data-val="G" class="tvalcrusigrama" style=" ">\n</td></tr>\n<tr>\n<td class="" style="background:url(\'__xRUTABASEx__/static/libs/crusigrama/numbers/3.png\') no-repeat">\n<input data-valn="3" value="" data-row="2" data-col="0" data-val="H" class="tvalcrusigrama" style=" ">\n</td><td class="">\n<input data-valn="3" value="" data-row="2" data-col="1" data-val="e" class="tvalcrusigrama" style=" ">\n</td><td class="">\n<input data-valn="3" value="" data-row="2" data-col="2" data-val="l" class="tvalcrusigrama" style=" ">\n</td><td class="">\n<input data-valn="3" value="" data-row="2" data-col="3" data-val="l" class="tvalcrusigrama" style=" ">\n</td><td class="">\n<input data-valn="3" value="" data-row="2" data-col="4" data-val="o" class="tvalcrusigrama" style=" ">\n</td></tr>\n<tr>\n<td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="">\n<input data-valn="3" value="" data-row="3" data-col="4" data-val="o" class="tvalcrusigrama" style=" ">\n</td></tr>\n<tr>\n<td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="no-border">\n&nbsp;\n</td><td class="">\n<input data-valn="3" value="" data-row="4" data-col="4" data-val="d" class="tvalcrusigrama" style=" ">\n</td></tr>\n</tbody></table></div>\n      <div class="col-xs-12 col-sm-12 col-md-12 ">\n              <table class="nopreview table table-striped table-responsive" id="alternacru58b8247b9fe64">               \n                <tbody>\n                  <tr>\n                    <td><div class="addtextvideo txtquestion58b8247b9fe64" data-initial_val="Hello">Hello</div></td>\n                    <td class="addtextvideo nopreview txtresponse58b8247b9fe64" data-initial_val="Hello">Hello</td>\n                    <td class="nopreview"><span class="removedialog tooltip" title="" data-original-title="Remove question"><i class="fa fa-trash"></i> </span></td>\n                  </tr>\n                  <tr>\n                    <td><div class="addtextvideo txtquestion58b8247b9fe64" data-initial_val="good">good</div></td>\n                    <td class="addtextvideo nopreview txtresponse58b8247b9fe64" data-initial_val="Good">Good</td>\n                    <td class="nopreview  "><span class="removedialog tooltip" title="" data-original-title="Remove question"><i class="fa fa-trash"></i> </span></td>\n                  </tr>\n                  <tr class="hidden" id="addpregunta58b8247b9fe64">\n                    <td><div class="addtextvideo txtquestion58b8247b9fe64">Add Question or reference</div></td>\n                    <td class="addtextvideo nopreview txtresponse58b8247b9fe64">Response</td>\n                    <td class="nopreview"><span class="removedialog tooltip" title="" data-original-title="Remove question"><i class="fa fa-trash"></i> </span></td>\n                  </tr>                                \n                <tr>\n                    <td><div class="addtextvideo txtquestion58b8247b9fe64" data-initial_val="bye">bye</div></td>\n                    <td class="addtextvideo nopreview txtresponse58b8247b9fe64" data-initial_val="bye">bye</td>\n                    <td class="nopreview"><span class="removedialog tooltip" title="" data-original-title="Remove question"><i class="fa fa-trash"></i> </span></td>\n                  </tr></tbody>              \n              </table>\n              <div class="text-center nopreview">\n                <span class="btn btn-xs btn-primary addclone58b8247b9fe64 nopreview tooltip" data-source="#addpregunta58b8247b9fe64" data-donde="#alternacru58b8247b9fe64" title="" data-original-title="Add more Question or reference">\n                  <i class="fa fa-plus-square"></i>\n                </span> \n                <span class="btn btn-xs btn-primary btngeneratecrusigrama nopreview tooltip" data-donde="#txtdialogocole" title="" data-original-title="Generate crossword">\n                  <i class="fa fa-gamepad"></i>\n                </span>\n              </div>\n              <table class="table table-responsive" id="tbopcion58b8247b9fe64">\n                  <thead>\n                      <tr>\n                          <th class="text-center">Horizontal</th>\n                          <th class="text-center">Vertical</th>\n                      </tr>\n                  </thead>\n                  <tbody>\n                      <tr>\n                          <td><ul id="across58b8247b9fe64"><li class="crusigramalista" data-valn="3" data-orient="row" data-val="HELLO"><strong>3.</strong> Hello</li></ul></td>\n                          <td><ul id="down58b8247b9fe64"><li class="crusigramalista" data-valn="1" data-orient="col" data-val="BYE"><strong>1.</strong> bye</li>\n<li class="crusigramalista" data-valn="2" data-orient="col" data-val="GOOD"><strong>2.</strong> good</li></ul></td>\n                      </tr>\n                  </tbody>\n              </table>\n            </div>      \n    </div>\n  </div>\n</div> \n\n<script type="text/javascript">\n$(\'.tooltip\').tooltip();\n$(document).ready(function(){\n  $(\'#alternacru58b8247b9fe64\').on(\'click\',\'.removedialog\',function(){\n    $(this).parent().parent(\'tr\').remove();\n  });\n\n  $(\'.addclone58b8247b9fe64\').click(function(){\n    var de=$(this).attr(\'data-source\');\n    var adonde=$(this).attr(\'data-donde\');\n    var clon=$(de).clone();\n    $(adonde).append(\'<tr>\'+clon.html()+\'</tr>\');\n    $(\'.tooltip\').tooltip();  \n  });\n\n  $(\'.btngeneratecrusigrama\').click(function(){\n      var question=$(\'.txtquestion58b8247b9fe64\');\n      var response=$(\'.txtresponse58b8247b9fe64\');\n      var datosp=[], datosf=[];\n      response.each(function(){\n        if(!$(this).parent().hasClass(\'hidden\')){\n          txt=$(this).text();\n          datosp.push(txt.trim());\n        }\n      });\n      question.each(function(){\n       if(!$(this).parent().parent().hasClass(\'hidden\')){\n          txt=$(this).text();\n          datosf.push(txt.trim());\n        }\n      });      \n      var datos=[{palabras:datosp,frase:datosf}];\n      $(\'#tbopcion58b8247b9fe64\').removeClass(\'hidden\');\n      cargarcrucigrama(\'58b8247b9fe64\',datos);\n      $(\'.btns-zone\').show(\'fast\');\n  });\n\n  $(\'#alternacru58b8247b9fe64\')\n      .on(\'click\',\'.addtextvideo\', function(e){\n       // if( $(\'.preview\').is(\':visible\') ){ //si btn.preview es Visible:estamos en edicion\n          var in_=$(this);\n          var texto_inicial = $(this).text().trim();\n          $(this).attr(\'data-initial_val\', texto_inicial);\n          in_.html(\'<input type="text" required="required" class="form-control" id="inputEditTexto" value="\'+ texto_inicial +\'">\');\n          if($(this).hasClass("addtime")){\n            $(\'input#inputEditTexto\',in_).addClass(\'addtime\');\n          }\n          $(\'input#inputEditTexto\',in_).focus().select();\n        //}\n      })\n      .on(\'blur\',\'input#inputEditTexto\',function(){\n        var dataesc=$(this).attr(\'data-esc\');\n        var vini=$(this).parent().attr(\'data-initial_val\');\n        var vin=$(this).val();\n        if(dataesc) _intxt=vini;\n        else _intxt=vin;\n        $(this).removeAttr(\'data-esc\');\n        $(this).parent().attr(\'data-initial_val\',_intxt); \n        if($(this).hasClass("addtime")){   \n          var time=_intxt.split(":");\n          var h=0,m=0,s=0;\n          if(time.length==3){h=parseInt(time[0]);m=parseInt(time[1]);s=parseInt(time[2])}\n          if(time.length==2){m=parseInt(time[0]);s=parseInt(time[1])}\n          if(time.length==1){s=parseInt(time[0])}\n          s=(h*3600)+(m*60)+s;\n          var parent = $(this).parent().parent().parent().parent().attr(\'data-time\',s);\n        }\n        $(this).parent().text(_intxt);\n      })\n      .on(\'keypress\',\'input#inputEditTexto\', function(e){      \n        if(e.which == 13){ //Enter pressed\n          e.preventDefault();          \n          $(this).trigger(\'blur\');\n        } \n      }).on(\'keyup\',\'input#inputEditTexto\', function(e){      \n        if(e.which == 27){ //Enter esc\n          $(this).attr(\'data-esc\',"1");\n          $(this).trigger(\'blur\');\n        }\n      });\n\n  $(\'#crossword58b8247b9fe64\').on(\'keydown\',\'input.tvalcrusigrama\',function(ev){\n    if($(this).hasClass(\'ok\')) return false;\n    $(this).val(\'\');\n  });\n\n  $(\'#crossword58b8247b9fe64\').on(\'keyup\',\'input.tvalcrusigrama\',function(){\n    val =$(this).data(\'val\');\n    val2=$(this).val();\n    var tvalrow=[\'--\'];\n    $(\'#across58b8247b9fe64\').find(\'li[data-orient="row"]\').each(function(){\n      var rval=$(this).data(\'val\');\n      tvalrow.push(rval.toUpperCase());\n    });\n    var tvalcol=new Array();\n    $(\'#down58b8247b9fe64\').find(\'li[data-orient="col"]\').each(function(){\n      var rval=$(this).data(\'val\');\n      tvalcol.push(rval.toUpperCase());\n    });\n    if(val.trim().toUpperCase()==val2.trim().toUpperCase()){\n      $(this).addClass(\'ok\').removeClass(\'error\').attr(\'readonly\',\'true\');\n      var r=$(this).data(\'row\');\n      var c=$(this).data(\'col\');\n      var todosrow=$(\'#crossword58b8247b9fe64\').find(\'input.tvalcrusigrama.ok[data-row="\'+r+\'"]\');\n      var palabra=new Array();\n      if(todosrow.length>1){\n        $.each(todosrow,function(){\n          var c=$(this).data(\'col\');\n          var v_=$(this).data(\'val\');\n          palabra[c]=v_;\n        });\n        txt=palabra.join("").toUpperCase();\n        if(tvalrow.indexOf(txt)>-1){\n          $(\'#across58b8247b9fe64\').find(\'li[data-val="\'+txt+\'"]\').css({color:\'green\',\'text-decoration\':\'line-through\'});\n        }\n      }\n      //var palabra=[];\n      var todoscol=$(\'#crossword58b8247b9fe64\').find(\'input.tvalcrusigrama.ok[data-col="\'+c+\'"]\');\n      var palabra=new Array();\n      if(todoscol.length>1){\n        $.each(todoscol,function(){         \n          var c=$(this).data(\'row\');\n          var v_=$(this).data(\'val\');\n          palabra[c]=v_;\n        });\n        txt=palabra.join("").toUpperCase();\n        if(tvalcol.indexOf(txt)>-1){\n          $(\'#down58b8247b9fe64\').find(\'li[data-val="\'+txt+\'"]\').css({color:\'green\',\'text-decoration\':\'line-through\'});\n        }\n      }      \n    }else\n      $(this).addClass(\'error\').removeClass(\'ok\');\n      \n  });\n\n  $(\'.save-crossword\').click(function(e) {\n    e.preventDefault();\n    msjes = {\n        \'attention\' : \'Attention\',\n        \'guardado_correcto\' : \'Game saved successfully\'\n    };\n    var data={\n      idgame:$(\'#idgame\').val(),\n      nivel:\'1\',\n      unidad:\'6\',\n      actividad:\'18\',\n      texto:$(\'.games-main\').html(),\n      titulo:$(\'.titulo-game\').val(),\n      descripcion:$(\'.descripcion-game\').val()\n    }\n    var idgame=saveGame(data,msjes);\n    $(\'#idgame\').val(idgame);\n  });\n\n  var iniciarCrucigrama = function(){\n    var valRol = $(\'#hRol\').val();\n    if( valRol!=\'\' && valRol!=undefined ){\n      if(valRol != 1){\n        $(\'.plantilla-crucigrama\').find(\'.nopreview\').remove();\n      }\n    }\n  };\n\n  iniciarCrucigrama();\n\n});\n\n</script>', 0, 'G');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `historial_sesion`
--

CREATE TABLE `historial_sesion` (
  `idhistorialsesion` int(11) NOT NULL,
  `tipousuario` char(2) COLLATE utf8_spanish_ci NOT NULL COMMENT 'A=Alumno ; P=Personal',
  `idusuario` int(11) NOT NULL,
  `lugar` char(2) COLLATE utf8_spanish_ci NOT NULL COMMENT 'P=Plataforma; A=Actividades; TR=Teacher Resrc.; G=Games; E=Examen',
  `fechaentrada` datetime NOT NULL,
  `fechasalida` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `local`
--

CREATE TABLE `local` (
  `idlocal` int(11) NOT NULL,
  `nombre` varchar(500) COLLATE utf8_spanish_ci NOT NULL DEFAULT '',
  `direccion` text COLLATE utf8_spanish_ci,
  `id_ubigeo` varchar(6) COLLATE utf8_spanish_ci NOT NULL,
  `tipo` char(1) COLLATE utf8_spanish_ci NOT NULL,
  `vacantes` int(11) DEFAULT NULL,
  `idugel` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `local`
--

INSERT INTO `local` (`idlocal`, `nombre`, `direccion`, `id_ubigeo`, `tipo`, `vacantes`, `idugel`) VALUES
(1, 'Institucion educativa 1154', 'una direccion #123', '140202', 'C', 10, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `manuales`
--

CREATE TABLE `manuales` (
  `idmanual` int(11) NOT NULL,
  `titulo` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `abreviado` varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  `stock` int(11) NOT NULL,
  `total` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `manuales_alumno`
--

CREATE TABLE `manuales_alumno` (
  `identrega` int(11) NOT NULL,
  `idgrupo` char(9) COLLATE utf8_spanish_ci NOT NULL,
  `idalumno` char(8) COLLATE utf8_spanish_ci NOT NULL,
  `idmanual` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `estado` char(1) COLLATE utf8_spanish_ci NOT NULL,
  `serie` varchar(100) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `matricula`
--

CREATE TABLE `matricula` (
  `idmatricula` int(11) NOT NULL,
  `idalumno` char(8) COLLATE utf8_spanish_ci NOT NULL,
  `idactividad` int(11) DEFAULT NULL,
  `fechamatricula` date NOT NULL,
  `estado` char(1) COLLATE utf8_spanish_ci NOT NULL,
  `regusuario` varchar(15) COLLATE utf8_spanish_ci NOT NULL,
  `regfecha` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `matricula_alumno`
--

CREATE TABLE `matricula_alumno` (
  `idmatriculaalumno` int(11) NOT NULL,
  `idalumno` int(11) NOT NULL,
  `codigo` int(11) NOT NULL COMMENT 'id de examen o id de actividad(leccion)',
  `tipo` char(1) COLLATE utf8_spanish_ci NOT NULL COMMENT 'L=Leccion ; E=Examen',
  `orden` int(11) NOT NULL,
  `fechamatricula` date NOT NULL,
  `fechacaduca` date NOT NULL,
  `regusuario` int(11) NOT NULL,
  `fecharegistro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `matricula_alumno`
--

INSERT INTO `matricula_alumno` (`idmatriculaalumno`, `idalumno`, `codigo`, `tipo`, `orden`, `fechamatricula`, `fechacaduca`, `regusuario`, `fecharegistro`) VALUES
(1, 72042592, 18, 'L', 1, '2017-04-10', '2017-11-30', 72042592, '2017-04-11 23:43:45'),
(2, 72042592, 19, 'L', 2, '2017-04-10', '2017-11-30', 72042592, '2017-04-12 00:04:23'),
(3, 72042592, 20, 'L', 3, '2017-04-10', '2017-11-30', 72042592, '2017-04-12 00:04:23');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mensajes`
--

CREATE TABLE `mensajes` (
  `idmensaje` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `menu`
--

CREATE TABLE `menu` (
  `idmenu` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `menu`
--

INSERT INTO `menu` (`idmenu`, `nombre`) VALUES
(1, 'Actividad'),
(2, 'Vocabulario'),
(3, 'seralumno'),
(4, 'permisos'),
(5, 'menu'),
(6, 'sincronizar'),
(7, 'registros'),
(8, 'Cursos'),
(9, 'reportes'),
(10, 'comunidad'),
(11, 'recursos'),
(12, 'Evaluacion'),
(13, 'ugel'),
(14, 'Local'),
(15, 'Ambiente'),
(16, 'unidad'),
(17, 'niveles'),
(18, 'detalle_matricula_alumno'),
(19, 'configuracion_docente'),
(20, 'reporte'),
(21, 'docente'),
(22, 'tarea'),
(23, 'tarea_asignacion'),
(24, 'tarea_respuesta'),
(25, 'tarea_archivos'),
(26, 'tarea_asignacion_alumno');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `metodologia_habilidad`
--

CREATE TABLE `metodologia_habilidad` (
  `idmetodologia` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `tipo` char(1) NOT NULL,
  `estado` int(11) DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Metodologias y Habilidades';

--
-- Volcado de datos para la tabla `metodologia_habilidad`
--

INSERT INTO `metodologia_habilidad` (`idmetodologia`, `nombre`, `tipo`, `estado`) VALUES
(1, 'Look', 'M', 1),
(2, 'Practice', 'M', 1),
(3, 'Do it by yourself', 'M', 1),
(4, 'Listening', 'H', 1),
(5, 'Reading', 'H', 1),
(6, 'Writing', 'H', 1),
(7, 'Speaking', 'H', 1),
(8, 'Grammar / Structure', 'H', 1),
(9, 'Vocabulary', 'H', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `niveles`
--

CREATE TABLE `niveles` (
  `idnivel` int(11) NOT NULL,
  `nombre` varchar(250) NOT NULL,
  `tipo` char(1) NOT NULL COMMENT 'N= nivel, U= unidad, L=lesson',
  `idpadre` int(11) NOT NULL,
  `idpersonal` int(11) NOT NULL DEFAULT '0',
  `estado` int(11) NOT NULL COMMENT '1 activo , 0 inactivo',
  `orden` tinyint(4) NOT NULL DEFAULT '0',
  `imagen` varchar(200) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `niveles`
--

INSERT INTO `niveles` (`idnivel`, `nombre`, `tipo`, `idpadre`, `idpersonal`, `estado`, `orden`, `imagen`) VALUES
(1, 'A1', 'N', 0, 0, 1, 0, NULL),
(2, 'A2', 'N', 0, 0, 1, 0, NULL),
(3, 'B1', 'N', 0, 0, 1, 0, NULL),
(4, 'B2', 'N', 0, 0, 1, 0, NULL),
(5, 'C1', 'N', 0, 0, 1, 0, NULL),
(56, 'What\'s this?', 'L', 6, 33333333, 1, 38, '__xRUTABASEx__/static/media/image/N1-U1-A1-20170313051952.jpg'),
(18, 'Hello', 'L', 6, 77777777, 1, 1, '__xRUTABASEx__/static/media/image/N1-U1-A1-20170222105623.jpg'),
(17, 'My year', 'U', 1, 77777777, 1, 0, '__xRUTABASEx__/static/media/image/N1-U1-A1-20170314122424.jpg'),
(16, 'I know what you did yesterday', 'U', 1, 77777777, 1, 0, '__xRUTABASEx__/static/media/image/N1-U1-A1-20170314121727.jpg'),
(15, 'Where do you live?', 'U', 1, 77777777, 1, 0, '__xRUTABASEx__/static/media/image/N1-U1-A1-20170314114745.jpg'),
(14, 'Right now!', 'U', 1, 33333333, 1, 0, '__xRUTABASEx__/static/media/image/N1-U1-A1-20170323063813.jpg'),
(13, 'It\'s a fact!', 'U', 1, 33333333, 1, 0, '__xRUTABASEx__/static/media/image/N1-U1-A1-20170314113752.jpg'),
(12, 'Food and drink', 'U', 1, 77777777, 1, 0, '__xRUTABASEx__/static/media/image/N1-U1-A1-20170313110118.jpg'),
(11, 'Going shopping', 'U', 1, 77777777, 1, 0, '__xRUTABASEx__/static/media/image/N1-U1-A1-20170313110322.jpg'),
(10, 'People', 'U', 1, 77777777, 1, 0, '__xRUTABASEx__/static/media/image/N1-U1-A1-20170313110217.jpg'),
(9, 'Leisure activities', 'U', 1, 33333333, 1, 0, '__xRUTABASEx__/static/media/image/N1-U1-A1-20170323063742.jpg'),
(8, 'Where are you from?', 'U', 1, 77777777, 1, 0, '__xRUTABASEx__/static/media/image/N1-U1-A1-20170314114908.png'),
(7, 'Numbers & Figures', 'U', 1, 33333333, 1, 0, '__xRUTABASEx__/static/media/image/N1-U1-A1-20170313033457.jpg'),
(6, 'Introductions', 'U', 1, 99999999, 1, 0, '__xRUTABASEx__/static/media/image/N1-U1-A1-20170221083851.jpg'),
(21, 'Numbers', 'L', 7, 77777777, 1, 2, '__xRUTABASEx__/static/media/image/N1-U1-A1-20170313053818.jpeg'),
(22, 'Figures', 'L', 7, 33333333, 1, 6, '__xRUTABASEx__/static/media/image/N1-U1-A1-20170313054124.jpg'),
(23, 'Names', 'L', 7, 33333333, 1, 10, '__xRUTABASEx__/static/media/image/N1-U1-A1-20170313054219.jpg'),
(24, 'Countries', 'L', 8, 77777777, 1, 11, '__xRUTABASEx__/static/media/image/N1-U1-A1-20170313055154.png'),
(25, 'Nationalities', 'L', 8, 77777777, 1, 12, '__xRUTABASEx__/static/media/image/N1-U1-A1-20170313055826.jpg'),
(26, 'Places', 'L', 8, 77777777, 1, 14, '__xRUTABASEx__/static/media/image/N1-U1-A1-20170313055956.jpg'),
(27, 'My week', 'L', 9, 77777777, 1, 3, '__xRUTABASEx__/static/media/image/N1-U1-A1-20170313060555.jpg'),
(29, 'Leisure activities', 'L', 9, 77777777, 1, 16, '__xRUTABASEx__/static/media/image/N1-U1-A1-20170313061943.jpg'),
(30, 'Describing people', 'L', 10, 77777777, 1, 8, '__xRUTABASEx__/static/media/image/N1-U1-A1-20170323052738.jpg'),
(31, 'The body', 'L', 10, 77777777, 1, 13, '__xRUTABASEx__/static/media/image/N1-U1-A1-20170313063050.jpg'),
(32, 'My family', 'L', 10, 77777777, 1, 15, '__xRUTABASEx__/static/media/image/N1-U1-A1-20170313063351.jpg'),
(33, 'At the store', 'L', 11, 77777777, 1, 17, '__xRUTABASEx__/static/media/image/N1-U1-A1-20170313065047.jpg'),
(34, 'Shopping', 'L', 11, 77777777, 1, 18, '__xRUTABASEx__/static/media/image/N1-U1-A1-20170313064055.jpg'),
(35, 'In a supermarket', 'L', 11, 77777777, 1, 19, '__xRUTABASEx__/static/media/image/N1-U1-A1-20170313065148.jpg'),
(36, 'Food items', 'L', 12, 77777777, 1, 20, '__xRUTABASEx__/static/media/image/N1-U1-A1-20170323052824.jpg'),
(37, 'In a restaurant', 'L', 12, 77777777, 1, 21, '__xRUTABASEx__/static/media/image/N1-U1-A1-20170313070130.jpg'),
(38, 'Cooking or dining out?', 'L', 12, 77777777, 1, 22, '__xRUTABASEx__/static/media/image/N1-U1-A1-20170313070403.jpg'),
(39, 'Clothing', 'L', 13, 33333333, 1, 23, '__xRUTABASEx__/static/media/image/N1-U1-A1-20170313070713.jpg'),
(40, 'Planets', 'L', 13, 77777777, 1, 25, '__xRUTABASEx__/static/media/image/N1-U1-A1-20170313070944.jpg'),
(41, 'Animals', 'L', 13, 77777777, 1, 26, '__xRUTABASEx__/static/media/image/N1-U1-A1-20170313071240.jpg'),
(42, 'Weather festivals', 'L', 14, 77777777, 1, 24, '__xRUTABASEx__/static/media/image/N1-U1-A1-20170313071500.jpg'),
(43, 'What am i doing?', 'L', 14, 77777777, 1, 9, '__xRUTABASEx__/static/media/image/N1-U1-A1-20170313072809.png'),
(44, 'Help!', 'L', 14, 77777777, 1, 27, '__xRUTABASEx__/static/media/image/N1-U1-A1-20170313072901.jpg'),
(45, 'My hometown', 'L', 15, 77777777, 1, 28, '__xRUTABASEx__/static/media/image/N1-U1-A1-20170313073410.jpg'),
(46, 'My house', 'L', 15, 77777777, 1, 29, '__xRUTABASEx__/static/media/image/N1-U1-A1-20170313073940.jpg'),
(47, 'My address', 'L', 15, 77777777, 1, 30, '__xRUTABASEx__/static/media/image/N1-U1-A1-20170314103719.png'),
(48, 'Where were you?', 'L', 16, 77777777, 1, 31, '__xRUTABASEx__/static/media/image/N1-U1-A1-20170323061239.jpg'),
(49, 'My School Years', 'L', 16, 77777777, 1, 32, '__xRUTABASEx__/static/media/image/N1-U1-A1-20170323061319.jpg'),
(50, 'My work', 'L', 16, 77777777, 1, 33, '__xRUTABASEx__/static/media/image/N1-U1-A1-20170323061349.jpg'),
(51, 'Dates and seasons', 'L', 17, 77777777, 1, 34, '__xRUTABASEx__/static/media/image/N1-U1-A1-20170323061752.jpg'),
(52, 'My holidays', 'L', 17, 77777777, 1, 35, '__xRUTABASEx__/static/media/image/N1-U1-A1-20170323062332.jpg'),
(53, 'Journeys', 'L', 17, 77777777, 1, 36, '__xRUTABASEx__/static/media/image/N1-U1-A1-20170323063656.jpg'),
(55, 'About you', 'L', 6, 33333333, 1, 37, '__xRUTABASEx__/static/media/image/N1-U1-A1-20170313054934.jpg'),
(58, 'Routines', 'L', 9, 33333333, 1, 7, '__xRUTABASEx__/static/media/image/N1-U1-A1-20170704050750.jpg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `notas`
--

CREATE TABLE `notas` (
  `idalumno` char(8) COLLATE utf8_spanish_ci NOT NULL,
  `idactividad` int(11) NOT NULL,
  `puntaje` int(11) DEFAULT NULL,
  `idmetodologia` int(11) DEFAULT NULL,
  `ejercicio` int(11) DEFAULT NULL,
  `intentos` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `permisos`
--

CREATE TABLE `permisos` (
  `idpermiso` int(11) NOT NULL,
  `rol` int(11) NOT NULL,
  `menu` int(11) NOT NULL,
  `_list` tinyint(1) NOT NULL,
  `_add` tinyint(1) NOT NULL,
  `_edit` tinyint(1) NOT NULL,
  `_delete` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `permisos`
--

INSERT INTO `permisos` (`idpermiso`, `rol`, `menu`, `_list`, `_add`, `_edit`, `_delete`) VALUES
(1, 1, 2, 1, 1, 1, 1),
(2, 1, 1, 1, 1, 1, 1),
(3, 2, 2, 1, 1, 1, 1),
(4, 3, 1, 1, 1, 1, 0),
(5, 2, 1, 1, 0, 1, 1),
(6, 3, 2, 1, 1, 1, 0),
(7, 4, 2, 0, 0, 0, 0),
(8, 1, 3, 1, 0, 0, 0),
(9, 2, 3, 1, 0, 0, 0),
(10, 1, 4, 1, 1, 1, 1),
(11, 1, 5, 1, 1, 1, 1),
(12, 1, 6, 1, 1, 0, 0),
(13, 1, 7, 1, 0, 0, 0),
(14, 1, 8, 1, 0, 0, 0),
(15, 1, 9, 1, 0, 0, 0),
(16, 1, 10, 1, 0, 0, 0),
(17, 3, 6, 1, 0, 0, 0),
(18, 2, 11, 1, 0, 1, 0),
(19, 1, 11, 1, 1, 1, 1),
(20, 1, 12, 1, 0, 0, 0),
(21, 1, 13, 1, 1, 1, 1),
(22, 2, 13, 0, 0, 0, 0),
(23, 1, 14, 1, 1, 1, 1),
(24, 1, 15, 1, 1, 1, 1),
(25, 1, 16, 1, 1, 1, 1),
(26, 2, 16, 1, 1, 0, 0),
(27, 2, 17, 1, 1, 0, 0),
(28, 2, 18, 0, 0, 1, 0),
(29, 1, 18, 1, 1, 1, 1),
(30, 1, 19, 1, 1, 1, 1),
(31, 1, 20, 1, 1, 1, 1),
(32, 2, 19, 1, 1, 1, 0),
(33, 2, 20, 1, 1, 1, 0),
(35, 1, 21, 1, 1, 1, 1),
(36, 2, 21, 1, 1, 1, 1),
(37, 1, 22, 1, 1, 1, 1),
(38, 1, 23, 1, 1, 1, 1),
(39, 1, 24, 1, 1, 1, 1),
(40, 1, 25, 1, 1, 1, 1),
(41, 2, 22, 1, 1, 1, 0),
(42, 2, 23, 1, 1, 1, 1),
(43, 2, 24, 0, 0, 0, 0),
(44, 2, 25, 1, 1, 1, 1),
(45, 1, 26, 1, 1, 1, 1),
(46, 2, 26, 1, 1, 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `personal`
--

CREATE TABLE `personal` (
  `dni` int(11) NOT NULL,
  `ape_paterno` varchar(50) NOT NULL,
  `ape_materno` varchar(50) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `fechanac` date DEFAULT NULL,
  `sexo` char(1) NOT NULL,
  `estado_civil` char(1) NOT NULL,
  `ubigeo` char(8) NOT NULL,
  `urbanizacion` varchar(100) NOT NULL,
  `direccion` varchar(100) NOT NULL,
  `telefono` varchar(15) NOT NULL,
  `celular` varchar(15) NOT NULL,
  `email` varchar(75) NOT NULL,
  `idugel` char(4) NOT NULL,
  `regusuario` int(11) NOT NULL DEFAULT '0',
  `regfecha` date NOT NULL,
  `usuario` varchar(35) NOT NULL,
  `clave` varchar(35) NOT NULL,
  `token` varchar(35) NOT NULL,
  `rol` int(11) NOT NULL,
  `foto` varchar(100) NOT NULL,
  `estado` tinyint(1) NOT NULL,
  `situacion` char(1) NOT NULL,
  `idioma` varchar(2) NOT NULL DEFAULT 'EN'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `personal`
--

INSERT INTO `personal` (`dni`, `ape_paterno`, `ape_materno`, `nombre`, `fechanac`, `sexo`, `estado_civil`, `ubigeo`, `urbanizacion`, `direccion`, `telefono`, `celular`, `email`, `idugel`, `regusuario`, `regfecha`, `usuario`, `clave`, `token`, `rol`, `foto`, `estado`, `situacion`, `idioma`) VALUES
(43831104, 'Chinguit', 'Tello', 'Abel', '1986-10-28', 'M', '', '', '', '', '', '', 'abel_chingo@hotmail.com', '', 0, '2017-11-11', 'admin', '0192023a7bbd73250516f069df18b500', '0', 1, '', 1, '1', 'EN'),
(43371672, 'Muñoz', 'Meza', 'Evelio', '2016-11-14', 'M', 'C', '14010101', 'MARIA PARA DE BELLIDO', 'MEXICO 790', '251924', '979222660', 'emunozmeza@gmail.com', '0001', 1, '2016-11-14', '43371672', 'c4ca4238a0b923820dcc509a6f75849b', '43371672', 1, '', 1, '1', 'EN'),
(72042592, 'Figueroa', 'Piscoya', 'Eder', NULL, 'M', 'S', '', '', '', '', '', 'eder.figueroa@outlook.com', '', 0, '2016-12-26', 'eder.figueroa', '81dc9bdb52d04dc20036dbd8313ed055', '1234', 1, '', 1, '1', 'EN'),
(22222222, 'Eder', 'F.P.', 'Docente', NULL, 'M', 'S', '', '', '', '', '', 'docente1@hotmail.com', '', 1, '2016-12-26', 'doc_eder', '81dc9bdb52d04dc20036dbd8313ed055', '1234', 2, '', 1, '1', 'EN'),
(99999999, 'System', '', 'Cv', '2017-02-01', 'M', 'S', '', '', '', '', '', 'info@pvingles.com', '', 0, '0000-00-00', 'pvingles', 'eeb696d150083337a5610b560c2526bb', '', 3, '', 1, '1', 'EN'),
(75935728, 'Garrido', 'Reque', 'Tattiana', '1992-06-21', 'F', 'S', '', '', '', '', '', '', '', 0, '0000-00-00', 'tattiana', '4f1b1013e66d7a6331d96375ff34d814', 'tattiana2017', 1, '', 1, '1', 'EN'),
(33333333, 'System', '', 'docente1', '2017-02-01', 'M', 'S', '', '', '', '', '', 'docente1@hotmail.com', '', 0, '2017-02-09', 'kzoeger', '0192023a7bbd73250516f069df18b500', '8be2efedf58bc5ca5ef33aec86a7a217', 1, '', 1, '1', 'EN'),
(46500285, 'Zoeger', '', 'Karla', '2017-02-01', 'F', 'S', '', '', '', '', '', 'karlazoeger.123@hotmail.com', '', 0, '2017-02-09', 'kzoeger', '0192023a7bbd73250516f069df18b500', 'admin123', 1, '', 1, '1', 'EN'),
(55555555, 'Castro', 'Alzamora', 'Manuel', '1966-10-31', 'M', 'S', '', '', '', '', '', 'mcmanolito@gmail.com', '', 0, '2017-02-09', 'mcastro', '0192023a7bbd73250516f069df18b500', 'admin123', 1, '', 1, '1', 'EN'),
(66666666, 'Enny ', 'Enny', 'Enny', '2017-02-01', 'M', 'S', '', '', '', '', '', 'docente5@hotmail.com', '', 0, '2017-02-09', 'enny21', '81dc9bdb52d04dc20036dbd8313ed055', '0192023a7bbd73250516f069df18b500', 1, '', 1, '1', 'EN'),
(77777777, 'Anais', 'pvingles', 'Anais', '2017-02-01', 'M', 'S', '', '', '', '', '', 'docente6@hotmail.com', '', 0, '2017-02-09', 'anais', '3710fd04a04df697f0af60d5e1345549', '3710fd04a04df697f0af60d5e1345549', 1, '', 0, '0', 'EN'),
(88888888, 'm', '', 'Sharon', '2017-02-01', 'F', 'S', '', '', '', '', '', 'smamud@hotmail.com', '', 0, '2017-02-09', 'smamud', '0577b7cf26c365b19ec51219a7867fb7', '0577b7cf26c365b19ec51219a7867fb7', 1, '', 1, '1', 'EN'),
(11111111, 'Garrido', 'Reque', 'Tatiana', '2017-05-26', 'F', 'S', '', '', '', '', '', '', '', 0, '0000-00-00', 'tatiana', '4e39d9452ab78db254167d7fcf5261e1', '4e39d9452ab78db254167d7fcf5261e1', 1, '', 1, '1', 'EN'),
(21212121, 'Morante', '', 'Monica', NULL, 'F', 'C', '', '', '', '', '', 'mmorante@hotmail.com', '', 0, '2017-06-22', 'mmorante', '0192023a7bbd73250516f069df18b500', 'admin123', 1, '', 1, '1', 'EN'),
(10101010, 'Demo', 'Demo', 'Demo', '1906-06-06', 'F', 'S', '', '', '', '', '', 'demo@hotmail.com', '', 0, '2017-06-23', 'demo', '62cc2d8b4bf2d8728120d052163a77df', 'demo123', 1, '', 1, '1', 'EN');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `record`
--

CREATE TABLE `record` (
  `idrecord` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `link` varchar(150) NOT NULL,
  `idpersonal` int(11) NOT NULL DEFAULT '0',
  `idnivel` int(11) NOT NULL,
  `idunidad` int(11) NOT NULL,
  `idleccion` int(11) NOT NULL,
  `fechareg` date DEFAULT NULL,
  `idherramienta` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `recursos`
--

CREATE TABLE `recursos` (
  `idrecurso` int(11) NOT NULL,
  `idnivel` int(11) NOT NULL,
  `idunidad` int(11) NOT NULL,
  `idactividad` int(11) NOT NULL,
  `idpersonal` int(11) NOT NULL,
  `titulo` varchar(250) COLLATE utf8_spanish_ci DEFAULT NULL,
  `texto` text COLLATE utf8_spanish_ci NOT NULL,
  `caratula` text COLLATE utf8_spanish_ci,
  `orden` tinyint(4) NOT NULL,
  `tipo` char(1) COLLATE utf8_spanish_ci NOT NULL COMMENT 'V=vocabulario,L=pdf,P=pdf,G=Game,R=recursoteachear,A=audiorecord',
  `publicado` tinyint(4) NOT NULL DEFAULT '0' COMMENT '1=si, 0=no',
  `compartido` tinyint(4) NOT NULL DEFAULT '0' COMMENT '1=si,0=no'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `recursos`
--

INSERT INTO `recursos` (`idrecurso`, `idnivel`, `idunidad`, `idactividad`, `idpersonal`, `titulo`, `texto`, `caratula`, `orden`, `tipo`, `publicado`, `compartido`) VALUES
(1, 1, 6, 34, 43831104, 'titulo de ', '<p style="text-align: justify;">CASAS is the most widely used competency-based assessment system in the United States designed to assess the relevant real-world basic skills of adult learners. CASAS measures the basic skills and the English language and literacy skills needed to function effectively at work and in life.</p>\n<p style="text-align: justify;"></p>\n<p style="text-align: justify;">For more than three decades, CASAS, a nonprofit organization, has strengthened the efforts of education and business and industry to transition people age 16 and older to postsecondary education and workplace success. CASAS is used nationally and internationally and is validated by the U.S. Department of Education and the U.S. Department of Labor. The primary focus of CASAS is adult education and workforce development &mdash; identifying the needs and providing the solutions.&nbsp;</p>', NULL, 0, 'P', 0, 0),
(2, 1, 6, 18, 72042592, 'My Teacher Resource', '<p>Teach my student</p>', '__xRUTABASEx__/static/media/image/N1-U1-A1-20170323061349.jpg', 1, 'P', 0, 0),
(3, 1, 6, 18, 99999999, 'Lenguajes de Programación', '<p>HTML , PHP, JS,&nbsp;</p>', '__xRUTABASEx__/static/media/image/N1-U1-A1-20170313070944.jpg', 1, 'P', 0, 0),
(36, 1, 6, 18, 22222222, '', 'null', '__xRUTABASEx__/static/media/video/N1-U1-A1-20170228070928.mp4', 0, 'V', 0, 0),
(37, 1, 6, 18, 22222222, 'Exercises', '', '53,66,75', 0, 'E', 0, 0),
(38, 1, 6, 18, 22222222, 'Games', '', '10', 0, 'G', 0, 0),
(39, 1, 6, 18, 22222222, 'Exam', '', '1', 0, 'X', 0, 0),
(4, 1, 6, 18, 99999999, '', 'null,null,null', '__xRUTABASEx__/static/media/image/N1-U1-A1-20170323061349.jpg,__xRUTABASEx__/static/media/image/N1-U1-A1-20170323061319.jpg,__xRUTABASEx__/static/media/image/N1-U1-A1-20170314114349.jpg', 1, 'I', 0, 0),
(34, 1, 6, 18, 22222222, '', 'null,null', '__xRUTABASEx__/static/media/pdf/N1-U1-A1-20170222111145.pdf,__xRUTABASEx__/static/media/pdf/N1-U1-A1-20170221095258.pdf', 0, 'D', 0, 0),
(35, 1, 6, 18, 22222222, '', 'null,null', '__xRUTABASEx__/static/media/image/N1-U1-A1-20170327111758.jpg,__xRUTABASEx__/static/media/image/N1-U1-A1-20170327120409.jpg', 0, 'I', 0, 0),
(5, 1, 6, 18, 99999999, '', 'null', '__xRUTABASEx__/static/media/video/N1-U1-A1-20170228070928.mp4', 1, 'V', 0, 0),
(33, 1, 6, 18, 22222222, 'Informática e Internet', '<p>La historia de la inform&aacute;tica y el Internet</p>', '__xRUTABASEx__/static/media/image/N1-U1-A1-20170327111758.jpg', 0, 'P', 0, 0),
(11, 1, 6, 18, 99999999, '', 'good evening,Audio 2', '__xRUTABASEx__/static/media/audio/N1-U1-A1-20170218101501.mp3,__xRUTABASEx__/static/media/audio/N1-U1-A1-20170214074459.mp3', 0, 'A', 0, 0),
(6, 1, 6, 18, 99999999, '', 'null', '__xRUTABASEx__/static/media/pdf/N1-U1-A1-20161226093400.pdf', 1, 'D', 0, 0),
(7, 1, 6, 19, 99999999, 'About you', '<p class="MsoNormal"><span lang="EN-US">In this activity &ldquo;About you&rdquo; students will learn how to ask, give and respond to personal information. Students will be able to practice this activity through images, videos, audios, exercises and vocabulary. The objective of this lesson is that students will know how to respond when they are being asked for personal information or when giving personal information. This activity will be very useful at the beginning of the school year for students of all levels.<o:p></o:p></span></p>', '__xRUTABASEx__/static/media/image/N1-U1-A1-20170222105604.jpg', 0, 'P', 0, 0),
(8, 1, 6, 19, 99999999, '', 'Personal information at a job interview,null,null,null,null,Personal information', '__xRUTABASEx__/static/media/image/N1-U1-A1-20170222105623.jpg,__xRUTABASEx__/static/media/image/N1-U1-A1-20170222105631.jpg,__xRUTABASEx__/static/media/image/N1-U1-A1-20170222105631.jpg,__xRUTABASEx__/static/media/image/N1-U1-A1-20170222105631.jpg,__xRUTABASEx__/static/media/image/N1-U1-A1-20170222105604.jpg,__xRUTABASEx__/static/media/image/N1-U1-A1-20170222105604.jpg', 0, 'I', 0, 0),
(9, 1, 6, 19, 99999999, '', 'null,Personal information,At a job interview', '__xRUTABASEx__/static/media/video/N1-U1-A1-20170222103753.mp4,__xRUTABASEx__/static/media/video/N1-U1-A1-20170220071000.mp4,__xRUTABASEx__/static/media/video/N1-U1-A1-20170220071727.mp4', 0, 'V', 0, 0),
(10, 1, 6, 19, 99999999, '', 'null', '__xRUTABASEx__/static/media/pdf/N1-U1-A1-20170222111145.pdf', 0, 'D', 0, 0),
(12, 1, 6, 18, 99999999, 'Vocabulary', '', '{"21431":{"palabra":"Bye","tipo":"[n.]","definicion":"A thing not directly aimed at; something which is a secondary\\n   object of regard; an object by the way, etc.; as in on or upon the bye,\\n   i. e., in passing; indirectly; by implication."},"54667":{"palabra":"Evening","tipo":"[n.]","definicion":"The latter part and close of the day, and the beginning of\\n   darkness or night; properly, the decline of the day, or of the sum."},"71626":{"palabra":"Hello","tipo":"[interj. & n.]","definicion":"See Halloo."},"99076":{"palabra":"Morning","tipo":"[a.]","definicion":"Pertaining to the first part or early part of the day;\\n   being in the early part of the day; as, morning dew; morning light;\\n   morning service."},"102217":{"palabra":"Night","tipo":"[n.]","definicion":"That part of the natural day when the sun is beneath the\\n   horizon, or the time from sunset to sunrise; esp., the time between\\n   dusk and dawn, when there is no light of the sun, but only moonlight,\\n   starlight, or artificial light."}}', 0, 'O', 0, 0),
(13, 1, 6, 18, 99999999, 'Exercises', '', '48,69,79', 1, 'E', 0, 0),
(14, 1, 6, 18, 99999999, 'Games', '', '13', 1, 'G', 0, 0),
(32, 1, 6, 19, 99999999, 'Exercises', '', '7,10', 0, 'E', 0, 0),
(15, 1, 6, 18, 72042592, '', 'null', '__xRUTABASEx__/static/media/pdf/N1-U1-A1-20170221095258.pdf', 1, 'D', 0, 0),
(16, 1, 6, 18, 72042592, 'Vocabulary', '', '{"21431":{"palabra":"Bye","tipo":"[n.]","definicion":"A thing not directly aimed at; something which is a secondary\\n   object of regard; an object by the way, etc.; as in on or upon the bye,\\n   i. e., in passing; indirectly; by implication."},"68234":{"palabra":"Greet","tipo":"[v. t.]","definicion":"To address with salutations or expressions of kind\\n   wishes; to salute; to hail; to welcome; to accost with friendship; to\\n   pay respects or compliments to, either personally or through the\\n   intervention of another, or by writing or token."},"71626":{"palabra":"Hello","tipo":"[interj. & n.]","definicion":"See Halloo."}}', 1, 'O', 0, 0),
(17, 1, 6, 18, 72042592, '', 'good evening', '__xRUTABASEx__/static/media/audio/N1-U1-A1-20170218101501.mp3', 1, 'A', 0, 0),
(18, 1, 6, 18, 72042592, '', 'Leading,how are you?,null', '__xRUTABASEx__/static/media/image/N1-U1-A1-20170323061349.jpg,__xRUTABASEx__/static/media/image/N1-U1-A1-20170323061135.jpg,__xRUTABASEx__/static/media/image/N1-U1-A1-20170314105159.jpg', 1, 'I', 0, 0),
(19, 1, 6, 18, 72042592, '', 'Hello, my name is...,null', '__xRUTABASEx__/static/media/video/N1-U1-A1-20170220062626.mp4,__xRUTABASEx__/static/media/video/N1-U1-A1-20170220062626.mp4', 1, 'V', 0, 0),
(20, 1, 6, 18, 72042592, 'Exercises', '', '49,58', 1, 'E', 0, 0),
(21, 1, 6, 18, 72042592, 'Games', '', '14', 1, 'G', 0, 0),
(22, 1, 6, 18, 43831104, 'Greeteings', '<p>Saludos de prueba</p>', '__xRUTABASEx__/static/media/image/N1-U1-A1-20170221075548.jpg', 0, 'P', 1, 0),
(23, 1, 6, 18, 43831104, 'Games', '', '10,13', 0, 'G', 0, 0),
(24, 1, 6, 18, 43831104, '', 'null', '__xRUTABASEx__/static/media/pdf/N1-U1-A1-20170221095258.pdf', 0, 'D', 1, 0),
(25, 1, 6, 18, 43831104, 'Vocabulary', '', '{"21431":{"palabra":"Bye","tipo":"[n.]","definicion":"A thing not directly aimed at; something which is a secondary\\n   object of regard; an object by the way, etc.; as in on or upon the bye,\\n   i. e., in passing; indirectly; by implication."},"71626":{"palabra":"Hello","tipo":"[interj. & n.]","definicion":"See Halloo."},"99076":{"palabra":"Morning","tipo":"[a.]","definicion":"Pertaining to the first part or early part of the day;\\n   being in the early part of the day; as, morning dew; morning light;\\n   morning service."}}', 0, 'O', 1, 0),
(26, 1, 6, 18, 43831104, '', 'null,null', '__xRUTABASEx__/static/media/image/N1-U1-A1-20170221075548.jpg,__xRUTABASEx__/static/media/image/N1-U1-A1-20170222105518.jpg', 0, 'I', 1, 0),
(27, 1, 6, 18, 43831104, '', 'null,null', '__xRUTABASEx__/static/media/video/N1-U1-A1-20170228070928.mp4,__xRUTABASEx__/static/media/video/N1-U1-A1-20170228122149.mp4', 0, 'V', 1, 0),
(28, 1, 6, 18, 43831104, 'Exercises', '', '48,49,50', 0, 'E', 1, 0),
(29, 1, 6, 18, 43831104, '', 'ten.mp3', '__xRUTABASEx__/static/media/audio/N1-U1-A1-20170227074446.mp3', 0, 'A', 1, 0),
(30, 1, 6, 18, 72042592, 'Exam', '', '4', 1, 'X', 0, 0),
(31, 1, 6, 18, 99999999, 'Exam', '', '4', 1, 'X', 0, 0),
(40, 1, 6, 55, 33333333, '', '<p>In this English lesson you will learn about greetings and introductions, this lesson will teach you how to greet and introduce yourself to others, you will learn<span style="font-family: Calibri, sans-serif; font-size: 11pt; line-height: 107%;">&nbsp;through images, audios, videos, games and more exercises of </span><span style="font-family: Calibri, sans-serif; font-size: 11pt; line-height: 107%;">&nbsp;</span><span style="font-family: Calibri, sans-serif; font-size: 11pt; line-height: 107%;">how to greet and introduce yourself to others. T</span><span style="line-height: 1.42857;">he importance of this lesson is that you will know how to greet and introduce yourself before starting a conversation and this will give you confidence when entering a room or a place.</span></p>', '__xRUTABASEx__/static/media/image/N1-U1-A1-20170706114437.jpg', 0, 'P', 0, 0),
(41, 1, 6, 55, 33333333, '', 'null', '__xRUTABASEx__/static/media/pdf/N1-U1-A1-20170706122240.pdf', 0, 'D', 0, 0),
(42, 1, 6, 55, 33333333, 'Vocabulary', '', '{"54667":{"palabra":"Evening","tipo":"[n.]","definicion":"The latter part and close of the day, and the beginning of\\n   darkness or night; properly, the decline of the day, or of the sum."},"59105":{"palabra":"Fine","tipo":"[superl.]","definicion":"Finished; brought to perfection; refined; hence, free\\n   from impurity; excellent; superior; elegant; worthy of admiration;\\n   accomplished; beautiful."},"67144":{"palabra":"Good","tipo":"[superl.]","definicion":"Possessing desirable qualities; adapted to answer the\\n   end designed; promoting success, welfare, or happiness; serviceable;\\n   useful; fit; excellent; admirable; commendable; not bad, corrupt, evil,\\n   noxious, offensive, or troublesome, etc."},"71626":{"palabra":"Hello","tipo":"[interj. & n.]","definicion":"See Halloo."},"100808":{"palabra":"Name","tipo":"[n.]","definicion":"The title by which any person or thing is known or\\n   designated; a distinctive specific appellation, whether of an\\n   individual or a class."},"102217":{"palabra":"Night","tipo":"[n.]","definicion":"That part of the natural day when the sun is beneath the\\n   horizon, or the time from sunset to sunrise; esp., the time between\\n   dusk and dawn, when there is no light of the sun, but only moonlight,\\n   starlight, or artificial light."}}', 0, 'O', 1, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reporte`
--

CREATE TABLE `reporte` (
  `idreporte` int(11) NOT NULL,
  `idusuario` int(11) NOT NULL,
  `idrol` int(11) NOT NULL,
  `tipo` varchar(16) COLLATE utf8_spanish_ci NOT NULL COMMENT 'Identificador del tipo de reporte. Ej: Seguimiento x alumno=SEGALUM; Seguimiento x Grupo=SEGGRUPO',
  `id_alumno_grupo` int(11) NOT NULL COMMENT 'Dependiendo del TIPO, aqui se especifica el ID ya sea del Alumno o un Grupo de alumnos',
  `informacion` text COLLATE utf8_spanish_ci NOT NULL COMMENT 'Información del reporte en formato JSON',
  `fechacreacion` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `reporte`
--

INSERT INTO `reporte` (`idreporte`, `idusuario`, `idrol`, `tipo`, `id_alumno_grupo`, `informacion`, `fechacreacion`) VALUES
(1, 22222222, 2, 'SEGALUM', 72042592, '[{"idhabilidad":4,"porcentaje":37},{"idhabilidad":5,"porcentaje":66.740740740741},{"idhabilidad":6,"porcentaje":98.75},{"idhabilidad":7,"porcentaje":0},{"idhabilidad":8,"porcentaje":54.5444},{"idhabilidad":9,"porcentaje":78.5}]', '2017-05-11'),
(4, 22222222, 2, 'SEGALUM', 72042592, '[{"idhabilidad":4,"porcentaje":38.7455111111},{"idhabilidad":5,"porcentaje":79.2970838709},{"idhabilidad":6,"porcentaje":75.75},{"idhabilidad":7,"porcentaje":0},{"idhabilidad":8,"porcentaje":75.55},{"idhabilidad":9,"porcentaje":56.75}]', '2017-05-31'),
(5, 22222222, 2, 'SEGGRUPO', 1, '[{"idhabilidad":4,"porcentaje":29.5818370371},{"idhabilidad":5,"porcentaje":40.9492685367},{"idhabilidad":6,"porcentaje":32.1388666667},{"idhabilidad":7,"porcentaje":0},{"idhabilidad":8,"porcentaje":39.4747971015},{"idhabilidad":9,"porcentaje":35.5833333333}]', '2017-05-31'),
(3, 22222222, 2, 'SEGALUM', 12345678, '[{"idhabilidad":4,"porcentaje":50},{"idhabilidad":5,"porcentaje":43.5507217391},{"idhabilidad":6,"porcentaje":20.6666},{"idhabilidad":7,"porcentaje":0},{"idhabilidad":8,"porcentaje":42.8743913043},{"idhabilidad":9,"porcentaje":50}]', '2017-05-11'),
(6, 22222222, 2, 'SEGALUM', 72042592, '[{"idhabilidad":4,"porcentaje":38.7455111111},{"idhabilidad":5,"porcentaje":79.2970838709},{"idhabilidad":6,"porcentaje":75.75},{"idhabilidad":7,"porcentaje":0},{"idhabilidad":8,"porcentaje":75.55},{"idhabilidad":9,"porcentaje":56.75}]', '2017-06-01'),
(2, 22222222, 2, 'SEGGRUPO', 1, '[{"idhabilidad":4,"porcentaje":16.6666666666},{"idhabilidad":5,"porcentaje":22.2638208266},{"idhabilidad":6,"porcentaje":22.1388666667},{"idhabilidad":7,"porcentaje":0},{"idhabilidad":8,"porcentaje":18.7729452496},{"idhabilidad":9,"porcentaje":30.4166666667}]', '2017-05-11'),
(7, 22222222, 2, 'SEGALUM', 72042592, '[{"idhabilidad":4,"porcentaje":38.745511111111},{"idhabilidad":5,"porcentaje":79.297083870967},{"idhabilidad":6,"porcentaje":75.75},{"idhabilidad":7,"porcentaje":0},{"idhabilidad":8,"porcentaje":75.55},{"idhabilidad":9,"porcentaje":56.75}]', '2017-07-04'),
(8, 22222222, 2, 'SEGALUM', 72042592, '[{"idhabilidad":4,"porcentaje":38.745511111111},{"idhabilidad":5,"porcentaje":79.297083870967},{"idhabilidad":6,"porcentaje":75.75},{"idhabilidad":7,"porcentaje":0},{"idhabilidad":8,"porcentaje":75.55},{"idhabilidad":9,"porcentaje":56.75}]', '2017-07-20'),
(9, 22222222, 2, 'SEGALUM', 72042592, '[{"idhabilidad":4,"porcentaje":38.745511111111},{"idhabilidad":5,"porcentaje":79.297083870967},{"idhabilidad":6,"porcentaje":75.75},{"idhabilidad":7,"porcentaje":0},{"idhabilidad":8,"porcentaje":75.55},{"idhabilidad":9,"porcentaje":56.75}]', '2017-07-26'),
(10, 22222222, 2, 'SEGALUM', 72042592, '[{"idhabilidad":4,"porcentaje":38.745511111111},{"idhabilidad":5,"porcentaje":79.297083870967},{"idhabilidad":6,"porcentaje":75.75},{"idhabilidad":7,"porcentaje":0},{"idhabilidad":8,"porcentaje":75.55},{"idhabilidad":9,"porcentaje":56.75}]', '2017-07-27'),
(11, 22222222, 2, 'SEGGRUPO', 1, '[{"idhabilidad":4,"porcentaje":29.581837037037},{"idhabilidad":5,"porcentaje":40.949268536699},{"idhabilidad":6,"porcentaje":32.138866666667},{"idhabilidad":7,"porcentaje":0},{"idhabilidad":8,"porcentaje":39.474797101449},{"idhabilidad":9,"porcentaje":35.583333333333}]', '2017-08-01'),
(12, 22222222, 2, 'SEGALUM', 72042592, '[{"idhabilidad":4,"porcentaje":38.745511111111},{"idhabilidad":5,"porcentaje":79.297083870967},{"idhabilidad":6,"porcentaje":75.75},{"idhabilidad":7,"porcentaje":0},{"idhabilidad":8,"porcentaje":75.55},{"idhabilidad":9,"porcentaje":56.75}]', '2017-08-01'),
(13, 22222222, 2, 'SEGALUM', 72042592, '[{"idhabilidad":4,"porcentaje":38.745511111111},{"idhabilidad":5,"porcentaje":79.297083870967},{"idhabilidad":6,"porcentaje":75.75},{"idhabilidad":7,"porcentaje":0},{"idhabilidad":8,"porcentaje":75.55},{"idhabilidad":9,"porcentaje":56.75}]', '2017-08-02'),
(14, 22222222, 2, 'SEGALUM', 72042592, '[{"idhabilidad":4,"porcentaje":38.745511111111},{"idhabilidad":5,"porcentaje":79.297083870967},{"idhabilidad":6,"porcentaje":75.75},{"idhabilidad":7,"porcentaje":0},{"idhabilidad":8,"porcentaje":75.55},{"idhabilidad":9,"porcentaje":56.75}]', '2017-08-03');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles`
--

CREATE TABLE `roles` (
  `idrol` int(11) NOT NULL,
  `rol` varchar(35) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `roles`
--

INSERT INTO `roles` (`idrol`, `rol`) VALUES
(1, 'administrador'),
(2, 'docente'),
(3, 'Supervisor');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rub_detalle_respuestas`
--

CREATE TABLE `rub_detalle_respuestas` (
  `id_det_rptas` int(11) NOT NULL,
  `id_respuestas` int(11) NOT NULL,
  `id_pregunta` int(11) NOT NULL,
  `respuesta` text NOT NULL,
  `puntaje_obtenido` float NOT NULL,
  `id_dimension` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `rub_detalle_respuestas`
--

INSERT INTO `rub_detalle_respuestas` (`id_det_rptas`, `id_respuestas`, `id_pregunta`, `respuesta`, `puntaje_obtenido`, `id_dimension`) VALUES
(1, 1, 1, '2', 2, 1),
(2, 1, 2, '2', 2, 1),
(3, 1, 3, '2', 2, 1),
(4, 1, 4, '2', 2, 1),
(5, 1, 5, '1', 3, 1),
(6, 1, 6, '1', 1, 1),
(7, 1, 7, '1', 1, 1),
(8, 1, 15, '3', 3, 1),
(9, 1, 16, '3', 3, 1),
(10, 1, 17, '3', 3, 1),
(11, 1, 18, '2', 2, 2),
(12, 1, 19, '2', 2, 2),
(13, 1, 20, '1', 1, 3),
(14, 1, 21, '1', 1, 3),
(15, 1, 22, '2', 2, 4),
(16, 1, 23, '2', 2, 4),
(17, 1, 24, '2', 2, 4),
(18, 1, 25, '2', 2, 4),
(19, 1, 26, '2', 2, 4),
(20, 1, 27, '3', 3, 4),
(21, 1, 28, '3', 3, 4),
(22, 1, 29, '3', 3, 4),
(23, 1, 30, '3', 3, 4),
(24, 1, 31, '3', 3, 4),
(25, 1, 32, '3', 3, 4),
(26, 1, 33, '2', 2, 4),
(27, 1, 34, '2', 2, 4),
(28, 1, 35, '2', 2, 4),
(29, 1, 36, '2', 2, 4),
(30, 1, 37, '2', 2, 4),
(31, 1, 38, '1', 1, 5),
(32, 1, 39, '1', 1, 5),
(33, 1, 40, '1', 1, 5),
(34, 1, 41, '1', 1, 5),
(35, 1, 42, '2', 2, 6),
(36, 1, 43, '2', 2, 6),
(37, 1, 44, '2', 2, 6),
(38, 1, 45, '2', 2, 6),
(39, 1, 46, '2', 2, 6),
(40, 1, 47, '3', 3, 7),
(41, 1, 48, '3', 3, 7),
(42, 1, 49, '3', 3, 7),
(43, 1, 50, '3', 3, 7),
(44, 1, 51, '3', 3, 7),
(45, 1, 52, '3', 3, 7),
(46, 1, 53, '2', 2, 7),
(47, 1, 54, '2', 2, 7),
(48, 1, 55, '2', 2, 8),
(49, 1, 56, '2', 2, 8),
(50, 1, 57, '1', 1, 8),
(51, 1, 58, '1', 1, 8),
(52, 1, 59, '1', 1, 8),
(53, 2, 1, '3', 3, 1),
(54, 2, 2, '2', 2, 1),
(55, 2, 3, '1', 1, 1),
(56, 2, 4, '1', 1, 1),
(57, 2, 5, '2', 2, 1),
(58, 2, 6, '1', 1, 1),
(59, 2, 7, '2', 2, 1),
(60, 2, 15, '2', 2, 1),
(61, 2, 16, '2', 2, 1),
(62, 2, 17, '3', 3, 1),
(63, 2, 18, '3', 3, 2),
(64, 2, 19, '2', 2, 2),
(65, 2, 20, '1', 1, 3),
(66, 2, 21, '1', 1, 3),
(67, 2, 22, '2', 2, 4),
(68, 2, 23, '3', 3, 4),
(69, 2, 24, '3', 3, 4),
(70, 2, 25, '2', 2, 4),
(71, 2, 26, '1', 1, 4),
(72, 2, 27, '1', 1, 4),
(73, 2, 28, '1', 1, 4),
(74, 2, 29, '2', 2, 4),
(75, 2, 30, '3', 3, 4),
(76, 2, 31, '3', 3, 4),
(77, 2, 32, '2', 2, 4),
(78, 2, 33, '2', 2, 4),
(79, 2, 34, '2', 2, 4),
(80, 2, 35, '1', 1, 4),
(81, 2, 36, '1', 1, 4),
(82, 2, 37, '2', 2, 4),
(83, 2, 38, '3', 3, 5),
(84, 2, 39, '3', 3, 5),
(85, 2, 40, '2', 2, 5),
(86, 2, 41, '2', 2, 5),
(87, 2, 42, '2', 2, 6),
(88, 2, 43, '2', 2, 6),
(89, 2, 44, '1', 1, 6),
(90, 2, 45, '2', 2, 6),
(91, 2, 46, '3', 3, 6),
(92, 2, 47, '3', 3, 7),
(93, 2, 48, '2', 2, 7),
(94, 2, 49, '2', 2, 7),
(95, 2, 50, '1', 1, 7),
(96, 2, 51, '1', 1, 7),
(97, 2, 52, '2', 2, 7),
(98, 2, 53, '3', 3, 7),
(99, 2, 54, '3', 3, 7),
(100, 2, 55, '3', 3, 8),
(101, 2, 56, '2', 2, 8),
(102, 2, 57, '2', 2, 8),
(103, 2, 58, '3', 3, 8),
(104, 2, 59, '2', 2, 8),
(105, 3, 113, '1', 1, 67),
(106, 3, 114, '1', 1, 67),
(107, 3, 115, '1', 1, 67),
(108, 3, 116, '1', 1, 68),
(109, 3, 117, '0', 0, 68),
(110, 3, 118, '1', 1, 69),
(134, 5, 1068, '1', 1, 568),
(135, 5, 1069, '1', 1, 568),
(136, 5, 1070, '2', 2, 568),
(137, 5, 1071, '3', 3, 568),
(138, 5, 1072, '4', 4, 568),
(139, 5, 1073, '4', 4, 568),
(140, 5, 1074, '3', 3, 568),
(141, 5, 1075, '2', 2, 568),
(142, 5, 1076, '2', 2, 568),
(143, 5, 1077, '1', 1, 568),
(144, 5, 1078, '1', 1, 568),
(145, 5, 1079, '2', 2, 568),
(146, 5, 1080, '3', 3, 568),
(147, 5, 1081, '4', 4, 568),
(148, 5, 1082, '4', 4, 568),
(149, 5, 1083, '3', 3, 568),
(150, 5, 1084, '2', 2, 568),
(151, 6, 113, '1', 1, 67),
(152, 6, 114, '1', 1, 67),
(153, 6, 115, '0', 0, 67),
(154, 6, 116, '0', 0, 68),
(155, 6, 117, '1', 1, 68),
(156, 6, 118, '1', 1, 69),
(157, 7, 113, '1', 1, 67),
(158, 7, 114, '1', 1, 67),
(159, 7, 115, '0', 0, 67),
(160, 7, 116, '1', 1, 68),
(161, 7, 117, '1', 1, 68),
(162, 7, 118, '0', 0, 69),
(163, 8, 113, '1', 1, 67),
(164, 8, 114, '1', 1, 67),
(165, 8, 115, '1', 1, 67),
(166, 8, 116, '1', 1, 68),
(167, 8, 117, '1', 1, 68),
(168, 8, 118, '1', 1, 69),
(169, 0, 1332, '1', 1, 592),
(170, 0, 1333, '2', 2, 592),
(171, 0, 1334, '3', 3, 592),
(172, 0, 1335, '4', 4, 592),
(173, 0, 1336, '1', 1, 592),
(174, 0, 1337, '2', 2, 592),
(175, 0, 1338, '3', 3, 592),
(176, 0, 1339, '4', 4, 592),
(177, 0, 1340, '3', 3, 592),
(178, 0, 1341, '2', 2, 592),
(179, 0, 1342, '1', 1, 592),
(180, 0, 1343, '2', 2, 592),
(181, 0, 1344, '3', 3, 592),
(182, 0, 1332, '1', 1, 592),
(183, 0, 1333, '2', 2, 592),
(184, 0, 1334, '2', 2, 592),
(185, 0, 1335, '2', 2, 592),
(186, 0, 1336, '2', 2, 592),
(187, 0, 1337, '2', 2, 592),
(188, 0, 1338, '2', 2, 592),
(189, 0, 1339, '2', 2, 592),
(190, 0, 1340, '2', 2, 592),
(191, 0, 1341, '2', 2, 592),
(192, 0, 1342, '2', 2, 592),
(193, 0, 1343, '2', 2, 592),
(194, 0, 1344, '2', 2, 592),
(208, 10, 1332, '1', 1, 592),
(209, 10, 1333, '2', 2, 592),
(210, 10, 1334, '3', 3, 592),
(211, 10, 1335, '4', 4, 592),
(212, 10, 1336, '3', 3, 592),
(213, 10, 1337, '1', 1, 592),
(214, 10, 1338, '2', 2, 592),
(215, 10, 1339, '3', 3, 592),
(216, 10, 1340, '4', 4, 592),
(217, 10, 1341, '3', 3, 592),
(218, 10, 1342, '2', 2, 592),
(219, 10, 1343, '1', 1, 592),
(220, 10, 1344, '2', 2, 592);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rub_dimension`
--

CREATE TABLE `rub_dimension` (
  `id_dimension` int(11) NOT NULL,
  `nombre` varchar(60) NOT NULL,
  `tipo_pregunta` varchar(2) NOT NULL COMMENT 'PE=Ponderacion Estandar;  \r\nSN=Si - No ;  \r\nC=Completar;   \r\nA=Alternativas   \r\n',
  `escalas_evaluacion` text CHARACTER SET utf8 NOT NULL,
  `tipo` varchar(1) NOT NULL COMMENT 'N=Nueva ;  R=Repetida',
  `otros_datos` text COMMENT 'Usando en Pond.Est. para guardar cant. de niveles (1-5)',
  `id_rubrica` int(11) NOT NULL,
  `activo` tinyint(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `rub_dimension`
--

INSERT INTO `rub_dimension` (`id_dimension`, `nombre`, `tipo_pregunta`, `escalas_evaluacion`, `tipo`, `otros_datos`, `id_rubrica`, `activo`) VALUES
(1, 'Conducción Institucional', 'PE', '{"Poco Avance":{"rango_inicial":"1","rango_final":"33"},"Avance Significativo":{"rango_inicial":"34","rango_final":"66"},"Logrado":{"rango_inicial":"67","rango_final":"100"}}', 'N', '{"niveles":"3"}', 1, 1),
(2, 'Gestión de la Información para la toma de decisiones', 'PE', '{"Poco Avance":{"rango_inicial":"1","rango_final":"33"},"Avance Significativo":{"rango_inicial":"34","rango_final":"66"},"Logrado":{"rango_inicial":"67","rango_final":"100"}}', 'N', '{"niveles":"3"}', 1, 1),
(3, 'Convivencia y Clima Institucional', 'PE', '{"Poco avance":{"rango_inicial":"1","rango_final":"33"},"Avance Significativo":{"rango_inicial":"34","rango_final":"66"},"Logrado":{"rango_inicial":"67","rango_final":"100"}}', 'N', '{"niveles":"3"}', 1, 1),
(4, 'Procesos pedagógicos', 'PE', '{"Poco Avance":{"rango_inicial":"1","rango_final":"33"},"Avance Significativo":{"rango_inicial":"34","rango_final":"66"},"Logrado":{"rango_inicial":"67","rango_final":"100"}}', 'N', '{"niveles":"3"}', 1, 1),
(5, 'Trabajo conjunto con las familias y la comunidad', 'PE', '{"Poco Avance":{"rango_inicial":"1","rango_final":"33"},"Avance Significativo":{"rango_inicial":"34","rango_final":"66"},"Logrado":{"rango_inicial":"67","rango_final":"100"}}', 'N', '{"niveles":"3"}', 1, 1),
(6, 'Tutoría para el bienestar de niños y adolescentes', 'PE', '{"Poco avance":{"rango_inicial":"1","rango_final":"33"},"Avance significativo":{"rango_inicial":"34","rango_final":"66"},"Logrado":{"rango_inicial":"67","rango_final":"100"}}', 'N', '{"niveles":"3"}', 1, 1),
(7, ' Infraestructura y Recursos', 'PE', '{"Poco avance":{"rango_inicial":"1","rango_final":"33"},"Avance significativo":{"rango_inicial":"34","rango_final":"66"},"Logrado":{"rango_inicial":"67","rango_final":"100"}}', 'N', '{"niveles":"3"}', 1, 1),
(8, 'Verificación del perfil de egreso', 'PE', '{"Poco avance":{"rango_inicial":"1","rango_final":"33"},"Avance significativo":{"rango_inicial":"34","rango_final":"66"},"Logrado":{"rango_inicial":"67","rango_final":"100"}}', 'N', '{"niveles":"3"}', 1, 1),
(67, 'Evaluación de la atención', 'SN', '', 'N', '{"niveles":"2"}', 19, 1),
(68, 'Evaluación del servicio', 'SN', '', 'N', '{"niveles":"2"}', 19, 1),
(69, 'Evaluación de aseo y limpieza', 'SN', '', 'N', '{"niveles":"2"}', 19, 1),
(545, 'nulo', 'PE', '', 'N', '{"niveles":"4"}', 2, 1),
(546, 'nulo', 'SN', '', 'N', '{"niveles":"2"}', 2, 1),
(547, 'nulo', 'PE', '', 'N', '{"niveles":"2"}', 2, 1),
(568, 'nulo', 'PE', '{"Nunca":{"rango_inicial":"0","rango_final":"25"},"Algunas Veces":{"rango_inicial":"26","rango_final":"50"},"Casi Siempre":{"rango_inicial":"51","rango_final":"75"},"Siempre":{"rango_inicial":"76","rango_final":"100"}}', 'N', '{"niveles":"4"}', 32, 1),
(579, 'nulo', 'PE', '', 'N', '{"niveles":"4"}', 36, 1),
(581, 'Conocimiento y Sensibilización', 'PE', '', 'N', '{"niveles":"4"}', 33, 1),
(583, 'Capacidad de aprendizaje', 'PE', '', 'N', '{"niveles":"4"}', 33, 1),
(584, 'Trabajo en Equipo', 'PE', '', 'N', '{"niveles":"4"}', 33, 1),
(585, 'Creatividad', 'PE', '', 'N', '{"niveles":"4"}', 33, 1),
(586, 'GESTIÓN DE PROYECTOS', 'PE', '', 'N', '{"niveles":"4"}', 33, 1),
(587, 'EMPRENDIMIENTO E INNOVACIÓN', 'PE', '', 'N', '{"niveles":"4"}', 33, 1),
(588, 'SOSTENIBILIDAD', 'PE', '', 'N', '{"niveles":"4"}', 33, 1),
(589, 'ORAL', 'PE', '', 'N', '{"niveles":"4"}', 33, 1),
(590, 'ESCRITA', 'PE', '', 'N', '{"niveles":"4"}', 33, 1),
(591, 'INVOLUCRA ACTIVAMENTE A LOS ESTUDIANTES EN EL PROCESO DE APR', 'PE', '', 'N', '{"niveles":"4"}', 37, 1),
(592, 'nulo', 'PE', '', 'N', '{"niveles":"4"}', 38, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rub_estandar`
--

CREATE TABLE `rub_estandar` (
  `id_estandar` int(11) NOT NULL,
  `nombre` text NOT NULL,
  `id_rubrica` int(11) NOT NULL,
  `puntuacion` text NOT NULL,
  `id_dimension` int(11) NOT NULL,
  `activo` tinyint(5) NOT NULL,
  `tipo` varchar(1) NOT NULL,
  `duplicado_id_estandar` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `rub_estandar`
--

INSERT INTO `rub_estandar` (`id_estandar`, `nombre`, `id_rubrica`, `puntuacion`, `id_dimension`, `activo`, `tipo`, `duplicado_id_estandar`) VALUES
(1, 'Proyecto Educativo Institucional (PEI): Pertinente y orientado', 0, '', 1, 1, '', 0),
(2, 'Proyecto Curricular Institucional (PCI): Coherente, diversificado y orientador de los procesos pedagógicos.', 0, '', 1, 1, '', 0),
(3, 'Liderazgo pedagógico: El equipo directivo que ejerce liderazgo pedagógico colaborativo, manteniendo una visión común centrada en la mejora continua, el desarrollo integral y los aprendizajes de los niños y adolescentes ', 0, '', 1, 1, '', 0),
(17, 'Información para la toma de decisiones', 0, '', 2, 1, '', 0),
(18, 'Buen clima institucional', 0, '', 3, 1, '', 0),
(19, 'Desarrollo profesional docente', 0, '', 4, 1, '', 0),
(20, 'Trabajo conjunto con las familias', 0, '', 5, 1, '', 0),
(21, 'Programación curricular pertinente', 0, '', 4, 1, '', 0),
(22, 'Implementación de estrategias pedagógicas', 0, '', 4, 1, '', 0),
(23, 'Monitoreo y evaluación del desempeño de niños y adolescentes', 0, '', 4, 1, '', 0),
(24, 'Monitoreo y evaluación del desempeño de niños y adolescentes', 0, '', 5, 1, '', 0),
(25, 'Tutoría', 0, '', 6, 1, '', 0),
(26, 'Servicios de atención complementaria', 0, '', 6, 1, '', 0),
(27, 'Gestión de Infraestructura', 0, '', 7, 1, '', 0),
(28, 'Gestión de recursos para el desarrollo y los aprendizajes de la institución', 0, '', 7, 1, '', 0),
(29, 'Desarrollo de capacidades del personal de apoyo y/o administrativo', 0, '', 7, 1, '', 0),
(30, 'Logro de competencias', 0, '', 8, 1, '', 0),
(31, 'Seguimiento del egresado', 0, '', 8, 1, '', 0),
(37, 'Laboratorios', 0, '', 30, 1, '', 0),
(38, 'Aulas', 0, '', 30, 1, '', 0),
(39, 'Cumplimiento de requisitos de la alimentación escolar', 0, '', 31, 1, '', 0),
(40, 'Cambio de metodología escolar', 0, '', 32, 1, '', 0),
(45, 'eeest1', 0, '', 0, 1, 'N', 0),
(46, 'eee1', 0, '', 0, 1, 'N', 0),
(47, 'eee1', 0, '', 47, 1, 'R', 0),
(48, 'eee2', 0, '', 47, 1, 'N', 0),
(49, 'eeee3', 0, '', 47, 1, 'N', 0),
(50, 'estandar 1', 0, '', 48, 1, 'N', 0),
(51, 'estandar 2', 0, '', 48, 1, 'N', 0),
(52, 'Buen clima institucional', 0, '', 49, 1, 'R', 18),
(53, 'Estandar A', 0, '', 50, 1, 'N', 0),
(54, 'Estándar alternativa', 0, '', 51, 1, 'N', 0),
(55, 'Estándar autocompletar', 0, '', 52, 1, 'N', 0),
(56, 'ffffff', 0, '', 56, 1, 'N', 0),
(57, 'est 1', 0, '', 58, 1, 'N', 0),
(58, 'Buen clima institucional', 0, '', 59, 1, 'R', 18),
(59, 'Est alternativa', 0, '', 60, 1, 'N', 0),
(60, 'Est Autocompletar', 0, '', 61, 1, 'N', 0),
(61, 'Est 1', 0, '', 62, 1, 'R', 0),
(62, 'Tutoría', 0, '', 63, 1, 'R', 25),
(63, 'Servicios de atención complementaria', 0, '', 63, 1, 'R', 26),
(65, 'Trabajo conjunto con las familias', 0, '', 64, 1, 'R', 20),
(66, 'Monitoreo y evaluación del desempeño de niños y adolescentes', 0, '', 64, 1, 'R', 24),
(68, 'Est alternativa', 0, '', 65, 1, 'R', 0),
(69, 'Est autocompletar', 0, '', 66, 1, 'R', 0),
(70, 'Atención Docente', 0, '', 67, 1, 'N', 0),
(71, 'Atención Administrativo', 0, '', 67, 1, 'N', 0),
(72, 'Atención Alumno', 0, '', 67, 1, 'N', 0),
(73, 'Variedad de productos', 0, '', 68, 1, 'N', 0),
(74, 'Rapidez de la atención', 0, '', 68, 1, 'N', 0),
(75, 'Envase de los productos', 0, '', 69, 1, 'N', 0),
(77, 'Proyecto Educativo Institucional (PEI): Pertinente y orientado', 0, '', 70, 1, 'R', 1),
(78, 'Proyecto Curricular Institucional (PCI): Coherente, diversificado y orientador de los procesos pedagógicos.', 0, '', 70, 1, 'R', 2),
(79, 'Liderazgo pedagógico: El equipo directivo que ejerce liderazgo pedagógico colaborativo, manteniendo una visión común centrada en la mejora continua, el desarrollo integral y los aprendizajes de los niños y adolescentes ', 0, '', 70, 1, 'R', 3),
(80, 'bbbddd', 0, '', 0, 1, 'N', 0),
(81, 'esssttttt1', 0, '', 92, 1, 'N', 0),
(82, 'abc', 0, '', 93, 1, 'N', 0),
(83, 'aaaaaaaaaaaaaaaa', 0, '', 95, 1, 'N', 0),
(84, 'ssssssssssseeeeeeee', 0, '', 96, 1, 'N', 0),
(460, 'dsadsadsadsa', 0, '', 395, 1, 'N', 0),
(461, 'assssss', 0, '', 397, 1, 'N', 0),
(462, 'aaaaaa', 0, '', 398, 1, 'N', 0),
(463, 'aaaaa', 0, '', 399, 1, 'N', 0),
(464, 'bbbbb', 0, '', 400, 1, 'N', 0),
(465, 'qqqqq', 0, '', 401, 1, 'N', 0),
(466, 'ffffffff', 0, '', 402, 1, 'N', 0),
(467, 'eeeeee', 0, '', 403, 1, 'N', 0),
(468, 'qqqqq', 0, '', 404, 1, 'N', 0),
(469, 'dasdsadsadas', 0, '', 405, 1, 'N', 0),
(470, 'ddddddd', 0, '', 406, 1, 'N', 0),
(471, 'gfdgfdgfd', 0, '', 407, 1, 'N', 0),
(472, 'GGGGG', 0, '', 408, 1, 'N', 0),
(473, 'eeeee', 0, '', 409, 1, 'N', 0),
(474, 'dsadsadsa', 0, '', 410, 1, 'N', 0),
(475, 'sssss', 0, '', 411, 1, 'N', 0),
(476, 'qqqqq', 0, '', 412, 1, 'N', 0),
(477, 'qqqqq', 0, '', 413, 1, 'N', 0),
(478, 'aaaaaa', 0, '', 414, 1, 'N', 0),
(479, 'wwwww', 0, '', 415, 1, 'N', 0),
(480, 'aaaaaa', 0, '', 416, 1, 'N', 0),
(481, 'eeeeeww', 0, '', 417, 1, 'N', 0),
(482, 'rrrrrr', 0, '', 418, 1, 'N', 0),
(483, 'est editado', 0, '', 419, 1, 'N', 0),
(484, 'aaaaa', 0, '', 420, 1, 'N', 0),
(485, 'ccccccc', 0, '', 420, 1, 'N', 0),
(486, 'sssss', 0, '', 421, 1, 'N', 0),
(487, 'gggggg', 0, '', 421, 1, 'N', 0),
(488, 'dddddd', 0, '', 422, 1, 'N', 0),
(489, 'dddd', 0, '', 422, 1, 'N', 0),
(490, 'ddddd', 0, '', 423, 1, 'N', 0),
(491, 'eeeee', 0, '', 424, 1, 'N', 0),
(492, 'eeeee', 0, '', 425, 1, 'N', 0),
(493, 'aaaaaa', 0, '', 426, 1, 'N', 0),
(494, 'aaaaaa', 0, '', 427, 1, 'N', 0),
(495, 'dsasadsa', 0, '', 428, 1, 'N', 0),
(496, 'dsadsad', 0, '', 429, 1, 'N', 0),
(497, 'dasdsada', 0, '', 430, 1, 'N', 0),
(498, 'eeeeee', 0, '', 431, 1, 'N', 0),
(499, 'dasdsadas', 0, '', 432, 1, 'N', 0),
(500, 'dasdsadsa', 0, '', 433, 1, 'N', 0),
(501, 'ddddd', 0, '', 434, 1, 'N', 0),
(502, 'dsdsadsa', 0, '', 435, 1, 'N', 0),
(503, 'aaaaa', 0, '', 436, 1, 'N', 0),
(504, 'ffdfsd', 0, '', 436, 1, 'N', 0),
(505, 'aaaa', 0, '', 437, 1, 'N', 0),
(506, 'aaaa', 0, '', 438, 1, 'N', 0),
(507, 'ssssss', 0, '', 439, 1, 'N', 0),
(508, 'dsadsdsa', 0, '', 439, 1, 'N', 0),
(509, 'ddddd', 0, '', 440, 1, 'N', 0),
(510, 'qqqqq', 0, '', 441, 1, 'N', 0),
(511, 'sssss', 0, '', 442, 1, 'N', 0),
(512, 'ddddd', 0, '', 443, 1, 'N', 0),
(513, 'ssss', 0, '', 444, 1, 'N', 0),
(514, 'eeeee', 0, '', 444, 1, 'N', 0),
(515, 'dddd', 0, '', 445, 1, 'N', 0),
(517, 'eeeeeee', 0, '', 446, 1, 'N', 0),
(518, 'ddddd', 0, '', 446, 1, 'N', 0),
(519, 'ddddd', 0, '', 447, 1, 'N', 0),
(520, 'ddddd', 0, '', 448, 1, 'N', 0),
(522, 'qqqqq', 0, '', 449, 1, 'N', 0),
(523, 'eeeeee', 0, '', 449, 1, 'N', 0),
(524, 'eeeeee', 0, '', 452, 1, 'N', 0),
(525, 'ddddd', 0, '', 454, 1, 'N', 0),
(526, 'ddddd', 0, '', 455, 1, 'N', 0),
(527, 'dsds', 0, '', 456, 1, 'N', 0),
(528, 'dasdsadsa', 0, '', 457, 1, 'N', 0),
(529, 'eeee', 0, '', 458, 1, 'N', 0),
(530, 'rrrrr', 0, '', 459, 1, 'N', 0),
(532, 'fdfdsfdsfd', 0, '', 460, 1, 'N', 0),
(533, 'rrrrr', 0, '', 461, 1, 'N', 0),
(534, 'est A_editada', 0, '', 462, 1, 'N', 0),
(535, 'est B', 0, '', 462, 1, 'N', 0),
(537, 'aaaa', 0, '', 464, 1, 'N', 0),
(538, 'eeee', 0, '', 465, 1, 'N', 0),
(539, 'ssss', 0, '', 466, 1, 'N', 0),
(540, 'gfgfdgfd', 0, '', 470, 1, 'N', 0),
(541, 'ddddd', 0, '', 471, 1, 'N', 0),
(543, 'Proyecto Educativo Institucional (PEI): Pertinente y orientado', 0, '', 472, 1, 'R', 1),
(544, 'Proyecto Curricular Institucional (PCI): Coherente, diversificado y orientador de los procesos pedagógicos.', 0, '', 472, 1, 'R', 2),
(545, 'Liderazgo pedagógico: El equipo directivo que ejerce liderazgo pedagógico colaborativo, manteniendo una visión común centrada en la mejora continua, el desarrollo integral y los aprendizajes de los niños y adolescentes ', 0, '', 472, 1, 'R', 3),
(546, 'ssss', 0, '', 473, 1, 'R', 539),
(547, 'est aa', 0, '', 474, 1, 'R', 536),
(548, '111', 0, '', 475, 1, 'N', 0),
(549, 'aaaaa', 0, '', 476, 1, 'N', 0),
(550, 'aaaaa', 0, '', 477, 1, 'R', 549),
(551, 'aaa_1', 0, '', 478, 1, 'N', 0),
(552, 'bbbb_1', 0, '', 478, 1, 'N', 0),
(553, 'Información para la toma de decisiones', 0, '', 479, 1, 'R', 17),
(554, 'Buen clima institucional', 0, '', 480, 1, 'R', 18),
(555, 'Información para la toma de decisiones', 0, '', 481, 1, 'R', 17),
(556, 'Información para la toma de decisiones', 0, '', 482, 1, 'R', 17),
(557, 'Trabajo conjunto con las familias', 0, '', 483, 1, 'R', 20),
(558, 'Monitoreo y evaluación del desempeño de niños y adolescentes', 0, '', 483, 1, 'R', 24),
(560, 'Información para la toma de decisiones', 0, '', 484, 1, 'R', 17),
(561, 'est nuevo', 0, '', 484, 1, 'N', 0),
(562, 'abcd', 0, '', 485, 1, 'N', 0),
(563, 'abcd', 0, '', 486, 1, 'N', 0),
(564, 'est a', 0, '', 487, 1, 'N', 0),
(565, 'Buen clima institucional', 0, '', 488, 1, 'R', 18),
(566, 'Logro de competencias', 0, '', 489, 1, 'R', 30),
(567, 'Seguimiento del egresado', 0, '', 489, 1, 'R', 31),
(569, 'Proyecto Educativo Institucional (PEI): Pertinente y orientado', 0, '', 490, 1, 'R', 1),
(570, 'Proyecto Curricular Institucional (PCI): Coherente, diversificado y orientador de los procesos pedagógicos.', 0, '', 490, 1, 'R', 2),
(571, 'Liderazgo pedagógico: El equipo directivo que ejerce liderazgo pedagógico colaborativo, manteniendo una visión común centrada en la mejora continua, el desarrollo integral y los aprendizajes de los niños y adolescentes ', 0, '', 490, 1, 'R', 3),
(572, 'Trabajo conjunto con las familias', 0, '', 491, 1, 'R', 20),
(573, 'Monitoreo y evaluación del desempeño de niños y adolescentes', 0, '', 491, 1, 'R', 24),
(574, '112233', 0, '', 504, 1, 'N', 0),
(576, 'tgfdgd', 0, '', 504, 1, 'N', 0),
(577, 'dimension A', 0, '', 506, 1, 'N', 0),
(578, 'mmm', 0, '', 517, 1, 'N', 0),
(579, 'nnnbbbb', 0, '', 518, 1, 'N', 0),
(580, 'mn', 0, '', 519, 1, 'N', 0),
(582, 'dddffff', 0, '', 522, 1, 'N', 0),
(583, 'dddssss', 0, '', 531, 1, 'N', 0),
(584, 'dddsaaa', 0, '', 533, 1, 'N', 0),
(585, 'sssdddddd', 0, '', 545, 1, 'N', 0),
(586, 'ddsdsadsad', 0, '', 546, 1, 'N', 0),
(587, 'nulo', 0, '', 550, 1, 'N', 0),
(588, 'nulo', 0, '', 551, 1, 'N', 0),
(589, 'nulo', 0, '', 552, 1, 'N', 0),
(590, 'nulo', 0, '', 554, 1, 'N', 0),
(591, 'nulo', 0, '', 555, 1, 'N', 0),
(592, 'nulo', 0, '', 556, 1, 'N', 0),
(593, 'nulo', 0, '', 557, 1, 'N', 0),
(594, 'nulo', 0, '', 558, 1, 'N', 0),
(595, 'nulo', 0, '', 559, 1, 'N', 0),
(596, 'nulo', 0, '', 560, 1, 'N', 0),
(597, 'nulo', 0, '', 561, 1, 'N', 0),
(598, 'nulo', 0, '', 562, 1, 'N', 0),
(599, 'nulo', 0, '', 563, 1, 'N', 0),
(600, 'nulo', 0, '', 564, 1, 'N', 0),
(601, 'nulo', 0, '', 565, 1, 'N', 0),
(602, 'nulo', 0, '', 566, 1, 'N', 0),
(603, 'nulo', 0, '', 567, 1, 'N', 0),
(605, 'nulo', 0, '', 568, 1, 'N', 0),
(607, 'Conocimiento y sensibilización: cuestionarse la realidad y ser conscientes de los conceptos y valores a partir de los que se construye la misma', 0, '', 569, 1, 'N', 0),
(608, 'Juicio crítico: analizar críticamente los juicios propios y ajenos sobre la realidad, y ser conscientes de las consecuencias e implicaciones de estos.', 0, '', 569, 1, 'N', 0),
(609, 'Comportamientos, decisiones coherentes con los valores anteriores: mostrar y argumentar la pertenencia de los comportamientos y juicios que se emiten, fundamentados con conceptos éticos y deontológicos.', 0, '', 569, 1, 'N', 0),
(611, 'Capacidad de asimilar los conocimientos aportados por el profesor', 0, '', 570, 1, 'N', 0),
(612, 'Capacidad de integrar, procesar y ampliar los conocimientos en el marco de aplicación, con toma de decisiones sencillas', 0, '', 570, 1, 'N', 0),
(613, 'Capacidad de uso estratégico de los conocimientos adquiridos, con toma de decisiones complejas', 0, '', 570, 1, 'N', 0),
(614, 'responsabilidad', 0, '', 570, 1, 'N', 0),
(616, 'Participar y colaborar activamente en las tareas del equipo, y fomentar la confianza, la cordialidad y la orientación en el trabajo conjunto.', 0, '', 571, 1, 'N', 0),
(617, 'Contribuir a la consolidación y al desarrollo del equipo, favoreciendo la comunicación, la distribución equilibrada de tareas, el clima interno y la cohesión.', 0, '', 571, 1, 'N', 0),
(618, 'Dirigir grupos de trabajo, asegurando la interacción de los miembros y su orientación hacia un elevado rendimiento.', 0, '', 571, 1, 'N', 0),
(620, 'Generar nuevas ideas para problemas que se le plantean externamente.', 0, '', 572, 1, 'N', 0),
(621, 'Generar nuevas ideas para problemas que se le plantean y transmitirlas adecuadamente al grupo.', 0, '', 572, 1, 'N', 0),
(624, 'Diseñar un proyecto de trabajo sin llegar a su ejecución.', 0, '', 573, 1, 'N', 0),
(632, 'Entender la necesidad de valorar las consecuencias de las actuaciones profesionales en cuanto a repercusión social, ambiental y económica, y actuar consecuentemente.', 0, '', 575, 1, 'N', 0),
(635, 'nulo', 0, '', 576, 1, 'N', 0),
(636, 'Comprensión: Dominar estrategias de selección de la información relevante, de organizar esta información orientada a la finalidad que se desee conseguir e integrarla en los conocimientos ya adquiridos.', 0, '', 576, 1, 'N', 0),
(640, 'Comprensión Dominar estrategias de selección de la información relevante, de organizar esta información orientada a la finalidad que se desee conseguir e integrarla en los conocimientos ya adquiridos.', 0, '', 577, 1, 'N', 0),
(643, 'nulo', 0, '', 578, 1, 'N', 0),
(645, 'nulo', 0, '', 579, 1, 'N', 0),
(646, 'nulo', 0, '', 580, 1, 'N', 0),
(648, 'Cuestionarse la realidad y ser conscientes de los conceptos y valores a partir de los que se construye la misma.', 0, '', 581, 1, 'N', 0),
(649, 'Analizar críticamente los juicios propios y ajenos sobre la realidad, y ser conscientes de las consecuencias e implicaciones de estos.', 0, '', 581, 1, 'N', 0),
(650, 'Mostrar y argumentar la pertenencia de los comportamientos y juicios que se emiten, fundamentados con conceptos éticos y deontológicos.', 0, '', 581, 1, 'N', 0),
(651, 'nulo', 0, '', 582, 1, 'N', 0),
(653, 'Tiene la capacidad de asimilar los conocimientos aportados por el profesor?', 0, '', 583, 1, 'N', 0),
(654, 'Tiene la capacidad de integrar, procesar y ampliar los conocimientos en el marco de aplicación, con toma de decisiones sencillas', 0, '', 583, 1, 'N', 0),
(655, 'Tiene la capacidad de uso estratégico de los conocimientos adquiridos, con toma de decisiones complejas.', 0, '', 583, 1, 'N', 0),
(656, 'Tiene responsabilidad', 0, '', 583, 1, 'N', 0),
(658, 'Participa y colabora activamente en las tareas del equipo, fomenta la confianza, la cordialidad y la orientación en el trabajo conjunto.', 0, '', 584, 1, 'N', 0),
(659, 'Contribuye a la consolidación y al desarrollo del equipo, favoreciendo la comunicación, la distribución equilibrada de tareas, el clima interno y la cohesión.', 0, '', 584, 1, 'N', 0),
(660, 'Dirige grupos de trabajo, asegurando la interacción de los miembros y su orientación hacia un elevado rendimiento.', 0, '', 584, 1, 'N', 0),
(662, 'Genera nuevas ideas para problemas que se le plantean externamente.', 0, '', 585, 1, 'N', 0),
(663, 'Genera nuevas ideas para problemas que se le plantean y las transmite adecuadamente al grupo.', 0, '', 585, 1, 'N', 0),
(664, 'Genera ideas innovadoras para solucionar situaciones que transcienden su entorno próximo.', 0, '', 585, 1, 'N', 0),
(666, 'Diseña un proyecto de trabajo sin llegar a su ejecución.', 0, '', 586, 1, 'N', 0),
(667, 'Diseña y planifica un proyecto en colaboración con otras personas sobre un problema dado (sin ejecutarlo).', 0, '', 586, 1, 'N', 0),
(668, 'Gestiona globalmente proyectos, desde el diseño a la ejecución, contemplando procesos de seguimiento, evaluación y proyección.', 0, '', 586, 1, 'N', 0),
(670, 'Tiene visión de la realidad, analizando aspectos positivos y negativos y proponiendo nuevos procedimientos por iniciativa propia y con actitud de liderazgo.', 0, '', 587, 1, 'N', 0),
(671, 'Busca y propone nuevos procedimientos y soluciones a un problema dado, implicando a los demás, con visión de futuro y actitud de liderazgo.', 0, '', 587, 1, 'N', 0),
(672, 'Diseña y aplica procesos innovadores, con estrategias a medio y largo plazo, proponiendo proyectos globales que afecten a un amplio sector social.', 0, '', 587, 1, 'N', 0),
(674, 'Entender la necesidad de valorar las consecuencias de las actuaciones profesionales en cuanto a repercusión social, ambiental y económica, y actuar consecuentemente.', 0, '', 588, 1, 'N', 0),
(675, 'Diseñar, organizar y aplicar actuaciones profesionales específicas respetuosas con el entorno social, económico y ambiental.', 0, '', 588, 1, 'N', 0),
(676, 'Coordinar y evaluar actuaciones integrales en el ámbito profesional, respetuosas con el entorno social, económico y ambiental.', 0, '', 588, 1, 'N', 0),
(678, 'Comprensión: Dominar estrategias de selección de la información relevante, de organizar esta información orientada a la finalidad que se desee conseguir e integrarla en los conocimientos ya adquiridos.', 0, '', 589, 1, 'N', 0),
(679, 'Comunicación: Analizar la información para seleccionar las ideas que hay que comunicar, preparar un discurso coherente y decidir el mejor contexto comunicativo para comunicar tales ideas a la audiencia.', 0, '', 589, 1, 'N', 0),
(680, 'Expresión: Dominar los aspectos no verbales que contribuyen a la construcción del sentido y a la buena transmisión de la información, así como ser capaz de construir textos escritos de cualquier género y tipología con estilo propio y con profusión y riqueza de recursos lingüísticos.', 0, '', 589, 1, 'N', 0),
(682, 'Comprensión: Dominar estrategias de selección de la información relevante, de organizar esta información orientada a la finalidad que se desee conseguir e integrarla en los conocimientos ya adquiridos.', 0, '', 590, 1, 'N', 0),
(683, 'Comunicación: Analizar la información para seleccionar las ideas que hay que comunicar, preparar un discurso coherente y decidir el mejor contexto comunicativo para comunicar tales ideas a la audiencia.', 0, '', 590, 1, 'N', 0),
(684, 'Expresión: Dominar los aspectos no verbales que contribuyen a la construcción del sentido y a la buena transmisión de la información, así como ser capaz de construir textos escritos de cualquier género y tipología con estilo propio y con profusión y riqueza de recursos lingüísticos.', 0, '', 590, 1, 'R', 0),
(685, 'nulo', 0, '', 591, 1, 'N', 0),
(688, 'INVOLUCRA ACTIVAMENTE A LOS ESTUDIANTES EN EL PROCESO DE APRENDIZAJE?', 0, '', 592, 1, 'N', 0),
(689, 'MAXIMIZA EL TIEMPO DEDICADO AL APRENDIZAJE?', 0, '', 592, 1, 'N', 0),
(690, 'PROMUEVE EL RAZONAMIENTO, LA CREATIVIDAD Y/O EL PENSAMIENTO CRITICO?', 0, '', 592, 1, 'N', 0),
(691, 'EVALUA EL PROGRESO DE LOS APRENDIZAJES PARA RETRO-ALIMENTAR A LOS ESTUDIANTES Y ADECUAR SU ENSEÑANZA?', 0, '', 592, 1, 'N', 0),
(692, 'PROPICIA UN AMBIENTE DE RESPETO Y PROXIMIDAD?', 0, '', 592, 1, 'N', 0),
(693, 'REGULA POSITIVAMENTE EL COMPORTAMIENTO DE LOS ESTUDIANTES?', 0, '', 592, 1, 'N', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rub_pregunta`
--

CREATE TABLE `rub_pregunta` (
  `id_pregunta` int(11) NOT NULL,
  `nombre` text NOT NULL,
  `otros_datos` text COMMENT 'Usado para guardar ALTERNATIVAS',
  `id_estandar` int(11) NOT NULL,
  `tipo` varchar(1) NOT NULL,
  `activo` tinyint(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `rub_pregunta`
--

INSERT INTO `rub_pregunta` (`id_pregunta`, `nombre`, `otros_datos`, `id_estandar`, `tipo`, `activo`) VALUES
(1, 'El PCI se basa en lineamientos pedagógicos, técnicos y de política educativa nacional, en los enfoques transversales del PEI. el juego y el trabajo planificado con las familias y comunidad a favor del desarrollo integral y los aprendizajes de los niños', NULL, 2, '', 1),
(2, 'El PCI contextualizada en forma participativa el currículo nacional/regional, evidenciando altas expectativas de desarrollo y desempeño de los niños en todas las áreas.', NULL, 2, '', 1),
(3, 'El PCI define y establece prioridades para la integración de las áreas curriculares en el diseño, implementación y evaluación de las intervenciones pedagógicos y las adaptaciones para atender a todos los niños', NULL, 2, '', 1),
(4, 'El PCI orienta la planificación, programación, implementación y evaluación de los procesos del currículo a favor de la intervención pedagógica y el desarrollo integral de todos y cada uno de los niños.', NULL, 2, '', 1),
(5, 'EL PEI considera las características y demandas sociales, económico – productivas, lingüísticas, culturales, geográficas y medio ambientales de todos los estudiantes, sus familias y su comunidad.', '{"alternativas":{"a":"aaa","b":"bbbb"}}', 1, '', 1),
(6, 'El PEI responde al perfil de egreso del estudiante y los enfoques transversales vigentes establecidos en el currículo nacional', NULL, 1, 'N', 1),
(7, 'El PEI es conocido e incorporado en el accionar de todos los miembros de la comunidad educativa.', NULL, 1, '', 1),
(15, 'El equipo directivo gestiona, con la colaboración de la comunidad educativa, una organización dinámica centrada en los aprendizajes y la formación integral de todos los estudiantes.', NULL, 3, '', 1),
(16, 'El equipo directivo trabaja de manera colaborativa y transparente, generando compromiso entre los miembros de la comunidad educativa para el logro de los objetivos institucionales', NULL, 3, '', 1),
(17, 'La comunidad educativa asume sus roles y funciones y los articula en acciones planificadas para mantener la visión común y la orientación hacia la mejora continua.', NULL, 3, '', 1),
(18, 'La institución educativa gestiona información relevante para el logro de los objetivos institucionales y la mejora de los aprendizajes.', NULL, 17, '', 1),
(19, 'La institución educativa analiza y hace uso de la información, generada al interior de la institución y de fuentes externas, para implementar planes de mejora que contribuyan  al logro de los objetivos institucionales y la mejora de los aprendizajes.', NULL, 17, '', 1),
(20, 'La institución educativa promueve la comunicación efectiva, el trato respetuoso, la participación activa, la valoración y el reconocimiento de todos los miembros de la comunidad educativa, generando un ambiente propicio para el logro de los objetivos institucionales.', NULL, 18, '', 1),
(21, 'La institución educativa implementa mecanismos para la prevención y manejo de conflictos y situaciones de violencia que afectan a los estudiantes.', NULL, 18, '', 1),
(22, 'La institución educativa el diagnóstico de las potencialidades y necesidades de formación continua, pedagógica y disciplinar, del equipo docente considerado la información del monitoreo en el aula y los resultados de evaluaciones de los aprendizajes, para atender adecuadamente a todos los estudiantes.', NULL, 19, '', 1),
(23, 'La institución educativa gestiona el fortalecimiento de competencias docentes que incluyen el dominio de los contenidos disciplinares y desarrollo de las capacidades pedagógicas tomando en cuenta el diagnostico.', NULL, 19, '', 1),
(24, 'La institución educativa acompaña a los docentes y monitores los procesos de enseñanza y aprendizaje para asegurar el logro de competencias, conocimientos y la formación integral de todos los estudiantes', NULL, 19, '', 1),
(25, 'La institución educativa estimula, facilita y apoya el intercambio de experiencias pedagógicas y de trabajo colaborativo del equipo docente.', NULL, 19, '', 1),
(26, 'La institución educativa estimula, facilita y apoya la innovación pedagógica en el equipo docente y directivo.', NULL, 19, '', 1),
(27, 'La programación curricular recoge y articula las competencias del currículo diversificado (PCI) en cada grado y área.', NULL, 21, '', 1),
(28, 'La programación curricular considera actividades y estrategias de enseñanza y aprendizaje en concordancia a las orientaciones del PCI basadas en los enfoques transversales vigentes.', NULL, 21, '', 1),
(29, 'La programación curricular responde a las características, necesidades e intereses de los estudiantes del grado/aula atendiendo, y precisa acciones de apoyo para los que requieren de intervenciones específicas.', NULL, 21, '', 1),
(30, 'La programación curricular organiza y articula coherentemente las competencias a lograr, las estrategias pedagógicas, los recursos y evaluación. ', NULL, 21, '', 1),
(31, 'La programación anual, unidades didácticas y sesiones de aprendizaje se articulan coherentemente y orientan proceso de enseñanza y aprendizaje.', NULL, 21, '', 1),
(32, 'La programación curricular se reajusta en base a la evaluación de su implementación, del logro de las competencias y la formación integral de los estudiantes', NULL, 21, '', 1),
(33, 'Las estrategias pedagógicas implementadas son coherentes con las competencias a lograr, se basan en la programación curricular y se ajustan a las situaciones surgidas durante su implementación.', NULL, 22, '', 1),
(34, 'Las estrategias pedagógicas implementadas generan un clima de confianza y respeto en el aula, necesario para el desarrollo de competencias.', NULL, 22, '', 1),
(35, 'La institución educativa utiliza estándares de aprendizaje e implementa estrategias diferenciadas para evaluar y monitorear permanentemente el desempeño de los estudiantes.', NULL, 23, '', 1),
(36, 'La institución educativa implementa estrategias pedagógicas de evaluación para generar el compromiso de los estudiantes con su propio aprendizaje y la verificación de sus progresos.', NULL, 23, '', 1),
(37, 'La institución educativa adecua la practica pedagógicas, en base a las potencialidades y necesidades educativas identificadas en el monitoreo y evaluación, para atender a los estudiantes de acuerdo a sus niveles desempeño.', NULL, 23, '', 1),
(38, 'La institución educativa identifica, conjuntamente con los padres de familia o persona encargada del cuidado del estudiante las características y necesidades de los estudiantes para diseñar estrategias pedagógicas permanentes.', NULL, 20, '', 1),
(39, 'La institución educativa implementa estrategias para que los padres y madres de familia o persona encargada del cuidado del estudiante comprendan y acompañen los procesos de aprendizajes de los estudiantes y aspectos de la gestión escolar de acuerdo a su rol.', NULL, 20, '', 1),
(40, 'La institución educativa implementa proyectos que permitan a los estudiantes desarrollar sus competencias para contribuir al desarrollo de la comunidad.', NULL, 24, '', 1),
(41, 'La institución educativa implementa acción conjunta con las instituciones de las comunidades aprovechando los recursos humanos y materiales que contribuyan al logro de los aprendizajes y formación integral de los estudiantes', NULL, 24, '', 1),
(42, 'La institución educativa identifica necesidades socio – afectivas y cognitivas de los estudiantes y conflictos que los afecten, para su atención en la institución o derivación a servicios especializados', NULL, 25, '', 1),
(43, 'La institución educativa desarrolla sesiones de tutoría grupal en base a necesidades de orientación identificadas y/o conflictos que afecten el clima del aula.', NULL, 25, '', 1),
(44, 'La institución educativa implementa estrategias de acompañamiento, orientación y seguimiento individual a los estudiantes que lo requieran.', NULL, 25, '', 1),
(45, 'La institución educativa gestiona el acceso de los estudiantes a servicios especializados de atención complementaria, utilizando recursos profesionales de la comunidad educativa y/o local.', NULL, 26, '', 1),
(46, 'La institución educativa hace seguimiento al desempeño de los estudiantes que reciben servicios especializados de atención complementaria.', NULL, 26, '', 1),
(47, 'La institución educativa implementa acciones de mejora, cuidado y mantenimiento de la infraestructura, para facilitar los procesos de enseñanza y aprendizaje, garantizar la seguridad, salubridad y accesibilidad.', NULL, 27, '', 1),
(48, 'La institución educativa asegura el acceso oportuno de docentes y estudiante a diversos ambientes y espacios para el desarrollo de los procesos enseñanza y aprendizaje.', NULL, 27, '', 1),
(49, 'La institución educativa implementa acciones para la gestión de riesgos que permitan la prevención y respuesta ante situaciones de peligro, desastre y emergencia.', NULL, 27, '', 1),
(50, 'La institución educativa gestiona y/o desarrolla recursos innovadores para el aprendizaje, de acuerdo a las necesidades de sus estudiantes, privilegiando el uso de materiales propios de la comunidad, material reciclado y recursos tecnológicos.', NULL, 28, '', 1),
(51, 'La institución educativa implementa acciones de mejora, cuidado y mantenimiento del equipamiento y materiales necesarios para facilitar el logro de los aprendizajes y responder a las necesidades de toda la comunidad educativa.', NULL, 28, '', 1),
(52, 'La institución educativa gestiona el tiempo desarrollando las actividades institucionales con sentido formativo.', NULL, 28, '', 1),
(53, 'La institución educativa gestiona acciones de capacitación para el personal administrativo y/o de apoyo, de acuerdo a sus roles, funciones y necesidades. ', NULL, 29, '', 1),
(54, 'La institución educativa realiza el seguimiento a las mejoras en el desempeño del personal administrativo y/o de apoyo capacitando a través del monitoreo y acompañamiento.', NULL, 29, '', 1),
(55, 'La institución educativa establece y define mecanismos institucionalizados de evaluación del logro del perfil de egreso del estudiante tomando como referente el perfil de egreso del currículo nacional.', NULL, 30, '', 1),
(56, 'La institución educativa evalúa el logro de las competencias establecidas en los estándares de aprendizajes de las diferentes áreas del currículo nacional.', NULL, 30, '', 1),
(57, 'La institución educativa implementa estrategias para evaluar el nivel de satisfacción de los padres de familia y estudiantes en relación a la formación recibida.', NULL, 31, '', 1),
(58, 'La institución educativa implementa estrategias para el seguimiento de egresados al concluir en nivel educativo evaluado.', NULL, 31, '', 1),
(59, 'La institución educativa entrega el diploma de egresado con mención en un área técnica a los estudiantes que concluyeron satisfactoriamente el nivel secundario.', NULL, 31, '', 1),
(60, 'Pregunta 1', NULL, 35, '', 1),
(61, 'Pregunta 2', NULL, 35, '', 1),
(62, 'La limpieza de las aulas es constante', NULL, 37, '', 1),
(63, 'Existen los implementos adecuados en los laboratorios', NULL, 37, '', 1),
(64, 'Existe una limpieza constante en las aulas', NULL, 38, '', 1),
(65, 'Existe una organización adecuada en las aulas', NULL, 38, '', 1),
(66, '¿Cumplen con los requisitos del desayuno escolar?', '{"alternativas":{"a":"Siempre","b":"Casi siempre","c":"Regular","d":"Poco"},"correcta":"c"}', 100, '', 1),
(67, 'Qué opinas sobre los nuevos temas que se impartirán en clase de ciencias', NULL, 40, '', 1),
(76, 'pp1', '', 48, 'N', 1),
(77, 'ppp2', '', 48, 'N', 1),
(78, 'ppppppppp1', '', 47, 'N', 1),
(79, 'ppppp4', '', 49, 'N', 1),
(80, 'pregunta 1', '', 50, 'N', 1),
(81, 'pregunta 2', '', 50, 'N', 1),
(82, 'pregunta 3', '', 51, 'N', 1),
(83, 'La institución educativa promueve la comunicación efectiva, el trato respetuoso, la participación activa, la valoración y el reconocimiento de todos los miembros de la comunidad educativa, generando un ambiente propicio para el logro de los objetivos institucionales.', NULL, 52, 'R', 1),
(84, 'La institución educativa implementa mecanismos para la prevención y manejo de conflictos y situaciones de violencia que afectan a los estudiantes.', NULL, 52, 'R', 1),
(86, 'Pregunta 1', '', 53, 'N', 1),
(87, 'Pregunta 2', '', 53, 'N', 1),
(88, 'Pregunta alternativa', '{"alternativas":{"a":"Premisa A","b":"Premisa B","c":"Premisa C","d":"Premisa D"},"correcta":"d"}', 54, 'N', 1),
(89, 'Pregunta autcompletar', '', 55, 'N', 1),
(90, 'preg 1', '', 57, 'N', 1),
(91, 'preg 2', '', 57, 'N', 1),
(92, 'La institución educativa promueve la comunicación efectiva, el trato respetuoso, la participación activa, la valoración y el reconocimiento de todos los miembros de la comunidad educativa, generando un ambiente propicio para el logro de los objetivos institucionales.', NULL, 58, 'R', 1),
(93, 'La institución educativa implementa mecanismos para la prevención y manejo de conflictos y situaciones de violencia que afectan a los estudiantes.', NULL, 58, 'R', 1),
(95, 'Preg alternativa', '{"alternativas":{"a":"Premisa A","b":"Premisa B","c":"Premisa C","d":"Premisa D"},"correcta":"b"}', 59, 'N', 1),
(96, 'Preg Autocompletar', '', 60, 'N', 1),
(97, 'Preg 1', '', 61, 'N', 1),
(98, 'Preg 2', '', 61, 'N', 1),
(99, 'La institución educativa identifica necesidades socio – afectivas y cognitivas de los estudiantes y conflictos que los afecten, para su atención en la institución o derivación a servicios especializados', NULL, 62, 'R', 1),
(100, 'La institución educativa desarrolla sesiones de tutoría grupal en base a necesidades de orientación identificadas y/o conflictos que afecten el clima del aula.', NULL, 62, 'R', 1),
(101, 'La institución educativa implementa estrategias de acompañamiento, orientación y seguimiento individual a los estudiantes que lo requieran.', NULL, 62, 'R', 1),
(102, 'La institución educativa gestiona el acceso de los estudiantes a servicios especializados de atención complementaria, utilizando recursos profesionales de la comunidad educativa y/o local.', NULL, 63, 'R', 1),
(103, 'La institución educativa hace seguimiento al desempeño de los estudiantes que reciben servicios especializados de atención complementaria.', NULL, 63, 'R', 1),
(105, 'La institución educativa identifica, conjuntamente con los padres de familia o persona encargada del cuidado del estudiante las características y necesidades de los estudiantes para diseñar estrategias pedagógicas permanentes.', NULL, 65, 'R', 1),
(106, 'La institución educativa implementa estrategias para que los padres y madres de familia o persona encargada del cuidado del estudiante comprendan y acompañen los procesos de aprendizajes de los estudiantes y aspectos de la gestión escolar de acuerdo a su rol.', NULL, 65, 'R', 1),
(108, 'La institución educativa implementa proyectos que permitan a los estudiantes desarrollar sus competencias para contribuir al desarrollo de la comunidad.', NULL, 66, 'R', 1),
(109, 'La institución educativa implementa acción conjunta con las instituciones de las comunidades aprovechando los recursos humanos y materiales que contribuyan al logro de los aprendizajes y formación integral de los estudiantes', NULL, 66, 'R', 1),
(111, 'Preg alternativa', '{"alternativas":{"a":"Premisa A","b":"Premisa B","c":"Premisa C","d":"Premisa D"},"correcta":"b"}', 68, 'N', 1),
(112, 'Preg autocompletar', '', 69, 'N', 1),
(113, 'Recomendaría el servicio a la cafetería', '', 70, 'N', 1),
(114, 'Le satisface el servicio de cafetería', '', 71, 'N', 1),
(115, 'Usas frecuentemente el servicio de cafetería', '', 72, 'N', 1),
(116, 'Le satisface la variedad de productos', '', 73, 'N', 1),
(117, 'Debería tener más personal la cafetería', '', 74, 'N', 1),
(118, 'Te parece si el producto está envasado correctamente', '', 75, 'N', 1),
(119, 'EL PEI considera las características y demandas sociales, económico – productivas, lingüísticas, culturales, geográficas y medio ambientales de todos los estudiantes, sus familias y su comunidad.', NULL, 77, 'R', 1),
(120, ' El PEI responde al perfil de egreso del estudiante y los enfoques transversales vigentes establecidos en el currículo nacional', NULL, 77, 'R', 1),
(121, 'El PEI es conocido e incorporado en el accionar de todos los miembros de la comunidad educativa.', NULL, 77, 'R', 1),
(122, 'El PCI se basa en lineamientos pedagógicos, técnicos y de política educativa nacional, en los enfoques transversales del PEI. el juego y el trabajo planificado con las familias y comunidad a favor del desarrollo integral y los aprendizajes de los niños', NULL, 78, 'R', 1),
(123, 'El PCI contextualizada en forma participativa el currículo nacional/regional, evidenciando altas expectativas de desarrollo y desempeño de los niños en todas las áreas.', NULL, 78, 'R', 1),
(124, 'El PCI define y establece prioridades para la integración de las áreas curriculares en el diseño, implementación y evaluación de las intervenciones pedagógicos y las adaptaciones para atender a todos los niños', NULL, 78, 'R', 1),
(125, 'El PCI orienta la planificación, programación, implementación y evaluación de los procesos del currículo a favor de la intervención pedagógica y el desarrollo integral de todos y cada uno de los niños.', NULL, 78, 'R', 1),
(129, 'El equipo directivo gestiona, con la colaboración de la comunidad educativa, una organización dinámica centrada en los aprendizajes y la formación integral de todos los estudiantes.', NULL, 79, 'R', 1),
(130, 'El equipo directivo trabaja de manera colaborativa y transparente, generando compromiso entre los miembros de la comunidad educativa para el logro de los objetivos institucionales', NULL, 79, 'R', 1),
(131, 'La comunidad educativa asume sus roles y funciones y los articula en acciones planificadas para mantener la visión común y la orientación hacia la mejora continua.', NULL, 79, 'R', 1),
(132, 'reeee', '', 97, 'N', 0),
(133, 'preg1', '', 98, 'N', 0),
(134, 'dddfefefe', '', 99, 'N', 0),
(135, 'tttyyyyyyy', '', 100, 'N', 0),
(136, 'avbbddd', '', 101, 'N', 0),
(137, 'weeeeeeeeeeeee', '', 102, 'N', 0),
(138, 'rrrrrrrrrrrrrrr', '', 103, 'N', 0),
(139, 'tttttttttttttttt', '', 104, 'N', 0),
(140, 'qqqqqqq', '', 105, 'N', 0),
(141, 'ffffffffffffff', '', 106, 'N', 0),
(142, 'gffffff', '', 106, 'N', 0),
(143, 'rtrtertertertre', '', 106, 'N', 0),
(842, 'aaaaaaa', '', 460, 'N', 0),
(843, 'dddddd', '{"alternativas":{"a":"aaa","b":"bbb","c":"ccc"},"correcta":"c"}', 461, 'N', 0),
(844, 'eeeeeee', '', 462, 'N', 0),
(845, 'weeeee', '{"alternativas":{"a":"aaa","b":"bbb","c":"ccc","d":"ddd"},"correcta":"b"}', 463, 'N', 0),
(846, 'vvvvv', '', 464, 'N', 0),
(847, 'dddddd', '{"alternativas":{"a":"aaa","b":"bbb","c":"ccc"},"correcta":"c"}', 465, 'N', 0),
(848, '1111', '{"alternativas":{"a":"aaa","b":"bbb","c":"ccc"},"correcta":"a"}', 466, 'N', 0),
(849, '111111', '{"alternativas":{"a":"aaaa","b":"bbbb","c":"cccc"},"correcta":"c"}', 467, 'N', 0),
(850, 'eeeee', '{"alternativas":{"a":"aaa11","b":"bbb11","c":"ccc11","d":"ddd11"},"correcta":"d"}', 468, 'N', 0),
(851, 'ssssss', '{"alternativas":{"a":"eee","b":"rrrr","c":"sssss"},"correcta":"a"}', 468, 'N', 0),
(852, 'dsadsadsa', '', 469, 'N', 0),
(853, 'dsadsadsadsa', '', 469, 'N', 0),
(854, 'rrrrrr', '', 470, 'N', 0),
(855, 'ttttttt', '', 470, 'N', 0),
(856, 'gfdgfd', '', 471, 'N', 0),
(857, 'gdfgfd', '', 471, 'N', 0),
(858, 'RRRR', '', 472, 'N', 0),
(859, 'ttttt', '', 472, 'N', 0),
(860, 'rrrrr', '', 473, 'N', 0),
(861, 'eeeee', '', 473, 'N', 0),
(862, 'ddsda', '', 474, 'N', 0),
(863, 'eeee', '', 475, 'N', 0),
(864, 'fffff', '', 475, 'N', 0),
(865, 'vvvvv', '', 476, 'N', 0),
(866, 'eeeee', '', 476, 'N', 0),
(867, 'eeeeeee', '', 477, 'N', 0),
(868, 'dddddd', '', 478, 'N', 0),
(869, 'eeeee', '', 478, 'N', 0),
(870, 'rrrrrr', '', 478, 'N', 0),
(871, 'fffff', '', 479, 'N', 0),
(872, 'gggggg', '', 479, 'N', 0),
(873, 'wwwww', '', 480, 'N', 0),
(874, 'eeeee', '', 480, 'N', 0),
(875, 'wwww', '', 481, 'N', 0),
(876, 'fffffgg', '', 481, 'N', 0),
(877, 'wwwww', '{"alternativas":{"a":"aaa","b":"bbb","c":"ccc"},"correcta":"a"}', 482, 'N', 0),
(878, '1111', '', 483, 'N', 0),
(879, '2222', '', 483, 'N', 0),
(880, '3333', '', 483, 'N', 0),
(881, '11111', '', 484, 'N', 0),
(882, 'ddddddd', '', 485, 'N', 0),
(883, 'eeeeee', '', 484, 'N', 0),
(884, 'fffffff', '', 485, 'N', 0),
(885, 'eeeee', '{"alternativas":{"a":"aaaa","b":"bbb"}}', 486, 'N', 0),
(886, 'rrrrr', '{"alternativas":{"a":"111","b":"2222"}}', 486, 'N', 0),
(887, 'tttttt', '{"alternativas":{"a":"aaass","b":"ffgggg"}}', 486, 'N', 0),
(888, 'wwwwww', '{"alternativas":{"a":"444","b":"555"}}', 487, 'N', 0),
(889, 'ddsdsds', '', 488, 'N', 0),
(890, 'dsdssss', '{"alternativas":{"a":"aaa","b":"bbb"}}', 490, 'N', 0),
(891, 'dssss', '{"alternativas":{"a":"aaa111","b":"bbbb22"}}', 490, 'N', 0),
(892, 'fffff', '{"alternativas":{"a":"111","b":"222"},"correcta":"b"}', 491, 'N', 0),
(893, 'eeee', '{"alternativas":{"a":"333","b":"444"},"correcta":"a"}', 491, 'N', 0),
(894, 'eeee', '', 492, 'N', 0),
(895, 'ffffff', '', 492, 'N', 0),
(896, '11111', '', 493, 'N', 0),
(897, '2222', '', 493, 'N', 0),
(898, '33333', '', 493, 'N', 0),
(899, '111111', '{"alternativas":{"a":"aaaa","b":"bbbb"},"correcta":"a"}', 494, 'N', 0),
(900, '222222', '{"alternativas":{"a":"cccc","b":"ddd"},"correcta":"b"}', 494, 'N', 0),
(901, 'dsadsadsa', '', 495, 'N', 0),
(902, 'dasdsda', '', 496, 'N', 0),
(903, 'dsdsad', '', 496, 'N', 0),
(904, 'vvxcvcxvx', '', 497, 'N', 0),
(905, 'fdfdsfds', '', 497, 'N', 0),
(906, 'ggggggg', '', 498, 'N', 0),
(907, 'dsadsadas', '', 499, 'N', 0),
(908, 'dsadsadsa', '', 500, 'N', 0),
(909, 'eeeee', '{"alternativas":{"a":"aaa","b":"bbb"},"correcta":"a"}', 501, 'N', 0),
(910, 'dddddd', '{"alternativas":{"a":"ccc","b":"ddd"},"correcta":"b"}', 501, 'N', 0),
(911, 'aaaa', '', 502, 'N', 0),
(912, 'dsadsadas', '', 502, 'N', 0),
(913, 'fffff', '', 503, 'N', 0),
(914, 'eeeee', '', 503, 'N', 0),
(915, 'bbbbb', '{"alternativas":{"a":"dddd","b":"eeee"},"correcta":"a"}', 505, 'N', 0),
(916, '11111', '', 506, 'N', 0),
(917, '2222', '', 506, 'N', 0),
(918, 'ddddd', '', 507, 'N', 0),
(919, 'dsadsadsa', '', 507, 'N', 0),
(920, 'dasdsd', '', 508, 'N', 0),
(921, 'rrrr', '', 509, 'N', 0),
(922, 'ddddd', '', 510, 'N', 0),
(923, 'fffff', '', 510, 'N', 0),
(924, 'ddddd', '', 511, 'N', 0),
(925, 'fffff', '', 511, 'N', 0),
(926, 'ssssss', '', 512, 'N', 0),
(927, 'ddddd', '', 512, 'N', 0),
(929, 'fffff', '', 513, 'N', 0),
(930, 'fffff', '', 514, 'N', 0),
(931, 'wwww', '', 515, 'N', 0),
(933, 'fffff', '', 516, 'N', 0),
(935, 'ffffff', '', 517, 'N', 0),
(936, 'eeeee', '', 518, 'N', 0),
(937, 'eeee', '{"alternativas":{"a":"aaa","b":"bbb"},"correcta":"a"}', 519, 'N', 0),
(941, 'dsadasdsadsa', '{"alternativas":{"a":"dsa","b":"dsadsa"},"correcta":"a"}', 520, 'N', 0),
(942, 'dsadsadsa', '{"alternativas":{"a":"dsadsa","b":"dsadsadsa"}}', 521, 'N', 0),
(943, 'wwwww', '{"alternativas":{"a":"aaa","b":"bbb"}}', 522, 'N', 0),
(944, 'eeeee', '{"alternativas":{"a":"rrr","b":"eeee"},"correcta":"b"}', 522, 'N', 0),
(945, 'fffff', '{"alternativas":{"a":"wqq","b":"rrrr"},"correcta":"b"}', 523, 'N', 0),
(946, 'rrrr', '', 524, 'N', 0),
(947, 'wwwww', '', 524, 'N', 0),
(948, 'eeeee', '{"alternativas":{"a":"wwww","b":"qqqq"}}', 525, 'N', 0),
(949, 'wwwww', '{"alternativas":{"a":"ffff","b":"gggg"},"correcta":"b"}', 526, 'N', 0),
(950, 'dsadsadsa', '{"alternativas":{"a":"qqq","b":"wwwww"},"correcta":"a"}', 528, 'N', 0),
(951, 'fffff', '', 529, 'N', 0),
(954, 'gfdgfdgfd', '', 530, 'N', 0),
(956, 'preg 1_editada', '{"alternativas":{"a":"aa","b":"bb"},"correcta":"a"}', 534, 'N', 0),
(957, 'preg 2', '{"alternativas":{"a":"111|","b":"222"},"correcta":"b"}', 534, 'N', 0),
(958, 'preg 3', '{"alternativas":{"a":"1111","b":"2222"},"correcta":"b"}', 534, 'N', 0),
(959, 'preg 4', '{"alternativas":{"a":"ewq","b":"fsd"},"correcta":"a"}', 535, 'N', 0),
(960, 'preg 1', '{"alternativas":{"a":"AA","b":"BB"},"correcta":"a"}', 536, 'N', 0),
(961, 'preg 2', '{"alternativas":{"a":"cc","b":"dd"},"correcta":"a"}', 536, 'N', 0),
(962, '1111', '{"alternativas":{"a":"111","b":"22222"},"correcta":"b"}', 537, 'N', 0),
(963, '2222', '{"alternativas":{"a":"cccc11111","b":"dddd222222"},"correcta":"a"}', 537, 'N', 0),
(964, 'dddsssss', '', 538, 'N', 0),
(965, 'dddssss', '', 538, 'N', 0),
(966, 'dddd', '', 539, 'N', 0),
(967, 'eeee', '', 539, 'N', 0),
(968, 'gfdgfdgdf', '', 540, 'N', 0),
(969, 'ssss', '', 541, 'N', 0),
(970, 'gfdgfdgdf', '', 542, 'N', 0),
(971, 'EL PEI considera las características y demandas sociales, económico – productivas, lingüísticas, culturales, geográficas y medio ambientales de todos los estudiantes, sus familias y su comunidad.', NULL, 543, 'R', 1),
(972, ' El PEI responde al perfil de egreso del estudiante y los enfoques transversales vigentes establecidos en el currículo nacional', NULL, 543, 'R', 1),
(973, 'El PEI es conocido e incorporado en el accionar de todos los miembros de la comunidad educativa.', NULL, 543, 'R', 1),
(974, 'El PCI se basa en lineamientos pedagógicos, técnicos y de política educativa nacional, en los enfoques transversales del PEI. el juego y el trabajo planificado con las familias y comunidad a favor del desarrollo integral y los aprendizajes de los niños', NULL, 544, 'R', 1),
(975, 'El PCI contextualizada en forma participativa el currículo nacional/regional, evidenciando altas expectativas de desarrollo y desempeño de los niños en todas las áreas.', NULL, 544, 'R', 1),
(976, 'El PCI define y establece prioridades para la integración de las áreas curriculares en el diseño, implementación y evaluación de las intervenciones pedagógicos y las adaptaciones para atender a todos los niños', NULL, 544, 'R', 1),
(977, 'El PCI orienta la planificación, programación, implementación y evaluación de los procesos del currículo a favor de la intervención pedagógica y el desarrollo integral de todos y cada uno de los niños.', NULL, 544, 'R', 1),
(981, 'El equipo directivo gestiona, con la colaboración de la comunidad educativa, una organización dinámica centrada en los aprendizajes y la formación integral de todos los estudiantes.', NULL, 545, 'R', 1),
(982, 'El equipo directivo trabaja de manera colaborativa y transparente, generando compromiso entre los miembros de la comunidad educativa para el logro de los objetivos institucionales', NULL, 545, 'R', 1),
(983, 'La comunidad educativa asume sus roles y funciones y los articula en acciones planificadas para mantener la visión común y la orientación hacia la mejora continua.', NULL, 545, 'R', 1),
(984, 'dddd', '', 546, 'R', 1),
(985, 'eeee', '', 546, 'R', 1),
(987, 'preg 1', '{"alternativas":{"a":"AA","b":"BB"},"correcta":"a"}', 547, 'R', 1),
(988, 'preg 2', '{"alternativas":{"a":"cc","b":"dd"},"correcta":"a"}', 547, 'R', 1),
(990, '333', '{"alternativas":{"a":"111","b":"222","c":"3333"},"correcta":"b"}', 548, 'N', 0),
(991, 'bbbbb', '', 549, 'N', 0),
(992, 'bbbbb', '', 550, 'R', 1),
(993, 'preg 1', '', 551, 'N', 0),
(995, 'preg 2', '', 551, 'N', 0),
(996, 'preg 3', '', 552, 'N', 0),
(997, 'La institución educativa gestiona información relevante para el logro de los objetivos institucionales y la mejora de los aprendizajes.', NULL, 553, 'R', 1),
(998, 'La institución educativa analiza y hace uso de la información, generada al interior de la institución y de fuentes externas, para implementar planes de mejora que contribuyan  al logro de los objetivos institucionales y la mejora de los aprendizajes.', NULL, 553, 'R', 1),
(1000, 'La institución educativa promueve la comunicación efectiva, el trato respetuoso, la participación activa, la valoración y el reconocimiento de todos los miembros de la comunidad educativa, generando un ambiente propicio para el logro de los objetivos institucionales.', NULL, 554, 'R', 1),
(1001, 'La institución educativa implementa mecanismos para la prevención y manejo de conflictos y situaciones de violencia que afectan a los estudiantes.', NULL, 554, 'R', 1),
(1003, 'La institución educativa gestiona información relevante para el logro de los objetivos institucionales y la mejora de los aprendizajes.', NULL, 555, 'R', 1),
(1004, 'La institución educativa analiza y hace uso de la información, generada al interior de la institución y de fuentes externas, para implementar planes de mejora que contribuyan  al logro de los objetivos institucionales y la mejora de los aprendizajes.', NULL, 555, 'R', 1),
(1006, 'La institución educativa gestiona información relevante para el logro de los objetivos institucionales y la mejora de los aprendizajes.', NULL, 556, 'R', 1),
(1007, 'La institución educativa analiza y hace uso de la información, generada al interior de la institución y de fuentes externas, para implementar planes de mejora que contribuyan  al logro de los objetivos institucionales y la mejora de los aprendizajes.', NULL, 556, 'R', 1),
(1009, 'La institución educativa identifica, conjuntamente con los padres de familia o persona encargada del cuidado del estudiante las características y necesidades de los estudiantes para diseñar estrategias pedagógicas permanentes.', NULL, 557, 'R', 1),
(1010, 'La institución educativa implementa estrategias para que los padres y madres de familia o persona encargada del cuidado del estudiante comprendan y acompañen los procesos de aprendizajes de los estudiantes y aspectos de la gestión escolar de acuerdo a su rol.', NULL, 557, 'R', 1),
(1012, 'La institución educativa implementa proyectos que permitan a los estudiantes desarrollar sus competencias para contribuir al desarrollo de la comunidad.', NULL, 558, 'R', 1),
(1013, 'La institución educativa implementa acción conjunta con las instituciones de las comunidades aprovechando los recursos humanos y materiales que contribuyan al logro de los aprendizajes y formación integral de los estudiantes', NULL, 558, 'R', 1),
(1015, 'La institución educativa gestiona información relevante para el logro de los objetivos institucionales y la mejora de los aprendizajes.', NULL, 560, 'R', 1),
(1016, 'La institución educativa analiza y hace uso de la información, generada al interior de la institución y de fuentes externas, para implementar planes de mejora que contribuyan  al logro de los objetivos institucionales y la mejora de los aprendizajes.', NULL, 560, 'R', 1),
(1018, 'abc_editada', '', 560, 'N', 0),
(1019, 'as', '', 561, 'N', 0),
(1020, 'dew', '', 562, 'N', 0),
(1021, 'ssssss', '', 562, 'N', 0),
(1022, 'bbfff', '', 562, 'N', 0),
(1023, 'bbbbb', '', 561, 'N', 0),
(1024, '111', '{"alternativas":{"a":"premisa a","b":"premisa b","c":"premisa c"},"correcta":"a"}', 563, 'N', 0),
(1025, '222', '{"alternativas":{"a":"pre a","b":"pre b","c":"pre c"},"correcta":"c"}', 563, 'N', 0),
(1026, '111', '', 564, 'N', 0),
(1027, '222', '', 564, 'N', 0),
(1028, 'La institución educativa promueve la comunicación efectiva, el trato respetuoso, la participación activa, la valoración y el reconocimiento de todos los miembros de la comunidad educativa, generando un ambiente propicio para el logro de los objetivos institucionales.', NULL, 565, 'R', 1),
(1029, 'La institución educativa implementa mecanismos para la prevención y manejo de conflictos y situaciones de violencia que afectan a los estudiantes.', NULL, 565, 'R', 1),
(1031, 'La institución educativa establece y define mecanismos institucionalizados de evaluación del logro del perfil de egreso del estudiante tomando como referente el perfil de egreso del currículo nacional.', NULL, 566, 'R', 1),
(1032, 'La institución educativa evalúa el logro de las competencias establecidas en los estándares de aprendizajes de las diferentes áreas del currículo nacional.', NULL, 566, 'R', 1),
(1034, 'La institución educativa implementa estrategias para evaluar el nivel de satisfacción de los padres de familia y estudiantes en relación a la formación recibida.', NULL, 567, 'R', 1),
(1035, 'La institución educativa implementa estrategias para el seguimiento de egresados al concluir en nivel educativo evaluado.', NULL, 567, 'R', 1),
(1036, 'La institución educativa entrega el diploma de egresado con mención en un área técnica a los estudiantes que concluyeron satisfactoriamente el nivel secundario.', NULL, 567, 'R', 1),
(1037, 'EL PEI considera las características y demandas sociales, económico – productivas, lingüísticas, culturales, geográficas y medio ambientales de todos los estudiantes, sus familias y su comunidad.', NULL, 569, 'R', 1),
(1038, ' El PEI responde al perfil de egreso del estudiante y los enfoques transversales vigentes establecidos en el currículo nacional', NULL, 569, 'R', 1),
(1039, 'El PEI es conocido e incorporado en el accionar de todos los miembros de la comunidad educativa.', NULL, 569, 'R', 1),
(1040, 'El PCI se basa en lineamientos pedagógicos, técnicos y de política educativa nacional, en los enfoques transversales del PEI. el juego y el trabajo planificado con las familias y comunidad a favor del desarrollo integral y los aprendizajes de los niños', NULL, 570, 'R', 1),
(1041, 'El PCI contextualizada en forma participativa el currículo nacional/regional, evidenciando altas expectativas de desarrollo y desempeño de los niños en todas las áreas.', NULL, 570, 'R', 1),
(1042, 'El PCI define y establece prioridades para la integración de las áreas curriculares en el diseño, implementación y evaluación de las intervenciones pedagógicos y las adaptaciones para atender a todos los niños', NULL, 570, 'R', 1),
(1043, 'El PCI orienta la planificación, programación, implementación y evaluación de los procesos del currículo a favor de la intervención pedagógica y el desarrollo integral de todos y cada uno de los niños.', NULL, 570, 'R', 1),
(1047, 'El equipo directivo gestiona, con la colaboración de la comunidad educativa, una organización dinámica centrada en los aprendizajes y la formación integral de todos los estudiantes.', NULL, 571, 'R', 1),
(1048, 'El equipo directivo trabaja de manera colaborativa y transparente, generando compromiso entre los miembros de la comunidad educativa para el logro de los objetivos institucionales', NULL, 571, 'R', 1),
(1049, 'La comunidad educativa asume sus roles y funciones y los articula en acciones planificadas para mantener la visión común y la orientación hacia la mejora continua.', NULL, 571, 'R', 1),
(1050, 'La institución educativa identifica, conjuntamente con los padres de familia o persona encargada del cuidado del estudiante las características y necesidades de los estudiantes para diseñar estrategias pedagógicas permanentes.', NULL, 572, 'R', 1),
(1051, 'La institución educativa implementa estrategias para que los padres y madres de familia o persona encargada del cuidado del estudiante comprendan y acompañen los procesos de aprendizajes de los estudiantes y aspectos de la gestión escolar de acuerdo a su rol.', NULL, 572, 'R', 1),
(1053, 'La institución educativa implementa proyectos que permitan a los estudiantes desarrollar sus competencias para contribuir al desarrollo de la comunidad.', NULL, 573, 'R', 1),
(1054, 'La institución educativa implementa acción conjunta con las instituciones de las comunidades aprovechando los recursos humanos y materiales que contribuyan al logro de los aprendizajes y formación integral de los estudiantes', NULL, 573, 'R', 1),
(1055, 'aabbcc', '', 574, 'N', 0),
(1056, 'awe', '', 574, 'N', 0),
(1057, 'preg 1', '', 577, 'N', 0),
(1058, 'preg 2', '', 577, 'N', 0),
(1060, 'aaaeeee', '', 582, 'N', 0),
(1061, 'ddeeee', '', 583, 'N', 0),
(1062, 'dddsadsadsa', '', 584, 'N', 0),
(1063, 'wwweeeee', '', 585, 'N', 0),
(1064, 'ddsdsa', '', 585, 'N', 0),
(1065, 'sssfffffdd', '', 601, 'N', 0),
(1066, 'hghfghfg', '', 601, 'N', 0),
(1067, 'sssffff', '', 602, 'N', 0),
(1068, 'Demostró dominio de los contenidos que abordó la sesión de aprendizaje - enseñanza.', '', 605, 'N', 0),
(1069, 'Los contenidos fueron apropiados para complementar el proceso general de la formación educativa del estudiante.', '', 605, 'N', 0),
(1070, 'Utilizó diversas estrategias para el logro de los aprendizajes esperados.', '', 605, 'N', 0),
(1071, 'Las estrategias usadas fueron adecuadas, por el logro de aprendizaje.', '', 605, 'N', 0),
(1072, 'Las estrategias pedagógicas incorporadas durante la sesión fueron innovadoras y participativas. ', '', 605, 'N', 0),
(1073, 'El espacio utilizado fue apropiado por el desarrollo de todas las actividades desarrolladas en toda la sesión.', '', 605, 'N', 0),
(1074, 'Promovió en los estudiantes el trabajo académico individual y colaborativo en las actividades desarrolladas.', '', 605, 'N', 0),
(1075, 'Mantuvo una comunicación agradable con los estudiantes empleando un lenguaje apropiada en la temática abordada.', '', 605, 'N', 0),
(1076, 'Aprovecho de manera efectiva el tiempo establecido para el desarrollo de la sesión.', '', 605, 'N', 0),
(1077, 'Mantuvo una actitud positiva al inter relacionarse con sus estudiantes.', '', 605, 'N', 0),
(1078, 'Demostró disposición por el trabajo realizado en la sesión.', '', 605, 'N', 0),
(1079, 'Atendió las inquietudes y los problemas que se le presentaron a los estudiantes durante la sesión y contribuyo en una solución.', '', 605, 'N', 0),
(1080, 'Suministro nuevas ideas, sugerencias y/o propuestas en función del mejoramiento, del trabajo académico durante la sesión.', '', 605, 'N', 0),
(1081, 'Promovió correcciones correctivas para sus estudiantes con el objeto de solventar sus deficiencias.', '', 605, 'N', 0),
(1082, 'Valoro y respetó el trabajo desarrollado por los estudiantes.', '', 605, 'N', 0),
(1083, 'Fue receptivo antes los cambios que se promovieron los estudiantes para mejorar la calidad de la sesión de aprendizaje - enseñanza.', '', 605, 'N', 0),
(1084, 'Evaluó periódicamente el avance del grupo y el desarrollo individual. ', '', 605, 'N', 0),
(1085, 'Consciencia de las otras maneras de ver y percibir las cosas', '', 607, 'N', 0),
(1086, 'Aceptación crítica de nuevas perspectivas, aunque se cuestionen las propias', '', 607, 'N', 0),
(1087, 'Diferenciación entre hechos y opiniones o interpretaciones en las argumentaciones de los demás', '', 607, 'N', 0),
(1088, 'Reflexión sobre las consecuencias y efectos (implicaciones prácticas) que las decisiones y propuestas tienen sobre las personas', '', 607, 'N', 0),
(1089, 'Reconocimiento de los conceptos éticos y deontológicos de la profesión', '', 607, 'N', 0),
(1090, 'Capacidad crítica: interpretar y valorar críticamente la información y la realidad', '', 608, 'N', 0),
(1091, 'Fundamentación y argumentación de los juicios propios', '', 608, 'N', 0),
(1092, 'Capacidad autocrítica: reconocer las limitaciones propias y considerar los juicios de los demás', '', 608, 'N', 0),
(1093, 'Incorporación y valoración crítica de los conceptos éticos y deontológicos de la profesión', '', 608, 'N', 0),
(1094, 'Actuación coherente y responsable en sus decisiones y conductas', '', 609, 'N', 0),
(1095, 'Gestión adecuada de situaciones que desde un punto de vista ético resulten significativas, complejas o conflictivas', '', 609, 'N', 0),
(1096, 'Satisfacción, mediante el diálogo, de alguna necesidad vinculada a la convivencia a partir de los valores éticos deseados', '', 609, 'N', 0),
(1097, 'Aplicación de los conceptos éticos y deontológicos de la profesión', '', 609, 'N', 0),
(1098, 'No discriminación de las personas por razones de diferencia social, cultural o de género', '', 609, 'N', 0),
(1099, 'Análisis de la información: Identificación, reconocimiento e interpretación de las ideas y conceptos básicos de la información', '', 611, 'N', 0),
(1100, 'Síntesis de la información: Capacidad de síntesis de la información', '', 611, 'N', 0),
(1101, 'Aplicación de los conocimientos teóricos a situaciones reales: Práctica de manera disciplinaria de enfoques, métodos y experiencias que propone el profesor', '', 611, 'N', 0),
(1102, 'Adaptación a situaciones nuevas: Capacidad de reacción ante imprevistos o pequeñas variaciones en un planteamiento', '', 611, 'N', 0),
(1103, 'Toma de decisiones: Capacidad de transferir los conocimientos teóricos a situaciones prácticas', '', 611, 'N', 0),
(1104, 'Análisis y síntesis de la información: • Ordenación y explicación coherente de las ideas y conceptos básicos • Identificación correcta de los conceptos fundamentales • Establecimiento de relaciones que ordenan los elementos cualitativos', '', 612, 'N', 0),
(1105, 'Aplicación de los conocimientos teóricos a situaciones reales: Selección de un proceso o de procedimientos de entre los que propone el profesor', '', 612, 'N', 0),
(1106, 'Adaptación a situaciones nuevas: • Intercambio de ideas e información con el profesor y los compañeros más expertos • Aprendizaje de los propios errores o de las críticas • Análisis para mejorar', '', 612, 'N', 0),
(1107, 'Toma de decisiones: Toma de decisiones en ámbitos concretos de trabajo', '', 612, 'N', 0),
(1108, 'Análisis y síntesis de la información: Relación e integración de la información multidisciplinar', '', 613, 'N', 0),
(1109, 'Aplicación de los conocimientos teóricos a situaciones reales: propuesta o diseño de un proceso o procedimiento adecuado para conseguir los objetivos propuestos en situaciones reales', '', 613, 'N', 0),
(1110, 'Adaptación a situaciones nuevas: • Reconducción de los cambios o contratiempos que pueden surgir durante el desarrollo de una o varias actividades • Afrontamiento como reto y capacidad de atender simultáneamente diversos trabajos complejos', '', 613, 'N', 0),
(1111, 'Toma de decisiones: Toma de decisiones precisas y con coherencia en ámbitos o situaciones complejas o comprometidas', '', 613, 'N', 0),
(1112, 'Asistencia a las sesiones presenciales', '', 614, 'N', 0),
(1113, 'Puntualidad en la entrega de los trabajos, a pesar de las dificultades', '', 614, 'N', 0),
(1114, 'Entrega del trabajo en el plazo fijado', '', 616, 'N', 0),
(1115, 'Intervención en la definición de los objetivos del trabajo', '', 616, 'N', 0),
(1116, 'Colaboración en la definición y en la distribución de las treas del trabajo en grupo', '', 616, 'N', 0),
(1117, 'Compartir con el equipo el conocimiento y la información', '', 616, 'N', 0),
(1118, 'Implicación en los objetivos del grupo y retroalimentación constructiva', '', 616, 'N', 0),
(1119, 'Aceptación y cumplimiento de las normas del grupo', '', 617, 'N', 0),
(1120, 'Aceptación y cumplimiento de las normas del grupo', '', 617, 'N', 0),
(1121, 'Aceptación y cumplimiento de las normas del grupo', '', 617, 'N', 0),
(1122, 'Contribución al establecimiento y a la aplicación de los procesos del trabajo en equipo', '', 617, 'N', 0),
(1123, 'Actuación para afrontar los conflictos del equipo y su cohesión', '', 617, 'N', 0),
(1124, 'Valoración de la colaboración del trabajo en equipo', '', 617, 'N', 0),
(1125, 'Colaboración en la definición y en la distribución de las tareas del trabajo en equipo', '', 618, 'N', 0),
(1126, 'Propuesta al grupo de objetivos ambiciosos', '', 618, 'N', 0),
(1127, 'Actuación para afrontar los conflictos del equipo y su cohesión', '', 618, 'N', 0),
(1128, 'Promoción de la implicación en la gestión y funcionamiento del equipo', '', 618, 'N', 0),
(1129, 'Propuesta de ideas innovadoras y originales', '', 620, 'N', 0),
(1130, 'Generación de nuevas ideas o soluciones a situaciones o problemas basándose en lo que conoce', '', 620, 'N', 0),
(1131, 'Flexibilidad a la hora de trabajar', '', 620, 'N', 0),
(1132, 'Aportación de ideas originales para solucionar problemas presentados con los recursos disponibles', '', 621, 'N', 0),
(1133, 'Integración de los conocimientos de diferentes disciplinas para generar ideas', '', 621, 'N', 0),
(1134, 'Expresión formal de las ideas', '', 621, 'N', 0),
(1135, 'Propuesta de ideas rompedoras respecto a procedimientos establecidos', '', 622, 'N', 0),
(1136, 'Utilización de metodologías de trabajo para provocar la generación de ideas originales', '', 622, 'N', 0),
(1137, 'Propuesta de ideas que afectan a un amplio contexto de agentes', '', 622, 'N', 0),
(1138, 'Justificación razonada de la necesidad del proyecto', '', 624, 'N', 0),
(1139, 'Establecimiento de unos objetivos claros del proyecto', '', 624, 'N', 0),
(1140, 'Asignación de plazos necesarios para completar las acciones previstas', '', 624, 'N', 0),
(1141, 'Planificación de las acciones que hay que realizar para la consecución de los objetivos', '', 624, 'N', 0),
(1142, 'Planificación de la evaluación de la ejecución y de los resultados del proyecto', '', 624, 'N', 0),
(1143, 'Descripción del contexto del proyecto con pruebas y datos', '', 625, 'N', 0),
(1144, 'Coherencia de los objetivos del proyecto con las necesidades o problemas planteados', '', 625, 'N', 0),
(1145, 'Aprovechamiento de los recursos disponibles', '', 625, 'N', 0),
(1146, 'Organización de tareas que hay que desarrollar para cubrir los objetivos', '', 625, 'N', 0),
(1147, 'Planificación de los mecanismos de implementación y control', '', 625, 'N', 0),
(1148, 'Análisis del contexto para definir objetivos concretos como respuesta a retos innovadores que él mismo propone', '', 626, 'N', 0),
(1149, 'Priorización de objetivos a medio y largo plazo, emprendiendo acciones correctivas si es necesario', '', 626, 'N', 0),
(1150, 'Utilización de los recursos disponibles y búsqueda de recursos necesarios para el desarrollo', '', 626, 'N', 0),
(1151, 'Planificación y coordinación de manera flexible de las tareas de los miembros del equipo', '', 626, 'N', 0),
(1152, 'Aplicación de procedimientos de seguimiento de la calidad y evaluación del proyecto', '', 626, 'N', 0),
(1153, 'Visión de la realidad que le envuelve y evaluación de aspectos positivos y negativos del contexto planteado', '', 628, 'N', 0),
(1154, 'Iniciativa y propuesta de acciones innovadoras ante retos planteados', '', 628, 'N', 0),
(1155, 'Evaluación de las consecuencias y riesgos de las posibles acciones que se emprendan.', '', 628, 'N', 0),
(1156, 'Adopción de actitudes de liderazgo ante las situaciones planteadas', '', 628, 'N', 0),
(1157, 'Visión de la realidad que le envuelve y capacidad de proponer mejoras ante una situación dada', '', 629, 'N', 0),
(1158, 'Capacidad de proponer y promover métodos y soluciones innovadoras ante un proyecto que enriquece a los demás.', '', 629, 'N', 0),
(1159, 'Evaluación de las consecuencias y riesgos, implicando a otras personas para poder alcanzarlos', '', 629, 'N', 0),
(1160, 'Actitudes de liderazgo, transmisión de confianza y capacidad de animar a la acción a los demás', '', 629, 'N', 0),
(1161, 'Visión de futuro y toma de iniciativas después de identificar las necesidades de mejora en las situaciones complejas', '', 630, 'N', 0),
(1162, 'Utilización de nuevos métodos para trabajar y obtención de resultados con la innovación', '', 630, 'N', 0),
(1163, 'Análisis de los riesgos y beneficios de la innovación con visión estratégica', '', 630, 'N', 0),
(1164, 'Puesta en marcha y promoción de proyectos complejos y desafiantes', '', 630, 'N', 0),
(1165, 'Conocimiento y comprensión de los objetivos de los planes de ahorro energético y de agua y de los planes de residuos de la institución u organización en la que se desarrolla su actividad', '', 632, 'N', 0),
(1166, 'Comprensión de la relación entre estos objetivos y las actuaciones individuales y/o colectivas', '', 632, 'N', 0),
(1167, 'Actuación individual de acuerdo con los planes consensuados y vigentes', '', 632, 'N', 0),
(1168, 'Conocimiento de los elementos de acción necesarios para el diseño de actuaciones', '', 633, 'N', 0),
(1169, 'Diseño de acciones específicas en el entorno profesional correspondiente', '', 633, 'N', 0),
(1170, 'Organización de las acciones específicas previamente diseñadas en colaboración con otros agentes implicados', '', 633, 'N', 0),
(1171, 'Aplicación de las acciones específicas en el entorno profesional correspondiente', '', 633, 'N', 0),
(1172, 'Coordinación de acciones integrales, respetuosas en el ámbito profesional', '', 634, 'N', 0),
(1173, 'Evaluación de actuaciones integrales y profesionales según los recursos disponibles materiales y humanos respecto al entorno social, económico y ambiental', '', 634, 'N', 0),
(1174, 'Organización: Agrupación y secuencia de ideas y de material de apoyo en la presentación oral', '', 636, 'N', 0),
(1175, 'Material de soporte: Explicaciones, ejemplos, ilustraciones, estadísticas, analogías, citas de autoridades competentes y otros tipos de información que sustraigan las ideas principales de la presentación oral', '', 636, 'N', 0),
(1176, 'Mensaje central: Punto principal, tesis o argumentación de la presentación oral', '', 636, 'N', 0),
(1181, 'Organización: Agrupación y secuencia de ideas y de material de apoyo en la presentación oral', '', 637, 'N', 0),
(1182, 'Material de soporte: Explicaciones, ejemplos, ilustraciones, estadísticas, analogías, citas de autoridades competentes y otros tipos de información que sustraigan las ideas principales de la presentación oral', '', 637, 'N', 0),
(1183, 'Mensaje central: Punto principal, tesis o argumentación de la presentación oral', '', 637, 'N', 0),
(1184, 'Lenguaje: Vocabulario, terminología y estructura de las oraciones', '', 637, 'N', 0),
(1185, 'Organización: Agrupación y secuencia de ideas y de material de apoyo en la presentación oral', '', 638, 'N', 0),
(1186, 'Material de soporte: Explicaciones, ejemplos, ilustraciones, estadísticas, analogías, citas de autoridades competentes y otros tipos de información que sustraigan las ideas principales de la presentación oral', '', 638, 'N', 0),
(1187, 'Mensaje central: Punto principal, tesis o argumentación de la presentación oral', '', 638, 'N', 0),
(1188, 'Lenguaje: Vocabulario, terminología y estructura de las oraciones', '', 638, 'N', 0),
(1189, 'Expresión: Uso de voz, gestos, contacto visual y posturas', '', 638, 'N', 0),
(1190, 'Contexto y propósito: Contexto, entendido como la situación que rodea al texto, y propósito, como el efecto deseado por el escritor en su audiencia', '', 640, 'N', 0),
(1191, 'Desarrollo de contenidos: Maneras como el texto explora y representa el tema con relación a su audiencia y propósito', '', 640, 'N', 0),
(1192, 'Fuentes y pruebas: Fuentes, entendidas como textos que se emplean para trabajar en una gran variedad de propósitos, para ampliar información, para discutir y desarrollar ideas, etc.; pruebas entendidas como material de base utilizado para manifestar útilmente ideas de otros en un texto', '', 640, 'N', 0),
(1193, 'Contexto y propósito: Contexto, entendido como la situación que rodea al texto, y propósito, como el efecto deseado por el escritor en su audiencia', '', 641, 'N', 0),
(1194, 'Desarrollo de contenidos: Maneras como el texto explora y representa el tema con relación a su audiencia y propósito', '', 641, 'N', 0);
INSERT INTO `rub_pregunta` (`id_pregunta`, `nombre`, `otros_datos`, `id_estandar`, `tipo`, `activo`) VALUES
(1195, 'Fuentes y pruebas: Fuentes, entendidas como textos que se emplean para trabajar en una gran variedad de propósitos, para ampliar información, para discutir y desarrollar ideas, etc.; pruebas entendidas como material de base utilizado para manifestar útilmente ideas de otros en un texto.', '', 641, 'N', 0),
(1196, 'Géneros, reglas y convenciones: Reglas formales e informales para determinados tipos de textos o medios de comunicación que guíen el formato, la organización y las opciones de estilo (informes de laboratorio, artículos académicos, ensayos, documentos web, etc.)', '', 641, 'N', 0),
(1197, 'Contexto y propósito: Contexto, entendido como la situación que rodea al texto, y propósito, como el efecto deseado por el escritor en su audiencia', '', 642, 'N', 0),
(1198, 'Desarrollo de contenidos: Maneras como el texto explora y representa el tema con relación a su audiencia y propósito.', '', 642, 'N', 0),
(1199, 'Fuentes y pruebas: Fuentes, entendidas como textos que se emplean para trabajar en una gran variedad de propósitos, para ampliar información, para discutir y desarrollar ideas, etc.; pruebas entendidas como material de base utilizado para manifestar útilmente ideas de otros en un texto.', '', 642, 'N', 0),
(1200, 'Géneros, reglas y convenciones: Reglas formales e informales para determinados tipos de textos o medios de comunicación que guíen el formato, la organización y las opciones de estilo (informes de laboratorio, artículos académicos, ensayos, documentos web, etc.)', '', 642, 'N', 0),
(1201, 'Dominio de reglas sintácticas y gramaticales: Reglas formales que garanticen una correcta expresión escrita de los conceptos e ideas.', '', 642, 'N', 0),
(1202, 'Mi hijo(a) asiste todos los días a la escuela.', '', 645, 'N', 0),
(1203, 'Estoy al pendiente que asista puntualmente a la escuela.', '', 645, 'N', 0),
(1204, 'Me encargo de enviar a mi hijo(a) todos los días con su uniforme escolar.', '', 645, 'N', 0),
(1205, 'Me aseguro que mi hijo(a) asista bien aseado(a) y peinado(a) a la escuela.', '', 645, 'N', 0),
(1206, 'Mi hijo(a) asiste con uniforme deportivo cuando tiene educación física.', '', 645, 'N', 0),
(1207, 'Estoy al pendiente de que mi hijo(a) cumpla con todas sus tareas.', '', 645, 'N', 0),
(1208, 'Estoy al pendiente que mi hijo(a) asista a clases con todos sus materiales para trabajar.', '', 645, 'N', 0),
(1210, 'Estoy al pendiente que mi hijo(a) tenga en buen estado sus materiales escolares.', '', 645, 'N', 0),
(1211, 'Me encargo que mi hijo(a) ingiera alimentos nutritivos antes de irse a la escuela.', '', 645, 'N', 0),
(1212, 'Estoy al pendiente de su desempeño escolar y asisto a la escuela regularmente a preguntarle al maestro sobre la conducta de mi hijo(a).', '', 645, 'N', 0),
(1213, 'Asisto a las reuniones de padres de familia.', '', 645, 'N', 0),
(1214, 'Atiendo las indicaciones y sugerencias del maestro.', '', 645, 'N', 0),
(1215, 'Asisto a los llamados cuando se requiere mi presencia en la escuela.', '', 645, 'N', 0),
(1216, 'Toma consciencia de las otras maneras de ver y percibir las cosas', '', 648, 'N', 0),
(1217, 'Tiene aceptación crítica de nuevas perspectivas, aunque se cuestionen las propias', '', 648, 'N', 0),
(1218, 'Diferencia hechos y opiniones o interpretaciones en las argumentaciones de los demás.', '', 648, 'N', 0),
(1219, 'Reflexióna sobre las consecuencias y efectos (implicaciones prácticas) que las decisiones y propuestas tienen sobre las personas.', '', 648, 'N', 0),
(1220, 'Reconoce los conceptos éticos y deontológicos de la profesión.', '', 648, 'N', 0),
(1221, 'Capacidad crítica: interpreta y valora críticamente la información y la realidad', '', 649, 'N', 0),
(1222, 'Fundamenta y argumenta los juicios propios.', '', 649, 'N', 0),
(1223, 'Capacidad autocrítica: reconoce las limitaciones propias y considera los juicios de los demás.', '', 649, 'N', 0),
(1224, 'Incorpora y valora la crítica de los conceptos éticos y deontológicos de la profesión.', '', 649, 'N', 0),
(1225, 'Actúa coherente y responsable en sus decisiones y conductas.', '', 650, 'N', 0),
(1226, 'Gestióna adecuadamente situaciones que desde un punto de vista ético resulten significativas, complejas o conflictivas.', '', 650, 'N', 0),
(1227, 'Satisface, mediante el diálogo, de alguna necesidad vinculada a la convivencia a partir de los valores éticos deseados.', '', 650, 'N', 0),
(1228, 'Aplica los conceptos éticos y deontológicos de la profesión.', '', 650, 'N', 0),
(1229, 'No discrimina a las personas por razones de diferencia social, cultural o de género.', '', 650, 'N', 0),
(1230, 'Identifica, reconoce e interpreta las ideas y conceptos básicos de la información', '', 653, 'N', 0),
(1231, 'Tiene la capacidad de sintetizar la información.', '', 653, 'N', 0),
(1232, 'Práctica de manera disciplinaria de enfoques, métodos y experiencias que propone el profesor.', '', 653, 'N', 0),
(1233, 'Capacidad de reacción ante imprevistos o pequeñas variaciones en un planteamiento.', '', 653, 'N', 0),
(1234, 'Capacidad de transferir los conocimientos teóricos a situaciones prácticas.', '', 653, 'N', 0),
(1235, 'Ordena y explica coherentemente las ideas y conceptos básicos , Identifica correcto los conceptos fundamentales, y Establece las relaciones que ordenan los elementos cualitativos.', '', 654, 'N', 0),
(1236, 'Selecciona un proceso o procedimientos de entre los que propone el profesor.', '', 654, 'N', 0),
(1237, 'Intercambia ideas e información con el profesor y los compañeros más expertos, Aprende de los propios errores o de las críticas y Análisa para mejorar.', '', 654, 'N', 0),
(1238, 'Toma decisiones en ámbitos concretos de trabajo.', '', 654, 'N', 0),
(1239, 'Relaciona e integra la información multidisciplinaria.', '', 655, 'N', 0),
(1240, 'Propone o diseña un proceso o procedimiento adecuado para conseguir los objetivos propuestos en situaciones reales.', '', 655, 'N', 0),
(1241, 'Reconducciona los cambios o contratiempos que pueden surgir durante el desarrollo de una o varias actividades, Afronta como reto y capacidad de atender simultáneamente diversos trabajos complejos.', '', 655, 'N', 0),
(1242, 'Toma decisiones precisas y con coherencia en ámbitos o situaciones complejas o comprometidas.', '', 655, 'N', 0),
(1243, 'Asiste a las sesiones presenciales.', '', 656, 'N', 0),
(1244, 'Es Puntual en la entrega de los trabajos, a pesar de las dificultades.', '', 656, 'N', 0),
(1245, 'Entrega del trabajo en el plazo fijado.', '', 658, 'N', 0),
(1246, 'Interviene en la definición de los objetivos del trabajo.', '', 658, 'N', 0),
(1247, 'Colabora en la definición y en la distribución de las tareas del trabajo en grupo.', '', 658, 'N', 0),
(1248, 'Comparte con el equipo el conocimiento y la información.', '', 658, 'N', 0),
(1249, 'Implica en los objetivos del grupo y retroalimentación constructiva.', '', 658, 'N', 0),
(1250, 'Acepta y cumple con las normas del grupo.', '', 659, 'N', 0),
(1251, 'Contribuye con el establecimiento y en la aplicación de los procesos del trabajo en equipo', '', 659, 'N', 0),
(1252, 'Actua para afrontar los conflictos del equipo y su cohesión.', '', 659, 'N', 0),
(1253, 'Valora la colaboración del trabajo en equipo.', '', 659, 'N', 0),
(1254, 'Colabora en la definición y en la distribución de las tareas del trabajo en equipo.', '', 660, 'N', 0),
(1255, 'Propone al grupo objetivos ambiciosos.', '', 660, 'N', 0),
(1256, 'Actua para afrontar los conflictos del equipo y su cohesión.', '', 660, 'N', 0),
(1257, 'Promociona la implicación de la gestión y funcionamiento del equipo.', '', 660, 'N', 0),
(1259, 'Propone ideas innovadoras y originales', '', 662, 'N', 0),
(1260, 'Genera nuevas ideas o soluciones a situaciones o problemas basándose en lo que conoce.', '', 662, 'N', 0),
(1261, 'Es Flexible a la hora de trabajar.', '', 662, 'N', 0),
(1262, 'Aporta ideas originales para solucionar problemas presentados con los recursos disponibles.', '', 663, 'N', 0),
(1263, 'Integra conocimientos de diferentes disciplinas para generar ideas.', '', 663, 'N', 0),
(1264, 'Expresa formalmente las ideas.', '', 663, 'N', 0),
(1265, 'Propone ideas rompedoras respecto a procedimientos establecidos.', '', 664, 'N', 0),
(1266, 'Utiliza metodologías de trabajo para provocar la generación de ideas originales.', '', 664, 'N', 0),
(1267, 'Propone ideas que afectan a un amplio contexto de agentes.', '', 664, 'N', 0),
(1268, 'Tiene justificación razonada de la necesidad del proyecto.', '', 666, 'N', 0),
(1269, 'Establece objetivos claros del proyecto.', '', 666, 'N', 0),
(1270, 'Asigna plazos necesarios para completar las acciones previstas.', '', 666, 'N', 0),
(1271, 'Planifica las acciones que hay que realizar para la consecución de los objetivos.', '', 666, 'N', 0),
(1272, 'Planifica la evaluación de la ejecución y de los resultados del proyecto.', '', 666, 'N', 0),
(1273, 'Describe el contexto del proyecto con pruebas y datos.', '', 667, 'N', 0),
(1274, 'Tiene coherencia de los objetivos del proyecto con las necesidades o problemas planteados.', '', 667, 'N', 0),
(1275, 'Aprovecha los recursos disponibles.', '', 667, 'N', 0),
(1276, 'Organiza las tareas que hay que desarrollar para cubrir los objetivos.', '', 667, 'N', 0),
(1277, 'Planifica los mecanismos de implementación y control', '', 667, 'N', 0),
(1278, 'Analisa el contexto para definir objetivos concretos como respuesta a retos innovadores que él mismo propone.', '', 668, 'N', 0),
(1279, 'Prioriza objetivos a medio y largo plazo, emprendiendo acciones correctivas si es necesario.', '', 668, 'N', 0),
(1280, 'Utiliza los recursos disponibles y búsqueda de recursos necesarios para el desarrollo.', '', 668, 'N', 0),
(1281, 'Planifica y coordina de manera flexible las tareas de los miembros del equipo.', '', 668, 'N', 0),
(1282, 'Aplica procedimientos de seguimiento de la calidad y evaluación del proyecto.', '', 668, 'N', 0),
(1283, 'Tiene visión de la realidad que le envuelve y evaluación de aspectos positivos y negativos del contexto planteado.', '', 670, 'N', 0),
(1284, 'Tiene Iniciativa y propone acciones innovadoras ante retos planteados.', '', 670, 'N', 0),
(1285, 'Evalua las consecuencias y riesgos de las posibles acciones que se emprendan.', '', 670, 'N', 0),
(1286, 'Adopta actitudes de liderazgo ante las situaciones planteadas.', '', 670, 'N', 0),
(1287, 'Tiene visión de la realidad que le envuelve y capacidad de proponer mejoras ante una situación dada.', '', 671, 'N', 0),
(1288, 'Tiene la capacidad de proponer y promover métodos y soluciones innovadoras ante un proyecto que enriquece a los demás.', '', 671, 'N', 0),
(1289, 'Evalúa las consecuencias y riesgos, implicando a otras personas para poder alcanzarlos.', '', 671, 'N', 0),
(1290, 'Tiene actitudes de liderazgo, transmisión de confianza y capacidad de animar a la acción a los demás.', '', 671, 'N', 0),
(1291, 'Tiene visión de futuro y toma de iniciativas después de identificar las necesidades de mejora en las situaciones complejas.', '', 672, 'N', 0),
(1292, 'Utiliza nuevos métodos para trabajar y obtención de resultados con la innovación.', '', 672, 'N', 0),
(1293, 'Analiza los riesgos y beneficios de la innovación con visión estratégica.', '', 672, 'N', 0),
(1294, 'Pone en marcha y promociona proyectos complejos y desafiantes.', '', 672, 'N', 0),
(1295, 'Conoce y comprende los objetivos de los planes de ahorro energético y de agua y de los planes de residuos de la institución u organización en la que se desarrolla su actividad.', '', 674, 'N', 0),
(1296, 'Comprende la relación entre estos objetivos y las actuaciones individuales y/o colectivas.', '', 674, 'N', 0),
(1297, 'Actua individualmente de acuerdo con los planes consensuados y vigentes.', '', 674, 'N', 0),
(1298, 'Conoce los elementos de acción necesarios para el diseño de actuaciones.', '', 675, 'N', 0),
(1299, 'Diseña acciones específicas en el entorno profesional correspondiente.', '', 675, 'N', 0),
(1300, 'Organiza acciones específicas previamente diseñadas en colaboración con otros agentes implicados.', '', 675, 'N', 0),
(1301, 'Aplica acciones específicas en el entorno profesional correspondiente.', '', 675, 'N', 0),
(1302, 'Coordina acciones integrales, respetuosas en el ámbito profesional.', '', 676, 'N', 0),
(1303, 'Evalua actuaciones integrales y profesionales según los recursos disponibles materiales y humanos respecto al entorno social, económico y ambiental', '', 676, 'N', 0),
(1304, 'Tiene organización: Agrupa y hace secuencia de ideas y de material de apoyo en la presentación oral.', '', 678, 'N', 0),
(1305, 'Tiene material de soporte: Explicaciones, ejemplos, ilustraciones, estadísticas, analogías, citas de autoridades competentes y otros tipos de información que sustraigan las ideas principales de la presentación oral.', '', 678, 'N', 0),
(1306, 'Proporciona un mensaje central: Punto principal, tesis o argumentación de la presentación oral.', '', 678, 'N', 0),
(1307, 'Tiene organización: Agrupa y hace secuencia de ideas y de material de apoyo en la presentación oral.', '', 679, 'N', 0),
(1308, 'Tiene material de soporte: Explicaciones, ejemplos, ilustraciones, estadísticas, analogías, citas de autoridades competentes y otros tipos de información que sustraigan las ideas principales de la presentación oral.', '', 679, 'N', 0),
(1309, 'Proporciona un mensaje central: Punto principal, tesis o argumentación de la presentación oral.', '', 679, 'N', 0),
(1310, 'Tiene lenguaje adecuado: Vocabulario, terminología y estructura de las oraciones', '', 679, 'N', 0),
(1311, 'Tiene organización: Agrupa y hace secuencia de ideas y de material de apoyo en la presentación oral.', '', 680, 'N', 0),
(1312, 'Tiene material de soporte: Explicaciones, ejemplos, ilustraciones, estadísticas, analogías, citas de autoridades competentes y otros tipos de información que sustraigan las ideas principales de la presentación oral.', '', 680, 'N', 0),
(1313, 'Proporciona un mensaje central: Punto principal, tesis o argumentación de la presentación oral.', '', 680, 'N', 0),
(1314, 'Tiene lenguaje adecuado: Vocabulario, terminología y estructura de las oraciones.', '', 680, 'N', 0),
(1315, 'Tiene buena Expresión: Uso de voz, gestos, contacto visual y posturas.', '', 680, 'N', 0),
(1317, 'Tiene un contexto y propósito: Contexto, entendido como la situación que rodea al texto, y propósito, como el efecto deseado por el escritor en su audiencia.', '', 682, 'N', 0),
(1318, 'Desarrolla los contenidos: Maneras como el texto explora y representa el tema con relación a su audiencia y propósito.', '', 682, 'N', 0),
(1319, 'Entiende fuentes y emplea pruebas: Fuentes, entendidas como textos que se emplean para trabajar en una gran variedad de propósitos, para ampliar información, para discutir y desarrollar ideas, etc.; pruebas entendidas como material de base utilizado para manifestar útilmente ideas de otros en un texto.', '', 682, 'N', 0),
(1320, 'Tiene un contexto y propósito: Contexto, entendido como la situación que rodea al texto, y propósito, como el efecto deseado por el escritor en su audiencia.', '', 683, 'N', 0),
(1321, 'Desarrolla contenidos: Maneras como el texto explora y representa el tema con relación a su audiencia y propósito.', '', 683, 'N', 0),
(1322, 'Entiende y emplea fuentes y pruebas: Fuentes, entendidas como textos que se emplean para trabajar en una gran variedad de propósitos, para ampliar información, para discutir y desarrollar ideas, etc.; pruebas entendidas como material de base utilizado para manifestar útilmente ideas de otros en un texto.', '', 683, 'N', 0),
(1323, 'Usa géneros, reglas y convenciones: Reglas formales e informales para determinados tipos de textos o medios de comunicación que guíen el formato, la organización y las opciones de estilo (informes de laboratorio, artículos académicos, ensayos, documentos web, etc.)', '', 683, 'N', 0),
(1324, 'Entiende contextos y propósitos: Contexto, entendido como la situación que rodea al texto, y propósito, como el efecto deseado por el escritor en su audiencia.', '', 684, 'N', 0),
(1325, 'Desarrolla contenidos: Maneras como el texto explora y representa el tema con relación a su audiencia y propósito.', '', 684, 'N', 0),
(1326, 'Entiende y emplea fuentes y pruebas: Fuentes, entendidas como textos que se emplean para trabajar en una gran variedad de propósitos, para ampliar información, para discutir y desarrollar ideas, etc.; pruebas entendidas como material de base utilizado para manifestar útilmente ideas de otros en un texto.', '', 684, 'N', 0),
(1327, 'Usa géneros, reglas y convenciones: Reglas formales e informales para determinados tipos de textos o medios de comunicación que guíen el formato, la organización y las opciones de estilo (informes de laboratorio, artículos académicos, ensayos, documentos web, etc.).', '', 684, 'N', 0),
(1328, 'Domina reglas sintácticas y gramaticales: Reglas formales que garanticen una correcta expresión escrita de los conceptos e ideas.', '', 684, 'N', 0),
(1329, 'Promueve el interés y/o la participación de los estudiantes en las actividades de aprendizaje?', '', 686, 'N', 0),
(1330, 'Evalúa en qué medida el grupo de estudiantes se encuentra interesado y/o participa activamente durante el desarrollo de las actividades de aprendizaje?', '', 686, 'N', 0),
(1331, 'Favorece la comprensión del sentido, importancia o utilidad de lo que se aprende?', '', 686, 'N', 0),
(1332, 'Promueve el interés y/o la participación de los estudiantes en las actividades de aprendizaje.', '', 688, 'N', 0),
(1333, 'Evalúa en qué medida el grupo de estudiantes se encuentra interesado y/o participa activamente durante el desarrollo de las actividades de aprendizaje.', '', 688, 'N', 0),
(1334, 'Favorece la comprensión del sentido, importancia o utilidad de lo que se aprende.', '', 688, 'N', 0),
(1335, 'Evalúa que los estudiantes estén ocupados en las actividades de aprendizaje propuestas, ya sea de forma individual o en grupos.', '', 689, 'N', 0),
(1336, 'Maneja con fluidez las transiciones, gestiona de manera efectiva las interrupciones e invierte poco tiempo en las acciones accesorias.', '', 689, 'N', 0),
(1337, 'Realiza actividades e interacciones que promueven efectivamente el razonamiento, la creatividad y/o el pensamiento crítico.', '', 690, 'N', 0),
(1338, 'Monitorea el aprendizaje de los estudiantes y sus avances durante la sesión.', '', 691, 'N', 0),
(1339, 'Ofrece calidad de retro-alimentación y/o la adaptación a las actividades que realiza en la sesión a partir de las necesidades de aprendizaje identificadas.', '', 691, 'N', 0),
(1340, 'Muestra un trato respetuoso y consideración hacia la perspectiva de los estudiantes.', '', 692, 'N', 0),
(1341, 'Transmite cordialidad o calidez.', '', 692, 'N', 0),
(1342, 'Es comprensivo y  empático ante las necesidades afectivas o físicas de los estudiantes.', '', 692, 'N', 0),
(1343, 'Promueve la autorregulacion del comportamiento y el respeto de las normas de convivencia en el aula.', '', 693, 'N', 0),
(1344, 'Implementa los mecanismos para regular el comportamiento de los estudiantes, lo que se traduce en la mayor o menor continuidad en el desarrollo de la sesión.', '', 693, 'N', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rub_respuestas`
--

CREATE TABLE `rub_respuestas` (
  `id_respuestas` int(11) NOT NULL,
  `id_rubrica` int(11) NOT NULL,
  `nombre` varchar(128) DEFAULT NULL COMMENT 'Quien Rellena la Rubrica',
  `institucion` varchar(255) DEFAULT NULL,
  `idlocal` int(11) DEFAULT NULL,
  `iddocente` varchar(8) DEFAULT NULL,
  `fecha_registro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `activo` tinyint(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `rub_respuestas`
--

INSERT INTO `rub_respuestas` (`id_respuestas`, `id_rubrica`, `nombre`, `institucion`, `idlocal`, `iddocente`, `fecha_registro`, `activo`) VALUES
(1, 1, 'María', '', 1, '', '2017-05-19 00:47:36', 1),
(2, 1, 'Javier', '', 1, '', '2017-02-01 00:57:32', 1),
(3, 19, 'Evelio Muñoz', '', 1, '', '2017-06-15 09:12:39', 1),
(5, 32, 'Miluska', '', 1, '55555555', '2017-06-15 09:54:07', 1),
(6, 19, 'Miluska', '', 1, '', '2017-06-15 09:47:57', 1),
(7, 19, 'Eder', '', 1, '', '2017-06-15 11:03:38', 1),
(8, 19, 'Abel Chingo', '', 1, '', '2017-06-20 01:47:24', 1),
(10, 38, 'Pv Ingles', '', 1, '99999999', '2017-07-04 05:54:31', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rub_rubrica`
--

CREATE TABLE `rub_rubrica` (
  `id_rubrica` int(11) NOT NULL,
  `titulo` varchar(50) NOT NULL,
  `descripcion` text,
  `foto` text,
  `tipo_encuestado` varchar(1) DEFAULT NULL COMMENT 'A=Anonimo; P=Publico',
  `opcion_publico` varchar(10) NOT NULL COMMENT 'E=Empresa, I=Institución',
  `autor` varchar(200) NOT NULL,
  `tipo_letra` varchar(32) NOT NULL,
  `tamanio_letra` tinyint(5) NOT NULL,
  `fecha_creacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `tipo_rubrica` char(2) NOT NULL,
  `puntuacion_general` text NOT NULL,
  `tipo_pregunta` varchar(2) NOT NULL,
  `id_usuario` int(11) NOT NULL COMMENT 'Creador de la Rubrica',
  `activo` int(11) NOT NULL,
  `registros` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `rub_rubrica`
--

INSERT INTO `rub_rubrica` (`id_rubrica`, `titulo`, `descripcion`, `foto`, `tipo_encuestado`, `opcion_publico`, `autor`, `tipo_letra`, `tamanio_letra`, `fecha_creacion`, `tipo_rubrica`, `puntuacion_general`, `tipo_pregunta`, `id_usuario`, `activo`, `registros`) VALUES
(1, 'Seguimiento de la Acreditación', 'El objetivo de esta rúbrica es hacer un seguimiento del proceso de acreditación definido por el SINEACE en ocho dimensiones.\n', 'imagenes/20170502061309_PORE-2-3-A-SDARC-9688500-Noticia-775901.jpeg', 'P', 'I', 'Miembros de Ugel', 'Arial', 14, '2017-01-01 10:00:00', '3', '', '', 72042592, 1, 1),
(2, 'Dimensiones de Rúbrica 2013', 'rub_editada', 'imagenes/20170502061340_f1-01-01.jpg', 'A', '0', 'abc', 'Arial', 14, '2017-01-01 10:00:00', '2', '', '', 72042592, 1, 1),
(19, 'Evaluación del cafetín', 'Medir el servicio de cafetín y el grado de satisfacción del personal administrativo, docente y alumnos', 'imagenes/20170614045640_MZFP_RXNjdWVsYXNfRE5fMC5qcGc=.jpg', 'P', 'I', '', 'Arial', 14, '2017-05-07 00:26:16', '3', '', '', 72042592, 1, 2),
(32, 'Instrumento de Evaluación del Desempeño del Docent', 'Este Instrumento tiene como fin de evaluar de forma integral el proceso de enseñanza. observe cada característica general se enmarca dentro de unas especificaciones mencionadas en la parte inferior del instrumento. Lea cuidadosamente los Items y marca con X la casilla de acuerdo a tu criterio.', 'imagenes/20170614051558_ID-MparaM-acordeon-evaluacion.jpg', 'P', 'I', 'Juan Pérez', 'Arial', 14, '2017-06-15 08:12:48', '1', '', '', 72042592, 1, 2),
(33, 'Evaluación de Competencias transversales.', '-Diseñar, orientar y desarrollar contenidos, actividades de formación y de evaluación, y otros recursos vinculados a la enseñanza-aprendizaje, de modo que se valoren los resultados y se elaboren propuestas de mejora.\n-Aplicar estrategias metodológicas de aprendizaje y evaluación adecuadas a las necesidades de los estudiantes, de modo que sean coherentes con los objetivos y los procesos de evaluación para contribuir a mejorar los procesos se enseñanza-aprendizaje.\n- Desarrollar procesos bidireccionales de comunicación eficaz y correctamente.', 'imagenes/20170621062625_Docente.jpg', 'P', 'I', 'Ugel', 'Arial', 14, '2017-06-22 09:27:03', '3', '', '', 72042592, 1, 0),
(35, 'AUTO EVALUACIÓN DEL DESEMPEÑO DEL PADRE O MADRE DE', '', 'imagenes/20170623085057_descarga.jpg', 'P', 'I', '', 'Arial', 14, '2017-06-23 23:51:54', '2', '', '', 72042592, 0, 1),
(36, 'Autoevaluación del desempeño del padre y madre', 'Dirigida a los padres y madres de familia o representantes legales de los estudiantes de la institución educativa, con el objeto de recoger las opiniones sobre distintos aspectos relacionados con la calidad de la educación que se brinda en ella. ', 'imagenes/20170623085346_descarga.jpg', 'P', 'I', 'UGEL', 'Arial', 14, '2017-06-23 23:53:57', '1', '', '', 72042592, 1, 1),
(38, 'Evaluación del Desempeño Docente', 'La Evaluación del Desempeño busca dar oportunidades para que los docentes demuestren sus habilidades y destrezas y reciban retro-alimentación útil para la mejora de su práctica. Además, se debe evitar generar ansiedad o temor hacia la situación de evaluación.', 'imagenes/20170627111839_evalua_desem150.jpg', 'P', 'I', '', 'Arial', 14, '2017-06-28 01:33:12', '2', '', '', 72042592, 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rub_tipo_letra`
--

CREATE TABLE `rub_tipo_letra` (
  `id_tipoletra` int(11) NOT NULL,
  `descripcion` varchar(30) NOT NULL,
  `activo` tinyint(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `rub_tipo_letra`
--

INSERT INTO `rub_tipo_letra` (`id_tipoletra`, `descripcion`, `activo`) VALUES
(1, 'Arial', 1),
(2, 'Arial Black', 1),
(3, 'Book Antiqua', 1),
(4, 'Courier New', 1),
(5, 'Georgia', 1),
(6, 'Helvetica', 1),
(7, 'Impact', 1),
(8, 'Lucida Console', 1),
(9, 'Lucida Sans Unicode', 1),
(10, 'Palatino Linotype', 1),
(11, 'Tahoma', 1),
(12, 'Times New Roman', 1),
(13, 'Verdana', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sis_configuracion`
--

CREATE TABLE `sis_configuracion` (
  `config` varchar(30) NOT NULL,
  `valor` longtext,
  `autocargar` enum('si','no') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sis_configuracion`
--

INSERT INTO `sis_configuracion` (`config`, `valor`, `autocargar`) VALUES
('descripcion_sitio', 'Sistema integral para la administración de ventas y gastos', 'no'),
('direccion_negocio', 'Perú', 'no'),
('email_contacto', 'abel_chingo@hotmail.com', 'no'),
('facebook_id', '', 'no'),
('idioma_defecto', 'EN', 'si'),
('multiidioma', 'SI', 'si'),
('nombre_sitio', 'Portal ingles', 'no'),
('servidor_correo_clave', 'info2016@', 'no'),
('servidor_correo_cuenta', 'info@ingles.com', 'no'),
('servidor_correo_puerto', '25', 'no'),
('servidor_correo_url', 'ip-50-62-188-218.ip.secureserver.net', 'no'),
('telefono', '55555', 'no');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tarea`
--

CREATE TABLE `tarea` (
  `idtarea` int(11) NOT NULL,
  `idnivel` int(11) NOT NULL,
  `idunidad` int(11) NOT NULL,
  `idactividad` int(11) NOT NULL,
  `iddocente` int(11) NOT NULL,
  `nombre` varchar(32) COLLATE utf8_spanish_ci NOT NULL,
  `descripcion` text COLLATE utf8_spanish_ci,
  `foto` text COLLATE utf8_spanish_ci,
  `habilidades` text COLLATE utf8_spanish_ci,
  `puntajemaximo` decimal(5,2) NOT NULL,
  `puntajeminimo` decimal(5,2) NOT NULL,
  `estado` varchar(2) COLLATE utf8_spanish_ci NOT NULL DEFAULT 'NA' COMMENT 'A=Asignado ; NA=No Asignado',
  `eliminado` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `tarea`
--

INSERT INTO `tarea` (`idtarea`, `idnivel`, `idunidad`, `idactividad`, `iddocente`, `nombre`, `descripcion`, `foto`, `habilidades`, `puntajemaximo`, `puntajeminimo`, `estado`, `eliminado`) VALUES
(1, 1, 6, 18, 22222222, 'Primera tarea de Hello', 'Tarea de reforzamiento.', '__xRUTABASEx__/static/media/image/N1-U1-A1-20170630052326.jpg', '', '20.00', '10.00', 'NA', 0),
(2, 1, 6, 18, 22222222, 'Nueva Tarea Hello', 'Descripción para tarea de Hello.', '__xRUTABASEx__/static/media/image/N1-U1-A1-20170323061349.jpg', '', '20.00', '11.00', 'NA', 0),
(3, 1, 6, 18, 22222222, 'Saludos', 'Completa correctamente las actividades adjuntas. Y si deseas reforzar tu tarea con imagenes, link, u otros archivos; lo puedes hacer.', '__xRUTABASEx__/static/media/image/N1-U1-A1-20170323061135.jpg', '', '100.00', '75.00', 'NA', 0),
(4, 1, 6, 55, 22222222, 'Tarea de Sesion 02 (A1- Introduc', '', '__xRUTABASEx__/static/media/image/N1-U1-A1-20170323061319.jpg', '', '90.00', '51.00', 'NA', 0),
(5, 1, 6, 18, 22222222, 'Mi Tarea Ejemplo para Hello', 'El objetivo de esta tarea es determinar tus habilidades', '__xRUTABASEx__/static/media/image/N1-U1-A1-20170327111758.jpg', '', '100.00', '80.00', 'NA', 0),
(6, 1, 6, 18, 22222222, 'Tarea de NUmbers', 'Desarrolle la tarea correctamente', '__xRUTABASEx__/static/media/image/N1-U1-A1-20170327120409.jpg', '', '100.00', '70.00', 'NA', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tarea_archivos`
--

CREATE TABLE `tarea_archivos` (
  `idtarea_archivos` int(11) NOT NULL,
  `tablapadre` varchar(1) COLLATE utf8_spanish_ci DEFAULT NULL COMMENT 'T=Tarea ; R=Tarea_Respuesta',
  `idpadre` int(11) NOT NULL DEFAULT '0',
  `nombre` varchar(256) COLLATE utf8_spanish_ci NOT NULL,
  `ruta` text COLLATE utf8_spanish_ci NOT NULL,
  `tipo` varchar(1) COLLATE utf8_spanish_ci NOT NULL COMMENT 'D=Documento ; G=Grabacion_Voz ; V=Video ; L=Link ; A=Actividad ; J=Juego ; E=Examen',
  `puntaje` decimal(5,2) DEFAULT NULL,
  `habilidad` text COLLATE utf8_spanish_ci,
  `texto` longtext COLLATE utf8_spanish_ci COMMENT 'Para guardar JSON y HTML',
  `idtarea_archivos_padre` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `tarea_archivos`
--

INSERT INTO `tarea_archivos` (`idtarea_archivos`, `tablapadre`, `idpadre`, `nombre`, `ruta`, `tipo`, `puntaje`, `habilidad`, `texto`, `idtarea_archivos_padre`) VALUES
(17, 'T', 1, 'archivo3.pdf', '__xRUTABASEx__/static/tarea_archivos/documento/20170718103119_archivo3.pdf', 'D', NULL, NULL, NULL, NULL),
(2, 'T', 1, 'Correo', 'https://outlook.live.com', 'L', NULL, NULL, NULL, NULL),
(16, 'T', 1, 'video_prueba_01.mp4', '__xRUTABASEx__/static/tarea_archivos/video/20170718103046_video_prueba_01.mp4', 'V', NULL, NULL, NULL, NULL),
(18, 'T', 2, 'archivo1.pdf', '__xRUTABASEx__/static/tarea_archivos/documento/20170718103347_archivo1.pdf', 'D', NULL, NULL, NULL, NULL),
(5, 'T', 2, 'EsanGlobal', 'http://www.esanglobal.com/tacna', 'L', NULL, NULL, NULL, NULL),
(6, 'T', 2, 'Activity', '__xRUTABASEx__/tarea_archivos/visor/?idarchivo=', 'A', NULL, NULL, '[85,86,87]', NULL),
(7, 'T', 2, 'Exam', '__xRUTABASEx__/tarea_archivos/visor/?idarchivo=', 'E', NULL, NULL, '[4]', NULL),
(8, 'R', 1, 'TDR LEARNING COMMUNITIES Rev KO.pdf', '__xRUTABASEx__/static/tarea_archivos/documento/20170711104819_TDR LEARNING COMMUNITIES Rev KO.pdf', 'D', '15.00', '["5"]', NULL, NULL),
(9, 'T', 2, 'Game', '__xRUTABASEx__/tarea_archivos/visor/?idarchivo=', 'J', NULL, NULL, '[10]', NULL),
(10, 'T', 3, 'Activity', '__xRUTABASEx__/tarea_archivos/visor/?idarchivo=', 'A', NULL, NULL, '[82,88,87]', NULL),
(11, 'T', 3, 'Game', '__xRUTABASEx__/tarea_archivos/visor/?idarchivo=', 'J', NULL, NULL, '[11]', NULL),
(12, 'T', 3, 'Exam', '__xRUTABASEx__/tarea_archivos/visor/?idarchivo=', 'E', NULL, NULL, '[1]', NULL),
(13, 'R', 2, 'Activity', '__xRUTABASEx__/tarea_archivos/visor/?idarchivo=', 'A', '66.70', '["4","6","8","9"]', '[{"id":"82","html":"\\n                        <div class=\\"row\\"> <div class=\\"col-xs-12\\"><div class=\\"tab-title-zone pw1_3\\"> <div class=\\"col-md-12\\"><h3 class=\\"addtext addtexttitulo2 showreset\\" data-initial_val=\\"Jane and Chris\\">Jane and Chris</h3></div><div class=\\"space-line pw1_3\\"></div></div><div class=\\"tab-description pw1_3\\"> <div class=\\"col-md-12\\"><p class=\\"addtext addtextdescription23 showreset\\" data-initial_val=\\"Watch the video, listen carefully and choose the correct option.\\">Watch the video, listen carefully and choose the correct option.</p></div></div><div class=\\"aquicargaplantilla\\" id=\\"tmppt_3_3\\" data-tpl=\\"dby_multiplantilla\\" data-puntaje=\\"0\\" data-time=\\"02:00\\" data-showpuntaje=\\"1\\" data-showtime=\\"1\\" data-addclass=\\"isclicked\\" data-tpltitulo=\\"Options-Video\\" data-tools=\\"chingoinput chingovideo\\" data-showalternativas=\\"1\\" data-showasistido=\\"0\\" data-helpvideo=\\"8\\" data-intentos=\\"3\\"><input type=\\"hidden\\" id=\\"sysidorden\\" name=\\"orden[3][3]\\" value=\\"3\\"><input type=\\"hidden\\" id=\\"sysdettipo\\" name=\\"det_tipo[3][3]\\" value=\\"Do it by yourself\\"><input type=\\"hidden\\" id=\\"systipodesarrollo\\" name=\\"tipo_desarrollo[3][3]\\" value=\\"Options-Video\\"><input type=\\"hidden\\" id=\\"systipoactividad\\" name=\\"tipo_actividad[3][3]\\" value=\\"A\\"><input type=\\"hidden\\" id=\\"sysiduser\\" name=\\"iduser[3][3]\\" value=\\"99999999\\"><input type=\\"hidden\\" id=\\"idgui\\" name=\\"idgui\\" value=\\"58acc36a082a5\\"><input type=\\"hidden\\" id=\\"iddetalle_3_3\\" name=\\"iddetalle[3][3]\\" value=\\"82\\"><link href=\\"__xRUTABASEx__/frontend/plantillas/tema1/css/actividad_completar.css\\" rel=\\"stylesheet\\">\\n\\n\\n\\n\\n\\n\\n<div class=\\"plantilla plantilla-completar ejerc-terminado-correcto tiempo-pausado\\" id=\\"tmp_58acc36a082a5\\" data-idgui=\\"58acc36a082a5\\" data-tipo-tmp=\\"tipo_2\\">\\n  <div class=\\"row\\">\\n    <div class=\\"col-md-12\\">\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n      <div class=\\"tpl_plantilla\\" style=\\"display: block;\\" data-idgui=\\"58acc36a082a5\\">\\n        <div class=\\"row active\\" id=\\"tpl58acc36a082a5\\">\\n          <div class=\\"col-md-12 col-sm-12 col-xs-12 discol1\\" id=\\"txt58acc36a082a5\\" style=\\"padding: 1ex;\\">\\n            <div class=\\"col-md-12 col-sm-12 col-xs-12 removelineedit img-thumbnail\\">\\n              <div class=\\"panel panelEjercicio\\" id=\\"pnl_edithtml58acc36a082a5\\"><p></p>\\n<p></p>\\n<p>\\n</p><div class=\\"col-md-12 col-sm-12 col-xs-12 embed-responsive embed-responsive-16by9 mce-object-video\\" controls=\\"controls\\" alt=\\"video Player\\" data-mce-object=\\"video\\" width=\\"100\\" height=\\"25\\"><video class=\\"col-md-12 col-sm-12 col-xs-12 data-mce-video embed-responsive-item\\" src=\\"__xRUTABASEx__/static/media/video/N1-U1-A1-20161228074145.mp4\\" controls=\\"true\\" alt=\\"HELLO U1E1S1.mp4\\"></video></div>\\n<p></p>\\n<p></p>\\n<p></p>\\n<p></p>\\n<p>Where are Chris and Jane?</p>\\n<p><div class=\\"inp-corregido wrapper\\" data-corregido=\\"good\\"><input width=\\"100\\" height=\\"25\\" class=\\"mce-object-input valinput isclicked\\" type=\\"text\\" data-texto=\\"They’re at the park.\\" data-mce-object=\\"input\\" data-options=\\"They’re at the mall.,They’re at the hospital.,They’re at school.\\" value=\\"They’re at the park.\\"><span class=\\"inner-icon\\"><i class=\\"fa fa-check color-green2\\"></i></span></div></p></div>              \\n            </div>            \\n          </div>\\n          <div class=\\"col-md-12 col-sm-12 col-xs-12 discol2\\" id=\\"alter58acc36a082a5\\" style=\\"padding: 1ex;\\">\\n            <div class=\\"col-md-12 col-sm-12 col-xs-12 removelineedit2 tpl_alternatives img-thumbnail\\">\\n              <div class=\\"panel panelAlternativas text-center\\" id=\\"pnl_editalternatives58acc36a082a5\\"><ul class=\\"alternativas-list\\"><li class=\\"alt-item\\"><span class=\\"alt-letra\\">a) </span><div class=\\"inp-corregido wrapper\\" data-corregido=\\"good\\"><a href=\\"#\\">They’re at the park.</a><span class=\\"inner-icon\\"><i class=\\"fa fa-check color-green2\\"></i></span></div></li><li class=\\"alt-item\\"><span class=\\"alt-letra\\">b) </span><a href=\\"#\\">They’re at the mall.</a></li><li class=\\"alt-item\\"><span class=\\"alt-letra\\">c) </span><a href=\\"#\\">They’re at the hospital.</a></li><li class=\\"alt-item\\"><span class=\\"alt-letra\\">d) </span><a href=\\"#\\">They’re at school.</a></li></ul></div>\\n            </div>\\n          </div>\\n        </div>        \\n      </div>       \\n      <textarea class=\\"txtareaedit\\" id=\\"txtarea58acc36a082a5\\" aria-hidden=\\"true\\" style=\\"display: none;\\">&lt;p&gt;&lt;/p&gt;\\n&lt;p&gt;&lt;/p&gt;\\n&lt;p&gt;\\n&lt;div data-mce-object=\\"video\\" controls=\\"controls\\" alt=\\"video Player\\" class=\\"col-md-12 col-sm-12 col-xs-12 embed-responsive embed-responsive-16by9 mce-object-video\\" height=\\"25\\" width=\\"100\\"&gt;&lt;video src=\\"__xRUTABASEx__/static/media/video/N1-U1-A1-20161228074145.mp4\\" alt=\\"HELLO U1E1S1.mp4\\" controls=\\"true\\" class=\\"col-md-12 col-sm-12 col-xs-12 data-mce-video embed-responsive-item\\"&gt;&lt;/video&gt;&lt;/div&gt;\\n&lt;/p&gt;\\n&lt;p&gt;&lt;/p&gt;\\n&lt;p&gt;&lt;/p&gt;\\n&lt;p&gt;&lt;/p&gt;\\n&lt;p&gt;Where are Chris and Jane?&lt;/p&gt;\\n&lt;p&gt;&lt;input data-options=\\"They&amp;rsquo;re at the mall.,They&amp;rsquo;re at the hospital.,They&amp;rsquo;re at school.\\" data-mce-object=\\"input\\" data-texto=\\"They&amp;rsquo;re at the park.\\" type=\\"text\\" class=\\"mce-object-input valinput isclicked\\" height=\\"25\\" width=\\"100\\"&gt;&lt;/input&gt;&lt;/p&gt;</textarea>\\n    </div>\\n  </div>\\n</div>\\n<script type=\\"text/javascript\\">\\n$(document).ready(function(){\\n  iniciarCompletar_DBY(\'58acc36a082a5\');\\n});\\n</script></div><input name=\\"habilidades[3][3]\\" class=\\"selected-skills\\" id=\\"habilidades_met-3_3\\" type=\\"hidden\\" value=\\"5|5|5|4\\"></div></div>                    "},{"id":"88","html":"\\n                        <div class=\\"row\\"> <div class=\\"col-xs-12\\"><div class=\\"tab-title-zone pw1_3\\"> <div class=\\"col-md-12\\"><h3 class=\\"addtext addtexttitulo2 showreset\\" data-initial_val=\\"first or second person singular\\">first or second person singular</h3></div><div class=\\"space-line pw1_3\\"></div></div><div class=\\"tab-description pw1_3\\"> <div class=\\"col-md-12\\"><p class=\\"addtext addtextdescription23 showreset\\" data-initial_val=\\"Choose first or second person singular.\\">Choose first or second person singular.</p></div></div><div class=\\"aquicargaplantilla\\" id=\\"tmppt_3_9\\" data-tpl=\\"dby_multiplantilla\\" data-puntaje=\\"0\\" data-time=\\"01:00\\" data-showpuntaje=\\"1\\" data-showtime=\\"1\\" data-addclass=\\"isclicked\\" data-tpltitulo=\\"Options-Text\\" data-tools=\\"chingoinput\\" data-showalternativas=\\"1\\" data-showasistido=\\"0\\" data-helpvideo=\\"5\\" data-intentos=\\"3\\"><input type=\\"hidden\\" id=\\"sysidorden\\" name=\\"orden[3][9]\\" value=\\"9\\"><input type=\\"hidden\\" id=\\"sysdettipo\\" name=\\"det_tipo[3][9]\\" value=\\"Do it by yourself\\"><input type=\\"hidden\\" id=\\"systipodesarrollo\\" name=\\"tipo_desarrollo[3][9]\\" value=\\"Options-Text\\"><input type=\\"hidden\\" id=\\"systipoactividad\\" name=\\"tipo_actividad[3][9]\\" value=\\"A\\"><input type=\\"hidden\\" id=\\"sysiduser\\" name=\\"iduser[3][9]\\" value=\\"99999999\\"><input type=\\"hidden\\" id=\\"idgui\\" name=\\"idgui\\" value=\\"58accd71230a6\\"><input type=\\"hidden\\" id=\\"iddetalle_3_9\\" name=\\"iddetalle[3][9]\\" value=\\"88\\"><link href=\\"__xRUTABASEx__/frontend/plantillas/tema1/css/actividad_completar.css\\" rel=\\"stylesheet\\">\\n\\n\\n\\n\\n\\n\\n<div class=\\"plantilla plantilla-completar tiempo-pausado\\" id=\\"tmp_58accd71230a6\\" data-idgui=\\"58accd71230a6\\" data-tipo-tmp=\\"tipo_2\\">\\n  <div class=\\"row\\">\\n    <div class=\\"col-md-12\\">\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n      <div class=\\"tpl_plantilla\\" style=\\"display: block;\\" data-idgui=\\"58accd71230a6\\">\\n        <div class=\\"row active\\" id=\\"tpl58accd71230a6\\">\\n          <div class=\\"col-md-12 col-sm-12 col-xs-12 discol1\\" id=\\"txt58accd71230a6\\" style=\\"padding: 1ex;\\">\\n            <div class=\\"col-md-12 col-sm-12 col-xs-12 removelineedit img-thumbnail\\">\\n              <div class=\\"panel panelEjercicio\\" id=\\"pnl_edithtml58accd71230a6\\"><p>I want to go to the movies.</p>\\n<p><div class=\\"inp-corregido wrapper\\" data-corregido=\\"good\\"><input width=\\"100\\" height=\\"25\\" class=\\"mce-object-input valinput isclicked\\" type=\\"text\\" data-texto=\\"First person\\" data-mce-object=\\"input\\" data-options=\\"Second person,None of the above\\" value=\\"First person\\"><span class=\\"inner-icon\\"><i class=\\"fa fa-check color-green2\\"></i></span></div></p>\\n<p></p>\\n<p></p></div>              \\n            </div>            \\n          </div>\\n          <div class=\\"col-md-12 col-sm-12 col-xs-12 discol2\\" id=\\"alter58accd71230a6\\" style=\\"padding: 1ex;\\">\\n            <div class=\\"col-md-12 col-sm-12 col-xs-12 removelineedit2 tpl_alternatives img-thumbnail\\">\\n              <div class=\\"panel panelAlternativas text-center\\" id=\\"pnl_editalternatives58accd71230a6\\"><ul class=\\"alternativas-list\\"><li class=\\"alt-item\\"><span class=\\"alt-letra\\">a) </span><a href=\\"#\\">Second person</a></li><li class=\\"alt-item\\"><span class=\\"alt-letra\\">b) </span><div class=\\"inp-corregido wrapper\\" data-corregido=\\"good\\"><a href=\\"#\\">First person</a><span class=\\"inner-icon\\"><i class=\\"fa fa-check color-green2\\"></i></span></div></li><li class=\\"alt-item\\"><span class=\\"alt-letra\\">c) </span><a href=\\"#\\">None of the above</a></li></ul></div>\\n            </div>\\n          </div>\\n        </div>        \\n      </div>       \\n      <textarea class=\\"txtareaedit\\" id=\\"txtarea58accd71230a6\\" aria-hidden=\\"true\\" style=\\"display: none;\\">&lt;p&gt;I want to go to the movies.&lt;/p&gt;\\n&lt;p&gt;&lt;input data-options=\\"Second person,none of the above\\" data-mce-object=\\"input\\" height=\\"25\\" width=\\"100\\" type=\\"text\\" data-texto=\\"First person\\" class=\\"mce-object-input valinput isclicked\\"&gt;&lt;/p&gt;\\n&lt;p&gt;&lt;/p&gt;\\n&lt;p&gt;&lt;/p&gt;</textarea>\\n    </div>\\n  </div>\\n</div>\\n<script type=\\"text/javascript\\">\\n$(document).ready(function(){\\n  iniciarCompletar_DBY(\'58accd71230a6\');\\n});\\n</script></div><input name=\\"habilidades[3][9]\\" class=\\"selected-skills\\" id=\\"habilidades_met-3_9\\" type=\\"hidden\\" value=\\"5|8\\"></div></div>                    "},{"id":"87","html":"\\n                        <div class=\\"row\\"> <div class=\\"col-xs-12\\"><div class=\\"tab-title-zone pw1_3\\"> <div class=\\"col-md-12\\"><h3 class=\\"addtext addtexttitulo2 showreset\\" data-initial_val=\\"Ordering sentences in Simple Present\\">Ordering sentences in Simple Present</h3></div><div class=\\"space-line pw1_3\\"></div></div><div class=\\"tab-description pw1_3\\"> <div class=\\"col-md-12\\"><p class=\\"addtext addtextdescription23 showreset\\" data-initial_val=\\"Put the words in the correct order.\\">Put the words in the correct order.</p></div></div><div class=\\"aquicargaplantilla\\" id=\\"tmppt_3_8\\" data-tpl=\\"dby_ordenar_simple\\" data-puntaje=\\"0\\" data-time=\\"01:00\\" data-showpuntaje=\\"1\\" data-showtime=\\"1\\" data-addclass=\\"\\" data-tpltitulo=\\"Other activities-Put in order\\" data-tools=\\"\\" data-showalternativas=\\"0\\" data-showasistido=\\"0\\" data-helpvideo=\\"19\\" data-intentos=\\"3\\"><input type=\\"hidden\\" id=\\"sysidorden\\" name=\\"orden[3][8]\\" value=\\"8\\"><input type=\\"hidden\\" id=\\"sysdettipo\\" name=\\"det_tipo[3][8]\\" value=\\"Do it by yourself\\"><input type=\\"hidden\\" id=\\"systipodesarrollo\\" name=\\"tipo_desarrollo[3][8]\\" value=\\"Other activities-Put in order\\"><input type=\\"hidden\\" id=\\"systipoactividad\\" name=\\"tipo_actividad[3][8]\\" value=\\"A\\"><input type=\\"hidden\\" id=\\"sysiduser\\" name=\\"iduser[3][8]\\" value=\\"99999999\\"><input type=\\"hidden\\" id=\\"idgui\\" name=\\"idgui\\" value=\\"58acccec32e1e\\"><input type=\\"hidden\\" id=\\"iddetalle_3_8\\" name=\\"iddetalle[3][8]\\" value=\\"87\\"><link href=\\"__xRUTABASEx__/frontend/plantillas/tema1/css/actividad_ordenar.css\\" rel=\\"stylesheet\\">\\n\\n\\n\\n\\n\\n\\n\\n<div class=\\"plantilla plantilla-ordenar ord_simple\\" id=\\"tmp_58acccec32e1e\\" data-idgui=\\"58acccec32e1e\\" data-tipo-tmp=\\"ordenar_simple\\">\\n    \\n    \\n    <div class=\\"row ejerc-ordenar tpl_plantilla\\" id=\\"ejerc-ordenar58acccec32e1e\\" style=\\"display: block;\\"><div class=\\"col-xs-12 element\\" id=\\"e_1487719657990\\" data-resp=\\"She goes home after work.\\">\\n            <div class=\\"col-xs-12 col-sm-4 multimedia hide\\">\\n                <img class=\\"img-responsive hide\\" src=\\"\\">\\n                <a class=\\"btn btn-orange btn-block hide\\" href=\\"#\\" data-tooltip=\\"tooltip\\"><i class=\\"fa fa-play\\"></i></a>\\n            </div>\\n            <div class=\\"col-xs-12 col-sm-8 texto\\">\\n                <div class=\\"drag\\"><div>home</div><div>work.</div><div>goes</div><div>after</div><div>She</div></div>\\n                <div class=\\"drop\\">\\n                <div class=\\"parte blank word\\"></div><div class=\\"parte blank word\\"></div><div class=\\"parte blank word\\"></div><div class=\\"parte blank word\\"></div><div class=\\"parte blank word\\"></div></div>\\n            </div>\\n        </div></div>\\n    \\n    <audio class=\\"hidden\\" id=\\"audio-ordenar58acccec32e1e\\" style=\\"display: none;\\" src=\\"\\"></audio>\\n</div>\\n<script> $(document).ready(function(){ initOrdenarSimple(\'58acccec32e1e\', false); }); </script></div><input name=\\"habilidades[3][8]\\" class=\\"selected-skills\\" id=\\"habilidades_met-3_8\\" type=\\"hidden\\" value=\\"0\\"></div></div>                    "}]', 10),
(14, 'R', 2, 'Game', '__xRUTABASEx__/tarea_archivos/visor/?idarchivo=', 'J', '83.30', '["5","7"]', '[{"id":"11","html":"\\n            <link rel=\\"stylesheet\\" href=\\"__xRUTABASEx__/frontend/plantillas/tema1/css/actividad_sopaletras.css\\">\\n<input type=\\"hidden\\" name=\\"idgui\\" id=\\"idgui\\" value=\\"58b61a345aab9\\">\\n<div class=\\"plantilla plantilla-sopaletras\\" data-idgui=\\"58b61a345aab9\\">\\n    \\n\\n    <div class=\\"row\\" id=\\"findword-game\\">\\n        \\n\\n        <div class=\\"col-xs-12\\" style=\\"text-align: center;\\">\\n            <div id=\\"puzzle\\"><div><button class=\\"puzzleSquare\\" x=\\"0\\" y=\\"0\\">h</button><button class=\\"puzzleSquare\\" x=\\"1\\" y=\\"0\\">r</button><button class=\\"puzzleSquare found\\" x=\\"2\\" y=\\"0\\">n</button><button class=\\"puzzleSquare found\\" x=\\"3\\" y=\\"0\\">o</button><button class=\\"puzzleSquare found\\" x=\\"4\\" y=\\"0\\">s</button><button class=\\"puzzleSquare found\\" x=\\"5\\" y=\\"0\\">a</button><button class=\\"puzzleSquare found\\" x=\\"6\\" y=\\"0\\">J</button><button class=\\"puzzleSquare found\\" x=\\"7\\" y=\\"0\\">y</button></div><div><button class=\\"puzzleSquare found\\" x=\\"0\\" y=\\"1\\">S</button><button class=\\"puzzleSquare found\\" x=\\"1\\" y=\\"1\\">u</button><button class=\\"puzzleSquare found\\" x=\\"2\\" y=\\"1\\">s</button><button class=\\"puzzleSquare found\\" x=\\"3\\" y=\\"1\\">a</button><button class=\\"puzzleSquare found\\" x=\\"4\\" y=\\"1\\">n</button><button class=\\"puzzleSquare\\" x=\\"5\\" y=\\"1\\">e</button><button class=\\"puzzleSquare\\" x=\\"6\\" y=\\"1\\">d</button><button class=\\"puzzleSquare found\\" x=\\"7\\" y=\\"1\\">r</button></div><div><button class=\\"puzzleSquare found\\" x=\\"0\\" y=\\"2\\">y</button><button class=\\"puzzleSquare found\\" x=\\"1\\" y=\\"2\\">a</button><button class=\\"puzzleSquare found\\" x=\\"2\\" y=\\"2\\">J</button><button class=\\"puzzleSquare\\" x=\\"3\\" y=\\"2\\">i</button><button class=\\"puzzleSquare found\\" x=\\"4\\" y=\\"2\\">G</button><button class=\\"puzzleSquare found\\" x=\\"5\\" y=\\"2\\">h</button><button class=\\"puzzleSquare found\\" x=\\"6\\" y=\\"2\\">i</button><button class=\\"puzzleSquare found\\" x=\\"7\\" y=\\"2\\">a</button></div><div><button class=\\"puzzleSquare found\\" x=\\"0\\" y=\\"3\\">M</button><button class=\\"puzzleSquare\\" x=\\"1\\" y=\\"3\\">t</button><button class=\\"puzzleSquare\\" x=\\"2\\" y=\\"3\\">r</button><button class=\\"puzzleSquare\\" x=\\"3\\" y=\\"3\\">e</button><button class=\\"puzzleSquare\\" x=\\"4\\" y=\\"3\\">b</button><button class=\\"puzzleSquare\\" x=\\"5\\" y=\\"3\\">o</button><button class=\\"puzzleSquare\\" x=\\"6\\" y=\\"3\\">R</button><button class=\\"puzzleSquare found\\" x=\\"7\\" y=\\"3\\">M</button></div><div><button class=\\"puzzleSquare\\" x=\\"0\\" y=\\"4\\">j</button><button class=\\"puzzleSquare found\\" x=\\"1\\" y=\\"4\\">a</button><button class=\\"puzzleSquare found\\" x=\\"2\\" y=\\"4\\">e</button><button class=\\"puzzleSquare found\\" x=\\"3\\" y=\\"4\\">s</button><button class=\\"puzzleSquare found\\" x=\\"4\\" y=\\"4\\">l</button><button class=\\"puzzleSquare found\\" x=\\"5\\" y=\\"4\\">e</button><button class=\\"puzzleSquare found\\" x=\\"6\\" y=\\"4\\">h</button><button class=\\"puzzleSquare found\\" x=\\"7\\" y=\\"4\\">C</button></div><div><button class=\\"puzzleSquare\\" x=\\"0\\" y=\\"5\\">K</button><button class=\\"puzzleSquare found\\" x=\\"1\\" y=\\"5\\">M</button><button class=\\"puzzleSquare found\\" x=\\"2\\" y=\\"5\\">i</button><button class=\\"puzzleSquare found\\" x=\\"3\\" y=\\"5\\">c</button><button class=\\"puzzleSquare found\\" x=\\"4\\" y=\\"5\\">h</button><button class=\\"puzzleSquare found\\" x=\\"5\\" y=\\"5\\">a</button><button class=\\"puzzleSquare found\\" x=\\"6\\" y=\\"5\\">e</button><button class=\\"puzzleSquare found\\" x=\\"7\\" y=\\"5\\">l</button></div><div><button class=\\"puzzleSquare found\\" x=\\"0\\" y=\\"6\\">a</button><button class=\\"puzzleSquare found\\" x=\\"1\\" y=\\"6\\">i</button><button class=\\"puzzleSquare found\\" x=\\"2\\" y=\\"6\\">r</button><button class=\\"puzzleSquare found\\" x=\\"3\\" y=\\"6\\">t</button><button class=\\"puzzleSquare found\\" x=\\"4\\" y=\\"6\\">e</button><button class=\\"puzzleSquare found\\" x=\\"5\\" y=\\"6\\">m</button><button class=\\"puzzleSquare found\\" x=\\"6\\" y=\\"6\\">e</button><button class=\\"puzzleSquare found\\" x=\\"7\\" y=\\"6\\">D</button></div><div><button class=\\"puzzleSquare found\\" x=\\"0\\" y=\\"7\\">y</button><button class=\\"puzzleSquare found\\" x=\\"1\\" y=\\"7\\">r</button><button class=\\"puzzleSquare found\\" x=\\"2\\" y=\\"7\\">a</button><button class=\\"puzzleSquare found\\" x=\\"3\\" y=\\"7\\">m</button><button class=\\"puzzleSquare found\\" x=\\"4\\" y=\\"7\\">e</button><button class=\\"puzzleSquare found\\" x=\\"5\\" y=\\"7\\">s</button><button class=\\"puzzleSquare found\\" x=\\"6\\" y=\\"7\\">o</button><button class=\\"puzzleSquare found\\" x=\\"7\\" y=\\"7\\">R</button></div></div>\\n        </div>\\n        <div class=\\"col-xs-12\\">\\n            <div id=\\"words\\"><ul><li class=\\"word Chelsea wordFound\\">Chelsea</li><li class=\\"word Demetria wordFound\\">Demetria</li><li class=\\"word Ghia wordFound\\">Ghia</li><li class=\\"word Jason wordFound\\">Jason</li><li class=\\"word Jay wordFound\\">Jay</li><li class=\\"word Karina\\">Karina</li><li class=\\"word Maite wordFound\\">Maite</li><li class=\\"word Mary wordFound\\">Mary</li><li class=\\"word Michael wordFound\\">Michael</li><li class=\\"word Robert\\">Robert</li><li class=\\"word Rosemary wordFound\\">Rosemary</li><li class=\\"word Susan wordFound\\">Susan</li></ul></div>\\n        </div>\\n    </div>\\n</div>\\n\\n<script>\\n\\n$(\\"*[data-tooltip=\\\\\\"tooltip\\\\\\"]\\").tooltip();\\n\\nvar inputsValidos = function(){\\n    var rspta = true;\\n    var $contenInputs = $(\\"#list-words-game\\");\\n    $contenInputs.find(\\"input.word-game\\").each(function() {\\n        var valor = $(this).val();\\n        if(valor.length<=1){\\n            $(this).parents(\\".form-group\\").addClass(\\"has-error\\");\\n        } else {\\n            $(this).parents(\\".form-group\\").removeClass(\\"has-error\\");\\n        }\\n    });\\n    $contenInputs.find(\\"input.word-game\\").each(function() {\\n        var valor = $(this).val();\\n        if(valor.length<=1){\\n            rspta = false;\\n            return false;\\n        }\\n    });\\n    return rspta;\\n};\\n\\nvar obtenerPalabras = function(){\\n    var palabras = [];\\n\\n    var $contenInputs = $(\\"#list-words-game\\");\\n    $contenInputs.find(\\"input.word-game\\").each(function() {\\n        var valor = $(this).val();\\n        $(this).attr(\\"value\\", valor);\\n        palabras.push(valor);\\n    });\\n\\n    return palabras;\\n};\\n\\n$(\\"#list-words-game\\")\\n    .on(\\"keydown\\", \\"input.word-game\\", function(e){\\n        if (e.which === 32)\\n            return false;\\n        if( $(this).parents(\\".form-group\\").hasClass(\\"has-error\\") ) inputsValidos();\\n    })\\n    .on(\\"change\\", \\"input.word-game\\", function(e){\\n        this.value = this.value.replace(/\\\\s/g, \\"\\");\\n    })\\n    .on(\\"click\\", \\"span.form-control-feedback\\", function(e) {\\n        e.preventDefault();\\n        $(this).parents(\\".item-word-game\\").remove();\\n    });;\\n\\n$(\\".btn.add-word-game\\").click(function(e) {\\n    e.preventDefault();\\n    var id_cloneFrom = $(this).data(\\"clonefrom\\");\\n    var id_cloneTo = $(this).data(\\"cloneto\\");\\n    var now = Date.now();\\n    var newWord_html = $(id_cloneFrom).clone(true, true);\\n\\n    newWord_html.removeClass(\\"hidden\\");\\n    newWord_html.removeAttr(\\"id\\");\\n    newWord_html.find(\\"input.word-game\\").attr(\\"id\\", \\"input-\\"+now);\\n    newWord_html.find(\\"span.form-control-feedback\\").attr(\\"data-tooltip\\", \\"tooltip\\");\\n    $(id_cloneTo).append(newWord_html);\\n    \\n    $(\\"#input-\\"+now).focus();\\n    $(\\"*[data-tooltip=\\\\\\"tooltip\\\\\\"]\\").tooltip();\\n});\\n\\n$(\\".btn.create-word-search\\").click(function(e) {\\n    e.preventDefault();\\n    if( inputsValidos() ){\\n        var idPuzzle = \\"#puzzle\\";\\n        var idWords = \\"#words\\";\\n        var words = obtenerPalabras();\\n        var gamePuzzle = wordfindgame.create(words, idPuzzle, idWords);\\n\\n        $(\\"#pnl-edit\\").hide();\\n        $(\\"#findword-game\\").show(\\"fast\\");\\n    } else {\\n        $(\\"#list-words-game\\").find(\\".has-error\\").first().find(\\"input.word-game\\").focus();\\n        mostrar_notificacion(\\"Attention\\",\\"words must have more than one letter\\", \\"warning\\");\\n    }\\n});\\n\\n$(\\".edit-wordfind\\").click(function(e) {\\n    e.preventDefault();\\n\\n    $(\\"#pnl-edit\\").show(\\"fast\\");\\n    $(\\"#findword-game\\").hide();\\n\\n    $(\\"#puzzle\\").html(\\"\\");\\n    $(\\"#words\\").html(\\"\\");\\n});\\n\\n$(\\".btn.save-wordfind\\").click(function(e) {\\n    e.preventDefault();\\n    msjes = {\\n        \'attention\' : \'Attention\',\\n        \'guardado_correcto\' : \'Game saved successfully\'\\n    }\\n    saveGame(msjes);\\n});\\n\\nvar iniciarSopaLetras = function(){\\n    var arrWords=[];\\n    if( $(\'#words\').html()!=\'\' ){\\n        $(\'#words\').find(\'li.word\').each(function() {\\n            var word = $(this).text();\\n            arrWords.push(word);\\n        });\\n\\n        var gamePuzzle = wordfindgame.create(arrWords, \'#puzzle\', \'#words\');\\n    }\\n\\n    var valRol = $(\'#hRol\').val();\\n    if( valRol!=\'\' && valRol!=undefined ){\\n        if(valRol != 1){\\n            $(\'.plantilla-sopaletras\').find(\'.nopreview\').remove();\\n        }\\n    }\\n};\\n\\n$(document).ready(function() {\\n    iniciarSopaLetras();\\n});\\n\\n</script>        "}]', 11);
INSERT INTO `tarea_archivos` (`idtarea_archivos`, `tablapadre`, `idpadre`, `nombre`, `ruta`, `tipo`, `puntaje`, `habilidad`, `texto`, `idtarea_archivos_padre`) VALUES
(15, 'R', 2, 'Exam', '__xRUTABASEx__/tarea_archivos/visor/?idarchivo=', 'E', '3.77', '["5","6","7"]', '[{"id":"1","html":{"1":"<img src=\\"__xRUTABASEx__/static/media/image/N1-U1-A1-20170313065148.jpg\\" id=\\"image_question1490456174348\\" class=\\"img-responsive center-block\\">","2":"<div class=\\"titulo_descripcion\\"><b class=\\"npregunta\\">4.- Question  </b><h3 class=\\"titulo\\" data-orden=\\"4\\"></h3><p class=\\"descripcion\\"></p></div><div class=\\"ejercicio plantilla plantilla-completar\\"><div class=\\"panelEjercicio img-thumbnail panel pnl100\\"><p></p>\\n<p>The man is <div class=\\"inp-corregido wrapper\\" data-corregido=\\"good\\"><input data-options=\\"running,eating,dreaming\\" data-mce-object=\\"input\\" height=\\"25\\" width=\\"100\\" type=\\"text\\" data-texto=\\"buying\\" class=\\"mce-object-input valinput isdrop ui-droppable active\\" id=\\"input_1500130873453\\" readonly=\\"readonly\\" value=\\"buying\\"></div></p></div><div class=\\"panelAlternativas img-thumbnail panel pnl100\\"><span class=\\"isdragable ui-draggable ui-draggable-handle active\\" style=\\"position:relative\\">running</span>,<span class=\\"isdragable ui-draggable ui-draggable-handle active\\" style=\\"position:relative\\">buying</span>,<span class=\\"isdragable ui-draggable ui-draggable-handle\\" style=\\"position: relative;\\">eating</span>,<span class=\\"isdragable ui-draggable ui-draggable-handle\\" style=\\"position: relative;\\">dreaming</span></div></div>","3":"<div class=\\"titulo_descripcion\\"><b class=\\"npregunta\\">6.- Question  </b><h3 class=\\"titulo\\" data-orden=\\"6\\">Select exercise</h3><p class=\\"descripcion\\"></p></div><div class=\\"ejercicio plantilla plantilla-completar\\"><div class=\\"panelEjercicio img-thumbnail panel pnl100\\"><p>Hello <div class=\\"inp-corregido wrapper\\" data-corregido=\\"good\\"><input data-options=\\"mundo,hola,none\\" data-mce-object=\\"input\\" height=\\"25\\" width=\\"100\\" type=\\"text\\" data-texto=\\"world\\" class=\\"mce-object-input valinput ischoice active\\" id=\\"input_1500130886025\\" data-num=\\"1\\" readonly=\\"readonly\\" value=\\"world\\"></div></p></div></div>","4":"<div class=\\"titulo_descripcion\\"><b class=\\"npregunta\\">5.- Question  </b><h3 class=\\"titulo\\" data-orden=\\"5\\"></h3><p class=\\"descripcion\\"></p></div><div class=\\"ejercicio plantilla plantilla-verdad_falso\\"><div class=\\"row list-premises tpl_plantilla\\" id=\\"pnl_premisas58d68f71e019c\\" data-idgui=\\"58d68f71e019c\\">\\n        <!-- Premises container -->\\n    <div class=\\"col-xs-12 premise pr-1490456433389 good\\">\\n            <div class=\\"col-xs-12 col-sm-5\\">\\n                <span class=\\"live-edit addtext\\" data-initial_val=\\"Some premise . this is true\\">Some premise . this is true</span>\\n                <input type=\\"hidden\\" class=\\"valPremise valin nopreview\\" data-nopreview=\\".premise.pr-1490456433389\\" data-cini=\\"1\\" data-valueini=\\"Premise\\" value=\\"Some premise . this is true\\">\\n            </div>\\n            <div class=\\"col-xs-12 col-sm-6 options\\">\\n                <label class=\\"col-xs-6\\"><input type=\\"radio\\" class=\\"radio-ctrl\\" name=\\"opcPremise-1490456433389\\" value=\\"T\\" checked=\\"checked\\"> True</label>\\n                <label class=\\"col-xs-6\\"><input type=\\"radio\\" class=\\"radio-ctrl\\" name=\\"opcPremise-1490456433389\\" value=\\"F\\"> False</label>\\n            </div>\\n            <div class=\\"col-xs-2 col-sm-1 icon-zone\\">\\n                <a href=\\"#\\" class=\\"btn color-danger btn-xs tooltip delete nopreview\\" title=\\"remove\\" style=\\"display: none;\\"><i class=\\"fa fa-trash \\"></i></a>\\n                <span class=\\"icon-result\\"></span>\\n            </div>\\n            <input type=\\"hidden\\" id=\\"opcPremise-1490456433389\\" data-lock=\\"1490456444219\\" value=\\"be8b3f73c994e096cf961eae98c2303b\\">\\n        </div><div class=\\"col-xs-12 premise pr-1490456446283 good\\">\\n            <div class=\\"col-xs-12 col-sm-5\\">\\n                <span class=\\"live-edit addtext\\" data-initial_val=\\"a false premise\\">a false premise</span>\\n                <input type=\\"hidden\\" class=\\"valPremise valin nopreview\\" data-nopreview=\\".premise.pr-1490456446283\\" data-cini=\\"1\\" data-valueini=\\"Premise\\" value=\\"a false premise\\">\\n            </div>\\n            <div class=\\"col-xs-12 col-sm-6 options\\">\\n                <label class=\\"col-xs-6\\"><input type=\\"radio\\" class=\\"radio-ctrl\\" name=\\"opcPremise-1490456446283\\" value=\\"T\\"> True</label>\\n                <label class=\\"col-xs-6\\"><input type=\\"radio\\" class=\\"radio-ctrl\\" name=\\"opcPremise-1490456446283\\" value=\\"F\\" checked=\\"checked\\"> False</label>\\n            </div>\\n            <div class=\\"col-xs-2 col-sm-1 icon-zone\\">\\n                <a href=\\"#\\" class=\\"btn color-danger btn-xs tooltip delete nopreview\\" title=\\"remove\\" style=\\"display: none;\\"><i class=\\"fa fa-trash \\"></i></a>\\n                <span class=\\"icon-result\\"></span>\\n            </div>\\n            <input type=\\"hidden\\" id=\\"opcPremise-1490456446283\\" data-lock=\\"1490456454368\\" value=\\"af054ad65df62655f602c1c7c5e36cce\\">\\n        </div><div class=\\"col-xs-12 premise pr-1490456456913 bad\\">\\n            <div class=\\"col-xs-12 col-sm-5\\">\\n                <span class=\\"live-edit addtext\\" data-initial_val=\\"A premise more\\">A premise more</span>\\n                <input type=\\"hidden\\" class=\\"valPremise valin nopreview\\" data-nopreview=\\".premise.pr-1490456456913\\" data-cini=\\"1\\" data-valueini=\\"Premise\\" value=\\"A premise more\\">\\n            </div>\\n            <div class=\\"col-xs-12 col-sm-6 options\\">\\n                <label class=\\"col-xs-6\\"><input type=\\"radio\\" class=\\"radio-ctrl\\" name=\\"opcPremise-1490456456913\\" value=\\"T\\"> True</label>\\n                <label class=\\"col-xs-6\\"><input type=\\"radio\\" class=\\"radio-ctrl\\" name=\\"opcPremise-1490456456913\\" value=\\"F\\" checked=\\"checked\\"> False</label>\\n            </div>\\n            <div class=\\"col-xs-2 col-sm-1 icon-zone\\">\\n                <a href=\\"#\\" class=\\"btn color-danger btn-xs tooltip delete nopreview\\" title=\\"remove\\" style=\\"display: none;\\"><i class=\\"fa fa-trash \\"></i></a>\\n                <span class=\\"icon-result\\"></span>\\n            </div>\\n            <input type=\\"hidden\\" id=\\"opcPremise-1490456456913\\" data-lock=\\"1490456465091\\" value=\\"fe50e6017ffbefa5815800c5b248fe2c\\">\\n        </div></div></div>","5":"<div class=\\"titulo_descripcion\\"><b class=\\"npregunta\\">2.- Question  </b><h3 class=\\"titulo\\" data-orden=\\"2\\"></h3><p class=\\"descripcion\\"></p></div><div class=\\"ejercicio plantilla plantilla-completar\\"><div class=\\"panelEjercicio img-thumbnail panel pnl100\\"><p>The answer is <div class=\\"inp-corregido wrapper\\" data-corregido=\\"good\\"><input data-options=\\"\\" data-mce-object=\\"input\\" height=\\"25\\" width=\\"100\\" type=\\"text\\" data-texto=\\"correct\\" class=\\"mce-object-input valinput iswrite\\" id=\\"input_1500130855784\\" value=\\"correct\\"></div></p></div></div>","6":"<div class=\\"titulo_descripcion\\"><b class=\\"npregunta\\">3.- Question  </b><h3 class=\\"titulo\\" data-orden=\\"3\\"></h3><p class=\\"descripcion\\"></p></div><div class=\\"ejercicio plantilla plantilla-img_puntos\\">\\n      <input type=\\"hidden\\" name=\\"idgui\\" id=\\"idgui\\" value=\\"58d68ff7e7b53\\">\\n      <div class=\\"row\\">\\n        <div class=\\"col-xs-12 botones-edicion nopreview\\" style=\\"display: none;\\">\\n            <a class=\\"btn btn-primary selimage\\" data-tipo=\\"image\\" data-url=\\".img-dots\\">\\n                <i class=\\"fa fa-image\\"></i> \\n                Choose image \\n            </a>\\n            <a class=\\"btn btn-primary start-tag\\" style=\\"display: inline-block;\\"> \\n                <i class=\\"fa fa-dot-circle-o\\"></i>\\n                Start tagging \\n            </a>\\n            <a class=\\"btn btn-red stop-tag\\" style=\\"display: none;\\">\\n                <i class=\\"fa fa-stop\\"></i> \\n                Stop tagging \\n            </a>\\n            <a class=\\"btn btn-danger delete-tag pull-right\\" style=\\"display: block;\\">\\n                <i class=\\"fa fa-trash-o\\"></i> \\n                Delete tags \\n            </a>\\n            <a class=\\"btn btn-danger stop-del-tag pull-right\\" style=\\"display: none;\\">\\n                <i class=\\"fa fa-stop\\"></i> \\n                Stop deleting tags \\n            </a>\\n            <a class=\\"btn btn-success back-edit-tag hidden\\" style=\\"display: none !important;\\">\\n                <i class=\\"fa fa-pencil\\"></i> \\n                Back and edit            </a>\\n\\n            <div class=\\"hidden dot-container edition clone_punto\\" data-id=\\"clone_punto58d68ff7e7b53\\">\\n                <div class=\\"dot\\"></div>\\n                <div class=\\"dot-tag\\"> </div>\\n            </div>\\n        </div>\\n        <div class=\\"col-xs-12 col-md-9 contenedor-img\\">\\n            <picture>\\n                <img src=\\"__xRUTABASEx__/static/media/image/N1-U1-A1-20170313063351.jpg\\" class=\\"img-responsive valvideo img-dots\\" style=\\"display: inline-block;\\" data-nombre-file=\\"family.jpg\\">\\n                <img src=\\"__xRUTABASEx__/static/media/web/noimage.png\\" class=\\"img-responsive nopreview\\" style=\\"width: initial; margin: 0px auto; display: none;\\">\\n            </picture>\\n            <div class=\\"mask-dots playing\\">\\n            <div class=\\"dot-container\\" id=\\"dot_1490456656510\\" style=\\"left: 20.3597%; top: 23.0769%;\\">\\n                <div class=\\"dot\\"></div>\\n                <div class=\\"dot-tag corregido\\" data-number=\\"1\\" data-corregido=\\"good\\">Father</div>\\n            </div><div class=\\"dot-container\\" id=\\"dot_1490456657770\\" style=\\"left: 78.2243%; top: 26.2916%;\\">\\n                <div class=\\"dot\\"></div>\\n                <div class=\\"dot-tag corregido\\" data-number=\\"2\\" data-corregido=\\"good\\">Mother</div>\\n            </div><div class=\\"dot-container\\" id=\\"dot_1490456659086\\" style=\\"left: 53.7313%; top: 52.0092%;\\">\\n                <div class=\\"dot\\"></div>\\n                <div class=\\"dot-tag corregido\\" data-number=\\"3\\" data-corregido=\\"good\\">Son</div>\\n            </div><div class=\\"dot-container\\" id=\\"dot_1490456659986\\" style=\\"left: 40.1072%; top: 45.1206%;\\">\\n                <div class=\\"dot\\"></div>\\n                <div class=\\"dot-tag corregido\\" data-number=\\"4\\" data-corregido=\\"good\\">Daughter</div>\\n            </div></div>\\n        </div>\\n\\n        <div class=\\"col-xs-12 col-md-3 tag-alts-edit nopreview\\" style=\\"display: none;\\"> <div class=\\"input-group\\" id=\\"edit_tag_1490456656510\\"><span class=\\"input-group-addon\\">1</span><input type=\\"text\\" class=\\"form-control\\" placeholder=\\"Write tag\\" value=\\"Father\\"></div><div class=\\"input-group\\" id=\\"edit_tag_1490456657770\\"><span class=\\"input-group-addon\\">2</span><input type=\\"text\\" class=\\"form-control\\" placeholder=\\"Write tag\\" value=\\"Mother\\"></div><div class=\\"input-group\\" id=\\"edit_tag_1490456659086\\"><span class=\\"input-group-addon\\">3</span><input type=\\"text\\" class=\\"form-control\\" placeholder=\\"Write tag\\" value=\\"Son\\"></div><div class=\\"input-group\\" id=\\"edit_tag_1490456659986\\"><span class=\\"input-group-addon\\">4</span><input type=\\"text\\" class=\\"form-control\\" placeholder=\\"Write tag\\" value=\\"Daughter\\"></div></div>\\n\\n        <div class=\\"col-xs-12 col-md-3 tag-alts\\" style=\\"\\"><div class=\\"tag\\" id=\\"tag_1490456659986\\"><div class=\\"arrow\\"></div>Daughter</div><div class=\\"tag\\" id=\\"tag_1490456657770\\"><div class=\\"arrow\\"></div>Mother</div><div class=\\"tag\\" id=\\"tag_1490456659086\\"><div class=\\"arrow\\"></div>Son</div><div class=\\"tag\\" id=\\"tag_1490456656510\\"><div class=\\"arrow\\"></div>Father</div></div>\\n      </div>\\n    </div>","7":"<div class=\\"titulo_descripcion\\"><b class=\\"npregunta\\">1.- Question  </b><h3 class=\\"titulo\\" data-orden=\\"1\\"></h3><p class=\\"descripcion\\"></p></div><div class=\\"ejercicio plantilla plantilla-fichas\\">\\n        <input type=\\"hidden\\" name=\\"idgui\\" id=\\"idgui\\" value=\\"58d690c166fd8\\">\\n        <div class=\\"row nopreview _lista-fichas\\" id=\\"lista-fichas58d690c166fd8\\" style=\\"display: none;\\">\\n\\n        <div class=\\"col-xs-12 ficha-edit\\" id=\\"f_1490456768634\\">\\n                <div class=\\"col-xs-12 col-sm-11\\">\\n                    <div class=\\"col-xs-12 part-1\\">\\n                        <div class=\\"col-xs-12 col-sm-5 col-md-3 btns-media\\">\\n                            <div class=\\"col-xs-6\\">\\n                                <a href=\\"#\\" class=\\"btn btn-default btn-block selectmedia istooltip\\" data-tipo=\\"image\\" title=\\"select image\\" data-url=\\".img_1_1490456768634\\"><i class=\\"fa fa-picture-o\\"></i></a>\\n                                <img src=\\"\\" class=\\"hidden    img_1_1490456768634\\">\\n                            </div>\\n                            <div class=\\"col-xs-6\\">\\n                                <a href=\\"#\\" class=\\"btn btn-default btn-block selectmedia    istooltip\\" data-tipo=\\"audio\\" title=\\"select audio\\" data-url=\\".audio_1_1490456768634\\"><i class=\\"fa fa-headphones\\"></i></a>\\n                                <audio src=\\"\\" class=\\"hidden    audio_1_1490456768634\\"></audio>\\n                            </div>\\n                        </div>\\n                        <div class=\\"col-xs-12 col-sm-7 col-md-9\\">\\n                            <input type=\\"text\\" class=\\"form-control    txt_1_1490456768634\\" name=\\"txtPalabra\\" value=\\"Greetings\\">\\n                        </div>\\n                    </div>\\n\\n                    <div class=\\"col-xs-12 part-2\\">\\n                        <hr>\\n                        <div class=\\"col-xs-12 ficha-row\\">\\n                            <div class=\\"col-xs-12 col-sm-5 col-md-3 btns-media\\">\\n                                <div class=\\"col-xs-6\\">\\n                                    <a href=\\"#\\" class=\\"btn btn-default btn-block selectmedia istooltip\\" data-tipo=\\"image\\" title=\\"select image\\" data-url=\\".img_2_1490456768634\\"><i class=\\"fa fa-picture-o\\"></i></a>\\n                                    <img src=\\"\\" class=\\"hidden    img_2_1490456768634\\">\\n                                </div>\\n                                <div class=\\"col-xs-6\\">\\n                                    <a href=\\"#\\" class=\\"btn btn-default btn-block selectmedia     istooltip\\" data-tipo=\\"audio\\" title=\\"select audio\\" data-url=\\".audio_2_1490456768634\\"><i class=\\"fa fa-headphones\\"></i></a>\\n                                    <audio src=\\"\\" class=\\"hidden    audio_2_1490456768634\\"></audio>\\n                                </div>\\n                            </div>\\n                            <div class=\\"col-xs-12 col-sm-7 col-md-9\\">\\n                                <input type=\\"text\\" class=\\"form-control    txt_2_1490456768634\\" name=\\"txtPalabra\\" value=\\"Hello\\">\\n                            </div>\\n                        </div>\\n                    <div class=\\"col-xs-12 ficha-row\\">\\n                <div class=\\"col-xs-12 col-sm-5 col-md-3 btns-media\\">\\n                    <div class=\\"col-xs-6\\">\\n                        <a href=\\"#\\" class=\\"btn btn-default btn-block selectmedia istooltip\\" data-tipo=\\"image\\" title=\\"Select image\\" data-url=\\".img_2_1490456768634_1490456789814\\"><i class=\\"fa fa-picture-o\\"></i></a>\\n                        <img src=\\"\\" class=\\"hidden    img_2_1490456768634_1490456789814\\">\\n                    </div>\\n                    <div class=\\"col-xs-6\\">\\n                        <a href=\\"#\\" class=\\"btn btn-default btn-block selectmedia istooltip\\" data-tipo=\\"audio\\" title=\\"select audio\\" data-url=\\".audio_2_1490456768634_1490456789814\\"><i class=\\"fa fa-headphones\\"></i></a>\\n                        <audio src=\\"\\" class=\\"hidden    audio_2_1490456768634_1490456789814\\"></audio>\\n                    </div>\\n                </div>\\n                <div class=\\"col-xs-10 col-sm-5 col-md-8\\">\\n                    <input type=\\"text\\" class=\\"form-control    txt_2_1490456768634_1490456789814\\" name=\\"txtPalabra\\" value=\\"Hi\\">\\n                </div>\\n                <div class=\\"col-xs-2 col-sm-2 col-md-1\\">\\n                    <a href=\\"#\\" class=\\"btn color-danger delete-ficha-row istooltip\\" title=\\"discard row\\"><i class=\\"fa fa-trash\\"></i></a>\\n                </div>\\n            </div><div class=\\"col-xs-12 ficha-row\\">\\n                <div class=\\"col-xs-12 col-sm-5 col-md-3 btns-media\\">\\n                    <div class=\\"col-xs-6\\">\\n                        <a href=\\"#\\" class=\\"btn btn-default btn-block selectmedia istooltip\\" data-tipo=\\"image\\" title=\\"Select image\\" data-url=\\".img_2_1490456768634_1490456794309\\"><i class=\\"fa fa-picture-o\\"></i></a>\\n                        <img src=\\"\\" class=\\"hidden    img_2_1490456768634_1490456794309\\">\\n                    </div>\\n                    <div class=\\"col-xs-6\\">\\n                        <a href=\\"#\\" class=\\"btn btn-default btn-block selectmedia istooltip\\" data-tipo=\\"audio\\" title=\\"select audio\\" data-url=\\".audio_2_1490456768634_1490456794309\\"><i class=\\"fa fa-headphones\\"></i></a>\\n                        <audio src=\\"\\" class=\\"hidden    audio_2_1490456768634_1490456794309\\"></audio>\\n                    </div>\\n                </div>\\n                <div class=\\"col-xs-10 col-sm-5 col-md-8\\">\\n                    <input type=\\"text\\" class=\\"form-control    txt_2_1490456768634_1490456794309\\" name=\\"txtPalabra\\" value=\\"Good morning\\">\\n                </div>\\n                <div class=\\"col-xs-2 col-sm-2 col-md-1\\">\\n                    <a href=\\"#\\" class=\\"btn color-danger delete-ficha-row istooltip\\" title=\\"discard row\\"><i class=\\"fa fa-trash\\"></i></a>\\n                </div>\\n            </div></div>\\n                </div>\\n                <div class=\\"col-xs-12 col-sm-1 btns-ficha\\">\\n                    <a href=\\"#\\" class=\\"col-xs-6 col-sm-12 btn btn-danger delete-ficha istooltip\\" title=\\"discard sheet\\"><i class=\\"fa fa-trash-o\\"></i></a>\\n\\n                    <a href=\\"#\\" class=\\"col-xs-6 col-sm-12 btn btn-primary add-ficha-row istooltip\\" data-clone-from=\\"#clone_row\\" data-clone-to=\\"#f_1490456768634 .part-2\\" title=\\"add row\\"><i class=\\"fa fa-plus-square-o\\"></i></a>\\n                </div>\\n            </div><div class=\\"col-xs-12 ficha-edit\\" id=\\"f_1490456805546\\">\\n                <div class=\\"col-xs-12 col-sm-11\\">\\n                    <div class=\\"col-xs-12 part-1\\">\\n                        <div class=\\"col-xs-12 col-sm-5 col-md-3 btns-media\\">\\n                            <div class=\\"col-xs-6\\">\\n                                <a href=\\"#\\" class=\\"btn btn-default btn-block selectmedia istooltip\\" data-tipo=\\"image\\" title=\\"select image\\" data-url=\\".img_1_1490456805546\\"><i class=\\"fa fa-picture-o\\"></i></a>\\n                                <img src=\\"\\" class=\\"hidden    img_1_1490456805546\\">\\n                            </div>\\n                            <div class=\\"col-xs-6\\">\\n                                <a href=\\"#\\" class=\\"btn btn-default btn-block selectmedia    istooltip\\" data-tipo=\\"audio\\" title=\\"select audio\\" data-url=\\".audio_1_1490456805546\\"><i class=\\"fa fa-headphones\\"></i></a>\\n                                <audio src=\\"\\" class=\\"hidden    audio_1_1490456805546\\"></audio>\\n                            </div>\\n                        </div>\\n                        <div class=\\"col-xs-12 col-sm-7 col-md-9\\">\\n                            <input type=\\"text\\" class=\\"form-control    txt_1_1490456805546\\" name=\\"txtPalabra\\" value=\\"Farewells\\">\\n                        </div>\\n                    </div>\\n\\n                    <div class=\\"col-xs-12 part-2\\">\\n                        <hr>\\n                        <div class=\\"col-xs-12 ficha-row\\">\\n                            <div class=\\"col-xs-12 col-sm-5 col-md-3 btns-media\\">\\n                                <div class=\\"col-xs-6\\">\\n                                    <a href=\\"#\\" class=\\"btn btn-default btn-block selectmedia istooltip\\" data-tipo=\\"image\\" title=\\"select image\\" data-url=\\".img_2_1490456805546\\"><i class=\\"fa fa-picture-o\\"></i></a>\\n                                    <img src=\\"\\" class=\\"hidden    img_2_1490456805546\\">\\n                                </div>\\n                                <div class=\\"col-xs-6\\">\\n                                    <a href=\\"#\\" class=\\"btn btn-default btn-block selectmedia     istooltip\\" data-tipo=\\"audio\\" title=\\"select audio\\" data-url=\\".audio_2_1490456805546\\"><i class=\\"fa fa-headphones\\"></i></a>\\n                                    <audio src=\\"\\" class=\\"hidden    audio_2_1490456805546\\"></audio>\\n                                </div>\\n                            </div>\\n                            <div class=\\"col-xs-12 col-sm-7 col-md-9\\">\\n                                <input type=\\"text\\" class=\\"form-control    txt_2_1490456805546\\" name=\\"txtPalabra\\" value=\\"Bye\\">\\n                            </div>\\n                        </div>\\n                    <div class=\\"col-xs-12 ficha-row\\">\\n                <div class=\\"col-xs-12 col-sm-5 col-md-3 btns-media\\">\\n                    <div class=\\"col-xs-6\\">\\n                        <a href=\\"#\\" class=\\"btn btn-default btn-block selectmedia istooltip\\" data-tipo=\\"image\\" title=\\"Select image\\" data-url=\\".img_2_1490456805546_1490456815268\\"><i class=\\"fa fa-picture-o\\"></i></a>\\n                        <img src=\\"\\" class=\\"hidden    img_2_1490456805546_1490456815268\\">\\n                    </div>\\n                    <div class=\\"col-xs-6\\">\\n                        <a href=\\"#\\" class=\\"btn btn-default btn-block selectmedia istooltip\\" data-tipo=\\"audio\\" title=\\"select audio\\" data-url=\\".audio_2_1490456805546_1490456815268\\"><i class=\\"fa fa-headphones\\"></i></a>\\n                        <audio src=\\"\\" class=\\"hidden    audio_2_1490456805546_1490456815268\\"></audio>\\n                    </div>\\n                </div>\\n                <div class=\\"col-xs-10 col-sm-5 col-md-8\\">\\n                    <input type=\\"text\\" class=\\"form-control    txt_2_1490456805546_1490456815268\\" name=\\"txtPalabra\\" value=\\"Good Bye\\">\\n                </div>\\n                <div class=\\"col-xs-2 col-sm-2 col-md-1\\">\\n                    <a href=\\"#\\" class=\\"btn color-danger delete-ficha-row istooltip\\" title=\\"discard row\\"><i class=\\"fa fa-trash\\"></i></a>\\n                </div>\\n            </div><div class=\\"col-xs-12 ficha-row\\">\\n                <div class=\\"col-xs-12 col-sm-5 col-md-3 btns-media\\">\\n                    <div class=\\"col-xs-6\\">\\n                        <a href=\\"#\\" class=\\"btn btn-default btn-block selectmedia istooltip\\" data-tipo=\\"image\\" title=\\"Select image\\" data-url=\\".img_2_1490456805546_1490456819885\\"><i class=\\"fa fa-picture-o\\"></i></a>\\n                        <img src=\\"\\" class=\\"hidden    img_2_1490456805546_1490456819885\\">\\n                    </div>\\n                    <div class=\\"col-xs-6\\">\\n                        <a href=\\"#\\" class=\\"btn btn-default btn-block selectmedia istooltip\\" data-tipo=\\"audio\\" title=\\"select audio\\" data-url=\\".audio_2_1490456805546_1490456819885\\"><i class=\\"fa fa-headphones\\"></i></a>\\n                        <audio src=\\"\\" class=\\"hidden    audio_2_1490456805546_1490456819885\\"></audio>\\n                    </div>\\n                </div>\\n                <div class=\\"col-xs-10 col-sm-5 col-md-8\\">\\n                    <input type=\\"text\\" class=\\"form-control    txt_2_1490456805546_1490456819885\\" name=\\"txtPalabra\\" value=\\"See you later\\">\\n                </div>\\n                <div class=\\"col-xs-2 col-sm-2 col-md-1\\">\\n                    <a href=\\"#\\" class=\\"btn color-danger delete-ficha-row istooltip\\" title=\\"discard row\\"><i class=\\"fa fa-trash\\"></i></a>\\n                </div>\\n            </div></div>\\n                </div>\\n                <div class=\\"col-xs-12 col-sm-1 btns-ficha\\">\\n                    <a href=\\"#\\" class=\\"col-xs-6 col-sm-12 btn btn-danger delete-ficha istooltip\\" title=\\"discard sheet\\"><i class=\\"fa fa-trash-o\\"></i></a>\\n\\n                    <a href=\\"#\\" class=\\"col-xs-6 col-sm-12 btn btn-primary add-ficha-row istooltip\\" data-clone-from=\\"#clone_row\\" data-clone-to=\\"#f_1490456805546 .part-2\\" title=\\"add row\\"><i class=\\"fa fa-plus-square-o\\"></i></a>\\n                </div>\\n            </div></div>\\n        <div class=\\"row nopreview\\">\\n            <div class=\\"col-xs-12 text-center botones-creacion\\" style=\\"display: none;\\">\\n                <a href=\\"#\\" class=\\"btn btn-primary add-ficha\\" data-clone-from=\\"#clone\\" data-clone-to=\\"._lista-fichas\\"><i class=\\"fa fa-plus-square\\"></i> Add group of sheets</a>\\n            </div>\\n\\n            <div class=\\"col-xs-12 text-center botones-editar hidden\\" style=\\"display: none !important;\\">\\n                <a href=\\"#\\" class=\\"btn btn-success back-edit\\"><i class=\\"fa fa-pencil\\"></i> back and edit</a>\\n            </div>\\n\\n            <div class=\\"hidden col-xs-12 ficha-edit\\" data-clase=\\"f_\\" id=\\"clone\\">\\n                <div class=\\"col-xs-12 col-sm-11\\">\\n                    <div class=\\"col-xs-12 part-1\\">\\n                        <div class=\\"col-xs-12 col-sm-5 col-md-3 btns-media\\">\\n                            <div class=\\"col-xs-6\\">\\n                                <a href=\\"#\\" class=\\"btn btn-default btn-block selectmedia renameDataUrl istooltip\\" data-tipo=\\"image\\" data-url=\\".img_1_\\" title=\\"select image\\"><i class=\\"fa fa-picture-o\\"></i></a>\\n                                <img src=\\"\\" class=\\"hidden    renameClass\\" data-clase=\\"img_1_\\">\\n                            </div>\\n                            <div class=\\"col-xs-6\\">\\n                                <a href=\\"#\\" class=\\"btn btn-default btn-block selectmedia    renameDataUrl istooltip\\" data-tipo=\\"audio\\" data-url=\\".audio_1_\\" title=\\"select audio\\"><i class=\\"fa fa-headphones\\"></i></a>\\n                                <audio src=\\"\\" class=\\"hidden    renameClass\\" data-clase=\\"audio_1_\\"></audio>\\n                            </div>\\n                        </div>\\n                        <div class=\\"col-xs-12 col-sm-7 col-md-9\\">\\n                            <input type=\\"text\\" class=\\"form-control    renameClass\\" data-clase=\\"txt_1_\\" name=\\"txtPalabra\\">\\n                        </div>\\n                    </div>\\n\\n                    <div class=\\"col-xs-12 part-2\\">\\n                        <hr>\\n                        <div class=\\"col-xs-12 ficha-row\\">\\n                            <div class=\\"col-xs-12 col-sm-5 col-md-3 btns-media\\">\\n                                <div class=\\"col-xs-6\\">\\n                                    <a href=\\"#\\" class=\\"btn btn-default btn-block selectmedia renameDataUrl istooltip\\" data-tipo=\\"image\\" data-url=\\".img_2_\\" title=\\"select image\\"><i class=\\"fa fa-picture-o\\"></i></a>\\n                                    <img src=\\"\\" class=\\"hidden    renameClass\\" data-clase=\\"img_2_\\">\\n                                </div>\\n                                <div class=\\"col-xs-6\\">\\n                                    <a href=\\"#\\" class=\\"btn btn-default btn-block selectmedia     renameDataUrl istooltip\\" data-tipo=\\"audio\\" data-url=\\".audio_2_\\" title=\\"select audio\\"><i class=\\"fa fa-headphones\\"></i></a>\\n                                    <audio src=\\"\\" class=\\"hidden    renameClass\\" data-clase=\\"audio_2_\\"></audio>\\n                                </div>\\n                            </div>\\n                            <div class=\\"col-xs-12 col-sm-7 col-md-9\\">\\n                                <input type=\\"text\\" class=\\"form-control    renameClass\\" data-clase=\\"txt_2_\\" name=\\"txtPalabra\\">\\n                            </div>\\n                        </div>\\n                    </div>\\n                </div>\\n                <div class=\\"col-xs-12 col-sm-1 btns-ficha\\">\\n                    <a href=\\"#\\" class=\\"col-xs-6 col-sm-12 btn btn-danger delete-ficha istooltip\\" title=\\"discard sheet\\"><i class=\\"fa fa-trash-o\\"></i></a>\\n\\n                    <a href=\\"#\\" class=\\"col-xs-6 col-sm-12 btn btn-primary add-ficha-row istooltip\\" data-clone-from=\\"#clone_row\\" data-clone-to=\\"#f_ .part-2\\" title=\\"add row\\"><i class=\\"fa fa-plus-square-o\\"></i></a>\\n                </div>\\n            </div>\\n\\n            <div class=\\"hidden col-xs-12 ficha-row\\" id=\\"clone_row\\">\\n                <div class=\\"col-xs-12 col-sm-5 col-md-3 btns-media\\">\\n                    <div class=\\"col-xs-6\\">\\n                        <a href=\\"#\\" class=\\"btn btn-default btn-block selectmedia renameDataUrl istooltip\\" data-tipo=\\"image\\" data-url=\\".img_2_\\" title=\\"Select image\\"><i class=\\"fa fa-picture-o\\"></i></a>\\n                        <img src=\\"\\" class=\\"hidden    renameClass\\" data-clase=\\"img_2_\\">\\n                    </div>\\n                    <div class=\\"col-xs-6\\">\\n                        <a href=\\"#\\" class=\\"btn btn-default btn-block selectmedia renameDataUrl istooltip\\" data-tipo=\\"audio\\" data-url=\\".audio_2_\\" title=\\"select audio\\"><i class=\\"fa fa-headphones\\"></i></a>\\n                        <audio src=\\"\\" class=\\"hidden    renameClass\\" data-clase=\\"audio_2_\\"></audio>\\n                    </div>\\n                </div>\\n                <div class=\\"col-xs-10 col-sm-5 col-md-8\\">\\n                    <input type=\\"text\\" class=\\"form-control    renameClass\\" data-clase=\\"txt_2_\\" name=\\"txtPalabra\\">\\n                </div>\\n                <div class=\\"col-xs-2 col-sm-2 col-md-1\\">\\n                    <a href=\\"#\\" class=\\"btn color-danger delete-ficha-row istooltip\\" title=\\"discard row\\"><i class=\\"fa fa-trash\\"></i></a>\\n                </div>\\n            </div>\\n\\n            <div class=\\"hidden ficha\\" data-key=\\"\\" id=\\"to_generate\\">\\n                <img src=\\"\\" class=\\"img-responsive hide\\">\\n                <a href=\\"#\\" class=\\"btn btn-orange btn-block hide\\"><i class=\\"fa fa-play\\"></i></a>\\n                <p class=\\"hide\\">word</p>\\n            </div>\\n        </div>\\n        <div class=\\"row ejerc-fichas tpl_plantilla contenido\\" id=\\"ejerc-fichas58d690c166fd8\\" style=\\"display: block;\\">\\n            <div class=\\"col-xs-12 partes-1\\"><div class=\\"ficha\\" data-key=\\"1490456768634\\" data-color=\\"blue\\">\\n                \\n                \\n                <p>Greetings</p>\\n            </div><div class=\\"ficha\\" data-key=\\"1490456805546\\" data-color=\\"red\\">\\n                \\n                \\n                <p>Farewells</p>\\n            </div></div>\\n            <div class=\\"col-xs-12 partes-2\\"><div class=\\"ficha active good\\" data-key=\\"1490456768634\\" data-color=\\"blue\\">\\n                \\n                \\n                <p>Hello</p>\\n            </div><div class=\\"ficha active good\\" data-key=\\"1490456805546\\" data-color=\\"red\\">\\n                \\n                \\n                <p>Bye</p>\\n            </div><div class=\\"ficha active good\\" data-key=\\"1490456768634\\" data-color=\\"blue\\">\\n                \\n                \\n                <p>Hi</p>\\n            </div><div class=\\"ficha active good\\" data-key=\\"1490456805546\\" data-color=\\"red\\">\\n                \\n                \\n                <p>Good Bye</p>\\n            </div><div class=\\"ficha active good\\" data-key=\\"1490456805546\\" data-color=\\"red\\">\\n                \\n                \\n                <p>See you later</p>\\n            </div><div class=\\"ficha active good\\" data-key=\\"1490456768634\\" data-color=\\"blue\\">\\n                \\n                \\n                <p>Good morning</p>\\n            </div></div>\\n\\n            <audio src=\\"\\" class=\\"hidden\\" id=\\"audio-ejercicio58d690c166fd8\\" style=\\"display: none;\\"></audio>\\n        </div>\\n    </div>"}}]', 12),
(19, 'T', 4, 'Manual de uso del TMS cb.pdf', '__xRUTABASEx__/static/tarea_archivos/documento/20170718103913_Manual de uso del TMS cb.pdf', 'D', NULL, NULL, NULL, NULL),
(20, 'T', 4, 'N1-U1-A1-20170121105015.mp4', '__xRUTABASEx__/static/tarea_archivos/video/20170718104056_N1-U1-A1-20170121105015.mp4', 'V', NULL, NULL, NULL, NULL),
(21, 'T', 4, 'Más educativo que tu Cole', 'https://www.youtube.com/watch?v=y4tLcb0tI7g', 'L', NULL, NULL, NULL, NULL),
(22, 'T', 4, 'Activity', '__xRUTABASEx__/tarea_archivos/visor/?idarchivo=', 'A', NULL, NULL, '[114,115,116,117]', NULL),
(23, 'T', 4, 'Game', '__xRUTABASEx__/tarea_archivos/visor/?idarchivo=', 'J', NULL, NULL, '[11]', NULL),
(24, 'T', 5, 'Logo original dret.pdf', '__xRUTABASEx__/static/tarea_archivos/documento/20170720095933_Logo original dret.pdf', 'D', NULL, NULL, NULL, NULL),
(25, 'T', 5, 'YouTube', 'www.youtube.com', 'L', NULL, NULL, NULL, NULL),
(26, 'T', 5, 'Activity', '__xRUTABASEx__/tarea_archivos/visor/?idarchivo=', 'A', NULL, NULL, '[89,86,83]', NULL),
(27, 'R', 4, 'Logo original dret.pdf', '__xRUTABASEx__/static/tarea_archivos/documento/20170720100250_Logo original dret.pdf', 'D', NULL, NULL, NULL, NULL);
INSERT INTO `tarea_archivos` (`idtarea_archivos`, `tablapadre`, `idpadre`, `nombre`, `ruta`, `tipo`, `puntaje`, `habilidad`, `texto`, `idtarea_archivos_padre`) VALUES
(28, 'R', 4, 'Activity', '__xRUTABASEx__/tarea_archivos/visor/?idarchivo=', 'A', '44.40', NULL, '[{"id":"89","html":"\\n                        <div class=\\"row\\"> <div class=\\"col-xs-12\\"><div class=\\"tab-title-zone pw1_3\\"> <div class=\\"col-md-12\\"><h3 class=\\"addtext addtexttitulo2 showreset\\" data-initial_val=\\"first or second person singular\\">first or second person singular</h3></div><div class=\\"space-line pw1_3\\"></div></div><div class=\\"tab-description pw1_3\\"> <div class=\\"col-md-12\\"><p class=\\"addtext addtextdescription23 showreset\\" data-initial_val=\\"Choose first or second person singular.\\">Choose first or second person singular.</p></div></div><div class=\\"aquicargaplantilla\\" id=\\"tmppt_3_10\\" data-tpl=\\"dby_multiplantilla\\" data-puntaje=\\"0\\" data-time=\\"01:00\\" data-showpuntaje=\\"1\\" data-showtime=\\"1\\" data-addclass=\\"isclicked\\" data-tpltitulo=\\"Options-Text\\" data-tools=\\"chingoinput\\" data-showalternativas=\\"1\\" data-showasistido=\\"0\\" data-helpvideo=\\"5\\" data-intentos=\\"3\\"><input type=\\"hidden\\" id=\\"sysidorden\\" name=\\"orden[3][10]\\" value=\\"10\\"><input type=\\"hidden\\" id=\\"sysdettipo\\" name=\\"det_tipo[3][10]\\" value=\\"Do it by yourself\\"><input type=\\"hidden\\" id=\\"systipodesarrollo\\" name=\\"tipo_desarrollo[3][10]\\" value=\\"Options-Text\\"><input type=\\"hidden\\" id=\\"systipoactividad\\" name=\\"tipo_actividad[3][10]\\" value=\\"A\\"><input type=\\"hidden\\" id=\\"sysiduser\\" name=\\"iduser[3][10]\\" value=\\"99999999\\"><input type=\\"hidden\\" id=\\"idgui\\" name=\\"idgui\\" value=\\"58acd18db459f\\"><input type=\\"hidden\\" id=\\"iddetalle_3_10\\" name=\\"iddetalle[3][10]\\" value=\\"89\\"><link href=\\"__xRUTABASEx__/frontend/plantillas/tema1/css/actividad_completar.css\\" rel=\\"stylesheet\\">\\n\\n\\n\\n\\n\\n\\n<div class=\\"plantilla plantilla-completar tiempo-pausado\\" id=\\"tmp_58acd18db459f\\" data-idgui=\\"58acd18db459f\\" data-tipo-tmp=\\"tipo_2\\">\\n  <div class=\\"row\\">\\n    <div class=\\"col-md-12\\">\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n      <div class=\\"tpl_plantilla\\" style=\\"display: block;\\" data-idgui=\\"58acd18db459f\\">\\n        <div class=\\"row active\\" id=\\"tpl58acd18db459f\\">\\n          <div class=\\"col-md-12 col-sm-12 col-xs-12 discol1\\" id=\\"txt58acd18db459f\\" style=\\"padding: 1ex;\\">\\n            <div class=\\"col-md-12 col-sm-12 col-xs-12 removelineedit img-thumbnail\\">\\n              <div class=\\"panel panelEjercicio\\" id=\\"pnl_edithtml58acd18db459f\\"><p>You can use my hairbrush.</p>\\n<p><div class=\\"inp-corregido wrapper\\" data-corregido=\\"good\\"><input width=\\"100\\" height=\\"25\\" class=\\"mce-object-input valinput isclicked\\" type=\\"text\\" data-texto=\\"Second person\\" data-mce-object=\\"input\\" data-options=\\"First person,None of the above\\" value=\\"Second person\\"><span class=\\"inner-icon\\"><i class=\\"fa fa-check color-green2\\"></i></span></div></p></div>              \\n            </div>            \\n          </div>\\n          <div class=\\"col-md-12 col-sm-12 col-xs-12 discol2\\" id=\\"alter58acd18db459f\\" style=\\"padding: 1ex;\\">\\n            <div class=\\"col-md-12 col-sm-12 col-xs-12 removelineedit2 tpl_alternatives img-thumbnail\\">\\n              <div class=\\"panel panelAlternativas text-center\\" id=\\"pnl_editalternatives58acd18db459f\\"><ul class=\\"alternativas-list\\"><li class=\\"alt-item\\"><span class=\\"alt-letra\\">a) </span><a href=\\"#\\">First person</a></li><li class=\\"alt-item\\"><span class=\\"alt-letra\\">b) </span><div class=\\"inp-corregido wrapper\\" data-corregido=\\"good\\"><a href=\\"#\\">Second person</a><span class=\\"inner-icon\\"><i class=\\"fa fa-check color-green2\\"></i></span></div></li><li class=\\"alt-item\\"><span class=\\"alt-letra\\">c) </span><a href=\\"#\\">None of the above</a></li></ul></div>\\n            </div>\\n          </div>\\n        </div>        \\n      </div>       \\n      <textarea class=\\"txtareaedit\\" id=\\"txtarea58acd18db459f\\" aria-hidden=\\"true\\" style=\\"display: none;\\">&lt;p&gt;You can use my hairbrush.&lt;/p&gt;\\n&lt;p&gt;&lt;input data-options=\\"First person,None of the above\\" data-mce-object=\\"input\\" data-texto=\\"Second person\\" type=\\"text\\" class=\\"mce-object-input valinput isclicked\\" height=\\"25\\" width=\\"100\\"&gt;&lt;/input&gt;&lt;/p&gt;</textarea>\\n    </div>\\n  </div>\\n</div>\\n<script type=\\"text/javascript\\">\\n$(document).ready(function(){\\n  iniciarCompletar_DBY(\'58acd18db459f\');\\n});\\n</script></div><input name=\\"habilidades[3][10]\\" class=\\"selected-skills\\" id=\\"habilidades_met-3_10\\" type=\\"hidden\\" value=\\"0\\"></div></div>                    "},{"id":"86","html":"\\n                        <div class=\\"row\\"> <div class=\\"col-xs-12\\"><div class=\\"tab-title-zone pw1_3\\"> <div class=\\"col-md-12\\"><h3 class=\\"addtext addtexttitulo2 showreset\\" data-initial_val=\\"Ordering sentences in Simple Present\\">Ordering sentences in Simple Present</h3></div><div class=\\"space-line pw1_3\\"></div></div><div class=\\"tab-description pw1_3\\"> <div class=\\"col-md-12\\"><p class=\\"addtext addtextdescription23 showreset\\" data-initial_val=\\"Put the words in the correct order\\">Put the words in the correct order</p></div></div><div class=\\"aquicargaplantilla\\" id=\\"tmppt_3_7\\" data-tpl=\\"dby_ordenar_simple\\" data-puntaje=\\"0\\" data-time=\\"01:00\\" data-showpuntaje=\\"1\\" data-showtime=\\"1\\" data-addclass=\\"\\" data-tpltitulo=\\"Other activities-Put in order\\" data-tools=\\"\\" data-showalternativas=\\"0\\" data-showasistido=\\"0\\" data-helpvideo=\\"19\\" data-intentos=\\"3\\"><input type=\\"hidden\\" id=\\"sysidorden\\" name=\\"orden[3][7]\\" value=\\"7\\"><input type=\\"hidden\\" id=\\"sysdettipo\\" name=\\"det_tipo[3][7]\\" value=\\"Do it by yourself\\"><input type=\\"hidden\\" id=\\"systipodesarrollo\\" name=\\"tipo_desarrollo[3][7]\\" value=\\"Other activities-Put in order\\"><input type=\\"hidden\\" id=\\"systipoactividad\\" name=\\"tipo_actividad[3][7]\\" value=\\"A\\"><input type=\\"hidden\\" id=\\"sysiduser\\" name=\\"iduser[3][7]\\" value=\\"99999999\\"><input type=\\"hidden\\" id=\\"idgui\\" name=\\"idgui\\" value=\\"58accc9b735fe\\"><input type=\\"hidden\\" id=\\"iddetalle_3_7\\" name=\\"iddetalle[3][7]\\" value=\\"86\\"><link href=\\"__xRUTABASEx__/frontend/plantillas/tema1/css/actividad_ordenar.css\\" rel=\\"stylesheet\\">\\n\\n\\n\\n\\n\\n\\n\\n<div class=\\"plantilla plantilla-ordenar ord_simple tiempo-pausado\\" id=\\"tmp_58accc9b735fe\\" data-idgui=\\"58accc9b735fe\\" data-tipo-tmp=\\"ordenar_simple\\">\\n    \\n    \\n    <div class=\\"row ejerc-ordenar tpl_plantilla\\" id=\\"ejerc-ordenar58accc9b735fe\\" style=\\"display: block;\\"><div class=\\"col-xs-12 element\\" id=\\"e_1487719577256\\" data-resp=\\"The girls have new dolls.\\" data-corregido=\\"bad\\">\\n            <div class=\\"col-xs-12 col-sm-4 multimedia hide\\">\\n                <img class=\\"img-responsive hide\\" src=\\"\\">\\n                <a class=\\"btn btn-orange btn-block hide\\" href=\\"#\\" data-tooltip=\\"tooltip\\"><i class=\\"fa fa-play\\"></i></a>\\n            </div>\\n            <div class=\\"col-xs-12 col-sm-8 texto\\">\\n                <div class=\\"drag\\"></div>\\n                <div class=\\"drop bad\\">\\n                <div class=\\"parte blank word bad\\"><div>The</div></div><div class=\\"parte blank word bad\\"><div>have</div></div><div class=\\"parte blank word bad\\"><div>girls</div></div><div class=\\"parte blank word bad\\"><div>dolls.</div></div><div class=\\"parte blank word bad\\"><div>new</div></div></div>\\n            </div>\\n        </div></div>\\n    \\n    <audio class=\\"hidden\\" id=\\"audio-ordenar58accc9b735fe\\" style=\\"display: none;\\" src=\\"\\"></audio>\\n</div>\\n<script> $(document).ready(function(){ initOrdenarSimple(\'58accc9b735fe\', false); }); </script></div><input name=\\"habilidades[3][7]\\" class=\\"selected-skills\\" id=\\"habilidades_met-3_7\\" type=\\"hidden\\" value=\\"5|8\\"></div></div>                    "},{"id":"83","html":"\\n                        <div class=\\"row\\"> <div class=\\"col-xs-12\\"><div class=\\"tab-title-zone pw1_3\\"> <div class=\\"col-md-12\\"><h3 class=\\"addtext addtexttitulo2 showreset\\" data-initial_val=\\"Verb &quot;to be&quot; --Simple Present\\">Verb \\"to be\\" --Simple Present</h3></div><div class=\\"space-line pw1_3\\"></div></div><div class=\\"tab-description pw1_3\\"> <div class=\\"col-md-12\\"><p class=\\"addtext addtextdescription23 showreset\\" data-initial_val=\\"Click and drag the correct form of the verb &quot;to be&quot; in the simple present.\\">Click and drag the correct form of the verb \\"to be\\" in the simple present.</p></div></div><div class=\\"aquicargaplantilla\\" id=\\"tmppt_3_4\\" data-tpl=\\"dby_multiplantilla\\" data-puntaje=\\"0\\" data-time=\\"02:20\\" data-showpuntaje=\\"1\\" data-showtime=\\"1\\" data-addclass=\\"isdrop\\" data-tpltitulo=\\"Click and drag-Text\\" data-tools=\\"chingoinput\\" data-showalternativas=\\"1\\" data-showasistido=\\"0\\" data-helpvideo=\\"1\\" data-intentos=\\"3\\"><input type=\\"hidden\\" id=\\"sysidorden\\" name=\\"orden[3][4]\\" value=\\"4\\"><input type=\\"hidden\\" id=\\"sysdettipo\\" name=\\"det_tipo[3][4]\\" value=\\"Do it by yourself\\"><input type=\\"hidden\\" id=\\"systipodesarrollo\\" name=\\"tipo_desarrollo[3][4]\\" value=\\"Click and drag-Text\\"><input type=\\"hidden\\" id=\\"systipoactividad\\" name=\\"tipo_actividad[3][4]\\" value=\\"A\\"><input type=\\"hidden\\" id=\\"sysiduser\\" name=\\"iduser[3][4]\\" value=\\"99999999\\"><input type=\\"hidden\\" id=\\"idgui\\" name=\\"idgui\\" value=\\"58acc411c789d\\"><input type=\\"hidden\\" id=\\"iddetalle_3_4\\" name=\\"iddetalle[3][4]\\" value=\\"83\\"><link href=\\"__xRUTABASEx__/frontend/plantillas/tema1/css/actividad_completar.css\\" rel=\\"stylesheet\\">\\n\\n\\n\\n\\n\\n\\n<div class=\\"plantilla plantilla-completar\\" id=\\"tmp_58acc411c789d\\" data-idgui=\\"58acc411c789d\\" data-tipo-tmp=\\"tipo_1\\">\\n  <div class=\\"row\\">\\n    <div class=\\"col-md-12\\">\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n    \\n\\n      <div class=\\"tpl_plantilla\\" style=\\"display: block;\\" data-idgui=\\"58acc411c789d\\">\\n        <div class=\\"row active\\" id=\\"tpl58acc411c789d\\">\\n          <div class=\\"col-md-12 col-sm-12 col-xs-12 discol1\\" id=\\"txt58acc411c789d\\" style=\\"padding: 1ex;\\">\\n            <div class=\\"col-md-12 col-sm-12 col-xs-12 removelineedit img-thumbnail\\">\\n              <div class=\\"panel panelEjercicio\\" id=\\"pnl_edithtml58acc411c789d\\"><p>Jane and <g class=\\"gr_ gr_7 gr-alert gr_gramm gr_run_anim Style multiReplace\\" id=\\"7\\" data-gr-id=\\"7\\"><g class=\\"gr_ gr_7 gr-alert gr_gramm gr_run_anim Style multiReplace\\" id=\\"7\\" data-gr-id=\\"7\\">Jack &nbsp;</g></g><div class=\\"inp-corregido wrapper\\" data-corregido=\\"good\\"><input width=\\"100\\" height=\\"25\\" class=\\"mce-object-input valinput isdrop ui-droppable\\" type=\\"text\\" data-texto=\\"are\\" data-mce-object=\\"input\\" data-options=\\"is,am\\" value=\\"are\\"><span class=\\"inner-icon\\"><i class=\\"fa fa-check color-green2\\"></i></span></div><g class=\\"gr_ gr_7 gr-alert gr_gramm gr_disable_anim_appear Style multiReplace\\" id=\\"7\\" data-gr-id=\\"7\\"><g class=\\"gr_ gr_7 gr-alert gr_gramm gr_disable_anim_appear Style multiReplace\\" id=\\"7\\" data-gr-id=\\"7\\">&nbsp; at</g></g> church.</p>\\n<p></p>\\n<p>She<div class=\\"inp-corregido wrapper\\" data-corregido=\\"good\\"><input width=\\"100\\" height=\\"25\\" class=\\"mce-object-input valinput isdrop ui-droppable\\" type=\\"text\\" data-texto=\\"is\\" data-mce-object=\\"input\\" data-options=\\"are,am\\" value=\\"is\\"><span class=\\"inner-icon\\"><i class=\\"fa fa-check color-green2\\"></i></span></div>&nbsp;in the kitchen.</p>\\n<p></p>\\n<p style=\\"text-align: justify;\\"><g class=\\"gr_ gr_11 gr-alert gr_gramm gr_run_anim Style multiReplace\\" id=\\"11\\" data-gr-id=\\"11\\">London </g><input width=\\"100\\" height=\\"25\\" class=\\"mce-object-input valinput isdrop ui-droppable\\" type=\\"text\\" data-texto=\\"is\\" data-mce-object=\\"input\\" data-options=\\"are,am\\"><g class=\\"gr_ gr_11 gr-alert gr_gramm gr_disable_anim_appear Style multiReplace\\" id=\\"11\\" data-gr-id=\\"11\\"> the</g> capital of Great Britain.</p>\\n<p style=\\"text-align: justify;\\"></p>\\n<p style=\\"text-align: justify;\\">The <g class=\\"gr_ gr_37 gr-alert gr_gramm gr_run_anim Style multiReplace\\" id=\\"37\\" data-gr-id=\\"37\\">nurse </g><input width=\\"100\\" height=\\"25\\" class=\\"mce-object-input valinput isdrop ui-droppable\\" type=\\"text\\" data-texto=\\"is\\" data-mce-object=\\"input\\" data-options=\\"are,am\\"><g class=\\"gr_ gr_37 gr-alert gr_gramm gr_disable_anim_appear Style multiReplace\\" id=\\"37\\" data-gr-id=\\"37\\"> with</g> him all the time.</p>\\n<p style=\\"text-align: justify;\\"></p>\\n<p style=\\"text-align: justify;\\">All the <g class=\\"gr_ gr_54 gr-alert gr_gramm gr_run_anim Style multiReplace\\" id=\\"54\\" data-gr-id=\\"54\\">shops </g><input width=\\"100\\" height=\\"25\\" class=\\"mce-object-input valinput isdrop ui-droppable\\" type=\\"text\\" data-texto=\\"are\\" data-mce-object=\\"input\\" data-options=\\"is,am\\"><g class=\\"gr_ gr_54 gr-alert gr_gramm gr_disable_anim_appear Style multiReplace\\" id=\\"54\\" data-gr-id=\\"54\\"> closed</g>.</p>\\n<p style=\\"text-align: justify;\\"></p>\\n<p style=\\"text-align: justify;\\"><g class=\\"gr_ gr_65 gr-alert gr_gramm gr_hide gr_run_anim Grammar\\" id=\\"65\\" data-gr-id=\\"65\\">They </g><input width=\\"100\\" height=\\"25\\" class=\\"mce-object-input valinput isdrop ui-droppable\\" type=\\"text\\" data-texto=\\"are\\" data-mce-object=\\"input\\" data-options=\\"is,am\\"><g class=\\"gr_ gr_65 gr-alert gr_gramm gr_hide gr_disable_anim_appear Grammar\\" id=\\"65\\" data-gr-id=\\"65\\">my</g> friends.</p></div>              \\n            </div>            \\n          </div>\\n          <div class=\\"col-md-12 col-sm-12 col-xs-12 discol2\\" id=\\"alter58acc411c789d\\" style=\\"padding: 1ex;\\">\\n            <div class=\\"col-md-12 col-sm-12 col-xs-12 removelineedit2 tpl_alternatives img-thumbnail\\">\\n              <div class=\\"panel panelAlternativas text-center\\" id=\\"pnl_editalternatives58acc411c789d\\"><span class=\\"isdragable ui-draggable ui-draggable-handle\\" style=\\"position:relative\\">are</span>,<span class=\\"isdragable ui-draggable ui-draggable-handle\\" style=\\"position:relative\\">is</span>,<span class=\\"isdragable ui-draggable ui-draggable-handle\\" style=\\"position: relative;\\">is</span>,<span class=\\"isdragable ui-draggable ui-draggable-handle\\" style=\\"position: relative;\\">are</span>,<span class=\\"isdragable ui-draggable ui-draggable-handle\\" style=\\"position: relative;\\">is</span>,<span class=\\"isdragable ui-draggable ui-draggable-handle\\" style=\\"position: relative;\\">are</span></div>\\n            </div>\\n          </div>\\n        </div>        \\n      </div>       \\n      <textarea class=\\"txtareaedit\\" id=\\"txtarea58acc411c789d\\" aria-hidden=\\"true\\" style=\\"display: none;\\">&lt;p&gt;Jane and &lt;g class=\\"gr_ gr_7 gr-alert gr_gramm gr_run_anim Style multiReplace\\" id=\\"7\\" data-gr-id=\\"7\\"&gt;&lt;g class=\\"gr_ gr_7 gr-alert gr_gramm gr_run_anim Style multiReplace\\" id=\\"7\\" data-gr-id=\\"7\\"&gt;Jack &nbsp;&lt;/g&gt;&lt;/g&gt;&lt;input data-options=\\"is,am\\" data-mce-object=\\"input\\" height=\\"25\\" width=\\"100\\" type=\\"text\\" data-texto=\\"are\\" class=\\"mce-object-input valinput isdrop ui-droppable\\"&gt;&lt;g class=\\"gr_ gr_7 gr-alert gr_gramm gr_disable_anim_appear Style multiReplace\\" id=\\"7\\" data-gr-id=\\"7\\"&gt;&lt;g class=\\"gr_ gr_7 gr-alert gr_gramm gr_disable_anim_appear Style multiReplace\\" id=\\"7\\" data-gr-id=\\"7\\"&gt;&nbsp; at&lt;/g&gt;&lt;/g&gt; church.&lt;/p&gt;\\n&lt;p&gt;&lt;/p&gt;\\n&lt;p&gt;She&lt;input data-options=\\"are,am\\" data-mce-object=\\"input\\" height=\\"25\\" width=\\"100\\" type=\\"text\\" data-texto=\\"is\\" class=\\"mce-object-input valinput isdrop ui-droppable\\"&gt;&nbsp;in the kitchen.&lt;/p&gt;\\n&lt;p&gt;&lt;/p&gt;\\n&lt;p style=\\"text-align: justify;\\"&gt;&lt;g class=\\"gr_ gr_11 gr-alert gr_gramm gr_run_anim Style multiReplace\\" id=\\"11\\" data-gr-id=\\"11\\"&gt;London &lt;/g&gt;&lt;input data-options=\\"are,am\\" data-mce-object=\\"input\\" height=\\"25\\" width=\\"100\\" type=\\"text\\" data-texto=\\"is\\" class=\\"mce-object-input valinput isdrop ui-droppable\\"&gt;&lt;g class=\\"gr_ gr_11 gr-alert gr_gramm gr_disable_anim_appear Style multiReplace\\" id=\\"11\\" data-gr-id=\\"11\\"&gt; the&lt;/g&gt; capital of Great Britain.&lt;/p&gt;\\n&lt;p style=\\"text-align: justify;\\"&gt;&lt;/p&gt;\\n&lt;p style=\\"text-align: justify;\\"&gt;The &lt;g class=\\"gr_ gr_37 gr-alert gr_gramm gr_run_anim Style multiReplace\\" id=\\"37\\" data-gr-id=\\"37\\"&gt;nurse &lt;/g&gt;&lt;input data-options=\\"are,am\\" data-mce-object=\\"input\\" height=\\"25\\" width=\\"100\\" type=\\"text\\" data-texto=\\"is\\" class=\\"mce-object-input valinput isdrop ui-droppable\\"&gt;&lt;g class=\\"gr_ gr_37 gr-alert gr_gramm gr_disable_anim_appear Style multiReplace\\" id=\\"37\\" data-gr-id=\\"37\\"&gt; with&lt;/g&gt; him all the time.&lt;/p&gt;\\n&lt;p style=\\"text-align: justify;\\"&gt;&lt;/p&gt;\\n&lt;p style=\\"text-align: justify;\\"&gt;All the &lt;g class=\\"gr_ gr_54 gr-alert gr_gramm gr_run_anim Style multiReplace\\" id=\\"54\\" data-gr-id=\\"54\\"&gt;shops &lt;/g&gt;&lt;input data-options=\\"is,am\\" data-mce-object=\\"input\\" height=\\"25\\" width=\\"100\\" type=\\"text\\" data-texto=\\"are\\" class=\\"mce-object-input valinput isdrop ui-droppable\\"&gt;&lt;g class=\\"gr_ gr_54 gr-alert gr_gramm gr_disable_anim_appear Style multiReplace\\" id=\\"54\\" data-gr-id=\\"54\\"&gt; closed&lt;/g&gt;.&lt;/p&gt;\\n&lt;p style=\\"text-align: justify;\\"&gt;&lt;/p&gt;\\n&lt;p style=\\"text-align: justify;\\"&gt;&lt;g class=\\"gr_ gr_65 gr-alert gr_gramm gr_hide gr_run_anim Grammar\\" id=\\"65\\" data-gr-id=\\"65\\"&gt;They &lt;/g&gt;&lt;input data-options=\\"is,am\\" data-mce-object=\\"input\\" height=\\"25\\" width=\\"100\\" type=\\"text\\" data-texto=\\"are\\" class=\\"mce-object-input valinput isdrop ui-droppable\\"&gt;&lt;g class=\\"gr_ gr_65 gr-alert gr_gramm gr_hide gr_disable_anim_appear Grammar\\" id=\\"65\\" data-gr-id=\\"65\\"&gt;my&lt;/g&gt; friends.&lt;/p&gt;</textarea>\\n    </div>\\n  </div>\\n</div>\\n<script type=\\"text/javascript\\">\\n$(document).ready(function(){\\n  iniciarCompletar_DBY(\'58acc411c789d\');\\n});\\n</script></div><input name=\\"habilidades[3][4]\\" class=\\"selected-skills\\" id=\\"habilidades_met-3_4\\" type=\\"hidden\\" value=\\"8|5\\"></div></div>                    "}]', 26),
(29, 'T', 6, 'rubricas.PDF', '__xRUTABASEx__/static/tarea_archivos/documento/20170727100122_rubricas.PDF', 'D', NULL, NULL, NULL, NULL),
(30, 'T', 6, 'YT', 'youtube.com', 'L', NULL, NULL, NULL, NULL),
(31, 'T', 6, 'Activity', '__xRUTABASEx__/tarea_archivos/visor/?idarchivo=', 'A', NULL, NULL, '[85,86,87]', NULL),
(32, 'T', 6, 'Game', '__xRUTABASEx__/tarea_archivos/visor/?idarchivo=', 'J', NULL, NULL, '[10]', NULL),
(33, 'T', 6, 'Exam', '__xRUTABASEx__/tarea_archivos/visor/?idarchivo=', 'E', NULL, NULL, '[4]', NULL),
(34, 'R', 5, 'links_mcastro.txt', '__xRUTABASEx__/static/tarea_archivos/documento/20170727100736_links_mcastro.txt', 'D', '80.00', '["4","7"]', NULL, NULL),
(35, 'R', 5, 'Activity', '__xRUTABASEx__/tarea_archivos/visor/?idarchivo=', 'A', '33.30', '["4","5"]', '[{"id":"85","html":"\\n                        <div class=\\"row\\"> <div class=\\"col-xs-12\\"><div class=\\"tab-title-zone pw1_3\\"> <div class=\\"col-md-12\\"><h3 class=\\"addtext addtexttitulo2 showreset\\" data-initial_val=\\"Ordering sentences in simple present\\">Ordering sentences in simple present</h3></div><div class=\\"space-line pw1_3\\"></div></div><div class=\\"tab-description pw1_3\\"> <div class=\\"col-md-12\\"><p class=\\"addtext addtextdescription23 showreset\\" data-initial_val=\\"Put the words in the correct order.\\">Put the words in the correct order.</p></div></div><div class=\\"aquicargaplantilla\\" id=\\"tmppt_3_6\\" data-tpl=\\"dby_ordenar_simple\\" data-puntaje=\\"0\\" data-time=\\"01:00\\" data-showpuntaje=\\"1\\" data-showtime=\\"1\\" data-addclass=\\"\\" data-tpltitulo=\\"Other activities-Put in order\\" data-tools=\\"\\" data-showalternativas=\\"0\\" data-showasistido=\\"0\\" data-helpvideo=\\"19\\" data-intentos=\\"3\\"><input type=\\"hidden\\" id=\\"sysidorden\\" name=\\"orden[3][6]\\" value=\\"6\\"><input type=\\"hidden\\" id=\\"sysdettipo\\" name=\\"det_tipo[3][6]\\" value=\\"Do it by yourself\\"><input type=\\"hidden\\" id=\\"systipodesarrollo\\" name=\\"tipo_desarrollo[3][6]\\" value=\\"Other activities-Put in order\\"><input type=\\"hidden\\" id=\\"systipoactividad\\" name=\\"tipo_actividad[3][6]\\" value=\\"A\\"><input type=\\"hidden\\" id=\\"sysiduser\\" name=\\"iduser[3][6]\\" value=\\"99999999\\"><input type=\\"hidden\\" id=\\"idgui\\" name=\\"idgui\\" value=\\"58accc0fd1541\\"><input type=\\"hidden\\" id=\\"iddetalle_3_6\\" name=\\"iddetalle[3][6]\\" value=\\"85\\"><link href=\\"__xRUTABASEx__/frontend/plantillas/tema1/css/actividad_ordenar.css\\" rel=\\"stylesheet\\">\\n\\n\\n\\n\\n\\n\\n\\n<div class=\\"plantilla plantilla-ordenar ord_simple tiempo-pausado\\" id=\\"tmp_58accc0fd1541\\" data-idgui=\\"58accc0fd1541\\" data-tipo-tmp=\\"ordenar_simple\\">\\n    \\n    \\n    <div class=\\"row ejerc-ordenar tpl_plantilla\\" id=\\"ejerc-ordenar58accc0fd1541\\" style=\\"display: block;\\"><div class=\\"col-xs-12 element\\" id=\\"e_1487719560990\\" data-resp=\\"You read this book everyday.\\" data-corregido=\\"good\\">\\n            <div class=\\"col-xs-12 col-sm-4 multimedia hide\\">\\n                <img class=\\"img-responsive hide\\" src=\\"\\">\\n                <a class=\\"btn btn-orange btn-block hide\\" href=\\"#\\" data-tooltip=\\"tooltip\\"><i class=\\"fa fa-play\\"></i></a>\\n            </div>\\n            <div class=\\"col-xs-12 col-sm-8 texto\\">\\n                <div class=\\"drag\\"></div>\\n                <div class=\\"drop good\\">\\n                <div class=\\"parte blank word good\\"><div>You</div></div><div class=\\"parte blank word good\\"><div>read</div></div><div class=\\"parte blank word good\\"><div>this</div></div><div class=\\"parte blank word good\\"><div>book</div></div><div class=\\"parte blank word good\\"><div>everyday.</div></div></div>\\n            </div>\\n        </div></div>\\n    \\n    <audio class=\\"hidden\\" id=\\"audio-ordenar58accc0fd1541\\" style=\\"display: none;\\" src=\\"\\"></audio>\\n</div>\\n<script> $(document).ready(function(){ initOrdenarSimple(\'58accc0fd1541\', false); }); </script></div><input name=\\"habilidades[3][6]\\" class=\\"selected-skills\\" id=\\"habilidades_met-3_6\\" type=\\"hidden\\" value=\\"0\\"></div></div>                    "},{"id":"86","html":"\\n                        <div class=\\"row\\"> <div class=\\"col-xs-12\\"><div class=\\"tab-title-zone pw1_3\\"> <div class=\\"col-md-12\\"><h3 class=\\"addtext addtexttitulo2 showreset\\" data-initial_val=\\"Ordering sentences in Simple Present\\">Ordering sentences in Simple Present</h3></div><div class=\\"space-line pw1_3\\"></div></div><div class=\\"tab-description pw1_3\\"> <div class=\\"col-md-12\\"><p class=\\"addtext addtextdescription23 showreset\\" data-initial_val=\\"Put the words in the correct order\\">Put the words in the correct order</p></div></div><div class=\\"aquicargaplantilla\\" id=\\"tmppt_3_7\\" data-tpl=\\"dby_ordenar_simple\\" data-puntaje=\\"0\\" data-time=\\"01:00\\" data-showpuntaje=\\"1\\" data-showtime=\\"1\\" data-addclass=\\"\\" data-tpltitulo=\\"Other activities-Put in order\\" data-tools=\\"\\" data-showalternativas=\\"0\\" data-showasistido=\\"0\\" data-helpvideo=\\"19\\" data-intentos=\\"3\\"><input type=\\"hidden\\" id=\\"sysidorden\\" name=\\"orden[3][7]\\" value=\\"7\\"><input type=\\"hidden\\" id=\\"sysdettipo\\" name=\\"det_tipo[3][7]\\" value=\\"Do it by yourself\\"><input type=\\"hidden\\" id=\\"systipodesarrollo\\" name=\\"tipo_desarrollo[3][7]\\" value=\\"Other activities-Put in order\\"><input type=\\"hidden\\" id=\\"systipoactividad\\" name=\\"tipo_actividad[3][7]\\" value=\\"A\\"><input type=\\"hidden\\" id=\\"sysiduser\\" name=\\"iduser[3][7]\\" value=\\"99999999\\"><input type=\\"hidden\\" id=\\"idgui\\" name=\\"idgui\\" value=\\"58accc9b735fe\\"><input type=\\"hidden\\" id=\\"iddetalle_3_7\\" name=\\"iddetalle[3][7]\\" value=\\"86\\"><link href=\\"__xRUTABASEx__/frontend/plantillas/tema1/css/actividad_ordenar.css\\" rel=\\"stylesheet\\">\\n\\n\\n\\n\\n\\n\\n\\n<div class=\\"plantilla plantilla-ordenar ord_simple\\" id=\\"tmp_58accc9b735fe\\" data-idgui=\\"58accc9b735fe\\" data-tipo-tmp=\\"ordenar_simple\\">\\n    \\n    \\n    <div class=\\"row ejerc-ordenar tpl_plantilla\\" id=\\"ejerc-ordenar58accc9b735fe\\" style=\\"display: block;\\"><div class=\\"col-xs-12 element\\" id=\\"e_1487719577256\\" data-resp=\\"The girls have new dolls.\\">\\n            <div class=\\"col-xs-12 col-sm-4 multimedia hide\\">\\n                <img class=\\"img-responsive hide\\" src=\\"\\">\\n                <a class=\\"btn btn-orange btn-block hide\\" href=\\"#\\" data-tooltip=\\"tooltip\\"><i class=\\"fa fa-play\\"></i></a>\\n            </div>\\n            <div class=\\"col-xs-12 col-sm-8 texto\\">\\n                <div class=\\"drag\\"><div>dolls.</div><div>The</div></div>\\n                <div class=\\"drop\\">\\n                <div class=\\"parte blank word\\"><div>new</div></div><div class=\\"parte blank word\\"><div>have</div></div><div class=\\"parte blank word\\"><div>girls</div></div><div class=\\"parte blank word\\"></div><div class=\\"parte blank word\\"></div></div>\\n            </div>\\n        </div></div>\\n    \\n    <audio class=\\"hidden\\" id=\\"audio-ordenar58accc9b735fe\\" style=\\"display: none;\\" src=\\"\\"></audio>\\n</div>\\n<script> $(document).ready(function(){ initOrdenarSimple(\'58accc9b735fe\', false); }); </script></div><input name=\\"habilidades[3][7]\\" class=\\"selected-skills\\" id=\\"habilidades_met-3_7\\" type=\\"hidden\\" value=\\"5|8\\"></div></div>                    "},{"id":"87","html":"\\n                        <div class=\\"row\\"> <div class=\\"col-xs-12\\"><div class=\\"tab-title-zone pw1_3\\"> <div class=\\"col-md-12\\"><h3 class=\\"addtext addtexttitulo2 showreset\\" data-initial_val=\\"Ordering sentences in Simple Present\\">Ordering sentences in Simple Present</h3></div><div class=\\"space-line pw1_3\\"></div></div><div class=\\"tab-description pw1_3\\"> <div class=\\"col-md-12\\"><p class=\\"addtext addtextdescription23 showreset\\" data-initial_val=\\"Put the words in the correct order.\\">Put the words in the correct order.</p></div></div><div class=\\"aquicargaplantilla\\" id=\\"tmppt_3_8\\" data-tpl=\\"dby_ordenar_simple\\" data-puntaje=\\"0\\" data-time=\\"01:00\\" data-showpuntaje=\\"1\\" data-showtime=\\"1\\" data-addclass=\\"\\" data-tpltitulo=\\"Other activities-Put in order\\" data-tools=\\"\\" data-showalternativas=\\"0\\" data-showasistido=\\"0\\" data-helpvideo=\\"19\\" data-intentos=\\"3\\"><input type=\\"hidden\\" id=\\"sysidorden\\" name=\\"orden[3][8]\\" value=\\"8\\"><input type=\\"hidden\\" id=\\"sysdettipo\\" name=\\"det_tipo[3][8]\\" value=\\"Do it by yourself\\"><input type=\\"hidden\\" id=\\"systipodesarrollo\\" name=\\"tipo_desarrollo[3][8]\\" value=\\"Other activities-Put in order\\"><input type=\\"hidden\\" id=\\"systipoactividad\\" name=\\"tipo_actividad[3][8]\\" value=\\"A\\"><input type=\\"hidden\\" id=\\"sysiduser\\" name=\\"iduser[3][8]\\" value=\\"99999999\\"><input type=\\"hidden\\" id=\\"idgui\\" name=\\"idgui\\" value=\\"58acccec32e1e\\"><input type=\\"hidden\\" id=\\"iddetalle_3_8\\" name=\\"iddetalle[3][8]\\" value=\\"87\\"><link href=\\"__xRUTABASEx__/frontend/plantillas/tema1/css/actividad_ordenar.css\\" rel=\\"stylesheet\\">\\n\\n\\n\\n\\n\\n\\n\\n<div class=\\"plantilla plantilla-ordenar ord_simple\\" id=\\"tmp_58acccec32e1e\\" data-idgui=\\"58acccec32e1e\\" data-tipo-tmp=\\"ordenar_simple\\">\\n    \\n    \\n    <div class=\\"row ejerc-ordenar tpl_plantilla\\" id=\\"ejerc-ordenar58acccec32e1e\\" style=\\"display: block;\\"><div class=\\"col-xs-12 element\\" id=\\"e_1487719657990\\" data-resp=\\"She goes home after work.\\">\\n            <div class=\\"col-xs-12 col-sm-4 multimedia hide\\">\\n                <img class=\\"img-responsive hide\\" src=\\"\\">\\n                <a class=\\"btn btn-orange btn-block hide\\" href=\\"#\\" data-tooltip=\\"tooltip\\"><i class=\\"fa fa-play\\"></i></a>\\n            </div>\\n            <div class=\\"col-xs-12 col-sm-8 texto\\">\\n                <div class=\\"drag\\"><div>home</div><div>work.</div><div>goes</div><div>after</div><div>She</div></div>\\n                <div class=\\"drop\\">\\n                <div class=\\"parte blank word\\"></div><div class=\\"parte blank word\\"></div><div class=\\"parte blank word\\"></div><div class=\\"parte blank word\\"></div><div class=\\"parte blank word\\"></div></div>\\n            </div>\\n        </div></div>\\n    \\n    <audio class=\\"hidden\\" id=\\"audio-ordenar58acccec32e1e\\" style=\\"display: none;\\" src=\\"\\"></audio>\\n</div>\\n<script> $(document).ready(function(){ initOrdenarSimple(\'58acccec32e1e\', false); }); </script></div><input name=\\"habilidades[3][8]\\" class=\\"selected-skills\\" id=\\"habilidades_met-3_8\\" type=\\"hidden\\" value=\\"0\\"></div></div>                    "}]', 31),
(36, 'R', 5, 'Game', '__xRUTABASEx__/tarea_archivos/visor/?idarchivo=', 'J', '16.60', '["6","7"]', '[{"id":"10","html":"\\n            <link rel=\\"stylesheet\\" href=\\"__xRUTABASEx__/frontend/plantillas/tema1/css/actividad_sopaletras.css\\">\\n<input type=\\"hidden\\" name=\\"idgui\\" id=\\"idgui\\" value=\\"58acd4c50fca0\\">\\n<div class=\\"plantilla plantilla-sopaletras\\" data-idgui=\\"58acd4c50fca0\\">\\n    \\n\\n    <div class=\\"row\\" id=\\"findword-game\\" style=\\"display: block;\\">\\n        \\n\\n        <div class=\\"col-xs-12\\" style=\\"text-align: center;\\">\\n            <div id=\\"puzzle\\"><div><button class=\\"puzzleSquare\\" x=\\"0\\" y=\\"0\\">p</button><button class=\\"puzzleSquare\\" x=\\"1\\" y=\\"0\\">l</button><button class=\\"puzzleSquare found\\" x=\\"2\\" y=\\"0\\">g</button><button class=\\"puzzleSquare\\" x=\\"3\\" y=\\"0\\">p</button><button class=\\"puzzleSquare\\" x=\\"4\\" y=\\"0\\">u</button><button class=\\"puzzleSquare\\" x=\\"5\\" y=\\"0\\">p</button><button class=\\"puzzleSquare\\" x=\\"6\\" y=\\"0\\">f</button><button class=\\"puzzleSquare\\" x=\\"7\\" y=\\"0\\">v</button><button class=\\"puzzleSquare\\" x=\\"8\\" y=\\"0\\">e</button><button class=\\"puzzleSquare\\" x=\\"9\\" y=\\"0\\">j</button><button class=\\"puzzleSquare\\" x=\\"10\\" y=\\"0\\">g</button><button class=\\"puzzleSquare\\" x=\\"11\\" y=\\"0\\">e</button><button class=\\"puzzleSquare\\" x=\\"12\\" y=\\"0\\">t</button></div><div><button class=\\"puzzleSquare\\" x=\\"0\\" y=\\"1\\">m</button><button class=\\"puzzleSquare\\" x=\\"1\\" y=\\"1\\">v</button><button class=\\"puzzleSquare found\\" x=\\"2\\" y=\\"1\\">o</button><button class=\\"puzzleSquare\\" x=\\"3\\" y=\\"1\\">y</button><button class=\\"puzzleSquare\\" x=\\"4\\" y=\\"1\\">e</button><button class=\\"puzzleSquare\\" x=\\"5\\" y=\\"1\\">o</button><button class=\\"puzzleSquare\\" x=\\"6\\" y=\\"1\\">i</button><button class=\\"puzzleSquare\\" x=\\"7\\" y=\\"1\\">e</button><button class=\\"puzzleSquare\\" x=\\"8\\" y=\\"1\\">j</button><button class=\\"puzzleSquare\\" x=\\"9\\" y=\\"1\\">m</button><button class=\\"puzzleSquare\\" x=\\"10\\" y=\\"1\\">l</button><button class=\\"puzzleSquare\\" x=\\"11\\" y=\\"1\\">w</button><button class=\\"puzzleSquare\\" x=\\"12\\" y=\\"1\\">r</button></div><div><button class=\\"puzzleSquare\\" x=\\"0\\" y=\\"2\\">h</button><button class=\\"puzzleSquare\\" x=\\"1\\" y=\\"2\\">r</button><button class=\\"puzzleSquare found\\" x=\\"2\\" y=\\"2\\">o</button><button class=\\"puzzleSquare\\" x=\\"3\\" y=\\"2\\">p</button><button class=\\"puzzleSquare\\" x=\\"4\\" y=\\"2\\">p</button><button class=\\"puzzleSquare\\" x=\\"5\\" y=\\"2\\">r</button><button class=\\"puzzleSquare\\" x=\\"6\\" y=\\"2\\">j</button><button class=\\"puzzleSquare\\" x=\\"7\\" y=\\"2\\">d</button><button class=\\"puzzleSquare\\" x=\\"8\\" y=\\"2\\">c</button><button class=\\"puzzleSquare\\" x=\\"9\\" y=\\"2\\">k</button><button class=\\"puzzleSquare\\" x=\\"10\\" y=\\"2\\">o</button><button class=\\"puzzleSquare\\" x=\\"11\\" y=\\"2\\">g</button><button class=\\"puzzleSquare\\" x=\\"12\\" y=\\"2\\">a</button></div><div><button class=\\"puzzleSquare\\" x=\\"0\\" y=\\"3\\">w</button><button class=\\"puzzleSquare\\" x=\\"1\\" y=\\"3\\">i</button><button class=\\"puzzleSquare found\\" x=\\"2\\" y=\\"3\\">d</button><button class=\\"puzzleSquare\\" x=\\"3\\" y=\\"3\\">H</button><button class=\\"puzzleSquare\\" x=\\"4\\" y=\\"3\\">o</button><button class=\\"puzzleSquare\\" x=\\"5\\" y=\\"3\\">b</button><button class=\\"puzzleSquare\\" x=\\"6\\" y=\\"3\\">v</button><button class=\\"puzzleSquare\\" x=\\"7\\" y=\\"3\\">m</button><button class=\\"puzzleSquare\\" x=\\"8\\" y=\\"3\\">d</button><button class=\\"puzzleSquare\\" x=\\"9\\" y=\\"3\\">u</button><button class=\\"puzzleSquare\\" x=\\"10\\" y=\\"3\\">n</button><button class=\\"puzzleSquare\\" x=\\"11\\" y=\\"3\\">m</button><button class=\\"puzzleSquare\\" x=\\"12\\" y=\\"3\\">y</button></div><div><button class=\\"puzzleSquare\\" x=\\"0\\" y=\\"4\\">r</button><button class=\\"puzzleSquare\\" x=\\"1\\" y=\\"4\\">g</button><button class=\\"puzzleSquare found\\" x=\\"2\\" y=\\"4\\">a</button><button class=\\"puzzleSquare\\" x=\\"3\\" y=\\"4\\">l</button><button class=\\"puzzleSquare\\" x=\\"4\\" y=\\"4\\">e</button><button class=\\"puzzleSquare\\" x=\\"5\\" y=\\"4\\">a</button><button class=\\"puzzleSquare\\" x=\\"6\\" y=\\"4\\">u</button><button class=\\"puzzleSquare\\" x=\\"7\\" y=\\"4\\">u</button><button class=\\"puzzleSquare\\" x=\\"8\\" y=\\"4\\">d</button><button class=\\"puzzleSquare\\" x=\\"9\\" y=\\"4\\">i</button><button class=\\"puzzleSquare\\" x=\\"10\\" y=\\"4\\">i</button><button class=\\"puzzleSquare\\" x=\\"11\\" y=\\"4\\">p</button><button class=\\"puzzleSquare\\" x=\\"12\\" y=\\"4\\">b</button></div><div><button class=\\"puzzleSquare\\" x=\\"0\\" y=\\"5\\">k</button><button class=\\"puzzleSquare\\" x=\\"1\\" y=\\"5\\">t</button><button class=\\"puzzleSquare found\\" x=\\"2\\" y=\\"5\\">f</button><button class=\\"puzzleSquare\\" x=\\"3\\" y=\\"5\\">p</button><button class=\\"puzzleSquare\\" x=\\"4\\" y=\\"5\\">k</button><button class=\\"puzzleSquare\\" x=\\"5\\" y=\\"5\\">l</button><button class=\\"puzzleSquare\\" x=\\"6\\" y=\\"5\\">w</button><button class=\\"puzzleSquare\\" x=\\"7\\" y=\\"5\\">w</button><button class=\\"puzzleSquare\\" x=\\"8\\" y=\\"5\\">n</button><button class=\\"puzzleSquare\\" x=\\"9\\" y=\\"5\\">e</button><button class=\\"puzzleSquare\\" x=\\"10\\" y=\\"5\\">s</button><button class=\\"puzzleSquare\\" x=\\"11\\" y=\\"5\\">t</button><button class=\\"puzzleSquare\\" x=\\"12\\" y=\\"5\\">l</button></div><div><button class=\\"puzzleSquare\\" x=\\"0\\" y=\\"6\\">m</button><button class=\\"puzzleSquare\\" x=\\"1\\" y=\\"6\\">j</button><button class=\\"puzzleSquare found\\" x=\\"2\\" y=\\"6\\">t</button><button class=\\"puzzleSquare\\" x=\\"3\\" y=\\"6\\">w</button><button class=\\"puzzleSquare\\" x=\\"4\\" y=\\"6\\">i</button><button class=\\"puzzleSquare\\" x=\\"5\\" y=\\"6\\">w</button><button class=\\"puzzleSquare\\" x=\\"6\\" y=\\"6\\">l</button><button class=\\"puzzleSquare\\" x=\\"7\\" y=\\"6\\">r</button><button class=\\"puzzleSquare\\" x=\\"8\\" y=\\"6\\">s</button><button class=\\"puzzleSquare\\" x=\\"9\\" y=\\"6\\">h</button><button class=\\"puzzleSquare\\" x=\\"10\\" y=\\"6\\">m</button><button class=\\"puzzleSquare\\" x=\\"11\\" y=\\"6\\">p</button><button class=\\"puzzleSquare\\" x=\\"12\\" y=\\"6\\">n</button></div><div><button class=\\"puzzleSquare\\" x=\\"0\\" y=\\"7\\">e</button><button class=\\"puzzleSquare\\" x=\\"1\\" y=\\"7\\">w</button><button class=\\"puzzleSquare found\\" x=\\"2\\" y=\\"7\\">e</button><button class=\\"puzzleSquare\\" x=\\"3\\" y=\\"7\\">y</button><button class=\\"puzzleSquare\\" x=\\"4\\" y=\\"7\\">b</button><button class=\\"puzzleSquare\\" x=\\"5\\" y=\\"7\\">d</button><button class=\\"puzzleSquare\\" x=\\"6\\" y=\\"7\\">o</button><button class=\\"puzzleSquare\\" x=\\"7\\" y=\\"7\\">o</button><button class=\\"puzzleSquare\\" x=\\"8\\" y=\\"7\\">g</button><button class=\\"puzzleSquare\\" x=\\"9\\" y=\\"7\\">i</button><button class=\\"puzzleSquare\\" x=\\"10\\" y=\\"7\\">o</button><button class=\\"puzzleSquare\\" x=\\"11\\" y=\\"7\\">r</button><button class=\\"puzzleSquare\\" x=\\"12\\" y=\\"7\\">w</button></div><div><button class=\\"puzzleSquare\\" x=\\"0\\" y=\\"8\\">y</button><button class=\\"puzzleSquare\\" x=\\"1\\" y=\\"8\\">m</button><button class=\\"puzzleSquare found\\" x=\\"2\\" y=\\"8\\">r</button><button class=\\"puzzleSquare\\" x=\\"3\\" y=\\"8\\">a</button><button class=\\"puzzleSquare\\" x=\\"4\\" y=\\"8\\">j</button><button class=\\"puzzleSquare\\" x=\\"5\\" y=\\"8\\">m</button><button class=\\"puzzleSquare\\" x=\\"6\\" y=\\"8\\">u</button><button class=\\"puzzleSquare\\" x=\\"7\\" y=\\"8\\">f</button><button class=\\"puzzleSquare\\" x=\\"8\\" y=\\"8\\">y</button><button class=\\"puzzleSquare\\" x=\\"9\\" y=\\"8\\">v</button><button class=\\"puzzleSquare\\" x=\\"10\\" y=\\"8\\">e</button><button class=\\"puzzleSquare\\" x=\\"11\\" y=\\"8\\">o</button><button class=\\"puzzleSquare\\" x=\\"12\\" y=\\"8\\">j</button></div><div><button class=\\"puzzleSquare\\" x=\\"0\\" y=\\"9\\">c</button><button class=\\"puzzleSquare\\" x=\\"1\\" y=\\"9\\">m</button><button class=\\"puzzleSquare found\\" x=\\"2\\" y=\\"9\\">n</button><button class=\\"puzzleSquare\\" x=\\"3\\" y=\\"9\\">i</button><button class=\\"puzzleSquare\\" x=\\"4\\" y=\\"9\\">d</button><button class=\\"puzzleSquare\\" x=\\"5\\" y=\\"9\\">y</button><button class=\\"puzzleSquare\\" x=\\"6\\" y=\\"9\\">i</button><button class=\\"puzzleSquare\\" x=\\"7\\" y=\\"9\\">d</button><button class=\\"puzzleSquare\\" x=\\"8\\" y=\\"9\\">g</button><button class=\\"puzzleSquare\\" x=\\"9\\" y=\\"9\\">p</button><button class=\\"puzzleSquare\\" x=\\"10\\" y=\\"9\\">o</button><button class=\\"puzzleSquare\\" x=\\"11\\" y=\\"9\\">j</button><button class=\\"puzzleSquare\\" x=\\"12\\" y=\\"9\\">o</button></div><div><button class=\\"puzzleSquare\\" x=\\"0\\" y=\\"10\\">i</button><button class=\\"puzzleSquare\\" x=\\"1\\" y=\\"10\\">g</button><button class=\\"puzzleSquare found\\" x=\\"2\\" y=\\"10\\">o</button><button class=\\"puzzleSquare\\" x=\\"3\\" y=\\"10\\">o</button><button class=\\"puzzleSquare\\" x=\\"4\\" y=\\"10\\">d</button><button class=\\"puzzleSquare\\" x=\\"5\\" y=\\"10\\">n</button><button class=\\"puzzleSquare\\" x=\\"6\\" y=\\"10\\">i</button><button class=\\"puzzleSquare\\" x=\\"7\\" y=\\"10\\">g</button><button class=\\"puzzleSquare\\" x=\\"8\\" y=\\"10\\">h</button><button class=\\"puzzleSquare\\" x=\\"9\\" y=\\"10\\">t</button><button class=\\"puzzleSquare\\" x=\\"10\\" y=\\"10\\">c</button><button class=\\"puzzleSquare\\" x=\\"11\\" y=\\"10\\">m</button><button class=\\"puzzleSquare\\" x=\\"12\\" y=\\"10\\">e</button></div><div><button class=\\"puzzleSquare\\" x=\\"0\\" y=\\"11\\">e</button><button class=\\"puzzleSquare\\" x=\\"1\\" y=\\"11\\">j</button><button class=\\"puzzleSquare found\\" x=\\"2\\" y=\\"11\\">o</button><button class=\\"puzzleSquare\\" x=\\"3\\" y=\\"11\\">a</button><button class=\\"puzzleSquare\\" x=\\"4\\" y=\\"11\\">s</button><button class=\\"puzzleSquare\\" x=\\"5\\" y=\\"11\\">v</button><button class=\\"puzzleSquare\\" x=\\"6\\" y=\\"11\\">b</button><button class=\\"puzzleSquare\\" x=\\"7\\" y=\\"11\\">y</button><button class=\\"puzzleSquare\\" x=\\"8\\" y=\\"11\\">s</button><button class=\\"puzzleSquare\\" x=\\"9\\" y=\\"11\\">g</button><button class=\\"puzzleSquare\\" x=\\"10\\" y=\\"11\\">n</button><button class=\\"puzzleSquare\\" x=\\"11\\" y=\\"11\\">k</button><button class=\\"puzzleSquare\\" x=\\"12\\" y=\\"11\\">f</button></div><div><button class=\\"puzzleSquare\\" x=\\"0\\" y=\\"12\\">b</button><button class=\\"puzzleSquare\\" x=\\"1\\" y=\\"12\\">g</button><button class=\\"puzzleSquare found\\" x=\\"2\\" y=\\"12\\">n</button><button class=\\"puzzleSquare\\" x=\\"3\\" y=\\"12\\">i</button><button class=\\"puzzleSquare\\" x=\\"4\\" y=\\"12\\">n</button><button class=\\"puzzleSquare\\" x=\\"5\\" y=\\"12\\">e</button><button class=\\"puzzleSquare\\" x=\\"6\\" y=\\"12\\">v</button><button class=\\"puzzleSquare\\" x=\\"7\\" y=\\"12\\">e</button><button class=\\"puzzleSquare\\" x=\\"8\\" y=\\"12\\">d</button><button class=\\"puzzleSquare\\" x=\\"9\\" y=\\"12\\">o</button><button class=\\"puzzleSquare\\" x=\\"10\\" y=\\"12\\">o</button><button class=\\"puzzleSquare\\" x=\\"11\\" y=\\"12\\">g</button><button class=\\"puzzleSquare\\" x=\\"12\\" y=\\"12\\">p</button></div></div>\\n        </div>\\n        <div class=\\"col-xs-12\\">\\n            <div id=\\"words\\"><ul><li class=\\"word Hello\\">Hello</li><li class=\\"word goodafternoon wordFound\\">goodafternoon</li><li class=\\"word goodbye\\">goodbye</li><li class=\\"word goodevening\\">goodevening</li><li class=\\"word goodmorning\\">goodmorning</li><li class=\\"word goodnight\\">goodnight</li></ul></div>\\n        </div>\\n    </div>\\n</div>\\n\\n<script>\\n\\n$(\\"*[data-tooltip=\\\\\\"tooltip\\\\\\"]\\").tooltip();\\n\\nvar inputsValidos = function(){\\n    var rspta = true;\\n    var $contenInputs = $(\\"#list-words-game\\");\\n    $contenInputs.find(\\"input.word-game\\").each(function() {\\n        var valor = $(this).val();\\n        if(valor.length<=1){\\n            $(this).parents(\\".form-group\\").addClass(\\"has-error\\");\\n        } else {\\n            $(this).parents(\\".form-group\\").removeClass(\\"has-error\\");\\n        }\\n    });\\n    $contenInputs.find(\\"input.word-game\\").each(function() {\\n        var valor = $(this).val();\\n        if(valor.length<=1){\\n            rspta = false;\\n            return false;\\n        }\\n    });\\n    return rspta;\\n};\\n\\nvar obtenerPalabras = function(){\\n    var palabras = [];\\n\\n    var $contenInputs = $(\\"#list-words-game\\");\\n    $contenInputs.find(\\"input.word-game\\").each(function() {\\n        var valor = $(this).val();\\n        $(this).attr(\\"value\\", valor);\\n        palabras.push(valor);\\n    });\\n\\n    return palabras;\\n};\\n\\n$(\\"#list-words-game\\")\\n    .on(\\"keydown\\", \\"input.word-game\\", function(e){\\n        if (e.which === 32)\\n            return false;\\n        if( $(this).parents(\\".form-group\\").hasClass(\\"has-error\\") ) inputsValidos();\\n    })\\n    .on(\\"change\\", \\"input.word-game\\", function(e){\\n        this.value = this.value.replace(/\\\\s/g, \\"\\");\\n    })\\n    .on(\\"click\\", \\"span.form-control-feedback\\", function(e) {\\n        e.preventDefault();\\n        $(this).parents(\\".item-word-game\\").remove();\\n    });;\\n\\n$(\\".btn.add-word-game\\").click(function(e) {\\n    e.preventDefault();\\n    var id_cloneFrom = $(this).data(\\"clonefrom\\");\\n    var id_cloneTo = $(this).data(\\"cloneto\\");\\n    var now = Date.now();\\n    var newWord_html = $(id_cloneFrom).clone(true, true);\\n\\n    newWord_html.removeClass(\\"hidden\\");\\n    newWord_html.removeAttr(\\"id\\");\\n    newWord_html.find(\\"input.word-game\\").attr(\\"id\\", \\"input-\\"+now);\\n    newWord_html.find(\\"span.form-control-feedback\\").attr(\\"data-tooltip\\", \\"tooltip\\");\\n    $(id_cloneTo).append(newWord_html);\\n    \\n    $(\\"#input-\\"+now).focus();\\n    $(\\"*[data-tooltip=\\\\\\"tooltip\\\\\\"]\\").tooltip();\\n});\\n\\n$(\\".btn.create-word-search\\").click(function(e) {\\n    e.preventDefault();\\n    if( inputsValidos() ){\\n        var idPuzzle = \\"#puzzle\\";\\n        var idWords = \\"#words\\";\\n        var words = obtenerPalabras();\\n        var gamePuzzle = wordfindgame.create(words, idPuzzle, idWords);\\n\\n        $(\\"#pnl-edit\\").hide();\\n        $(\\"#findword-game\\").show(\\"fast\\");\\n    } else {\\n        $(\\"#list-words-game\\").find(\\".has-error\\").first().find(\\"input.word-game\\").focus();\\n        mostrar_notificacion(\\"Attention\\",\\"words must have more than one letter\\", \\"warning\\");\\n    }\\n});\\n\\n$(\\".edit-wordfind\\").click(function(e) {\\n    e.preventDefault();\\n\\n    $(\\"#pnl-edit\\").show(\\"fast\\");\\n    $(\\"#findword-game\\").hide();\\n\\n    $(\\"#puzzle\\").html(\\"\\");\\n    $(\\"#words\\").html(\\"\\");\\n});\\n\\n$(\\".btn.save-wordfind\\").click(function(e) {\\n    e.preventDefault();\\n    msjes = {\\n        \'attention\' : \'Attention\',\\n        \'guardado_correcto\' : \'Game saved successfully\'\\n    }\\n    saveGame(msjes);\\n});\\n\\nvar iniciarSopaLetras = function(){\\n    var arrWords=[];\\n    if( $(\'#words\').html()!=\'\' ){\\n        $(\'#words\').find(\'li.word\').each(function() {\\n            var word = $(this).text();\\n            arrWords.push(word);\\n        });\\n\\n        var gamePuzzle = wordfindgame.create(arrWords, \'#puzzle\', \'#words\');\\n    }\\n\\n    var valRol = $(\'#hRol\').val();\\n    if( valRol!=\'\' && valRol!=undefined ){\\n        if(valRol != 1){\\n            $(\'.plantilla-sopaletras\').find(\'.nopreview\').remove();\\n        }\\n    }\\n};\\n\\n$(document).ready(function() {\\n    iniciarSopaLetras();\\n});\\n\\n</script>        "}]', 32);
INSERT INTO `tarea_archivos` (`idtarea_archivos`, `tablapadre`, `idpadre`, `nombre`, `ruta`, `tipo`, `puntaje`, `habilidad`, `texto`, `idtarea_archivos_padre`) VALUES
(37, 'R', 5, 'Exam', '__xRUTABASEx__/tarea_archivos/visor/?idarchivo=', 'E', '83.30', '["8","9"]', '[{"id":"4","html":{"47":"<img src=\\"__xRUTABASEx__/static/media/image/N1-U1-A1-20170323063742.jpg\\" id=\\"image_question1490715231241\\" class=\\"img-responsive center-block\\">","48":"<div class=\\"titulo_descripcion\\"><b class=\\"npregunta\\">1.- Question  </b><h3 class=\\"titulo\\" data-orden=\\"1\\"></h3><p class=\\"descripcion\\"></p></div><div class=\\"ejercicio plantilla plantilla-img_puntos\\">\\n      <input type=\\"hidden\\" name=\\"idgui\\" id=\\"idgui\\" value=\\"58da827340c3f\\">\\n      <div class=\\"row\\">\\n        <div class=\\"col-xs-12 botones-edicion nopreview\\" style=\\"display: none;\\">\\n            <a class=\\"btn btn-primary selimage\\" data-tipo=\\"image\\" data-url=\\".img-dots\\">\\n                <i class=\\"fa fa-image\\"></i> \\n                Choose image \\n            </a>\\n            <a class=\\"btn btn-primary start-tag\\" style=\\"display: inline-block;\\"> \\n                <i class=\\"fa fa-dot-circle-o\\"></i>\\n                Start tagging \\n            </a>\\n            <a class=\\"btn btn-red stop-tag\\" style=\\"display: none;\\">\\n                <i class=\\"fa fa-stop\\"></i> \\n                Stop tagging \\n            </a>\\n            <a class=\\"btn btn-danger delete-tag pull-right\\">\\n                <i class=\\"fa fa-trash-o\\"></i> \\n                Delete tags \\n            </a>\\n            <a class=\\"btn btn-danger stop-del-tag pull-right\\" style=\\"display: none;\\">\\n                <i class=\\"fa fa-stop\\"></i> \\n                Stop deleting tags \\n            </a>\\n            <a class=\\"btn btn-success back-edit-tag hidden\\" style=\\"display: none !important;\\">\\n                <i class=\\"fa fa-pencil\\"></i> \\n                Back and edit            </a>\\n\\n            <div class=\\"hidden dot-container edition clone_punto\\" data-id=\\"clone_punto58da827340c3f\\">\\n                <div class=\\"dot\\"></div>\\n                <div class=\\"dot-tag\\"> </div>\\n            </div>\\n        </div>\\n        <div class=\\"col-xs-12 col-md-9 contenedor-img\\">\\n            <picture>\\n                <img src=\\"__xRUTABASEx__/static/media/image/N1-U1-A1-20170323063742.jpg\\" class=\\"img-responsive valvideo img-dots\\" style=\\"display: inline-block;\\" data-nombre-file=\\"leisure.jpg\\">\\n                <img src=\\"__xRUTABASEx__/static/media/web/noimage.png\\" class=\\"img-responsive nopreview\\" style=\\"width: initial; margin: 0px auto; display: none;\\">\\n            </picture>\\n            <div class=\\"mask-dots playing\\">\\n            <div class=\\"dot-container\\" id=\\"dot_1490715263797\\" style=\\"left: 32.147%; top: 52.455%;\\">\\n                <div class=\\"dot\\"></div>\\n                <div class=\\"dot-tag corregido\\" data-number=\\"1\\" data-corregido=\\"good\\">sillon</div>\\n            </div><div class=\\"dot-container\\" id=\\"dot_1490715266104\\" style=\\"left: 60.4669%; top: 21.6423%;\\">\\n                <div class=\\"dot\\"></div>\\n                <div class=\\"dot-tag corregido\\" data-number=\\"2\\" data-corregido=\\"good\\">cabeza</div>\\n            </div><div class=\\"dot-container\\" id=\\"dot_1490715267983\\" style=\\"left: 60.9261%; top: 50.4986%;\\">\\n                <div class=\\"dot\\"></div>\\n                <div class=\\"dot-tag corregido\\" data-number=\\"3\\" data-corregido=\\"good\\">brazo</div>\\n            </div></div>\\n        </div>\\n\\n        <div class=\\"col-xs-12 col-md-3 tag-alts-edit nopreview\\" style=\\"display: none;\\"> <div class=\\"input-group\\" id=\\"edit_tag_1490715263797\\"><span class=\\"input-group-addon\\">1</span><input type=\\"text\\" class=\\"form-control\\" placeholder=\\"Write tag\\" value=\\"sillon\\"></div><div class=\\"input-group\\" id=\\"edit_tag_1490715266104\\"><span class=\\"input-group-addon\\">2</span><input type=\\"text\\" class=\\"form-control\\" placeholder=\\"Write tag\\" value=\\"cabeza\\"></div><div class=\\"input-group\\" id=\\"edit_tag_1490715267983\\"><span class=\\"input-group-addon\\">3</span><input type=\\"text\\" class=\\"form-control\\" placeholder=\\"Write tag\\" value=\\"brazo\\"></div></div>\\n\\n        <div class=\\"col-xs-12 col-md-3 tag-alts\\" style=\\"\\"><div class=\\"tag\\" id=\\"tag_1490715266104\\"><div class=\\"arrow\\"></div>cabeza</div><div class=\\"tag\\" id=\\"tag_1490715263797\\"><div class=\\"arrow\\"></div>sillon</div><div class=\\"tag\\" id=\\"tag_1490715267983\\"><div class=\\"arrow\\"></div>brazo</div></div>\\n      </div>\\n    </div>","49":"<div class=\\"titulo_descripcion\\"><b class=\\"npregunta\\">2.- Question  </b><h3 class=\\"titulo\\" data-orden=\\"2\\"></h3><p class=\\"descripcion\\"></p></div><div class=\\"ejercicio plantilla plantilla-completar\\"><div class=\\"panelEjercicio img-thumbnail panel pnl100\\"><p>The girls is <div class=\\"inp-corregido wrapper\\" data-corregido=\\"good\\"><input data-options=\\"running,reading\\" data-mce-object=\\"input\\" height=\\"25\\" width=\\"100\\" type=\\"text\\" data-texto=\\"sleeping\\" class=\\"mce-object-input valinput isdrop ui-droppable\\" value=\\"sleeping\\"></div></p></div><div class=\\"panelAlternativas img-thumbnail panel pnl100\\"><span class=\\"isdragable ui-draggable ui-draggable-handle active\\" style=\\"position:relative\\">sleeping</span>,<span class=\\"isdragable ui-draggable ui-draggable-handle\\" style=\\"position: relative;\\">reading</span>,<span class=\\"isdragable ui-draggable ui-draggable-handle\\" style=\\"position: relative;\\">running</span></div></div>","52":"<div class=\\"titulo_descripcion\\"><b class=\\"npregunta\\">3.- Question  </b><h3 class=\\"titulo\\" data-orden=\\"3\\"></h3><p class=\\"descripcion\\"></p></div><div class=\\"ejercicio plantilla plantilla-verdad_falso\\"><div class=\\"row list-premises tpl_plantilla\\" id=\\"pnl_premisas58da8379d637e\\" data-idgui=\\"58da8379d637e\\">\\n        <!-- Premises container -->\\n    <div class=\\"col-xs-12 premise pr-1490715511496 bad\\">\\n            <div class=\\"col-xs-12 col-sm-5\\">\\n                <span class=\\"live-edit addtext\\" data-initial_val=\\"The girl is running\\">The girl is running</span>\\n                <input type=\\"hidden\\" class=\\"valPremise valin nopreview\\" data-nopreview=\\".premise.pr-1490715511496\\" data-cini=\\"1\\" data-valueini=\\"Premise\\" value=\\"The girl is running\\">\\n            </div>\\n            <div class=\\"col-xs-12 col-sm-6 options\\">\\n                <label class=\\"col-xs-6\\"><input type=\\"radio\\" class=\\"radio-ctrl\\" name=\\"opcPremise-1490715511496\\" value=\\"T\\" checked=\\"checked\\"> True</label>\\n                <label class=\\"col-xs-6\\"><input type=\\"radio\\" class=\\"radio-ctrl\\" name=\\"opcPremise-1490715511496\\" value=\\"F\\"> False</label>\\n            </div>\\n            <div class=\\"col-xs-2 col-sm-1 icon-zone\\">\\n                <a href=\\"#\\" class=\\"btn color-danger btn-xs tooltip delete nopreview\\" title=\\"remove\\" style=\\"display: none;\\"><i class=\\"fa fa-trash \\"></i></a>\\n                <span class=\\"icon-result\\"></span>\\n            </div>\\n            <input type=\\"hidden\\" id=\\"opcPremise-1490715511496\\" data-lock=\\"1490715522184\\" value=\\"0a1fd1bbda6822e43c4e99cf66447e8d\\">\\n        </div><div class=\\"col-xs-12 premise pr-1490715523520 good\\">\\n            <div class=\\"col-xs-12 col-sm-5\\">\\n                <span class=\\"live-edit addtext\\" data-initial_val=\\"She is sleeping\\">She is sleeping</span>\\n                <input type=\\"hidden\\" class=\\"valPremise valin nopreview\\" data-nopreview=\\".premise.pr-1490715523520\\" data-cini=\\"1\\" data-valueini=\\"Premise\\" value=\\"She is sleeping\\">\\n            </div>\\n            <div class=\\"col-xs-12 col-sm-6 options\\">\\n                <label class=\\"col-xs-6\\"><input type=\\"radio\\" class=\\"radio-ctrl\\" name=\\"opcPremise-1490715523520\\" value=\\"T\\" checked=\\"checked\\"> True</label>\\n                <label class=\\"col-xs-6\\"><input type=\\"radio\\" class=\\"radio-ctrl\\" name=\\"opcPremise-1490715523520\\" value=\\"F\\"> False</label>\\n            </div>\\n            <div class=\\"col-xs-2 col-sm-1 icon-zone\\">\\n                <a href=\\"#\\" class=\\"btn color-danger btn-xs tooltip delete nopreview\\" title=\\"remove\\" style=\\"display: none;\\"><i class=\\"fa fa-trash \\"></i></a>\\n                <span class=\\"icon-result\\"></span>\\n            </div>\\n            <input type=\\"hidden\\" id=\\"opcPremise-1490715523520\\" data-lock=\\"1490715532020\\" value=\\"4fd55d98fe288e7bdb5be0914b8b7e2d\\">\\n        </div><div class=\\"col-xs-12 premise pr-1490715533740 bad\\">\\n            <div class=\\"col-xs-12 col-sm-5\\">\\n                <span class=\\"live-edit addtext\\" data-initial_val=\\"She is on the sofa\\">She is on the sofa</span>\\n                <input type=\\"hidden\\" class=\\"valPremise valin nopreview\\" data-nopreview=\\".premise.pr-1490715533740\\" data-cini=\\"1\\" data-valueini=\\"Premise\\" value=\\"She is on the sofa\\">\\n            </div>\\n            <div class=\\"col-xs-12 col-sm-6 options\\">\\n                <label class=\\"col-xs-6\\"><input type=\\"radio\\" class=\\"radio-ctrl\\" name=\\"opcPremise-1490715533740\\" value=\\"T\\"> True</label>\\n                <label class=\\"col-xs-6\\"><input type=\\"radio\\" class=\\"radio-ctrl\\" name=\\"opcPremise-1490715533740\\" value=\\"F\\" checked=\\"checked\\"> False</label>\\n            </div>\\n            <div class=\\"col-xs-2 col-sm-1 icon-zone\\">\\n                <a href=\\"#\\" class=\\"btn color-danger btn-xs tooltip delete nopreview\\" title=\\"remove\\" style=\\"display: none;\\"><i class=\\"fa fa-trash \\"></i></a>\\n                <span class=\\"icon-result\\"></span>\\n            </div>\\n            <input type=\\"hidden\\" id=\\"opcPremise-1490715533740\\" data-lock=\\"1490715541066\\" value=\\"1b2e2b1a0e678246e454bda951190c46\\">\\n        </div></div></div>","53":"<img src=\\"__xRUTABASEx__/static/media/image/N1-U1-A1-20170327111122.jpg\\" id=\\"image_question1490719851651\\" class=\\"img-responsive center-block\\">","54":"<div class=\\"titulo_descripcion\\"><b class=\\"npregunta\\">4.- Question  </b><h3 class=\\"titulo\\" data-orden=\\"4\\"></h3><p class=\\"descripcion\\"></p></div><div class=\\"ejercicio plantilla plantilla-completar\\"><div class=\\"panelEjercicio img-thumbnail panel pnl100\\"><p>peruavian <div class=\\"inp-corregido wrapper\\" data-corregido=\\"good\\"><input data-options=\\"feet,full\\" data-mce-object=\\"input\\" height=\\"25\\" width=\\"100\\" type=\\"text\\" data-texto=\\"food\\" class=\\"mce-object-input valinput isdrop ui-droppable\\" value=\\"food\\"></div> is delicious</p></div><div class=\\"panelAlternativas img-thumbnail panel pnl100\\"><span class=\\"isdragable ui-draggable ui-draggable-handle active\\" style=\\"position:relative\\">food</span>,<span class=\\"isdragable ui-draggable ui-draggable-handle\\" style=\\"position: relative;\\">feet</span>,<span class=\\"isdragable ui-draggable ui-draggable-handle\\" style=\\"position: relative;\\">full</span></div></div>"}}]', 33);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tarea_asignacion`
--

CREATE TABLE `tarea_asignacion` (
  `idtarea_asignacion` int(11) NOT NULL,
  `idtarea` int(11) NOT NULL,
  `idgrupo` int(11) NOT NULL,
  `fechaentrega` date NOT NULL,
  `horaentrega` time NOT NULL,
  `eliminado` int(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `tarea_asignacion`
--

INSERT INTO `tarea_asignacion` (`idtarea_asignacion`, `idtarea`, `idgrupo`, `fechaentrega`, `horaentrega`, `eliminado`) VALUES
(1, 1, 1, '2017-07-10', '19:50:00', 0),
(2, 2, 1, '2017-07-19', '20:00:00', 0),
(3, 3, 1, '2017-07-21', '18:00:00', 0),
(4, 4, 1, '2017-07-19', '13:00:00', 0),
(5, 5, 1, '2017-07-28', '22:02:00', 0),
(6, 6, 1, '2017-08-01', '10:03:00', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tarea_asignacion_alumno`
--

CREATE TABLE `tarea_asignacion_alumno` (
  `iddetalle` int(11) NOT NULL,
  `idtarea_asignacion` int(11) NOT NULL,
  `idalumno` int(11) NOT NULL,
  `mensajedevolucion` text COLLATE utf8_spanish_ci,
  `notapromedio` decimal(5,2) DEFAULT NULL,
  `estado` varchar(1) COLLATE utf8_spanish_ci NOT NULL COMMENT 'N=Nuevo; P=Presentado; D=Devuelto; E=Evaluado'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `tarea_asignacion_alumno`
--

INSERT INTO `tarea_asignacion_alumno` (`iddetalle`, `idtarea_asignacion`, `idalumno`, `mensajedevolucion`, `notapromedio`, `estado`) VALUES
(1, 1, 72042592, NULL, NULL, 'P'),
(2, 1, 12345678, NULL, NULL, 'P'),
(3, 1, 87654321, NULL, NULL, 'P'),
(8, 2, 12345678, NULL, NULL, 'P'),
(9, 2, 87654321, NULL, NULL, 'P'),
(7, 2, 72042592, 'Complementa con mas archivos', '15.00', 'E'),
(10, 3, 72042592, 'Mejora tu examen', '51.20', 'E'),
(14, 4, 72042592, NULL, NULL, 'P'),
(15, 4, 12345678, NULL, NULL, 'P'),
(16, 4, 87654321, NULL, NULL, 'P'),
(17, 5, 72042592, NULL, NULL, 'P'),
(18, 5, 12345678, NULL, NULL, 'P'),
(19, 5, 87654321, NULL, NULL, 'P'),
(20, 6, 72042592, NULL, '53.30', 'E'),
(21, 6, 12345678, NULL, NULL, 'N'),
(22, 6, 87654321, NULL, NULL, 'N');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tarea_respuesta`
--

CREATE TABLE `tarea_respuesta` (
  `idtarea_respuesta` int(11) NOT NULL,
  `idtarea_asignacion_alumno` int(11) NOT NULL COMMENT 'tarea_asignacion_alumno->iddetalle',
  `comentario` text COLLATE utf8_spanish_ci,
  `fechapresentacion` date NOT NULL,
  `horapresentacion` time NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `tarea_respuesta`
--

INSERT INTO `tarea_respuesta` (`idtarea_respuesta`, `idtarea_asignacion_alumno`, `comentario`, `fechapresentacion`, `horapresentacion`) VALUES
(1, 7, 'Complete mi tarea.\nEstoy adjuntando archivo de la solucion de mi tarea.', '2017-07-11', '10:53:17'),
(2, 10, NULL, '2017-07-15', '10:02:38'),
(3, 14, NULL, '2017-07-18', '10:46:48'),
(4, 17, 'He resuelto mi tarea , por favor revisar.', '2017-07-20', '10:03:46'),
(5, 20, 'Adjunto los respuestas de mi tarea.', '2017-07-27', '10:11:05');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ubigeo`
--

CREATE TABLE `ubigeo` (
  `id_ubigeo` char(6) COLLATE utf8_spanish_ci NOT NULL,
  `pais` char(2) COLLATE utf8_spanish_ci NOT NULL,
  `departamento` char(2) COLLATE utf8_spanish_ci DEFAULT '00',
  `provincia` char(2) COLLATE utf8_spanish_ci NOT NULL DEFAULT '00',
  `distrito` char(2) COLLATE utf8_spanish_ci NOT NULL DEFAULT '00',
  `ciudad` varchar(100) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `ubigeo`
--

INSERT INTO `ubigeo` (`id_ubigeo`, `pais`, `departamento`, `provincia`, `distrito`, `ciudad`) VALUES
('010000', 'PE', '01', '00', '00', 'Amazonas'),
('010100', 'PE', '01', '01', '00', 'Chachapoyas'),
('010101', 'PE', '01', '01', '01', 'Chachapoyas'),
('010102', 'PE', '01', '01', '02', 'Asunción'),
('010103', 'PE', '01', '01', '03', 'Balsas'),
('010104', 'PE', '01', '01', '04', 'Cheto'),
('010105', 'PE', '01', '01', '05', 'Chiliquin'),
('010106', 'PE', '01', '01', '06', 'Chuquibamba'),
('010107', 'PE', '01', '01', '07', 'Granada'),
('010108', 'PE', '01', '01', '08', 'Huancas'),
('010109', 'PE', '01', '01', '09', 'La Jalca'),
('010110', 'PE', '01', '01', '10', 'Leimebamba'),
('010111', 'PE', '01', '01', '11', 'Levanto'),
('010112', 'PE', '01', '01', '12', 'Magdalena'),
('010113', 'PE', '01', '01', '13', 'Mariscal Castilla'),
('010114', 'PE', '01', '01', '14', 'Molinopampa'),
('010115', 'PE', '01', '01', '15', 'Montevideo'),
('010116', 'PE', '01', '01', '16', 'Olleros'),
('010117', 'PE', '01', '01', '17', 'Quinjalca'),
('010118', 'PE', '01', '01', '18', 'San Francisco de Daguas'),
('010119', 'PE', '01', '01', '19', 'San Isidro de Maino'),
('010120', 'PE', '01', '01', '20', 'Soloco'),
('010121', 'PE', '01', '01', '21', 'Sonche'),
('010200', 'PE', '01', '02', '00', 'Bagua'),
('010201', 'PE', '01', '02', '01', 'Bagua'),
('010202', 'PE', '01', '02', '02', 'Aramango'),
('010203', 'PE', '01', '02', '03', 'Copallin'),
('010204', 'PE', '01', '02', '04', 'El Parco'),
('010205', 'PE', '01', '02', '05', 'Imaza'),
('010206', 'PE', '01', '02', '06', 'La Peca'),
('010300', 'PE', '01', '03', '00', 'Bongará'),
('010301', 'PE', '01', '03', '01', 'Jumbilla'),
('010302', 'PE', '01', '03', '02', 'Chisquilla'),
('010303', 'PE', '01', '03', '03', 'Churuja'),
('010304', 'PE', '01', '03', '04', 'Corosha'),
('010305', 'PE', '01', '03', '05', 'Cuispes'),
('010306', 'PE', '01', '03', '06', 'Florida'),
('010307', 'PE', '01', '03', '07', 'Jazan'),
('010308', 'PE', '01', '03', '08', 'Recta'),
('010309', 'PE', '01', '03', '09', 'San Carlos'),
('010310', 'PE', '01', '03', '10', 'Shipasbamba'),
('010311', 'PE', '01', '03', '11', 'Valera'),
('010312', 'PE', '01', '03', '12', 'Yambrasbamba'),
('010400', 'PE', '01', '04', '00', 'Condorcanqui'),
('010401', 'PE', '01', '04', '01', 'Nieva'),
('010402', 'PE', '01', '04', '02', 'El Cenepa'),
('010403', 'PE', '01', '04', '03', 'Río Santiago'),
('010500', 'PE', '01', '05', '00', 'Luya'),
('010501', 'PE', '01', '05', '01', 'Lamud'),
('010502', 'PE', '01', '05', '02', 'Camporredondo'),
('010503', 'PE', '01', '05', '03', 'Cocabamba'),
('010504', 'PE', '01', '05', '04', 'Colcamar'),
('010505', 'PE', '01', '05', '05', 'Conila'),
('010506', 'PE', '01', '05', '06', 'Inguilpata'),
('010507', 'PE', '01', '05', '07', 'Longuita'),
('010508', 'PE', '01', '05', '08', 'Lonya Chico'),
('010509', 'PE', '01', '05', '09', 'Luya'),
('010510', 'PE', '01', '05', '10', 'Luya Viejo'),
('010511', 'PE', '01', '05', '11', 'María'),
('010512', 'PE', '01', '05', '12', 'Ocalli'),
('010513', 'PE', '01', '05', '13', 'Ocumal'),
('010514', 'PE', '01', '05', '14', 'Pisuquia'),
('010515', 'PE', '01', '05', '15', 'Providencia'),
('010516', 'PE', '01', '05', '16', 'San Cristóbal'),
('010517', 'PE', '01', '05', '17', 'San Francisco de Yeso'),
('010518', 'PE', '01', '05', '18', 'San Jerónimo'),
('010519', 'PE', '01', '05', '19', 'San Juan de Lopecancha'),
('010520', 'PE', '01', '05', '20', 'Santa Catalina'),
('010521', 'PE', '01', '05', '21', 'Santo Tomas'),
('010522', 'PE', '01', '05', '22', 'Tingo'),
('010523', 'PE', '01', '05', '23', 'Trita'),
('010600', 'PE', '01', '06', '00', 'Rodríguez de Mendoza'),
('010601', 'PE', '01', '06', '01', 'San Nicolás'),
('010602', 'PE', '01', '06', '02', 'Chirimoto'),
('010603', 'PE', '01', '06', '03', 'Cochamal'),
('010604', 'PE', '01', '06', '04', 'Huambo'),
('010605', 'PE', '01', '06', '05', 'Limabamba'),
('010606', 'PE', '01', '06', '06', 'Longar'),
('010607', 'PE', '01', '06', '07', 'Mariscal Benavides'),
('010608', 'PE', '01', '06', '08', 'Milpuc'),
('010609', 'PE', '01', '06', '09', 'Omia'),
('010610', 'PE', '01', '06', '10', 'Santa Rosa'),
('010611', 'PE', '01', '06', '11', 'Totora'),
('010612', 'PE', '01', '06', '12', 'Vista Alegre'),
('010700', 'PE', '01', '07', '00', 'Utcubamba'),
('010701', 'PE', '01', '07', '01', 'Bagua Grande'),
('010702', 'PE', '01', '07', '02', 'Cajaruro'),
('010703', 'PE', '01', '07', '03', 'Cumba'),
('010704', 'PE', '01', '07', '04', 'El Milagro'),
('010705', 'PE', '01', '07', '05', 'Jamalca'),
('010706', 'PE', '01', '07', '06', 'Lonya Grande'),
('010707', 'PE', '01', '07', '07', 'Yamon'),
('020000', 'PE', '02', '00', '00', 'Áncash'),
('020100', 'PE', '02', '01', '00', 'Huaraz'),
('020101', 'PE', '02', '01', '01', 'Huaraz'),
('020102', 'PE', '02', '01', '02', 'Cochabamba'),
('020103', 'PE', '02', '01', '03', 'Colcabamba'),
('020104', 'PE', '02', '01', '04', 'Huanchay'),
('020105', 'PE', '02', '01', '05', 'Independencia'),
('020106', 'PE', '02', '01', '06', 'Jangas'),
('020107', 'PE', '02', '01', '07', 'La Libertad'),
('020108', 'PE', '02', '01', '08', 'Olleros'),
('020109', 'PE', '02', '01', '09', 'Pampas Grande'),
('020110', 'PE', '02', '01', '10', 'Pariacoto'),
('020111', 'PE', '02', '01', '11', 'Pira'),
('020112', 'PE', '02', '01', '12', 'Tarica'),
('020200', 'PE', '02', '02', '00', 'Aija'),
('020201', 'PE', '02', '02', '01', 'Aija'),
('020202', 'PE', '02', '02', '02', 'Coris'),
('020203', 'PE', '02', '02', '03', 'Huacllan'),
('020204', 'PE', '02', '02', '04', 'La Merced'),
('020205', 'PE', '02', '02', '05', 'Succha'),
('020300', 'PE', '02', '03', '00', 'Antonio Raymondi'),
('020301', 'PE', '02', '03', '01', 'Llamellin'),
('020302', 'PE', '02', '03', '02', 'Aczo'),
('020303', 'PE', '02', '03', '03', 'Chaccho'),
('020304', 'PE', '02', '03', '04', 'Chingas'),
('020305', 'PE', '02', '03', '05', 'Mirgas'),
('020306', 'PE', '02', '03', '06', 'San Juan de Rontoy'),
('020400', 'PE', '02', '04', '00', 'Asunción'),
('020401', 'PE', '02', '04', '01', 'Chacas'),
('020402', 'PE', '02', '04', '02', 'Acochaca'),
('020500', 'PE', '02', '05', '00', 'Bolognesi'),
('020501', 'PE', '02', '05', '01', 'Chiquian'),
('020502', 'PE', '02', '05', '02', 'Abelardo Pardo Lezameta'),
('020503', 'PE', '02', '05', '03', 'Antonio Raymondi'),
('020504', 'PE', '02', '05', '04', 'Aquia'),
('020505', 'PE', '02', '05', '05', 'Cajacay'),
('020506', 'PE', '02', '05', '06', 'Canis'),
('020507', 'PE', '02', '05', '07', 'Colquioc'),
('020508', 'PE', '02', '05', '08', 'Huallanca'),
('020509', 'PE', '02', '05', '09', 'Huasta'),
('020510', 'PE', '02', '05', '10', 'Huayllacayan'),
('020511', 'PE', '02', '05', '11', 'La Primavera'),
('020512', 'PE', '02', '05', '12', 'Mangas'),
('020513', 'PE', '02', '05', '13', 'Pacllon'),
('020514', 'PE', '02', '05', '14', 'San Miguel de Corpanqui'),
('020515', 'PE', '02', '05', '15', 'Ticllos'),
('020600', 'PE', '02', '06', '00', 'Carhuaz'),
('020601', 'PE', '02', '06', '01', 'Carhuaz'),
('020602', 'PE', '02', '06', '02', 'Acopampa'),
('020603', 'PE', '02', '06', '03', 'Amashca'),
('020604', 'PE', '02', '06', '04', 'Anta'),
('020605', 'PE', '02', '06', '05', 'Ataquero'),
('020606', 'PE', '02', '06', '06', 'Marcara'),
('020607', 'PE', '02', '06', '07', 'Pariahuanca'),
('020608', 'PE', '02', '06', '08', 'San Miguel de Aco'),
('020609', 'PE', '02', '06', '09', 'Shilla'),
('020610', 'PE', '02', '06', '10', 'Tinco'),
('020611', 'PE', '02', '06', '11', 'Yungar'),
('020700', 'PE', '02', '07', '00', 'Carlos Fermín Fitzcarrald'),
('020701', 'PE', '02', '07', '01', 'San Luis'),
('020702', 'PE', '02', '07', '02', 'San Nicolás'),
('020703', 'PE', '02', '07', '03', 'Yauya'),
('020800', 'PE', '02', '08', '00', 'Casma'),
('020801', 'PE', '02', '08', '01', 'Casma'),
('020802', 'PE', '02', '08', '02', 'Buena Vista Alta'),
('020803', 'PE', '02', '08', '03', 'Comandante Noel'),
('020804', 'PE', '02', '08', '04', 'Yautan'),
('020900', 'PE', '02', '09', '00', 'Corongo'),
('020901', 'PE', '02', '09', '01', 'Corongo'),
('020902', 'PE', '02', '09', '02', 'Aco'),
('020903', 'PE', '02', '09', '03', 'Bambas'),
('020904', 'PE', '02', '09', '04', 'Cusca'),
('020905', 'PE', '02', '09', '05', 'La Pampa'),
('020906', 'PE', '02', '09', '06', 'Yanac'),
('020907', 'PE', '02', '09', '07', 'Yupan'),
('021000', 'PE', '02', '10', '00', 'Huari'),
('021001', 'PE', '02', '10', '01', 'Huari'),
('021002', 'PE', '02', '10', '02', 'Anra'),
('021003', 'PE', '02', '10', '03', 'Cajay'),
('021004', 'PE', '02', '10', '04', 'Chavin de Huantar'),
('021005', 'PE', '02', '10', '05', 'Huacachi'),
('021006', 'PE', '02', '10', '06', 'Huacchis'),
('021007', 'PE', '02', '10', '07', 'Huachis'),
('021008', 'PE', '02', '10', '08', 'Huantar'),
('021009', 'PE', '02', '10', '09', 'Masin'),
('021010', 'PE', '02', '10', '10', 'Paucas'),
('021011', 'PE', '02', '10', '11', 'Ponto'),
('021012', 'PE', '02', '10', '12', 'Rahuapampa'),
('021013', 'PE', '02', '10', '13', 'Rapayan'),
('021014', 'PE', '02', '10', '14', 'San Marcos'),
('021015', 'PE', '02', '10', '15', 'San Pedro de Chana'),
('021016', 'PE', '02', '10', '16', 'Uco'),
('021100', 'PE', '02', '11', '00', 'Huarmey'),
('021101', 'PE', '02', '11', '01', 'Huarmey'),
('021102', 'PE', '02', '11', '02', 'Cochapeti'),
('021103', 'PE', '02', '11', '03', 'Culebras'),
('021104', 'PE', '02', '11', '04', 'Huayan'),
('021105', 'PE', '02', '11', '05', 'Malvas'),
('021200', 'PE', '02', '12', '00', 'Huaylas'),
('021201', 'PE', '02', '12', '01', 'Caraz'),
('021202', 'PE', '02', '12', '02', 'Huallanca'),
('021203', 'PE', '02', '12', '03', 'Huata'),
('021204', 'PE', '02', '12', '04', 'Huaylas'),
('021205', 'PE', '02', '12', '05', 'Mato'),
('021206', 'PE', '02', '12', '06', 'Pamparomas'),
('021207', 'PE', '02', '12', '07', 'Pueblo Libre'),
('021208', 'PE', '02', '12', '08', 'Santa Cruz'),
('021209', 'PE', '02', '12', '09', 'Santo Toribio'),
('021210', 'PE', '02', '12', '10', 'Yuracmarca'),
('021300', 'PE', '02', '13', '00', 'Mariscal Luzuriaga'),
('021301', 'PE', '02', '13', '01', 'Piscobamba'),
('021302', 'PE', '02', '13', '02', 'Casca'),
('021303', 'PE', '02', '13', '03', 'Eleazar Guzmán Barron'),
('021304', 'PE', '02', '13', '04', 'Fidel Olivas Escudero'),
('021305', 'PE', '02', '13', '05', 'Llama'),
('021306', 'PE', '02', '13', '06', 'Llumpa'),
('021307', 'PE', '02', '13', '07', 'Lucma'),
('021308', 'PE', '02', '13', '08', 'Musga'),
('021400', 'PE', '02', '14', '00', 'Ocros'),
('021401', 'PE', '02', '14', '01', 'Ocros'),
('021402', 'PE', '02', '14', '02', 'Acas'),
('021403', 'PE', '02', '14', '03', 'Cajamarquilla'),
('021404', 'PE', '02', '14', '04', 'Carhuapampa'),
('021405', 'PE', '02', '14', '05', 'Cochas'),
('021406', 'PE', '02', '14', '06', 'Congas'),
('021407', 'PE', '02', '14', '07', 'Llipa'),
('021408', 'PE', '02', '14', '08', 'San Cristóbal de Rajan'),
('021409', 'PE', '02', '14', '09', 'San Pedro'),
('021410', 'PE', '02', '14', '10', 'Santiago de Chilcas'),
('021500', 'PE', '02', '15', '00', 'Pallasca'),
('021501', 'PE', '02', '15', '01', 'Cabana'),
('021502', 'PE', '02', '15', '02', 'Bolognesi'),
('021503', 'PE', '02', '15', '03', 'Conchucos'),
('021504', 'PE', '02', '15', '04', 'Huacaschuque'),
('021505', 'PE', '02', '15', '05', 'Huandoval'),
('021506', 'PE', '02', '15', '06', 'Lacabamba'),
('021507', 'PE', '02', '15', '07', 'Llapo'),
('021508', 'PE', '02', '15', '08', 'Pallasca'),
('021509', 'PE', '02', '15', '09', 'Pampas'),
('021510', 'PE', '02', '15', '10', 'Santa Rosa'),
('021511', 'PE', '02', '15', '11', 'Tauca'),
('021600', 'PE', '02', '16', '00', 'Pomabamba'),
('021601', 'PE', '02', '16', '01', 'Pomabamba'),
('021602', 'PE', '02', '16', '02', 'Huayllan'),
('021603', 'PE', '02', '16', '03', 'Parobamba'),
('021604', 'PE', '02', '16', '04', 'Quinuabamba'),
('021700', 'PE', '02', '17', '00', 'Recuay'),
('021701', 'PE', '02', '17', '01', 'Recuay'),
('021702', 'PE', '02', '17', '02', 'Catac'),
('021703', 'PE', '02', '17', '03', 'Cotaparaco'),
('021704', 'PE', '02', '17', '04', 'Huayllapampa'),
('021705', 'PE', '02', '17', '05', 'Llacllin'),
('021706', 'PE', '02', '17', '06', 'Marca'),
('021707', 'PE', '02', '17', '07', 'Pampas Chico'),
('021708', 'PE', '02', '17', '08', 'Pararin'),
('021709', 'PE', '02', '17', '09', 'Tapacocha'),
('021710', 'PE', '02', '17', '10', 'Ticapampa'),
('021800', 'PE', '02', '18', '00', 'Santa'),
('021801', 'PE', '02', '18', '01', 'Chimbote'),
('021802', 'PE', '02', '18', '02', 'Cáceres del Perú'),
('021803', 'PE', '02', '18', '03', 'Coishco'),
('021804', 'PE', '02', '18', '04', 'Macate'),
('021805', 'PE', '02', '18', '05', 'Moro'),
('021806', 'PE', '02', '18', '06', 'Nepeña'),
('021807', 'PE', '02', '18', '07', 'Samanco'),
('021808', 'PE', '02', '18', '08', 'Santa'),
('021809', 'PE', '02', '18', '09', 'Nuevo Chimbote'),
('021900', 'PE', '02', '19', '00', 'Sihuas'),
('021901', 'PE', '02', '19', '01', 'Sihuas'),
('021902', 'PE', '02', '19', '02', 'Acobamba'),
('021903', 'PE', '02', '19', '03', 'Alfonso Ugarte'),
('021904', 'PE', '02', '19', '04', 'Cashapampa'),
('021905', 'PE', '02', '19', '05', 'Chingalpo'),
('021906', 'PE', '02', '19', '06', 'Huayllabamba'),
('021907', 'PE', '02', '19', '07', 'Quiches'),
('021908', 'PE', '02', '19', '08', 'Ragash'),
('021909', 'PE', '02', '19', '09', 'San Juan'),
('021910', 'PE', '02', '19', '10', 'Sicsibamba'),
('022000', 'PE', '02', '20', '00', 'Yungay'),
('022001', 'PE', '02', '20', '01', 'Yungay'),
('022002', 'PE', '02', '20', '02', 'Cascapara'),
('022003', 'PE', '02', '20', '03', 'Mancos'),
('022004', 'PE', '02', '20', '04', 'Matacoto'),
('022005', 'PE', '02', '20', '05', 'Quillo'),
('022006', 'PE', '02', '20', '06', 'Ranrahirca'),
('022007', 'PE', '02', '20', '07', 'Shupluy'),
('022008', 'PE', '02', '20', '08', 'Yanama'),
('030000', 'PE', '03', '00', '00', 'Apurímac'),
('030100', 'PE', '03', '01', '00', 'Abancay'),
('030101', 'PE', '03', '01', '01', 'Abancay'),
('030102', 'PE', '03', '01', '02', 'Chacoche'),
('030103', 'PE', '03', '01', '03', 'Circa'),
('030104', 'PE', '03', '01', '04', 'Curahuasi'),
('030105', 'PE', '03', '01', '05', 'Huanipaca'),
('030106', 'PE', '03', '01', '06', 'Lambrama'),
('030107', 'PE', '03', '01', '07', 'Pichirhua'),
('030108', 'PE', '03', '01', '08', 'San Pedro de Cachora'),
('030109', 'PE', '03', '01', '09', 'Tamburco'),
('030200', 'PE', '03', '02', '00', 'Andahuaylas'),
('030201', 'PE', '03', '02', '01', 'Andahuaylas'),
('030202', 'PE', '03', '02', '02', 'Andarapa'),
('030203', 'PE', '03', '02', '03', 'Chiara'),
('030204', 'PE', '03', '02', '04', 'Huancarama'),
('030205', 'PE', '03', '02', '05', 'Huancaray'),
('030206', 'PE', '03', '02', '06', 'Huayana'),
('030207', 'PE', '03', '02', '07', 'Kishuara'),
('030208', 'PE', '03', '02', '08', 'Pacobamba'),
('030209', 'PE', '03', '02', '09', 'Pacucha'),
('030210', 'PE', '03', '02', '10', 'Pampachiri'),
('030211', 'PE', '03', '02', '11', 'Pomacocha'),
('030212', 'PE', '03', '02', '12', 'San Antonio de Cachi'),
('030213', 'PE', '03', '02', '13', 'San Jerónimo'),
('030214', 'PE', '03', '02', '14', 'San Miguel de Chaccrampa'),
('030215', 'PE', '03', '02', '15', 'Santa María de Chicmo'),
('030216', 'PE', '03', '02', '16', 'Talavera'),
('030217', 'PE', '03', '02', '17', 'Tumay Huaraca'),
('030218', 'PE', '03', '02', '18', 'Turpo'),
('030219', 'PE', '03', '02', '19', 'Kaquiabamba'),
('030220', 'PE', '03', '02', '20', 'José María Arguedas'),
('030300', 'PE', '03', '03', '00', 'Antabamba'),
('030301', 'PE', '03', '03', '01', 'Antabamba'),
('030302', 'PE', '03', '03', '02', 'El Oro'),
('030303', 'PE', '03', '03', '03', 'Huaquirca'),
('030304', 'PE', '03', '03', '04', 'Juan Espinoza Medrano'),
('030305', 'PE', '03', '03', '05', 'Oropesa'),
('030306', 'PE', '03', '03', '06', 'Pachaconas'),
('030307', 'PE', '03', '03', '07', 'Sabaino'),
('030400', 'PE', '03', '04', '00', 'Aymaraes'),
('030401', 'PE', '03', '04', '01', 'Chalhuanca'),
('030402', 'PE', '03', '04', '02', 'Capaya'),
('030403', 'PE', '03', '04', '03', 'Caraybamba'),
('030404', 'PE', '03', '04', '04', 'Chapimarca'),
('030405', 'PE', '03', '04', '05', 'Colcabamba'),
('030406', 'PE', '03', '04', '06', 'Cotaruse'),
('030407', 'PE', '03', '04', '07', 'Huayllo'),
('030408', 'PE', '03', '04', '08', 'Justo Apu Sahuaraura'),
('030409', 'PE', '03', '04', '09', 'Lucre'),
('030410', 'PE', '03', '04', '10', 'Pocohuanca'),
('030411', 'PE', '03', '04', '11', 'San Juan de Chacña'),
('030412', 'PE', '03', '04', '12', 'Sañayca'),
('030413', 'PE', '03', '04', '13', 'Soraya'),
('030414', 'PE', '03', '04', '14', 'Tapairihua'),
('030415', 'PE', '03', '04', '15', 'Tintay'),
('030416', 'PE', '03', '04', '16', 'Toraya'),
('030417', 'PE', '03', '04', '17', 'Yanaca'),
('030500', 'PE', '03', '05', '00', 'Cotabambas'),
('030501', 'PE', '03', '05', '01', 'Tambobamba'),
('030502', 'PE', '03', '05', '02', 'Cotabambas'),
('030503', 'PE', '03', '05', '03', 'Coyllurqui'),
('030504', 'PE', '03', '05', '04', 'Haquira'),
('030505', 'PE', '03', '05', '05', 'Mara'),
('030506', 'PE', '03', '05', '06', 'Challhuahuacho'),
('030600', 'PE', '03', '06', '00', 'Chincheros'),
('030601', 'PE', '03', '06', '01', 'Chincheros'),
('030602', 'PE', '03', '06', '02', 'Anco_Huallo'),
('030603', 'PE', '03', '06', '03', 'Cocharcas'),
('030604', 'PE', '03', '06', '04', 'Huaccana'),
('030605', 'PE', '03', '06', '05', 'Ocobamba'),
('030606', 'PE', '03', '06', '06', 'Ongoy'),
('030607', 'PE', '03', '06', '07', 'Uranmarca'),
('030608', 'PE', '03', '06', '08', 'Ranracancha'),
('030700', 'PE', '03', '07', '00', 'Grau'),
('030701', 'PE', '03', '07', '01', 'Chuquibambilla'),
('030702', 'PE', '03', '07', '02', 'Curpahuasi'),
('030703', 'PE', '03', '07', '03', 'Gamarra'),
('030704', 'PE', '03', '07', '04', 'Huayllati'),
('030705', 'PE', '03', '07', '05', 'Mamara'),
('030706', 'PE', '03', '07', '06', 'Micaela Bastidas'),
('030707', 'PE', '03', '07', '07', 'Pataypampa'),
('030708', 'PE', '03', '07', '08', 'Progreso'),
('030709', 'PE', '03', '07', '09', 'San Antonio'),
('030710', 'PE', '03', '07', '10', 'Santa Rosa'),
('030711', 'PE', '03', '07', '11', 'Turpay'),
('030712', 'PE', '03', '07', '12', 'Vilcabamba'),
('030713', 'PE', '03', '07', '13', 'Virundo'),
('030714', 'PE', '03', '07', '14', 'Curasco'),
('040000', 'PE', '04', '00', '00', 'Arequipa'),
('040100', 'PE', '04', '01', '00', 'Arequipa'),
('040101', 'PE', '04', '01', '01', 'Arequipa'),
('040102', 'PE', '04', '01', '02', 'Alto Selva Alegre'),
('040103', 'PE', '04', '01', '03', 'Cayma'),
('040104', 'PE', '04', '01', '04', 'Cerro Colorado'),
('040105', 'PE', '04', '01', '05', 'Characato'),
('040106', 'PE', '04', '01', '06', 'Chiguata'),
('040107', 'PE', '04', '01', '07', 'Jacobo Hunter'),
('040108', 'PE', '04', '01', '08', 'La Joya'),
('040109', 'PE', '04', '01', '09', 'Mariano Melgar'),
('040110', 'PE', '04', '01', '10', 'Miraflores'),
('040111', 'PE', '04', '01', '11', 'Mollebaya'),
('040112', 'PE', '04', '01', '12', 'Paucarpata'),
('040113', 'PE', '04', '01', '13', 'Pocsi'),
('040114', 'PE', '04', '01', '14', 'Polobaya'),
('040115', 'PE', '04', '01', '15', 'Quequeña'),
('040116', 'PE', '04', '01', '16', 'Sabandia'),
('040117', 'PE', '04', '01', '17', 'Sachaca'),
('040118', 'PE', '04', '01', '18', 'San Juan de Siguas'),
('040119', 'PE', '04', '01', '19', 'San Juan de Tarucani'),
('040120', 'PE', '04', '01', '20', 'Santa Isabel de Siguas'),
('040121', 'PE', '04', '01', '21', 'Santa Rita de Siguas'),
('040122', 'PE', '04', '01', '22', 'Socabaya'),
('040123', 'PE', '04', '01', '23', 'Tiabaya'),
('040124', 'PE', '04', '01', '24', 'Uchumayo'),
('040125', 'PE', '04', '01', '25', 'Vitor'),
('040126', 'PE', '04', '01', '26', 'Yanahuara'),
('040127', 'PE', '04', '01', '27', 'Yarabamba'),
('040128', 'PE', '04', '01', '28', 'Yura'),
('040129', 'PE', '04', '01', '29', 'José Luis Bustamante Y Rivero'),
('040200', 'PE', '04', '02', '00', 'Camaná'),
('040201', 'PE', '04', '02', '01', 'Camaná'),
('040202', 'PE', '04', '02', '02', 'José María Quimper'),
('040203', 'PE', '04', '02', '03', 'Mariano Nicolás Valcárcel'),
('040204', 'PE', '04', '02', '04', 'Mariscal Cáceres'),
('040205', 'PE', '04', '02', '05', 'Nicolás de Pierola'),
('040206', 'PE', '04', '02', '06', 'Ocoña'),
('040207', 'PE', '04', '02', '07', 'Quilca'),
('040208', 'PE', '04', '02', '08', 'Samuel Pastor'),
('040300', 'PE', '04', '03', '00', 'Caravelí'),
('040301', 'PE', '04', '03', '01', 'Caravelí'),
('040302', 'PE', '04', '03', '02', 'Acarí'),
('040303', 'PE', '04', '03', '03', 'Atico'),
('040304', 'PE', '04', '03', '04', 'Atiquipa'),
('040305', 'PE', '04', '03', '05', 'Bella Unión'),
('040306', 'PE', '04', '03', '06', 'Cahuacho'),
('040307', 'PE', '04', '03', '07', 'Chala'),
('040308', 'PE', '04', '03', '08', 'Chaparra'),
('040309', 'PE', '04', '03', '09', 'Huanuhuanu'),
('040310', 'PE', '04', '03', '10', 'Jaqui'),
('040311', 'PE', '04', '03', '11', 'Lomas'),
('040312', 'PE', '04', '03', '12', 'Quicacha'),
('040313', 'PE', '04', '03', '13', 'Yauca'),
('040400', 'PE', '04', '04', '00', 'Castilla'),
('040401', 'PE', '04', '04', '01', 'Aplao'),
('040402', 'PE', '04', '04', '02', 'Andagua'),
('040403', 'PE', '04', '04', '03', 'Ayo'),
('040404', 'PE', '04', '04', '04', 'Chachas'),
('040405', 'PE', '04', '04', '05', 'Chilcaymarca'),
('040406', 'PE', '04', '04', '06', 'Choco'),
('040407', 'PE', '04', '04', '07', 'Huancarqui'),
('040408', 'PE', '04', '04', '08', 'Machaguay'),
('040409', 'PE', '04', '04', '09', 'Orcopampa'),
('040410', 'PE', '04', '04', '10', 'Pampacolca'),
('040411', 'PE', '04', '04', '11', 'Tipan'),
('040412', 'PE', '04', '04', '12', 'Uñon'),
('040413', 'PE', '04', '04', '13', 'Uraca'),
('040414', 'PE', '04', '04', '14', 'Viraco'),
('040500', 'PE', '04', '05', '00', 'Caylloma'),
('040501', 'PE', '04', '05', '01', 'Chivay'),
('040502', 'PE', '04', '05', '02', 'Achoma'),
('040503', 'PE', '04', '05', '03', 'Cabanaconde'),
('040504', 'PE', '04', '05', '04', 'Callalli'),
('040505', 'PE', '04', '05', '05', 'Caylloma'),
('040506', 'PE', '04', '05', '06', 'Coporaque'),
('040507', 'PE', '04', '05', '07', 'Huambo'),
('040508', 'PE', '04', '05', '08', 'Huanca'),
('040509', 'PE', '04', '05', '09', 'Ichupampa'),
('040510', 'PE', '04', '05', '10', 'Lari'),
('040511', 'PE', '04', '05', '11', 'Lluta'),
('040512', 'PE', '04', '05', '12', 'Maca'),
('040513', 'PE', '04', '05', '13', 'Madrigal'),
('040514', 'PE', '04', '05', '14', 'San Antonio de Chuca'),
('040515', 'PE', '04', '05', '15', 'Sibayo'),
('040516', 'PE', '04', '05', '16', 'Tapay'),
('040517', 'PE', '04', '05', '17', 'Tisco'),
('040518', 'PE', '04', '05', '18', 'Tuti'),
('040519', 'PE', '04', '05', '19', 'Yanque'),
('040520', 'PE', '04', '05', '20', 'Majes'),
('040600', 'PE', '04', '06', '00', 'Condesuyos'),
('040601', 'PE', '04', '06', '01', 'Chuquibamba'),
('040602', 'PE', '04', '06', '02', 'Andaray'),
('040603', 'PE', '04', '06', '03', 'Cayarani'),
('040604', 'PE', '04', '06', '04', 'Chichas'),
('040605', 'PE', '04', '06', '05', 'Iray'),
('040606', 'PE', '04', '06', '06', 'Río Grande'),
('040607', 'PE', '04', '06', '07', 'Salamanca'),
('040608', 'PE', '04', '06', '08', 'Yanaquihua'),
('040700', 'PE', '04', '07', '00', 'Islay'),
('040701', 'PE', '04', '07', '01', 'Mollendo'),
('040702', 'PE', '04', '07', '02', 'Cocachacra'),
('040703', 'PE', '04', '07', '03', 'Dean Valdivia'),
('040704', 'PE', '04', '07', '04', 'Islay'),
('040705', 'PE', '04', '07', '05', 'Mejia'),
('040706', 'PE', '04', '07', '06', 'Punta de Bombón'),
('040800', 'PE', '04', '08', '00', 'La Uniòn'),
('040801', 'PE', '04', '08', '01', 'Cotahuasi'),
('040802', 'PE', '04', '08', '02', 'Alca'),
('040803', 'PE', '04', '08', '03', 'Charcana'),
('040804', 'PE', '04', '08', '04', 'Huaynacotas'),
('040805', 'PE', '04', '08', '05', 'Pampamarca'),
('040806', 'PE', '04', '08', '06', 'Puyca'),
('040807', 'PE', '04', '08', '07', 'Quechualla'),
('040808', 'PE', '04', '08', '08', 'Sayla'),
('040809', 'PE', '04', '08', '09', 'Tauria'),
('040810', 'PE', '04', '08', '10', 'Tomepampa'),
('040811', 'PE', '04', '08', '11', 'Toro'),
('050000', 'PE', '05', '00', '00', 'Ayacucho'),
('050100', 'PE', '05', '01', '00', 'Huamanga'),
('050101', 'PE', '05', '01', '01', 'Ayacucho'),
('050102', 'PE', '05', '01', '02', 'Acocro'),
('050103', 'PE', '05', '01', '03', 'Acos Vinchos'),
('050104', 'PE', '05', '01', '04', 'Carmen Alto'),
('050105', 'PE', '05', '01', '05', 'Chiara'),
('050106', 'PE', '05', '01', '06', 'Ocros'),
('050107', 'PE', '05', '01', '07', 'Pacaycasa'),
('050108', 'PE', '05', '01', '08', 'Quinua'),
('050109', 'PE', '05', '01', '09', 'San José de Ticllas'),
('050110', 'PE', '05', '01', '10', 'San Juan Bautista'),
('050111', 'PE', '05', '01', '11', 'Santiago de Pischa'),
('050112', 'PE', '05', '01', '12', 'Socos'),
('050113', 'PE', '05', '01', '13', 'Tambillo'),
('050114', 'PE', '05', '01', '14', 'Vinchos'),
('050115', 'PE', '05', '01', '15', 'Jesús Nazareno'),
('050116', 'PE', '05', '01', '16', 'Andrés Avelino Cáceres Dorregaray'),
('050200', 'PE', '05', '02', '00', 'Cangallo'),
('050201', 'PE', '05', '02', '01', 'Cangallo'),
('050202', 'PE', '05', '02', '02', 'Chuschi'),
('050203', 'PE', '05', '02', '03', 'Los Morochucos'),
('050204', 'PE', '05', '02', '04', 'María Parado de Bellido'),
('050205', 'PE', '05', '02', '05', 'Paras'),
('050206', 'PE', '05', '02', '06', 'Totos'),
('050300', 'PE', '05', '03', '00', 'Huanca Sancos'),
('050301', 'PE', '05', '03', '01', 'Sancos'),
('050302', 'PE', '05', '03', '02', 'Carapo'),
('050303', 'PE', '05', '03', '03', 'Sacsamarca'),
('050304', 'PE', '05', '03', '04', 'Santiago de Lucanamarca'),
('050400', 'PE', '05', '04', '00', 'Huanta'),
('050401', 'PE', '05', '04', '01', 'Huanta'),
('050402', 'PE', '05', '04', '02', 'Ayahuanco'),
('050403', 'PE', '05', '04', '03', 'Huamanguilla'),
('050404', 'PE', '05', '04', '04', 'Iguain'),
('050405', 'PE', '05', '04', '05', 'Luricocha'),
('050406', 'PE', '05', '04', '06', 'Santillana'),
('050407', 'PE', '05', '04', '07', 'Sivia'),
('050408', 'PE', '05', '04', '08', 'Llochegua'),
('050409', 'PE', '05', '04', '09', 'Canayre'),
('050410', 'PE', '05', '04', '10', 'Uchuraccay'),
('050411', 'PE', '05', '04', '11', 'Pucacolpa'),
('050500', 'PE', '05', '05', '00', 'La Mar'),
('050501', 'PE', '05', '05', '01', 'San Miguel'),
('050502', 'PE', '05', '05', '02', 'Anco'),
('050503', 'PE', '05', '05', '03', 'Ayna'),
('050504', 'PE', '05', '05', '04', 'Chilcas'),
('050505', 'PE', '05', '05', '05', 'Chungui'),
('050506', 'PE', '05', '05', '06', 'Luis Carranza'),
('050507', 'PE', '05', '05', '07', 'Santa Rosa'),
('050508', 'PE', '05', '05', '08', 'Tambo'),
('050509', 'PE', '05', '05', '09', 'Samugari'),
('050510', 'PE', '05', '05', '10', 'Anchihuay'),
('050600', 'PE', '05', '06', '00', 'Lucanas'),
('050601', 'PE', '05', '06', '01', 'Puquio'),
('050602', 'PE', '05', '06', '02', 'Aucara'),
('050603', 'PE', '05', '06', '03', 'Cabana'),
('050604', 'PE', '05', '06', '04', 'Carmen Salcedo'),
('050605', 'PE', '05', '06', '05', 'Chaviña'),
('050606', 'PE', '05', '06', '06', 'Chipao'),
('050607', 'PE', '05', '06', '07', 'Huac-Huas'),
('050608', 'PE', '05', '06', '08', 'Laramate'),
('050609', 'PE', '05', '06', '09', 'Leoncio Prado'),
('050610', 'PE', '05', '06', '10', 'Llauta'),
('050611', 'PE', '05', '06', '11', 'Lucanas'),
('050612', 'PE', '05', '06', '12', 'Ocaña'),
('050613', 'PE', '05', '06', '13', 'Otoca'),
('050614', 'PE', '05', '06', '14', 'Saisa'),
('050615', 'PE', '05', '06', '15', 'San Cristóbal'),
('050616', 'PE', '05', '06', '16', 'San Juan'),
('050617', 'PE', '05', '06', '17', 'San Pedro'),
('050618', 'PE', '05', '06', '18', 'San Pedro de Palco'),
('050619', 'PE', '05', '06', '19', 'Sancos'),
('050620', 'PE', '05', '06', '20', 'Santa Ana de Huaycahuacho'),
('050621', 'PE', '05', '06', '21', 'Santa Lucia'),
('050700', 'PE', '05', '07', '00', 'Parinacochas'),
('050701', 'PE', '05', '07', '01', 'Coracora'),
('050702', 'PE', '05', '07', '02', 'Chumpi'),
('050703', 'PE', '05', '07', '03', 'Coronel Castañeda'),
('050704', 'PE', '05', '07', '04', 'Pacapausa'),
('050705', 'PE', '05', '07', '05', 'Pullo'),
('050706', 'PE', '05', '07', '06', 'Puyusca'),
('050707', 'PE', '05', '07', '07', 'San Francisco de Ravacayco'),
('050708', 'PE', '05', '07', '08', 'Upahuacho'),
('050800', 'PE', '05', '08', '00', 'Pàucar del Sara Sara'),
('050801', 'PE', '05', '08', '01', 'Pausa'),
('050802', 'PE', '05', '08', '02', 'Colta'),
('050803', 'PE', '05', '08', '03', 'Corculla'),
('050804', 'PE', '05', '08', '04', 'Lampa'),
('050805', 'PE', '05', '08', '05', 'Marcabamba'),
('050806', 'PE', '05', '08', '06', 'Oyolo'),
('050807', 'PE', '05', '08', '07', 'Pararca'),
('050808', 'PE', '05', '08', '08', 'San Javier de Alpabamba'),
('050809', 'PE', '05', '08', '09', 'San José de Ushua'),
('050810', 'PE', '05', '08', '10', 'Sara Sara'),
('050900', 'PE', '05', '09', '00', 'Sucre'),
('050901', 'PE', '05', '09', '01', 'Querobamba'),
('050902', 'PE', '05', '09', '02', 'Belén'),
('050903', 'PE', '05', '09', '03', 'Chalcos'),
('050904', 'PE', '05', '09', '04', 'Chilcayoc'),
('050905', 'PE', '05', '09', '05', 'Huacaña'),
('050906', 'PE', '05', '09', '06', 'Morcolla'),
('050907', 'PE', '05', '09', '07', 'Paico'),
('050908', 'PE', '05', '09', '08', 'San Pedro de Larcay'),
('050909', 'PE', '05', '09', '09', 'San Salvador de Quije'),
('050910', 'PE', '05', '09', '10', 'Santiago de Paucaray'),
('050911', 'PE', '05', '09', '11', 'Soras'),
('051000', 'PE', '05', '10', '00', 'Víctor Fajardo'),
('051001', 'PE', '05', '10', '01', 'Huancapi'),
('051002', 'PE', '05', '10', '02', 'Alcamenca'),
('051003', 'PE', '05', '10', '03', 'Apongo'),
('051004', 'PE', '05', '10', '04', 'Asquipata'),
('051005', 'PE', '05', '10', '05', 'Canaria'),
('051006', 'PE', '05', '10', '06', 'Cayara'),
('051007', 'PE', '05', '10', '07', 'Colca'),
('051008', 'PE', '05', '10', '08', 'Huamanquiquia'),
('051009', 'PE', '05', '10', '09', 'Huancaraylla'),
('051010', 'PE', '05', '10', '10', 'Huaya'),
('051011', 'PE', '05', '10', '11', 'Sarhua'),
('051012', 'PE', '05', '10', '12', 'Vilcanchos'),
('051100', 'PE', '05', '11', '00', 'Vilcas Huamán'),
('051101', 'PE', '05', '11', '01', 'Vilcas Huaman'),
('051102', 'PE', '05', '11', '02', 'Accomarca'),
('051103', 'PE', '05', '11', '03', 'Carhuanca'),
('051104', 'PE', '05', '11', '04', 'Concepción'),
('051105', 'PE', '05', '11', '05', 'Huambalpa'),
('051106', 'PE', '05', '11', '06', 'Independencia'),
('051107', 'PE', '05', '11', '07', 'Saurama'),
('051108', 'PE', '05', '11', '08', 'Vischongo'),
('060000', 'PE', '06', '00', '00', 'Cajamarca'),
('060100', 'PE', '06', '01', '00', 'Cajamarca'),
('060101', 'PE', '06', '01', '01', 'Cajamarca'),
('060102', 'PE', '06', '01', '02', 'Asunción'),
('060103', 'PE', '06', '01', '03', 'Chetilla'),
('060104', 'PE', '06', '01', '04', 'Cospan'),
('060105', 'PE', '06', '01', '05', 'Encañada'),
('060106', 'PE', '06', '01', '06', 'Jesús'),
('060107', 'PE', '06', '01', '07', 'Llacanora'),
('060108', 'PE', '06', '01', '08', 'Los Baños del Inca'),
('060109', 'PE', '06', '01', '09', 'Magdalena'),
('060110', 'PE', '06', '01', '10', 'Matara'),
('060111', 'PE', '06', '01', '11', 'Namora'),
('060112', 'PE', '06', '01', '12', 'San Juan'),
('060200', 'PE', '06', '02', '00', 'Cajabamba'),
('060201', 'PE', '06', '02', '01', 'Cajabamba'),
('060202', 'PE', '06', '02', '02', 'Cachachi'),
('060203', 'PE', '06', '02', '03', 'Condebamba'),
('060204', 'PE', '06', '02', '04', 'Sitacocha'),
('060300', 'PE', '06', '03', '00', 'Celendín'),
('060301', 'PE', '06', '03', '01', 'Celendín'),
('060302', 'PE', '06', '03', '02', 'Chumuch'),
('060303', 'PE', '06', '03', '03', 'Cortegana'),
('060304', 'PE', '06', '03', '04', 'Huasmin'),
('060305', 'PE', '06', '03', '05', 'Jorge Chávez'),
('060306', 'PE', '06', '03', '06', 'José Gálvez'),
('060307', 'PE', '06', '03', '07', 'Miguel Iglesias'),
('060308', 'PE', '06', '03', '08', 'Oxamarca'),
('060309', 'PE', '06', '03', '09', 'Sorochuco'),
('060310', 'PE', '06', '03', '10', 'Sucre'),
('060311', 'PE', '06', '03', '11', 'Utco'),
('060312', 'PE', '06', '03', '12', 'La Libertad de Pallan'),
('060400', 'PE', '06', '04', '00', 'Chota'),
('060401', 'PE', '06', '04', '01', 'Chota'),
('060402', 'PE', '06', '04', '02', 'Anguia'),
('060403', 'PE', '06', '04', '03', 'Chadin'),
('060404', 'PE', '06', '04', '04', 'Chiguirip'),
('060405', 'PE', '06', '04', '05', 'Chimban'),
('060406', 'PE', '06', '04', '06', 'Choropampa'),
('060407', 'PE', '06', '04', '07', 'Cochabamba'),
('060408', 'PE', '06', '04', '08', 'Conchan'),
('060409', 'PE', '06', '04', '09', 'Huambos'),
('060410', 'PE', '06', '04', '10', 'Lajas'),
('060411', 'PE', '06', '04', '11', 'Llama'),
('060412', 'PE', '06', '04', '12', 'Miracosta'),
('060413', 'PE', '06', '04', '13', 'Paccha'),
('060414', 'PE', '06', '04', '14', 'Pion'),
('060415', 'PE', '06', '04', '15', 'Querocoto'),
('060416', 'PE', '06', '04', '16', 'San Juan de Licupis'),
('060417', 'PE', '06', '04', '17', 'Tacabamba'),
('060418', 'PE', '06', '04', '18', 'Tocmoche'),
('060419', 'PE', '06', '04', '19', 'Chalamarca'),
('060500', 'PE', '06', '05', '00', 'Contumazá'),
('060501', 'PE', '06', '05', '01', 'Contumaza'),
('060502', 'PE', '06', '05', '02', 'Chilete'),
('060503', 'PE', '06', '05', '03', 'Cupisnique'),
('060504', 'PE', '06', '05', '04', 'Guzmango'),
('060505', 'PE', '06', '05', '05', 'San Benito'),
('060506', 'PE', '06', '05', '06', 'Santa Cruz de Toledo'),
('060507', 'PE', '06', '05', '07', 'Tantarica'),
('060508', 'PE', '06', '05', '08', 'Yonan'),
('060600', 'PE', '06', '06', '00', 'Cutervo'),
('060601', 'PE', '06', '06', '01', 'Cutervo'),
('060602', 'PE', '06', '06', '02', 'Callayuc'),
('060603', 'PE', '06', '06', '03', 'Choros'),
('060604', 'PE', '06', '06', '04', 'Cujillo'),
('060605', 'PE', '06', '06', '05', 'La Ramada'),
('060606', 'PE', '06', '06', '06', 'Pimpingos'),
('060607', 'PE', '06', '06', '07', 'Querocotillo'),
('060608', 'PE', '06', '06', '08', 'San Andrés de Cutervo'),
('060609', 'PE', '06', '06', '09', 'San Juan de Cutervo'),
('060610', 'PE', '06', '06', '10', 'San Luis de Lucma'),
('060611', 'PE', '06', '06', '11', 'Santa Cruz'),
('060612', 'PE', '06', '06', '12', 'Santo Domingo de la Capilla'),
('060613', 'PE', '06', '06', '13', 'Santo Tomas'),
('060614', 'PE', '06', '06', '14', 'Socota'),
('060615', 'PE', '06', '06', '15', 'Toribio Casanova'),
('060700', 'PE', '06', '07', '00', 'Hualgayoc'),
('060701', 'PE', '06', '07', '01', 'Bambamarca'),
('060702', 'PE', '06', '07', '02', 'Chugur'),
('060703', 'PE', '06', '07', '03', 'Hualgayoc'),
('060800', 'PE', '06', '08', '00', 'Jaén'),
('060801', 'PE', '06', '08', '01', 'Jaén'),
('060802', 'PE', '06', '08', '02', 'Bellavista'),
('060803', 'PE', '06', '08', '03', 'Chontali'),
('060804', 'PE', '06', '08', '04', 'Colasay'),
('060805', 'PE', '06', '08', '05', 'Huabal'),
('060806', 'PE', '06', '08', '06', 'Las Pirias'),
('060807', 'PE', '06', '08', '07', 'Pomahuaca'),
('060808', 'PE', '06', '08', '08', 'Pucara'),
('060809', 'PE', '06', '08', '09', 'Sallique'),
('060810', 'PE', '06', '08', '10', 'San Felipe'),
('060811', 'PE', '06', '08', '11', 'San José del Alto'),
('060812', 'PE', '06', '08', '12', 'Santa Rosa'),
('060900', 'PE', '06', '09', '00', 'San Ignacio'),
('060901', 'PE', '06', '09', '01', 'San Ignacio'),
('060902', 'PE', '06', '09', '02', 'Chirinos'),
('060903', 'PE', '06', '09', '03', 'Huarango'),
('060904', 'PE', '06', '09', '04', 'La Coipa'),
('060905', 'PE', '06', '09', '05', 'Namballe'),
('060906', 'PE', '06', '09', '06', 'San José de Lourdes'),
('060907', 'PE', '06', '09', '07', 'Tabaconas'),
('061000', 'PE', '06', '10', '00', 'San Marcos'),
('061001', 'PE', '06', '10', '01', 'Pedro Gálvez'),
('061002', 'PE', '06', '10', '02', 'Chancay'),
('061003', 'PE', '06', '10', '03', 'Eduardo Villanueva'),
('061004', 'PE', '06', '10', '04', 'Gregorio Pita'),
('061005', 'PE', '06', '10', '05', 'Ichocan'),
('061006', 'PE', '06', '10', '06', 'José Manuel Quiroz'),
('061007', 'PE', '06', '10', '07', 'José Sabogal'),
('061100', 'PE', '06', '11', '00', 'San Miguel'),
('061101', 'PE', '06', '11', '01', 'San Miguel'),
('061102', 'PE', '06', '11', '02', 'Bolívar'),
('061103', 'PE', '06', '11', '03', 'Calquis'),
('061104', 'PE', '06', '11', '04', 'Catilluc'),
('061105', 'PE', '06', '11', '05', 'El Prado'),
('061106', 'PE', '06', '11', '06', 'La Florida'),
('061107', 'PE', '06', '11', '07', 'Llapa'),
('061108', 'PE', '06', '11', '08', 'Nanchoc'),
('061109', 'PE', '06', '11', '09', 'Niepos'),
('061110', 'PE', '06', '11', '10', 'San Gregorio'),
('061111', 'PE', '06', '11', '11', 'San Silvestre de Cochan'),
('061112', 'PE', '06', '11', '12', 'Tongod'),
('061113', 'PE', '06', '11', '13', 'Unión Agua Blanca'),
('061200', 'PE', '06', '12', '00', 'San Pablo'),
('061201', 'PE', '06', '12', '01', 'San Pablo'),
('061202', 'PE', '06', '12', '02', 'San Bernardino'),
('061203', 'PE', '06', '12', '03', 'San Luis'),
('061204', 'PE', '06', '12', '04', 'Tumbaden'),
('061300', 'PE', '06', '13', '00', 'Santa Cruz'),
('061301', 'PE', '06', '13', '01', 'Santa Cruz'),
('061302', 'PE', '06', '13', '02', 'Andabamba'),
('061303', 'PE', '06', '13', '03', 'Catache'),
('061304', 'PE', '06', '13', '04', 'Chancaybaños'),
('061305', 'PE', '06', '13', '05', 'La Esperanza'),
('061306', 'PE', '06', '13', '06', 'Ninabamba'),
('061307', 'PE', '06', '13', '07', 'Pulan'),
('061308', 'PE', '06', '13', '08', 'Saucepampa'),
('061309', 'PE', '06', '13', '09', 'Sexi'),
('061310', 'PE', '06', '13', '10', 'Uticyacu'),
('061311', 'PE', '06', '13', '11', 'Yauyucan'),
('070000', 'PE', '07', '00', '00', 'Callao'),
('070100', 'PE', '07', '01', '00', 'Prov. Const. del Callao'),
('070101', 'PE', '07', '01', '01', 'Callao'),
('070102', 'PE', '07', '01', '02', 'Bellavista'),
('070103', 'PE', '07', '01', '03', 'Carmen de la Legua Reynoso'),
('070104', 'PE', '07', '01', '04', 'La Perla'),
('070105', 'PE', '07', '01', '05', 'La Punta'),
('070106', 'PE', '07', '01', '06', 'Ventanilla'),
('070107', 'PE', '07', '01', '07', 'Mi Perú'),
('080000', 'PE', '08', '00', '00', 'Cusco'),
('080100', 'PE', '08', '01', '00', 'Cusco'),
('080101', 'PE', '08', '01', '01', 'Cusco'),
('080102', 'PE', '08', '01', '02', 'Ccorca'),
('080103', 'PE', '08', '01', '03', 'Poroy'),
('080104', 'PE', '08', '01', '04', 'San Jerónimo'),
('080105', 'PE', '08', '01', '05', 'San Sebastian'),
('080106', 'PE', '08', '01', '06', 'Santiago'),
('080107', 'PE', '08', '01', '07', 'Saylla'),
('080108', 'PE', '08', '01', '08', 'Wanchaq'),
('080200', 'PE', '08', '02', '00', 'Acomayo'),
('080201', 'PE', '08', '02', '01', 'Acomayo'),
('080202', 'PE', '08', '02', '02', 'Acopia'),
('080203', 'PE', '08', '02', '03', 'Acos'),
('080204', 'PE', '08', '02', '04', 'Mosoc Llacta'),
('080205', 'PE', '08', '02', '05', 'Pomacanchi'),
('080206', 'PE', '08', '02', '06', 'Rondocan'),
('080207', 'PE', '08', '02', '07', 'Sangarara'),
('080300', 'PE', '08', '03', '00', 'Anta'),
('080301', 'PE', '08', '03', '01', 'Anta'),
('080302', 'PE', '08', '03', '02', 'Ancahuasi'),
('080303', 'PE', '08', '03', '03', 'Cachimayo'),
('080304', 'PE', '08', '03', '04', 'Chinchaypujio'),
('080305', 'PE', '08', '03', '05', 'Huarocondo'),
('080306', 'PE', '08', '03', '06', 'Limatambo'),
('080307', 'PE', '08', '03', '07', 'Mollepata'),
('080308', 'PE', '08', '03', '08', 'Pucyura'),
('080309', 'PE', '08', '03', '09', 'Zurite'),
('080400', 'PE', '08', '04', '00', 'Calca'),
('080401', 'PE', '08', '04', '01', 'Calca'),
('080402', 'PE', '08', '04', '02', 'Coya'),
('080403', 'PE', '08', '04', '03', 'Lamay'),
('080404', 'PE', '08', '04', '04', 'Lares'),
('080405', 'PE', '08', '04', '05', 'Pisac'),
('080406', 'PE', '08', '04', '06', 'San Salvador'),
('080407', 'PE', '08', '04', '07', 'Taray'),
('080408', 'PE', '08', '04', '08', 'Yanatile'),
('080500', 'PE', '08', '05', '00', 'Canas'),
('080501', 'PE', '08', '05', '01', 'Yanaoca'),
('080502', 'PE', '08', '05', '02', 'Checca'),
('080503', 'PE', '08', '05', '03', 'Kunturkanki'),
('080504', 'PE', '08', '05', '04', 'Langui'),
('080505', 'PE', '08', '05', '05', 'Layo'),
('080506', 'PE', '08', '05', '06', 'Pampamarca'),
('080507', 'PE', '08', '05', '07', 'Quehue'),
('080508', 'PE', '08', '05', '08', 'Tupac Amaru'),
('080600', 'PE', '08', '06', '00', 'Canchis'),
('080601', 'PE', '08', '06', '01', 'Sicuani'),
('080602', 'PE', '08', '06', '02', 'Checacupe'),
('080603', 'PE', '08', '06', '03', 'Combapata'),
('080604', 'PE', '08', '06', '04', 'Marangani'),
('080605', 'PE', '08', '06', '05', 'Pitumarca'),
('080606', 'PE', '08', '06', '06', 'San Pablo'),
('080607', 'PE', '08', '06', '07', 'San Pedro'),
('080608', 'PE', '08', '06', '08', 'Tinta'),
('080700', 'PE', '08', '07', '00', 'Chumbivilcas'),
('080701', 'PE', '08', '07', '01', 'Santo Tomas'),
('080702', 'PE', '08', '07', '02', 'Capacmarca'),
('080703', 'PE', '08', '07', '03', 'Chamaca'),
('080704', 'PE', '08', '07', '04', 'Colquemarca'),
('080705', 'PE', '08', '07', '05', 'Livitaca'),
('080706', 'PE', '08', '07', '06', 'Llusco'),
('080707', 'PE', '08', '07', '07', 'Quiñota'),
('080708', 'PE', '08', '07', '08', 'Velille'),
('080800', 'PE', '08', '08', '00', 'Espinar'),
('080801', 'PE', '08', '08', '01', 'Espinar'),
('080802', 'PE', '08', '08', '02', 'Condoroma'),
('080803', 'PE', '08', '08', '03', 'Coporaque'),
('080804', 'PE', '08', '08', '04', 'Ocoruro'),
('080805', 'PE', '08', '08', '05', 'Pallpata'),
('080806', 'PE', '08', '08', '06', 'Pichigua'),
('080807', 'PE', '08', '08', '07', 'Suyckutambo'),
('080808', 'PE', '08', '08', '08', 'Alto Pichigua'),
('080900', 'PE', '08', '09', '00', 'La Convención'),
('080901', 'PE', '08', '09', '01', 'Santa Ana'),
('080902', 'PE', '08', '09', '02', 'Echarate'),
('080903', 'PE', '08', '09', '03', 'Huayopata'),
('080904', 'PE', '08', '09', '04', 'Maranura'),
('080905', 'PE', '08', '09', '05', 'Ocobamba'),
('080906', 'PE', '08', '09', '06', 'Quellouno'),
('080907', 'PE', '08', '09', '07', 'Kimbiri'),
('080908', 'PE', '08', '09', '08', 'Santa Teresa'),
('080909', 'PE', '08', '09', '09', 'Vilcabamba'),
('080910', 'PE', '08', '09', '10', 'Pichari'),
('080911', 'PE', '08', '09', '11', 'Inkawasi'),
('080912', 'PE', '08', '09', '12', 'Villa Virgen'),
('080913', 'PE', '08', '09', '13', 'Villa Kintiarina'),
('081000', 'PE', '08', '10', '00', 'Paruro'),
('081001', 'PE', '08', '10', '01', 'Paruro'),
('081002', 'PE', '08', '10', '02', 'Accha'),
('081003', 'PE', '08', '10', '03', 'Ccapi'),
('081004', 'PE', '08', '10', '04', 'Colcha'),
('081005', 'PE', '08', '10', '05', 'Huanoquite'),
('081006', 'PE', '08', '10', '06', 'Omacha'),
('081007', 'PE', '08', '10', '07', 'Paccaritambo'),
('081008', 'PE', '08', '10', '08', 'Pillpinto'),
('081009', 'PE', '08', '10', '09', 'Yaurisque'),
('081100', 'PE', '08', '11', '00', 'Paucartambo'),
('081101', 'PE', '08', '11', '01', 'Paucartambo'),
('081102', 'PE', '08', '11', '02', 'Caicay'),
('081103', 'PE', '08', '11', '03', 'Challabamba'),
('081104', 'PE', '08', '11', '04', 'Colquepata'),
('081105', 'PE', '08', '11', '05', 'Huancarani'),
('081106', 'PE', '08', '11', '06', 'Kosñipata'),
('081200', 'PE', '08', '12', '00', 'Quispicanchi'),
('081201', 'PE', '08', '12', '01', 'Urcos'),
('081202', 'PE', '08', '12', '02', 'Andahuaylillas'),
('081203', 'PE', '08', '12', '03', 'Camanti'),
('081204', 'PE', '08', '12', '04', 'Ccarhuayo'),
('081205', 'PE', '08', '12', '05', 'Ccatca'),
('081206', 'PE', '08', '12', '06', 'Cusipata'),
('081207', 'PE', '08', '12', '07', 'Huaro'),
('081208', 'PE', '08', '12', '08', 'Lucre'),
('081209', 'PE', '08', '12', '09', 'Marcapata'),
('081210', 'PE', '08', '12', '10', 'Ocongate'),
('081211', 'PE', '08', '12', '11', 'Oropesa'),
('081212', 'PE', '08', '12', '12', 'Quiquijana'),
('081300', 'PE', '08', '13', '00', 'Urubamba'),
('081301', 'PE', '08', '13', '01', 'Urubamba'),
('081302', 'PE', '08', '13', '02', 'Chinchero'),
('081303', 'PE', '08', '13', '03', 'Huayllabamba'),
('081304', 'PE', '08', '13', '04', 'Machupicchu'),
('081305', 'PE', '08', '13', '05', 'Maras'),
('081306', 'PE', '08', '13', '06', 'Ollantaytambo'),
('081307', 'PE', '08', '13', '07', 'Yucay'),
('090000', 'PE', '09', '00', '00', 'Huancavelica'),
('090100', 'PE', '09', '01', '00', 'Huancavelica'),
('090101', 'PE', '09', '01', '01', 'Huancavelica'),
('090102', 'PE', '09', '01', '02', 'Acobambilla'),
('090103', 'PE', '09', '01', '03', 'Acoria'),
('090104', 'PE', '09', '01', '04', 'Conayca'),
('090105', 'PE', '09', '01', '05', 'Cuenca'),
('090106', 'PE', '09', '01', '06', 'Huachocolpa'),
('090107', 'PE', '09', '01', '07', 'Huayllahuara'),
('090108', 'PE', '09', '01', '08', 'Izcuchaca'),
('090109', 'PE', '09', '01', '09', 'Laria'),
('090110', 'PE', '09', '01', '10', 'Manta'),
('090111', 'PE', '09', '01', '11', 'Mariscal Cáceres'),
('090112', 'PE', '09', '01', '12', 'Moya'),
('090113', 'PE', '09', '01', '13', 'Nuevo Occoro'),
('090114', 'PE', '09', '01', '14', 'Palca'),
('090115', 'PE', '09', '01', '15', 'Pilchaca'),
('090116', 'PE', '09', '01', '16', 'Vilca'),
('090117', 'PE', '09', '01', '17', 'Yauli'),
('090118', 'PE', '09', '01', '18', 'Ascensión'),
('090119', 'PE', '09', '01', '19', 'Huando'),
('090200', 'PE', '09', '02', '00', 'Acobamba'),
('090201', 'PE', '09', '02', '01', 'Acobamba'),
('090202', 'PE', '09', '02', '02', 'Andabamba'),
('090203', 'PE', '09', '02', '03', 'Anta'),
('090204', 'PE', '09', '02', '04', 'Caja'),
('090205', 'PE', '09', '02', '05', 'Marcas'),
('090206', 'PE', '09', '02', '06', 'Paucara'),
('090207', 'PE', '09', '02', '07', 'Pomacocha'),
('090208', 'PE', '09', '02', '08', 'Rosario'),
('090300', 'PE', '09', '03', '00', 'Angaraes'),
('090301', 'PE', '09', '03', '01', 'Lircay'),
('090302', 'PE', '09', '03', '02', 'Anchonga'),
('090303', 'PE', '09', '03', '03', 'Callanmarca'),
('090304', 'PE', '09', '03', '04', 'Ccochaccasa'),
('090305', 'PE', '09', '03', '05', 'Chincho'),
('090306', 'PE', '09', '03', '06', 'Congalla'),
('090307', 'PE', '09', '03', '07', 'Huanca-Huanca'),
('090308', 'PE', '09', '03', '08', 'Huayllay Grande'),
('090309', 'PE', '09', '03', '09', 'Julcamarca'),
('090310', 'PE', '09', '03', '10', 'San Antonio de Antaparco'),
('090311', 'PE', '09', '03', '11', 'Santo Tomas de Pata'),
('090312', 'PE', '09', '03', '12', 'Secclla'),
('090400', 'PE', '09', '04', '00', 'Castrovirreyna'),
('090401', 'PE', '09', '04', '01', 'Castrovirreyna'),
('090402', 'PE', '09', '04', '02', 'Arma'),
('090403', 'PE', '09', '04', '03', 'Aurahua'),
('090404', 'PE', '09', '04', '04', 'Capillas'),
('090405', 'PE', '09', '04', '05', 'Chupamarca'),
('090406', 'PE', '09', '04', '06', 'Cocas'),
('090407', 'PE', '09', '04', '07', 'Huachos'),
('090408', 'PE', '09', '04', '08', 'Huamatambo'),
('090409', 'PE', '09', '04', '09', 'Mollepampa'),
('090410', 'PE', '09', '04', '10', 'San Juan'),
('090411', 'PE', '09', '04', '11', 'Santa Ana'),
('090412', 'PE', '09', '04', '12', 'Tantara'),
('090413', 'PE', '09', '04', '13', 'Ticrapo'),
('090500', 'PE', '09', '05', '00', 'Churcampa'),
('090501', 'PE', '09', '05', '01', 'Churcampa'),
('090502', 'PE', '09', '05', '02', 'Anco'),
('090503', 'PE', '09', '05', '03', 'Chinchihuasi'),
('090504', 'PE', '09', '05', '04', 'El Carmen'),
('090505', 'PE', '09', '05', '05', 'La Merced'),
('090506', 'PE', '09', '05', '06', 'Locroja'),
('090507', 'PE', '09', '05', '07', 'Paucarbamba'),
('090508', 'PE', '09', '05', '08', 'San Miguel de Mayocc'),
('090509', 'PE', '09', '05', '09', 'San Pedro de Coris'),
('090510', 'PE', '09', '05', '10', 'Pachamarca'),
('090511', 'PE', '09', '05', '11', 'Cosme'),
('090600', 'PE', '09', '06', '00', 'Huaytará'),
('090601', 'PE', '09', '06', '01', 'Huaytara'),
('090602', 'PE', '09', '06', '02', 'Ayavi'),
('090603', 'PE', '09', '06', '03', 'Córdova'),
('090604', 'PE', '09', '06', '04', 'Huayacundo Arma'),
('090605', 'PE', '09', '06', '05', 'Laramarca'),
('090606', 'PE', '09', '06', '06', 'Ocoyo'),
('090607', 'PE', '09', '06', '07', 'Pilpichaca'),
('090608', 'PE', '09', '06', '08', 'Querco'),
('090609', 'PE', '09', '06', '09', 'Quito-Arma'),
('090610', 'PE', '09', '06', '10', 'San Antonio de Cusicancha'),
('090611', 'PE', '09', '06', '11', 'San Francisco de Sangayaico'),
('090612', 'PE', '09', '06', '12', 'San Isidro'),
('090613', 'PE', '09', '06', '13', 'Santiago de Chocorvos'),
('090614', 'PE', '09', '06', '14', 'Santiago de Quirahuara'),
('090615', 'PE', '09', '06', '15', 'Santo Domingo de Capillas'),
('090616', 'PE', '09', '06', '16', 'Tambo'),
('090700', 'PE', '09', '07', '00', 'Tayacaja'),
('090701', 'PE', '09', '07', '01', 'Pampas'),
('090702', 'PE', '09', '07', '02', 'Acostambo'),
('090703', 'PE', '09', '07', '03', 'Acraquia'),
('090704', 'PE', '09', '07', '04', 'Ahuaycha'),
('090705', 'PE', '09', '07', '05', 'Colcabamba'),
('090706', 'PE', '09', '07', '06', 'Daniel Hernández'),
('090707', 'PE', '09', '07', '07', 'Huachocolpa'),
('090709', 'PE', '09', '07', '09', 'Huaribamba'),
('090710', 'PE', '09', '07', '10', 'Ñahuimpuquio'),
('090711', 'PE', '09', '07', '11', 'Pazos'),
('090713', 'PE', '09', '07', '13', 'Quishuar'),
('090714', 'PE', '09', '07', '14', 'Salcabamba'),
('090715', 'PE', '09', '07', '15', 'Salcahuasi'),
('090716', 'PE', '09', '07', '16', 'San Marcos de Rocchac'),
('090717', 'PE', '09', '07', '17', 'Surcubamba'),
('090718', 'PE', '09', '07', '18', 'Tintay Puncu'),
('090719', 'PE', '09', '07', '19', 'Quichuas'),
('090720', 'PE', '09', '07', '20', 'Andaymarca'),
('100000', 'PE', '10', '00', '00', 'Huánuco'),
('100100', 'PE', '10', '01', '00', 'Huánuco'),
('100101', 'PE', '10', '01', '01', 'Huanuco'),
('100102', 'PE', '10', '01', '02', 'Amarilis'),
('100103', 'PE', '10', '01', '03', 'Chinchao'),
('100104', 'PE', '10', '01', '04', 'Churubamba'),
('100105', 'PE', '10', '01', '05', 'Margos'),
('100106', 'PE', '10', '01', '06', 'Quisqui (Kichki)'),
('100107', 'PE', '10', '01', '07', 'San Francisco de Cayran'),
('100108', 'PE', '10', '01', '08', 'San Pedro de Chaulan'),
('100109', 'PE', '10', '01', '09', 'Santa María del Valle'),
('100110', 'PE', '10', '01', '10', 'Yarumayo'),
('100111', 'PE', '10', '01', '11', 'Pillco Marca'),
('100112', 'PE', '10', '01', '12', 'Yacus'),
('100200', 'PE', '10', '02', '00', 'Ambo'),
('100201', 'PE', '10', '02', '01', 'Ambo'),
('100202', 'PE', '10', '02', '02', 'Cayna'),
('100203', 'PE', '10', '02', '03', 'Colpas'),
('100204', 'PE', '10', '02', '04', 'Conchamarca'),
('100205', 'PE', '10', '02', '05', 'Huacar'),
('100206', 'PE', '10', '02', '06', 'San Francisco'),
('100207', 'PE', '10', '02', '07', 'San Rafael'),
('100208', 'PE', '10', '02', '08', 'Tomay Kichwa'),
('100300', 'PE', '10', '03', '00', 'Dos de Mayo'),
('100301', 'PE', '10', '03', '01', 'La Unión'),
('100307', 'PE', '10', '03', '07', 'Chuquis'),
('100311', 'PE', '10', '03', '11', 'Marías'),
('100313', 'PE', '10', '03', '13', 'Pachas'),
('100316', 'PE', '10', '03', '16', 'Quivilla'),
('100317', 'PE', '10', '03', '17', 'Ripan'),
('100321', 'PE', '10', '03', '21', 'Shunqui'),
('100322', 'PE', '10', '03', '22', 'Sillapata'),
('100323', 'PE', '10', '03', '23', 'Yanas'),
('100400', 'PE', '10', '04', '00', 'Huacaybamba'),
('100401', 'PE', '10', '04', '01', 'Huacaybamba'),
('100402', 'PE', '10', '04', '02', 'Canchabamba'),
('100403', 'PE', '10', '04', '03', 'Cochabamba'),
('100404', 'PE', '10', '04', '04', 'Pinra'),
('100500', 'PE', '10', '05', '00', 'Huamalíes'),
('100501', 'PE', '10', '05', '01', 'Llata'),
('100502', 'PE', '10', '05', '02', 'Arancay'),
('100503', 'PE', '10', '05', '03', 'Chavín de Pariarca'),
('100504', 'PE', '10', '05', '04', 'Jacas Grande'),
('100505', 'PE', '10', '05', '05', 'Jircan'),
('100506', 'PE', '10', '05', '06', 'Miraflores'),
('100507', 'PE', '10', '05', '07', 'Monzón'),
('100508', 'PE', '10', '05', '08', 'Punchao'),
('100509', 'PE', '10', '05', '09', 'Puños'),
('100510', 'PE', '10', '05', '10', 'Singa'),
('100511', 'PE', '10', '05', '11', 'Tantamayo'),
('100600', 'PE', '10', '06', '00', 'Leoncio Prado'),
('100601', 'PE', '10', '06', '01', 'Rupa-Rupa'),
('100602', 'PE', '10', '06', '02', 'Daniel Alomía Robles'),
('100603', 'PE', '10', '06', '03', 'Hermílio Valdizan'),
('100604', 'PE', '10', '06', '04', 'José Crespo y Castillo'),
('100605', 'PE', '10', '06', '05', 'Luyando'),
('100606', 'PE', '10', '06', '06', 'Mariano Damaso Beraun'),
('100700', 'PE', '10', '07', '00', 'Marañón'),
('100701', 'PE', '10', '07', '01', 'Huacrachuco'),
('100702', 'PE', '10', '07', '02', 'Cholon'),
('100703', 'PE', '10', '07', '03', 'San Buenaventura'),
('100800', 'PE', '10', '08', '00', 'Pachitea'),
('100801', 'PE', '10', '08', '01', 'Panao'),
('100802', 'PE', '10', '08', '02', 'Chaglla');
INSERT INTO `ubigeo` (`id_ubigeo`, `pais`, `departamento`, `provincia`, `distrito`, `ciudad`) VALUES
('100803', 'PE', '10', '08', '03', 'Molino'),
('100804', 'PE', '10', '08', '04', 'Umari'),
('100900', 'PE', '10', '09', '00', 'Puerto Inca'),
('100901', 'PE', '10', '09', '01', 'Puerto Inca'),
('100902', 'PE', '10', '09', '02', 'Codo del Pozuzo'),
('100903', 'PE', '10', '09', '03', 'Honoria'),
('100904', 'PE', '10', '09', '04', 'Tournavista'),
('100905', 'PE', '10', '09', '05', 'Yuyapichis'),
('101000', 'PE', '10', '10', '00', 'Lauricocha '),
('101001', 'PE', '10', '10', '01', 'Jesús'),
('101002', 'PE', '10', '10', '02', 'Baños'),
('101003', 'PE', '10', '10', '03', 'Jivia'),
('101004', 'PE', '10', '10', '04', 'Queropalca'),
('101005', 'PE', '10', '10', '05', 'Rondos'),
('101006', 'PE', '10', '10', '06', 'San Francisco de Asís'),
('101007', 'PE', '10', '10', '07', 'San Miguel de Cauri'),
('101100', 'PE', '10', '11', '00', 'Yarowilca '),
('101101', 'PE', '10', '11', '01', 'Chavinillo'),
('101102', 'PE', '10', '11', '02', 'Cahuac'),
('101103', 'PE', '10', '11', '03', 'Chacabamba'),
('101104', 'PE', '10', '11', '04', 'Aparicio Pomares'),
('101105', 'PE', '10', '11', '05', 'Jacas Chico'),
('101106', 'PE', '10', '11', '06', 'Obas'),
('101107', 'PE', '10', '11', '07', 'Pampamarca'),
('101108', 'PE', '10', '11', '08', 'Choras'),
('110000', 'PE', '11', '00', '00', 'Ica'),
('110100', 'PE', '11', '01', '00', 'Ica '),
('110101', 'PE', '11', '01', '01', 'Ica'),
('110102', 'PE', '11', '01', '02', 'La Tinguiña'),
('110103', 'PE', '11', '01', '03', 'Los Aquijes'),
('110104', 'PE', '11', '01', '04', 'Ocucaje'),
('110105', 'PE', '11', '01', '05', 'Pachacutec'),
('110106', 'PE', '11', '01', '06', 'Parcona'),
('110107', 'PE', '11', '01', '07', 'Pueblo Nuevo'),
('110108', 'PE', '11', '01', '08', 'Salas'),
('110109', 'PE', '11', '01', '09', 'San José de Los Molinos'),
('110110', 'PE', '11', '01', '10', 'San Juan Bautista'),
('110111', 'PE', '11', '01', '11', 'Santiago'),
('110112', 'PE', '11', '01', '12', 'Subtanjalla'),
('110113', 'PE', '11', '01', '13', 'Tate'),
('110114', 'PE', '11', '01', '14', 'Yauca del Rosario'),
('110200', 'PE', '11', '02', '00', 'Chincha '),
('110201', 'PE', '11', '02', '01', 'Chincha Alta'),
('110202', 'PE', '11', '02', '02', 'Alto Laran'),
('110203', 'PE', '11', '02', '03', 'Chavin'),
('110204', 'PE', '11', '02', '04', 'Chincha Baja'),
('110205', 'PE', '11', '02', '05', 'El Carmen'),
('110206', 'PE', '11', '02', '06', 'Grocio Prado'),
('110207', 'PE', '11', '02', '07', 'Pueblo Nuevo'),
('110208', 'PE', '11', '02', '08', 'San Juan de Yanac'),
('110209', 'PE', '11', '02', '09', 'San Pedro de Huacarpana'),
('110210', 'PE', '11', '02', '10', 'Sunampe'),
('110211', 'PE', '11', '02', '11', 'Tambo de Mora'),
('110300', 'PE', '11', '03', '00', 'Nazca '),
('110301', 'PE', '11', '03', '01', 'Nasca'),
('110302', 'PE', '11', '03', '02', 'Changuillo'),
('110303', 'PE', '11', '03', '03', 'El Ingenio'),
('110304', 'PE', '11', '03', '04', 'Marcona'),
('110305', 'PE', '11', '03', '05', 'Vista Alegre'),
('110400', 'PE', '11', '04', '00', 'Palpa '),
('110401', 'PE', '11', '04', '01', 'Palpa'),
('110402', 'PE', '11', '04', '02', 'Llipata'),
('110403', 'PE', '11', '04', '03', 'Río Grande'),
('110404', 'PE', '11', '04', '04', 'Santa Cruz'),
('110405', 'PE', '11', '04', '05', 'Tibillo'),
('110500', 'PE', '11', '05', '00', 'Pisco '),
('110501', 'PE', '11', '05', '01', 'Pisco'),
('110502', 'PE', '11', '05', '02', 'Huancano'),
('110503', 'PE', '11', '05', '03', 'Humay'),
('110504', 'PE', '11', '05', '04', 'Independencia'),
('110505', 'PE', '11', '05', '05', 'Paracas'),
('110506', 'PE', '11', '05', '06', 'San Andrés'),
('110507', 'PE', '11', '05', '07', 'San Clemente'),
('110508', 'PE', '11', '05', '08', 'Tupac Amaru Inca'),
('120000', 'PE', '12', '00', '00', 'Junín'),
('120100', 'PE', '12', '01', '00', 'Huancayo '),
('120101', 'PE', '12', '01', '01', 'Huancayo'),
('120104', 'PE', '12', '01', '04', 'Carhuacallanga'),
('120105', 'PE', '12', '01', '05', 'Chacapampa'),
('120106', 'PE', '12', '01', '06', 'Chicche'),
('120107', 'PE', '12', '01', '07', 'Chilca'),
('120108', 'PE', '12', '01', '08', 'Chongos Alto'),
('120111', 'PE', '12', '01', '11', 'Chupuro'),
('120112', 'PE', '12', '01', '12', 'Colca'),
('120113', 'PE', '12', '01', '13', 'Cullhuas'),
('120114', 'PE', '12', '01', '14', 'El Tambo'),
('120116', 'PE', '12', '01', '16', 'Huacrapuquio'),
('120117', 'PE', '12', '01', '17', 'Hualhuas'),
('120119', 'PE', '12', '01', '19', 'Huancan'),
('120120', 'PE', '12', '01', '20', 'Huasicancha'),
('120121', 'PE', '12', '01', '21', 'Huayucachi'),
('120122', 'PE', '12', '01', '22', 'Ingenio'),
('120124', 'PE', '12', '01', '24', 'Pariahuanca'),
('120125', 'PE', '12', '01', '25', 'Pilcomayo'),
('120126', 'PE', '12', '01', '26', 'Pucara'),
('120127', 'PE', '12', '01', '27', 'Quichuay'),
('120128', 'PE', '12', '01', '28', 'Quilcas'),
('120129', 'PE', '12', '01', '29', 'San Agustín'),
('120130', 'PE', '12', '01', '30', 'San Jerónimo de Tunan'),
('120132', 'PE', '12', '01', '32', 'Saño'),
('120133', 'PE', '12', '01', '33', 'Sapallanga'),
('120134', 'PE', '12', '01', '34', 'Sicaya'),
('120135', 'PE', '12', '01', '35', 'Santo Domingo de Acobamba'),
('120136', 'PE', '12', '01', '36', 'Viques'),
('120200', 'PE', '12', '02', '00', 'Concepción '),
('120201', 'PE', '12', '02', '01', 'Concepción'),
('120202', 'PE', '12', '02', '02', 'Aco'),
('120203', 'PE', '12', '02', '03', 'Andamarca'),
('120204', 'PE', '12', '02', '04', 'Chambara'),
('120205', 'PE', '12', '02', '05', 'Cochas'),
('120206', 'PE', '12', '02', '06', 'Comas'),
('120207', 'PE', '12', '02', '07', 'Heroínas Toledo'),
('120208', 'PE', '12', '02', '08', 'Manzanares'),
('120209', 'PE', '12', '02', '09', 'Mariscal Castilla'),
('120210', 'PE', '12', '02', '10', 'Matahuasi'),
('120211', 'PE', '12', '02', '11', 'Mito'),
('120212', 'PE', '12', '02', '12', 'Nueve de Julio'),
('120213', 'PE', '12', '02', '13', 'Orcotuna'),
('120214', 'PE', '12', '02', '14', 'San José de Quero'),
('120215', 'PE', '12', '02', '15', 'Santa Rosa de Ocopa'),
('120300', 'PE', '12', '03', '00', 'Chanchamayo '),
('120301', 'PE', '12', '03', '01', 'Chanchamayo'),
('120302', 'PE', '12', '03', '02', 'Perene'),
('120303', 'PE', '12', '03', '03', 'Pichanaqui'),
('120304', 'PE', '12', '03', '04', 'San Luis de Shuaro'),
('120305', 'PE', '12', '03', '05', 'San Ramón'),
('120306', 'PE', '12', '03', '06', 'Vitoc'),
('120400', 'PE', '12', '04', '00', 'Jauja '),
('120401', 'PE', '12', '04', '01', 'Jauja'),
('120402', 'PE', '12', '04', '02', 'Acolla'),
('120403', 'PE', '12', '04', '03', 'Apata'),
('120404', 'PE', '12', '04', '04', 'Ataura'),
('120405', 'PE', '12', '04', '05', 'Canchayllo'),
('120406', 'PE', '12', '04', '06', 'Curicaca'),
('120407', 'PE', '12', '04', '07', 'El Mantaro'),
('120408', 'PE', '12', '04', '08', 'Huamali'),
('120409', 'PE', '12', '04', '09', 'Huaripampa'),
('120410', 'PE', '12', '04', '10', 'Huertas'),
('120411', 'PE', '12', '04', '11', 'Janjaillo'),
('120412', 'PE', '12', '04', '12', 'Julcán'),
('120413', 'PE', '12', '04', '13', 'Leonor Ordóñez'),
('120414', 'PE', '12', '04', '14', 'Llocllapampa'),
('120415', 'PE', '12', '04', '15', 'Marco'),
('120416', 'PE', '12', '04', '16', 'Masma'),
('120417', 'PE', '12', '04', '17', 'Masma Chicche'),
('120418', 'PE', '12', '04', '18', 'Molinos'),
('120419', 'PE', '12', '04', '19', 'Monobamba'),
('120420', 'PE', '12', '04', '20', 'Muqui'),
('120421', 'PE', '12', '04', '21', 'Muquiyauyo'),
('120422', 'PE', '12', '04', '22', 'Paca'),
('120423', 'PE', '12', '04', '23', 'Paccha'),
('120424', 'PE', '12', '04', '24', 'Pancan'),
('120425', 'PE', '12', '04', '25', 'Parco'),
('120426', 'PE', '12', '04', '26', 'Pomacancha'),
('120427', 'PE', '12', '04', '27', 'Ricran'),
('120428', 'PE', '12', '04', '28', 'San Lorenzo'),
('120429', 'PE', '12', '04', '29', 'San Pedro de Chunan'),
('120430', 'PE', '12', '04', '30', 'Sausa'),
('120431', 'PE', '12', '04', '31', 'Sincos'),
('120432', 'PE', '12', '04', '32', 'Tunan Marca'),
('120433', 'PE', '12', '04', '33', 'Yauli'),
('120434', 'PE', '12', '04', '34', 'Yauyos'),
('120500', 'PE', '12', '05', '00', 'Junín '),
('120501', 'PE', '12', '05', '01', 'Junin'),
('120502', 'PE', '12', '05', '02', 'Carhuamayo'),
('120503', 'PE', '12', '05', '03', 'Ondores'),
('120504', 'PE', '12', '05', '04', 'Ulcumayo'),
('120600', 'PE', '12', '06', '00', 'Satipo '),
('120601', 'PE', '12', '06', '01', 'Satipo'),
('120602', 'PE', '12', '06', '02', 'Coviriali'),
('120603', 'PE', '12', '06', '03', 'Llaylla'),
('120604', 'PE', '12', '06', '04', 'Mazamari'),
('120605', 'PE', '12', '06', '05', 'Pampa Hermosa'),
('120606', 'PE', '12', '06', '06', 'Pangoa'),
('120607', 'PE', '12', '06', '07', 'Río Negro'),
('120608', 'PE', '12', '06', '08', 'Río Tambo'),
('120609', 'PE', '12', '06', '09', 'Vizcatan del Ene'),
('120700', 'PE', '12', '07', '00', 'Tarma '),
('120701', 'PE', '12', '07', '01', 'Tarma'),
('120702', 'PE', '12', '07', '02', 'Acobamba'),
('120703', 'PE', '12', '07', '03', 'Huaricolca'),
('120704', 'PE', '12', '07', '04', 'Huasahuasi'),
('120705', 'PE', '12', '07', '05', 'La Unión'),
('120706', 'PE', '12', '07', '06', 'Palca'),
('120707', 'PE', '12', '07', '07', 'Palcamayo'),
('120708', 'PE', '12', '07', '08', 'San Pedro de Cajas'),
('120709', 'PE', '12', '07', '09', 'Tapo'),
('120800', 'PE', '12', '08', '00', 'Yauli '),
('120801', 'PE', '12', '08', '01', 'La Oroya'),
('120802', 'PE', '12', '08', '02', 'Chacapalpa'),
('120803', 'PE', '12', '08', '03', 'Huay-Huay'),
('120804', 'PE', '12', '08', '04', 'Marcapomacocha'),
('120805', 'PE', '12', '08', '05', 'Morococha'),
('120806', 'PE', '12', '08', '06', 'Paccha'),
('120807', 'PE', '12', '08', '07', 'Santa Bárbara de Carhuacayan'),
('120808', 'PE', '12', '08', '08', 'Santa Rosa de Sacco'),
('120809', 'PE', '12', '08', '09', 'Suitucancha'),
('120810', 'PE', '12', '08', '10', 'Yauli'),
('120900', 'PE', '12', '09', '00', 'Chupaca '),
('120901', 'PE', '12', '09', '01', 'Chupaca'),
('120902', 'PE', '12', '09', '02', 'Ahuac'),
('120903', 'PE', '12', '09', '03', 'Chongos Bajo'),
('120904', 'PE', '12', '09', '04', 'Huachac'),
('120905', 'PE', '12', '09', '05', 'Huamancaca Chico'),
('120906', 'PE', '12', '09', '06', 'San Juan de Iscos'),
('120907', 'PE', '12', '09', '07', 'San Juan de Jarpa'),
('120908', 'PE', '12', '09', '08', 'Tres de Diciembre'),
('120909', 'PE', '12', '09', '09', 'Yanacancha'),
('130000', 'PE', '13', '00', '00', 'La Libertad'),
('130100', 'PE', '13', '01', '00', 'Trujillo '),
('130101', 'PE', '13', '01', '01', 'Trujillo'),
('130102', 'PE', '13', '01', '02', 'El Porvenir'),
('130103', 'PE', '13', '01', '03', 'Florencia de Mora'),
('130104', 'PE', '13', '01', '04', 'Huanchaco'),
('130105', 'PE', '13', '01', '05', 'La Esperanza'),
('130106', 'PE', '13', '01', '06', 'Laredo'),
('130107', 'PE', '13', '01', '07', 'Moche'),
('130108', 'PE', '13', '01', '08', 'Poroto'),
('130109', 'PE', '13', '01', '09', 'Salaverry'),
('130110', 'PE', '13', '01', '10', 'Simbal'),
('130111', 'PE', '13', '01', '11', 'Victor Larco Herrera'),
('130200', 'PE', '13', '02', '00', 'Ascope '),
('130201', 'PE', '13', '02', '01', 'Ascope'),
('130202', 'PE', '13', '02', '02', 'Chicama'),
('130203', 'PE', '13', '02', '03', 'Chocope'),
('130204', 'PE', '13', '02', '04', 'Magdalena de Cao'),
('130205', 'PE', '13', '02', '05', 'Paijan'),
('130206', 'PE', '13', '02', '06', 'Rázuri'),
('130207', 'PE', '13', '02', '07', 'Santiago de Cao'),
('130208', 'PE', '13', '02', '08', 'Casa Grande'),
('130300', 'PE', '13', '03', '00', 'Bolívar '),
('130301', 'PE', '13', '03', '01', 'Bolívar'),
('130302', 'PE', '13', '03', '02', 'Bambamarca'),
('130303', 'PE', '13', '03', '03', 'Condormarca'),
('130304', 'PE', '13', '03', '04', 'Longotea'),
('130305', 'PE', '13', '03', '05', 'Uchumarca'),
('130306', 'PE', '13', '03', '06', 'Ucuncha'),
('130400', 'PE', '13', '04', '00', 'Chepén '),
('130401', 'PE', '13', '04', '01', 'Chepen'),
('130402', 'PE', '13', '04', '02', 'Pacanga'),
('130403', 'PE', '13', '04', '03', 'Pueblo Nuevo'),
('130500', 'PE', '13', '05', '00', 'Julcán '),
('130501', 'PE', '13', '05', '01', 'Julcan'),
('130502', 'PE', '13', '05', '02', 'Calamarca'),
('130503', 'PE', '13', '05', '03', 'Carabamba'),
('130504', 'PE', '13', '05', '04', 'Huaso'),
('130600', 'PE', '13', '06', '00', 'Otuzco '),
('130601', 'PE', '13', '06', '01', 'Otuzco'),
('130602', 'PE', '13', '06', '02', 'Agallpampa'),
('130604', 'PE', '13', '06', '04', 'Charat'),
('130605', 'PE', '13', '06', '05', 'Huaranchal'),
('130606', 'PE', '13', '06', '06', 'La Cuesta'),
('130608', 'PE', '13', '06', '08', 'Mache'),
('130610', 'PE', '13', '06', '10', 'Paranday'),
('130611', 'PE', '13', '06', '11', 'Salpo'),
('130613', 'PE', '13', '06', '13', 'Sinsicap'),
('130614', 'PE', '13', '06', '14', 'Usquil'),
('130700', 'PE', '13', '07', '00', 'Pacasmayo '),
('130701', 'PE', '13', '07', '01', 'San Pedro de Lloc'),
('130702', 'PE', '13', '07', '02', 'Guadalupe'),
('130703', 'PE', '13', '07', '03', 'Jequetepeque'),
('130704', 'PE', '13', '07', '04', 'Pacasmayo'),
('130705', 'PE', '13', '07', '05', 'San José'),
('130800', 'PE', '13', '08', '00', 'Pataz '),
('130801', 'PE', '13', '08', '01', 'Tayabamba'),
('130802', 'PE', '13', '08', '02', 'Buldibuyo'),
('130803', 'PE', '13', '08', '03', 'Chillia'),
('130804', 'PE', '13', '08', '04', 'Huancaspata'),
('130805', 'PE', '13', '08', '05', 'Huaylillas'),
('130806', 'PE', '13', '08', '06', 'Huayo'),
('130807', 'PE', '13', '08', '07', 'Ongon'),
('130808', 'PE', '13', '08', '08', 'Parcoy'),
('130809', 'PE', '13', '08', '09', 'Pataz'),
('130810', 'PE', '13', '08', '10', 'Pias'),
('130811', 'PE', '13', '08', '11', 'Santiago de Challas'),
('130812', 'PE', '13', '08', '12', 'Taurija'),
('130813', 'PE', '13', '08', '13', 'Urpay'),
('130900', 'PE', '13', '09', '00', 'Sánchez Carrión '),
('130901', 'PE', '13', '09', '01', 'Huamachuco'),
('130902', 'PE', '13', '09', '02', 'Chugay'),
('130903', 'PE', '13', '09', '03', 'Cochorco'),
('130904', 'PE', '13', '09', '04', 'Curgos'),
('130905', 'PE', '13', '09', '05', 'Marcabal'),
('130906', 'PE', '13', '09', '06', 'Sanagoran'),
('130907', 'PE', '13', '09', '07', 'Sarin'),
('130908', 'PE', '13', '09', '08', 'Sartimbamba'),
('131000', 'PE', '13', '10', '00', 'Santiago de Chuco '),
('131001', 'PE', '13', '10', '01', 'Santiago de Chuco'),
('131002', 'PE', '13', '10', '02', 'Angasmarca'),
('131003', 'PE', '13', '10', '03', 'Cachicadan'),
('131004', 'PE', '13', '10', '04', 'Mollebamba'),
('131005', 'PE', '13', '10', '05', 'Mollepata'),
('131006', 'PE', '13', '10', '06', 'Quiruvilca'),
('131007', 'PE', '13', '10', '07', 'Santa Cruz de Chuca'),
('131008', 'PE', '13', '10', '08', 'Sitabamba'),
('131100', 'PE', '13', '11', '00', 'Gran Chimú '),
('131101', 'PE', '13', '11', '01', 'Cascas'),
('131102', 'PE', '13', '11', '02', 'Lucma'),
('131103', 'PE', '13', '11', '03', 'Marmot'),
('131104', 'PE', '13', '11', '04', 'Sayapullo'),
('131200', 'PE', '13', '12', '00', 'Virú '),
('131201', 'PE', '13', '12', '01', 'Viru'),
('131202', 'PE', '13', '12', '02', 'Chao'),
('131203', 'PE', '13', '12', '03', 'Guadalupito'),
('140000', 'PE', '14', '00', '00', 'Lambayeque'),
('140100', 'PE', '14', '01', '00', 'Chiclayo '),
('140101', 'PE', '14', '01', '01', 'Chiclayo'),
('140102', 'PE', '14', '01', '02', 'Chongoyape'),
('140103', 'PE', '14', '01', '03', 'Eten'),
('140104', 'PE', '14', '01', '04', 'Eten Puerto'),
('140105', 'PE', '14', '01', '05', 'José Leonardo Ortiz'),
('140106', 'PE', '14', '01', '06', 'La Victoria'),
('140107', 'PE', '14', '01', '07', 'Lagunas'),
('140108', 'PE', '14', '01', '08', 'Monsefu'),
('140109', 'PE', '14', '01', '09', 'Nueva Arica'),
('140110', 'PE', '14', '01', '10', 'Oyotun'),
('140111', 'PE', '14', '01', '11', 'Picsi'),
('140112', 'PE', '14', '01', '12', 'Pimentel'),
('140113', 'PE', '14', '01', '13', 'Reque'),
('140114', 'PE', '14', '01', '14', 'Santa Rosa'),
('140115', 'PE', '14', '01', '15', 'Saña'),
('140116', 'PE', '14', '01', '16', 'Cayalti'),
('140117', 'PE', '14', '01', '17', 'Patapo'),
('140118', 'PE', '14', '01', '18', 'Pomalca'),
('140119', 'PE', '14', '01', '19', 'Pucala'),
('140120', 'PE', '14', '01', '20', 'Tuman'),
('140200', 'PE', '14', '02', '00', 'Ferreñafe '),
('140201', 'PE', '14', '02', '01', 'Ferreñafe'),
('140202', 'PE', '14', '02', '02', 'Cañaris'),
('140203', 'PE', '14', '02', '03', 'Incahuasi'),
('140204', 'PE', '14', '02', '04', 'Manuel Antonio Mesones Muro'),
('140205', 'PE', '14', '02', '05', 'Pitipo'),
('140206', 'PE', '14', '02', '06', 'Pueblo Nuevo'),
('140300', 'PE', '14', '03', '00', 'Lambayeque '),
('140301', 'PE', '14', '03', '01', 'Lambayeque'),
('140302', 'PE', '14', '03', '02', 'Chochope'),
('140303', 'PE', '14', '03', '03', 'Illimo'),
('140304', 'PE', '14', '03', '04', 'Jayanca'),
('140305', 'PE', '14', '03', '05', 'Mochumi'),
('140306', 'PE', '14', '03', '06', 'Morrope'),
('140307', 'PE', '14', '03', '07', 'Motupe'),
('140308', 'PE', '14', '03', '08', 'Olmos'),
('140309', 'PE', '14', '03', '09', 'Pacora'),
('140310', 'PE', '14', '03', '10', 'Salas'),
('140311', 'PE', '14', '03', '11', 'San José'),
('140312', 'PE', '14', '03', '12', 'Tucume'),
('150000', 'PE', '15', '00', '00', 'Lima'),
('150100', 'PE', '15', '01', '00', 'Lima '),
('150101', 'PE', '15', '01', '01', 'Lima'),
('150102', 'PE', '15', '01', '02', 'Ancón'),
('150103', 'PE', '15', '01', '03', 'Ate'),
('150104', 'PE', '15', '01', '04', 'Barranco'),
('150105', 'PE', '15', '01', '05', 'Breña'),
('150106', 'PE', '15', '01', '06', 'Carabayllo'),
('150107', 'PE', '15', '01', '07', 'Chaclacayo'),
('150108', 'PE', '15', '01', '08', 'Chorrillos'),
('150109', 'PE', '15', '01', '09', 'Cieneguilla'),
('150110', 'PE', '15', '01', '10', 'Comas'),
('150111', 'PE', '15', '01', '11', 'El Agustino'),
('150112', 'PE', '15', '01', '12', 'Independencia'),
('150113', 'PE', '15', '01', '13', 'Jesús María'),
('150114', 'PE', '15', '01', '14', 'La Molina'),
('150115', 'PE', '15', '01', '15', 'La Victoria'),
('150116', 'PE', '15', '01', '16', 'Lince'),
('150117', 'PE', '15', '01', '17', 'Los Olivos'),
('150118', 'PE', '15', '01', '18', 'Lurigancho'),
('150119', 'PE', '15', '01', '19', 'Lurin'),
('150120', 'PE', '15', '01', '20', 'Magdalena del Mar'),
('150121', 'PE', '15', '01', '21', 'Pueblo Libre'),
('150122', 'PE', '15', '01', '22', 'Miraflores'),
('150123', 'PE', '15', '01', '23', 'Pachacamac'),
('150124', 'PE', '15', '01', '24', 'Pucusana'),
('150125', 'PE', '15', '01', '25', 'Puente Piedra'),
('150126', 'PE', '15', '01', '26', 'Punta Hermosa'),
('150127', 'PE', '15', '01', '27', 'Punta Negra'),
('150128', 'PE', '15', '01', '28', 'Rímac'),
('150129', 'PE', '15', '01', '29', 'San Bartolo'),
('150130', 'PE', '15', '01', '30', 'San Borja'),
('150131', 'PE', '15', '01', '31', 'San Isidro'),
('150132', 'PE', '15', '01', '32', 'San Juan de Lurigancho'),
('150133', 'PE', '15', '01', '33', 'San Juan de Miraflores'),
('150134', 'PE', '15', '01', '34', 'San Luis'),
('150135', 'PE', '15', '01', '35', 'San Martín de Porres'),
('150136', 'PE', '15', '01', '36', 'San Miguel'),
('150137', 'PE', '15', '01', '37', 'Santa Anita'),
('150138', 'PE', '15', '01', '38', 'Santa María del Mar'),
('150139', 'PE', '15', '01', '39', 'Santa Rosa'),
('150140', 'PE', '15', '01', '40', 'Santiago de Surco'),
('150141', 'PE', '15', '01', '41', 'Surquillo'),
('150142', 'PE', '15', '01', '42', 'Villa El Salvador'),
('150143', 'PE', '15', '01', '43', 'Villa María del Triunfo'),
('150200', 'PE', '15', '02', '00', 'Barranca '),
('150201', 'PE', '15', '02', '01', 'Barranca'),
('150202', 'PE', '15', '02', '02', 'Paramonga'),
('150203', 'PE', '15', '02', '03', 'Pativilca'),
('150204', 'PE', '15', '02', '04', 'Supe'),
('150205', 'PE', '15', '02', '05', 'Supe Puerto'),
('150300', 'PE', '15', '03', '00', 'Cajatambo '),
('150301', 'PE', '15', '03', '01', 'Cajatambo'),
('150302', 'PE', '15', '03', '02', 'Copa'),
('150303', 'PE', '15', '03', '03', 'Gorgor'),
('150304', 'PE', '15', '03', '04', 'Huancapon'),
('150305', 'PE', '15', '03', '05', 'Manas'),
('150400', 'PE', '15', '04', '00', 'Canta '),
('150401', 'PE', '15', '04', '01', 'Canta'),
('150402', 'PE', '15', '04', '02', 'Arahuay'),
('150403', 'PE', '15', '04', '03', 'Huamantanga'),
('150404', 'PE', '15', '04', '04', 'Huaros'),
('150405', 'PE', '15', '04', '05', 'Lachaqui'),
('150406', 'PE', '15', '04', '06', 'San Buenaventura'),
('150407', 'PE', '15', '04', '07', 'Santa Rosa de Quives'),
('150500', 'PE', '15', '05', '00', 'Cañete '),
('150501', 'PE', '15', '05', '01', 'San Vicente de Cañete'),
('150502', 'PE', '15', '05', '02', 'Asia'),
('150503', 'PE', '15', '05', '03', 'Calango'),
('150504', 'PE', '15', '05', '04', 'Cerro Azul'),
('150505', 'PE', '15', '05', '05', 'Chilca'),
('150506', 'PE', '15', '05', '06', 'Coayllo'),
('150507', 'PE', '15', '05', '07', 'Imperial'),
('150508', 'PE', '15', '05', '08', 'Lunahuana'),
('150509', 'PE', '15', '05', '09', 'Mala'),
('150510', 'PE', '15', '05', '10', 'Nuevo Imperial'),
('150511', 'PE', '15', '05', '11', 'Pacaran'),
('150512', 'PE', '15', '05', '12', 'Quilmana'),
('150513', 'PE', '15', '05', '13', 'San Antonio'),
('150514', 'PE', '15', '05', '14', 'San Luis'),
('150515', 'PE', '15', '05', '15', 'Santa Cruz de Flores'),
('150516', 'PE', '15', '05', '16', 'Zúñiga'),
('150600', 'PE', '15', '06', '00', 'Huaral '),
('150601', 'PE', '15', '06', '01', 'Huaral'),
('150602', 'PE', '15', '06', '02', 'Atavillos Alto'),
('150603', 'PE', '15', '06', '03', 'Atavillos Bajo'),
('150604', 'PE', '15', '06', '04', 'Aucallama'),
('150605', 'PE', '15', '06', '05', 'Chancay'),
('150606', 'PE', '15', '06', '06', 'Ihuari'),
('150607', 'PE', '15', '06', '07', 'Lampian'),
('150608', 'PE', '15', '06', '08', 'Pacaraos'),
('150609', 'PE', '15', '06', '09', 'San Miguel de Acos'),
('150610', 'PE', '15', '06', '10', 'Santa Cruz de Andamarca'),
('150611', 'PE', '15', '06', '11', 'Sumbilca'),
('150612', 'PE', '15', '06', '12', 'Veintisiete de Noviembre'),
('150700', 'PE', '15', '07', '00', 'Huarochirí '),
('150701', 'PE', '15', '07', '01', 'Matucana'),
('150702', 'PE', '15', '07', '02', 'Antioquia'),
('150703', 'PE', '15', '07', '03', 'Callahuanca'),
('150704', 'PE', '15', '07', '04', 'Carampoma'),
('150705', 'PE', '15', '07', '05', 'Chicla'),
('150706', 'PE', '15', '07', '06', 'Cuenca'),
('150707', 'PE', '15', '07', '07', 'Huachupampa'),
('150708', 'PE', '15', '07', '08', 'Huanza'),
('150709', 'PE', '15', '07', '09', 'Huarochiri'),
('150710', 'PE', '15', '07', '10', 'Lahuaytambo'),
('150711', 'PE', '15', '07', '11', 'Langa'),
('150712', 'PE', '15', '07', '12', 'Laraos'),
('150713', 'PE', '15', '07', '13', 'Mariatana'),
('150714', 'PE', '15', '07', '14', 'Ricardo Palma'),
('150715', 'PE', '15', '07', '15', 'San Andrés de Tupicocha'),
('150716', 'PE', '15', '07', '16', 'San Antonio'),
('150717', 'PE', '15', '07', '17', 'San Bartolomé'),
('150718', 'PE', '15', '07', '18', 'San Damian'),
('150719', 'PE', '15', '07', '19', 'San Juan de Iris'),
('150720', 'PE', '15', '07', '20', 'San Juan de Tantaranche'),
('150721', 'PE', '15', '07', '21', 'San Lorenzo de Quinti'),
('150722', 'PE', '15', '07', '22', 'San Mateo'),
('150723', 'PE', '15', '07', '23', 'San Mateo de Otao'),
('150724', 'PE', '15', '07', '24', 'San Pedro de Casta'),
('150725', 'PE', '15', '07', '25', 'San Pedro de Huancayre'),
('150726', 'PE', '15', '07', '26', 'Sangallaya'),
('150727', 'PE', '15', '07', '27', 'Santa Cruz de Cocachacra'),
('150728', 'PE', '15', '07', '28', 'Santa Eulalia'),
('150729', 'PE', '15', '07', '29', 'Santiago de Anchucaya'),
('150730', 'PE', '15', '07', '30', 'Santiago de Tuna'),
('150731', 'PE', '15', '07', '31', 'Santo Domingo de Los Olleros'),
('150732', 'PE', '15', '07', '32', 'Surco'),
('150800', 'PE', '15', '08', '00', 'Huaura '),
('150801', 'PE', '15', '08', '01', 'Huacho'),
('150802', 'PE', '15', '08', '02', 'Ambar'),
('150803', 'PE', '15', '08', '03', 'Caleta de Carquin'),
('150804', 'PE', '15', '08', '04', 'Checras'),
('150805', 'PE', '15', '08', '05', 'Hualmay'),
('150806', 'PE', '15', '08', '06', 'Huaura'),
('150807', 'PE', '15', '08', '07', 'Leoncio Prado'),
('150808', 'PE', '15', '08', '08', 'Paccho'),
('150809', 'PE', '15', '08', '09', 'Santa Leonor'),
('150810', 'PE', '15', '08', '10', 'Santa María'),
('150811', 'PE', '15', '08', '11', 'Sayan'),
('150812', 'PE', '15', '08', '12', 'Vegueta'),
('150900', 'PE', '15', '09', '00', 'Oyón '),
('150901', 'PE', '15', '09', '01', 'Oyon'),
('150902', 'PE', '15', '09', '02', 'Andajes'),
('150903', 'PE', '15', '09', '03', 'Caujul'),
('150904', 'PE', '15', '09', '04', 'Cochamarca'),
('150905', 'PE', '15', '09', '05', 'Navan'),
('150906', 'PE', '15', '09', '06', 'Pachangara'),
('151000', 'PE', '15', '10', '00', 'Yauyos '),
('151001', 'PE', '15', '10', '01', 'Yauyos'),
('151002', 'PE', '15', '10', '02', 'Alis'),
('151003', 'PE', '15', '10', '03', 'Allauca'),
('151004', 'PE', '15', '10', '04', 'Ayaviri'),
('151005', 'PE', '15', '10', '05', 'Azángaro'),
('151006', 'PE', '15', '10', '06', 'Cacra'),
('151007', 'PE', '15', '10', '07', 'Carania'),
('151008', 'PE', '15', '10', '08', 'Catahuasi'),
('151009', 'PE', '15', '10', '09', 'Chocos'),
('151010', 'PE', '15', '10', '10', 'Cochas'),
('151011', 'PE', '15', '10', '11', 'Colonia'),
('151012', 'PE', '15', '10', '12', 'Hongos'),
('151013', 'PE', '15', '10', '13', 'Huampara'),
('151014', 'PE', '15', '10', '14', 'Huancaya'),
('151015', 'PE', '15', '10', '15', 'Huangascar'),
('151016', 'PE', '15', '10', '16', 'Huantan'),
('151017', 'PE', '15', '10', '17', 'Huañec'),
('151018', 'PE', '15', '10', '18', 'Laraos'),
('151019', 'PE', '15', '10', '19', 'Lincha'),
('151020', 'PE', '15', '10', '20', 'Madean'),
('151021', 'PE', '15', '10', '21', 'Miraflores'),
('151022', 'PE', '15', '10', '22', 'Omas'),
('151023', 'PE', '15', '10', '23', 'Putinza'),
('151024', 'PE', '15', '10', '24', 'Quinches'),
('151025', 'PE', '15', '10', '25', 'Quinocay'),
('151026', 'PE', '15', '10', '26', 'San Joaquín'),
('151027', 'PE', '15', '10', '27', 'San Pedro de Pilas'),
('151028', 'PE', '15', '10', '28', 'Tanta'),
('151029', 'PE', '15', '10', '29', 'Tauripampa'),
('151030', 'PE', '15', '10', '30', 'Tomas'),
('151031', 'PE', '15', '10', '31', 'Tupe'),
('151032', 'PE', '15', '10', '32', 'Viñac'),
('151033', 'PE', '15', '10', '33', 'Vitis'),
('160000', 'PE', '16', '00', '00', 'Loreto'),
('160100', 'PE', '16', '01', '00', 'Maynas '),
('160101', 'PE', '16', '01', '01', 'Iquitos'),
('160102', 'PE', '16', '01', '02', 'Alto Nanay'),
('160103', 'PE', '16', '01', '03', 'Fernando Lores'),
('160104', 'PE', '16', '01', '04', 'Indiana'),
('160105', 'PE', '16', '01', '05', 'Las Amazonas'),
('160106', 'PE', '16', '01', '06', 'Mazan'),
('160107', 'PE', '16', '01', '07', 'Napo'),
('160108', 'PE', '16', '01', '08', 'Punchana'),
('160110', 'PE', '16', '01', '10', 'Torres Causana'),
('160112', 'PE', '16', '01', '12', 'Belén'),
('160113', 'PE', '16', '01', '13', 'San Juan Bautista'),
('160200', 'PE', '16', '02', '00', 'Alto Amazonas '),
('160201', 'PE', '16', '02', '01', 'Yurimaguas'),
('160202', 'PE', '16', '02', '02', 'Balsapuerto'),
('160205', 'PE', '16', '02', '05', 'Jeberos'),
('160206', 'PE', '16', '02', '06', 'Lagunas'),
('160210', 'PE', '16', '02', '10', 'Santa Cruz'),
('160211', 'PE', '16', '02', '11', 'Teniente Cesar López Rojas'),
('160300', 'PE', '16', '03', '00', 'Loreto '),
('160301', 'PE', '16', '03', '01', 'Nauta'),
('160302', 'PE', '16', '03', '02', 'Parinari'),
('160303', 'PE', '16', '03', '03', 'Tigre'),
('160304', 'PE', '16', '03', '04', 'Trompeteros'),
('160305', 'PE', '16', '03', '05', 'Urarinas'),
('160400', 'PE', '16', '04', '00', 'Mariscal Ramón Castilla '),
('160401', 'PE', '16', '04', '01', 'Ramón Castilla'),
('160402', 'PE', '16', '04', '02', 'Pebas'),
('160403', 'PE', '16', '04', '03', 'Yavari'),
('160404', 'PE', '16', '04', '04', 'San Pablo'),
('160500', 'PE', '16', '05', '00', 'Requena '),
('160501', 'PE', '16', '05', '01', 'Requena'),
('160502', 'PE', '16', '05', '02', 'Alto Tapiche'),
('160503', 'PE', '16', '05', '03', 'Capelo'),
('160504', 'PE', '16', '05', '04', 'Emilio San Martín'),
('160505', 'PE', '16', '05', '05', 'Maquia'),
('160506', 'PE', '16', '05', '06', 'Puinahua'),
('160507', 'PE', '16', '05', '07', 'Saquena'),
('160508', 'PE', '16', '05', '08', 'Soplin'),
('160509', 'PE', '16', '05', '09', 'Tapiche'),
('160510', 'PE', '16', '05', '10', 'Jenaro Herrera'),
('160511', 'PE', '16', '05', '11', 'Yaquerana'),
('160600', 'PE', '16', '06', '00', 'Ucayali '),
('160601', 'PE', '16', '06', '01', 'Contamana'),
('160602', 'PE', '16', '06', '02', 'Inahuaya'),
('160603', 'PE', '16', '06', '03', 'Padre Márquez'),
('160604', 'PE', '16', '06', '04', 'Pampa Hermosa'),
('160605', 'PE', '16', '06', '05', 'Sarayacu'),
('160606', 'PE', '16', '06', '06', 'Vargas Guerra'),
('160700', 'PE', '16', '07', '00', 'Datem del Marañón '),
('160701', 'PE', '16', '07', '01', 'Barranca'),
('160702', 'PE', '16', '07', '02', 'Cahuapanas'),
('160703', 'PE', '16', '07', '03', 'Manseriche'),
('160704', 'PE', '16', '07', '04', 'Morona'),
('160705', 'PE', '16', '07', '05', 'Pastaza'),
('160706', 'PE', '16', '07', '06', 'Andoas'),
('160800', 'PE', '16', '08', '00', 'Putumayo'),
('160801', 'PE', '16', '08', '01', 'Putumayo'),
('160802', 'PE', '16', '08', '02', 'Rosa Panduro'),
('160803', 'PE', '16', '08', '03', 'Teniente Manuel Clavero'),
('160804', 'PE', '16', '08', '04', 'Yaguas'),
('170000', 'PE', '17', '00', '00', 'Madre de Dios'),
('170100', 'PE', '17', '01', '00', 'Tambopata '),
('170101', 'PE', '17', '01', '01', 'Tambopata'),
('170102', 'PE', '17', '01', '02', 'Inambari'),
('170103', 'PE', '17', '01', '03', 'Las Piedras'),
('170104', 'PE', '17', '01', '04', 'Laberinto'),
('170200', 'PE', '17', '02', '00', 'Manu '),
('170201', 'PE', '17', '02', '01', 'Manu'),
('170202', 'PE', '17', '02', '02', 'Fitzcarrald'),
('170203', 'PE', '17', '02', '03', 'Madre de Dios'),
('170204', 'PE', '17', '02', '04', 'Huepetuhe'),
('170300', 'PE', '17', '03', '00', 'Tahuamanu '),
('170301', 'PE', '17', '03', '01', 'Iñapari'),
('170302', 'PE', '17', '03', '02', 'Iberia'),
('170303', 'PE', '17', '03', '03', 'Tahuamanu'),
('180000', 'PE', '18', '00', '00', 'Moquegua'),
('180100', 'PE', '18', '01', '00', 'Mariscal Nieto '),
('180101', 'PE', '18', '01', '01', 'Moquegua'),
('180102', 'PE', '18', '01', '02', 'Carumas'),
('180103', 'PE', '18', '01', '03', 'Cuchumbaya'),
('180104', 'PE', '18', '01', '04', 'Samegua'),
('180105', 'PE', '18', '01', '05', 'San Cristóbal'),
('180106', 'PE', '18', '01', '06', 'Torata'),
('180200', 'PE', '18', '02', '00', 'General Sánchez Cerro '),
('180201', 'PE', '18', '02', '01', 'Omate'),
('180202', 'PE', '18', '02', '02', 'Chojata'),
('180203', 'PE', '18', '02', '03', 'Coalaque'),
('180204', 'PE', '18', '02', '04', 'Ichuña'),
('180205', 'PE', '18', '02', '05', 'La Capilla'),
('180206', 'PE', '18', '02', '06', 'Lloque'),
('180207', 'PE', '18', '02', '07', 'Matalaque'),
('180208', 'PE', '18', '02', '08', 'Puquina'),
('180209', 'PE', '18', '02', '09', 'Quinistaquillas'),
('180210', 'PE', '18', '02', '10', 'Ubinas'),
('180211', 'PE', '18', '02', '11', 'Yunga'),
('180300', 'PE', '18', '03', '00', 'Ilo '),
('180301', 'PE', '18', '03', '01', 'Ilo'),
('180302', 'PE', '18', '03', '02', 'El Algarrobal'),
('180303', 'PE', '18', '03', '03', 'Pacocha'),
('190000', 'PE', '19', '00', '00', 'Pasco'),
('190100', 'PE', '19', '01', '00', 'Pasco '),
('190101', 'PE', '19', '01', '01', 'Chaupimarca'),
('190102', 'PE', '19', '01', '02', 'Huachon'),
('190103', 'PE', '19', '01', '03', 'Huariaca'),
('190104', 'PE', '19', '01', '04', 'Huayllay'),
('190105', 'PE', '19', '01', '05', 'Ninacaca'),
('190106', 'PE', '19', '01', '06', 'Pallanchacra'),
('190107', 'PE', '19', '01', '07', 'Paucartambo'),
('190108', 'PE', '19', '01', '08', 'San Francisco de Asís de Yarusyacan'),
('190109', 'PE', '19', '01', '09', 'Simon Bolívar'),
('190110', 'PE', '19', '01', '10', 'Ticlacayan'),
('190111', 'PE', '19', '01', '11', 'Tinyahuarco'),
('190112', 'PE', '19', '01', '12', 'Vicco'),
('190113', 'PE', '19', '01', '13', 'Yanacancha'),
('190200', 'PE', '19', '02', '00', 'Daniel Alcides Carrión '),
('190201', 'PE', '19', '02', '01', 'Yanahuanca'),
('190202', 'PE', '19', '02', '02', 'Chacayan'),
('190203', 'PE', '19', '02', '03', 'Goyllarisquizga'),
('190204', 'PE', '19', '02', '04', 'Paucar'),
('190205', 'PE', '19', '02', '05', 'San Pedro de Pillao'),
('190206', 'PE', '19', '02', '06', 'Santa Ana de Tusi'),
('190207', 'PE', '19', '02', '07', 'Tapuc'),
('190208', 'PE', '19', '02', '08', 'Vilcabamba'),
('190300', 'PE', '19', '03', '00', 'Oxapampa '),
('190301', 'PE', '19', '03', '01', 'Oxapampa'),
('190302', 'PE', '19', '03', '02', 'Chontabamba'),
('190303', 'PE', '19', '03', '03', 'Huancabamba'),
('190304', 'PE', '19', '03', '04', 'Palcazu'),
('190305', 'PE', '19', '03', '05', 'Pozuzo'),
('190306', 'PE', '19', '03', '06', 'Puerto Bermúdez'),
('190307', 'PE', '19', '03', '07', 'Villa Rica'),
('190308', 'PE', '19', '03', '08', 'Constitución'),
('200000', 'PE', '20', '00', '00', 'Piura'),
('200100', 'PE', '20', '01', '00', 'Piura '),
('200101', 'PE', '20', '01', '01', 'Piura'),
('200104', 'PE', '20', '01', '04', 'Castilla'),
('200105', 'PE', '20', '01', '05', 'Atacaos'),
('200107', 'PE', '20', '01', '07', 'Cura Mori'),
('200108', 'PE', '20', '01', '08', 'El Tallan'),
('200109', 'PE', '20', '01', '09', 'La Arena'),
('200110', 'PE', '20', '01', '10', 'La Unión'),
('200111', 'PE', '20', '01', '11', 'Las Lomas'),
('200114', 'PE', '20', '01', '14', 'Tambo Grande'),
('200115', 'PE', '20', '01', '15', 'Veintiseis de Octubre'),
('200200', 'PE', '20', '02', '00', 'Ayabaca '),
('200201', 'PE', '20', '02', '01', 'Ayabaca'),
('200202', 'PE', '20', '02', '02', 'Frias'),
('200203', 'PE', '20', '02', '03', 'Jilili'),
('200204', 'PE', '20', '02', '04', 'Lagunas'),
('200205', 'PE', '20', '02', '05', 'Montero'),
('200206', 'PE', '20', '02', '06', 'Pacaipampa'),
('200207', 'PE', '20', '02', '07', 'Paimas'),
('200208', 'PE', '20', '02', '08', 'Sapillica'),
('200209', 'PE', '20', '02', '09', 'Sicchez'),
('200210', 'PE', '20', '02', '10', 'Suyo'),
('200300', 'PE', '20', '03', '00', 'Huancabamba '),
('200301', 'PE', '20', '03', '01', 'Huancabamba'),
('200302', 'PE', '20', '03', '02', 'Canchaque'),
('200303', 'PE', '20', '03', '03', 'El Carmen de la Frontera'),
('200304', 'PE', '20', '03', '04', 'Huarmaca'),
('200305', 'PE', '20', '03', '05', 'Lalaquiz'),
('200306', 'PE', '20', '03', '06', 'San Miguel de El Faique'),
('200307', 'PE', '20', '03', '07', 'Sondor'),
('200308', 'PE', '20', '03', '08', 'Sondorillo'),
('200400', 'PE', '20', '04', '00', 'Morropón '),
('200401', 'PE', '20', '04', '01', 'Chulucanas'),
('200402', 'PE', '20', '04', '02', 'Buenos Aires'),
('200403', 'PE', '20', '04', '03', 'Chalaco'),
('200404', 'PE', '20', '04', '04', 'La Matanza'),
('200405', 'PE', '20', '04', '05', 'Morropon'),
('200406', 'PE', '20', '04', '06', 'Salitral'),
('200407', 'PE', '20', '04', '07', 'San Juan de Bigote'),
('200408', 'PE', '20', '04', '08', 'Santa Catalina de Mossa'),
('200409', 'PE', '20', '04', '09', 'Santo Domingo'),
('200410', 'PE', '20', '04', '10', 'Yamango'),
('200500', 'PE', '20', '05', '00', 'Paita '),
('200501', 'PE', '20', '05', '01', 'Paita'),
('200502', 'PE', '20', '05', '02', 'Amotape'),
('200503', 'PE', '20', '05', '03', 'Arenal'),
('200504', 'PE', '20', '05', '04', 'Colan'),
('200505', 'PE', '20', '05', '05', 'La Huaca'),
('200506', 'PE', '20', '05', '06', 'Tamarindo'),
('200507', 'PE', '20', '05', '07', 'Vichayal'),
('200600', 'PE', '20', '06', '00', 'Sullana '),
('200601', 'PE', '20', '06', '01', 'Sullana'),
('200602', 'PE', '20', '06', '02', 'Bellavista'),
('200603', 'PE', '20', '06', '03', 'Ignacio Escudero'),
('200604', 'PE', '20', '06', '04', 'Lancones'),
('200605', 'PE', '20', '06', '05', 'Marcavelica'),
('200606', 'PE', '20', '06', '06', 'Miguel Checa'),
('200607', 'PE', '20', '06', '07', 'Querecotillo'),
('200608', 'PE', '20', '06', '08', 'Salitral'),
('200700', 'PE', '20', '07', '00', 'Talara '),
('200701', 'PE', '20', '07', '01', 'Pariñas'),
('200702', 'PE', '20', '07', '02', 'El Alto'),
('200703', 'PE', '20', '07', '03', 'La Brea'),
('200704', 'PE', '20', '07', '04', 'Lobitos'),
('200705', 'PE', '20', '07', '05', 'Los Organos'),
('200706', 'PE', '20', '07', '06', 'Mancora'),
('200800', 'PE', '20', '08', '00', 'Sechura '),
('200801', 'PE', '20', '08', '01', 'Sechura'),
('200802', 'PE', '20', '08', '02', 'Bellavista de la Unión'),
('200803', 'PE', '20', '08', '03', 'Bernal'),
('200804', 'PE', '20', '08', '04', 'Cristo Nos Valga'),
('200805', 'PE', '20', '08', '05', 'Vice'),
('200806', 'PE', '20', '08', '06', 'Rinconada Llicuar'),
('210000', 'PE', '21', '00', '00', 'Puno'),
('210100', 'PE', '21', '01', '00', 'Puno '),
('210101', 'PE', '21', '01', '01', 'Puno'),
('210102', 'PE', '21', '01', '02', 'Acora'),
('210103', 'PE', '21', '01', '03', 'Amantani'),
('210104', 'PE', '21', '01', '04', 'Atuncolla'),
('210105', 'PE', '21', '01', '05', 'Capachica'),
('210106', 'PE', '21', '01', '06', 'Chucuito'),
('210107', 'PE', '21', '01', '07', 'Coata'),
('210108', 'PE', '21', '01', '08', 'Huata'),
('210109', 'PE', '21', '01', '09', 'Mañazo'),
('210110', 'PE', '21', '01', '10', 'Paucarcolla'),
('210111', 'PE', '21', '01', '11', 'Pichacani'),
('210112', 'PE', '21', '01', '12', 'Plateria'),
('210113', 'PE', '21', '01', '13', 'San Antonio'),
('210114', 'PE', '21', '01', '14', 'Tiquillaca'),
('210115', 'PE', '21', '01', '15', 'Vilque'),
('210200', 'PE', '21', '02', '00', 'Azángaro '),
('210201', 'PE', '21', '02', '01', 'Azángaro'),
('210202', 'PE', '21', '02', '02', 'Achaya'),
('210203', 'PE', '21', '02', '03', 'Arapa'),
('210204', 'PE', '21', '02', '04', 'Asillo'),
('210205', 'PE', '21', '02', '05', 'Caminaca'),
('210206', 'PE', '21', '02', '06', 'Chupa'),
('210207', 'PE', '21', '02', '07', 'José Domingo Choquehuanca'),
('210208', 'PE', '21', '02', '08', 'Muñani'),
('210209', 'PE', '21', '02', '09', 'Potoni'),
('210210', 'PE', '21', '02', '10', 'Saman'),
('210211', 'PE', '21', '02', '11', 'San Anton'),
('210212', 'PE', '21', '02', '12', 'San José'),
('210213', 'PE', '21', '02', '13', 'San Juan de Salinas'),
('210214', 'PE', '21', '02', '14', 'Santiago de Pupuja'),
('210215', 'PE', '21', '02', '15', 'Tirapata'),
('210300', 'PE', '21', '03', '00', 'Carabaya '),
('210301', 'PE', '21', '03', '01', 'Macusani'),
('210302', 'PE', '21', '03', '02', 'Ajoyani'),
('210303', 'PE', '21', '03', '03', 'Ayapata'),
('210304', 'PE', '21', '03', '04', 'Coasa'),
('210305', 'PE', '21', '03', '05', 'Corani'),
('210306', 'PE', '21', '03', '06', 'Crucero'),
('210307', 'PE', '21', '03', '07', 'Ituata'),
('210308', 'PE', '21', '03', '08', 'Ollachea'),
('210309', 'PE', '21', '03', '09', 'San Gaban'),
('210310', 'PE', '21', '03', '10', 'Usicayos'),
('210400', 'PE', '21', '04', '00', 'Chucuito '),
('210401', 'PE', '21', '04', '01', 'Juli'),
('210402', 'PE', '21', '04', '02', 'Desaguadero'),
('210403', 'PE', '21', '04', '03', 'Huacullani'),
('210404', 'PE', '21', '04', '04', 'Kelluyo'),
('210405', 'PE', '21', '04', '05', 'Pisacoma'),
('210406', 'PE', '21', '04', '06', 'Pomata'),
('210407', 'PE', '21', '04', '07', 'Zepita'),
('210500', 'PE', '21', '05', '00', 'El Collao '),
('210501', 'PE', '21', '05', '01', 'Ilave'),
('210502', 'PE', '21', '05', '02', 'Capazo'),
('210503', 'PE', '21', '05', '03', 'Pilcuyo'),
('210504', 'PE', '21', '05', '04', 'Santa Rosa'),
('210505', 'PE', '21', '05', '05', 'Conduriri'),
('210600', 'PE', '21', '06', '00', 'Huancané '),
('210601', 'PE', '21', '06', '01', 'Huancane'),
('210602', 'PE', '21', '06', '02', 'Cojata'),
('210603', 'PE', '21', '06', '03', 'Huatasani'),
('210604', 'PE', '21', '06', '04', 'Inchupalla'),
('210605', 'PE', '21', '06', '05', 'Pusi'),
('210606', 'PE', '21', '06', '06', 'Rosaspata'),
('210607', 'PE', '21', '06', '07', 'Taraco'),
('210608', 'PE', '21', '06', '08', 'Vilque Chico'),
('210700', 'PE', '21', '07', '00', 'Lampa '),
('210701', 'PE', '21', '07', '01', 'Lampa'),
('210702', 'PE', '21', '07', '02', 'Cabanilla'),
('210703', 'PE', '21', '07', '03', 'Calapuja'),
('210704', 'PE', '21', '07', '04', 'Nicasio'),
('210705', 'PE', '21', '07', '05', 'Ocuviri'),
('210706', 'PE', '21', '07', '06', 'Palca'),
('210707', 'PE', '21', '07', '07', 'Paratia'),
('210708', 'PE', '21', '07', '08', 'Pucara'),
('210709', 'PE', '21', '07', '09', 'Santa Lucia'),
('210710', 'PE', '21', '07', '10', 'Vilavila'),
('210800', 'PE', '21', '08', '00', 'Melgar '),
('210801', 'PE', '21', '08', '01', 'Ayaviri'),
('210802', 'PE', '21', '08', '02', 'Antauta'),
('210803', 'PE', '21', '08', '03', 'Cupi'),
('210804', 'PE', '21', '08', '04', 'Llalli'),
('210805', 'PE', '21', '08', '05', 'Macari'),
('210806', 'PE', '21', '08', '06', 'Nuñoa'),
('210807', 'PE', '21', '08', '07', 'Orurillo'),
('210808', 'PE', '21', '08', '08', 'Santa Rosa'),
('210809', 'PE', '21', '08', '09', 'Umachiri'),
('210900', 'PE', '21', '09', '00', 'Moho '),
('210901', 'PE', '21', '09', '01', 'Moho'),
('210902', 'PE', '21', '09', '02', 'Conima'),
('210903', 'PE', '21', '09', '03', 'Huayrapata'),
('210904', 'PE', '21', '09', '04', 'Tilali'),
('211000', 'PE', '21', '10', '00', 'San Antonio de Putina '),
('211001', 'PE', '21', '10', '01', 'Putina'),
('211002', 'PE', '21', '10', '02', 'Ananea'),
('211003', 'PE', '21', '10', '03', 'Pedro Vilca Apaza'),
('211004', 'PE', '21', '10', '04', 'Quilcapuncu'),
('211005', 'PE', '21', '10', '05', 'Sina'),
('211100', 'PE', '21', '11', '00', 'San Román '),
('211101', 'PE', '21', '11', '01', 'Juliaca'),
('211102', 'PE', '21', '11', '02', 'Cabana'),
('211103', 'PE', '21', '11', '03', 'Cabanillas'),
('211104', 'PE', '21', '11', '04', 'Caracoto'),
('211200', 'PE', '21', '12', '00', 'Sandia '),
('211201', 'PE', '21', '12', '01', 'Sandia'),
('211202', 'PE', '21', '12', '02', 'Cuyocuyo'),
('211203', 'PE', '21', '12', '03', 'Limbani'),
('211204', 'PE', '21', '12', '04', 'Patambuco'),
('211205', 'PE', '21', '12', '05', 'Phara'),
('211206', 'PE', '21', '12', '06', 'Quiaca'),
('211207', 'PE', '21', '12', '07', 'San Juan del Oro'),
('211208', 'PE', '21', '12', '08', 'Yanahuaya'),
('211209', 'PE', '21', '12', '09', 'Alto Inambari'),
('211210', 'PE', '21', '12', '10', 'San Pedro de Putina Punco'),
('211300', 'PE', '21', '13', '00', 'Yunguyo '),
('211301', 'PE', '21', '13', '01', 'Yunguyo'),
('211302', 'PE', '21', '13', '02', 'Anapia'),
('211303', 'PE', '21', '13', '03', 'Copani'),
('211304', 'PE', '21', '13', '04', 'Cuturapi'),
('211305', 'PE', '21', '13', '05', 'Ollaraya'),
('211306', 'PE', '21', '13', '06', 'Tinicachi'),
('211307', 'PE', '21', '13', '07', 'Unicachi'),
('220000', 'PE', '22', '00', '00', 'San Martín'),
('220100', 'PE', '22', '01', '00', 'Moyobamba '),
('220101', 'PE', '22', '01', '01', 'Moyobamba'),
('220102', 'PE', '22', '01', '02', 'Calzada'),
('220103', 'PE', '22', '01', '03', 'Habana'),
('220104', 'PE', '22', '01', '04', 'Jepelacio'),
('220105', 'PE', '22', '01', '05', 'Soritor'),
('220106', 'PE', '22', '01', '06', 'Yantalo'),
('220200', 'PE', '22', '02', '00', 'Bellavista '),
('220201', 'PE', '22', '02', '01', 'Bellavista'),
('220202', 'PE', '22', '02', '02', 'Alto Biavo'),
('220203', 'PE', '22', '02', '03', 'Bajo Biavo'),
('220204', 'PE', '22', '02', '04', 'Huallaga'),
('220205', 'PE', '22', '02', '05', 'San Pablo'),
('220206', 'PE', '22', '02', '06', 'San Rafael'),
('220300', 'PE', '22', '03', '00', 'El Dorado '),
('220301', 'PE', '22', '03', '01', 'San José de Sisa'),
('220302', 'PE', '22', '03', '02', 'Agua Blanca'),
('220303', 'PE', '22', '03', '03', 'San Martín'),
('220304', 'PE', '22', '03', '04', 'Santa Rosa'),
('220305', 'PE', '22', '03', '05', 'Shatoja'),
('220400', 'PE', '22', '04', '00', 'Huallaga '),
('220401', 'PE', '22', '04', '01', 'Saposoa'),
('220402', 'PE', '22', '04', '02', 'Alto Saposoa'),
('220403', 'PE', '22', '04', '03', 'El Eslabón'),
('220404', 'PE', '22', '04', '04', 'Piscoyacu'),
('220405', 'PE', '22', '04', '05', 'Sacanche'),
('220406', 'PE', '22', '04', '06', 'Tingo de Saposoa'),
('220500', 'PE', '22', '05', '00', 'Lamas '),
('220501', 'PE', '22', '05', '01', 'Lamas'),
('220502', 'PE', '22', '05', '02', 'Alonso de Alvarado'),
('220503', 'PE', '22', '05', '03', 'Barranquita'),
('220504', 'PE', '22', '05', '04', 'Caynarachi'),
('220505', 'PE', '22', '05', '05', 'Cuñumbuqui'),
('220506', 'PE', '22', '05', '06', 'Pinto Recodo'),
('220507', 'PE', '22', '05', '07', 'Rumisapa'),
('220508', 'PE', '22', '05', '08', 'San Roque de Cumbaza'),
('220509', 'PE', '22', '05', '09', 'Shanao'),
('220510', 'PE', '22', '05', '10', 'Tabalosos'),
('220511', 'PE', '22', '05', '11', 'Zapatero'),
('220600', 'PE', '22', '06', '00', 'Mariscal Cáceres '),
('220601', 'PE', '22', '06', '01', 'Juanjuí'),
('220602', 'PE', '22', '06', '02', 'Campanilla'),
('220603', 'PE', '22', '06', '03', 'Huicungo'),
('220604', 'PE', '22', '06', '04', 'Pachiza'),
('220605', 'PE', '22', '06', '05', 'Pajarillo'),
('220700', 'PE', '22', '07', '00', 'Picota '),
('220701', 'PE', '22', '07', '01', 'Picota'),
('220702', 'PE', '22', '07', '02', 'Buenos Aires'),
('220703', 'PE', '22', '07', '03', 'Caspisapa'),
('220704', 'PE', '22', '07', '04', 'Pilluana'),
('220705', 'PE', '22', '07', '05', 'Pucacaca'),
('220706', 'PE', '22', '07', '06', 'San Cristóbal'),
('220707', 'PE', '22', '07', '07', 'San Hilarión'),
('220708', 'PE', '22', '07', '08', 'Shamboyacu'),
('220709', 'PE', '22', '07', '09', 'Tingo de Ponasa'),
('220710', 'PE', '22', '07', '10', 'Tres Unidos'),
('220800', 'PE', '22', '08', '00', 'Rioja '),
('220801', 'PE', '22', '08', '01', 'Rioja'),
('220802', 'PE', '22', '08', '02', 'Awajun'),
('220803', 'PE', '22', '08', '03', 'Elías Soplin Vargas'),
('220804', 'PE', '22', '08', '04', 'Nueva Cajamarca'),
('220805', 'PE', '22', '08', '05', 'Pardo Miguel'),
('220806', 'PE', '22', '08', '06', 'Posic'),
('220807', 'PE', '22', '08', '07', 'San Fernando'),
('220808', 'PE', '22', '08', '08', 'Yorongos'),
('220809', 'PE', '22', '08', '09', 'Yuracyacu'),
('220900', 'PE', '22', '09', '00', 'San Martín '),
('220901', 'PE', '22', '09', '01', 'Tarapoto'),
('220902', 'PE', '22', '09', '02', 'Alberto Leveau'),
('220903', 'PE', '22', '09', '03', 'Cacatachi'),
('220904', 'PE', '22', '09', '04', 'Chazuta'),
('220905', 'PE', '22', '09', '05', 'Chipurana'),
('220906', 'PE', '22', '09', '06', 'El Porvenir'),
('220907', 'PE', '22', '09', '07', 'Huimbayoc'),
('220908', 'PE', '22', '09', '08', 'Juan Guerra'),
('220909', 'PE', '22', '09', '09', 'La Banda de Shilcayo'),
('220910', 'PE', '22', '09', '10', 'Morales'),
('220911', 'PE', '22', '09', '11', 'Papaplaya'),
('220912', 'PE', '22', '09', '12', 'San Antonio'),
('220913', 'PE', '22', '09', '13', 'Sauce'),
('220914', 'PE', '22', '09', '14', 'Shapaja'),
('221000', 'PE', '22', '10', '00', 'Tocache '),
('221001', 'PE', '22', '10', '01', 'Tocache'),
('221002', 'PE', '22', '10', '02', 'Nuevo Progreso'),
('221003', 'PE', '22', '10', '03', 'Polvora'),
('221004', 'PE', '22', '10', '04', 'Shunte'),
('221005', 'PE', '22', '10', '05', 'Uchiza'),
('230000', 'PE', '23', '00', '00', 'Tacna'),
('230100', 'PE', '23', '01', '00', 'Tacna '),
('230101', 'PE', '23', '01', '01', 'Tacna'),
('230102', 'PE', '23', '01', '02', 'Alto de la Alianza'),
('230103', 'PE', '23', '01', '03', 'Calana'),
('230104', 'PE', '23', '01', '04', 'Ciudad Nueva'),
('230105', 'PE', '23', '01', '05', 'Inclan'),
('230106', 'PE', '23', '01', '06', 'Pachia'),
('230107', 'PE', '23', '01', '07', 'Palca'),
('230108', 'PE', '23', '01', '08', 'Pocollay'),
('230109', 'PE', '23', '01', '09', 'Sama'),
('230110', 'PE', '23', '01', '10', 'Coronel Gregorio Albarracín Lanchipa'),
('230200', 'PE', '23', '02', '00', 'Candarave '),
('230201', 'PE', '23', '02', '01', 'Candarave'),
('230202', 'PE', '23', '02', '02', 'Cairani'),
('230203', 'PE', '23', '02', '03', 'Camilaca'),
('230204', 'PE', '23', '02', '04', 'Curibaya'),
('230205', 'PE', '23', '02', '05', 'Huanuara'),
('230206', 'PE', '23', '02', '06', 'Quilahuani'),
('230300', 'PE', '23', '03', '00', 'Jorge Basadre '),
('230301', 'PE', '23', '03', '01', 'Locumba'),
('230302', 'PE', '23', '03', '02', 'Ilabaya'),
('230303', 'PE', '23', '03', '03', 'Ite'),
('230400', 'PE', '23', '04', '00', 'Tarata '),
('230401', 'PE', '23', '04', '01', 'Tarata'),
('230402', 'PE', '23', '04', '02', 'Héroes Albarracín'),
('230403', 'PE', '23', '04', '03', 'Estique'),
('230404', 'PE', '23', '04', '04', 'Estique-Pampa'),
('230405', 'PE', '23', '04', '05', 'Sitajara'),
('230406', 'PE', '23', '04', '06', 'Susapaya'),
('230407', 'PE', '23', '04', '07', 'Tarucachi'),
('230408', 'PE', '23', '04', '08', 'Ticaco'),
('240000', 'PE', '24', '00', '00', 'Tumbes'),
('240100', 'PE', '24', '01', '00', 'Tumbes '),
('240101', 'PE', '24', '01', '01', 'Tumbes'),
('240102', 'PE', '24', '01', '02', 'Corrales'),
('240103', 'PE', '24', '01', '03', 'La Cruz'),
('240104', 'PE', '24', '01', '04', 'Pampas de Hospital'),
('240105', 'PE', '24', '01', '05', 'San Jacinto'),
('240106', 'PE', '24', '01', '06', 'San Juan de la Virgen'),
('240200', 'PE', '24', '02', '00', 'Contralmirante Villar '),
('240201', 'PE', '24', '02', '01', 'Zorritos'),
('240202', 'PE', '24', '02', '02', 'Casitas'),
('240203', 'PE', '24', '02', '03', 'Canoas de Punta Sal'),
('240300', 'PE', '24', '03', '00', 'Zarumilla '),
('240301', 'PE', '24', '03', '01', 'Zarumilla'),
('240302', 'PE', '24', '03', '02', 'Aguas Verdes'),
('240303', 'PE', '24', '03', '03', 'Matapalo'),
('240304', 'PE', '24', '03', '04', 'Papayal'),
('250000', 'PE', '25', '00', '00', 'Ucayali'),
('250100', 'PE', '25', '01', '00', 'Coronel Portillo '),
('250101', 'PE', '25', '01', '01', 'Calleria'),
('250102', 'PE', '25', '01', '02', 'Campoverde'),
('250103', 'PE', '25', '01', '03', 'Iparia'),
('250104', 'PE', '25', '01', '04', 'Masisea'),
('250105', 'PE', '25', '01', '05', 'Yarinacocha'),
('250106', 'PE', '25', '01', '06', 'Nueva Requena'),
('250107', 'PE', '25', '01', '07', 'Manantay'),
('250200', 'PE', '25', '02', '00', 'Atalaya '),
('250201', 'PE', '25', '02', '01', 'Raymondi'),
('250202', 'PE', '25', '02', '02', 'Sepahua'),
('250203', 'PE', '25', '02', '03', 'Tahuania'),
('250204', 'PE', '25', '02', '04', 'Yurua'),
('250300', 'PE', '25', '03', '00', 'Padre Abad '),
('250301', 'PE', '25', '03', '01', 'Padre Abad'),
('250302', 'PE', '25', '03', '02', 'Irazola'),
('250303', 'PE', '25', '03', '03', 'Curimana'),
('250304', 'PE', '25', '03', '04', 'Neshuya'),
('250305', 'PE', '25', '03', '05', 'Alexander Von Humboldt'),
('250400', 'PE', '25', '04', '00', 'Purús'),
('250401', 'PE', '25', '04', '01', 'Purus');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ugel`
--

CREATE TABLE `ugel` (
  `idugel` int(11) NOT NULL,
  `descripcion` varchar(250) COLLATE utf8_spanish_ci DEFAULT NULL,
  `abrev` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL,
  `iddepartamento` varchar(6) COLLATE utf8_spanish_ci DEFAULT NULL,
  `idprovincia` varchar(6) COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `ugel`
--

INSERT INTO `ugel` (`idugel`, `descripcion`, `abrev`, `iddepartamento`, `idprovincia`) VALUES
(1, 'Ugel lambayeque', 'Ugel lam', '140000', '140200'),
(2, 'UGEL Tacna', NULL, '230000', NULL),
(3, 'UGEL Jorge Basadre', NULL, '230000', NULL),
(4, 'UGEL Tarata', NULL, '230000', NULL),
(5, 'UGEL Candarave', NULL, '230000', NULL);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `actividades`
--
ALTER TABLE `actividades`
  ADD PRIMARY KEY (`idactividad`);

--
-- Indices de la tabla `actividad_detalle`
--
ALTER TABLE `actividad_detalle`
  ADD PRIMARY KEY (`iddetalle`);

--
-- Indices de la tabla `alumno`
--
ALTER TABLE `alumno`
  ADD PRIMARY KEY (`dni`);

--
-- Indices de la tabla `apps_countries`
--
ALTER TABLE `apps_countries`
  ADD PRIMARY KEY (`id_pais`);

--
-- Indices de la tabla `asistencia`
--
ALTER TABLE `asistencia`
  ADD PRIMARY KEY (`idasistencia`);

--
-- Indices de la tabla `aulasvirtuales`
--
ALTER TABLE `aulasvirtuales`
  ADD PRIMARY KEY (`aulaid`);

--
-- Indices de la tabla `aulavirtualinvitados`
--
ALTER TABLE `aulavirtualinvitados`
  ADD PRIMARY KEY (`idinvitado`);

--
-- Indices de la tabla `biblioteca`
--
ALTER TABLE `biblioteca`
  ADD PRIMARY KEY (`idbiblioteca`);

--
-- Indices de la tabla `bib_autor`
--
ALTER TABLE `bib_autor`
  ADD PRIMARY KEY (`id_autor`),
  ADD KEY `id_pais` (`id_pais`);

--
-- Indices de la tabla `bib_categoria`
--
ALTER TABLE `bib_categoria`
  ADD PRIMARY KEY (`id_categoria`);

--
-- Indices de la tabla `bib_detalle_estudio_editorial`
--
ALTER TABLE `bib_detalle_estudio_editorial`
  ADD PRIMARY KEY (`id_detalle`),
  ADD KEY `id_editorial` (`id_editorial`),
  ADD KEY `id_estudio` (`id_estudio`);

--
-- Indices de la tabla `bib_detalle_subcategoria_estudio`
--
ALTER TABLE `bib_detalle_subcategoria_estudio`
  ADD PRIMARY KEY (`id_detalle`),
  ADD KEY `id_subcategoria` (`id_subcategoria`),
  ADD KEY `id_estudio` (`id_estudio`);

--
-- Indices de la tabla `bib_detalle_usuario_estudio`
--
ALTER TABLE `bib_detalle_usuario_estudio`
  ADD PRIMARY KEY (`id_detalle`);

--
-- Indices de la tabla `bib_det_estudio_autor`
--
ALTER TABLE `bib_det_estudio_autor`
  ADD PRIMARY KEY (`id_detalle`),
  ADD KEY `id_autor` (`id_autor`),
  ADD KEY `id_estudio` (`id_estudio`);

--
-- Indices de la tabla `bib_editorial`
--
ALTER TABLE `bib_editorial`
  ADD PRIMARY KEY (`id_editorial`);

--
-- Indices de la tabla `bib_estudio`
--
ALTER TABLE `bib_estudio`
  ADD PRIMARY KEY (`id_estudio`);

--
-- Indices de la tabla `bib_historial`
--
ALTER TABLE `bib_historial`
  ADD PRIMARY KEY (`id_historial`),
  ADD KEY `id_estudio` (`id_estudio`);

--
-- Indices de la tabla `bib_idioma`
--
ALTER TABLE `bib_idioma`
  ADD PRIMARY KEY (`id_idioma`);

--
-- Indices de la tabla `bib_portada`
--
ALTER TABLE `bib_portada`
  ADD PRIMARY KEY (`id_portada`);

--
-- Indices de la tabla `bib_recursos`
--
ALTER TABLE `bib_recursos`
  ADD PRIMARY KEY (`id_recursos`);

--
-- Indices de la tabla `bib_setting`
--
ALTER TABLE `bib_setting`
  ADD PRIMARY KEY (`id_setting`);

--
-- Indices de la tabla `bib_subcategoria`
--
ALTER TABLE `bib_subcategoria`
  ADD PRIMARY KEY (`id_subcategoria`),
  ADD KEY `id_categoria` (`id_categoria`);

--
-- Indices de la tabla `bib_tipo`
--
ALTER TABLE `bib_tipo`
  ADD PRIMARY KEY (`id_tipo`);

--
-- Indices de la tabla `bib_tipo_registro`
--
ALTER TABLE `bib_tipo_registro`
  ADD PRIMARY KEY (`id_registro`);

--
-- Indices de la tabla `configuracion`
--
ALTER TABLE `configuracion`
  ADD PRIMARY KEY (`nivel`,`ventana`,`atributo`) USING BTREE;

--
-- Indices de la tabla `dcn`
--
ALTER TABLE `dcn`
  ADD PRIMARY KEY (`iddcn`);

--
-- Indices de la tabla `diccionario`
--
ALTER TABLE `diccionario`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `examenes`
--
ALTER TABLE `examenes`
  ADD PRIMARY KEY (`idexamen`);

--
-- Indices de la tabla `examenes_preguntas`
--
ALTER TABLE `examenes_preguntas`
  ADD PRIMARY KEY (`idpregunta`);

--
-- Indices de la tabla `examen_alumno`
--
ALTER TABLE `examen_alumno`
  ADD PRIMARY KEY (`idexaalumno`);

--
-- Indices de la tabla `examen_tipo`
--
ALTER TABLE `examen_tipo`
  ADD PRIMARY KEY (`idtipo`);

--
-- Indices de la tabla `foros`
--
ALTER TABLE `foros`
  ADD PRIMARY KEY (`idforo`);

--
-- Indices de la tabla `grupos`
--
ALTER TABLE `grupos`
  ADD PRIMARY KEY (`idgrupo`);

--
-- Indices de la tabla `grupo_matricula`
--
ALTER TABLE `grupo_matricula`
  ADD PRIMARY KEY (`idgrupo_matricula`);

--
-- Indices de la tabla `herramientas`
--
ALTER TABLE `herramientas`
  ADD PRIMARY KEY (`idtool`);

--
-- Indices de la tabla `historial_sesion`
--
ALTER TABLE `historial_sesion`
  ADD PRIMARY KEY (`idhistorialsesion`);

--
-- Indices de la tabla `local`
--
ALTER TABLE `local`
  ADD PRIMARY KEY (`idlocal`);

--
-- Indices de la tabla `manuales`
--
ALTER TABLE `manuales`
  ADD PRIMARY KEY (`idmanual`);

--
-- Indices de la tabla `manuales_alumno`
--
ALTER TABLE `manuales_alumno`
  ADD PRIMARY KEY (`identrega`);

--
-- Indices de la tabla `matricula_alumno`
--
ALTER TABLE `matricula_alumno`
  ADD PRIMARY KEY (`idmatriculaalumno`);

--
-- Indices de la tabla `mensajes`
--
ALTER TABLE `mensajes`
  ADD PRIMARY KEY (`idmensaje`);

--
-- Indices de la tabla `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`idmenu`);

--
-- Indices de la tabla `metodologia_habilidad`
--
ALTER TABLE `metodologia_habilidad`
  ADD PRIMARY KEY (`idmetodologia`);

--
-- Indices de la tabla `niveles`
--
ALTER TABLE `niveles`
  ADD PRIMARY KEY (`idnivel`);

--
-- Indices de la tabla `notas`
--
ALTER TABLE `notas`
  ADD PRIMARY KEY (`idalumno`,`idactividad`);

--
-- Indices de la tabla `permisos`
--
ALTER TABLE `permisos`
  ADD PRIMARY KEY (`idpermiso`);

--
-- Indices de la tabla `personal`
--
ALTER TABLE `personal`
  ADD PRIMARY KEY (`dni`),
  ADD KEY `nombre` (`nombre`,`ape_paterno`);

--
-- Indices de la tabla `record`
--
ALTER TABLE `record`
  ADD PRIMARY KEY (`idrecord`);

--
-- Indices de la tabla `recursos`
--
ALTER TABLE `recursos`
  ADD PRIMARY KEY (`idrecurso`);

--
-- Indices de la tabla `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`idrol`);

--
-- Indices de la tabla `rub_detalle_respuestas`
--
ALTER TABLE `rub_detalle_respuestas`
  ADD PRIMARY KEY (`id_det_rptas`);

--
-- Indices de la tabla `rub_dimension`
--
ALTER TABLE `rub_dimension`
  ADD PRIMARY KEY (`id_dimension`);

--
-- Indices de la tabla `rub_estandar`
--
ALTER TABLE `rub_estandar`
  ADD PRIMARY KEY (`id_estandar`);

--
-- Indices de la tabla `rub_pregunta`
--
ALTER TABLE `rub_pregunta`
  ADD PRIMARY KEY (`id_pregunta`);

--
-- Indices de la tabla `rub_respuestas`
--
ALTER TABLE `rub_respuestas`
  ADD PRIMARY KEY (`id_respuestas`);

--
-- Indices de la tabla `rub_rubrica`
--
ALTER TABLE `rub_rubrica`
  ADD PRIMARY KEY (`id_rubrica`);

--
-- Indices de la tabla `sis_configuracion`
--
ALTER TABLE `sis_configuracion`
  ADD PRIMARY KEY (`config`);

--
-- Indices de la tabla `tarea`
--
ALTER TABLE `tarea`
  ADD PRIMARY KEY (`idtarea`);

--
-- Indices de la tabla `tarea_archivos`
--
ALTER TABLE `tarea_archivos`
  ADD PRIMARY KEY (`idtarea_archivos`);

--
-- Indices de la tabla `tarea_asignacion`
--
ALTER TABLE `tarea_asignacion`
  ADD PRIMARY KEY (`idtarea_asignacion`);

--
-- Indices de la tabla `tarea_asignacion_alumno`
--
ALTER TABLE `tarea_asignacion_alumno`
  ADD PRIMARY KEY (`iddetalle`);

--
-- Indices de la tabla `tarea_respuesta`
--
ALTER TABLE `tarea_respuesta`
  ADD PRIMARY KEY (`idtarea_respuesta`);

--
-- Indices de la tabla `ubigeo`
--
ALTER TABLE `ubigeo`
  ADD PRIMARY KEY (`id_ubigeo`);

--
-- Indices de la tabla `ugel`
--
ALTER TABLE `ugel`
  ADD PRIMARY KEY (`idugel`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `apps_countries`
--
ALTER TABLE `apps_countries`
  MODIFY `id_pais` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=246;
--
-- AUTO_INCREMENT de la tabla `bib_autor`
--
ALTER TABLE `bib_autor`
  MODIFY `id_autor` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `bib_categoria`
--
ALTER TABLE `bib_categoria`
  MODIFY `id_categoria` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `bib_detalle_estudio_editorial`
--
ALTER TABLE `bib_detalle_estudio_editorial`
  MODIFY `id_detalle` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `bib_detalle_subcategoria_estudio`
--
ALTER TABLE `bib_detalle_subcategoria_estudio`
  MODIFY `id_detalle` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `bib_detalle_usuario_estudio`
--
ALTER TABLE `bib_detalle_usuario_estudio`
  MODIFY `id_detalle` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `bib_det_estudio_autor`
--
ALTER TABLE `bib_det_estudio_autor`
  MODIFY `id_detalle` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `bib_editorial`
--
ALTER TABLE `bib_editorial`
  MODIFY `id_editorial` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `bib_estudio`
--
ALTER TABLE `bib_estudio`
  MODIFY `id_estudio` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `bib_historial`
--
ALTER TABLE `bib_historial`
  MODIFY `id_historial` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `bib_idioma`
--
ALTER TABLE `bib_idioma`
  MODIFY `id_idioma` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `bib_recursos`
--
ALTER TABLE `bib_recursos`
  MODIFY `id_recursos` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `bib_setting`
--
ALTER TABLE `bib_setting`
  MODIFY `id_setting` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `bib_subcategoria`
--
ALTER TABLE `bib_subcategoria`
  MODIFY `id_subcategoria` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `bib_tipo`
--
ALTER TABLE `bib_tipo`
  MODIFY `id_tipo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT de la tabla `bib_tipo_registro`
--
ALTER TABLE `bib_tipo_registro`
  MODIFY `id_registro` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `diccionario`
--
ALTER TABLE `diccionario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `rub_detalle_respuestas`
--
ALTER TABLE `rub_detalle_respuestas`
  MODIFY `id_det_rptas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=221;
--
-- AUTO_INCREMENT de la tabla `rub_dimension`
--
ALTER TABLE `rub_dimension`
  MODIFY `id_dimension` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=593;
--
-- AUTO_INCREMENT de la tabla `rub_estandar`
--
ALTER TABLE `rub_estandar`
  MODIFY `id_estandar` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=694;
--
-- AUTO_INCREMENT de la tabla `rub_pregunta`
--
ALTER TABLE `rub_pregunta`
  MODIFY `id_pregunta` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1345;
--
-- AUTO_INCREMENT de la tabla `rub_respuestas`
--
ALTER TABLE `rub_respuestas`
  MODIFY `id_respuestas` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT de la tabla `rub_rubrica`
--
ALTER TABLE `rub_rubrica`
  MODIFY `id_rubrica` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;
--
-- AUTO_INCREMENT de la tabla `tarea_asignacion`
--
ALTER TABLE `tarea_asignacion`
  MODIFY `idtarea_asignacion` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
