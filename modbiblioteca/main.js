// Creación del módulo
var angularRoutingApp = angular.module('angularRoutingApp', ['ngRoute']);
// Configuración de las rutas
angularRoutingApp.config(function($routeProvider) {

	$routeProvider
		.when('/', {
			templateUrl	: 'pages/inicio.html',
			controller 	: 'mainController'
		})
		.when('/buscar', {
			templateUrl	: 'pages/home.html',
			controller 	: 'mainController'
		})
		.when('/libros', {
			templateUrl	: 'app/libros/views/index-principal.html',
			controller 	: 'LibroController'
		})
		.when('/libros/ver/:id', {
			templateUrl	: 'app/libros/views/index.html',
			controller 	: 'LibroVerController'
		})
		.when('/libros/verpdf/:id', {
			templateUrl	: 'app/libros/views/verPdf.html',
			controller 	: 'LibroVerController'
		})
		.when('/audio', {
			templateUrl	: 'app/audio/views/index-principal.html',
			controller 	: 'AudioController'
		})
		.when('/audio/ver/:id', {
			templateUrl	: 'app/audio/views/index.html',
			controller 	: 'AudioController'
		})
		.when('/video', {
			templateUrl	: 'app/video/views/index-principal.html',
			controller 	: 'VideoController'
		})
		.when('/video/ver/:id', {
			templateUrl	: 'app/video/views/index.html',
			controller 	: 'VideoController'
		})
		.when('/biblioteca', {
			templateUrl	: 'app/biblioteca/views/index-principal.html',
			controller 	: 'BibliotecaController'
		})
		.when('/biblioteca/ver/:id', {
			templateUrl	: 'app/biblioteca/views/index.html',
			controller 	: 'BibliotecaController'
		})
		.when('/museo', {
			templateUrl	: 'app/museo/views/index-principal.html',
			controller 	: 'MuseoController'
		})
		.when('/museo/ver/:id', {
			templateUrl	: 'app/museo/views/index.html',
			controller 	: 'MuseoController'
		})
        .when('/favoritos', {
			templateUrl	: 'pages/favoritos.html',
			controller 	: 'FavoritoController'
		})
        .when('/notas/agregar', {
			templateUrl	: 'app/notas/views/index-principal.html',
			controller 	: 'NotasController'
		})
        .when('/notas', {
			templateUrl	: 'app/notas/views/index-principal.html',
			controller 	: 'NotasController'
		})
		.otherwise({
			redirectTo: '/'
		});
});


angularRoutingApp.controller('mainController', function($scope,$http,$rootScope,$window,$anchorScroll,$q) {
	console.log("Hola mainController");
	$scope.categorias;
	$scope.subcategorias;
	$scope.autor;
	$scope.buscar;
	$scope.message = 'Hola, Mundo!!';

	$http.get('../biblioteca/getVarsGlobales')
		.success(function(resp){
			if(resp.code=='ok'){
				$rootScope._sysUsuario_ = resp.data.usuario;
				$rootScope._sysUrlStatic_=resp.data.url_static;
				$rootScope._sysUrlBase_=resp.data.url_base;
				if(resp.data.usuario.length==0){
					$window.location.href = resp.data.url_base;
				}
			} else {
				//$window.location.href = '../';
			}
		}).error(function(){
			console.log('error al cargar datos');
		});

	$scope.resultado = function(){
		var databuscar={
					'nombre':$scope.buscar,
				};
		$http.post('../bib_estudio/xLibro',databuscar)
			.success(function(resp){
				if(resp.code=='ok'){
					$scope.libro=resp.data;
					document.getElementById('nuevoante').style.display = 'none';
					document.getElementById('nuevo25').style.display = 'block';
					//$location.path('/resultado');
				} else {
					console.log('error al cargar datos: '+resp.msj);
				}
			}).error(function(){
				console.log('error al cargar datos');
			});
	}

	$rootScope.historial={
		agregar: function(data){
			var deferred = $q.defer();
			$http.post($rootScope._sysUrlBase_+'/bib_historial/xAgregar', data)
				.success(function(resp){
					if(resp.code=='ok'){
						$rootScope.historial_usu=resp.data;
					}else{
						console.log('hay error rootScope.historial.post: '+resp.msj);
					}
				}).error(function(resp){
					console.log('error al cargar datos: '+resp.msj);
				});
			return deferred.promise;
		},
		listar:function(params=''){
			var deferred = $q.defer();
			$http.get($rootScope._sysUrlBase_+'/bib_historial/xListar/'+params)
			.success(function(resp){
				if(resp.code=='ok'){
					$rootScope.historial_usu=resp.data;
					console.log($rootScope.historial_usu);
				} else {
					console.log('error al cargar datos historial: '+resp.msj);
				}
			}).error(function(){
				console.log('error al cargar datos historial');
			});
			return deferred.promise;
		}
	};
	//========================
		
	$http.get('../bib_categoria/xCategorias')
		.success(function(resp){
			if(resp.code=='ok'){
				$scope.categorias=resp.data;
				//console.log($scope.categorias);
			} else {
				console.log('error al cargar datos: '+resp.msj);
			}
		}).error(function(){
			console.log('error al cargar datos');
	});

	$http.get('../bib_subcategoria/xSubCategorias')
		.success(function(resp){
			if(resp.code=='ok'){
				$scope.subcategorias=resp.data;
				//console.log($scope.subcategorias);
			} else {
				console.log('error al cargar datos: '+resp.msj);
			}
		}).error(function(){
			console.log('error al cargar datos');
	});

	$http.get('../bib_autor/xAutor')
		.success(function(resp){
			if(resp.code=='ok'){
				$scope.autor=resp.data;
				//console.log($scope.autor);
			} else {
				console.log('error al cargar datos: '+resp.msj);
			}
		}).error(function(){
			console.log('error al cargar datos');
	});

	$scope.aceptar = function(){
			var data={
				'codigo':'',
				'nombre':'',
				'precio': '',
				'editorial': '',
				'edicion':'',
				'fec_publicacion':'',
				'foto': '',
				'archivo': '',
				'id_tipo':'',
				'id_autor':'',
				'condicion':'',
				'idioma': '',
			};
			$http.post($rootScope._sysUrlBase_+'/bib_libro/xBusca/', data)
				.success(function(resp){
					if(resp.code=='ok'){
						//console.log(resp.data);
						$scope.bib_libro=resp.data;
					}else{
						console.log('hay error'+resp.msj);
					}
				}).error(function(){
					console.log('error al cargar datos: '+resp.msj);
				});
			console.log($scope.buscar);
	}

	$scope.cancelar = function(){
		$scope.buscar={};
	}

	});

angularRoutingApp.controller('LibroController', function($scope,$rootScope,$http,$routeParams,$location) {
	$scope.message= 'libro';
	var id=$routeParams.id;
	$scope.tipo;
	$scope.estudio;
	$scope.tipo=0;
	$scope.selectEstudios=[];
	$scope.bib_estudio=[];

	$scope.cargarLibros = function (){
		$http.get('../bib_tipo/xTipo')
		.success(function(resp){
			if(resp.code=='ok'){
				$scope.tipo=resp.data;
				//console.log($scope.tipo);
			} else {
				console.log('error al cargar datos: '+resp.msj);
			}
		}).error(function(){
			console.log('error al cargar datos');
		});
	}

	$scope.init = function (){
		$scope.cargarLibros();
	}
	$scope.init();

	if (id!=undefined) {
		$http.get('../bib_estudio/xGetEstudio/?id='+id)
		.success(function(resp){
			if(resp.code=='ok'){
				$scope.bib_estudio=resp.data;
			}else{
				console.log('hay error'+resp.msj);
			}
		}).error(function(){
			console.log('error al cargar datos: '+resp.msj);
		});
	}
    
	$http.get('../bib_estudio/xEstudio')
		.success(function(resp){
			if(resp.code=='ok'){
				$scope.estudio=resp.data;
			} else {
				console.log('error al cargar datos: '+resp.msj);
			}
		}).error(function(){
			console.log('error al cargar datos');
		});

	$scope.selecionarTipo = function(id){
		$scope.tipo=id;
		if($scope.tipo===0){
			$scope.selectEstudios=[];
			$scope.cargarLibros();
		}else{
			if ($scope.estudio!=undefined) {
				for (var i = $scope.estudio.length - 1; i >= 0; i--) {
					if($scope.estudio[i].id_tipo ===$scope.tipo){
						$scope.selectEstudios.push($scope.estudio[i]);
					}
				}
			}
		}
	}
    
    $scope.buscar=function(buscar){  
        $http.get('../bib_libro/xbuscar/?idlibro='+id)
		.success(function(resp){
			buscar=$scope.buscar.palabra;
            console.log(id);
            console.log(resp);
			if(resp.code=='ok'){
				$scope.bib_libro=resp.data;
			} else {
				console.log('error al borrar datos Libro: '+resp.msj);
			}
		}).error(function(){
			console.log('error al borrar datos Libro');
		});  
    }

	$scope.verLibro = function(id){
		$location.path('/libros/ver/'+id);		
	};
	
	$scope.agregarFavorito=function(id){  
		var data={
			'id_estudio': id,
		};
		$http.post('../bib_estudio/xAgregar?id_estudio='+id, data)
			.success(function(resp){
				if(resp.code=='ok'){
					$scope.estudio=resp.data;
					alert('El libro fue agregado a favoritos');
				}else{
					console.log('hay error agregarFavorito: '+resp.msj);
				}
			}).error(function(resp){
				console.log('error al cargar datos: '+resp.msj);
			});
	}

	$scope.guardar = function(){
		console.log($scope.nombre);
	}

	//==============================PAGINACION=================================
	 $scope.pager = {};
     $scope.pages = [];
	 $scope.setPage= function(page){

            if (page < 1 || page > $scope.pager.totalPages) {
                return;
            }
            // get pager object from service
             $scope.GetPager($scope.selectEstudios.length, page);
             $scope.pager.pages=$scope.pages;

            // get current page of items
           $scope.items = $scope.selectEstudios.slice($scope.pager.startIndex, $scope.pager.endIndex + 1);
           //$scope.items = $scope.selectLibros.slice(1, 10);        
           
        }

        // service implementation
        $scope.GetPager=function(totalItems, currentPage, pageSize) {
            // default to first page
            currentPage = currentPage || 1;

            // default page size is 10
            pageSize = pageSize || 10;

            // calculate total pages
            var totalPages = Math.ceil(totalItems / pageSize);

            var startPage, endPage;
            if (totalPages <= 10) {
                // less than 10 total pages so show all
                startPage = 1;
                endPage = totalPages;
            } else {
                // more than 10 total pages so calculate start and end pages
                if (currentPage <= 6) {
                    startPage = 1;
                    endPage = 10;
                } else if (currentPage + 4 >= totalPages) {
                    startPage = totalPages - 9;
                    endPage = totalPages;
                } else {
                    startPage = currentPage - 5;
                    endPage = currentPage + 4;
                }
            }
            
            // calculate start and end item indexes
            var startIndex = (currentPage - 1) * pageSize;
            var endIndex = Math.min(startIndex + pageSize - 1, totalItems - 1);

            // create an array of pages to ng-repeat in the pager control
            //var pages = _.range(startPage, endPage + 1);
            var max = (totalItems / 10)+1 ;
            for (var i=startIndex+1; i<(max); i++) {
		      $scope.pages.push(i);
		    }

            $scope.pager.currentPage = currentPage;
            $scope.pager.totalPages = totalPages;

            $scope.pager.startIndex = startIndex;
            $scope.pager.endIndex = endIndex;
        }

	//=========================================================================

});

angularRoutingApp.controller('LibroVerController', function($scope,$rootScope,$http,$routeParams,$location){

	var id=$routeParams.id;
	$scope.bib_historial=[];
	$scope.historial={};

	$http.get('../bib_estudio/xGetEstudio/?id='+id)
		.success(function(resp){
			if(resp.code=='ok'){
				$scope.bib_estudio=resp.data;
			} else {
				console.log('error al cargar datos: '+resp.msj);
			}
		}).error(function(){
			console.log('error al cargar datos');
		});

	$http.get('../bib_historial/xListar/?id_estudio='+id+'&descripcion=ver')
		.success(function(resp){
			if(resp.code=='ok'){
				console.log(resp);
				if(resp.data.length!==0) $scope.historial['ver']= resp.data[0].id_historial ;
			} else {
				console.log('error al cargar datos historial: '+resp.msj);
			}
		}).error(function(){
			console.log('error al cargar datos historial');
		});	

	var data={
		'id_historial' : $scope.historial.ver,
		'id_estudio':id,
		'descripcion':'ver',
	};
	$http.post('../bib_historial/xAgregar', data)
		.success(function(resp){
			if(resp.code=='ok'){
				$scope.historial={'ver':resp.data};
			}else{
				console.log('hay error rootScope.historial.post::ver: '+resp.msj);
			}
		}).error(function(resp){
			console.log('error al cargar datos: '+resp.msj);
		});

		$scope.descargar=function(id,cantidad_descarga,historial_cantidad){
			historial_cantidad=historial_cantidad||1;
			console.log(cantidad_descarga);
			console.log(historial_cantidad);

			$http.get('../bib_historial/xListar/?id_estudio='+id+'&descripcion=descargar')
			.success(function(resp){
				if(resp.code=='ok'){
					//console.log('-------NUEVA DESCARGA----');
					//console.log(resp.data);
					if(resp.data.length!==0) {
						$scope.historial['descargar']= resp.data[0].id_historial;
						$scope.historial['cant']= resp.data[0].cantidad;

						//console.log($scope.historial.cant);
					}
				} else {
					console.log('error al cargar datos historial: '+resp.msj);
				}
			}).error(function(){
				console.log('error al cargar datos historial');
			});	

			if(historial_cantidad<cantidad_descarga){
				//console.log('entre cantidad');
				/*...descargar..*/
				var a=document.createElement('a');
				a.href='../static/libreria/pdf/'+$scope.bib_estudio.archivo;
				a.download=$scope.bib_estudio.archivo;
				a.target='_blank';
				a.click();
				console.log('dfghjk')
				console.log (a);
				var data={
					'id_historial' : $scope.historial.descargar,
					'id_estudio':id,
					'descripcion':'descargar',
				};

				$http.post('../bib_historial/xAgregar', data)
				.success(function(resp){
					if(resp.code=='ok'){
						$scope.historial={'descargar':resp.data};
					}else{
						console.log('hay error rootScope.historial.post::descargar: '+resp.msj);
					}
				}).error(function(resp){
					console.log('error al cargar datos: '+resp.msj);
				});
			}else{
				alert("Usted alcanzó su máximo de descargas permitidas");
			}

		}
		$scope.banderaVerPDF=false;
		$scope.verPDF=function(id){
			//$location.path('/libros/verpdf/'+id);
			var iframe=document.createElement('iframe');
			var selector = document.getElementById('contenedor_frame')
			//var iframe=document.createElement('iframe');
				//iframe.src='../static/libreria/pdf/'+$scope.bib_estudio.archivo;
				iframe.target='_blank';
				iframe.width='800px';
				iframe.height='500px';
				iframe.margin='auto';
				iframe.setAttribute("src", '../static/libreria/pdf/viewer.html?file='+$scope.bib_estudio.archivo);
				//iframe.setAttribute("src", '../static/libreria/pdf/'+$scope.bib_estudio.archivo);
				//iframe.setAttribute("src", '../static/libreria/pdf/'+$scope.bib_estudio.archivo);
				selector.appendChild(iframe);

				console.log('dfghjk');
				console.log (iframe.setAttribute);
				$scope.banderaVerPDF=true;
		}
	
});

angularRoutingApp.controller('AudioController', function($scope,$rootScope,$http,$routeParams,$location) {
	$scope.message= 'Audio';
	$scope.nombre;
   
	var id=$routeParams.id;
	$scope.tipo;
	$scope.multimedia;
	$scope.tipo=0;
	$scope.selectMultimedia=[];
	$scope.bib_Multimedia=[];
	$scope.cargarMultimedia = function (){
		$http.get('../bib_tipo/xTipo')
		.success(function(resp){
			if(resp.code=='ok'){
				$scope.tipo=resp.data;
				console.log($scope.tipo);
			} else {
				console.log('error al cargar datos: '+resp.msj);
			}
		}).error(function(){
			console.log('error al cargar datos');
		});
	}

	$scope.init = function (){
		$scope.cargarMultimedia();
	}
	$scope.init();

	if (id!=undefined) {
		$http.get('../bib_multimedia/xGetMultimedia/?id='+id)
		.success(function(resp){
			if(resp.code=='ok'){
				$scope.bib_multimedia=resp.data;
			}else{
				console.log('hay error'+resp.msj);
			}
		}).error(function(){
			console.log('error al cargar datos: '+resp.msj);
		});
	}

	$http.get('../bib_multimedia/xMultimedia')
		.success(function(resp){
			if(resp.code=='ok'){
				$scope.multimedia=resp.data;
				console.log($scope.multimedia);
			} else {
				console.log('error al cargar datos: '+resp.msj);
			}
		}).error(function(){
			console.log('error al cargar datos');
		});

	$scope.selecionarTipo = function(id){
		console.log(id);
		$scope.tipo=id;
		if($scope.tipo===0){
			$scope.selectMultimedia=[];
			$scope.cargarMultimedia();
		}else{
			if ($scope.multimedia!=undefined) {
			for (var i = $scope.multimedia.length - 1; i >= 0; i--) {
				if($scope.multimedia[i].idtipo ===$scope.tipo){
					$scope.selectMultimedias.push($scope.multimedia[i]);
				}
			}
		}
		}
	}
	$scope.verMultimedia = function(id){
		console.log("Entre + "+id);
		$location.path('/audio/ver/'+id);	
	}	
	$scope.guardar = function(){
		console.log($scope.nombre);

	}    
});

angularRoutingApp.controller('VideoController', function($scope,$rootScope,$http,$routeParams,$location) {
	$scope.message= 'Video';
	$scope.nombre;
   
	var id=$routeParams.id;
	$scope.tipo;
	$scope.multimedia;
	$scope.tipo=0;
	$scope.selectMultimedia=[];
	$scope.bib_Multimedia=[];
	$scope.cargarMultimedia = function (){
		$http.get('../bib_tipo/xTipo')
		.success(function(resp){
			if(resp.code=='ok'){
				$scope.tipo=resp.data;
				console.log($scope.tipo);
			} else {
				console.log('error al cargar datos: '+resp.msj);
			}
		}).error(function(){
			console.log('error al cargar datos');
		});
	}

	$scope.init = function (){
		$scope.cargarMultimedia();
	}
	$scope.init();

	if (id!=undefined) {
		$http.get('../bib_multimedia/xGetMultimedia/?id='+id)
		.success(function(resp){
			if(resp.code=='ok'){
				$scope.bib_multimedia=resp.data;
			}else{
				console.log('hay error'+resp.msj);
			}
		}).error(function(){
			console.log('error al cargar datos: '+resp.msj);
		});
	}

	$http.get('../bib_multimedia/xMultimedia')
		.success(function(resp){
			if(resp.code=='ok'){
				$scope.multimedia=resp.data;
				console.log($scope.multimedia);
			} else {
				console.log('error al cargar datos: '+resp.msj);
			}
		}).error(function(){
			console.log('error al cargar datos');
		});

	$scope.selecionarTipo = function(id){
		console.log(id);
		$scope.tipo=id;
		if($scope.tipo===0){
			$scope.selectMultimedia=[];
			$scope.cargarMultimedia();
		}else{
			if ($scope.multimedia!=undefined) {
			for (var i = $scope.multimedia.length - 1; i >= 0; i--) {
				if($scope.multimedia[i].idtipo ===$scope.tipo){
					$scope.selectMultimedias.push($scope.multimedia[i]);
				}
			}
		}
		}
	}

	$scope.verMultimedia = function(id){
		console.log("Entre + "+id);
		$location.path('/video/ver/'+id);	
	}
	  
				
	$scope.guardar = function(){
		console.log($scope.nombre);
	}
});

angularRoutingApp.controller('BibliotecaController', function($scope,$rootScope,$http,$routeParams,$location) {
	$scope.message= 'Biblioteca';
	$scope.nombre;
   
	var id=$routeParams.id;
		$scope.tipo;
		$scope.video;
		$scope.tipo=0;
		$scope.selectServicioss=[];
		$scope.bib_servicios=[];
		$scope.cargarServicios = function (){
			$http.get('../bib_tipo/xTipo')
			.success(function(resp){
				if(resp.code=='ok'){
					$scope.tipo=resp.data;
					console.log($scope.tipo);
				} else {
					console.log('error al cargar datos: '+resp.msj);
				}
			}).error(function(){
				console.log('error al cargar datos');
			});
		}

		$scope.init = function (){
			$scope.cargarServicios();
		}
		$scope.init();

		if (id!=undefined) {
			$http.get('../bib_servicios/xGetServicios/?id='+id)
			.success(function(resp){
				if(resp.code=='ok'){
					$scope.bib_servicios=resp.data;
				}else{
					console.log('hay error'+resp.msj);
				}
			}).error(function(){
				console.log('error al cargar datos: '+resp.msj);
			});
		}

		$http.get('../bib_servicios/xServicios')
			.success(function(resp){
				if(resp.code=='ok'){
					$scope.servicios=resp.data;
					console.log($scope.servicios);
				} else {
					console.log('error al cargar datos: '+resp.msj);
				}
			}).error(function(){
				console.log('error al cargar datos');
			});

		$scope.selecionarTipo = function(id){
			console.log(id);
			$scope.tipo=id;
			if($scope.tipo===0){
				$scope.selectServicios=[];
				$scope.cargarServicios();
			}else{
				if ($scope.servicios!=undefined) {
				for (var i = $scope.servicios.length - 1; i >= 0; i--) {
					if($scope.servicios[i].idtipo ===$scope.tipo){
						$scope.selectServicios.push($scope.servicios[i]);
					}
				}
			}
			}


		}
		$scope.verServicios = function(id){
			console.log("Entre + "+id);
			$location.path('/biblioteca/ver/'+id);	
		}
		 
					
		$scope.guardar = function(){
			console.log($scope.nombre);

		}
});

angularRoutingApp.controller('MuseoController', function($scope,$rootScope,$http,$routeParams,$location) {
	$scope.message= 'Museo';
	$scope.nombre;
   
	var id=$routeParams.id;
	$scope.tipo;
	$scope.video;
	$scope.tipo=0;
	$scope.selectServicioss=[];
	$scope.bib_servicios=[];
	$scope.cargarServicios = function (){
		$http.get('../bib_tipo/xTipo')
		.success(function(resp){
			if(resp.code=='ok'){
				$scope.tipo=resp.data;
				console.log($scope.tipo);
			} else {
				console.log('error al cargar datos: '+resp.msj);
			}
		}).error(function(){
			console.log('error al cargar datos');
		});
	}

	$scope.init = function (){
		$scope.cargarServicios();
	}
	$scope.init();

	if (id!=undefined) {
		$http.get('../bib_servicios/xGetServicios/?id='+id)
		.success(function(resp){
			if(resp.code=='ok'){
				$scope.bib_servicios=resp.data;
			}else{
				console.log('hay error'+resp.msj);
			}
		}).error(function(){
			console.log('error al cargar datos: '+resp.msj);
		});
	}

	$http.get('../bib_servicios/xServicios')
		.success(function(resp){
			if(resp.code=='ok'){
				$scope.servicios=resp.data;
				console.log($scope.servicios);
			} else {
				console.log('error al cargar datos: '+resp.msj);
			}
		}).error(function(){
			console.log('error al cargar datos');
		});

	$scope.selecionarTipo = function(id){
		console.log(id);
		$scope.tipo=id;
		if($scope.tipo===0){
			$scope.selectServicios=[];
			$scope.cargarServicios();
		}else{
			if ($scope.servicios!=undefined) {
			for (var i = $scope.servicios.length - 1; i >= 0; i--) {
				if($scope.servicios[i].idtipo ===$scope.tipo){
					$scope.selectServicios.push($scope.servicios[i]);
				}
			}
		}
		}
	}
	$scope.verServicios = function(id){
		console.log("Entre + "+id);
		$location.path('/museo/ver/'+id);	
	}
	 
				
	$scope.guardar = function(){
		console.log($scope.nombre);

	}
});

angularRoutingApp.controller('NotasController',function($scope,$rootScope,$http,$routeParams,$location) {
    $scope.bib_recursos=[];
    
    $http.get('../bib_recursos/xRecursos')
		.success(function(resp){
			if(resp.code=='ok'){
				$scope.recursos=resp.data;
				console.log($scope.recursos);
			} else {
				console.log('error al cargar datos: '+resp.msj);
			}
		}).error(function(){
			console.log('error al cargar datos');
		});
   
    $scope.eliminar=function($dni,$orden){  
        $http.get('../bib_recursos/xEliminarRecursos/?dni='+$dni+'&orden='+$orden)
		.success(function(resp){
            console.log(resp);
			if(resp.code=='ok'){
				$scope.recursos=resp.data;
				console.log($scope.recursos);
			} else {
				console.log('error al cargar datos: '+resp.msj);
			}
		}).error(function(){
			console.log('error al cargar datos');
		});
        
    }
       
    $scope.guardar = function(){
		console.log($scope.nombre);   
    }
});

angularRoutingApp.controller('FavoritoController', function($scope,$rootScope,$http,$routeParams,$location) {
	$scope.dni;
	var id=$routeParams.id;

	$http.get('../bib_detalle_usuario_estudio/xFavoritos')
		.success(function(resp){
			if(resp.code=='ok'){
				$scope.estudioFav=resp.data;
				console.log($scope.estudioFav);
			} else {
				console.log('error al cargar datos: '+resp.msj);
			}
		}).error(function(){
			console.log('error al cargar datos');
	});

	$scope.verEstudio = function(id){
		$location.path('/libros/ver/'+id);		
	};
});
