<?php
require_once("class/class_mantenimiento.php");

$obj = new Mantenimiento();
$id_dimension = $_POST["id_dimension"];
$dimension = $obj->listar_id_dimension($id_dimension);
$estandar = $obj->listar_estandares($id_dimension);

?>
<div class = 'row'>
<input type = 'hidden' id= 'id_dimension' value = '<?php echo $id_dimension;?>'>
<div class = 'col-lg-12'>
	<div class = 'table-responsive'>
		<table class = 'table table-striped table-condensed table-hover display' id='tblEstandares'>
			<thead>
				<tr style = 'background-color:#428bca; color: #fff;'>
					<th>N°</th>
					<th>Nombre Estándar</th>
					<th width = '20%'>Ver Preguntas</th>
				</tr>
			</thead>
			<tbody>
			<?php
			if(!empty($estandar)){
				$cont = 0;
				foreach($estandar as $valor) {
					$cont++;
			?>
				<tr id_estandar="<?php echo $valor["id_estandar"] ?>" class="estandar">
						<td><?php echo $cont ?></td>
						<td>
							<div class = 'ver' tecla_esc='no' valor_estatico='<?php echo $valor["nombre"] ?>'><?php echo $valor["nombre"] ?></div>
						</td>
						<td style='position: relative;' class="text-center">
							<button class = 'btn btn-info btn-circle mostrar_preguntas'><i class = 'icon-chevron-down'></i></button>
							<!--
							<button class = 'btn btn-danger btn-circle eliminar_estandar'><i class = 'icon-trash'></i>					
							</button>
							-->
						</td>
					</tr>
			<?php } 
			} else { ?>
				<tr><td colspan="3">No se encontraron estándares.</td></tr>
			<?php } ?>
			</tbody>
		</table>
	</div>
</div>
</div>

<script>
	//$("textarea").autoResize();
/*
	$(".container-fluid").on("click",".cambio_texto1",function(){
		var valor_estatico = $(this).attr("valor_estatico");
		$(this).html("<textarea style='width:100%;' class = 'form-control cambio1'>"+valor_estatico+"</textarea>");
		$(".cambio1").focus();
		$(this).parents("#tblEstandares").find(".cambio_texto1").removeClass("cambio_texto1");
	});

	$(".modal-footer").on("click",".agregar_estandar",function(){
		var total_filas = parseInt($("#tblEstandares tbody tr").length) + 1;
		var id_dimension = $("#id_dimension").val();
		$("#tblEstandares").append("<tr class = 'ultima_fila'>"+
									"<td>"+total_filas+"</td>"+
									"<td><textarea class = 'form-control ultimo_valor'></textarea></td>"+
									"<td>"+
										"<button class = 'btn btn-default btn-circle cancelar_add'><i class = 'icon-remove'></i></button>"+
									"</td>"+
							"</tr>");
		$(".ultimo_valor").focus();
		
		$(".agregar_estandar").replaceWith("<button class = 'btn btn-primary guardar_estandar'>Guardar</button>");
	});

	$(".modal-body").on("click",".cancelar_add",function(){
		$(".ultima_fila").remove();
		$(".guardar_estandar").replaceWith("<button class = 'btn btn-success agregar_estandar'>Agregar</button>");
	});

	$(".modal-footer").on("click",".guardar_estandar",function(){
		
		var nombre = $(".ultima_fila").find(".ultimo_valor").val();
		var id_dimension = $("#id_dimension").val();
		var total_filas = parseInt($("#tblEstandares tbody tr").length);
		
		if(nombre.length==0)
		{
			$(".ultimo_valor").replaceWith("<div class='form-group has-feedback' style = 'margin-bottom: 0px'><textarea class='form-control ultimo_valor' style = 'width:100%;' placeholder = 'Ingrese un nombre'/><i class='glyphicon glyphicon-exclamation-sign form-control-feedback red'></i></textarea></div>");
			$(".ultimo_valor").addClass("borde_rojo");
		}
		else
		{
			$.ajax({
    		url: "grabar_ajax.php",
			type: "POST",
			dataType: "json",
			data: {
				nombre: nombre,
				id_dimension: id_dimension,
				valor: "crear_estandar"
				}
		    })

		    .done(function(res){
				if(res.codigo=="OK")
				{
					$(".ultima_fila").html("<td>"+total_filas+"</td>"+
											"<td><div class = 'ver cambio_texto1' tecla_esc = 'no' valor_estatico='"+nombre+"'>"+nombre+"</div></td>"+
											"<td><button class = 'btn btn-primary btn-circle mostrar_preguntas' data-toggle='modal' data-target='#notificationModal_1'><i class = 'icon-search'></i></button>&nbsp;"+
												"<button class = 'btn btn-danger btn-circle eliminar_estandar'><i class = 'icon-trash'></i></button>"+
											"</td>");
					$(".ultima_fila").attr("id_estandar",res.id_ultimo).removeClass("ultima_fila");
					$(".guardar_estandar").replaceWith("<button class = 'btn btn-success agregar_estandar'>Agregar</button>");
				}
			})

			.fail(function(error){
			 	console.log("error-no-crea-estandar");
			})
		}
	});

	$(".modal-body").on("click",".eliminar_estandar",function(){
		var id_estandar = $(this).parents("tr").attr("id_estandar");
		var $this = $(this);
		$.ajax({
    		url: "grabar_ajax.php",
			type: "POST",
			dataType: "json",
			data: {
				id_estandar: id_estandar,
				valor: "eliminar_estandar"
				}
		    })
		
		.done(function(res){
			if(res.codigo=="OK")
			{
				$this.parents("tr").remove();

				var i = 1;
    			$("#tblEstandares").find("tbody tr").each(function(){
    				$(this).find("td:first").text(i);
    				i++;	
    			})

    			new PNotify({
						title: 'Mensaje',
						text: 'Estándar eliminado',
						delay: 1200

					});
			}
		})

		.fail(function(error){
            console.log("error-no-elimina-estandar");
        })
    })
*/
</script>