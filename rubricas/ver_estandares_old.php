<?php
	require_once("class/class_mantenimiento.php");

	$obj = new Mantenimiento();
	$id_dimension = $_POST["id_dimension"];
	$dimension = $obj->listar_id_dimension($id_dimension);
?>

<script src="js/jquery-3.1.1.min.js"></script>
<script src = 'js/jquery.cokidoo-textarea.js'></script>

<div class = 'modal-header'>
	<h4>Estándares de Dimensión: <?php echo '"'.$dimension[0]["nombre"].'"';?>
	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button></h4>
</div>
<div class = 'modal-body'>
<?php
	$estandar = $obj->listar_estandares($id_dimension);

	?>
	<div class = 'row'>
		<input type = 'hidden' id= 'id_dimension' value = '<?php echo $id_dimension;?>'>
		<div class = 'col-lg-12'>
			<div class = 'table-responsive'>
				<table class = 'table table-striped table-condensed table-hover display' id='example2'>
					<thead>
						<tr style = 'background-color:#428bca; color: #fff;'>
							<th width = '5%'>N°</th>
							<th width = '80%'>Nombre</th>
							<th width = '15%'>Acción</th>
						</tr>
					</thead>
					<tbody>
						<?php
							$cont = 0;
							
							foreach($estandar as $valor)
							{
							
								$cont++;
								echo "<tr id_estandar = '".$valor["id_estandar"]."'>
										<td>".$cont."</td>
										<td><div class = 'ver cambio_texto1' tecla_esc = 'no' valor_estatico='".$valor["nombre"]."'>".$valor["nombre"]."</div></td>
										<td style = 'position:relative;'>
											<button class = 'btn btn-primary btn-circle mostrar_preguntas' data-toggle='modal' data-target='#notificationModal_1'><i class = 'icon-search'></i></button>
											<button class = 'btn btn-danger btn-circle eliminar_estandar'><i class = 'icon-trash'></i>					
											</button>
										</td>
									</tr>";
							}
						?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<div class = 'modal-footer'>
	<div class = 'row'>
		<div class = 'col-lg-12 text-right'>
			<button type="button" class="btn btn-default btn-close" data-dismiss="modal">Cerrar</button>
			<button class = 'btn btn-success agregar_estandar'>Agregar</button>
		</div>
	</div>
</div>


<script>
	

	$(".modal-body").on("click",".mostrar_preguntas",function(){
		var id_estandar = $(this).parents("tr").attr("id_estandar");
		$.ajax({
                url: "lista_preguntas.php",
                type: "POST",
              	data: { id_estandar: id_estandar },
                success: function(result){
                     $("#mostrar_preguntas").html(result);
                    }
            });
	});

	//$("textarea").autoResize();

	$(".container-fluid").on("click",".cambio_texto1",function(){
		var valor_estatico = $(this).attr("valor_estatico");
		$(this).html("<textarea style='width:100%;' class = 'form-control cambio1'>"+valor_estatico+"</textarea>");
		$(".cambio1").focus();
		$(this).parents("#example2").find(".cambio_texto1").removeClass("cambio_texto1");
	});

	$(".modal-footer").on("click",".agregar_estandar",function(){
		var total_filas = parseInt($("#example2 tbody tr").length) + 1;
		var id_dimension = $("#id_dimension").val();
		$("#example2").append("<tr class = 'ultima_fila'>"+
									"<td>"+total_filas+"</td>"+
									"<td><textarea class = 'form-control ultimo_valor'></textarea></td>"+
									"<td>"+
										"<button class = 'btn btn-default btn-circle cancelar_add'><i class = 'icon-remove'></i></button>"+
									"</td>"+
							"</tr>");
		$(".ultimo_valor").focus();
		
		$(".agregar_estandar").replaceWith("<button class = 'btn btn-primary guardar_estandar'>Guardar</button>");
	});

	$(".modal-body").on("click",".cancelar_add",function(){
		$(".ultima_fila").remove();
		$(".guardar_estandar").replaceWith("<button class = 'btn btn-success agregar_estandar'>Agregar</button>");
	});

	$(".modal-footer").on("click",".guardar_estandar",function(){
		
		var nombre = $(".ultima_fila").find(".ultimo_valor").val();
		var id_dimension = $("#id_dimension").val();
		var total_filas = parseInt($("#example2 tbody tr").length);
		
		if(nombre.length==0)
		{
			$(".ultimo_valor").replaceWith("<div class='form-group has-feedback' style = 'margin-bottom: 0px'><textarea class='form-control ultimo_valor' style = 'width:100%;' placeholder = 'Ingrese un nombre'/><i class='glyphicon glyphicon-exclamation-sign form-control-feedback red'></i></textarea></div>");
			$(".ultimo_valor").addClass("borde_rojo");
		}
		else
		{
			$.ajax({
    		url: "grabar_ajax.php",
			type: "POST",
			dataType: "json",
			data: {
				nombre: nombre,
				id_dimension: id_dimension,
				valor: "crear_estandar"
				}
		    })

		    .done(function(res){
				if(res.codigo=="OK")
				{
					$(".ultima_fila").html("<td>"+total_filas+"</td>"+
											"<td><div class = 'ver cambio_texto1' tecla_esc = 'no' valor_estatico='"+nombre+"'>"+nombre+"</div></td>"+
											"<td><button class = 'btn btn-primary btn-circle mostrar_preguntas' data-toggle='modal' data-target='#notificationModal_1'><i class = 'icon-search'></i></button>&nbsp;"+
												"<button class = 'btn btn-danger btn-circle eliminar_estandar'><i class = 'icon-trash'></i></button>"+
											"</td>");
					$(".ultima_fila").attr("id_estandar",res.id_ultimo).removeClass("ultima_fila");
					$(".guardar_estandar").replaceWith("<button class = 'btn btn-success agregar_estandar'>Agregar</button>");
				}
			})

			.fail(function(error){
			 	console.log("error-no-crea-estandar");
			})
		}
	});

	$(".modal-body").on("click",".eliminar_estandar",function(){
		var id_estandar = $(this).parents("tr").attr("id_estandar");
		var $this = $(this);
		$.ajax({
    		url: "grabar_ajax.php",
			type: "POST",
			dataType: "json",
			data: {
				id_estandar: id_estandar,
				valor: "eliminar_estandar"
				}
		    })
		
		.done(function(res){
			if(res.codigo=="OK")
			{
				$this.parents("tr").remove();

				var i = 1;
    			$("#example2").find("tbody tr").each(function(){
    				$(this).find("td:first").text(i);
    				i++;	
    			})

    			new PNotify({
						title: 'Mensaje',
						text: 'Estándar eliminado',
						delay: 1200

					});
			}
		})

		.fail(function(error){
            console.log("error-no-elimina-estandar");
        })
    })

</script>