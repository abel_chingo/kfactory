function valida_correo(correo) 
{
	if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(correo))
	{
		return (true)
	} 
	else 
	{
		return (false);
	}
}

function limpiar_logueo()
{
	document.form.reset();
	document.form.usuario.focus();
}

function validar_md5()
{
	form.password.value = calcMD5(form.password.value);
	form.submit();
}

function validar_ficha1()
{
	var form=document.form;
	var fecha = new Date();
	
	var f1 = new Date(form.fec.value);
	var f2 = new Date(form.fec2.value);
	
	
	if (form.tipo_ficha.value==0)
	{
		alert("No ha seleccionado el tipo de creación de ficha");
		form.tipo_ficha.value="0";
		form.tipo_ficha.focus();
		return false;
	}
	
	if (form.poblacion.value==0)
	{
		alert("No ha seleccionado el tipo de selección de ficha");
		form.poblacion.value="0";
		form.poblacion.focus();
		return false;
	}
	
	if (form.fec.value==0)
	{
		alert("La fecha de inicio está vacía...Ingrese una fecha");
		form.fec.value="";
		form.fec.focus();
		return false;
	}
	
	else if (form.fec2.value==0)
	{
		alert("La fecha de término está vacía...Ingrese una fecha");
		form.fec2.value="";
		form.fec2.focus();
		return false;
	}
	
	else if (form.nombre.value==0)
	{
		alert("El nombre del modelo de ficha está vacío...Ingrese un modelo");
		form.nombre.value="";
		form.nombre.focus();
		return false;
	}
	
	
	else if(f1 > f2)
	{
		alert("La fecha de inicio es mayor a la fecha de término, ingrese la fecha nuevamente");
		form.fec.value="";
		form.fec.focus();
		return false;
	}

	else if (f1.getTime() == f2.getTime())
	{
		alert("Son la misma fecha, ingrese las fechas nuevamente");
		form.fec.value="";
		form.fec.focus();
		form.fec2.value="";
		form.fec2.focus();
	}
	
	else if(form.repetido.value==1)
	{
		alert("El nombre del modelo ya está habilitado en el sistema.  Ingrese otro nombre");
		form.nombre.value="";
		form.nombre.focus();
	}
	
	else
	{
		form.submit(); 
	}
}