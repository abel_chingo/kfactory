var evaluarRsptaVoF = function( $inpRadio,pnl ){
    var name = $inpRadio.attr('name');
    var marcada_value = $('input[name="'+name+'"].radio-ctrl:checked',pnl).val();
    var rspta_crrta = $('input#'+name,pnl).val();
    var lock = $('input#'+name,pnl).attr('data-lock');
    var rspta_marcada = $.md5(marcada_value, lock);
    var $icon_zone = $inpRadio.parents('.options').siblings('.icon-zone').find('.icon-result');
    
    if(rspta_marcada===rspta_crrta){
        //var icono = '<i class="fa fa-check color-green2"></i>';
        var claseAdd = 'good';
        var claseRemove = 'bad';
    } else {
        //var icono = '<i class="fa fa-times color-red"></i>';
        var claseAdd = 'bad';
        var claseRemove = 'good';
    }
    //$($icon_zone,pnl).html(icono);
    $($icon_zone,pnl).closest('.premise').addClass(claseAdd).removeClass(claseRemove);
};

(function($){
    $.fn.examTrueFalse = function(opciones){
        var opts = $.extend({}, $.fn.examTrueFalse.defaults, opciones);
        return this.each(function() {
            var that = $(this);
            that.on('click', '.premise .options input[type="radio"]', function(e) {
                e.stopPropagation();
                var $inpRadio = $(this);
                evaluarRsptaVoF($inpRadio, that);

                
                $(this).attr('checked', 'checked');
                $(this).closest('label').siblings('label').find('input[type="radio"]').removeAttr('checked');
            });
        });
    }

    $.fn.examTrueFalse.defaults = {};
}(jQuery));