/**
* Relacionar pares de fichas plugin
**/
var CLAVE_ACTIVA = null;
var COLOR_SELECC = null;

var initFichas = function(IDGUI){
    if($('#lista-fichas'+IDGUI).html()!=undefined && $('#lista-fichas'+IDGUI).html().trim()=='') { 
        $('#tmp_'+IDGUI+'   .add-ficha').trigger('click');
    }
    mostrarBtnsDinamicos(IDGUI);
    $("*[data-tooltip=\"tooltip\"]").tooltip();
};

var desmarcarIncorrectas = function( unicoIntento , content ){    
    $('.ejerc-fichas',content).find('.ficha.active').each(function() {
        if( !$(this).hasClass('good') ){ 
            $(this).removeClass('active');
            if(!unicoIntento){ $(this).removeClass('bad'); }
        }
    });
};

var corregirFicha = function( $ficha ){
    if( $ficha.hasClass('active') ){
        var valor = $ficha.attr('data-key');
        if(CLAVE_ACTIVA==valor){ /* Match correcto */
            $ficha.removeClass('bad').addClass('good');
        } else { /* Match incorrecto */
            $ficha.removeClass('good').addClass('bad');
            if( esDBYself($ficha) ) restarPuntaje();
        }
    } else {
        $ficha.removeClass('bad').removeClass('good');
    }
    if( esDBYself($ficha) ) pausarTiempoFichas();
};

(function($){
    $.fn.examJoin = function(opciones){
        var opts = $.extend({}, $.fn.examJoin.defaults, opciones);
        return this.each(function() {
            var that = $(this);
            var _idgui = that.find('#idgui').val();
            var _isEditando = that.hasClass('editando');

            that.on('click', '.ejerc-fichas .partes-1>.ficha', function(e) {
                if(_isEditando==true) return false;
                e.stopPropagation();
                $(this).closest('.ejerc-fichas').find('.partes-1>.ficha').removeClass('active');

                var seleccionado = $(this).attr('data-key');        
                if(CLAVE_ACTIVA != seleccionado){
                    CLAVE_ACTIVA = seleccionado;
                    COLOR_SELECC = $(this).attr('data-color');
                    $('.partes-1 .ficha[data-key="'+CLAVE_ACTIVA+'"]').addClass('active');
                } else {
                    $(this).removeClass('active');
                    CLAVE_ACTIVA = null;
                    COLOR_SELECC = null;
                }
            }).on('click', '.ejerc-fichas .partes-2>.ficha', function(e) {
                e.stopPropagation();
                
                if(CLAVE_ACTIVA!=null){
                    $(this).addClass('active');
                    corregirFicha( $(this) );
                    $(this).attr('data-color', COLOR_SELECC);
                    $('.partes-1 .ficha.active').removeClass('active');
                    CLAVE_ACTIVA = null;
                    COLOR_SELECC = null;
                }
            }).on('click', '.ejerc-fichas .ficha a', function(e) {
                if(_isEditando==true) return false;
                e.preventDefault();
                e.stopPropagation();
                var $plantilla=$(this).parents('.plantilla-fichas');
                var $audio = $('#audio-ejercicio'+_idgui),
                src = $(this).attr('data-audio');
                $audio.attr('src', _sysUrlStatic_ +'/media/audio/'+src);
                $audio.trigger('play');
                $(this).closest('.ficha').trigger('click');
            });

            initFichas(_idgui);
        });
    }

    $.fn.examJoin.defaults = {};
}(jQuery));