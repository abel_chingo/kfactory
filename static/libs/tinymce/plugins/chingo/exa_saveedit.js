tinymce.PluginManager.add('exasave', function(editor, url) { 
    function showDialog(){
        $('.guardartpl').trigger('click');
    }
  
    editor.addButton('exasave', {
        tooltip: 'Save edit',
        icon: 'fa fa-save',
        onclick: showDialog,
    });
    editor.addMenuItem('exasave', {
        icon: 'fa fa-save',
        text: 'Save',
        onclick: showDialog,
        context: 'Save',
        prependToContext: true
    });
    editor.addCommand('mceSave', showDialog);
    this.showDialog = showDialog;
});