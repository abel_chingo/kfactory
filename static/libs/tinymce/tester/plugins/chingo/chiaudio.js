
tinymce.PluginManager.add('chingoaudio', function(editor, url){ 
    function showDialog(){
        var rutabase=tinymce.baseURL+'/../../../../';
        var win, width, height, data={}, dom=editor.dom,element=editor.selection;
        var frmItems=[{name: 'url',  type: 'textbox', size: 40, autofocus: true, label: 'Url'}];
        ;
        win = editor.windowManager.open({
            title: 'Selected Audio',
            url: rutabase+'biblioteca/?plt=tinymce&type=audio',
            width: 400,
            height: 600
        },{
            editor:editor
        });
    }
    editor.addButton('chingoaudio', {
        tooltip: 'Insert/edit Audio',
        icon: 'fa fa-music',
        onclick: showDialog,
        stateSelector: ['video[data-mce-object=video]']
    });
    editor.addMenuItem('chingoaudio', {
        icon: 'fa fa-music',
        text: 'Insert/edit Audio',
        onclick: showDialog,
        context: 'insert',
        prependToContext: true
    });
    editor.addCommand('mceImage', showDialog);
    this.showDialog = showDialog;
});