<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		31-05-2017 
 * @copyright	Copyright (C) 31-05-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegTarea_respuesta', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegTarea_asignacion_alumno', RUTA_BASE, 'sys_negocio');
class WebTarea_respuesta extends JrWeb
{
	private $oNegTarea_respuesta;
	private $oNegTarea_asignacion_alumno;
	public function __construct()
	{
		parent::__construct();		
		$this->oNegTarea_respuesta = new NegTarea_respuesta;
		$this->oNegTarea_asignacion_alumno = new NegTarea_asignacion_alumno;
	}

	public function defecto(){
		//return $this->listado();
	}
/*
	public function listado()
	{
		try{
			global $aplicacion;			
			if(!NegSesion::tiene_acceso('Tarea_respuesta', 'list')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
            $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
            $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
			//$this->documento->script(null, ConfigSitio::get('tema_general') . '/js/datatables/js/jquery.dataTables.js');
			
			
			$this->datos=$this->oNegTarea_respuesta->buscar();

						$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Tarea_respuesta'), true);
			$this->esquema = 'tarea_respuesta-list';			
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}


	public function agregar()
	{
		try {
			global $aplicacion;			
			if(!NegSesion::tiene_acceso('Tarea_respuesta', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->frmaccion='Nuevo';
			$this->documento->setTitulo(JrTexto::_('Tarea_respuesta').' /'.JrTexto::_('New'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function editar()
	{
		try {
			global $aplicacion;
			
			if(!NegSesion::tiene_acceso('Tarea_respuesta', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->frmaccion='Editar';
			$this->oNegTarea_respuesta->idtarea_respuesta = @$_GET['id'];
			$this->datos = $this->oNegTarea_respuesta->dataTarea_respuesta;
			$this->pk=@$_GET['id'];
			$this->documento->setTitulo(JrTexto::_('Tarea_respuesta').' /'.JrTexto::_('Edit'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function ver(){
		try{
			global $aplicacion;						
			$this->oNegTarea_respuesta->idtarea_respuesta = @$_GET['id'];
			$this->datos = $this->oNegTarea_respuesta->dataTarea_respuesta;
									$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Tarea_respuesta').' /'.JrTexto::_('see'), true);
			$this->esquema = 'tarea_respuesta-see';
			$this->documento->plantilla = $tpl;
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	private function form()
	{
		try {
			global $aplicacion;	
			
			//$this->documento->script(null, 'http://tinymce.cachefly.net/4.2/tinymce.min.js');			
			$this->esquema = 'tarea_respuesta-frm';
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}
*/
	public function xGuardar()
	{
		$this->documento->plantilla = 'returnjson';
		try {
			global $aplicacion;
			if(empty($_POST['idtarea_asignacion_alumno']) && empty($_POST['actualizarEstadoAsignacion'])){
				throw new Exception(JrTexto::_('No data to insert'));
			}
			if(!empty($_POST['pkIdtarea_respuesta'])){
				$this->oNegTarea_respuesta->idtarea_respuesta = $_POST['pkIdtarea_respuesta'];
			}
			$this->oNegTarea_respuesta->__set('idtarea_asignacion_alumno',@$_POST['idtarea_asignacion_alumno']);
			$this->oNegTarea_respuesta->__set('comentario',@$_POST['comentario']);
			$this->oNegTarea_respuesta->__set('fechapresentacion', date('Y-m-d'));
			$this->oNegTarea_respuesta->__set('horapresentacion',date('H:i:s'));
			if(empty($_POST['pkIdtarea_respuesta'])){
				$idTareaRspta=$this->oNegTarea_respuesta->agregar();
			}else{
				$idTareaRspta=$this->oNegTarea_respuesta->editar();
		    }
		    /*************	Actualizar `tarea_asignacion_alumno`.`estado`	*************/
		    if($_POST['actualizarEstadoAsignacion']=='true'){
		    	$this->oNegTarea_asignacion_alumno->iddetalle=@$_POST['idtarea_asignacion_alumno'];
		    	$this->oNegTarea_asignacion_alumno->__set('estado', 'P');
		    	$resp = $this->oNegTarea_asignacion_alumno->editar();
		    }

			$data=array('code'=>'ok','data'=>$idTareaRspta, 'msj'=>JrTexto::_('Submitted successfully'));
            echo json_encode($data);
            return parent::getEsquema();
		} catch (Exception $e) {
			$data=array('code'=>'Error','msj'=>JrTexto::_($e->getMessage()));
            echo json_encode($data);
		}
	}
/*
	// ========================== Funciones xajax ========================== //
	public function xSaveTarea_respuesta(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$frm = $args[0];
				
				if(!empty($frm['pkIdtarea_respuesta'])) {
					$this->oNegTarea_respuesta->idtarea_respuesta = $frm['pkIdtarea_respuesta'];
				}
				
				$this->oNegTarea_respuesta->__set('idtarea_asigancion_alumno',@$frm["txtIdtarea_asigancion_alumno"]);
				$this->oNegTarea_respuesta->__set('comentario',@$frm["txtComentario"]);
				$this->oNegTarea_respuesta->__set('fechapresentacion',@$frm["txtFechapresentacion"]);
				$this->oNegTarea_respuesta->__set('horapresentacion',@$frm["txtHorapresentacion"]);
				
			    if(@$frm["accion"]=="Nuevo"){
					$res=$this->oNegTarea_respuesta->agregar();
				}else{
					$res=$this->oNegTarea_respuesta->editar();
			    }
				if(!empty($res)) $oRespAjax->setReturnValue($this->oNegTarea_respuesta->idtarea_respuesta);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Failed to save registry')),'warning');
					$oRespAjax->setReturnValue(false);
				}
							
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}

	public function xGetxIDTarea_respuesta(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegTarea_respuesta->__set('idtarea_respuesta', $pk);
				$this->datos = $this->oNegTarea_respuesta->dataTarea_respuesta;
				$res=$this->oNegTarea_respuesta->getXid();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')),  $this->pasarHtml(JrTexto::_('Get record')), 'warning');
				$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}
	public function xEliminar(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegTarea_respuesta->__set('idtarea_respuesta', $pk);
				$res=$this->oNegTarea_respuesta->eliminar();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Error').' '.JrTexto::_('Delete Record')), 'warning');
					$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}
*/
	     
}