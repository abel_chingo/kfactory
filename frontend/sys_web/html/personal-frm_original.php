<?php 
defined('RUTA_BASE') or die();
$id_vent = uniqid();
if(!empty($this->datos)) $frm=$this->datos;
$ventanapadre=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:'eeeexzx-1';
?><div class="row" style="<?php echo $this->documento->plantilla=="blanco"?"min-width:600px":"" ?>">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="panel">
      <?php if($this->documento->plantilla!='modal'){?><div class="panel-heading bg-blue">
        <h3 class="panel-title"><?php echo JrTexto::_('Personal'); ?> <i class="fa fa-angle-double-right" aria-hidden="true"></i> <small id="frmaction"> <?php echo JrTexto::_($this->frmaccion);?></small></h3>
        <div class="clearfix"></div>
      </div><?php } ?>      
      <div class="panel-body">
        <div id="msj-interno"></div>
        <form method="post" id="frm-<?php echo $id_vent;?>"  target="" enctype="" class="form-horizontal form-label-left" >
          <input type="hidden" name="pkDni" id="pkdni" value="<?php echo JrTexto::_($this->pk);?>">
          <input type="hidden" name="accion" id="pkaccion" value="<?php echo JrTexto::_($this->frmaccion);?>">
          <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtApe_paterno">
              <?php echo JrTexto::_('Ape paterno');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtApe_paterno" name="txtApe_paterno" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["ape_paterno"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtApe_materno">
              <?php echo JrTexto::_('Ape materno');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtApe_materno" name="txtApe_materno" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["ape_materno"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtNombre">
              <?php echo JrTexto::_('Nombre');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtNombre" name="txtNombre" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["nombre"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtFechanac">
              <?php echo JrTexto::_('Fechanac');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtFechanac" name="txtFechanac" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["fechanac"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtSexo">
              <?php echo JrTexto::_('Sexo');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
              
              <?php 
              if(!empty($this->fksexo))
                foreach ($this->fksexo as $fksexo) {
                 ?><input type="radio" id="txtsexo" name="txtSexo" >
               <?php } ?>                    
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtEstado_civil">
              <?php echo JrTexto::_('Estado civil');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
              
              <?php 
              if(!empty($this->fkestado_civil))
                foreach ($this->fkestado_civil as $fkestado_civil) {
                 ?><input type="radio" id="txtestado_civil" name="txtEstado_civil" >
               <?php } ?>                    
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtUbigeo">
              <?php echo JrTexto::_('Ubigeo');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
              <select id="txtubigeo" name="txtUbigeo" class="form-control">
               <option value=""><?php echo JrTexto::_('Seleccione'); ?></option>
              <?php 
              if(!empty($this->fkubigeo))
                foreach ($this->fkubigeo as $fkubigeo) { ?><option value="<?php echo $fkubigeo["id_ubigeo"]?>" <?php echo $fkubigeo["id_ubigeo"]==@$frm["ubigeo"]?"selected":""; ?> ><?php echo $fkubigeo["pais"] ?></option><?php } ?>              </select>
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtUrbanizacion">
              <?php echo JrTexto::_('Urbanizacion');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtUrbanizacion" name="txtUrbanizacion" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["urbanizacion"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtDireccion">
              <?php echo JrTexto::_('Direccion');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtDireccion" name="txtDireccion" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["direccion"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtTelefono">
              <?php echo JrTexto::_('Telefono');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtTelefono" name="txtTelefono" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["telefono"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtCelular">
              <?php echo JrTexto::_('Celular');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtCelular" name="txtCelular" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["celular"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtEmail">
              <?php echo JrTexto::_('Email');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="email" id="txtEmail" name="txtEmail" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["email"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtIdugel">
              <?php echo JrTexto::_('Idugel');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
              <select id="txtidugel" name="txtIdugel" class="form-control">
               <option value=""><?php echo JrTexto::_('Seleccione'); ?></option>
              <?php 
              if(!empty($this->fkidugel))
                foreach ($this->fkidugel as $fkidugel) { ?><option value="<?php echo $fkidugel["idugel"]?>" <?php echo $fkidugel["idugel"]==@$frm["idugel"]?"selected":""; ?> ><?php echo $fkidugel["descripcion"] ?></option><?php } ?>              </select>
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtRegusuario">
              <?php echo JrTexto::_('Regusuario');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
              <br />
<font size='1'><table class='xdebug-error xe-notice' dir='ltr' border='1' cellspacing='0' cellpadding='1'>
<tr><th align='left' bgcolor='#f57900' colspan="5"><span style='background-color: #cc0000; color: #fce94f; font-size: x-large;'>( ! )</span> Notice: Undefined index: tipofkcomo_regusuario in D:\wamp64\www\pvingles.local\frontend\sys_web\html\generador\frm.php on line <i>329</i></th></tr>
<tr><th align='left' bgcolor='#e9b96e' colspan='5'>Call Stack</th></tr>
<tr><th align='center' bgcolor='#eeeeec'>#</th><th align='left' bgcolor='#eeeeec'>Time</th><th align='left' bgcolor='#eeeeec'>Memory</th><th align='left' bgcolor='#eeeeec'>Function</th><th align='left' bgcolor='#eeeeec'>Location</th></tr>
<tr><td bgcolor='#eeeeec' align='center'>1</td><td bgcolor='#eeeeec' align='center'>0.0003</td><td bgcolor='#eeeeec' align='right'>283200</td><td bgcolor='#eeeeec'>{main}(  )</td><td title='D:\wamp64\www\pvingles.local\frontend\ServerxAjax.php' bgcolor='#eeeeec'>...\ServerxAjax.php<b>:</b>0</td></tr>
<tr><td bgcolor='#eeeeec' align='center'>2</td><td bgcolor='#eeeeec' align='center'>0.0556</td><td bgcolor='#eeeeec' align='right'>2966464</td><td bgcolor='#eeeeec'>xajax->processRequest(  )</td><td title='D:\wamp64\www\pvingles.local\frontend\ServerxAjax.php' bgcolor='#eeeeec'>...\ServerxAjax.php<b>:</b>19</td></tr>
<tr><td bgcolor='#eeeeec' align='center'>3</td><td bgcolor='#eeeeec' align='center'>0.0557</td><td bgcolor='#eeeeec' align='right'>2966928</td><td bgcolor='#eeeeec'>xajaxPluginManager->processRequest(  )</td><td title='D:\wamp64\www\pvingles.local\sys_lib\xajax\xajax_core\xajaxAIO.inc.php' bgcolor='#eeeeec'>...\xajaxAIO.inc.php<b>:</b>445</td></tr>
<tr><td bgcolor='#eeeeec' align='center'>4</td><td bgcolor='#eeeeec' align='center'>0.0557</td><td bgcolor='#eeeeec' align='right'>2968072</td><td bgcolor='#eeeeec'>xajaxFunctionPlugin->processRequest(  )</td><td title='D:\wamp64\www\pvingles.local\sys_lib\xajax\xajax_core\xajaxAIO.inc.php' bgcolor='#eeeeec'>...\xajaxAIO.inc.php<b>:</b>51</td></tr>
<tr><td bgcolor='#eeeeec' align='center'>5</td><td bgcolor='#eeeeec' align='center'>0.0602</td><td bgcolor='#eeeeec' align='right'>2968880</td><td bgcolor='#eeeeec'>xajaxUserFunction->call(  )</td><td title='D:\wamp64\www\pvingles.local\sys_lib\xajax\xajax_core\xajaxAIO.inc.php' bgcolor='#eeeeec'>...\xajaxAIO.inc.php<b>:</b>1013</td></tr>
<tr><td bgcolor='#eeeeec' align='center'>6</td><td bgcolor='#eeeeec' align='center'>0.0605</td><td bgcolor='#eeeeec' align='right'>3005376</td><td bgcolor='#eeeeec'><a href='http://www.php.net/function.call-user-func-array:{D:\wamp64\www\pvingles.local\sys-lib\xajax\xajax-core\xajaxAIO.inc.php:1077}' target='_new'>call_user_func_array:{D:\wamp64\www\pvingles.local\sys_lib\xajax\xajax_core\xajaxAIO.inc.php:1077}</a>
(  )</td><td title='D:\wamp64\www\pvingles.local\sys_lib\xajax\xajax_core\xajaxAIO.inc.php' bgcolor='#eeeeec'>...\xajaxAIO.inc.php<b>:</b>1077</td></tr>
<tr><td bgcolor='#eeeeec' align='center'>7</td><td bgcolor='#eeeeec' align='center'>0.0605</td><td bgcolor='#eeeeec' align='right'>3006152</td><td bgcolor='#eeeeec'>WebAdminxAjax::call(  )</td><td title='D:\wamp64\www\pvingles.local\sys_lib\xajax\xajax_core\xajaxAIO.inc.php' bgcolor='#eeeeec'>...\xajaxAIO.inc.php<b>:</b>1077</td></tr>
<tr><td bgcolor='#eeeeec' align='center'>8</td><td bgcolor='#eeeeec' align='center'>0.0606</td><td bgcolor='#eeeeec' align='right'>3008640</td><td bgcolor='#eeeeec'>WebAdminxAjax::recurso(  )</td><td title='D:\wamp64\www\pvingles.local\sys_inc\cls.WebAdminxAjax.php' bgcolor='#eeeeec'>...\cls.WebAdminxAjax.php<b>:</b>25</td></tr>
<tr><td bgcolor='#eeeeec' align='center'>9</td><td bgcolor='#eeeeec' align='center'>0.0617</td><td bgcolor='#eeeeec' align='right'>3116280</td><td bgcolor='#eeeeec'>WebGenerador->xsetGenerarPagina(  )</td><td title='D:\wamp64\www\pvingles.local\sys_inc\cls.WebAdminxAjax.php' bgcolor='#eeeeec'>...\cls.WebAdminxAjax.php<b>:</b>66</td></tr>
<tr><td bgcolor='#eeeeec' align='center'>10</td><td bgcolor='#eeeeec' align='center'>0.6425</td><td bgcolor='#eeeeec' align='right'>3181568</td><td bgcolor='#eeeeec'>JrWeb->getEsquema(  )</td><td title='D:\wamp64\www\pvingles.local\frontend\sys_web\cls.WebGenerador.php' bgcolor='#eeeeec'>...\cls.WebGenerador.php<b>:</b>141</td></tr>
<tr><td bgcolor='#eeeeec' align='center'>11</td><td bgcolor='#eeeeec' align='center'>0.6437</td><td bgcolor='#eeeeec' align='right'>3348808</td><td bgcolor='#eeeeec'>require_once( <font color='#00bb00'>'D:\wamp64\www\pvingles.local\frontend\sys_web\html\generador\frm.php'</font> )</td><td title='D:\wamp64\www\pvingles.local\sys_lib\jrAdwen\cls.JrWeb.php' bgcolor='#eeeeec'>...\cls.JrWeb.php<b>:</b>34</td></tr>
</table></font>
<br />
<font size='1'><table class='xdebug-error xe-notice' dir='ltr' border='1' cellspacing='0' cellpadding='1'>
<tr><th align='left' bgcolor='#f57900' colspan="5"><span style='background-color: #cc0000; color: #fce94f; font-size: x-large;'>( ! )</span> Notice: Undefined index: tipofkcomo_regusuario in D:\wamp64\www\pvingles.local\frontend\sys_web\html\generador\frm.php on line <i>340</i></th></tr>
<tr><th align='left' bgcolor='#e9b96e' colspan='5'>Call Stack</th></tr>
<tr><th align='center' bgcolor='#eeeeec'>#</th><th align='left' bgcolor='#eeeeec'>Time</th><th align='left' bgcolor='#eeeeec'>Memory</th><th align='left' bgcolor='#eeeeec'>Function</th><th align='left' bgcolor='#eeeeec'>Location</th></tr>
<tr><td bgcolor='#eeeeec' align='center'>1</td><td bgcolor='#eeeeec' align='center'>0.0003</td><td bgcolor='#eeeeec' align='right'>283200</td><td bgcolor='#eeeeec'>{main}(  )</td><td title='D:\wamp64\www\pvingles.local\frontend\ServerxAjax.php' bgcolor='#eeeeec'>...\ServerxAjax.php<b>:</b>0</td></tr>
<tr><td bgcolor='#eeeeec' align='center'>2</td><td bgcolor='#eeeeec' align='center'>0.0556</td><td bgcolor='#eeeeec' align='right'>2966464</td><td bgcolor='#eeeeec'>xajax->processRequest(  )</td><td title='D:\wamp64\www\pvingles.local\frontend\ServerxAjax.php' bgcolor='#eeeeec'>...\ServerxAjax.php<b>:</b>19</td></tr>
<tr><td bgcolor='#eeeeec' align='center'>3</td><td bgcolor='#eeeeec' align='center'>0.0557</td><td bgcolor='#eeeeec' align='right'>2966928</td><td bgcolor='#eeeeec'>xajaxPluginManager->processRequest(  )</td><td title='D:\wamp64\www\pvingles.local\sys_lib\xajax\xajax_core\xajaxAIO.inc.php' bgcolor='#eeeeec'>...\xajaxAIO.inc.php<b>:</b>445</td></tr>
<tr><td bgcolor='#eeeeec' align='center'>4</td><td bgcolor='#eeeeec' align='center'>0.0557</td><td bgcolor='#eeeeec' align='right'>2968072</td><td bgcolor='#eeeeec'>xajaxFunctionPlugin->processRequest(  )</td><td title='D:\wamp64\www\pvingles.local\sys_lib\xajax\xajax_core\xajaxAIO.inc.php' bgcolor='#eeeeec'>...\xajaxAIO.inc.php<b>:</b>51</td></tr>
<tr><td bgcolor='#eeeeec' align='center'>5</td><td bgcolor='#eeeeec' align='center'>0.0602</td><td bgcolor='#eeeeec' align='right'>2968880</td><td bgcolor='#eeeeec'>xajaxUserFunction->call(  )</td><td title='D:\wamp64\www\pvingles.local\sys_lib\xajax\xajax_core\xajaxAIO.inc.php' bgcolor='#eeeeec'>...\xajaxAIO.inc.php<b>:</b>1013</td></tr>
<tr><td bgcolor='#eeeeec' align='center'>6</td><td bgcolor='#eeeeec' align='center'>0.0605</td><td bgcolor='#eeeeec' align='right'>3005376</td><td bgcolor='#eeeeec'><a href='http://www.php.net/function.call-user-func-array:{D:\wamp64\www\pvingles.local\sys-lib\xajax\xajax-core\xajaxAIO.inc.php:1077}' target='_new'>call_user_func_array:{D:\wamp64\www\pvingles.local\sys_lib\xajax\xajax_core\xajaxAIO.inc.php:1077}</a>
(  )</td><td title='D:\wamp64\www\pvingles.local\sys_lib\xajax\xajax_core\xajaxAIO.inc.php' bgcolor='#eeeeec'>...\xajaxAIO.inc.php<b>:</b>1077</td></tr>
<tr><td bgcolor='#eeeeec' align='center'>7</td><td bgcolor='#eeeeec' align='center'>0.0605</td><td bgcolor='#eeeeec' align='right'>3006152</td><td bgcolor='#eeeeec'>WebAdminxAjax::call(  )</td><td title='D:\wamp64\www\pvingles.local\sys_lib\xajax\xajax_core\xajaxAIO.inc.php' bgcolor='#eeeeec'>...\xajaxAIO.inc.php<b>:</b>1077</td></tr>
<tr><td bgcolor='#eeeeec' align='center'>8</td><td bgcolor='#eeeeec' align='center'>0.0606</td><td bgcolor='#eeeeec' align='right'>3008640</td><td bgcolor='#eeeeec'>WebAdminxAjax::recurso(  )</td><td title='D:\wamp64\www\pvingles.local\sys_inc\cls.WebAdminxAjax.php' bgcolor='#eeeeec'>...\cls.WebAdminxAjax.php<b>:</b>25</td></tr>
<tr><td bgcolor='#eeeeec' align='center'>9</td><td bgcolor='#eeeeec' align='center'>0.0617</td><td bgcolor='#eeeeec' align='right'>3116280</td><td bgcolor='#eeeeec'>WebGenerador->xsetGenerarPagina(  )</td><td title='D:\wamp64\www\pvingles.local\sys_inc\cls.WebAdminxAjax.php' bgcolor='#eeeeec'>...\cls.WebAdminxAjax.php<b>:</b>66</td></tr>
<tr><td bgcolor='#eeeeec' align='center'>10</td><td bgcolor='#eeeeec' align='center'>0.6425</td><td bgcolor='#eeeeec' align='right'>3181568</td><td bgcolor='#eeeeec'>JrWeb->getEsquema(  )</td><td title='D:\wamp64\www\pvingles.local\frontend\sys_web\cls.WebGenerador.php' bgcolor='#eeeeec'>...\cls.WebGenerador.php<b>:</b>141</td></tr>
<tr><td bgcolor='#eeeeec' align='center'>11</td><td bgcolor='#eeeeec' align='center'>0.6437</td><td bgcolor='#eeeeec' align='right'>3348808</td><td bgcolor='#eeeeec'>require_once( <font color='#00bb00'>'D:\wamp64\www\pvingles.local\frontend\sys_web\html\generador\frm.php'</font> )</td><td title='D:\wamp64\www\pvingles.local\sys_lib\jrAdwen\cls.JrWeb.php' bgcolor='#eeeeec'>...\cls.JrWeb.php<b>:</b>34</td></tr>
</table></font>
<br />
<font size='1'><table class='xdebug-error xe-notice' dir='ltr' border='1' cellspacing='0' cellpadding='1'>
<tr><th align='left' bgcolor='#f57900' colspan="5"><span style='background-color: #cc0000; color: #fce94f; font-size: x-large;'>( ! )</span> Notice: Undefined index: tipofkcomo_regusuario in D:\wamp64\www\pvingles.local\frontend\sys_web\html\generador\frm.php on line <i>349</i></th></tr>
<tr><th align='left' bgcolor='#e9b96e' colspan='5'>Call Stack</th></tr>
<tr><th align='center' bgcolor='#eeeeec'>#</th><th align='left' bgcolor='#eeeeec'>Time</th><th align='left' bgcolor='#eeeeec'>Memory</th><th align='left' bgcolor='#eeeeec'>Function</th><th align='left' bgcolor='#eeeeec'>Location</th></tr>
<tr><td bgcolor='#eeeeec' align='center'>1</td><td bgcolor='#eeeeec' align='center'>0.0003</td><td bgcolor='#eeeeec' align='right'>283200</td><td bgcolor='#eeeeec'>{main}(  )</td><td title='D:\wamp64\www\pvingles.local\frontend\ServerxAjax.php' bgcolor='#eeeeec'>...\ServerxAjax.php<b>:</b>0</td></tr>
<tr><td bgcolor='#eeeeec' align='center'>2</td><td bgcolor='#eeeeec' align='center'>0.0556</td><td bgcolor='#eeeeec' align='right'>2966464</td><td bgcolor='#eeeeec'>xajax->processRequest(  )</td><td title='D:\wamp64\www\pvingles.local\frontend\ServerxAjax.php' bgcolor='#eeeeec'>...\ServerxAjax.php<b>:</b>19</td></tr>
<tr><td bgcolor='#eeeeec' align='center'>3</td><td bgcolor='#eeeeec' align='center'>0.0557</td><td bgcolor='#eeeeec' align='right'>2966928</td><td bgcolor='#eeeeec'>xajaxPluginManager->processRequest(  )</td><td title='D:\wamp64\www\pvingles.local\sys_lib\xajax\xajax_core\xajaxAIO.inc.php' bgcolor='#eeeeec'>...\xajaxAIO.inc.php<b>:</b>445</td></tr>
<tr><td bgcolor='#eeeeec' align='center'>4</td><td bgcolor='#eeeeec' align='center'>0.0557</td><td bgcolor='#eeeeec' align='right'>2968072</td><td bgcolor='#eeeeec'>xajaxFunctionPlugin->processRequest(  )</td><td title='D:\wamp64\www\pvingles.local\sys_lib\xajax\xajax_core\xajaxAIO.inc.php' bgcolor='#eeeeec'>...\xajaxAIO.inc.php<b>:</b>51</td></tr>
<tr><td bgcolor='#eeeeec' align='center'>5</td><td bgcolor='#eeeeec' align='center'>0.0602</td><td bgcolor='#eeeeec' align='right'>2968880</td><td bgcolor='#eeeeec'>xajaxUserFunction->call(  )</td><td title='D:\wamp64\www\pvingles.local\sys_lib\xajax\xajax_core\xajaxAIO.inc.php' bgcolor='#eeeeec'>...\xajaxAIO.inc.php<b>:</b>1013</td></tr>
<tr><td bgcolor='#eeeeec' align='center'>6</td><td bgcolor='#eeeeec' align='center'>0.0605</td><td bgcolor='#eeeeec' align='right'>3005376</td><td bgcolor='#eeeeec'><a href='http://www.php.net/function.call-user-func-array:{D:\wamp64\www\pvingles.local\sys-lib\xajax\xajax-core\xajaxAIO.inc.php:1077}' target='_new'>call_user_func_array:{D:\wamp64\www\pvingles.local\sys_lib\xajax\xajax_core\xajaxAIO.inc.php:1077}</a>
(  )</td><td title='D:\wamp64\www\pvingles.local\sys_lib\xajax\xajax_core\xajaxAIO.inc.php' bgcolor='#eeeeec'>...\xajaxAIO.inc.php<b>:</b>1077</td></tr>
<tr><td bgcolor='#eeeeec' align='center'>7</td><td bgcolor='#eeeeec' align='center'>0.0605</td><td bgcolor='#eeeeec' align='right'>3006152</td><td bgcolor='#eeeeec'>WebAdminxAjax::call(  )</td><td title='D:\wamp64\www\pvingles.local\sys_lib\xajax\xajax_core\xajaxAIO.inc.php' bgcolor='#eeeeec'>...\xajaxAIO.inc.php<b>:</b>1077</td></tr>
<tr><td bgcolor='#eeeeec' align='center'>8</td><td bgcolor='#eeeeec' align='center'>0.0606</td><td bgcolor='#eeeeec' align='right'>3008640</td><td bgcolor='#eeeeec'>WebAdminxAjax::recurso(  )</td><td title='D:\wamp64\www\pvingles.local\sys_inc\cls.WebAdminxAjax.php' bgcolor='#eeeeec'>...\cls.WebAdminxAjax.php<b>:</b>25</td></tr>
<tr><td bgcolor='#eeeeec' align='center'>9</td><td bgcolor='#eeeeec' align='center'>0.0617</td><td bgcolor='#eeeeec' align='right'>3116280</td><td bgcolor='#eeeeec'>WebGenerador->xsetGenerarPagina(  )</td><td title='D:\wamp64\www\pvingles.local\sys_inc\cls.WebAdminxAjax.php' bgcolor='#eeeeec'>...\cls.WebAdminxAjax.php<b>:</b>66</td></tr>
<tr><td bgcolor='#eeeeec' align='center'>10</td><td bgcolor='#eeeeec' align='center'>0.6425</td><td bgcolor='#eeeeec' align='right'>3181568</td><td bgcolor='#eeeeec'>JrWeb->getEsquema(  )</td><td title='D:\wamp64\www\pvingles.local\frontend\sys_web\cls.WebGenerador.php' bgcolor='#eeeeec'>...\cls.WebGenerador.php<b>:</b>141</td></tr>
<tr><td bgcolor='#eeeeec' align='center'>11</td><td bgcolor='#eeeeec' align='center'>0.6437</td><td bgcolor='#eeeeec' align='right'>3348808</td><td bgcolor='#eeeeec'>require_once( <font color='#00bb00'>'D:\wamp64\www\pvingles.local\frontend\sys_web\html\generador\frm.php'</font> )</td><td title='D:\wamp64\www\pvingles.local\sys_lib\jrAdwen\cls.JrWeb.php' bgcolor='#eeeeec'>...\cls.JrWeb.php<b>:</b>34</td></tr>
</table></font>
<br />
<font size='1'><table class='xdebug-error xe-notice' dir='ltr' border='1' cellspacing='0' cellpadding='1'>
<tr><th align='left' bgcolor='#f57900' colspan="5"><span style='background-color: #cc0000; color: #fce94f; font-size: x-large;'>( ! )</span> Notice: Undefined index: tipofkcomo_regusuario in D:\wamp64\www\pvingles.local\frontend\sys_web\html\generador\frm.php on line <i>357</i></th></tr>
<tr><th align='left' bgcolor='#e9b96e' colspan='5'>Call Stack</th></tr>
<tr><th align='center' bgcolor='#eeeeec'>#</th><th align='left' bgcolor='#eeeeec'>Time</th><th align='left' bgcolor='#eeeeec'>Memory</th><th align='left' bgcolor='#eeeeec'>Function</th><th align='left' bgcolor='#eeeeec'>Location</th></tr>
<tr><td bgcolor='#eeeeec' align='center'>1</td><td bgcolor='#eeeeec' align='center'>0.0003</td><td bgcolor='#eeeeec' align='right'>283200</td><td bgcolor='#eeeeec'>{main}(  )</td><td title='D:\wamp64\www\pvingles.local\frontend\ServerxAjax.php' bgcolor='#eeeeec'>...\ServerxAjax.php<b>:</b>0</td></tr>
<tr><td bgcolor='#eeeeec' align='center'>2</td><td bgcolor='#eeeeec' align='center'>0.0556</td><td bgcolor='#eeeeec' align='right'>2966464</td><td bgcolor='#eeeeec'>xajax->processRequest(  )</td><td title='D:\wamp64\www\pvingles.local\frontend\ServerxAjax.php' bgcolor='#eeeeec'>...\ServerxAjax.php<b>:</b>19</td></tr>
<tr><td bgcolor='#eeeeec' align='center'>3</td><td bgcolor='#eeeeec' align='center'>0.0557</td><td bgcolor='#eeeeec' align='right'>2966928</td><td bgcolor='#eeeeec'>xajaxPluginManager->processRequest(  )</td><td title='D:\wamp64\www\pvingles.local\sys_lib\xajax\xajax_core\xajaxAIO.inc.php' bgcolor='#eeeeec'>...\xajaxAIO.inc.php<b>:</b>445</td></tr>
<tr><td bgcolor='#eeeeec' align='center'>4</td><td bgcolor='#eeeeec' align='center'>0.0557</td><td bgcolor='#eeeeec' align='right'>2968072</td><td bgcolor='#eeeeec'>xajaxFunctionPlugin->processRequest(  )</td><td title='D:\wamp64\www\pvingles.local\sys_lib\xajax\xajax_core\xajaxAIO.inc.php' bgcolor='#eeeeec'>...\xajaxAIO.inc.php<b>:</b>51</td></tr>
<tr><td bgcolor='#eeeeec' align='center'>5</td><td bgcolor='#eeeeec' align='center'>0.0602</td><td bgcolor='#eeeeec' align='right'>2968880</td><td bgcolor='#eeeeec'>xajaxUserFunction->call(  )</td><td title='D:\wamp64\www\pvingles.local\sys_lib\xajax\xajax_core\xajaxAIO.inc.php' bgcolor='#eeeeec'>...\xajaxAIO.inc.php<b>:</b>1013</td></tr>
<tr><td bgcolor='#eeeeec' align='center'>6</td><td bgcolor='#eeeeec' align='center'>0.0605</td><td bgcolor='#eeeeec' align='right'>3005376</td><td bgcolor='#eeeeec'><a href='http://www.php.net/function.call-user-func-array:{D:\wamp64\www\pvingles.local\sys-lib\xajax\xajax-core\xajaxAIO.inc.php:1077}' target='_new'>call_user_func_array:{D:\wamp64\www\pvingles.local\sys_lib\xajax\xajax_core\xajaxAIO.inc.php:1077}</a>
(  )</td><td title='D:\wamp64\www\pvingles.local\sys_lib\xajax\xajax_core\xajaxAIO.inc.php' bgcolor='#eeeeec'>...\xajaxAIO.inc.php<b>:</b>1077</td></tr>
<tr><td bgcolor='#eeeeec' align='center'>7</td><td bgcolor='#eeeeec' align='center'>0.0605</td><td bgcolor='#eeeeec' align='right'>3006152</td><td bgcolor='#eeeeec'>WebAdminxAjax::call(  )</td><td title='D:\wamp64\www\pvingles.local\sys_lib\xajax\xajax_core\xajaxAIO.inc.php' bgcolor='#eeeeec'>...\xajaxAIO.inc.php<b>:</b>1077</td></tr>
<tr><td bgcolor='#eeeeec' align='center'>8</td><td bgcolor='#eeeeec' align='center'>0.0606</td><td bgcolor='#eeeeec' align='right'>3008640</td><td bgcolor='#eeeeec'>WebAdminxAjax::recurso(  )</td><td title='D:\wamp64\www\pvingles.local\sys_inc\cls.WebAdminxAjax.php' bgcolor='#eeeeec'>...\cls.WebAdminxAjax.php<b>:</b>25</td></tr>
<tr><td bgcolor='#eeeeec' align='center'>9</td><td bgcolor='#eeeeec' align='center'>0.0617</td><td bgcolor='#eeeeec' align='right'>3116280</td><td bgcolor='#eeeeec'>WebGenerador->xsetGenerarPagina(  )</td><td title='D:\wamp64\www\pvingles.local\sys_inc\cls.WebAdminxAjax.php' bgcolor='#eeeeec'>...\cls.WebAdminxAjax.php<b>:</b>66</td></tr>
<tr><td bgcolor='#eeeeec' align='center'>10</td><td bgcolor='#eeeeec' align='center'>0.6425</td><td bgcolor='#eeeeec' align='right'>3181568</td><td bgcolor='#eeeeec'>JrWeb->getEsquema(  )</td><td title='D:\wamp64\www\pvingles.local\frontend\sys_web\cls.WebGenerador.php' bgcolor='#eeeeec'>...\cls.WebGenerador.php<b>:</b>141</td></tr>
<tr><td bgcolor='#eeeeec' align='center'>11</td><td bgcolor='#eeeeec' align='center'>0.6437</td><td bgcolor='#eeeeec' align='right'>3348808</td><td bgcolor='#eeeeec'>require_once( <font color='#00bb00'>'D:\wamp64\www\pvingles.local\frontend\sys_web\html\generador\frm.php'</font> )</td><td title='D:\wamp64\www\pvingles.local\sys_lib\jrAdwen\cls.JrWeb.php' bgcolor='#eeeeec'>...\cls.JrWeb.php<b>:</b>34</td></tr>
</table></font>
<br />
<font size='1'><table class='xdebug-error xe-notice' dir='ltr' border='1' cellspacing='0' cellpadding='1'>
<tr><th align='left' bgcolor='#f57900' colspan="5"><span style='background-color: #cc0000; color: #fce94f; font-size: x-large;'>( ! )</span> Notice: Undefined index: tipofkcomo_regusuario in D:\wamp64\www\pvingles.local\frontend\sys_web\html\generador\frm.php on line <i>359</i></th></tr>
<tr><th align='left' bgcolor='#e9b96e' colspan='5'>Call Stack</th></tr>
<tr><th align='center' bgcolor='#eeeeec'>#</th><th align='left' bgcolor='#eeeeec'>Time</th><th align='left' bgcolor='#eeeeec'>Memory</th><th align='left' bgcolor='#eeeeec'>Function</th><th align='left' bgcolor='#eeeeec'>Location</th></tr>
<tr><td bgcolor='#eeeeec' align='center'>1</td><td bgcolor='#eeeeec' align='center'>0.0003</td><td bgcolor='#eeeeec' align='right'>283200</td><td bgcolor='#eeeeec'>{main}(  )</td><td title='D:\wamp64\www\pvingles.local\frontend\ServerxAjax.php' bgcolor='#eeeeec'>...\ServerxAjax.php<b>:</b>0</td></tr>
<tr><td bgcolor='#eeeeec' align='center'>2</td><td bgcolor='#eeeeec' align='center'>0.0556</td><td bgcolor='#eeeeec' align='right'>2966464</td><td bgcolor='#eeeeec'>xajax->processRequest(  )</td><td title='D:\wamp64\www\pvingles.local\frontend\ServerxAjax.php' bgcolor='#eeeeec'>...\ServerxAjax.php<b>:</b>19</td></tr>
<tr><td bgcolor='#eeeeec' align='center'>3</td><td bgcolor='#eeeeec' align='center'>0.0557</td><td bgcolor='#eeeeec' align='right'>2966928</td><td bgcolor='#eeeeec'>xajaxPluginManager->processRequest(  )</td><td title='D:\wamp64\www\pvingles.local\sys_lib\xajax\xajax_core\xajaxAIO.inc.php' bgcolor='#eeeeec'>...\xajaxAIO.inc.php<b>:</b>445</td></tr>
<tr><td bgcolor='#eeeeec' align='center'>4</td><td bgcolor='#eeeeec' align='center'>0.0557</td><td bgcolor='#eeeeec' align='right'>2968072</td><td bgcolor='#eeeeec'>xajaxFunctionPlugin->processRequest(  )</td><td title='D:\wamp64\www\pvingles.local\sys_lib\xajax\xajax_core\xajaxAIO.inc.php' bgcolor='#eeeeec'>...\xajaxAIO.inc.php<b>:</b>51</td></tr>
<tr><td bgcolor='#eeeeec' align='center'>5</td><td bgcolor='#eeeeec' align='center'>0.0602</td><td bgcolor='#eeeeec' align='right'>2968880</td><td bgcolor='#eeeeec'>xajaxUserFunction->call(  )</td><td title='D:\wamp64\www\pvingles.local\sys_lib\xajax\xajax_core\xajaxAIO.inc.php' bgcolor='#eeeeec'>...\xajaxAIO.inc.php<b>:</b>1013</td></tr>
<tr><td bgcolor='#eeeeec' align='center'>6</td><td bgcolor='#eeeeec' align='center'>0.0605</td><td bgcolor='#eeeeec' align='right'>3005376</td><td bgcolor='#eeeeec'><a href='http://www.php.net/function.call-user-func-array:{D:\wamp64\www\pvingles.local\sys-lib\xajax\xajax-core\xajaxAIO.inc.php:1077}' target='_new'>call_user_func_array:{D:\wamp64\www\pvingles.local\sys_lib\xajax\xajax_core\xajaxAIO.inc.php:1077}</a>
(  )</td><td title='D:\wamp64\www\pvingles.local\sys_lib\xajax\xajax_core\xajaxAIO.inc.php' bgcolor='#eeeeec'>...\xajaxAIO.inc.php<b>:</b>1077</td></tr>
<tr><td bgcolor='#eeeeec' align='center'>7</td><td bgcolor='#eeeeec' align='center'>0.0605</td><td bgcolor='#eeeeec' align='right'>3006152</td><td bgcolor='#eeeeec'>WebAdminxAjax::call(  )</td><td title='D:\wamp64\www\pvingles.local\sys_lib\xajax\xajax_core\xajaxAIO.inc.php' bgcolor='#eeeeec'>...\xajaxAIO.inc.php<b>:</b>1077</td></tr>
<tr><td bgcolor='#eeeeec' align='center'>8</td><td bgcolor='#eeeeec' align='center'>0.0606</td><td bgcolor='#eeeeec' align='right'>3008640</td><td bgcolor='#eeeeec'>WebAdminxAjax::recurso(  )</td><td title='D:\wamp64\www\pvingles.local\sys_inc\cls.WebAdminxAjax.php' bgcolor='#eeeeec'>...\cls.WebAdminxAjax.php<b>:</b>25</td></tr>
<tr><td bgcolor='#eeeeec' align='center'>9</td><td bgcolor='#eeeeec' align='center'>0.0617</td><td bgcolor='#eeeeec' align='right'>3116280</td><td bgcolor='#eeeeec'>WebGenerador->xsetGenerarPagina(  )</td><td title='D:\wamp64\www\pvingles.local\sys_inc\cls.WebAdminxAjax.php' bgcolor='#eeeeec'>...\cls.WebAdminxAjax.php<b>:</b>66</td></tr>
<tr><td bgcolor='#eeeeec' align='center'>10</td><td bgcolor='#eeeeec' align='center'>0.6425</td><td bgcolor='#eeeeec' align='right'>3181568</td><td bgcolor='#eeeeec'>JrWeb->getEsquema(  )</td><td title='D:\wamp64\www\pvingles.local\frontend\sys_web\cls.WebGenerador.php' bgcolor='#eeeeec'>...\cls.WebGenerador.php<b>:</b>141</td></tr>
<tr><td bgcolor='#eeeeec' align='center'>11</td><td bgcolor='#eeeeec' align='center'>0.6437</td><td bgcolor='#eeeeec' align='right'>3348808</td><td bgcolor='#eeeeec'>require_once( <font color='#00bb00'>'D:\wamp64\www\pvingles.local\frontend\sys_web\html\generador\frm.php'</font> )</td><td title='D:\wamp64\www\pvingles.local\sys_lib\jrAdwen\cls.JrWeb.php' bgcolor='#eeeeec'>...\cls.JrWeb.php<b>:</b>34</td></tr>
</table></font>
<br />
<font size='1'><table class='xdebug-error xe-notice' dir='ltr' border='1' cellspacing='0' cellpadding='1'>
<tr><th align='left' bgcolor='#f57900' colspan="5"><span style='background-color: #cc0000; color: #fce94f; font-size: x-large;'>( ! )</span> Notice: Undefined index: tipofkcomo_regusuario in D:\wamp64\www\pvingles.local\frontend\sys_web\html\generador\frm.php on line <i>361</i></th></tr>
<tr><th align='left' bgcolor='#e9b96e' colspan='5'>Call Stack</th></tr>
<tr><th align='center' bgcolor='#eeeeec'>#</th><th align='left' bgcolor='#eeeeec'>Time</th><th align='left' bgcolor='#eeeeec'>Memory</th><th align='left' bgcolor='#eeeeec'>Function</th><th align='left' bgcolor='#eeeeec'>Location</th></tr>
<tr><td bgcolor='#eeeeec' align='center'>1</td><td bgcolor='#eeeeec' align='center'>0.0003</td><td bgcolor='#eeeeec' align='right'>283200</td><td bgcolor='#eeeeec'>{main}(  )</td><td title='D:\wamp64\www\pvingles.local\frontend\ServerxAjax.php' bgcolor='#eeeeec'>...\ServerxAjax.php<b>:</b>0</td></tr>
<tr><td bgcolor='#eeeeec' align='center'>2</td><td bgcolor='#eeeeec' align='center'>0.0556</td><td bgcolor='#eeeeec' align='right'>2966464</td><td bgcolor='#eeeeec'>xajax->processRequest(  )</td><td title='D:\wamp64\www\pvingles.local\frontend\ServerxAjax.php' bgcolor='#eeeeec'>...\ServerxAjax.php<b>:</b>19</td></tr>
<tr><td bgcolor='#eeeeec' align='center'>3</td><td bgcolor='#eeeeec' align='center'>0.0557</td><td bgcolor='#eeeeec' align='right'>2966928</td><td bgcolor='#eeeeec'>xajaxPluginManager->processRequest(  )</td><td title='D:\wamp64\www\pvingles.local\sys_lib\xajax\xajax_core\xajaxAIO.inc.php' bgcolor='#eeeeec'>...\xajaxAIO.inc.php<b>:</b>445</td></tr>
<tr><td bgcolor='#eeeeec' align='center'>4</td><td bgcolor='#eeeeec' align='center'>0.0557</td><td bgcolor='#eeeeec' align='right'>2968072</td><td bgcolor='#eeeeec'>xajaxFunctionPlugin->processRequest(  )</td><td title='D:\wamp64\www\pvingles.local\sys_lib\xajax\xajax_core\xajaxAIO.inc.php' bgcolor='#eeeeec'>...\xajaxAIO.inc.php<b>:</b>51</td></tr>
<tr><td bgcolor='#eeeeec' align='center'>5</td><td bgcolor='#eeeeec' align='center'>0.0602</td><td bgcolor='#eeeeec' align='right'>2968880</td><td bgcolor='#eeeeec'>xajaxUserFunction->call(  )</td><td title='D:\wamp64\www\pvingles.local\sys_lib\xajax\xajax_core\xajaxAIO.inc.php' bgcolor='#eeeeec'>...\xajaxAIO.inc.php<b>:</b>1013</td></tr>
<tr><td bgcolor='#eeeeec' align='center'>6</td><td bgcolor='#eeeeec' align='center'>0.0605</td><td bgcolor='#eeeeec' align='right'>3005376</td><td bgcolor='#eeeeec'><a href='http://www.php.net/function.call-user-func-array:{D:\wamp64\www\pvingles.local\sys-lib\xajax\xajax-core\xajaxAIO.inc.php:1077}' target='_new'>call_user_func_array:{D:\wamp64\www\pvingles.local\sys_lib\xajax\xajax_core\xajaxAIO.inc.php:1077}</a>
(  )</td><td title='D:\wamp64\www\pvingles.local\sys_lib\xajax\xajax_core\xajaxAIO.inc.php' bgcolor='#eeeeec'>...\xajaxAIO.inc.php<b>:</b>1077</td></tr>
<tr><td bgcolor='#eeeeec' align='center'>7</td><td bgcolor='#eeeeec' align='center'>0.0605</td><td bgcolor='#eeeeec' align='right'>3006152</td><td bgcolor='#eeeeec'>WebAdminxAjax::call(  )</td><td title='D:\wamp64\www\pvingles.local\sys_lib\xajax\xajax_core\xajaxAIO.inc.php' bgcolor='#eeeeec'>...\xajaxAIO.inc.php<b>:</b>1077</td></tr>
<tr><td bgcolor='#eeeeec' align='center'>8</td><td bgcolor='#eeeeec' align='center'>0.0606</td><td bgcolor='#eeeeec' align='right'>3008640</td><td bgcolor='#eeeeec'>WebAdminxAjax::recurso(  )</td><td title='D:\wamp64\www\pvingles.local\sys_inc\cls.WebAdminxAjax.php' bgcolor='#eeeeec'>...\cls.WebAdminxAjax.php<b>:</b>25</td></tr>
<tr><td bgcolor='#eeeeec' align='center'>9</td><td bgcolor='#eeeeec' align='center'>0.0617</td><td bgcolor='#eeeeec' align='right'>3116280</td><td bgcolor='#eeeeec'>WebGenerador->xsetGenerarPagina(  )</td><td title='D:\wamp64\www\pvingles.local\sys_inc\cls.WebAdminxAjax.php' bgcolor='#eeeeec'>...\cls.WebAdminxAjax.php<b>:</b>66</td></tr>
<tr><td bgcolor='#eeeeec' align='center'>10</td><td bgcolor='#eeeeec' align='center'>0.6425</td><td bgcolor='#eeeeec' align='right'>3181568</td><td bgcolor='#eeeeec'>JrWeb->getEsquema(  )</td><td title='D:\wamp64\www\pvingles.local\frontend\sys_web\cls.WebGenerador.php' bgcolor='#eeeeec'>...\cls.WebGenerador.php<b>:</b>141</td></tr>
<tr><td bgcolor='#eeeeec' align='center'>11</td><td bgcolor='#eeeeec' align='center'>0.6437</td><td bgcolor='#eeeeec' align='right'>3348808</td><td bgcolor='#eeeeec'>require_once( <font color='#00bb00'>'D:\wamp64\www\pvingles.local\frontend\sys_web\html\generador\frm.php'</font> )</td><td title='D:\wamp64\www\pvingles.local\sys_lib\jrAdwen\cls.JrWeb.php' bgcolor='#eeeeec'>...\cls.JrWeb.php<b>:</b>34</td></tr>
</table></font>
                    
              </div>
            </div>

              <input type="hidden" id="txtRegfecha" name="txtRegfecha" value="<?php echo !empty($frm["regfecha"])?$frm["regfecha"]:date("Y/m/d") ?>">

              <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtUsuario">
              <?php echo JrTexto::_('Usuario');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtUsuario" name="txtUsuario" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["usuario"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtClave">
              <?php echo JrTexto::_('Clave');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
               <input type="password" id="txtClave" name="txtClave" class="form-control" required /> 
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtToken">
              <?php echo JrTexto::_('Token');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtToken" name="txtToken" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["token"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtRol">
              <?php echo JrTexto::_('Rol');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
              <select id="txtrol" name="txtRol" class="form-control">
               <option value=""><?php echo JrTexto::_('Seleccione'); ?></option>
              <?php 
              if(!empty($this->fkrol))
                foreach ($this->fkrol as $fkrol) { ?><option value="<?php echo $fkrol["idrol"]?>" <?php echo $fkrol["idrol"]==@$frm["rol"]?"selected":""; ?> ><?php echo $fkrol["rol"] ?></option><?php } ?>              </select>
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtFoto">
              <?php echo JrTexto::_('Foto');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
               
                                <div class="row">
                  <input type="hidden" name="txtFoto" id="txtFoto">
                  <input type="hidden" name="txtFoto_old" value="<?php echo @$frm["foto"];?>">
                  <div class="col-md-12">
                    <div class="img-container">
                      <img src="<?php echo !empty($frm["foto"])?$this->documento->getUrlBase().$frm["foto"]:($this->documento->getUrlStatic()."/media/personal/default.png");?>" alt="imgfoto" data-ancho="100"  data-alto="100">
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12 docs-buttons text-center">
                    <div class="btn-group">
                      <label class="btn btn-info btn-upload" for="inputImagefoto" title="Upload image file">
                        <input class="sr-only btncropper-upload" alt="imgfoto" id="inputImagefoto" name="file" type="file" accept="image/*">
                        <span class="docs-tooltip" data-toggle="tooltip" title="<?php echo JrTexto::_("Seleccionar imagen");?>">
                        <span class="fa fa-upload"></span></span>
                      </label>
                      <button class="btn btn-primary btncropper" data-image="imgfoto"  data-method="setDragMode" data-option="move" type="button">
                        <span class="docs-tooltip" data-toggle="tooltip" title="<?php echo JrTexto::_("Mover imagen");?>"><span class="fa fa-arrows"></span></span>
                      </button>
                      <button class="btn btn-primary btncropper" data-image="imgfoto" data-method="zoom" data-option="0.1" type="button" >
                        <span class="docs-tooltip" data-toggle="tooltip" title="<?php echo JrTexto::_("Aumentar Zoom");?>"><span class="fa fa-search-plus"></span></span>
                      </button>
                      <button class="btn btn-primary btncropper" data-image="imgfoto" data-method="zoom" data-option="-0.1" type="button">
                        <span class="docs-tooltip" data-toggle="tooltip" title="<?php echo JrTexto::_("Disminuir Zoom");?>"><span class="fa fa-search-minus"></span></span>
                      </button>
                      <button class="btn btn-primary btncropper" data-image="imgfoto" data-method="rotate" data-option="-45" type="button">
                        <span class="docs-tooltip" data-toggle="tooltip" title="<?php echo JrTexto::_("Rotar a la Izquierda");?>" ><span class="fa fa-rotate-left"></span></span>
                      </button>
                      <button class="btn btn-primary btncropper" data-image="imgfoto" data-method="rotate" data-option="45" type="button">
                        <span class="docs-tooltip" data-toggle="tooltip" title="<?php echo JrTexto::_("Rotar a la Derecha");?>" ><span class="fa fa-rotate-right"></span></span>
                      </button>
                      <button class="btn btn-primary btncropper" data-image="imgfoto" data-method="reset" type="button">
                        <span class="docs-tooltip" data-toggle="tooltip" title="<?php echo JrTexto::_("Resetear Imagen");?>"><span class="fa fa-refresh"></span></span>
                      </button>
                    </div>
                  </div>
                </div>

                                 
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtEstado">
              <?php echo JrTexto::_('Estado');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                                <a style="cursor:pointer;" class="chkformulario fa  <?php echo @$frm["estado"]==1?"fa-check-circle":"fa-circle-o";?>" 
                data-value="<?php echo @$frm["estado"];?>"  data-valueno="0" data-value2="<?php echo @$frm["estado"]==1?1:0;?>">
                 <span> <?php echo JrTexto::_(@$frm["estado"]==1?"Activo":"Inactivo");?></span>
                 <input type="hidden" name="txtEstado" value="<?php echo !empty($frm["estado"])?$frm["estado"]:0;?>" > 
                 </a>
                                                      
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtSituacion">
              <?php echo JrTexto::_('Situacion');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                                <a style="cursor:pointer;" class="chkformulario fa  <?php echo @$frm["situacion"]==1?"fa-check-circle":"fa-circle-o";?>" 
                data-value="<?php echo @$frm["situacion"];?>"  data-valueno="0" data-value2="<?php echo @$frm["situacion"]==1?1:0;?>">
                 <span> <?php echo JrTexto::_(@$frm["situacion"]==1?"Activo":"Inactivo");?></span>
                 <input type="hidden" name="txtSituacion" value="<?php echo !empty($frm["situacion"])?$frm["situacion"]:0;?>" > 
                 </a>
                                                      
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtIdioma">
              <?php echo JrTexto::_('Idioma');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtIdioma" name="txtIdioma" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["idioma"];?>">
                                  
              </div>
            </div>

            
          <div class="ln_solid"></div>
          <div class="form-group">
            <div class="col-md-6 col-md-offset-4">
              <button id="btn-savePersonal" type="submit" class="btn btn-success" ><i class=" fa fa-save"></i> <?php echo JrTexto::_('Save');?> </button>
              <a type="button" class="btn btn-warning btn-close" href="<?php echo JrAplicacion::getJrUrl(array('personal'))?>" data-dismiss="modal"  ><i class=" fa fa-repeat"></i> <?php echo JrTexto::_('Cancel');?></a>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function(){  
            
$('#frm-<?php echo $id_vent;?>').bind({    
     submit: function(event){
      event.preventDefault();
      $(this).attr('disabled', true); 
      var res = xajax__('', 'personal', 'savePersonal', xajax.getFormValues('frm-<?php echo $id_vent;?>'));
      if(res){
        if(typeof <?php echo $ventanapadre?> == 'function'){
          <?php echo $ventanapadre?>(res);
          $(this).closest('.modal').find('.cerrarmodal').trigger('click');
        }else return redir('<?php echo JrAplicacion::getJrUrl(array("Personal"))?>');
      }
     }
  });

 
  
  $('.cargarfile').on('change',function(){
    var file=$(this);
     agregar_msj_interno('success', '<?php echo JrTexto::_("loading");?> '+file.attr('data-texto')+'...','msj-interno',true);
     $('#frmPersonal').attr('action',file.attr('data-action'));
     $('#frmPersonal').attr('target','if_cargar_'+file.attr('data-campo'));
     $('#frmPersonal').attr('enctype','multipart/form-data');
     $('#frmPersonal').submit();                
  });
    
});

$('.chkformulario').bind({
    click: function() {     
      if($(this).hasClass('fa-circle-o')) {
        $('span',this).text(' <?php echo JrTexto::_("Active");?>');
        $('input',this).val(1);
        $(this).removeClass('fa-circle-o').addClass('fa-check-circle');
      }else {
        $('span',this).text(' <?php echo JrTexto::_("Inactive");?>');
        $('input',this).val(0);
        $(this).addClass('fa-circle-o').removeClass('fa-check-circle');
      }      
    }
  });

function result_archivo(result, mensaje,txt) {
  if(result) {
    $("#txt"+txt).val(mensaje);
    agregar_msj_interno2("success", '<?php echo JrTexto::_("File updated successfully");?>',"msj-interno2",true);
    var tipo=$("#id"+txt).attr("data-type");
    if(tipo=="imagen"||tipo=="video"||tipo=="audio")
      $("#id"+txt).removeAttr("style").attr("src", "https://192.168.11.55/pvingles.local" + mensaje );
    else
      $("#id"+txt).removeAttr("style").attr("href",  "https://192.168.11.55/pvingles.local" + mensaje );
    }else{
      agregar_msj_interno2("warning", mensaje,"msj-interno2",true);
    }
}
  
</script>

