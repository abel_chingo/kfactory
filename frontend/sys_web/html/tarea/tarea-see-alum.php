<?php 
$RUTA_BASE = $this->documento->getUrlBase();
$tarea = @$this->datos;
$asig_alum=@$tarea['tarea_asignacion']['detalle'][0];
$arrIcon=array('D'=> 'fa fa-paperclip', 'V'=> 'fa fa-video-camera', 'L'=> 'fa fa-link', 'G'=> 'fa fa-microphone', 'A'=> 'fa fa-font', 'J'=> 'fa fa-puzzle-piece', 'E'=> 'fa fa-list' );
/*
var_dump($tarea);
*/
 ?>
<link rel="stylesheet" type="text/css" href="<?php echo $this->documento->getUrlTema()?>/tarea/general.css">
<input type="hidden" name="hIdTarea_Asignacion_Alumno" id="hIdTarea_Asignacion_Alumno" value="<?php echo @$asig_alum['iddetalle']; ?>">
<input type="hidden" name="hIdTarea_Respuesta" id="hIdTarea_Respuesta" value="<?php echo @$asig_alum['idtarea_respuesta']; ?>">
<input type="hidden" name="hIdTarea_Estado" id="hIdTarea_Estado" value="<?php echo @$asig_alum['estado']; ?>">
<div class="" id="tarea-see">
    <div class="row"> <div class="col-xs-12">
        <ol class="breadcrumb">
            <li><a href="<?php echo $RUTA_BASE;?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_('Home'); ?></a></li>                  
            <li><a href="<?php echo $RUTA_BASE;?>/tarea"><?php echo ucfirst(JrTexto::_('Homework')); ?></a></li>
            <li class="active"><?php echo $this->breadcrumb; ?></li>
        </ol>
    </div> </div>

    <div class="row">
        <div class="col-xs-12 col-sm-4">
            <div class="panel pnl-contenedor panel-default tarea-item" data-idtarea="<?php echo $tarea['idtarea']; ?>">
                <div class="panel-heading titulo">
                    <h3><?php echo $tarea['nombre']; ?></h3>
                </div>
                <div class="panel-body contenido">
                    <div class="col-xs-5" style="padding-right: 0;">
                        <?php $src = $this->documento->getUrlStatic().'/media/web/nofoto.jpg';
                        if(!empty($tarea['foto'])){ $src = str_replace('__xRUTABASEx__', $RUTA_BASE, $tarea['foto']); }  ?>
                        <img src="<?php echo $src;?>" alt="img" class="img-responsive">
                    </div>
                    <div class="col-xs-7 descripcion" style="overflow: auto;">
                        <?php echo $tarea['descripcion']; ?>
                    </div>
                    <div class="col-xs-12" style="padding:0;">
                        <div class="table-key-value">
                            <div class="table-kv-row">
                                 <div class="key"><?php echo ucfirst(JrTexto::_("Date of presentation")); ?></div>
                                 <div class="value"><?php echo  date('d-m-Y', strtotime($tarea['fechaentrega'])).' '. date('h:ia', strtotime($tarea['horaentrega'])); ?></div>
                             </div>
                             <div class="table-kv-row">
                                <div class="key"><?php echo ucfirst(JrTexto::_("Level")); ?></div>
                                <div class="value"><?php echo $tarea['nivel']['nombre']; ?></div>
                            </div>
                            <div class="table-kv-row">
                                <div class="key"><?php echo ucfirst(JrTexto::_("Unit")); ?></div>
                                <div class="value"><?php echo $tarea['unidad']['nombre']; ?></div>
                            </div>
                            <div class="table-kv-row">
                                <div class="key"><?php echo ucfirst(JrTexto::_("Activity")); ?></div>
                                <div class="value"><?php echo $tarea['actividad']['nombre']; ?></div>
                            </div>
                            <div class="table-kv-row puntajemaximo">
                                <div class="key"><?php echo ucfirst(JrTexto::_("Maximum score")); ?></div>
                                <div class="value"><?php echo $tarea['puntajemaximo']; ?></div>
                            </div>
                            <div class="table-kv-row puntajeminimo">
                                <div class="key"><?php echo ucfirst(JrTexto::_("Minimum score to pass")); ?></div>
                                <div class="value"><?php echo $tarea['puntajeminimo']; ?></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div style="max-height: 270px; overflow: auto;">
                    <table class="table table-striped">
                        <tbody>
                        <?php if(!empty($tarea['tarea_archivos'])){
                            foreach ($tarea['tarea_archivos'] as $adj){ ?>
                            <tr data-id="<?php echo $adj['idtarea_archivos'] ?>">
                                <td style="width:7%"><i class="<?php echo $arrIcon[$adj['tipo']] ?>"></i></td>
                                <td><?php echo $adj['nombre'] ?></td>
                                <td style="width:18%">
                                    <a href="<?php $ruta=$adj['ruta'];
                                    if($adj['tipo']=='A' || $adj['tipo']=='J' || $adj['tipo']=='E'){
                                        $ruta.=$adj['idtarea_archivos'].'&idasig_alum='.@$asig_alum['iddetalle'];
                                    }
                                    echo str_replace('__xRUTABASEx__', $RUTA_BASE, $ruta); ?>" class="btn btn-xs btn-default color-info verarchivo" title="<?php echo ucfirst(JrTexto::_('View')); ?>" target="_blank"><i class="fa fa-eye"></i></a>
                                </td>
                            </tr>
                        <?php } } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-sm-8">
            <div class="panel pnl-contenedor tarea_respuesta">
                <?php if(@$asig_alum['estado']!='N' && @$asig_alum['estado']!='D'){ ?>
                <div class="panel-heading text-right <?php echo (@$asig_alum['notapromedio']=='')?'bg-default':((@$asig_alum['notapromedio']>=$tarea['puntajeminimo'])?'bg-primary':'bg-danger'); ?>">
                    <h3><?php echo ucfirst(JrTexto::_('Average score')).':'; ?>
                        <i><?php echo (@$asig_alum['notapromedio']!='')?@$asig_alum['notapromedio']:'---'; ?></i>
                        <small>/ <?php echo round($tarea['puntajemaximo']); ?></small>
                    </h3>
                </div>
                <?php } ?>
                <div class="panel-body">
                    <div class="row">
                        <?php if(@$asig_alum['estado']=='D'){ ?>
                        <div class="col-xs-12 form-group">
                            <div class="alert alert-warning" role="alert"> 
                                <strong><?php echo ucfirst(JrTexto::_('Teacher returning message')); ?>:</strong>
                                <p><?php echo @$asig_alum['mensajedevolucion']; ?></p>
                            </div>
                        </div>
                        <?php } ?>

                        <div class="col-xs-12 form-group">
                            <label for="txtDescripcion" class="control-label col-xs-12 col-sm-3"><?php echo ucfirst(JrTexto::_('Attachment')); ?></label>

                            <?php if(@$asig_alum['estado']=='N' || @$asig_alum['estado']=='D'){ ?>
                            <div class="col-xs-12 col-sm-9">
                                <input type="file" name="inp_CargadorArchivos" id="inp_CargadorArchivos" class="hidden">
                                <button class="btn btn-default btnadjunto istooltip" data-adjunto="documento" title="<?php echo JrTexto::_('Document'); ?>"><i class="fa fa-paperclip"></i></button>
                                <button class="btn btn-default btnadjunto istooltip" data-adjunto="video" title="<?php echo JrTexto::_('Video'); ?>"><i class="fa fa-video-camera"></i></button>
                                <button class="btn btn-default btnadjunto istooltip" data-adjunto="grabacionvoz" title="<?php echo JrTexto::_('Voice recording'); ?>"><i class="fa fa-microphone"></i></button>
                                <button class="btn btn-default btnadjunto istooltip" data-adjunto="enlace" title="<?php echo JrTexto::_('Link'); ?>"><i class="fa fa-link"></i></button>
                            </div>
                            <?php } ?>

                            <div class="col-xs-12" style="height: 20px;">
                                <div id="barra-progreso" style="display: none;">
                                    <div class="progress" style="margin: 0;">
                                        <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
                                            <span class="sr-only">0%</span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-xs-12 lista-archivos_respuesta" style="max-height: 300px; overflow: auto;">
                                <table class="table table-striped" id="tblArchivosRespuesta">
                                    <tbody>
                                    <?php 
                                    if(!empty(@$asig_alum['respuesta_archivos'])){
                                        foreach (@$asig_alum['respuesta_archivos'] as $adj) { ?>
                                        <tr data-id="<?php echo $adj['idtarea_archivos'] ?>">
                                            <td style="width: 7%"><i class="<?php echo $arrIcon[$adj['tipo']] ?>"></i></td>
                                            <td><?php echo $adj['nombre'] ?></td>
                                            <td style="width: 18%">
                                                <a href="<?php $ruta=$adj['ruta'];
                                                if($adj['tipo']=='A' || $adj['tipo']=='J' || $adj['tipo']=='E'){
                                                    $ruta.=$adj['idtarea_archivos_padre'].'&idasig_alum='.@$asig_alum['iddetalle'];
                                                }
                                                echo str_replace('__xRUTABASEx__', $RUTA_BASE, $ruta) ?>" class="btn btn-xs btn-default color-info verarchivo" title="<?php echo ucfirst(JrTexto::_('View')); ?>" target="_blank"><i class="fa fa-eye"></i></a>
                                                
                                                <?php if(@$asig_alum['estado']=='N' || @$asig_alum['estado']=='D'){ ?>
                                                <button title="<?php echo ucfirst(JrTexto::_('Delete')); ?>" class="btn btn-xs btn-default color-red eliminararchivo"><i class="fa fa-trash"></i></button>
                                                <?php } ?>
                                            </td>
                                            <td style="width:15%">
                                                <?php if($asig_alum['estado']=='E'){ 
                                                $arrHabilid = json_decode($adj['habilidad'],true);
                                                if(!empty($arrHabilid)){
                                                    foreach ($arrHabilid as $idHab) { ?>
                                                <div class="btn btn-xs istooltip" style="background: <?php echo $this->habilidadColor[$idHab] ?>; color:#fff;" title="<?php echo $this->habilidades[$idHab]; ?>"><?php echo $this->habilidades[$idHab][0]; ?></div>
                                                <?php } } } ?>
                                            </td>
                                            <?php if(@$asig_alum['estado']=='E'){ ?>
                                            <td>
                                                <div class="text-right"><?php echo $adj['puntaje']; ?></div>
                                            </td>
                                            <?php } ?>
                                        </tr>
                                    <?php } }  ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <div class="col-xs-12 form-group">
                            <label for="txtDescripcion" class="control-label col-xs-12 col-sm-3"><?php echo ucfirst(JrTexto::_('Message to teacher')); ?></label>
                            <div class="col-xs-9">
                                <textarea rows="4" class="form-control" id="txtComentario" name="txtComentario" placeholder="<?php echo ucfirst(JrTexto::_('Write some text')); ?>" <?php if(@$asig_alum['estado']!='N' && @$asig_alum['estado']!='D'){ echo 'readonly';} ?>><?php echo @$asig_alum['comentario']; ?></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <?php if(@$asig_alum['estado']=='N' || @$asig_alum['estado']=='D'){ ?>
                <div class="panel-footer text-center">
                    <button class="btn btn-blue enviar_respuesta"><?php echo JrTexto::_('Submit'); ?></button>
                </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>

<section class="hidden">
    <!-- contenido de moda-body para tipo_adjunto="Enlace" -->
    <div id="adjuntar_link">
        <form class="form-horizontal" id="frm-adjuntar_link" name="frm-adjuntar_link">
            <input type="hidden" name="txtTipo" id="txtTipo" value="L">
            <input type="hidden" name="txtTablapadre" id="txtTablapadre" class="txtTablapadre" value="R">
            <input type="hidden" name="txtIdpadre" id="txtIdpadre" class="txtIdpadre" value="<?php echo @$asig_alum['idtarea_respuesta'];?>">
            <div class="form-group">
                <label for="txtRuta" class="col-xs-12 col-sm-2 control-label"><?php echo JrTexto::_('Link'); ?> (*)</label>
                <div class="col-xs-12 col-sm-10">
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-link"></i></span>
                        <input type="text" name="txtRuta" id="txtRuta" class="form-control" placeholder="<?php echo JrTexto::_('e.g.'); ?>: http://www.webpage.com"  autocomplete="off" required>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label for="txtNombre" class="col-xs-12 col-sm-2 control-label"><?php echo JrTexto::_('Name'); ?></label>
                <div class="col-xs-12 col-sm-6">
                    <input type="text" name="txtNombre" id="txtNombre" class="form-control">
                </div>
            </div>
        </form>
    </div>

    <!-- contenido de moda-body para tipo_adjunto="GrabacionVoz" -->
    <div id="adjuntar_grabacionvoz">
        <form class="form-horizontal" id="frm-adjuntar_grabacionvoz" name="frm-adjuntar_grabacionvoz">
            <input type="hidden" name="txtTipo" id="txtTipo" value="G">
            <input type="hidden" name="txtTablapadre" id="txtTablapadre" class="txtTablapadre" value="R">
            <input type="hidden" name="txtIdpadre" id="txtIdpadre" class="txtIdpadre" value="<?php echo @$asig_alum['idtarea_respuesta'];?>">

            <div class="form-group ">
                <label for="txtNombre" class="col-xs-12 col-sm-3 control-label"><?php echo JrTexto::_('Name'); ?></label>
                <div class="col-xs-12 col-sm-8">
                    <input type="text" name="txtNombre" id="txtNombre" class="form-control txtNombre" value="<?php echo JrTexto::_('recording');?>" data-uniqid="<?php echo uniqid();?>">
                </div>
            </div>
            <div class="form-group text-center">
                <button class="btn btn-danger grabarme" data-estado="stopped"><i class="fa fa-circle"></i> <span><?php echo JrTexto::_('Rec'); ?></span></button>
                <a class="btn btn-primary reproducir"  data-estado="paused" disabled="disabled"><i class="fa fa-play"></i> <span><?php echo JrTexto::_('Play'); ?></span></a>
            </div>
            <div class="form-group ">
                <div class="col-xs-12">
                    <div class="thumbnail onda_voz" style="min-height: 140px; padding-left: 0; padding-right: 0;"></div>
                </div>
            </div>
        </form>
    </div>
</section>

<script type="text/javascript">
$('.istooltip').tooltip({container:'#tarea-see'});
var rutaslib = _sysUrlStatic_+'/libs/audiorecord/';
var recorder;
$(document).ready(function() {
    var fnAjaxFail = function(xhr, textStatus, errorThrown) {
        /*console.log("Error");
        console.log(xhr);
        console.log(textStatus);*/
        throw errorThrown;
        mostrar_notificacion('<?php echo JrTexto::_('Ups'); ?>!', '<?php echo ucfirst(JrTexto::_('something went wrong')); ?>', 'error');
    };

    var initTareaRespuesta = function(){
        if(!$('#hIdTarea_Respuesta').val() && $('#hIdTarea_Estado').val()=='N'){
            guardarTareaRespuesta({ 
                'idtarea_asignacion_alumno': $('#hIdTarea_Asignacion_Alumno').val(), 
                'actualizarEstadoAsignacion': false,
            });
        }
    };

    var guardarTareaRespuesta = function(dataPost) {
        var $btn = $('.panel-footer .btn.enviar_respuesta');
        $.ajax({
            url: _sysUrlBase_+'/tarea_respuesta/xGuardar',
            type: 'POST',
            dataType: 'json',
            data: dataPost,
            beforeSend:function() {
                $btn.prepend('<i class="fa fa-circle-o-notch fa-spin"></i>').attr('disabled', 'disabled');
            }
        }).done(function(resp) {
            if(resp.code=='ok'){
                $('#hIdTarea_Respuesta').val(resp.data);
                $('.txtIdpadre').val(resp.data);
                if(dataPost.actualizarEstadoAsignacion){
                    mostrar_notificacion('<?php echo JrTexto::_('Done'); ?>', resp.msj, 'success');
                    setTimeout(function() {
                        return redir(_sysUrlBase_+'/tarea/#pnl-finalizados');
                    }, '1000');
                }
            }else{
                mostrar_notificacion('<?php echo JrTexto::_('Error'); ?>', resp.msj, 'error');
            }
        })
        .fail(fnAjaxFail).always(function() {
            $btn.removeAttr('disabled');
            $btn.find('i.fa').remove();
            $btn.siblings('.btn').removeAttr('disabled');
        });
    };

    var crearModal = function(param){
        if(param.deDonde==undefined || param.deDonde==''){ return false; }
        if(param.nombreContenedor==undefined || param.nombreContenedor=='') {
            param.nombreContenedor=param.deDonde;
        }
        var $modal = $('#modalclone').clone();
        $modal.attr('id','mdl-'+param.nombreContenedor);
        if(param.small){$modal.find('.modal-dialog').removeClass('modal-lg');}
        else{$modal.find('.modal-dialog').addClass('modal-lg');}
        $modal.find('.modal-header #modaltitle').html('<?php echo JrTexto::_('Attach'); ?> '+param.titulo);
        $modal.find('#modalfooter .btn.cerrarmodal').addClass('pull-left');
        $modal.find('#modalfooter').append('<button class="btn btn-success guardar_adjunto"><?php echo JrTexto::_('Save'); ?></button>');
        $('body').append($modal);
        $('#mdl-'+param.nombreContenedor).modal({keyboard:false, backdrop:'static'});
        $modal.find('#modalcontent').html($('#'+param.deDonde).html());
        $modal.find('#modalcontent').find('form').attr({'id':'frm-'+param.nombreContenedor,'name':'frm-'+param.nombreContenedor});
    };

    var setTarea_Archivos = function($modal, tipo) {
        var datos = {};
        if(tipo=='G'){
            grabacion_ConvertirMP3_Subir('.modal.in', true);
        }else if(tipo=='L'){
            if(!$modal.find('form #txtRuta').val()){ return {}; }
            datos = $modal.find('form').serialize();
        }
        return datos;
    };

    var agregarTblAdjuntos = function(objArchivo={}) {
        if(objArchivo.length==0) return false;
        var icon = '';
        if(objArchivo.tipo=='D'){ icon='fa-paperclip'; }
        else if(objArchivo.tipo=='V'){ icon='fa-video-camera'; }
        else if(objArchivo.tipo=='G'){ icon='fa-microphone'; }
        else if(objArchivo.tipo=='L'){ icon='fa-link'; }
        else if(objArchivo.tipo=='A'){ icon='fa-font'; }
        else if(objArchivo.tipo=='J'){ icon='fa-puzzle-piece'; }
        else if(objArchivo.tipo=='E'){ icon='fa-list'; }
        else{ icon='fa-file'; }

        if(objArchivo.nombre.trim()==''){ objArchivo.nombre = objArchivo.ruta; }
        if(objArchivo.ruta.indexOf('http://')==-1 && objArchivo.ruta.indexOf('https://')==-1){
            objArchivo.ruta = 'http://'+objArchivo.ruta;
        }
        var fila = '<tr data-id="'+objArchivo.idtarea_archivos+'">';
        fila+='<td style="width: 7%"> <i class="fa '+icon+'"></i> </td>';
        fila+='<td>'+objArchivo.nombre+'</td>'
        fila+='<td style="width: 18%">';
        fila+='<a href="'+objArchivo.ruta+'" title="<?php echo ucfirst(JrTexto::_('View')); ?>" target="_blank" class="btn btn-xs btn-default color-info verarchivo"><i class="fa fa-eye"></i></a>';
        fila+='<button title="<?php echo ucfirst(JrTexto::_('Delete')); ?>" class="btn btn-xs btn-default color-red eliminararchivo"><i class="fa fa-trash"></i></button>';
        fila+='</td>';
        fila += '</tr>';
        $('#tblArchivosRespuesta tbody').append(fila);

        actualizarInputIdArchivos();
    };

    var actualizarInputIdArchivos = function(){
        var arrIds=[];
        $('#tblArchivosRespuesta tbody tr').each(function(i, elem) {
            var id = $(elem).data('id');
            arrIds.push(id);
        });
        $('#txtIdTarea_archivos').val(JSON.stringify(arrIds));
    };

    var subirmedia=function(tipo, file, otrosDatos={}){
        var formData = new FormData();
        formData.append("tipo", tipo);
        if(tipo=='G'){
            formData.append("filearchivo", file);/* "file" es un Blob */
            formData.append("nombre_file", otrosDatos.nombrearchivo);
        }else{
            formData.append("filearchivo", file[0].files[0]);
        }
        formData.append("tablapadre", 'R');
        formData.append("idpadre", $('#hIdTarea_Respuesta').val());
        $.ajax({
            url: _sysUrlBase_+'/tarea_archivos/subirarchivo',
            type: "POST",
            data:  formData,
            contentType: false,
            processData: false,
            dataType :'json',
            cache: false,
            processData:false,
            xhr:function(){
                var xhr = new window.XMLHttpRequest();
                /*Upload progress*/
                xhr.upload.addEventListener("progress", function(evt){
                    if (evt.lengthComputable) {
                        var percentComplete = Math.floor((evt.loaded*100) / evt.total);            
                        $('#barra-progreso .progress-bar').width(percentComplete+'%');
                        $('#barra-progreso .progress-bar span').text(percentComplete+'%');
                    }
                }, false);
                /*Download progress*/
                xhr.addEventListener("progress", function(evt){
                    if (evt.lengthComputable) {
                        var percentComplete =  Math.floor((evt.loaded*100) / evt.total);
                    }
                }, false);
                return xhr;
            },
            beforeSend: function(XMLHttpRequest){
                div=$('#barra-progreso');
                $('#barra-progreso').removeClass('progress-bar-danger progress-bar-success progress-bar-striped progress-bar-animated').addClass('progress-bar-striped progress-bar-animated');        
                $('#barra-progreso .progress-bar').width('0%');
                $('#barra-progreso .progress-bar span').text('0%'); 
                $('#barra-progreso').fadeIn('fast'); 
                $('#btn-saveBib_libro').attr('disabled','disabled');
            },      
            success: function(data){
                if(data.code==='ok'){
                    $('#barra-progreso .progress-bar').width('100%');
                    $('#barra-progreso .progress-bar').html('Complete <span>100%</span>');
                    $('#barra-progreso').addClass('progress-bar-success').removeClass('progress-bar-animated');
                    
                    agregarTblAdjuntos({'idtarea_archivos': data.idtarea_archivos, 'nombre': data.nombre, 'ruta': data.ruta, 'tipo': data.tipo});
                    $('#inp_CargadorArchivos').val('');

                    mostrar_notificacion('<?php echo JrTexto::_('Attention');?>',data.msj,'success');
                    $('#btn-saveBib_libro').removeAttr('disabled');
                }else{
                    $('#barra-progreso').addClass('progress-bar-warning progress-bar-animated');
                    mostrar_notificacion('<?php echo JrTexto::_('Attention');?>',data.msj,'warning');
                    return false;
                }
            },
            error: function(e) {
                $('#barra-progreso').addClass('progress-bar-danger progress-bar-animated');
                $('#barra-progreso .progress-bar').html('Error <span>-1%</span>'); 
                mostrar_notificacion('<?php echo JrTexto::_('Attention');?>',e,'warning');
                return false;
            },
            complete: function(xhr){
                $('#barra-progreso .progress-bar').html('Complete <span>100%</span>'); 
                $('#barra-progreso').addClass('progress-bar-success progress-bar-animated').fadeOut('fast');
            }
        });
    };

    var initGrabarVoz = function(){
        audioRecorder.requestDevice(function(recorderObject){
            recorder = recorderObject;
        }, {recordAsOGG: false});
    };
    
    var grabacion_ConvertirMP3_Subir = function(classModal, subir=false){
        var fnCallback = function(blob){
            var a = Date.now();
            var length = ((blob.size*8)/128000);
            var url = URL.createObjectURL(blob);
            if(subir){
                subirmedia('G', blob, {'nombrearchivo': $(classModal).find('input.txtNombre').val()+'_'+$(classModal).find('input.txtNombre').attr('data-uniqid') } );
                $(classModal).modal('hide');
            }else{
                var wavesurfer = Object.create(WaveSurfer);
                wavesurfer.init({
                    container: document.querySelector(classModal+' .onda_voz'),
                    waveColor: '#85BCEA',
                    progressColor: '#337AB7',
                    backend: 'MediaElement'
                });
                wavesurfer.load(url);
                $(classModal+' .btn.reproducir').show();
                document.querySelector(classModal+' .btn.reproducir').addEventListener('click', wavesurfer.playPause.bind(wavesurfer));
            }
        };
        recorder.exportMP3(fnCallback);
    };

    $('.btnadjunto').click(function(e) {
        e.preventDefault();
        var tipo = $(this).data('adjunto');
        switch(tipo) {
            case 'documento':
                var accept = "application/msword, application/vnd.ms-excel, application/vnd.ms-powerpoint, application/vnd.ms-project, application/xhtml+xml, text/plain, application/pdf";
                $("#inp_CargadorArchivos").attr('accept', accept);
                $("#inp_CargadorArchivos").attr('data-tipo', 'D');
                $("#inp_CargadorArchivos").trigger('click');
                break;
            case 'video':
                var accept = "video/*";
                $("#inp_CargadorArchivos").attr('accept', accept);
                $("#inp_CargadorArchivos").attr('data-tipo', 'V');
                $("#inp_CargadorArchivos").trigger('click');
                break;
            case 'enlace':
                crearModal({deDonde:'adjuntar_link', titulo:'<?php echo JrTexto::_('Link'); ?>', small:true});
                $('#mdl-adjuntar_link .guardar_adjunto').attr('data-guardar', 'L');
                break;
            case 'grabacionvoz':
                crearModal({deDonde:'adjuntar_grabacionvoz', titulo:'<?php echo JrTexto::_('Voice recording'); ?>', small:true});
                $('#mdl-adjuntar_grabacionvoz .guardar_adjunto').attr('data-guardar', 'G');
                initGrabarVoz();
                break;
            default:
                break;
        }
    });

    $("#inp_CargadorArchivos").change(function(e) {
        var file=$(this);
        var tipo = $(this).attr('data-tipo');
        if(file.val()=='') return false;
        subirmedia(tipo, file);
        e.preventDefault();
        e.stopPropagation();
    });

    $('body').on('click', '.modal.in .guardar_adjunto', function(e) {
        e.preventDefault();
        var $btn = $(this);
        var tipo = $btn.data('guardar');
        var datos = setTarea_Archivos($(this).closest('.modal'), tipo);
        if($.isEmptyObject(datos)){ return false; }
        $.ajax({
            url: _sysUrlBase_+'/tarea_archivos/guardarTarea_archivos',
            type: 'POST',
            dataType: 'json',
            data:  datos,
            beforeSend: function() {
                $btn.prepend('<i class="fa fa-circle-o-notch fa-spin"></i>').attr('disabled', 'disabled');
                $btn.siblings('.btn').attr('disabled', 'disabled');
            },
        }).done(function(resp) {
            if(resp.code=='ok'){
                agregarTblAdjuntos({'idtarea_archivos': resp.idtarea_archivos, 'nombre': resp.nombre, 'ruta': resp.ruta, 'tipo': resp.tipo});
                mostrar_notificacion('<?php echo JrTexto::_('Done'); ?>', resp.msj, 'success');
            }else{
                mostrar_notificacion('<?php echo JrTexto::_('Error'); ?>', resp.msj, 'error');
            }
        }).fail(fnAjaxFail).always(function() {
            $btn.removeAttr('disabled');
            $btn.find('i.fa').remove();
            $btn.siblings('.btn').removeAttr('disabled');
            $('.modal.in').modal('hide');
        });
    }).on('click', '.modal.in #frm-adjuntar_grabacionvoz .btn.grabarme', function(e) {
        e.preventDefault();
        var $btn = $(this);
        var $modal = $(this).closest('.modal.in');
        if($btn.attr('data-estado')=='stopped'){
            $btn.attr('data-estado','recording');
            recorder.clear();
            recorder && recorder.record();
            $btn.siblings('.btn.reproducir').attr('disabled', 'disabled');
            $modal.find('.onda_voz').html('<h1 class="text-center animated infinite pulse"> <i class="fa fa-microphone"></i> Recoding...</h1>');
            $btn.removeClass('btn-danger').addClass('btn-default');
            $btn.find('i.fa').removeClass('fa-circle').addClass('fa-stop');
            $btn.find('span').text('<?php echo JrTexto::_('Stop'); ?>');
        }else{
            $btn.attr('data-estado','stopped');
            recorder && recorder.stop();
            $btn.siblings('.btn.reproducir').removeAttr('disabled');
            $modal.find('.onda_voz').html('');
            $btn.removeClass('btn-default').addClass('btn-danger');
            $btn.find('i.fa').removeClass('fa-stop').addClass('fa-circle');
            $btn.find('span').text('<?php echo JrTexto::_('Rec'); ?>');
            grabacion_ConvertirMP3_Subir('.modal.in');
        }
    }).on('click', '.modal.in #frm-adjuntar_grabacionvoz .btn.reproducir', function(e) {
        e.preventDefault();
        var $btn = $(this);
        if($btn.attr('data-estado')=='paused'){
            $btn.attr('data-estado','playing');
            $btn.siblings('.btn.grabarme').attr('disabled', 'disabled');
            $btn.find('i.fa').removeClass('fa-play').addClass('fa-pause');
            $btn.find('span').text('<?php echo JrTexto::_('Pause'); ?>');
        }else{
            $btn.attr('data-estado','paused');
            $btn.siblings('.btn.grabarme').removeAttr('disabled');
            $btn.find('i.fa').removeClass('fa-pause').addClass('fa-play');
            $btn.find('span').text('<?php echo JrTexto::_('Play'); ?>');
        }
    }).on('ended', '.modal.in #frm-adjuntar_grabacionvoz .onda_voz audio', function(e) {
        console.log('audio Pause');
        $(this).currentTime = 0;
        var $btn = $(this).closest('.modal.in').find('.btn.reproducir');
        $btn.attr('data-estado','paused');
        $btn.siblings('.btn.grabarme').removeAttr('disabled');
        $btn.find('i.fa').removeClass('fa-pause').addClass('fa-play');
        $btn.find('span').text('<?php echo JrTexto::_('Play'); ?>');
    });

    $('#tblArchivosRespuesta').on('click', '.btn.eliminararchivo', function(e) {
        e.preventDefault();
        var $btn = $(this);
        var $tr= $btn.closest('tr');
        var idArch= $tr.attr('data-id');
        $.confirm({
            title: '<?php echo JrTexto::_('Delete');?>',
            content: '<?php echo JrTexto::_('Are you sure to delete this file?'); ?>',
            confirmButton: '<?php echo JrTexto::_('Accept');?>',
            cancelButton: '<?php echo JrTexto::_('Cancel');?>',
            confirmButtonClass: 'btn-green2',
            cancelButtonClass: 'btn-red',
            closeIcon: true,
            confirm: function(){
                $.ajax({
                    url: _sysUrlBase_+'/tarea_archivos/xEliminar',
                    type: 'POST',
                    dataType: 'json',
                    data: {'idtarea_archivos': idArch},
                    beforeSend: function() {
                        $btn.prepend('<i class="fa fa-circle-o-notch fa-spin"></i>').attr('disabled', 'disabled');
                        $btn.siblings('.btn').attr('disabled', 'disabled');
                    },
                }).done(function(resp) {
                    if(resp.code="ok"){
                        $tr.remove();
                        actualizarInputIdArchivos();
                    }else{
                        mostrar_notificacion('<?php echo JrTexto::_('Error') ?>', resp.msj, 'error');
                    }
                }).fail(fnAjaxFail).always(function() {
                    $btn.removeAttr('disabled');
                    $btn.find('i.fa').remove();
                    $btn.siblings('.btn').removeAttr('disabled');
                });
            },
        });
    });
    
    $('body').on('hidden.bs.modal', '.modal', function(event) { 
        $(this).remove(); 
    }).on('keyup', '.form-group input[required]', function(e) {
        if(!$(this).val() || $(this).val().trim()==''){
            $(this).closest('.form-group').addClass('has-error');
        }else{
            $(this).closest('.form-group').removeClass('has-error');
        }
    }).on('click', '.modal.in .nav-tabs li>a', function(e) {
        var $modal = $(this).closest('.modal.in');
        var tipo = $modal.find('.modal-footer .btn.guardar_adjunto').data('guardar');
        if($modal.find('.divVistaTabs .tab-content iframe').length>0){
            var id = $(this).closest('li').data('id');
            if(tipo=='J'){
                $modal.find('.divVistaTabs .tab-content iframe').attr('src', _sysUrlBase_+'/game/ver/'+id).show();
            }else if(tipo=='E'){
                $modal.find('.divVistaTabs .tab-content iframe').attr('src', _sysUrlBase_+'/examenes/teacherresrc_view?idexamen='+id).show();
            }
        }
    });

    $('.panel.tarea_respuesta').on('click', '.panel-footer .btn.enviar_respuesta', function(e) {
        e.preventDefault();
        $.confirm({
            title: '<?php echo JrTexto::_('Submit').' '.JrTexto::_('Homework'); ?>',
            content: '<?php echo JrTexto::_('Do you want to submit the homework now?'); ?>',
            confirmButton: '<?php echo JrTexto::_('Accept');?>',
            cancelButton: '<?php echo JrTexto::_('Cancel');?>',
            confirmButtonClass: 'btn-green2',
            cancelButtonClass: 'btn-red',
            closeIcon: true,
            confirm: function(){
                guardarTareaRespuesta({
                    'pkIdtarea_respuesta': $('#hIdTarea_Respuesta').val(),
                    'idtarea_asignacion_alumno': $('#hIdTarea_Asignacion_Alumno').val(), 
                    'comentario': $('#txtComentario').val(),
                    'actualizarEstadoAsignacion': true,
                });
            },
        });
    });

    initTareaRespuesta();
});
</script>