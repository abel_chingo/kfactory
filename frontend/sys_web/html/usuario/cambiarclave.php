<?php 
defined('RUTA_BASE') or die();
$id_vent = uniqid();
if(!empty($this->datos)) $frm=$this->datos;
$ventanapadre=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:'eeeexzx-1';
//var_dump($frm);
?>
<div class="row" style="<?php echo $this->documento->plantilla=="blanco"?"min-width:600px":"" ?>">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="panel">
      <?php if($this->documento->plantilla!='modal'){?><div class="panel-heading bg-blue">
        <h3><?php echo JrTexto::_('Alumno'); ?><small id="frmaction"> <?php echo JrTexto::_($this->frmaccion);?></small></h3>
        <div class="clearfix"></div>
      </div><?php } ?>      
      <div class="panel-body">
        <div id="msj-interno"></div>
        <form method="post" id="frm-<?php echo $id_vent;?>"  target="" enctype="" class="form-horizontal form-label-left" >
          <input type="hidden" name="pkDni" id="pkdni" value="<?php echo JrTexto::_($this->pk);?>">
          <input type="hidden" name="accion" id="pkaccion" value="<?php echo JrTexto::_($this->frmaccion);?>">
          <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtemail">
              <?php echo JrTexto::_('Email');?> :
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="email"  id="txtemail" name="txtemail" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["email"];?>" placeholder="email@empresa.com">
              </div>
          </div>
          <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtusuario">
              <?php echo JrTexto::_('User');?> :
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text" id="txtusuario" name="txtusuario" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["usuario"];?>" placeholder="<?php echo JrTexto::_('user');?>">
              </div>
          </div>
          <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtclave">
               <?php echo JrTexto::_('Password');?> <span class="required"> (*) :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="password"  id="txtclave" name="txtclave" required="required" class="form-control col-md-7 col-xs-12" placeholder="<?php echo JrTexto::_('Password');?>" >
              </div>
          </div>
          <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtclave2">
              <?php echo JrTexto::_('Repit password');?> <span class="required"> (*) :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="password"  id="txtclave2" name="txtclave2" required="required" class="form-control col-md-7 col-xs-12" placeholder="<?php echo JrTexto::_('repit password');?>" >
              </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>