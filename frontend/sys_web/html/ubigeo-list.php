  <?php 
  defined("RUTA_BASE") or die(); 
  $idgui=uniqid();
  ?><style type="text/css">
    .panel-body{
      border: 1px solid rgba(90, 137, 248, 0.41);
      padding: 10px;
    }
    .title_left, .small{
      height: auto;
      color: #fff;
    }
    .form-group{
      margin-bottom: 0px; 
    }
    .input-group {
      margin-top: 0px;
    }
    .select-ctrl-wrapper:after{
      right: 0px;
    }
    select.select-ctrl, .form-control , .input-group-addon{   
      border: 1px solid #4683af;
      margin-bottom: 1ex;
    }
  </style>
  <div class="form-view" id="ventana_<?php echo $idgui; ?>" >
    <div class="page-title">
      <div class="title_left">
        <ol class="breadcrumb">
          <li><a href="<?php echo JrAplicacion::getJrUrl(array("academico"));?>"><?php echo JrTexto::_("Academic");?></a></li>
          <li><a href="<?php echo JrAplicacion::getJrUrl(array('ubigeo'));?>"><?php echo JrTexto::_('Ubigeo'); ?></a></li>
          <li class="active"><?php echo JrTexto::_('list')?></li>
        </ol>
      </div>
    </div>
    <div class="clearfix"></div>
    <div class="row">
    <div class="col-md-12">
      <div class="panel" >      
        <div class="panel-body">      
           <div class="row">                                                                 
              <div class="col-xs-6 col-sm-4 col-md-3 ">
                <label><?php echo JrTexto::_('Pais'); ?></label>
                <div class="select-ctrl-wrapper select-azul">
                <select id="fkcbpais" name="fkcbpais" class="form-control select-ctrl" >
                   <option value=""><?php echo JrTexto::_('Seleccione'); ?></option>
                  <?php 
                  if(!empty($this->fkpais))
                    foreach ($this->fkpais as $fkpais) { ?>
                      <option value="<?php echo $fkpais["pais"]?>" <?php echo $fkpais["pais"]==@$this->paisid?"selected":""; ?> ><?php echo $fkpais["ciudad"] ?></option>
                  <?php } ?>
                </select>
                </div>
              </div>                         
              <div class="col-xs-6 col-sm-4 col-md-3">
                <label><?php echo JrTexto::_('Departamento'); ?></label>
                <div class="select-ctrl-wrapper select-azul">
                <select id="fkcbdepartamento" name="fkcbdepartamento" class="form-control select-ctrl" >
                   <option value=""><?php echo JrTexto::_('Seleccione'); ?></option>
                  <?php 
                  if(!empty($this->fkdepartamento))
                    foreach ($this->fkdepartamento as $fkdepartamento) { ?><option value="<?php echo $fkdepartamento["departamento"]?>" <?php echo $fkdepartamento["departamento"]==@$this->departamentoid?"selected":""; ?> ><?php echo $fkdepartamento["ciudad"] ?></option><?php } ?>                          </select></div>
              </div>
              <div class="col-xs-6 col-sm-4 col-md-3">
                <label><?php echo JrTexto::_('Provincia'); ?></label>
                <div class="select-ctrl-wrapper select-azul">
                <select id="fkcbprovincia" name="fkcbprovincia" class="form-control select-ctrl" >
                    <option value=""><?php echo JrTexto::_('Seleccione'); ?></option>
                    <?php 
                  if(!empty($this->fkprovincia))
                    foreach ($this->fkprovincia as $fkprovincia) { ?><option value="<?php echo $fkprovincia["provincia"]?>" <?php echo $fkprovincia["provincia"]==@$this->provinciasid?"selected":""; ?> ><?php echo $fkprovincia["ciudad"] ?></option><?php } ?>                          
                  </select></div>
              </div>
              <div class="col-xs-6 col-sm-4 col-md-3 ">
                <label><?php echo JrTexto::_('Distritos'); ?></label>
                <div class="select-ctrl-wrapper select-azul">
                  <select id="fkcbdistrito" name="fkcbdistrito" class="form-control select-ctrl" >
                     <option value=""><?php echo JrTexto::_('Seleccione'); ?></option>
                    <?php 
                    if(!empty($this->fkdistrito))
                      foreach ($this->fkdistrito as $fkdistrito) { ?><option value="<?php echo $fkdistrito["distrito"]?>" <?php echo $fkdistrito["distrito"]==@$this->distritoid?"selected":""; ?> ><?php echo $fkdistrito["ciudad"] ?></option><?php } ?>                          
                    </select>
                  </div>
              </div>                                          
              <div class="col-xs-6 col-sm-6 col-md-6">
                <div class="form-group">
                <div class="input-group">
                  <input type="text" name="texto" id="texto" class="form-control border0" placeholder="<?php echo  ucfirst(JrTexto::_("text to search"))?>">
                  <span class="input-group-addon btn btnbuscar"><?php echo  ucfirst(JrTexto::_("Search"))?> <i class="fa fa-search"></i>
                     </span>  
                </div>
                </div>
              </div>
              <!--div class="col-xs-6 col-sm-3 col-md-3 text-center">
                  <a class="btn btn-success btnvermodal" data-modal="si" href="<?php //echo JrAplicacion::getJrUrl(array("Ubigeo", "agregar"));?>" data-titulo="<?php //echo JrTexto::_("Ubigeo").' - '.JrTexto::_("add"); ?>"><i class="fa fa-plus"></i> <?php //echo JrTexto::_('add')?></a>
              </div-->
            </div>
        </div>
      </div>
    </div>
  	<div class="col-md-12 col-sm-12 col-xs-12">
  	   <div class="panel">           
           <div class="panel-body">
              <table class="table table-striped table-responsive">
                <thead>
                  <tr class="headings">
                    <th>#</th>
                    <th><?php echo JrTexto::_("Pais");?></th>
                      <th><?php echo JrTexto::_("Departamento");?></th>
                      <th><?php echo JrTexto::_("Provincia");?></th>
                      <th><?php echo JrTexto::_("Distrito");?></th>
                      <!--th><?php echo JrTexto::_("Ciudad") ;?></th-->
                      <th class="sorting_disabled"><span class="nobr"><?php echo JrTexto::_('Actions');?></span></th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
          </table>
          </div>
          </div>
        </div>
      </div>
  </div>


  <script type="text/javascript">
  var tabledatos596fd8695575b='';
  function refreshdatos596fd8695575b(){
      tabledatos596fd8695575b.ajax.reload();
  }
  $(document).ready(function(){  
    var estados596fd8695575b={'1':'<?php echo JrTexto::_("Active") ?>','0':'<?php echo JrTexto::_("Inactive") ?>','C':'<?php echo JrTexto::_("Cancelled") ?>'}
    var tituloedit596fd8695575b='<?php echo ucfirst(JrTexto::_("ubigeo"))." - ".JrTexto::_("edit"); ?>';
    var draw596fd8695575b=0;

    var cargardatos=function(obj,data,returnobj){
      var midata=data||null;      
      $.ajax({
        url: _sysUrlBase_+'/ubigeo/buscarjson/?json=true',
        type: 'POST',
        dataType: 'json',
        data: midata,
      }).done(function(resp){
          if(resp.code=='ok'){                  
              $('option:first',obj).siblings().remove();
              if(resp.data!=''){
                $.each(resp.data,function(i,v){
                $.each(v,function(ii,vv){           
                  obj.append('<option value="'+vv[returnobj]+'">'+vv["ciudad"]+'</option>');                
                })});
              }
              if(returnobj=='departamento'){
                $('#fkcbdepartamento').trigger('change');
                return;
              }
              if(returnobj=='provincia'){
                $('#fkcbprovincia').trigger('change');
                return;
              }
              refreshdatos596fd8695575b();
          } else{              
              mostrar_notificacion('<?php echo JrTexto::_('Attention'); ?>', resp.mensaje, 'error'); 
          }
      }).fail(function(xhr, textStatus, errorThrown) {        
          console.log(xhr.responseText);
      });
    }
    


    $('#fkcbpais').change(function(ev){
      var d={};
      d.pais=$('#fkcbpais').val();      
      d.return='departamento';
      if(d.pais==''){
        $('#fkcbdepartamento option:first').siblings().remove();
        $('#fkcbprovincia option:first').siblings().remove();
        $('#fkcbdistrito option:first').siblings().remove();
        refreshdatos596fd8695575b();
      }else{
        cargardatos($('#fkcbdepartamento'),d,'departamento');     
      }
    });
    
    $('#fkcbdepartamento').change(function(ev){

      var d={};
      d.pais=$('#fkcbpais').val();
      d.departamento=$('#fkcbdepartamento').val();     
      d.return='provincia';
      if(d.departamento==''){
        $('#fkcbprovincia option:first').siblings().remove();
        $('#fkcbdistrito option:first').siblings().remove();
        refreshdatos596fd8695575b();
      }else{
        cargardatos($('#fkcbprovincia'),d,'provincia');     
      }
    });
    
    $('#fkcbprovincia').change(function(ev){
      var d={};
      d.pais=$('#fkcbpais').val();
      d.departamento=$('#fkcbdepartamento').val();
      d.provincia=$('#fkcbprovincia').val();
      d.return='distrito';
      if(d.provincia==''){
        $('#fkcbdistrito option:first').siblings().remove();
        refreshdatos596fd8695575b();
      }else{
        cargardatos($('#fkcbdistrito'),d,'distrito');     
      }
      
    });
    
    $('#fkcbdistrito').change(function(ev){      
      refreshdatos596fd8695575b();
    });
    
    $('.btnbuscar').click(function(ev){
      refreshdatos596fd8695575b();
    });   
    tabledatos596fd8695575b=$('#ventana_<?php echo $idgui; ?> .table').DataTable(
      { "searching": false,
        "processing": false,
        //"serverSide": true,
        "columns" : [
          {'data': '#'},          
          {'data': '<?php echo JrTexto::_("Pais") ;?>'},
          {'data': '<?php echo JrTexto::_("Departamento") ;?>'},
          {'data': '<?php echo JrTexto::_("Provincia") ;?>'},
          {'data': '<?php echo JrTexto::_("Distrito") ;?>'},
          //{'data': '<?php //echo JrTexto::_("Ciudad") ;?>'},
              
          {'data': '<?php echo JrTexto::_("Actions") ;?>'},
        ],
        "ajax":{
          url:_sysUrlBase_+'/ubigeo/buscarjson/?json=true',
          type: "post",                
          data:function(d){
              
              d.json=true                   
              d.pais=$('#fkcbpais').val(),
              d.departamento=$('#fkcbdepartamento').val(),
              d.provincia=$('#fkcbprovincia').val(),
              d.distrito=$('#fkcbdistrito').val(),
              d.ciudad=$('#texto').val(),                          
              draw596fd8695575b=d.draw;
             // console.log(d);
          },
          "dataSrc":function(json){
            //console.log(json);
            var data=json.data;                         
            json.draw = draw596fd8695575b;
            json.recordsTotal = json.data.length;
            json.recordsFiltered = json.data.length;
            var datainfo = new Array();
            for(var i=0;i< data.length; i++){  
            //console.log(data[i].strpais);
              datainfo.push({
                '#':(i+1),
                '<?php echo JrTexto::_("Pais") ;?>': data[i].strpais,
                '<?php echo JrTexto::_("Departamento") ;?>': data[i].strdepartamento,
                '<?php echo JrTexto::_("Provincia") ;?>': data[i].strprovincia,
                '<?php echo JrTexto::_("Distrito") ;?>': data[i].ciudad,
                '<?php echo JrTexto::_("Actions") ;?>'  :'<!--a class="btn btn-xs btnvermodal" data-modal="si" href="'+_sysUrlBase_+'/ubigeo/editar/?id='+data[i].id_ubigeo+'" data-titulo="'+tituloedit596fd8695575b+'"><i class="fa fa-edit"></i></a--><a class="btn-eliminar btn btn-xs" href="javascript:;" data-id="'+data[i].id_ubigeo+'" ><i class="fa fa-trash-o"></i>'
              });
            }
            return datainfo }, error: function(d){console.log(d)}
        }
        <?php echo $this->documento->getIdioma()!='EN'?(',"language": { "url": "'.$this->documento->getUrlStatic().'/libs/datatable1.10/idiomas/'.$this->documento->getIdioma().'.json"}'):''?>
      });


    $('#ventana_<?php echo $idgui; ?>').on('click','.btn-chkoption',function(){     
        var id=$(this).attr('data-id');
        var campo=$(this).attr('campo');
        var data=0;
        if($("i",this).hasClass('fa-circle-o')) data=1;
        $.confirm({
          title: '<?php echo JrTexto::_('Confirm action');?>',
          content: '<?php echo JrTexto::_('It is sure to change the status of this record ?'); ?>',
          confirmButton: '<?php echo JrTexto::_('Accept');?>',
          cancelButton: '<?php echo JrTexto::_('Cancel');?>',
          confirmButtonClass: 'btn-success',
          cancelButtonClass: 'btn-danger',
          closeIcon: true,
          confirm: function(){
            var res = xajax__('', 'ubigeo', 'setCampo', id,campo,data);
            if(res) tabledatos596fd8695575b.ajax.reload();
          }
        });
    });

    $('#ventana_<?php echo $idgui; ?>').on('click','.btnvermodal',function(e){
      e.preventDefault();
      e.stopPropagation();
      var enmodal=$(this).attr('data-modal')||'no';
      var fcall=$(this).attr('data-fcall')||'refreshdatos596fd8695575b';
      var url=$(this).attr('href')
      if(url.indexOf('?')!=-1) url+='&fcall='+fcall;
      else url+='?fcall='+fcall;
      var ventana=$(this).data('ventana')||'Ubigeo';
      var claseid=ventana+'_<?php echo $idgui; ?>';
      var titulo=$(this).attr('data-titulo')||'';
      titulo=titulo.toString().replace('<br>',' ');     
      if(enmodal=='no'){
        return redir(url);          
      }
      url+='&plt=modal';
      openModal('lg',titulo,url,ventana,claseid); 
    });
    
    $('#ventana_<?php echo $idgui; ?> .table').on('click','.btn-eliminar',function(){
       var id=$(this).attr('data-id');
       $.confirm({
        title: '<?php echo JrTexto::_('Confirm action');?>',
        content: '<?php echo JrTexto::_('It is sure to delete this record ?'); ?>',
        confirmButton: '<?php echo JrTexto::_('Accept');?>',
        cancelButton: '<?php echo JrTexto::_('Cancel');?>',
        confirmButtonClass: 'btn-success',
        cancelButtonClass: 'btn-danger',
        closeIcon: true,
        confirm: function(){             
          var res = xajax__('', 'ubigeo', 'eliminar', id);
          if(res) tabledatos596fd8695575b.ajax.reload();
        }
      }); 
    });

  });
  </script>