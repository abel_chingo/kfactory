<style type="text/css">
#cursos-listar .item-curso{
    display: table; 
    float: left;
    padding: 0px 15px;
    width: 20%; 
}
#cursos-listar .item-curso img{
    max-height: 187.59px; 
}
#cursos-listar .tabs-navegacion{
    background-color: #00A9AC;
}
#cursos-listar .tabs-navegacion .nav-tabs>li.active>a{
    color: inherit;
}
#cursos-listar .tabs-navegacion .nav-tabs>li>a{
    color: #fff;
}
</style>
<div class="row" id="cursos-listar">
    <div class="col-xs-12 tabs-navegacion">
        <ul class="nav nav-tabs navbar-right">
            <li class="tabpendientes active">
                <a href="#pnl-todo" data-toggle="tab">Todos</a>
            </li>
            <li class="tabfinalizados">
                <a href="#pnl-office_2013" data-toggle="tab">Office 2013</a>
            </li>
            <li class="tabtodos">
                <a href="#pnl-office_2010" data-toggle="tab">Office 2010</a>
            </li>
            <li class="tabtodos">
                <a href="#pnl-tics" data-toggle="tab">Tics</a>
            </li>
        </ul>
    </div>
    <div class="col-xs-12 tab-content">
        <div id="pnl-todo" class="tab-pane fade active in">
            <?php if(!empty($this->niveles)){
            $cnivel=count($this->niveles);
            $inivel=0;
            foreach ($this->niveles as $nivel){ $inivel++;?>
            <div class="item-curso">
                <a href="<?php echo $this->documento->getUrlBase(); ?>/cursos/contenido/<?php echo $nivel["idnivel"] ?>" class="hvr-bounce-in"> 
                    <img src="<?php $caratula=(!empty($nivel['imagen']))?$nivel['imagen']:'default.png';
                    echo $this->documento->getUrlStatic().'/img/cursos/'.$caratula; ?>" class="img img-responsive" alt="curso_cover">
                </a>
            </div>
            <?php }
            }?>
        </div>
        <div id="pnl-office_2013" class="tab-pane fade"></div>
        <div id="pnl-office_2010" class="tab-pane fade"></div>
        <div id="pnl-tics" class="tab-pane fade"></div>
    </div>
</div>

<script type="text/javascript">
$(document).ready(function() {
    
});
</script>