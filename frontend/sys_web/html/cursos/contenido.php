
<div class="row" id="cursos-contenido">
    <div class="col-xs-12 tabs-navegacion">
        <ul class="nav nav-tabs navbar-right">
            <li class="tabpendientes active">
                <a href="#pnl-todo" data-toggle="tab">Todos</a>
            </li>
            <li class="tabfinalizados">
                <a href="#pnl-office_2013" data-toggle="tab">Office 2013</a>
            </li>
            <li class="tabtodos">
                <a href="#pnl-office_2010" data-toggle="tab">Office 2010</a>
            </li>
            <li class="tabtodos">
                <a href="#pnl-tics" data-toggle="tab">Tics</a>
            </li>
        </ul>
    </div>
    <div class="col-xs-12 tab-content">
        <div id="pnl-todo" class="tab-pane fade active in"></div>
        <div id="pnl-office_2013" class="tab-pane fade"></div>
        <div id="pnl-office_2010" class="tab-pane fade"></div>
        <div id="pnl-tics" class="tab-pane fade"></div>
    </div>
</div>