<?php 
defined('RUTA_BASE') or die();
$id_vent = uniqid();
if(!empty($this->datos)) $frm=$this->datos;
?><div class="row" style="<?php echo $this->documento->plantilla=="blanco"?"min-width:600px":"" ?>">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2><?php echo JrTexto::_('Grupos'); ?><small id="frmaction"><?php echo JrTexto::_($this->frmaccion);?></small></h2>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <div id="msj-interno"></div>
        <form method="post" id="frm-<?php echo $id_vent;?>"  target="" enctype="" class="form-horizontal form-label-left" >
          <input type="hidden" name="pkIdgrupo" id="pkidgrupo" value="<?php echo JrTexto::_($this->pk);?>">
          <input type="hidden" name="accion" id="pkaccion" value="<?php echo JrTexto::_($this->frmaccion);?>">
          <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtIddocente">
              <?php echo JrTexto::_('Iddocente');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtIddocente" name="txtIddocente" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["iddocente"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtIdlocal">
              <?php echo JrTexto::_('Idlocal');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtIdlocal" name="txtIdlocal" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["idlocal"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtIdambiente">
              <?php echo JrTexto::_('Idambiente');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtIdambiente" name="txtIdambiente" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["idambiente"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtIdasistente">
              <?php echo JrTexto::_('Idasistente');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtIdasistente" name="txtIdasistente" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["idasistente"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtFechainicio">
              <?php echo JrTexto::_('Fechainicio');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtFechainicio" name="txtFechainicio" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["fechainicio"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtFechafin">
              <?php echo JrTexto::_('Fechafin');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtFechafin" name="txtFechafin" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["fechafin"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtDias">
              <?php echo JrTexto::_('Dias');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtDias" name="txtDias" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["dias"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtHoras">
              <?php echo JrTexto::_('Horas');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtHoras" name="txtHoras" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["horas"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtHorainicio">
              <?php echo JrTexto::_('Horainicio');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtHorainicio" name="txtHorainicio" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["horainicio"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtHorafin">
              <?php echo JrTexto::_('Horafin');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtHorafin" name="txtHorafin" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["horafin"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtValor">
              <?php echo JrTexto::_('Valor');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtValor" name="txtValor" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["valor"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtValor_asi">
              <?php echo JrTexto::_('Valor asi');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtValor_asi" name="txtValor_asi" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["valor_asi"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtRegusuario">
              <?php echo JrTexto::_('Regusuario');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtRegusuario" name="txtRegusuario" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["regusuario"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtRegfecha">
              <?php echo JrTexto::_('Regfecha');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtRegfecha" name="txtRegfecha" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["regfecha"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtMatriculados">
              <?php echo JrTexto::_('Matriculados');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtMatriculados" name="txtMatriculados" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["matriculados"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtNivel">
              <?php echo JrTexto::_('Nivel');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtNivel" name="txtNivel" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["nivel"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtCodigo">
              <?php echo JrTexto::_('Codigo');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtCodigo" name="txtCodigo" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["codigo"];?>">
                                  
              </div>
            </div>

            
          <div class="ln_solid"></div>
          <div class="form-group">
            <div class="col-md-6 col-md-offset-4">
              <button id="btn-saveGrupos" type="submit" class="btn btn-success" ><i class=" fa fa-save"></i> <?php echo JrTexto::_('Save');?> </button>
              <a type="button" class="btn btn-warning btn-close" href="<?php echo JrAplicacion::getJrUrl(array('grupos'))?>" data-dismiss="modal"  ><i class=" fa fa-repeat"></i> <?php echo JrTexto::_('Cancel');?></a>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function(){  
            
$('#frm-<?php echo $id_vent;?>').bind({    
     submit: function(event){
      event.preventDefault();
      $(this).attr('disabled', true); 
      var res = xajax__('', 'Grupos', 'saveGrupos', xajax.getFormValues('frm-<?php echo $id_vent;?>'));
      if(res){
        <?php if(!empty($this->parent)):?>
        if(typeof window.<?php echo $this->parent?> == 'function') {
          return <?php echo $this->parent?>(res);
        } else {
          return redir('<?php echo JrAplicacion::getJrUrl(array("Grupos"))?>');
        }
        <?php else:?>
        return redir('<?php echo JrAplicacion::getJrUrl(array("Grupos"))?>');
        <?php endif;?>       }
     }
  });







  
  
});


</script>

