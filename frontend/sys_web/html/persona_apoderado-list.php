<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
?><style type="text/css">
  .panel-body{
    border: 1px solid rgba(90, 137, 248, 0.41);
    padding: 10px;
  }
  .title_left, .small{
    height: auto;
    color: #fff;
  }
  .form-group{
    margin-bottom: 0px; 
  }
  .input-group {
    margin-top: 0px;
  }
  .select-ctrl-wrapper:after{
      right: 0px;
    }
  select.select-ctrl, .form-control , .input-group-addon{   
    border: 1px solid #4683af;
    margin-bottom: 1ex;
  }
</style>
<div class="form-view" id="ventana_<?php echo $idgui; ?>" >
  <div class="page-title">
    <div class="title_left">
      <ol class="breadcrumb">
        <li><a href="<?php echo JrAplicacion::getJrUrl(array("academico"));?>"><?php echo JrTexto::_("Academic");?></a></li>
        <li><a href="<?php echo JrAplicacion::getJrUrl(array('persona_apoderado'));?>"><?php echo JrTexto::_('Persona_apoderado'); ?></a></li>
        <li class="active"><?php echo JrTexto::_('list')?></li>
      </ol>
    </div>
  </div>
  <div class="clearfix"></div>
<div class="row">
  <div class="col-md-12">
    <div class="panel" >      
      <div class="panel-body">      
         <div class="row"> 
         <div class="col-xs-6 col-sm-4 col-md-3 ">
            <label><?php echo JrTexto::_('Sexo'); ?></label>
            <div class="select-ctrl-wrapper select-azul">
            <select id="fkcbsexo" name="fkcbsexo" class="form-control select-ctrl" >
                <option value=""><?php echo JrTexto::_('Seleccione'); ?></option>
                  <?php 
                          if(!empty($this->fksexo))
                            foreach ($this->fksexo as $fksexo) { ?><option value="<?php echo $fksexo["codigo"]?>" <?php echo $fksexo["codigo"]==@$frm["sexo"]?"selected":""; ?> ><?php echo $fksexo["nombre"] ?></option><?php } ?>              </select>
            </div>
            </div>
              <div class="col-xs-6 col-sm-4 col-md-3 ">
              <label><?php echo JrTexto::_('Tipo Documento'); ?></label>
              <div class="select-ctrl-wrapper select-azul">
              <select id="fkcbtipodoc" name="fkcbtipodoc" class="form-control select-ctrl" >
                <option value=""><?php echo JrTexto::_('Seleccione'); ?></option>
                  <?php 
                          if(!empty($this->fktipodoc))
                            foreach ($this->fktipodoc as $fktipodoc) { ?><option value="<?php echo $fktipodoc["codigo"]?>" <?php echo $fktipodoc["codigo"]==@$frm["tipodoc"]?"selected":""; ?> ><?php echo $fktipodoc["nombre"] ?></option><?php } ?>              </select>
            </div>
            </div>
             <div class="col-xs-6 col-sm-4 col-md-3">
              <label><?php echo JrTexto::_('Parentesco'); ?></label>
             <div class="select-ctrl-wrapper select-azul">
              <select id="fkcbparentesco" name="fkcbparentesco" class="form-control select-ctrl" >
                <option value=""><?php echo JrTexto::_('Seleccione'); ?></option>
                  <?php 
                          if(!empty($this->fkparentesco))
                            foreach ($this->fkparentesco as $fkparentesco) { ?><option value="<?php echo $fkparentesco["codigo"]?>" <?php echo $fkparentesco["codigo"]==@$frm["parentesco"]?"selected":""; ?> ><?php echo $fkparentesco["nombre"] ?></option><?php } ?>              </select>
                          </div>
            </div>
                                                                    
            <div class="col-xs-6 col-sm-6 col-md-6">
              <div class="form-group">
                <div class="input-group">
                  <input type="text" name="texto" id="texto" class="form-control border0" placeholder="<?php echo  ucfirst(JrTexto::_("text to search"))?>">
                  <span class="input-group-addon btn btnbuscar"><?php echo  ucfirst(JrTexto::_("Search"))?> <i class="fa fa-search"></i></span>  
                                    </div>
              </div>
            </div>
            <div class="col-xs-6 col-sm-3 col-md-3 text-center">
               <a class="btn btn-success btnvermodal" data-modal="si" href="<?php echo JrAplicacion::getJrUrl(array("Persona_apoderado", "agregar"));?>" data-titulo="<?php echo JrTexto::_("Persona_apoderado").' - '.JrTexto::_("add"); ?>"><i class="fa fa-plus"></i> <?php echo JrTexto::_('add')?></a>
              </div>
            </div>
          </div>
      </div>
    </div>
	  <div class="col-md-12 col-sm-12 col-xs-12">
	   <div class="panel">         
         <div class="panel-body">
            <table class="table table-striped table-responsive">
              <thead>
                <tr class="headings">
                  <th>#</th>
                  <th><?php echo JrTexto::_("Persona"); ?></th>
                    <th><?php echo JrTexto::_("Apoderado") ;?></th>
                    <th><?php echo JrTexto::_("Correo") ;?></th>
                    <th><?php echo JrTexto::_("Sexo"); ?></th>
                    <th><?php echo JrTexto::_("Tipo Doc"); ?></th>
                    <th><?php echo JrTexto::_("Ndoc") ;?></th>
                    <th><?php echo JrTexto::_("Parentesco"); ?></th>
                    <th><?php echo JrTexto::_("Telefono") ;?></th>
                    <th><?php echo JrTexto::_("Celular") ;?></th>
                    <th><?php echo JrTexto::_("Mostrar") ;?></th>
                    <th class="sorting_disabled"><span class="nobr"><?php echo JrTexto::_('Actions');?></span></th>
                </tr>
              </thead>
              <tbody>
                        </tbody>
        </table>
        </div>
        </div>
      </div>
    </div>
</div>

<script type="text/javascript">
var tabledatos5976198471759='';
function refreshdatos5976198471759(){
    tabledatos5976198471759.ajax.reload();
}
$(document).ready(function(){  
  var estados5976198471759={'1':'<?php echo JrTexto::_("Active") ?>','0':'<?php echo JrTexto::_("Inactive") ?>','C':'<?php echo JrTexto::_("Cancelled") ?>'}
  var tituloedit5976198471759='<?php echo ucfirst(JrTexto::_("persona_apoderado"))." - ".JrTexto::_("edit"); ?>';
  var draw5976198471759=0;

  
  $('#fkcbidpersona').change(function(ev){
    refreshdatos5976198471759();
  });
  $('#fkcbsexo').change(function(ev){
    refreshdatos5976198471759();
  });
  $('#fkcbtipodoc').change(function(ev){
    refreshdatos5976198471759();
  });
  $('#fkcbparentesco').change(function(ev){
    refreshdatos5976198471759();
  });
  $('.btnbuscar').click(function(ev){
    refreshdatos5976198471759();
  });
  tabledatos5976198471759=$('#ventana_<?php echo $idgui; ?> .table').DataTable(
    { "searching": false,
      "processing": false,
      //"serverSide": true,
      "columns" : [
        {'data': '#'},
        
        {'data': '<?php echo JrTexto::_("Persona") ;?>'},
            {'data': '<?php echo JrTexto::_("Apoderado") ;?>'},           
            {'data': '<?php echo JrTexto::_("Correo") ;?>'},
            {'data': '<?php echo JrTexto::_("Sexo") ;?>'},
            {'data': '<?php echo JrTexto::_("Tipo Doc") ;?>'},
            {'data': '<?php echo JrTexto::_("Ndoc") ;?>'},
            {'data': '<?php echo JrTexto::_("Parentesco") ;?>'},
            {'data': '<?php echo JrTexto::_("Telefono") ;?>'},
            {'data': '<?php echo JrTexto::_("Celular") ;?>'},
            {'data': '<?php echo JrTexto::_("Mostrar") ;?>'},
            
        {'data': '<?php echo JrTexto::_("Actions") ;?>'},
      ],
      "ajax":{
        url:_sysUrlBase_+'/persona_apoderado/buscarjson/?json=true',
        type: "post",                
        data:function(d){
            d.json=true                   
             d.idpersona=$('#fkcbidpersona').val(),
             d.sexo=$('#fkcbsexo').val(),
             d.tipodoc=$('#fkcbtipodoc').val(),
             d.parentesco=$('#fkcbparentesco').val(),
             //d.texto=$('#texto').val(),
                        
            draw5976198471759=d.draw;
           // console.log(d);
        },
        "dataSrc":function(json){
          var data=json.data;             
          json.draw = draw5976198471759;
          json.recordsTotal = json.data.length;
          json.recordsFiltered = json.data.length;
          var datainfo = new Array();
          for(var i=0;i< data.length; i++){                   
            datainfo.push({
              '#':(i+1),
              '<?php echo JrTexto::_("Persona") ;?>': data[i].ape_paterno,
              '<?php echo JrTexto::_("Apoderado") ;?>': data[i].apellidopaterno+''+data[i].apellidomaterno+', '+data[i].nombres,            
              '<?php echo JrTexto::_("Correo") ;?>': data[i].correo,
              '<?php echo JrTexto::_("Sexo") ;?>': data[i].nombre,
              '<?php echo JrTexto::_("Tipo Doc") ;?>': data[i].nombre,
              '<?php echo JrTexto::_("Ndoc") ;?>': data[i].ndoc,
              '<?php echo JrTexto::_("Parentesco") ;?>': data[i].nombre,
              '<?php echo JrTexto::_("Telefono") ;?>': data[i].telefono,
              '<?php echo JrTexto::_("Celular") ;?>': data[i].celular,
              '<?php echo JrTexto::_("Mostrar") ;?>': '<a href="javascript:;"  class="btn-chkoption" campo="mostrar"  data-id="'+data[i].idapoderado+'"> <i class="fa fa'+(data[i].mostrar=='1'?'-check':'')+'-circle-o fa-lg"></i> '+estados5976198471759[data[i].mostrar]+'</a>',
              
              '<?php echo JrTexto::_("Actions") ;?>'  :'<a class="btn btn-xs btnvermodal" data-modal="si" href="'+_sysUrlBase_+'/persona_apoderado/editar/?id='+data[i].idapoderado+'" data-titulo="'+tituloedit5976198471759+'"><i class="fa fa-edit"></i></a><a class="btn-eliminar btn btn-xs" href="javascript:;" data-id="'+data[i].idapoderado+'" ><i class="fa fa-trash-o"></i>'
            });
          }
          return datainfo }, error: function(d){console.log(d)}
      }
      <?php echo $this->documento->getIdioma()!='EN'?(',"language": { "url": "'.$this->documento->getUrlStatic().'/libs/datatable1.10/idiomas/'.$this->documento->getIdioma().'.json"}'):''?>
    });

  $('#ventana_<?php echo $idgui; ?>').on('click','.btn-chkoption',function(){     
      var id=$(this).attr('data-id');
      var campo=$(this).attr('campo');
      var data=0;
      if($("i",this).hasClass('fa-circle-o')) data=1;
      $.confirm({
        title: '<?php echo JrTexto::_('Confirm action');?>',
        content: '<?php echo JrTexto::_('It is sure to change the status of this record ?'); ?>',
        confirmButton: '<?php echo JrTexto::_('Accept');?>',
        cancelButton: '<?php echo JrTexto::_('Cancel');?>',
        confirmButtonClass: 'btn-success',
        cancelButtonClass: 'btn-danger',
        closeIcon: true,
        confirm: function(){
          var res = xajax__('', 'persona_apoderado', 'setCampo', id,campo,data);
          if(res) tabledatos5976198471759.ajax.reload();
        }
      });
  });

  $('#ventana_<?php echo $idgui; ?>').on('click','.btnvermodal',function(e){
    e.preventDefault();
    e.stopPropagation();
    var enmodal=$(this).attr('data-modal')||'no';
    var fcall=$(this).attr('data-fcall')||'refreshdatos5976198471759';
    var url=$(this).attr('href')
    if(url.indexOf('?')!=-1) url+='&fcall='+fcall;
    else url+='?fcall='+fcall;
    var ventana=$(this).data('ventana')||'Persona_apoderado';
    var claseid=ventana+'_<?php echo $idgui; ?>';
    var titulo=$(this).attr('data-titulo')||'';
    titulo=titulo.toString().replace('<br>',' ');     
    if(enmodal=='no'){
      return redir(url);          
    }
    url+='&plt=modal';
    openModal('lg',titulo,url,ventana,claseid); 
  });
  
  $('#ventana_<?php echo $idgui; ?> .table').on('click','.btn-eliminar',function(){
     var id=$(this).attr('data-id');
     $.confirm({
      title: '<?php echo JrTexto::_('Confirm action');?>',
      content: '<?php echo JrTexto::_('It is sure to delete this record ?'); ?>',
      confirmButton: '<?php echo JrTexto::_('Accept');?>',
      cancelButton: '<?php echo JrTexto::_('Cancel');?>',
      confirmButtonClass: 'btn-success',
      cancelButtonClass: 'btn-danger',
      closeIcon: true,
      confirm: function(){             
        var res = xajax__('', 'persona_apoderado', 'eliminar', id);
        if(res) tabledatos5976198471759.ajax.reload();
      }
    }); 
  });
});
</script>