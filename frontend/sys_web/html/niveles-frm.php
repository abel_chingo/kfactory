<?php 
defined('RUTA_BASE') or die();
$id_vent = uniqid();
if(!empty($this->datos)) $frm=$this->datos;
?><div class="row" style="<?php echo $this->documento->plantilla=="blanco"?"min-width:600px":"" ?>">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="title_left">
        <h3><?php echo JrTexto::_('Niveles'); ?> <?php echo JrTexto::_($this->frmaccion);?></h3>
        <div class="clearfix"></div>
      </div>
      <div class="x_content" align="center">
      
        <div id="msj-interno"></div>
        <form method="post" id="frm-<?php echo $id_vent;?>"  target="" enctype="" class="form-horizontal form-label-left" >
          <input type="hidden" name="pkIdnivel" id="pkidnivel" value="<?php echo $this->pk;?>">
          <input type="hidden" name="accion" id="pkaccion" value="<?php echo $this->frmaccion;?>">
          <div class="form-group" >
              <label class="col-md-4 col-sm-4 col-xs-12" for="txtNombre">
              <?php echo JrTexto::_('Nombre');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtNombre" name="txtNombre" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["nombre"];?>">
                                  
              </div>
            </div>

            <!--div class="form-group">
              <label class="col-md-4 col-sm-4 col-xs-12" for="txtTipo">
              <?php //echo JrTexto::_('Tipo');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">  
              </div>
            </div-->
            <input type="hidden"  id="txtTipo" name="txtTipo" required="required" class="form-control col-md-7 col-xs-12" value="N">
            <input type="hidden"  id="txtIdpadre" name="txtIdpadre" required="required" class="form-control col-md-7 col-xs-12" value="0">
            <input type="hidden"  id="txtIdpersonal" name="txtIdpersonal" required="required" class="form-control col-md-7 col-xs-12" value="0">
            <!--div class="form-group">
              <label class="col-md-4 col-sm-4 col-xs-12" for="txtIdpadre">
              <?php //echo JrTexto::_('Idpadre');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
               
              </div>
            </div-->

            <!--div class="form-group">
              <label class="col-md-4 col-sm-4 col-xs-12" for="txtIdpersonal">
              <?php //echo JrTexto::_('Idpersonal');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
              </div>
            </div-->

            <div class="form-group">
              <label class="col-md-4 col-sm-4 col-xs-12" for="txtEstado">
              <?php echo JrTexto::_('Estado');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12 " style="text-align: left;">
              <a href="javascript:;"  class="btn-chkoption" > <i class="fa fa<?php echo !empty($frm["estado"])?"-check":""; ?>-circle-o fa-lg"></i>  <span><?php echo @$frm["estado"]=="1"?JrTexto::_("Active"):JrTexto::_("Inactive"); ?></span>
               <input type="hidden" name="txtEstado" value="<?php echo @$frm["estado"];?>" >
              </a>                                
              </div>
            </div>

            
          <div class="ln_solid"></div>
          <div class="form-group">
            <div class="col-md-6 col-md-offset-4">
              <button id="btn-saveNiveles" type="submit" class="btn btn-success" ><i class=" fa fa-save"></i> <?php echo JrTexto::_('Save');?> </button>
              <a type="button" class="btn btn-warning btn-close" href="<?php echo JrAplicacion::getJrUrl(array('niveles'))?>" data-dismiss="modal"  ><i class=" fa fa-repeat"></i> <?php echo JrTexto::_('Cancel');?></a>
            </div>
          </div>
        </form>
      </div>
      
    </div>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function(){  
            
$('#frm-<?php echo $id_vent;?>').bind({    
     submit: function(event){
      event.preventDefault();
      $(this).attr('disabled', true); 
      var res = xajax__('', 'Niveles', 'saveNiveles', xajax.getFormValues('frm-<?php echo $id_vent;?>'));
      if(res){
        <?php if(!empty($this->parent)):?>
        if(typeof window.<?php echo $this->parent?> == 'function') {
          return <?php echo $this->parent?>(res);
        } else {
          return redir('<?php echo JrAplicacion::getJrUrl(array("Niveles"))?>');
        }
        <?php else:?>
        return redir('<?php echo JrAplicacion::getJrUrl(array("Niveles"))?>');
        <?php endif;?>       }
     }
  });

$('.btn-chkoption').bind({
    click: function() {     
      var data=0;
      if($("i",this).hasClass('fa-circle-o')) data=1;
      $("i",this).removeClass().addClass('fa fa'+(data==1?'-check-':'-')+'circle-o');
      $("span",this).html(data==1?'<?php echo JrTexto::_("Active"); ?>':'<?php echo JrTexto::_("Inactive"); ?>');
      $("input",this).val(data);

     
    }
  });







  
  
});


</script>

