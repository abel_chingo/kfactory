<?php defined('RUTA_BASE') or die(); 
$datos=$this->datos[0];
?>
	<br>   
<div>
    <ul class="nav nav-tabs">
      <li class="active"><a data-toggle="tab" href="#tbgeneral"><?php echo JrTexto::_('General information') ?></a></li>
      <li><a data-toggle="tab" href="#tblocation"><?php echo JrTexto::_('Location information') ?></a></li>
      <li><a data-toggle="tab" href="#tbcontact"><?php echo JrTexto::_('Contact information') ?></a></li>
      <li><a data-toggle="tab" href="#tbuser"><?php echo JrTexto::_('User information') ?></a></li>
    </ul>
    <div class="tab-content">
    <br>
      <div class="alert alert-info alert-dismissible" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <strong><?php echo JrTexto::_('Information') ?></strong> click sobre tu informacion y modificalo si lo crees necesario 
      </div>
      <div id="tbgeneral" class="tab-pane fade in active">
        <h3><?php echo JrTexto::_('General information') ?></h3>
        <div class="form-group col-md-12 col-sm-12 col-xs-12">
            <label class="control-label col-md-3 col-sm-3 col-xs-12 text-right"><?php echo JrTexto::_('DNI') ?> <span class="required">*</span></label>            
            <div class="col-md-9 col-sm-9 col-xs-12"  style="cursor:no-drop">
                <span class="info"><?php echo $datos["dni"]; ?></span>                           
            </div>
        </div>
        <div class="form-group col-md-12 col-sm-12 col-xs-12">
            <label class="control-label col-md-3 col-sm-3 col-xs-12 text-right"><?php echo JrTexto::_('Paternal Surname') ?> <span class="required">*</span></label>            
            <div class="col-md-9 col-sm-9 col-xs-12  dedit"  >
                <span class="info dataedit" data-campo="ape_paterno" data-tipo="text"  style="cursor:pointer"><span><?php echo $datos["ape_paterno"]; ?></span> <i class="fa fa-edit"></i></span>
                <span class="infoput" style="display:none"></span>                 
            </div>
        </div>
        <div class="form-group col-md-12 col-sm-12 col-xs-12">
            <label class="control-label col-md-3 col-sm-3 col-xs-12 text-right"><?php echo JrTexto::_('Maternal Surname') ?> <span class="required">*</span></label>            
            <div class="col-md-9 col-sm-9 col-xs-12  dedit" >
                <span class="info dataedit" data-campo="ape_materno"  data-tipo="text"  style="cursor:pointer"><span><?php echo $datos["ape_materno"]; ?></span> <i class="fa fa-edit"></i></span> 
                <span class="infoput" style="display:none"></span>              
            </div>
        </div>
        <div class="form-group col-md-12 col-sm-12 col-xs-12">
            <label class="control-label col-md-3 col-sm-3 col-xs-12 text-right"><?php echo JrTexto::_('Name') ?> <span class="required">*</span></label>            
            <div class="col-md-9 col-sm-9 col-xs-12 dedit">
                <span class="info dataedit" data-campo="nombre"  data-tipo="text"  style="cursor:pointer"><span><?php echo $datos["nombre"]; ?></span> <i class="fa fa-edit"></i></span>
                <span class="infoput" style="display:none"></span>                  
            </div>
        </div>
        <div class="form-group col-md-12 col-sm-12 col-xs-12">
            <label class="control-label col-md-3 col-sm-3 col-xs-12 text-right"><?php echo JrTexto::_('Date of birth') ?> <span class="required">*</span></label>            
            <div class="col-md-9 col-sm-9 col-xs-12 dedit"  >
              <span class="info dataedit" data-campo="fechanac" data-tipo="date" style="cursor:pointer">
                <span class="data"><?php echo $datos["fechanac"]; ?> <i class="fa fa-edit"></i></span>                
              </span>
              <span class="infoput" style="display:none">
                  <input type='infoput' value="<?php echo $datos["fechanac"]; ?>" class='txt txtdate form-control' />
              </span>  
            </div>
        </div>
         <div class="form-group col-md-12 col-sm-12 col-xs-12">
            <label class="control-label col-md-3 col-sm-3 col-xs-12 text-right"><?php echo JrTexto::_('Gender') ?> <span class="required">*</span></label>            
            <div class="col-md-9 col-sm-9 col-xs-12 edit">
              <span class="info dataedit" data-campo="sexo" data-tipo="cbo" style="cursor:pointer">
                <span class="data"><?php echo $datos["sexo"]=='M'?JrTexto::_('Male'):JrTexto::_('Female'); ?> <i class="fa fa-edit"></i></span>
              </span>
              <span class="infoput" style="display:none">
                  <select class='cbo form-control' >
                    <option value="M" <?php echo $datos["sexo"]=='M'?'selected="selected"':'';?>><?php echo JrTexto::_('Male') ?></option>
                    <option value="F" <?php echo $datos["sexo"]=='F'?'selected="selected"':'';?>><?php echo JrTexto::_('Female') ?></option>
                  </select>
              </span>              
            </div>
        </div>
        <div class="form-group col-md-12 col-sm-12 col-xs-12">
            <label class="control-label col-md-3 col-sm-3 col-xs-12 text-right"><?php echo JrTexto::_('civil status') ?> <span class="required">*</span></label>            
            <div class="col-md-9 col-sm-9 col-xs-12 edit " >
              <span class="info dataedit" data-campo="estado_civil" data-tipo="cbo" style="cursor:pointer">
                <span class="data"><?php                     
                    if($datos["estado_civil"]=='C')
                      echo JrTexto::_('Casado');
                    elseif($datos["estado_civil"]=='V')
                      echo JrTexto::_('Viudo');
                     elseif($datos["estado_civil"]=='D')
                      echo JrTexto::_('Divorciado');
                     else
                      echo JrTexto::_('Soltero');
                   ?> <i class="fa fa-edit"></i>
                </span>
              </span>
                <span class="infoput" style="display:none">
                  <select class='cbo form-control' >
                    <option value="S" <?php echo $datos["estado_civil"]=='S'?'selected="selected"':'';?>><?php echo JrTexto::_('Soltero') ?></option>
                    <option value="C" <?php echo $datos["estado_civil"]=='C'?'selected="selected"':'';?>><?php echo JrTexto::_('Casado') ?></option>
                    <option value="V" <?php echo $datos["estado_civil"]=='V'?'selected="selected"':'';?>><?php echo JrTexto::_('Viudo') ?></option>
                    <option value="D" <?php echo $datos["estado_civil"]=='D'?'selected="selected"':'';?>><?php echo JrTexto::_('Divorciado') ?></option>
                  </select>
                </span>
            </div>
        </div>     
      </div>
      <div id="tblocation" class="tab-pane fade">
        <h3><?php echo JrTexto::_('General information') ?></h3>
        <div class="form-group col-md-12 col-sm-12 col-xs-12">
            <label class="control-label col-md-3 col-sm-3 col-xs-12 text-right"><?php echo JrTexto::_('DNI') ?> <span class="required">*</span></label>            
            <div class="col-md-9 col-sm-9 col-xs-12"  style="cursor:no-drop">
                <span class="info"><?php echo $datos["dni"]; ?></span>                           
            </div>
        </div>
        <div class="form-group col-md-12 col-sm-12 col-xs-12">
            <label class="control-label col-md-3 col-sm-3 col-xs-12 text-right"><?php echo JrTexto::_('Paternal Surname') ?> <span class="required">*</span></label>            
            <div class="col-md-9 col-sm-9 col-xs-12  dedit"  >
                <span class="info dataedit" data-campo="ape_paterno" data-tipo="text"  style="cursor:pointer"><span><?php echo $datos["ape_paterno"]; ?></span> <i class="fa fa-edit"></i></span>
                <span class="infoput" style="display:none"></span>                 
            </div>
        </div>
      </div>
      <div id="tbcontact" class="tab-pane fade">
        <h3>Menu 2</h3>
        <p>Some content in menu 2.</p>
      </div>
      <div id="tbuser" class="tab-pane fade">
        <h3>Menu 3</h3>
        <p>Some content in menu 3.</p>
      </div>     
    </div>
</div>

<script type="text/javascript">
$(document).ready(function(){
    $('.txtdate').datetimepicker({
      format: 'YYYY-MM-DD',
      viewMode: 'years'
    });

    $('.dataedit').click(function(){ 
        var tipo=$(this).attr('data-tipo');
        var info= $(this);
        info.hide('fast');
        p=info.siblings('.infoput').show();
        if(tipo=='text'){          
          p.html('<input type="text" required="required" class="txt form-control edittxt" value="'+info.text().trim()+'">');
          $('input',p).focus();
        }
    });
    
    
    $('.infoput').on('keypress','input.txt',function(e){  
          if(e.which==13)  $(this).trigger("blur");
    });

    $('.infoput').on('change','.cbo',function(e){  
          $(this).trigger("blur");
    });
      
    $('.infoput').on('blur','input.txt, .txtdate, .cbo',function(e){
         var info=$(this).parent('.infoput').siblings('.info');
         var campo=info.attr('data-campo');
         var tipo=info.attr('data-tipo');
         var value=$(this).val().trim();         
         var data={'campo':campo,'value':value};
         var res = xajax__('', 'usuario', 'setUsuario',data);
         
         if(tipo=='text'){
          info.html(value + ' <i class="fa fa-edit"></i>');
          $(this).parent('.infoput').hide('fast');
         }
         else if(tipo=='date'){
          info.show('fast').html(value+' <i class="fa fa-edit"></i>'); 
           $(this).parent('.infoput').hide('fast');
         }
         else if(tipo=='cbo'){
          txt=$(":selected",this).text();          
          info.show('fast').html(txt+' <i class="fa fa-edit"></i>');
          $(this).parent('.infoput').hide('fast');
         }
         info.show('fast');
    });
	  $('#formRecupClave').bind({
		  submit: function() {
			xajax__('', 'sesion', 'solicitarCambioClave', xajax.getFormValues('formRecupClave'));
		 }
    });

});
</script>