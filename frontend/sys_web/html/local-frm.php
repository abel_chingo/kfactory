<?php 
defined('RUTA_BASE') or die();
$id_vent = uniqid();
if(!empty($this->datos)) $frm=$this->datos;
?><div class="row" style="<?php echo $this->documento->plantilla=="blanco"?"min-width:600px":"" ?>">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2><?php echo JrTexto::_('Local'); ?><small id="frmaction"><?php echo JrTexto::_($this->frmaccion);?></small></h2>
        <div class="clearfix"></div>
      </div>


      
         
      <div class="x_content">
        <div id="msj-interno"></div>
        <form method="post" id="frm-<?php echo $id_vent;?>"  target="" enctype="" class="form-horizontal form-label-left" >
          <input type="hidden" name="pkIdlocal" id="pkidlocal" value="<?php echo JrTexto::_($this->pk);?>">
          <input type="hidden" name="accion" id="pkaccion" value="<?php echo JrTexto::_($this->frmaccion);?>">
          <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtNombre">
              <?php echo JrTexto::_('Nombre');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtNombre" name="txtNombre" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["nombre"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtDireccion">
              <?php echo JrTexto::_('Direccion');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtDireccion" name="txtDireccion" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["direccion"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtId_ubigeo">
              <?php echo JrTexto::_('Departamento');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                
                <?php
                $_iddep=substr(@$frm["id_ubigeo"],0,2);
                $_idpro=substr(@$frm["id_ubigeo"],2,2);
                $_idubigeo=@$frm["id_ubigeo"];
                
                ?>
                <select id="txtiddepartamento" name="txtIddepartamento" class="form-control">
                <option value="">Seleccione Departamento</option>
                <?php
                foreach ($this->departamentos as $depa) { ?>
                <option value="<?php echo $depa["id_ubigeo"]; ?>" <?php echo $_iddep===substr($depa["id_ubigeo"],0,2)?'selected="selected"':''?>> <?php echo $depa["ciudad"]?> </option>
                <?php } ?>
                </select>

                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtidprovincia">
              <?php echo JrTexto::_('Provincia');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                
                <select id="txtidprovincia" name="txtidprovincia" class="form-control">
                <option value="">Seleccione Provincia</option>
                <?php 
                $selidpadre=@$frm["idprovincia"];
                foreach ($this->provincias as $idpro) { ?>
                <option value="<?php echo $idpro["id_ubigeo"]; ?>" <?php echo $_idpro===substr($idpro["id_ubigeo"],2,2)?'selected="selected"':''?>> <?php echo $idpro["ciudad"]?> </option>
                <?php } ?>
                </select>

              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtId_ubigeo">
              <?php echo JrTexto::_('Distrito');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                
                <select id="txtId_ubigeo" name="txtId_ubigeo" class="form-control">
                <option value="">Seleccione Distrito</option>
                <?php 
                $selidpadre=@$frm["iddistrito"];
                foreach ($this->distritos as $iddis) { ?>
                <option value="<?php echo $iddis["id_ubigeo"]; ?>" <?php echo $_idubigeo===$iddis["id_ubigeo"]?'selected="selected"':''?>> <?php echo $iddis["ciudad"]?> </option>
                <?php } ?>
                </select>
                         
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtTipo">
              <?php echo JrTexto::_('Tipo');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                
                <?php
                $_tipo=@$frm["tipo"];
                ?>
                <select id="txtTipo" name="txtTipo" class="form-control">
                <option value="T" <?php if ($_tipo=='T') echo "selected" ?>>Centro de Trabajo</option>
                <option value="C" <?php if ($_tipo=='C') echo "selected" ?>>Centro de Capacitación</option>
                </select>
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtVacantes">
              <?php echo JrTexto::_('Vacantes');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtVacantes" name="txtVacantes" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["vacantes"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtIdugel">
              <?php echo JrTexto::_('Idugel');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                
                <select id="txtIdugel" name="txtIdugel" class="form-control">
                <option value="">Seleccione Ugel</option>
                <?php 
                $selidpadre=@$frm["idugel"];
                foreach ($this->ugeles as $idugel) { ?>
                <option value="<?php echo $idugel["idugel"]; ?>" <?php echo $selidpadre===$idugel["idugel"]?'selected="selected"':''?>> <?php echo $idugel["descripcion"]?> </option>
                <?php } ?>
                </select>

              </div>
            </div>

            
          <div class="ln_solid"></div>
          <div class="form-group">
            <div class="col-md-6 col-md-offset-4">
              <button id="btn-saveLocal" type="submit" class="btn btn-success" ><i class=" fa fa-save"></i> <?php echo JrTexto::_('Save');?> </button>
              <a type="button" class="btn btn-warning btn-close" href="<?php echo JrAplicacion::getJrUrl(array('local'))?>" data-dismiss="modal"  ><i class=" fa fa-repeat"></i> <?php echo JrTexto::_('Cancel');?></a>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function(){  
            
$('#frm-<?php echo $id_vent;?>').bind({    
     submit: function(event){
      event.preventDefault();
      $(this).attr('disabled', true); 
      var res = xajax__('', 'Local', 'saveLocal', xajax.getFormValues('frm-<?php echo $id_vent;?>'));
      if(res){
        <?php if(!empty($this->parent)):?>
        if(typeof window.<?php echo $this->parent?> == 'function') {
          return <?php echo $this->parent?>(res);
        } else {
          return redir('<?php echo JrAplicacion::getJrUrl(array("Local"))?>');
        }
        <?php else:?>
        return redir('<?php echo JrAplicacion::getJrUrl(array("Local"))?>');
        <?php endif;?>       }
     }
  });


$('#txtiddepartamento').change(function(){
    var iddep=$(this).val();
    //alert(iddep);
    $('#txtidprovincia option').remove();
    $('#txtId_ubigeo option').remove();
    var data={'idpadre':iddep}
    var res = xajax__('', 'ugel', 'getxPadre',data);
    if(res){      //$('#txtUnidad').append('<option value=-1>'+x["nombre"]+'</option>');
    $('#txtidprovincia').append('<option>Seleccione Provincia</option>');

      $.each(res,function(){
        x=this;
        $('#txtidprovincia').append('<option value='+x["id_ubigeo"]+'>'+x["ciudad"]+'</option>');
      });
       $( "#filtros" ).submit();
    }
  });

$('#txtidprovincia').change(function(){
    var idpro=$(this).val();
    //alert(idpro);
    $('#txtId_ubigeo option').remove();
    var data={'idpadre1':idpro}
    var res = xajax__('', 'ugel', 'getxPadre',data);
    if(res){      //$('#txtUnidad').append('<option value=-1>'+x["nombre"]+'</option>');}
    $('#txtId_ubigeo').append('<option>Seleccione Distrito</option>');
      $.each(res,function(){
        x=this;
        $('#txtId_ubigeo').append('<option value='+x["id_ubigeo"]+'>'+x["ciudad"]+'</option>');
      });
       $( "#filtros" ).submit();
    }
  });






  
  
});


</script>

