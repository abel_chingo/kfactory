<?php 
defined('RUTA_BASE') or die();
$idgui = uniqid();
$usuarioAct = NegSesion::getUsuario();
?>
<style type="text/css">
	.slick-items{
		position: relative;
	}
	.panel{
		border: 1px solid #dad7d7;  margin-top: 1ex; margin-bottom:0px;  
		-webkit-box-shadow: 2px 1px 2px 1px #e6e0e0;
        -moz-box-shadow: 2px 1px 2px 1px #e6e0e0;
        box-shadow: 2px 1px 2px 1px #e6e0e0;

	}
	.panel-heading{
		color:#fff; font-size: 0.9em;
	}
	.slick-item .image{
		overflow: hidden;
		width: 100%		
	}
	.slick-item{
		text-align: center !important;	

	}
	.slick-slide img ,.slick-item img{
		display: inline-block;
		text-align: center;
	}
	.slick-item .titulo{
		font-size: 1.6em;
		text-align: center;
	}

	
</style>
<div class="row">
	<div class="col-md-12">
		<div class="panel" >
			<div class="panel-heading bg-blue" >
				<h3 class="panel-title" style="font-size: 1.8em;"><?php echo JrTexto::_('Maintenance'); ?></h3>
				<span class="pull-right clickable"><i class="glyphicon glyphicon-chevron-up"></i></span>
			</div>
			<div class="panel-body">			
			   <div class="row">
			   		<div class="col-md-12">
				   		<div class="slick-items">		                                 
		                    <div class="slick-item"> 
		                        <a href="<?php echo $this->documento->getUrlBase();?>/cargos" class="hvr-float">
		                        	<img class="img-responsive" src="<?php echo $this->documento->getUrlStatic();?>/media/academico/cargos.png">
		                            <div class="titulo"><?php echo JrTexto::_('Cargos'); ?></div>
		                        </a>
		                    </div>
		                    <div class="slick-item"> 
		                        <a href="<?php echo $this->documento->getUrlBase();?>/alumno" class="hvr-float">
		                        	<img class="img-responsive" src="<?php echo $this->documento->getUrlStatic();?>/media/academico/estudiantes.png">
		                            <div class="titulo"><?php echo JrTexto::_('Students'); ?></div>
		                        </a>
		                    </div>
		                    <div class="slick-item"> 
		                        <a href="<?php echo $this->documento->getUrlBase();?>/personal" class="hvr-float">
		                        	<img class="img-responsive" src="<?php echo $this->documento->getUrlStatic();?>/media/academico/personal.png">
		                            <div class="titulo"><?php echo JrTexto::_('Workforce'); ?></div>
		                        </a>
		                    </div>
		                    <div class="slick-item"> 
		                        <a href="<?php echo $this->documento->getUrlBase();?>/cursos" class="hvr-float">
		                        	<img class="img-responsive" src="<?php echo $this->documento->getUrlStatic();?>/media/academico/cursos.png">
		                            <div class="titulo"><?php echo JrTexto::_('Courses'); ?></div>
		                        </a>
		                    </div>
		                    <div class="slick-item"> 
		                        <a href="<?php echo $this->documento->getUrlBase();?>/infraestructura" class="hvr-float">
		                        	<img class="img-responsive" src="<?php echo $this->documento->getUrlStatic();?>/media/academico/infraestructura.png">
		                            <div class="titulo"><?php echo JrTexto::_('Infrastructure'); ?></div>
		                        </a>
		                    </div>
		                    <div class="slick-item"> 
		                        <a href="<?php echo $this->documento->getUrlBase();?>/certificados" class="hvr-float">
		                        	<img class="img-responsive" src="<?php echo $this->documento->getUrlStatic();?>/media/academico/certificados.png">
		                            <div class="titulo"><?php echo JrTexto::_('Certificates'); ?></div>
		                        </a>
		                    </div>		                   
		                    <div class="slick-item" style="text-align: center !important;"> 
		                        <a href="<?php echo $this->documento->getUrlBase();?>/tramitedocumentario" class="hvr-float" style="text-align: center !important;">
		                        	<img src="<?php echo $this->documento->getUrlStatic();?>/media/academico/tramitedocumentario.png">
		                            <div class="titulo"><?php echo JrTexto::_('Documentary procedure'); ?></div>
		                        </a>
		                    </div>
		                </div>
					</div>
				</div>
			</div>
		</div>
		<div class="panel" >
			<div class="panel-heading bg-blue" >
				<h3 class="panel-title" style="font-size: 1.8em;"><?php echo JrTexto::_('Management'); ?></h3>
				<span class="pull-right clickable"><i class="glyphicon glyphicon-chevron-up"></i></span>
			</div>
			<div class="panel-body">			
			   <div class="row">
			   		<div class="col-md-12">
				   		<div class="slick-items">		                                 
		                    <div class="slick-item"> 
		                        <a href="<?php echo $this->documento->getUrlBase();?>/grupoestudio" class="hvr-float">
		                        	<img class="img-responsive" src="<?php echo $this->documento->getUrlStatic();?>/media/academico/grupoestudio.png">
		                            <div class="titulo"><?php echo JrTexto::_('Study group'); ?></div>
		                        </a>
		                    </div>
		                    <div class="slick-item"> 
		                        <a href="<?php echo $this->documento->getUrlBase();?>/matriculas" class="hvr-float">
		                        	<img class="img-responsive" src="<?php echo $this->documento->getUrlStatic();?>/media/academico/matriculas.png">
		                            <div class="titulo"><?php echo JrTexto::_('Matriculate'); ?></div>
		                        </a>
		                    </div>
		                    <div class="slick-item"> 
		                        <a href="<?php echo $this->documento->getUrlBase();?>/marcacion" class="hvr-float">
		                        	<img class="img-responsive" src="<?php echo $this->documento->getUrlStatic();?>/media/academico/marcacion.png">
		                            <div class="titulo"><?php echo JrTexto::_('Dialing'); ?></div>
		                        </a>
		                    </div>
		                    <div class="slick-item"> 
		                        <a href="<?php echo $this->documento->getUrlBase();?>/asistencias" class="hvr-float">
		                        	<img class="img-responsive" src="<?php echo $this->documento->getUrlStatic();?>/media/academico/asistencia.png">
		                            <div class="titulo"><?php echo JrTexto::_('Assistance'); ?></div>
		                        </a>
		                    </div>
		                    <div class="slick-item"> 
		                        <a href="<?php echo $this->documento->getUrlBase();?>/notas" class="hvr-float">
		                        	<img class="img-responsive" src="<?php echo $this->documento->getUrlStatic();?>/media/academico/notas.png">
		                            <div class="titulo"><?php echo JrTexto::_('Notes'); ?></div>
		                        </a>
		                    </div>		                   
		                </div>
					</div>
				</div>
			</div>
		</div>
		<div class="panel" >
			<div class="panel-heading bg-blue">
				<h3 class="panel-title" style="font-size: 1.8em;"><?php echo JrTexto::_('Complementary options'); ?></h3>
				<span class="pull-right clickable"><i class="glyphicon glyphicon-chevron-up"></i></span>
			</div>
			<div class="panel-body">			
			   <div class="row">
			   		<div class="col-md-12">
				   		<div class="slick-items">
		                    <div class="slick-item"> 
		                        <a href="<?php echo $this->documento->getUrlBase();?>/bolsa" class="hvr-float">
		                        	<img class="img-responsive" src="<?php echo $this->documento->getUrlStatic();?>/media/academico/bolsa.png">
		                            <div class="titulo"><?php echo JrTexto::_('Job vacancies'); ?></div>
		                        </a>
		                    </div>
		                    <div class="slick-item"> 
		                        <a href="<?php echo $this->documento->getUrlBase();?>/materialestudio" class="hvr-float">
		                        	<img class="img-responsive" src="<?php echo $this->documento->getUrlStatic();?>/media/academico/materialestudio.png">
		                            <div class="titulo"><?php echo JrTexto::_('Study material'); ?></div>
		                        </a>
		                    </div>
		                    <div class="slick-item"> 
		                        <a href="<?php echo $this->documento->getUrlBase();?>/materialayuda" class="hvr-float">
		                        	<img class="img-responsive" src="<?php echo $this->documento->getUrlStatic();?>/media/academico/materialayuda.png">
		                            <div class="titulo"><?php echo JrTexto::_('Help material'); ?></div>
		                        </a>
		                    </div>		                    
		                </div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function(e){	
		var optionslike={
	 		//dots: true,
		  	infinite: false,
		  	//speed: 300,
		   	//adaptiveHeight: true
	   		navigation: false,
	  		slidesToScroll: 1,
	  		centerPadding: '60px',
	  	  slidesToShow: 6,
	  	  responsive:[
	          { breakpoint: 1200, settings: {slidesToShow: 5} },
	          { breakpoint: 992, settings: {slidesToShow: 4 } },
	          { breakpoint: 880, settings: {slidesToShow: 3 } },
	          { breakpoint: 720, settings: {slidesToShow: 2 } },
	          { breakpoint: 320, settings: {slidesToShow: 1 /*,arrows: false, centerPadding: '40px',*/} }	  	
	  	  ]
 		};
 	var slikitems=$('.slick-items').slick(optionslike); 	
    $('header').show('fast').addClass('static');
	});
</script>