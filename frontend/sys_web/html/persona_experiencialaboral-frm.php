<?php 
defined('RUTA_BASE') or die();
$id_vent = uniqid();
if(!empty($this->datos)) $frm=$this->datos;
$ventanapadre=!empty($_REQUEST["fcall"])?$_REQUEST["fcall"]:'eeeexzx-1';
?><div class="row" style="<?php echo $this->documento->plantilla=="blanco"?"min-width:600px":"" ?>">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="panel">
      <?php if($this->documento->plantilla!='modal'){?><div class="panel-heading bg-blue">
        <h3><?php echo JrTexto::_('Persona_experiencialaboral'); ?><small id="frmaction"> <?php echo JrTexto::_($this->frmaccion);?></small></h3>
        <div class="clearfix"></div>
      </div><?php } ?>      
      <div class="panel-body">
        <div id="msj-interno"></div>
        <form method="post" id="frm-<?php echo $id_vent;?>"  target="" enctype="" class="form-horizontal form-label-left" >
          <input type="hidden" name="pkIdexperiencia" id="pkidexperiencia" value="<?php echo JrTexto::_($this->pk);?>">
          <input type="hidden" name="accion" id="pkaccion" value="<?php echo JrTexto::_($this->frmaccion);?>">
          <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtIdpersona">
              <?php echo JrTexto::_('Idpersona');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
              <select id="txtidpersona" name="txtIdpersona" class="form-control">
               <option value=""><?php echo JrTexto::_('Seleccione'); ?></option>
              <?php 
              if(!empty($this->fkidpersona))
                foreach ($this->fkidpersona as $fkidpersona) { ?><option value="<?php echo $fkidpersona["dni"]?>" <?php echo $fkidpersona["dni"]==@$frm["idpersona"]?"selected":""; ?> ><?php echo $fkidpersona["nombre"] ?></option><?php } ?>              </select>
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtEmpresa">
              <?php echo JrTexto::_('Empresa');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtEmpresa" name="txtEmpresa" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["empresa"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtRubro">
              <?php echo JrTexto::_('Rubro');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtRubro" name="txtRubro" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["rubro"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtArea">
              <?php echo JrTexto::_('Area');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtArea" name="txtArea" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["area"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtCargo">
              <?php echo JrTexto::_('Cargo');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtCargo" name="txtCargo" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["cargo"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtFunciones">
              <?php echo JrTexto::_('Funciones');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input type="text"  id="txtFunciones" name="txtFunciones" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo @$frm["funciones"];?>">
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtFechade">
              <?php echo JrTexto::_('Fechade');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input name="txtFechade" id="txtFechade" class="verdate form-control col-md-7 col-xs-12" required="required" type="date" value="<?php echo @$frm["fechade"];?>">   
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtFechahasta">
              <?php echo JrTexto::_('Fechahasta');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                <input name="txtFechahasta" id="txtFechahasta" class="verdate form-control col-md-7 col-xs-12" required="required" type="date" value="<?php echo @$frm["fechahasta"];?>">   
                                  
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtActualmente">
              <?php echo JrTexto::_('Actualmente');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                                <a style="cursor:pointer;" class="chkformulario fa  <?php echo @$frm["actualmente"]==1?"fa-check-circle":"fa-circle-o";?>" 
                data-value="<?php echo @$frm["actualmente"];?>"  data-valueno="0" data-value2="<?php echo @$frm["actualmente"]==1?1:0;?>">
                 <span> <?php echo JrTexto::_(@$frm["actualmente"]==1?"Activo":"Inactivo");?></span>
                 <input type="hidden" name="txtActualmente" value="<?php echo !empty($frm["actualmente"])?$frm["actualmente"]:0;?>" > 
                 </a>
                                                      
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-md-4 col-sm-4 col-xs-12" for="txtMostrar">
              <?php echo JrTexto::_('Mostrar');?> <span class="required"> * :</span>
              </label>
              <div class="col-md-8 col-sm-8 col-xs-12">
                                <a style="cursor:pointer;" class="chkformulario fa  <?php echo @$frm["mostrar"]==1?"fa-check-circle":"fa-circle-o";?>" 
                data-value="<?php echo @$frm["mostrar"];?>"  data-valueno="0" data-value2="<?php echo @$frm["mostrar"]==1?1:0;?>">
                 <span> <?php echo JrTexto::_(@$frm["mostrar"]==1?"Activo":"Inactivo");?></span>
                 <input type="hidden" name="txtMostrar" value="<?php echo !empty($frm["mostrar"])?$frm["mostrar"]:0;?>" > 
                 </a>
                                                      
              </div>
            </div>

            
          <div class="ln_solid"></div>
          <div class="form-group">
            <div class="col-md-6 col-md-offset-4">
              <button id="btn-savePersona_experiencialaboral" type="submit" class="btn btn-success" ><i class=" fa fa-save"></i> <?php echo JrTexto::_('Save');?> </button>
              <a type="button" class="btn btn-warning btn-close" href="<?php echo JrAplicacion::getJrUrl(array('persona_experiencialaboral'))?>" data-dismiss="modal"  ><i class=" fa fa-repeat"></i> <?php echo JrTexto::_('Cancel');?></a>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function(){  
  
  $('#txtfechade').datetimepicker({
    lang:'es',
    timepicker:false,
    format:'Y/m/d'
  });
  $('#txtfechahasta').datetimepicker({
    lang:'es',
    timepicker:false,
    format:'Y/m/d'
  });          
$('#frm-<?php echo $id_vent;?>').bind({    
     submit: function(event){
      event.preventDefault();
      $(this).attr('disabled', true); 
      var res = xajax__('', 'persona_experiencialaboral', 'savePersona_experiencialaboral', xajax.getFormValues('frm-<?php echo $id_vent;?>'));
      if(res){
        if(typeof <?php echo $ventanapadre?> == 'function'){
          <?php echo $ventanapadre?>(res);
          $(this).closest('.modal').find('.cerrarmodal').trigger('click');
        }else return redir('<?php echo JrAplicacion::getJrUrl(array("Persona_experiencialaboral"))?>');
      }
     }
  });

 
  
});

$('.chkformulario').bind({
    click: function() {     
      if($(this).hasClass('fa-circle-o')) {
        $('span',this).text(' <?php echo JrTexto::_("Active");?>');
        $('input',this).val(1);
        $(this).removeClass('fa-circle-o').addClass('fa-check-circle');
      }else {
        $('span',this).text(' <?php echo JrTexto::_("Inactive");?>');
        $('input',this).val(0);
        $(this).addClass('fa-circle-o').removeClass('fa-check-circle');
      }      
    }
  });

</script>

