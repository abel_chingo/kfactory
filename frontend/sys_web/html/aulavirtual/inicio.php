<style type="text/css">
	.border0{
		border: 0px;
	}
	.input-group-addon{
		border-radius: 0.2ex;
	}
	.border1{
		margin-top:0px; 
		border:2px solid #4683af;
		border-radius: 0.25ex;
	}
</style>
<div class="row">
	<div class="col-xs-12">
        <ol class="breadcrumb">
            <li><a href="<?php echo $this->documento->getUrlBase();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_('Home'); ?></a></li>                  
            <li class="active"><?php echo JrTexto::_('Aulas Virtuales'); ?></li>
        </ol>
	</div>
</div>
<div class="row">
<div class="col-xs-6 col-sm-4 col-md-3">
	<label><?php echo ucfirst(JrTexto::_("Level"))?></label>
	<div class="cajaselect">
		<select name="nivel" id="level-item" class="conestilo">
     	<option value="" ><?php echo ucfirst(JrTexto::_("All Levels"))?></option>
     	<?php if(!empty($this->niveles))
             foreach ($this->niveles as $nivel){?>
             <option value="<?php echo $nivel["idnivel"]; ?>" <?php echo $nivel["idnivel"]==$this->idnivel?'Selected':'';?>><?php echo $nivel["nombre"]?></option>
      	<?php }?>
    	</select>
	</div>
</div>
<div class="col-xs-6 col-sm-4 col-md-3">
	<label><?php echo  ucfirst(JrTexto::_("Unit"))?></label>
	<div class="cajaselect"> 
	<select name="unidad" id="unit-item" class="conestilo">
  	<option value="" ><?php echo ucfirst(JrTexto::_("All Unit"))?></option>
  	<?php if(!empty($this->unidades))
             foreach ($this->unidades as $unidad){?>
             <option value="<?php echo $unidad["idnivel"]; ?>" <?php echo $unidad["idnivel"]==$this->idunidad?'Selected':'';?>><?php echo $unidad["nombre"]?></option>
      <?php }?>
	</select>
	</div>
</div>
<div class="col-xs-6 col-sm-4 col-md-3">
	<label><?php echo  ucfirst(JrTexto::_("Activity"))?></label>
	<div class="cajaselect"> 
  	<select name="actividad" id="activity-item" class="conestilo">
      	<option value="" ><?php echo ucfirst(JrTexto::_("All Activity"))?></option>
      	 <?php if(!empty($this->actividades))
                 foreach ($this->actividades as $act){?>
                 <option value="<?php echo $act["idnivel"]; ?>" <?php echo $act["idnivel"]==$this->idactividad?'Selected':'';?>><?php echo $act["nombre"]?></option>
          <?php }?>
    </select>
	</div>
</div>
<div class="col-xs-6 col-sm-4 col-md-3">
	<label><?php echo  ucfirst(JrTexto::_("State"));?></label>
	<div class="cajaselect"> 
  	<select name="estado" id="estado" class="conestilo">
      	<option value="0"><?php echo ucfirst(JrTexto::_("All state"))?></option>
      	<option value="A"><?php echo ucfirst(JrTexto::_("Active"))?></option>
      	<option value="I"><?php echo ucfirst(JrTexto::_("Inactive"))?></option>
      	<option value="C"><?php echo ucfirst(JrTexto::_("Cancelled"))?></option>
    </select>
	</div>
</div>
</div>
<hr>
<div class="row">
<div class="col-xs-6 col-sm-4 col-md-3">
	<div class="form-group">		
        <div class='input-group date datetimepicker1 border1' >
        	<span class="input-group-addon btn"><span class="glyphicon glyphicon-calendar"></span> <?php echo  ucfirst(JrTexto::_("date first"))?> </span>
            <input type='text' class="form-control border0" name="fecha_inicio" id="fecha_inicio" />           
        </div>
    </div>	
</div>
<div class="col-xs-6 col-sm-4 col-md-3">
	<div class="form-group">
        <div class='input-group date datetimepicker2 border1'>
        	<span class="input-group-addon btn"><span class="glyphicon glyphicon-calendar"></span> <?php echo  ucfirst(JrTexto::_("date finish"))?> </span>
            <input type='text' class="form-control border0" name="fecha_final" id="fecha_final" />           
        </div>
    </div>
</div>
<div class="col-xs-12 col-sm-6 col-md-6">
	<div class="form-group">
	<div class="input-group border1">
	  <input type="text" name="titulo" id="titulo" class="form-control border0" placeholder="<?php echo  ucfirst(JrTexto::_("title or name clase"))?>">
	  <span class="input-group-addon btn btnbuscar">
            <?php echo  ucfirst(JrTexto::_("buscar"))?> <i class="fa fa-search"></i>
       </span>	
	</div>
	</div>
</div>
</div>

<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
	   <div class="x_panel">
        <div class="x_title"><br>
          <a class="btn btn-success btn-sm" href="<?php echo JrAplicacion::getJrUrl(array("Aulasvirtuales", "agregar"));?>">
          <i class="fa fa-plus"></i> <?php echo ucfirst(JrTexto::_('add'))?></a>        
          <div class="clearfix"></div>
        </div>
        <div class="div_linea"></div>
         <div class="x_content">
            <table class="table table-striped table-responsive">
              <thead>
                <tr class="headings">
                  <th>#</th>                    
                    <th><?php echo JrTexto::_("Titulo") ;?></th>
                    <th><?php echo JrTexto::_("Fecha") ;?></th>
                    <th><?php echo JrTexto::_("Video") ;?></th>
                    <th><?php echo JrTexto::_("Estado") ;?></th>                 
                    <th class="sorting_disabled"><span class="nobr"><?php echo JrTexto::_('Actions');?></span></th>
                </tr>
              </thead>
              <tbody>
              <?php /*$i=0; 
                if(!empty($this->datos))
                foreach ($this->datos as $reg){ /*$i++; ?>
                <tr>
                  <td><?php echo $i;?></td>
                    <td><?php echo substr($reg["titulo"],0,100)."..."; ?></td>
                    <td><?php echo $reg["fecha_inicio"]." - ".$reg["fecha_final"] ;?></td>
                    <td></td>                   
                    <td><a class="btn btn-xs lis_ver " href="<?php echo JrAplicacion::getJrUrl(array('aulasvirtuales'))?>ver/?id=<?php echo $reg["aulaid"]; ?>"><i class="fa fa-eye"></i></a>                
                <a class="btn btn-xs lis_update" href="<?php echo JrAplicacion::getJrUrl(array("aulasvirtuales", "editar", "id=" . $reg["aulaid"]))?>"><i class="fa fa-edit"></i></a>
                <a class="btn-eliminar btn btn-xs lis_remove " href="javascript:;" data-id="<?php echo $reg["aulaid"]; ?>" ><i class="fa fa-trash-o"></i></a>
              </td>                        
            </tr>
            <?php }*/ ?>
                    </tbody>
        </table>
        </div>
        </div>
      </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $('.istooltip').tooltip();
//recargar combos de niveles 
        var leerniveles=function(data){
            try{
                var res = xajax__('', 'niveles', 'getxPadre', data);
                if(res){ return res; }
                return false;
            }catch(error){
                return false;
            }       
        }
        var addniveles=function(data,obj){
          var objini=obj.find('option:first').clone();
          obj.find('option').remove();
          obj.append(objini);
          if(data!==false){
            var html='';
            $.each(data,function(i,v){
              html+='<option value="'+v["idnivel"]+'">'+v["nombre"]+'</option>';
            });
            obj.append(html);
          }
          id=obj.attr('id');
          //if(id==='activity-item')  cargaraulaes();
        }

        $('#level-item').change(function(){
          var idnivel=$(this).val();
              var data={tipo:'U','idpadre':idnivel}
              var donde=$('#unit-item');
              if(idnivel!=='') addniveles(leerniveles(data),donde);
              else addniveles(false,donde);
              donde.trigger('change');
        });
        $('#unit-item').change(function(){
          var idunidad=$(this).val();
              var data={tipo:'L','idpadre':idunidad}
              var donde=$('#activity-item');
              if(idunidad!=='') addniveles(leerniveles(data),donde);
              else addniveles(false,donde);
              donde.trigger('change');
        });

        

        $('#activity-item').change(function(ev){
          tabledatos.ajax.reload();
        });
        $('#estado').change(function(ev){
          ev.preventDefault();
          tabledatos.ajax.reload();
        });
        $('#fecha_inicio').change(function(ev){
          ev.preventDefault();
         tabledatos.ajax.reload();
        });

        $('#fecha_final').change(function(ev){
           ev.preventDefault();
          tabledatos.ajax.reload();
        });

        $('.btnbuscar').click(function(ev){
          ev.preventDefault();
          tabledatos.ajax.reload();
        });

        $('.datetimepicker1').datetimepicker({
            defaultDate: "<?php echo date ( 'm/d/Y 00:00:00' , strtotime('-3 day',strtotime(date('Y-m-d')))); ?>",
            format: 'YYYY/MM/DD HH:MM:SS'           
        });
        $('.datetimepicker2').datetimepicker({
            defaultDate: "<?php echo date ( 'm/d/Y 00:00:00' , strtotime('+4 day',strtotime(date('Y-m-d')))); ?>",
            format: 'YYYY/MM/DD HH:MM:SS'           
        });

        var estados={'A':'<?php echo JrTexto::_("Active") ?>','I':'<?php echo JrTexto::_("Inactive") ?>','C':'<?php echo JrTexto::_("Cancelled") ?>','0':'-'}
        var draw=0;

        var tabledatos=$('.table').DataTable(
            { "searching": false,
              "processing": false,
              "serverSide": true,
              "columns" : [
                {'data': '#'},
                {'data': '<?php echo JrTexto::_("Title") ;?>'},
                {'data': '<?php echo JrTexto::_("Date") ;?>'},
                {'data': '<?php echo JrTexto::_("Video") ;?>'},
                {'data': '<?php echo JrTexto::_("State") ;?>'},
                {'data': '<?php echo JrTexto::_("Actions") ;?>'},
              ],
              "ajax":{
                url:_sysUrlBase_+'/aulasvirtuales/listado_doc/?json=true',
                type: "post",                
                data:function(d){
                    d.json=true
                    d.nivel=$('#level-item').val(),
                    d.unidad=$('#unit-item').val(),
                    d.actividad=$('#activity-item').val(),
                    d.estado=$('#estado').val(),
                    d.fecha_inicio=$('#fecha_inicio').val(),
                    d.fecha_final=$('#fecha_final').val(),
                    d.titulo=$('#titulo').val()
                    draw=d.draw;
                   // console.log(d);
                },
                "dataSrc":function(json){
                  var data=json.data;
                  console.log(data);
                  json.draw = draw;
                  json.recordsTotal = json.data.length;
                  json.recordsFiltered = json.data.length;
                  var datainfo = new Array();
                  for(var i=0;i< data.length; i++){
                    datainfo.push({
                      '#':(i+1),
                      '<?php echo JrTexto::_("Title") ;?>': data[i].titulo,
                      '<?php echo JrTexto::_("Date") ;?>'  : data[i].fecha_inicio+' <br> '+data[i].fecha_final,
                      '<?php echo JrTexto::_("Video") ;?>'  :  '<a href="'+_sysUrlStatic_ +'/media/aulasvirtuales/'+data[i].video+'" target="_blank">'+data[i].video+'</a>',
                      '<?php echo JrTexto::_("State") ;?>'  :  estados[data[i].estado],
                      '<?php echo JrTexto::_("Actions") ;?>'  :'<a class="btn btn-xs" href="'+_sysUrlBase_+'/aulavirtual/ver/?id='+data[i].aulaid+'"><i class="fa fa-drivers-license"></i></a><a class="btn btn-xs lis_update" href="'+_sysUrlBase_+'/aulasvirtuales/editar/?id='+data[i].aulaid+'"><i class="fa fa-edit"></i></a><a class="btn-eliminar btn btn-xs lis_remove " href="javascript:;" data-id="'+data[i].aulaid+'" ><i class="fa fa-trash-o"></i>'
                    })
                  }
                  return datainfo }, error: function(d){console.log(d)}
              }
              <?php echo $this->documento->getIdioma()!='EN'?(',"language": { "url": "'.$this->documento->getUrlStatic().'/libs/datatable1.10/idiomas/'.$this->documento->getIdioma().'.json"}'):''?>
            });

        $('.table').on('click','.btn-eliminar',function(){
             var id=$(this).attr('data-id');
             $.confirm({
              title: '<?php echo JrTexto::_('Confirm action');?>',
              content: '<?php echo JrTexto::_('It is sure to delete this record ?'); ?>',
              confirmButton: '<?php echo JrTexto::_('Accept');?>',
              cancelButton: '<?php echo JrTexto::_('Cancel');?>',
              confirmButtonClass: 'btn-success',
              cancelButtonClass: 'btn-danger',
              closeIcon: true,
              confirm: function(){             
                var res = xajax__('', 'aulasvirtuales', 'eliminar', parseInt(id));
                if(res) tabledatos.ajax.reload();
              }
            }); 
        });
    });
</script>