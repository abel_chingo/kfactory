<?php $idgui = uniqid(); 
//var_dump($this->datos);
$personal=$this->usuarioAct["nombre_full"]." <".$this->usuarioAct["email"].">";
$enviamail=$this->personal[0]["nombre"]." ";
$aula=@$this->datos;
$portada=$aula["portada"];
if(!empty($portada))
$portada='<div style="text-align: center;"><img  src="'.@str_replace('__xRUTABASEx__',$this->documento->getUrlBase(),$portada).'" width="60%" ></div>';
else
$portada='';
$enlace=$this->documento->getUrlBase()."/aulavirtual/?uri=".$aula["aulaid"].$idgui.$aula["aulaid"].date("Ymd");
?>
<form method="post" id="frm-<?php echo $idgui;?>" onsubmit="return false;">
<input type="hidden" name="idaula" id="idaula" value="<?php echo @$aula["aulaid"]; ?>">
<div class="row">
	<div class="panel panel-primary" style="margin-top: 1ex;">
		<div class="panel-heading" style="overflow: hidden;">
		<ol class="breadcrumb pull-left" style="margin: 0px; padding: 0px; background:rgba(187, 197, 184, 0);">
		  <li><a href="<?php echo $this->documento->getUrlBase() ?>/aulasvirtuales/" style="color:#fff"><?php echo ucfirst(JrTexto::_("Virtual classrooms")); ?></a></li>                  
		  <li class="active"  style="color:#ccc"><?php echo JrTexto::_('Invite participants'); ?></li>
		</ol>

		<div class="pull-right"><a href="#" class="btn btn-xs btn-default showdemo" data-videodemo="settings.mp4"><i class="fa fa-question-circle"></i> <?php echo JrTexto::_("Help"); ?></a></div>
    </div>
    <div class="panel-body">
      <div class="panel pnl100">
        <div class="panel-body">
           <div class="col-xs-12 col-sm-12 col-md-8">
           	<div class="form-group">
              <div class="input-group">
                <div class="input-group-addon"><b><?php echo ucfirst(JrTexto::_("Subject"))?> :</b></div>                
                <input type="text" name="asunto" id="asunto" value="<?php echo ucfirst(JrTexto::_("Invitation to participate in virtual class"))?>" class="form-control">
              </div>
            </div>          
            <div class="form-group">
              <div class="input-group">
                <div class="input-group-addon"><b><?php echo ucfirst(JrTexto::_("From"))?> :</b></div>                
                <input type="email" readonly="readonly" name="deemail" id="deemail" value="<?php echo $this->usuarioAct["email"]; ?>" class="form-control">                
              </div>
            </div>
            <div class="form-group">
              <div class="input-group">
                <div class="input-group-addon"><b><?php echo ucfirst(JrTexto::_("To"))?> :</b></div>
                <ul id="myTags" style="margin: 0px;">

                	
                </ul>               
                <!--input type="" name="" value="<?php echo $this->usuarioAct["email"]; ?>" class="form-control">
                <div class="input-group-addon"><b>@</b></div-->
              </div>
            </div>   
          </div>
          <div class="col-xs-12 col-sm-12 col-md-12">
	          <div class="form-group">
	              <label><b><?php echo ucfirst(JrTexto::_("Message"))?> :</b></label>
	              <textarea class="form-control" name="mensaje" id="mensaje">Ud. <b>!!_nameuser_!!</b> <br>Esta cordialemnete invitado a la siguiente clase virtual denominada:<br><h3><?php echo $aula["titulo"]; ?></h3><br><?php echo $portada; ?><br>
	              <p style="text-align: justify;" data-mce-style="text-align: justify;"><?php echo $aula["descripcion"]; ?></p><br> <p style="text-align: center;" data-mce-style="text-align: center;">Para poder ingresar solo accede haciendo <a href="<?php echo $enlace ?>">Click Aqui</a> o copia el siguiente enlace en tu navegador<br><?php echo $enlace; ?> </p></textarea>
	            </div>
            </div>
        </div>

      </div>
        <div class="col-xs-12 col-sm-12 col-md-12 text-center">          
          <button class="btn btn-primary btnsendemail"><i class="fa fa-paper-plane"></i>&nbsp;<?php echo ucfirst(JrTexto::_('send')); ?></button>          
        </div>      
    </div>  
  </div>
</div>
</form>


<script type="text/javascript">
	$(document).ready(function(){
		$('.btnsendemail').click(function(){
			tinyMCE.triggerSave(); 
			var mensaje=$('#mensaje').val();
			var Arradjuntos=[];
			var imgs=[];
			$(mensaje).find('img').each(function(){
				imgs.push($(this).attr('src'));
			});
			$paraemails=[];
			$('#myTags').find('li span.tagit-label').each(function(){
				var texto=$(this).text();
				var regex = /<([^>]*)>/;
				if(regex.exec(texto)!=null) email=regex.exec(texto)[1];				
				if(!/^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i.test(email))
				  return false;
				$paraemails.push(texto);
			});
			var formData = new FormData();
            formData.append("deemail", $('#deemail').val());
            formData.append("msje", mensaje);
            formData.append("asunto", $('#asunto').val());
            formData.append("images", JSON.stringify(imgs));
            formData.append("paraemail", JSON.stringify($paraemails));
            $.ajax({
	            url: _sysUrlBase_+'/sendemail/enviarphpmailer',
	            type: "POST",
	            data:  formData,
	            contentType: false,
	            dataType :'json',
	            cache: false,
	            processData:false,
	            beforeSend: function(XMLHttpRequest){
	                $('#procesando').show('fast');
	                $('.btnsendemail').attr('disabled','disabled');
	            },
	            success: function(data)
	            {
	                if(data.code==='ok'){
	                    console.log('mensaje enviado a todos los invitados');
	                }else{
	                    mostrar_notificacion('<?php echo JrTexto::_('Attention'); ?>', data.msj, 'warning');
	                }
	                $('#procesando').hide('fast');
	                $('.btnsendemail').removeAttr('disabled');
	                return false;
	            },
	            error: function(xhr,status,error){
	                mostrar_notificacion('<?php echo JrTexto::_('Attention'); ?>', status, 'warning');
	                $('#procesando').hide('fast');
	                return false;
	            },
	        }).always(function() {
	            $('.btnsendemail').removeAttr('disabled');
	        });
		})
		var mostrarEditorMCE  = function(obj,showtoolstiny){
		  var showtools=showtoolstiny||'';
		  tinymce.init({
		    relative_urls : false,
		    remove_script_host: false,
		    convert_newlines_to_brs : true,
		    menubar: false,
		    statusbar: false,
		    verify_html : false,
		    content_css : _sysUrlBase_+'/static/tema/css/bootstrap.min.css',
		    selector: obj,
		    height: 200,
		    paste_auto_cleanup_on_paste : true,
		    paste_preprocess : function(pl, o) {
		        var html='<div>'+o.content+'</div>';
		        var txt =$(html).text(); 
		        o.content = txt;
		    },paste_postprocess : function(pl, o) {       
		        o.node.innerHTML = o.node.innerHTML;
		    },
		    plugins:[showtools+"  link image textcolor paste" ],  //chingosave chingoinput chingoimage chingoaudio chingovideo styleselect
		    toolbar: ' undo redo |  bold italic underline | alignleft aligncenter alignright alignjustify | numlist |  forecolor backcolor |  '+showtools // chingosave chingoinput chingoimage chingoaudio chingovideo 
		  });
		};
		mostrarEditorMCE('#mensaje');
		$('#myTags').tagit({ 
			beforeTagAdded: function(event, ui){
			var texto=ui.tag[0].innerText;
			var email=texto;
			var regex = /<([^>]*)>/;
			if(regex.exec(texto)!=null) email=regex.exec(texto)[1];
			else email=email.substr(0,email.length-1);
			if(!/^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i.test(email))
			  return false;
		    },fieldName: "toemails",placeholderText:"example@tuempresa.com"});
		<?php 
		if(!empty($this->alumnos))
			foreach ($this->alumnos as $alumno){
			echo '$("#myTags").tagit("createTag", "'.$alumno["ape_paterno"].' '.$alumno["ape_materno"].' '.$alumno["nombre"]."<".$alumno["email"].'>");';
			}
		?>
});
</script>