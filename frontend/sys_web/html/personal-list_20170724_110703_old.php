<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
?><style type="text/css">
  .panel-body{
    border: 1px solid rgba(90, 137, 248, 0.41);
    padding: 10px;
  }
  .title_left, .small{
    height: auto;
    color: #fff;
  }
  .form-group{
    margin-bottom: 0px; 
  }
  .input-group {
    margin-top: 0px;
  }
  select.select-ctrl, .form-control , .input-group-addon{   
    border: 1px solid #4683af;
    margin-bottom: 1ex;
  }
</style>
<div class="form-view" id="ventana_<?php echo $idgui; ?>" >
  <div class="page-title">
    <div class="title_left">
      <ol class="breadcrumb">
        <li><a href="<?php echo JrAplicacion::getJrUrl(array("academico"));?>"><?php echo JrTexto::_("Academic");?></a></li>
        <li><a href="<?php echo JrAplicacion::getJrUrl(array('personal'));?>"><?php echo JrTexto::_('Personal'); ?></a></li>
        <li class="active"><?php echo JrTexto::_('list')?></li>
      </ol>
    </div>
  </div>
  <div class="clearfix"></div>
  <div class="div_linea"></div>
<div class="row">
  <div class="col-md-12">
    <div class="panel" >      
      <div class="panel-body">      
         <div class="row">
            <div class="col-md-12">
                                                                                                                                            <div class="col-xs-6 col-sm-4 col-md-3">
                          <div class="form-group">    
                                <div class='input-group date datetimepicker1 border1' >
                                  <span class="input-group-addon btn"><span class="fa fa-calendar"></span> <?php echo  ucfirst(JrTexto::_("fechanac"))?> </span>
                                  <input type='text' class="form-control border0" name="datefechanac" id="datefechanac" />           
                                </div>
                            </div>
                            </div>
                                              
                <div class="col-xs-6 col-sm-4 col-md-3 select-ctrl-wrapper select-azul">
                        <select id="fkcbsexo" name="fkcbsexo" class="form-control select-ctrl" >
                           <option value=""><?php echo JrTexto::_('Seleccione'); ?></option>
                          <?php 
                          if(!empty($this->fksexo))
                            foreach ($this->fksexo as $fksexo) { ?><option value="<?php echo $fksexo["codigo"]?>" <?php echo $fksexo["codigo"]==@$frm["sexo"]?"selected":""; ?> ><?php echo $fksexo["nombre"] ?></option><?php } ?>                          </select>
                    </div>
                          </div>                          
                                            
                <div class="col-xs-6 col-sm-4 col-md-3 select-ctrl-wrapper select-azul">
                        <select id="fkcbestado_civil" name="fkcbestado_civil" class="form-control select-ctrl" >
                           <option value=""><?php echo JrTexto::_('Seleccione'); ?></option>
                          <?php 
                          if(!empty($this->fkestado_civil))
                            foreach ($this->fkestado_civil as $fkestado_civil) { ?><option value="<?php echo $fkestado_civil["codigo"]?>" <?php echo $fkestado_civil["codigo"]==@$frm["estado_civil"]?"selected":""; ?> ><?php echo $fkestado_civil["nombre"] ?></option><?php } ?>                          </select>
                    </div>
                          </div>                          
                                            
                <div class="col-xs-6 col-sm-4 col-md-3 select-ctrl-wrapper select-azul">
                        <select id="fkcbubigeo" name="fkcbubigeo" class="form-control select-ctrl" >
                           <option value=""><?php echo JrTexto::_('Seleccione'); ?></option>
                          <?php 
                          if(!empty($this->fkubigeo))
                            foreach ($this->fkubigeo as $fkubigeo) { ?><option value="<?php echo $fkubigeo["id_ubigeo"]?>" <?php echo $fkubigeo["id_ubigeo"]==@$frm["ubigeo"]?"selected":""; ?> ><?php echo $fkubigeo["ciudad"] ?></option><?php } ?>                          </select>
                    </div>
                          </div>                          
                                                                                                                                                
                <div class="col-xs-6 col-sm-4 col-md-3 select-ctrl-wrapper select-azul">
                        <select id="fkcbidugel" name="fkcbidugel" class="form-control select-ctrl" >
                           <option value=""><?php echo JrTexto::_('Seleccione'); ?></option>
                          <?php 
                          if(!empty($this->fkidugel))
                            foreach ($this->fkidugel as $fkidugel) { ?><option value="<?php echo $fkidugel["idugel"]?>" <?php echo $fkidugel["idugel"]==@$frm["idugel"]?"selected":""; ?> ><?php echo $fkidugel["descripcion"] ?></option><?php } ?>                          </select>
                    </div>
                          </div>                          
                                                                                          <div class="col-xs-6 col-sm-4 col-md-3">
                          <div class="form-group">    
                                <div class='input-group date datetimepicker1 border1' >
                                  <span class="input-group-addon btn"><span class="fa fa-calendar"></span> <?php echo  ucfirst(JrTexto::_("regfecha"))?> </span>
                                  <input type='text' class="form-control border0" name="dateregfecha" id="dateregfecha" />           
                                </div>
                            </div>
                            </div>
                                                                                                          
                <div class="col-xs-6 col-sm-4 col-md-3 select-ctrl-wrapper select-azul">
                        <select id="fkcbrol" name="fkcbrol" class="form-control select-ctrl" >
                           <option value=""><?php echo JrTexto::_('Seleccione'); ?></option>
                          <?php 
                          if(!empty($this->fkrol))
                            foreach ($this->fkrol as $fkrol) { ?><option value="<?php echo $fkrol["idrol"]?>" <?php echo $fkrol["idrol"]==@$frm["rol"]?"selected":""; ?> ><?php echo $fkrol["rol"] ?></option><?php } ?>                          </select>
                    </div>
                          </div>                          
                                                                                          
                      <div class="col-xs-6 col-sm-4 col-md-3 select-ctrl-wrapper select-azul" >
                        <select name="cbestado" id="cbestado" class="form-control select-ctrl">
                            <option value=""><?php echo ucfirst(JrTexto::_("All state"))?></option>
                            <option value="1"><?php echo ucfirst(JrTexto::_("Active"))?></option>
                            <option value="0"><?php echo ucfirst(JrTexto::_("Inactive"))?></option>                              
                        </select>
                      </div>
                                                                        
                      <div class="col-xs-6 col-sm-4 col-md-3 select-ctrl-wrapper select-azul" >
                        <select name="cbsituacion" id="cbsituacion" class="form-control select-ctrl">
                            <option value=""><?php echo ucfirst(JrTexto::_("All state"))?></option>
                            <option value="1"><?php echo ucfirst(JrTexto::_("Active"))?></option>
                            <option value="0"><?php echo ucfirst(JrTexto::_("Inactive"))?></option>                              
                        </select>
                      </div>
                                                            
              <div class="col-xs-6 col-sm-6 col-md-6">
                <div class="form-group">
                <div class="input-group">
                  <input type="text" name="texto" id="texto" class="form-control border0" placeholder="<?php echo  ucfirst(JrTexto::_("text to search"))?>">
                  <span class="input-group-addon btn btnbuscar"><?php echo  ucfirst(JrTexto::_("Search"))?> <i class="fa fa-search"></i>
                     </span>  
                                     </div>
                </div>
              </div>
              <div class="col-xs-6 col-sm-3 col-md-3 text-center">
               <a class="btn btn-success btnvermodal" data-modal="si" href="<?php echo JrAplicacion::getJrUrl(array("Personal", "agregar"));?>" data-titulo="<?php echo JrTexto::_("Personal").' - '.JrTexto::_("add"); ?>">
          <i class="fa fa-plus"></i> <?php echo JrTexto::_('add')?></a>
              </div>
            </div>
          </div>
      </div>
    </div>
  </div> 
	<div class="col-md-12 col-sm-12 col-xs-12">
	   <div class="panel">
         
         <div class="panel-body">
            <table class="table table-striped table-responsive">
              <thead>
                <tr class="headings">
                  <th>#</th>
                  <th><?php echo JrTexto::_("Ape paterno") ;?></th>
                    <th><?php echo JrTexto::_("Ape materno") ;?></th>
                    <th><?php echo JrTexto::_("Nombre") ;?></th>
                    <th><?php echo JrTexto::_("Fechanac") ;?></th>
                    <th><?php echo JrTexto::_("Ral")."(".JrTexto::_("Nombre").")"; ?></th>
                    <th><?php echo JrTexto::_("Ral")."(".JrTexto::_("Nombre").")"; ?></th>
                    <th><?php echo JrTexto::_("Eo")."(".JrTexto::_("Ciudad").")"; ?></th>
                    <th><?php echo JrTexto::_("Urbanizacion") ;?></th>
                    <th><?php echo JrTexto::_("Direccion") ;?></th>
                    <th><?php echo JrTexto::_("Telefono") ;?></th>
                    <th><?php echo JrTexto::_("Celular") ;?></th>
                    <th><?php echo JrTexto::_("Email") ;?></th>
                    <th><?php echo JrTexto::_("")."(".JrTexto::_("Descripcion").")"; ?></th>
                    <th><?php echo JrTexto::_("Regusuario") ;?></th>
                    <th><?php echo JrTexto::_("Regfecha") ;?></th>
                    <th><?php echo JrTexto::_("Usuario") ;?></th>
                    <th><?php echo JrTexto::_("Clave") ;?></th>
                    <th><?php echo JrTexto::_("Token") ;?></th>
                    <th><?php echo JrTexto::_("S")."(".JrTexto::_("Rol").")"; ?></th>
                    <th><?php echo JrTexto::_("Foto") ;?></th>
                    <th><?php echo JrTexto::_("Estado") ;?></th>
                    <th><?php echo JrTexto::_("Situacion") ;?></th>
                    <th><?php echo JrTexto::_("Idioma") ;?></th>
                    <th class="sorting_disabled"><span class="nobr"><?php echo JrTexto::_('Actions');?></span></th>
                </tr>
              </thead>
              <tbody>
              <?php $i=0; 
                if(!empty($this->datos))
                foreach ($this->datos as $reg){ $i++; ?>
                <tr>
                  <td><?php echo $i;?></td>
                  <td><?php echo $reg["ape_paterno"] ;?></td>
                    <td><?php echo $reg["ape_materno"] ;?></td>
                    <td><?php echo $reg["nombre"] ;?></td>
                    <td><?php echo $reg["fechanac"] ;?></td>
                    <td><?php echo !empty($reg["_nombre"])?$reg["_nombre"]:null; ?></td>
                    <td><?php echo !empty($reg["_nombre"])?$reg["_nombre"]:null; ?></td>
                    <td><?php echo !empty($reg["_ciudad"])?$reg["_ciudad"]:null; ?></td>
                    <td><?php echo $reg["urbanizacion"] ;?></td>
                    <td><?php echo $reg["direccion"] ;?></td>
                    <td><?php echo $reg["telefono"] ;?></td>
                    <td><?php echo $reg["celular"] ;?></td>
                    <td><?php echo $reg["email"] ;?></td>
                    <td><?php echo !empty($reg["_descripcion"])?$reg["_descripcion"]:null; ?></td>
                    <td class="text-right"><?php echo $reg["regusuario"] ;?></td>
                    <td><?php echo $reg["regfecha"] ;?></td>
                    <td><?php echo $reg["usuario"] ;?></td>
                    <td><?php echo $reg["clave"] ;?></td>
                    <td><?php echo $reg["token"] ;?></td>
                    <td><?php echo !empty($reg["_rol"])?$reg["_rol"]:null; ?></td>
                    <td class="text-center"><img src="<?php echo $this->documento->getUrlBase().$reg["foto"]; ?>" class="img-thumbnail" style="max-height:70px; max-width:50px;"></td>
                    <td class="text-center"><a href="javascript:;"  class="btn-chkoption" campo="estado"  data-id="<?php echo $reg["dni"]; ?>"> <i class="fa fa<?php echo !empty($reg["estado"])?"-check":""; ?>-circle-o fa-lg"></i> <?php echo $reg["estado"]=="1"?JrTexto::_("Active"):JrTexto::_("Inactive"); ?></a> </td>
                    <td class="text-center"><a href="javascript:;"  class="btn-chkoption" campo="situacion"  data-id="<?php echo $reg["dni"]; ?>"> <i class="fa fa<?php echo !empty($reg["situacion"])?"-check":""; ?>-circle-o fa-lg"></i> <?php echo $reg["situacion"]=="1"?JrTexto::_("Active"):JrTexto::_("Inactive"); ?></a> </td>
                    <td><?php echo $reg["idioma"] ;?></td>
                    <td><a class="btn btn-xs lis_ver " href="<?php echo JrAplicacion::getJrUrl(array('personal'))?>ver/?id=<?php echo $reg["dni"]; ?>"><i class="fa fa-eye"></i></a>                
                <a class="btn btn-xs lis_update btnvermodal" data-modal='si' href="<?php echo JrAplicacion::getJrUrl(array("personal", "editar", "id=" . $reg["dni"]))?>"  data-titulo="<?php echo JrTexto::_("Personal").' - '.JrTexto::_("edit"); ?>"><i class="fa fa-edit"></i></a>
                <a class="btn-eliminar btn btn-xs lis_remove " href="javascript:;" data-id="<?php echo $reg["dni"]; ?>" ><i class="fa fa-trash-o"></i></a>
              </td>                        
            </tr>
            <?php } ?>
                    </tbody>
        </table>
        </div>
        </div>
      </div>
    </div>
</div>


<script type="text/javascript">
var tabledatos596fd738835bf='';
function refreshdatos596fd738835bf(){
    tabledatos596fd738835bf.ajax.reload();
}
$(document).ready(function(){  
  var estados596fd738835bf={'1':'<?php echo JrTexto::_("Active") ?>','0':'<?php echo JrTexto::_("Inactive") ?>','C':'<?php echo JrTexto::_("Cancelled") ?>'}
  var tituloedit596fd738835bf='<?php echo ucfirst(JrTexto::_("personal"))." - ".JrTexto::_("edit"); ?>';
  var draw596fd738835bf=0;

  
  $('#datefechanac').change(function(ev){
    refreshdatos596fd738835bf();
  });
  $('#fkcbsexo').change(function(ev){
    refreshdatos596fd738835bf();
  });
  $('#fkcksexo').change(function(ev){
    refreshdatos596fd738835bf();
  });
  $('#fkcbestado_civil').change(function(ev){
    refreshdatos596fd738835bf();
  });
  $('#fkckestado_civil').change(function(ev){
    refreshdatos596fd738835bf();
  });
  $('#fkcbubigeo').change(function(ev){
    refreshdatos596fd738835bf();
  });
  $('#fkckubigeo').change(function(ev){
    refreshdatos596fd738835bf();
  });
  $('#fkcbidugel').change(function(ev){
    refreshdatos596fd738835bf();
  });
  $('#fkckidugel').change(function(ev){
    refreshdatos596fd738835bf();
  });
  $('#dateregfecha').change(function(ev){
    refreshdatos596fd738835bf();
  });
  $('#fkcbrol').change(function(ev){
    refreshdatos596fd738835bf();
  });
  $('#fkckrol').change(function(ev){
    refreshdatos596fd738835bf();
  });
  $('#cbestado').change(function(ev){
    refreshdatos596fd738835bf();
  });
  $('#cbsituacion').change(function(ev){
    refreshdatos596fd738835bf();
  });
  $('.btnbuscar').click(function(ev){
    refreshdatos596fd738835bf();
  });
  tabledatos596fd738835bf=$('#ventana_<?php echo $idgui; ?> .table').DataTable(
    { "searching": false,
      "processing": false,
      //"serverSide": true,
      "columns" : [
        {'data': '#'},
        
        {'data': '<?php echo JrTexto::_("Ape_paterno") ;?>'},
            {'data': '<?php echo JrTexto::_("Ape_materno") ;?>'},
            {'data': '<?php echo JrTexto::_("Nombre") ;?>'},
            {'data': '<?php echo JrTexto::_("Fechanac") ;?>'},
            {'data': '<?php echo JrTexto::_("Nombre") ;?>'},
            {'data': '<?php echo JrTexto::_("Nombre") ;?>'},
            {'data': '<?php echo JrTexto::_("Ciudad") ;?>'},
            {'data': '<?php echo JrTexto::_("Urbanizacion") ;?>'},
            {'data': '<?php echo JrTexto::_("Direccion") ;?>'},
            {'data': '<?php echo JrTexto::_("Telefono") ;?>'},
            {'data': '<?php echo JrTexto::_("Celular") ;?>'},
            {'data': '<?php echo JrTexto::_("Email") ;?>'},
            {'data': '<?php echo JrTexto::_("Descripcion") ;?>'},
            {'data': '<?php echo JrTexto::_("Regusuario") ;?>'},
            {'data': '<?php echo JrTexto::_("Regfecha") ;?>'},
            {'data': '<?php echo JrTexto::_("Usuario") ;?>'},
            {'data': '<?php echo JrTexto::_("Clave") ;?>'},
            {'data': '<?php echo JrTexto::_("Token") ;?>'},
            {'data': '<?php echo JrTexto::_("Rol") ;?>'},
            {'data': '<?php echo JrTexto::_("Foto") ;?>'},
            {'data': '<?php echo JrTexto::_("Estado") ;?>'},
            {'data': '<?php echo JrTexto::_("Situacion") ;?>'},
            {'data': '<?php echo JrTexto::_("Idioma") ;?>'},
            
        {'data': '<?php echo JrTexto::_("Actions") ;?>'},
      ],
      "ajax":{
        url:_sysUrlBase_+'/personal/buscarjson/?json=true',
        type: "post",                
        data:function(d){
            d.json=true                   
             d.fechanac=$('#datefechanac').val(),
             d.sexo=$('#fkcbsexo').val(),
             d.sexo=$('#fkcksexo').val(),
             d.estado_civil=$('#fkcbestado_civil').val(),
             d.estado_civil=$('#fkckestado_civil').val(),
             d.ubigeo=$('#fkcbubigeo').val(),
             d.ubigeo=$('#fkckubigeo').val(),
             d.idugel=$('#fkcbidugel').val(),
             d.idugel=$('#fkckidugel').val(),
             d.regfecha=$('#dateregfecha').val(),
             d.rol=$('#fkcbrol').val(),
             d.rol=$('#fkckrol').val(),
             d.estado=$('#cbestado').val(),
             d.situacion=$('#cbsituacion').val(),
             //d.texto=$('#texto').val(),
                        
            draw596fd738835bf=d.draw;
           // console.log(d);
        },
        "dataSrc":function(json){
          var data=json.data;             
          json.draw = draw596fd738835bf;
          json.recordsTotal = json.data.length;
          json.recordsFiltered = json.data.length;
          var datainfo = new Array();
          for(var i=0;i< data.length; i++){                   
            datainfo.push({
              '#':(i+1),
              '<?php echo JrTexto::_("Ape_paterno") ;?>': data[i].ape_paterno,
              '<?php echo JrTexto::_("Ape_materno") ;?>': data[i].ape_materno,
              '<?php echo JrTexto::_("Nombre") ;?>': data[i].nombre,
              '<?php echo JrTexto::_("Fechanac") ;?>': data[i].fechanac,
              '<?php echo JrTexto::_("nombre") ;?>': data[i].nombre,
                    '<?php echo JrTexto::_("nombre") ;?>': data[i].nombre,
                    '<?php echo JrTexto::_("ciudad") ;?>': data[i].ciudad,
                    '<?php echo JrTexto::_("Urbanizacion") ;?>': data[i].urbanizacion,
              '<?php echo JrTexto::_("Direccion") ;?>': data[i].direccion,
              '<?php echo JrTexto::_("Telefono") ;?>': data[i].telefono,
              '<?php echo JrTexto::_("Celular") ;?>': data[i].celular,
              '<?php echo JrTexto::_("Email") ;?>': data[i].email,
              '<?php echo JrTexto::_("descripcion") ;?>': data[i].descripcion,
                    '<?php echo JrTexto::_("Regusuario") ;?>': data[i].regusuario,
                '<?php echo JrTexto::_("Regfecha") ;?>': data[i].regfecha,
              '<?php echo JrTexto::_("Usuario") ;?>': data[i].usuario,
              '<?php echo JrTexto::_("Clave") ;?>': data[i].clave,
              '<?php echo JrTexto::_("Token") ;?>': data[i].token,
              '<?php echo JrTexto::_("rol") ;?>': data[i].rol,
                    '<?php echo JrTexto::_("Foto") ;?>': '<img src="'+data[i].foto+' class="img-thumbnail" style="max-height:70px; max-width:50px;">',
                '<?php echo JrTexto::_("Estado") ;?>': '<a href="javascript:;"  class="btn-chkoption" campo="estado"  data-id="'+data[i].dni+'"> <i class="fa fa'+(data[i].estado=='1'?'-check':'')+'-circle-o fa-lg"></i> '+estados596fd738835bf[data[i].estado]+'</a>',
              '<?php echo JrTexto::_("Situacion") ;?>': '<a href="javascript:;"  class="btn-chkoption" campo="situacion"  data-id="'+data[i].dni+'"> <i class="fa fa'+(data[i].situacion=='1'?'-check':'')+'-circle-o fa-lg"></i> '+estados596fd738835bf[data[i].situacion]+'</a>',
              '<?php echo JrTexto::_("Idioma") ;?>': data[i].idioma,
              
              '<?php echo JrTexto::_("Actions") ;?>'  :'<a class="btn btn-xs btnvermodal" data-modal="si" href="'+_sysUrlBase_+'/personal/editar/?id='+data[i].dni+'" data-titulo="'+tituloedit596fd738835bf+'"><i class="fa fa-edit"></i></a><a class="btn-eliminar btn btn-xs" href="javascript:;" data-id="'+data[i].dni+'" ><i class="fa fa-trash-o"></i>'
            });
          }
          return datainfo }, error: function(d){console.log(d)}
      }
      <?php echo $this->documento->getIdioma()!='EN'?(',"language": { "url": "'.$this->documento->getUrlStatic().'/libs/datatable1.10/idiomas/'.$this->documento->getIdioma().'.json"}'):''?>
    });


  $('#ventana_<?php echo $idgui; ?>').on('click','.btn-chkoption',function(){     
      var id=$(this).attr('data-id');
      var campo=$(this).attr('campo');
      var data=0;
      if($("i",this).hasClass('fa-circle-o')) data=1;
      $.confirm({
        title: '<?php echo JrTexto::_('Confirm action');?>',
        content: '<?php echo JrTexto::_('It is sure to change the status of this record ?'); ?>',
        confirmButton: '<?php echo JrTexto::_('Accept');?>',
        cancelButton: '<?php echo JrTexto::_('Cancel');?>',
        confirmButtonClass: 'btn-success',
        cancelButtonClass: 'btn-danger',
        closeIcon: true,
        confirm: function(){
          var res = xajax__('', 'personal', 'setCampo', id,campo,data);
          if(res) tabledatos596fd738835bf.ajax.reload();
        }
      });
  });

  $('#ventana_<?php echo $idgui; ?>').on('click','.btnvermodal',function(e){
    e.preventDefault();
    e.stopPropagation();
    var enmodal=$(this).attr('data-modal')||'no';
    var fcall=$(this).attr('data-fcall')||'refreshdatos596fd738835bf';
    var url=$(this).attr('href')
    if(url.indexOf('?')!=-1) url+='&fcall='+fcall;
    else url+='?fcall='+fcall;
    var ventana=$(this).data('ventana')||'Personal';
    var claseid=ventana+'_<?php echo $idgui; ?>';
    var titulo=$(this).attr('data-titulo')||'';
    titulo=titulo.toString().replace('<br>',' ');     
    if(enmodal=='no'){
      return redir(url);          
    }
    url+='&plt=modal';
    openModal('lg',titulo,url,ventana,claseid); 
  });
  
 

  $('#ventana_<?php echo $idgui; ?> .table').on('click','.btn-eliminar',function(){
     var id=$(this).attr('data-id');
     $.confirm({
      title: '<?php echo JrTexto::_('Confirm action');?>',
      content: '<?php echo JrTexto::_('It is sure to delete this record ?'); ?>',
      confirmButton: '<?php echo JrTexto::_('Accept');?>',
      cancelButton: '<?php echo JrTexto::_('Cancel');?>',
      confirmButtonClass: 'btn-success',
      cancelButtonClass: 'btn-danger',
      closeIcon: true,
      confirm: function(){             
        var res = xajax__('', 'personal', 'eliminar', id);
        if(res) tabledatos596fd738835bf.ajax.reload();
      }
    }); 
  });

});
</script>