<?php defined("RUTA_BASE") or die(); ?><div class="form-view" >
  <div class="page-title">
    <div class="title_left">
      <ol class="breadcrumb">
        <li><a href="<?php echo JrAplicacion::getJrUrl(array("administrador"));?>"><?php echo JrTexto::_("Dashboard");?></a></li>
        <li><a href="<?php echo JrAplicacion::getJrUrl(array('local'));?>"><?php echo JrTexto::_('Local'); ?></a></li>
        <li class="active"><?php echo JrTexto::_('list')?></li>
      </ol>
    </div>
  </div>
  <div class="clearfix"></div>
  <div class="div_linea"></div>
<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
	   <div class="x_panel">
        <div class="x_title">
          <a class="btn btn-success btn-xs" href="<?php echo JrAplicacion::getJrUrl(array("Local", "agregar"));?>">
          <i class="fa fa-plus"></i> <?php echo JrTexto::_('add')?></a>
          <div class="nav navbar-right"></div>
          <div class="clearfix"></div>
        </div>

        <div class="div_linea"></div>
         <div class="x_content">
         <form id="filtros">
         <?php
         echo JrTexto::_('Tipo');
         $_tipo=$this->tipo1;
         //echo "<br>".$_tipo."asasasa";
         ?>         
          <select id="txtTipo1" name="txtTipo1" >
          <option value="T" <?php if ($_tipo=='T') echo "selected" ?>>Centro de Trabajo</option>
          <option value="C" <?php if ($_tipo=='C') echo "selected" ?>>Centro de Capacitación</option>         
          </select>

          <?php echo JrTexto::_('Ugel');?>         
          <select id="txtUgel" name="txtUgel" onchange="this.form.submit()">
          <option value="">Todos</option>
          <?php foreach ($this->ugeles as $ugel) { ?>
            <option value="<?php echo $ugel["idugel"]; ?>" <?php echo $this->idugel===$ugel["idugel"]?'selected="selected"':''?>> <?php echo $ugel["descripcion"]?> </option>
          <?php } ?>             
          </select>
          </form>
         </div>


        <div class="div_linea"></div>
         <div class="x_content">
            <table class="table table-striped table-responsive">
              <thead>
                <tr class="headings">
                  <th>#</th>
                  <th><?php echo JrTexto::_("Nombre") ;?></th>                    
                    <th><?php echo JrTexto::_("Tipo") ;?></th>
                    <th><?php echo JrTexto::_("Vacantes") ;?></th>
                    <th><?php echo JrTexto::_("Idugel") ;?></th>
                    <th class="sorting_disabled"><span class="nobr"><?php echo JrTexto::_('Actions');?></span></th>
                </tr>
              </thead>
              <tbody>
              <?php $i=0; 
                if(!empty($this->datos))
                foreach ($this->datos as $reg){ $i++; ?>
                <tr>
                  <td><?php echo $i;?></td>
                  <td><?php echo substr($reg["nombre"],0,100)."..."; ?></td>                    
                    <td>
                    <?php
                    if ($reg["tipo"]=='T'){
                      echo "Centro de Trabajo";
                    }else{
                      echo "Centro de Capacitación";
                    }
                    ?></td>

                    <td><?php echo $reg["vacantes"] ;?></td>
                    <td><?php echo $reg["ugel"] ;?></td>
                    <td><a class="btn btn-xs lis_ver " href="<?php echo JrAplicacion::getJrUrl(array('local'))?>ver/?id=<?php echo $reg["idlocal"]; ?>"><i class="fa fa-eye"></i></a>                
                <a class="btn btn-xs lis_update" href="<?php echo JrAplicacion::getJrUrl(array("local", "editar", "id=" . $reg["idlocal"]))?>"><i class="fa fa-edit"></i></a>
                <a class="btn-eliminar btn btn-xs lis_remove " href="javascript:;" data-id="<?php echo $reg["idlocal"]; ?>" ><i class="fa fa-trash-o"></i></a>
              </td>                        
            </tr>
            <?php } ?>
                    </tbody>
        </table>
        </div>
        </div>
      </div>
    </div>
</div>


<script type="text/javascript">
$(document).ready(function () {
  $('.btn-eliminar').bind({   
    click: function() {
       var id=$(this).attr('data-id');
       $.confirm({
        title: '<?php echo JrTexto::_('Confirm action');?>',
        content: '<?php echo JrTexto::_('It is sure to delete this record ?'); ?>',
        confirmButton: '<?php echo JrTexto::_('Accept');?>',
        cancelButton: '<?php echo JrTexto::_('Cancel');?>',
        confirmButtonClass: 'btn-success',
        cancelButtonClass: 'btn-danger',
        closeIcon: true,
        confirm: function(){
          var res = xajax__('', 'local', 'eliminar', id);
          if(res){
            return redir('<?php echo JrAplicacion::getJrUrl(array('local'))?>');
          }
        }
      });     
    }
  });
  
  $('.btn-chkoption').bind({
    click: function() {     
      var id=$(this).attr('data-id');
      var campo=$(this).attr('campo');
      var data=0;
      if($("i",this).hasClass('fa-circle-o')) data=1;
      $.confirm({
        title: '<?php echo JrTexto::_('Confirm action');?>',
        content: '<?php echo JrTexto::_('It is sure to change the status of this record ?'); ?>',
        confirmButton: '<?php echo JrTexto::_('Accept');?>',
        cancelButton: '<?php echo JrTexto::_('Cancel');?>',
        confirmButtonClass: 'btn-success',
        cancelButtonClass: 'btn-danger',
        closeIcon: true,
        confirm: function(){
           var res = xajax__('', 'local', 'setCampo', id,campo,data);
           if(res) {
            return redir('<?php echo JrAplicacion::getJrUrl(array('local'))?>');
           }
        }
      });
    }
  });
  
  $('.table').DataTable( {
    "language": {
            "url": "<?php echo $this->documento->getUrlStatic().'/libs/datatable1.10/idiomas/'.$this->documento->getIdioma(); ?>.json"
    }
  });
});


$('#txtTipo1').change(function(){
   
       $( "#filtros" ).submit();
   
  });


$('#txtUgel').change(function(){
   
       $( "#filtros" ).submit();
   
  });



</script>