<?php defined("RUTA_BASE") or die(); ?><div class="form-view" >
  <div class="page-title">
    <div class="title_left">
      <ol class="breadcrumb">
        <li><a href="<?php echo JrAplicacion::getJrUrl(array("administrador"));?>"><?php echo JrTexto::_("Dashboard");?></a></li>
        <li><a href="<?php echo JrAplicacion::getJrUrl(array('alumno'));?>"><?php echo JrTexto::_('Alumno'); ?></a></li>
        <li class="active"><?php echo JrTexto::_('list')?></li>
      </ol>
    </div>
  </div>
  <div class="clearfix"></div>
  <div class="div_linea"></div>
<div class="row">
	<div class="col-md-12 col-sm-12 col-xs-12">
	   <div class="x_panel">
        <div class="x_title">
          <a class="btn btn-success btn-xs" href="<?php echo JrAplicacion::getJrUrl(array("Alumno", "agregar"));?>">
          <i class="fa fa-plus"></i> <?php echo JrTexto::_('add')?></a>
          <div class="nav navbar-right"></div>
          <div class="clearfix"></div>
        </div>
        <div class="div_linea"></div>
         <div class="x_content">
            <table class="table table-striped table-responsive">
              <thead>
                <tr class="headings">
                  <th>#</th>
                  <th><?php echo JrTexto::_("Ape paterno") ;?></th>
                    <th><?php echo JrTexto::_("Ape materno") ;?></th>
                    <th><?php echo JrTexto::_("Nombre") ;?></th>
                    <th><?php echo JrTexto::_("Fechanac") ;?></th>
                    <th><?php echo JrTexto::_("Sexo") ;?></th>
                    <th><?php echo JrTexto::_("Estado civil") ;?></th>
                    <th><?php echo JrTexto::_("Ubigeo") ;?></th>
                    <th><?php echo JrTexto::_("Urbanizacion") ;?></th>
                    <th><?php echo JrTexto::_("Direccion") ;?></th>
                    <th><?php echo JrTexto::_("Telefono") ;?></th>
                    <th><?php echo JrTexto::_("Celular") ;?></th>
                    <th><?php echo JrTexto::_("Email") ;?></th>
                    <th><?php echo JrTexto::_("Idugel") ;?></th>
                    <th><?php echo JrTexto::_("Regusuario") ;?></th>
                    <th><?php echo JrTexto::_("Regfecha") ;?></th>
                    <th><?php echo JrTexto::_("Usuario") ;?></th>
                    <th><?php echo JrTexto::_("Clave") ;?></th>
                    <th><?php echo JrTexto::_("Token") ;?></th>
                    <th><?php echo JrTexto::_("Foto") ;?></th>
                    <th><?php echo JrTexto::_("Estado") ;?></th>
                    <th><?php echo JrTexto::_("Situacion") ;?></th>
                    <th class="sorting_disabled"><span class="nobr"><?php echo JrTexto::_('Actions');?></span></th>
                </tr>
              </thead>
              <tbody>
              <?php $i=0; 
                if(!empty($this->datos))
                foreach ($this->datos as $reg){ $i++; ?>
                <tr>
                  <td><?php echo $i;?></td>
                  <td><?php echo $reg["ape_paterno"] ;?></td>
                    <td><?php echo $reg["ape_materno"] ;?></td>
                    <td><?php echo $reg["nombre"] ;?></td>
                    <td><?php echo $reg["fechanac"] ;?></td>
                    <td><?php echo $reg["sexo"] ;?></td>
                    <td><?php echo $reg["estado_civil"] ;?></td>
                    <td><?php echo $reg["ubigeo"] ;?></td>
                    <td><?php echo $reg["urbanizacion"] ;?></td>
                    <td><?php echo $reg["direccion"] ;?></td>
                    <td><?php echo $reg["telefono"] ;?></td>
                    <td><?php echo $reg["celular"] ;?></td>
                    <td><?php echo $reg["email"] ;?></td>
                    <td><?php echo $reg["idugel"] ;?></td>
                    <td><?php echo $reg["regusuario"] ;?></td>
                    <td><?php echo $reg["regfecha"] ;?></td>
                    <td><?php echo $reg["usuario"] ;?></td>
                    <td><?php echo $reg["clave"] ;?></td>
                    <td><?php echo $reg["token"] ;?></td>
                    <td><?php echo $reg["foto"] ;?></td>
                    <td><?php echo $reg["estado"] ;?></td>
                    <td><?php echo $reg["situacion"] ;?></td>
                    <td><a class="btn btn-xs lis_ver " href="<?php echo JrAplicacion::getJrUrl(array('alumno'))?>ver/?id=<?php echo $reg["dni"]; ?>"><i class="fa fa-eye"></i></a>                
                <a class="btn btn-xs lis_update" href="<?php echo JrAplicacion::getJrUrl(array("alumno", "editar", "id=" . $reg["dni"]))?>"><i class="fa fa-edit"></i></a>
                <a class="btn-eliminar btn btn-xs lis_remove " href="javascript:;" data-id="<?php echo $reg["dni"]; ?>" ><i class="fa fa-trash-o"></i></a>
              </td>                        
            </tr>
            <?php } ?>
                    </tbody>
        </table>
        </div>
        </div>
      </div>
    </div>
</div>


<script type="text/javascript">
$(document).ready(function () {
  $('.btn-eliminar').bind({   
    click: function() {
       var id=$(this).attr('data-id');
       $.confirm({
        title: '<?php echo JrTexto::_('Confirm action');?>',
        content: '<?php echo JrTexto::_('It is sure to delete this record ?'); ?>',
        confirmButton: '<?php echo JrTexto::_('Accept');?>',
        cancelButton: '<?php echo JrTexto::_('Cancel');?>',
        confirmButtonClass: 'btn-success',
        cancelButtonClass: 'btn-danger',
        closeIcon: true,
        confirm: function(){
          var res = xajax__('', 'alumno', 'eliminar', id);
          if(res){
            return redir('<?php echo JrAplicacion::getJrUrl(array('alumno'))?>');
          }
        }
      });     
    }
  });
  
  $('.btn-chkoption').bind({
    click: function() {     
      var id=$(this).attr('data-id');
      var campo=$(this).attr('campo');
      var data=0;
      if($("i",this).hasClass('fa-circle-o')) data=1;
      $.confirm({
        title: '<?php echo JrTexto::_('Confirm action');?>',
        content: '<?php echo JrTexto::_('It is sure to change the status of this record ?'); ?>',
        confirmButton: '<?php echo JrTexto::_('Accept');?>',
        cancelButton: '<?php echo JrTexto::_('Cancel');?>',
        confirmButtonClass: 'btn-success',
        cancelButtonClass: 'btn-danger',
        closeIcon: true,
        confirm: function(){
           var res = xajax__('', 'alumno', 'setCampo', id,campo,data);
           if(res) {
            return redir('<?php echo JrAplicacion::getJrUrl(array('alumno'))?>');
           }
        }
      });
    }
  });
  
  $('.table').DataTable( {
    "language": {
            "url": "<?php echo $this->documento->getUrlStatic().'/libs/datatable1.10/idiomas/'.$this->documento->getIdioma(); ?>.json"
    }
  });
});
</script>