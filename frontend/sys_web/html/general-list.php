<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
?><style type="text/css">
  .panel-body{
    border: 1px solid rgba(90, 137, 248, 0.41);
    padding: 10px;
  }
  .title_left, .small{
    height: auto;
    color: #fff;
  }
  .form-group{
    margin-bottom: 0px; 
  }
  .input-group {
    margin-top: 0px;
  }
  select.select-ctrl, .form-control , .input-group-addon{   
    border: 1px solid #4683af;
    margin-bottom: 1ex;
  }
</style>
<div class="form-view" id="ventana_<?php echo $idgui; ?>" >
  <div class="page-title">
    <div class="title_left">
      <ol class="breadcrumb">
        <li><a href="<?php echo JrAplicacion::getJrUrl(array("academico"));?>"><?php echo JrTexto::_("Academic");?></a></li>
        <li><a href="<?php echo JrAplicacion::getJrUrl(array('general'));?>"><?php echo JrTexto::_('General'); ?></a></li>
        <li class="active"><?php echo JrTexto::_('list')?></li>
      </ol>
    </div>
  </div>
  <div class="clearfix"></div>
  <div class="div_linea"></div>
<div class="row">
  <div class="col-md-12">
    <div class="panel" >      
      <div class="panel-body">      
         <div class="row">
            <div class="col-md-12">
                                                                                                                                            
                      <div class="col-xs-6 col-sm-4 col-md-3 select-ctrl-wrapper select-azul" >
                        <select name="cbmostrar" id="cbmostrar" class="form-control select-ctrl">
                            <option value=""><?php echo ucfirst(JrTexto::_("All state"))?></option>
                            <option value="1"><?php echo ucfirst(JrTexto::_("Active"))?></option>
                            <option value="0"><?php echo ucfirst(JrTexto::_("Inactive"))?></option>                              
                        </select>
                      </div>
                                        
              <div class="col-xs-6 col-sm-6 col-md-6">
                <div class="form-group">
                <div class="input-group">
                  <input type="text" name="texto" id="texto" class="form-control border0" placeholder="<?php echo  ucfirst(JrTexto::_("text to search"))?>">
                  <span class="input-group-addon btn btnbuscar"><?php echo  ucfirst(JrTexto::_("Search"))?> <i class="fa fa-search"></i>
                     </span>  
                                     </div>
                </div>
              </div>
              <div class="col-xs-6 col-sm-3 col-md-3 text-center">
               <a class="btn btn-success btnvermodal" data-modal="si" href="<?php echo JrAplicacion::getJrUrl(array("General", "agregar"));?>" data-titulo="<?php echo JrTexto::_("General").' - '.JrTexto::_("add"); ?>">
          <i class="fa fa-plus"></i> <?php echo JrTexto::_('add')?></a>
              </div>
            </div>
          </div>
      </div>
    </div>
  </div> 
	<div class="col-md-12 col-sm-12 col-xs-12">
	   <div class="panel">
         
         <div class="panel-body">
            <table class="table table-striped table-responsive">
              <thead>
                <tr class="headings">
                  <th>#</th>
                  <th><?php echo JrTexto::_("Codigo") ;?></th>
                    <th><?php echo JrTexto::_("Nombre") ;?></th>
                    <th><?php echo JrTexto::_("Tipo tabla") ;?></th>
                    <th><?php echo JrTexto::_("Mostrar") ;?></th>
                    <th class="sorting_disabled"><span class="nobr"><?php echo JrTexto::_('Actions');?></span></th>
                </tr>
              </thead>
              <tbody>
              <?php $i=0; 
                if(!empty($this->datos))
                foreach ($this->datos as $reg){ $i++; ?>
                <tr>
                  <td><?php echo $i;?></td>
                  <td><?php echo $reg["codigo"] ;?></td>
                    <td><?php echo substr($reg["nombre"],0,100)."..."; ?></td>
                    <td><?php echo $reg["tipo_tabla"] ;?></td>
                    <td class="text-center"><a href="javascript:;"  class="btn-chkoption" campo="mostrar"  data-id="<?php echo $reg["idgeneral"]; ?>"> <i class="fa fa<?php echo !empty($reg["mostrar"])?"-check":""; ?>-circle-o fa-lg"></i> <?php echo $reg["mostrar"]=="1"?JrTexto::_("Active"):JrTexto::_("Inactive"); ?></a> </td>
                    <td><a class="btn btn-xs lis_ver " href="<?php echo JrAplicacion::getJrUrl(array('general'))?>ver/?id=<?php echo $reg["idgeneral"]; ?>"><i class="fa fa-eye"></i></a>                
                <a class="btn btn-xs lis_update btnvermodal" data-modal='si' href="<?php echo JrAplicacion::getJrUrl(array("general", "editar", "id=" . $reg["idgeneral"]))?>"  data-titulo="<?php echo JrTexto::_("General").' - '.JrTexto::_("edit"); ?>"><i class="fa fa-edit"></i></a>
                <a class="btn-eliminar btn btn-xs lis_remove " href="javascript:;" data-id="<?php echo $reg["idgeneral"]; ?>" ><i class="fa fa-trash-o"></i></a>
              </td>                        
            </tr>
            <?php } ?>
                    </tbody>
        </table>
        </div>
        </div>
      </div>
    </div>
</div>


<script type="text/javascript">
var tabledatos596fccf0676a0='';
function refreshdatos596fccf0676a0(){
    tabledatos596fccf0676a0.ajax.reload();
}
$(document).ready(function(){  
  var estados596fccf0676a0={'1':'<?php echo JrTexto::_("Active") ?>','0':'<?php echo JrTexto::_("Inactive") ?>','C':'<?php echo JrTexto::_("Cancelled") ?>'}
  var tituloedit596fccf0676a0='<?php echo ucfirst(JrTexto::_("general"))." - ".JrTexto::_("edit"); ?>';
  var draw596fccf0676a0=0;

  
  $('#cbmostrar').change(function(ev){
    refreshdatos596fccf0676a0();
  });
  $('.btnbuscar').click(function(ev){
    refreshdatos596fccf0676a0();
  });
  tabledatos596fccf0676a0=$('#ventana_<?php echo $idgui; ?> .table').DataTable(
    { "searching": false,
      "processing": false,
      //"serverSide": true,
      "columns" : [
        {'data': '#'},
        
        {'data': '<?php echo JrTexto::_("Codigo") ;?>'},
            {'data': '<?php echo JrTexto::_("Nombre") ;?>'},
            {'data': '<?php echo JrTexto::_("Tipo_tabla") ;?>'},
            {'data': '<?php echo JrTexto::_("Mostrar") ;?>'},
            
        {'data': '<?php echo JrTexto::_("Actions") ;?>'},
      ],
      "ajax":{
        url:_sysUrlBase_+'/general/buscarjson/?json=true',
        type: "post",                
        data:function(d){
            d.json=true                   
            d.mostrar=$('#cbmostrar').val(),
            d.nombre=$('#texto').val(),
                        
            draw596fccf0676a0=d.draw;
           // console.log(d);
        },
        "dataSrc":function(json){
          var data=json.data;             
          json.draw = draw596fccf0676a0;
          json.recordsTotal = json.data.length;
          json.recordsFiltered = json.data.length;
          var datainfo = new Array();
          for(var i=0;i< data.length; i++){                   
            datainfo.push({
              '#':(i+1),
              '<?php echo JrTexto::_("Codigo") ;?>': data[i].codigo,
              '<?php echo JrTexto::_("Nombre") ;?>': data[i].nombre,
                '<?php echo JrTexto::_("Tipo_tabla") ;?>': data[i].tipo_tabla,
              '<?php echo JrTexto::_("Mostrar") ;?>': '<a href="javascript:;"  class="btn-chkoption" campo="mostrar"  data-id="'+data[i].idgeneral+'"> <i class="fa fa'+(data[i].mostrar=='1'?'-check':'')+'-circle-o fa-lg"></i> '+estados596fccf0676a0[data[i].mostrar]+'</a>',
              
              '<?php echo JrTexto::_("Actions") ;?>'  :'<a class="btn btn-xs btnvermodal" data-modal="si" href="'+_sysUrlBase_+'/general/editar/?id='+data[i].idgeneral+'" data-titulo="'+tituloedit596fccf0676a0+'"><i class="fa fa-edit"></i></a><a class="btn-eliminar btn btn-xs" href="javascript:;" data-id="'+data[i].idgeneral+'" ><i class="fa fa-trash-o"></i>'
            });
          }
          return datainfo }, error: function(d){console.log(d)}
      }
      <?php echo $this->documento->getIdioma()!='EN'?(',"language": { "url": "'.$this->documento->getUrlStatic().'/libs/datatable1.10/idiomas/'.$this->documento->getIdioma().'.json"}'):''?>
    });


  $('#ventana_<?php echo $idgui; ?>').on('click','.btn-chkoption',function(){     
      var id=$(this).attr('data-id');
      var campo=$(this).attr('campo');
      var data=0;
      if($("i",this).hasClass('fa-circle-o')) data=1;
      $.confirm({
        title: '<?php echo JrTexto::_('Confirm action');?>',
        content: '<?php echo JrTexto::_('It is sure to change the status of this record ?'); ?>',
        confirmButton: '<?php echo JrTexto::_('Accept');?>',
        cancelButton: '<?php echo JrTexto::_('Cancel');?>',
        confirmButtonClass: 'btn-success',
        cancelButtonClass: 'btn-danger',
        closeIcon: true,
        confirm: function(){
          var res = xajax__('', 'general', 'setCampo', id,campo,data);
          if(res) tabledatos596fccf0676a0.ajax.reload();
        }
      });
  });

  $('#ventana_<?php echo $idgui; ?>').on('click','.btnvermodal',function(e){
    e.preventDefault();
    e.stopPropagation();
    var enmodal=$(this).attr('data-modal')||'no';
    var fcall=$(this).attr('data-fcall')||'refreshdatos596fccf0676a0';
    var url=$(this).attr('href')
    if(url.indexOf('?')!=-1) url+='&fcall='+fcall;
    else url+='?fcall='+fcall;
    var ventana=$(this).data('ventana')||'General';
    var claseid=ventana+'_<?php echo $idgui; ?>';
    var titulo=$(this).attr('data-titulo')||'';
    titulo=titulo.toString().replace('<br>',' ');     
    if(enmodal=='no'){
      return redir(url);          
    }
    url+='&plt=modal';
    openModal('lg',titulo,url,ventana,claseid); 
  });
  
 

  $('#ventana_<?php echo $idgui; ?> .table').on('click','.btn-eliminar',function(){
     var id=$(this).attr('data-id');
     $.confirm({
      title: '<?php echo JrTexto::_('Confirm action');?>',
      content: '<?php echo JrTexto::_('It is sure to delete this record ?'); ?>',
      confirmButton: '<?php echo JrTexto::_('Accept');?>',
      cancelButton: '<?php echo JrTexto::_('Cancel');?>',
      confirmButtonClass: 'btn-success',
      cancelButtonClass: 'btn-danger',
      closeIcon: true,
      confirm: function(){             
        var res = xajax__('', 'general', 'eliminar', id);
        if(res) tabledatos596fccf0676a0.ajax.reload();
      }
    }); 
  });

});
</script>