<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
?><style type="text/css">
  .panel-body{
    border: 1px solid rgba(90, 137, 248, 0.41);
    padding: 10px;
  }
  .title_left, .small{
    height: auto;
    color: #fff;
  }
  .form-group{
    margin-bottom: 0px; 
  }
  .input-group {
    margin-top: 0px;
  }
  .select-ctrl-wrapper:after{
      right: 0px;
    }
  select.select-ctrl, .form-control , .input-group-addon{   
    border: 1px solid #4683af;
    margin-bottom: 1ex;
  }
  .view-list2{
    border: 1px  solid #e4dddd;
    padding: 0.5ex;
  }
  .panel-user{
    padding:0.5ex;
  }
  .panel-user .panel{
    min-height:160px; 
    max-height: 160px;
    overflow: auto;
  }
  .pnlacciones{
    position: absolute;
    bottom: 29px;
    text-align: center;
    width: 98%;
    background: #469de8;
    margin: -11px;
    padding: 5px;
    -webkit-animation: fadein 1s; /* Safari, Chrome and Opera > 12.1 */
       -moz-animation: fadein 1s; /* Firefox < 16 */
        -ms-animation: fadein 1s; /* Internet Explorer */
         -o-animation: fadein 1s; /* Opera < 12.1 */
            animation: fadein 1s;
  }
  @keyframes fadein {
    from { opacity: 0; }
    to   { opacity: 1; }
  }
  .pnlacciones a{
    margin: 0.25ex;
    width: 30px;
  }
  .panel-user .pnlacciones{
    display: none;    
  }
  .panel-user.active .pnlacciones{
    display: block;
  }
  .breadcrumb a{ color: #fff; }
  .breadcrumb li.active{ color:#d2cccc; }

  
 
</style>
<div class="form-view" id="ventana_<?php echo $idgui; ?>" >
  <div class="page-title">
    <div class="title_left">
      <ol class="breadcrumb">
        <li><a href="<?php echo JrAplicacion::getJrUrl(array("academico"));?>"><?php echo JrTexto::_("Academic");?></a></li>
        <li><a href="<?php echo JrAplicacion::getJrUrl(array('personal'));?>"><?php echo JrTexto::_('Personal'); ?></a></li>
        <li class="active"><?php echo JrTexto::_('list')?></li>
      </ol>
    </div>
  </div>
  <div class="clearfix"></div>
<div class="row">
  <div class="col-md-12">
    <div class="panel" >      
      <div class="panel-body">      
         <div class="row">            
            <div class="col-xs-6 col-sm-4 col-md-3">
            <label><?php echo JrTexto::_('Sexo'); ?></label>
            <div class="select-ctrl-wrapper select-azul">
              <select id="fkcbsexo" name="fkcbsexo" class="form-control select-ctrl" >
                <option value=""><?php echo JrTexto::_('Seleccione'); ?></option>
                  <?php 
                  if(!empty($this->fksexo))
                  foreach ($this->fksexo as $fksexo) { ?><option value="<?php echo $fksexo["codigo"]?>" <?php echo $fksexo["codigo"]==@$frm["sexo"]?"selected":""; ?> ><?php echo $fksexo["nombre"] ?></option>
                   <?php } ?>                        
              </select>
            </div>
            </div>

            <div class="col-xs-6 col-sm-4 col-md-3">
            <label><?php echo JrTexto::_('Estado Civil'); ?></label>
            <div class="select-ctrl-wrapper select-azul">
              <select id="fkcbestado_civil" name="fkcbestado_civil" class="form-control select-ctrl" >
                <option value=""><?php echo JrTexto::_('Seleccione'); ?></option>
                  <?php 
                          if(!empty($this->fkestado_civil))
                            foreach ($this->fkestado_civil as $fkestado_civil) { ?><option value="<?php echo $fkestado_civil["codigo"]?>" <?php echo $fkestado_civil["codigo"]==@$frm["estado_civil"]?"selected":""; ?> ><?php echo $fkestado_civil["nombre"] ?></option><?php } ?>                        
              </select>
            </div>
            </div>
                          
            
                                                                        
            <div class="col-xs-6 col-sm-4 col-md-3 ">
              <label><?php echo JrTexto::_('Rol'); ?></label>
              <div class="select-ctrl-wrapper select-azul">
                <select id="fkcbrol" name="fkcbrol" class="form-control select-ctrl" >
                  <option value=""><?php echo JrTexto::_('Seleccione'); ?></option>
                    <?php 
                            if(!empty($this->fkrol))
                              foreach ($this->fkrol as $fkrol) { ?>
                            <option value="<?php echo $fkrol["idrol"]?>" <?php echo $fkrol["idrol"]==@$frm["rol"]?"selected":""; ?> ><?php echo ucfirst($fkrol["rol"]); ?></option><?php } ?>                        
                </select>
              </div>
            </div>
            <div class="col-xs-6 col-sm-4 col-md-3 ">
              <label><?php echo JrTexto::_('Estado'); ?></label>
              <div class="select-ctrl-wrapper select-azul">
                <select name="cbestado" id="cbestado" class="form-control select-ctrl">
                    <option value=""><?php echo ucfirst(JrTexto::_("All state"))?></option>
                    <option value="1"><?php echo ucfirst(JrTexto::_("Active"))?></option>
                    <option value="0"><?php echo ucfirst(JrTexto::_("Inactive"))?></option>                              
                </select>
              </div>
            </div>                                                          
           
                                              
            <div class="col-xs-12 col-sm-6 col-md-6">
              <div class="form-group">
                <div class="input-group">
                  <input type="text" name="texto" id="texto" class="form-control border0" placeholder="<?php echo  ucfirst(JrTexto::_("text to search"))?>">
                  <span class="input-group-addon btn btnbuscar"><?php echo  ucfirst(JrTexto::_("Search"))?> <i class="fa fa-search"></i></span>  
                                    </div>
              </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 text-center">
              <a class="btn btn-warning btnvermodal" data-modal="no" href="<?php echo JrAplicacion::getJrUrl(array("personal"));?>" data-titulo="<?php echo JrTexto::_("Personal").' - '.JrTexto::_("List"); ?>"><i class="fa fa-list-ol"></i> </a>
               <a class="btn btn-success btnvermodal" data-modal="si" href="<?php echo JrAplicacion::getJrUrl(array("personal", "agregar"));?>" data-titulo="<?php echo JrTexto::_("Personal").' - '.JrTexto::_("Add"); ?>"><i class="fa fa-plus"></i> <?php echo JrTexto::_('add')?></a>
               <a class="btn btn-primary btnvermodal" data-modal="si" href="<?php echo JrAplicacion::getJrUrl(array("personal", "importar"));?>" data-titulo="<?php echo JrTexto::_("Personal").' - '.JrTexto::_("Import"); ?>"><i class="fa fa-cloud-upload"></i> <?php echo JrTexto::_('Import')?></a>
              </div>
            </div>
          </div>
      </div>
    </div>
	  <div class="col-md-12 col-sm-12 col-xs-12" id="datalistado">
    <div id="cargando">cargando</div>
    <div id="sindatos">sindatos</div>
    <div id="error">sindatos</div>
    <div id="data">data aqui</div>	   
    </div>
</div>

<script type="text/javascript">
var tabledatos5976271b3962a='';
function refreshdatos5976271b3962a(){
    tabledatos5976271b3962a();
}
$(document).ready(function(){
  $('#datalistado').on("mouseenter",'.panel-user', function(){
    if(!$(this).hasClass('active')){ 
      $(this).siblings('.panel-user').removeClass('active');
      $(this).addClass('active');
    }
  });
  $('#datalistado').on("mouseleave",'.panel-user', function(){
    $(this).removeClass('active');
  });
  var estados5976271b3962a={'1':'<?php echo JrTexto::_("Active") ?>','0':'<?php echo JrTexto::_("Inactive") ?>','C':'<?php echo JrTexto::_("Cancelled") ?>'}
  var sexos597627b63b43a=<?php echo json_encode($this->fksexo); ?>;
  var sexo597627b63b43a={};
  $.each(sexos597627b63b43a,function(i,v){
    sexo597627b63b43a["'"+v.codigo+"'"]=v.nombre;
  })
  var estadocivil597627b63b43a=<?php echo json_encode($this->fkestado_civil); ?>;
  var estadociv597627b63b43a={};
  $.each(estadocivil597627b63b43a,function(i,v){
    estadociv597627b63b43a["'"+v.codigo+"'"]=v.nombre;
  })
  var tituloedit5976271b3962a='<?php echo ucfirst(JrTexto::_("personal"))." - ".JrTexto::_("edit"); ?>';
  var tituloficha5976271b3962a='<?php echo ucfirst(JrTexto::_("Ficha"));?> - ';
  var titulocambiaruser5976271b3962a='<?php echo ucfirst(JrTexto::_("Change"))." - ".JrTexto::_("User"); ?> ';

  tabledatos5976271b3962a=function(){
    var d={};
      d.json=true;              
      d.sexo=$('#fkcbsexo').val();
      d.estado_civil=$('#fkcbestado_civil').val();
      d.texto=$('#texto').val();
      d.rol=$('#fkcbrol').val();
      d.estado=$('#cbestado').val();
    $.ajax({
      url: _sysUrlBase_+'/personal/buscarjson/',
      type: "POST",
      data:  d,
      contentType: false,
      processData: false,
      dataType :'json',
      cache: false,
      processData:false,      
      beforeSend: function(XMLHttpRequest){
        $('#datalistado #cargando').show('fast').siblings('div').hide('fast');
      },      
      success: function(res)
      {  
        console.log(res);      
        if(res.code==='ok'){
          var midata=res.data;
          if(midata){
            var html='';
            $.each(midata,function(i,v){
               var fullname=v.ape_paterno+' '+v.ape_materno+', '+v.nombre;
              var img='<img class="img-circle img-responsive" src="'+_sysUrlStatic_ +'/media/usuarios/'+(v.foto||'user_avatar.jpg')+'">';

              html+='<div class="col-md-4 col-sm-6 col-xs-12 panel-user">'+
                      '<div class="panel panel-body"><h4 class="brief"><i>'+v.ape_paterno+' '+v.ape_materno+', '+v.nombre+'</i></h4>';
              html+='<div class="left col-xs-8">'+ 
              '<ul class="list-unstyled">'+
                '<li><i class="fa fa-envelope-o"></i> :'+v.email+' </li>'+
                '<li><i class="fa fa-phone"></i> :'+v.telefono+' / '+v.celular+' </li>'+
                '<li><i class="fa fa-user"></i> :'+v.usuario+' </li>'+
              '</ul>'+
            '</div>'+
            '<div class="right col-xs-4 text-center">'+img+'</div>'+
            '<div class="pnlacciones text-center">'+ 
               '<a class="btn  btn-success btn-xs btnvermodal" data-modal="si" href="'+_sysUrlBase_+'/alumno/verficha/?id='+v.dni+'" data-titulo="'+tituloficha5976271b3962a+fullname+'"> <i class="fa fa-user"></i><i class="fa fa-eye"></i></a>'+
                '<a class="btn btn-warning btn-xs btnvermodal" data-modal="si" href="'+_sysUrlBase_+'/alumno/editar/?id='+v.dni+'" data-titulo="'+tituloedit5976271b3962a+'"> <i class="fa fa-user"></i><i class="fa fa-pencil"></i></a>'+
                '<a class="btn  btn-success btn-xs btnvermodal" data-modal="si" href="'+_sysUrlBase_+'/alumno/cambiarclave/?id='+v.dni+'" data-titulo="'+titulocambiaruser5976271b3962a+'"><i class="fa fa-user"></i><i class="fa fa-key"></i></a>'+ 
                '<a class="btn  btn-success btn-xs btnvermodal" data-modal="si" href="'+_sysUrlBase_+'/alumno/notas/?id='+v.dni+'" data-titulo="'+tituloedit5976271b3962a+'"><i class="btn-icon glyphicon glyphicon-list-alt"></i></a>'+
                '<a class="btn  btn-success btn-xs btnvermodal" data-modal="si" href="'+_sysUrlBase_+'/alumno/horario/?id='+v.dni+'" data-titulo="'+tituloedit5976271b3962a+'"><i class="fa fa-calendar"></i></a>'+
                '<a class="btn-eliminar btn btn-danger  btn-xs" href="javascript:;" data-id="'+v.dni+'" ><i class="fa fa-trash-o"></i></a>'+ 
            '</div>';           
            html+='</div></div></div>';

            });
            $('#datalistado #data').html(html).show('fast').siblings('div').hide('fast');
          }else     
          $('#datalistado #sindatos').show('fast').siblings('div').hide('fast');
        }else{
          $('#datalistado #error').show('fast').siblings('div').hide('fast');
        }
      },
      error: function(e) 
      {
          $('#datalistado #error').show('fast').siblings('div').hide('fast');
      }      
    });
  };
  tabledatos5976271b3962a();
  $('#fkcbsexo').change(function(ev){
    refreshdatos5976271b3962a();
  });
  $('#fkcbestado_civil').change(function(ev){
    refreshdatos5976271b3962a();
  });
 
  $('#fkcbrol').change(function(ev){
    refreshdatos5976271b3962a();
  });
  $('#cbestado').change(function(ev){
    refreshdatos5976271b3962a();
  });
  $('#cbsituacion').change(function(ev){
    refreshdatos5976271b3962a();
  });
  $('.btnbuscar').click(function(ev){
    refreshdatos5976271b3962a();
  });
  $('#texto').keyup(function(e) {
    if(e.keyCode == 13) {
        refreshdatos5976271b3962a();
    }
  });
  
  $('#ventana_<?php echo $idgui; ?>').on('click','.btn-chkoption',function(){     
      var id=$(this).attr('data-id');
      var campo=$(this).attr('campo');
      var data=0;
      if($("i",this).hasClass('fa-circle-o')) data=1;
      $.confirm({
        title: '<?php echo JrTexto::_('Confirm action');?>',
        content: '<?php echo JrTexto::_('It is sure to change the status of this record ?'); ?>',
        confirmButton: '<?php echo JrTexto::_('Accept');?>',
        cancelButton: '<?php echo JrTexto::_('Cancel');?>',
        confirmButtonClass: 'btn-success',
        cancelButtonClass: 'btn-danger',
        closeIcon: true,
        confirm: function(){
          var res = xajax__('', 'personal', 'setCampo', id,campo,data);
          if(res) tabledatos5976271b3962a();
        }
      });
  });

  $('#ventana_<?php echo $idgui; ?>').on('click','.btnvermodal',function(e){
    e.preventDefault();
    e.stopPropagation();
    var enmodal=$(this).attr('data-modal')||'no';
    var fcall=$(this).attr('data-fcall')||'refreshdatos5976271b3962a';
    var url=$(this).attr('href')
    if(url.indexOf('?')!=-1) url+='&fcall='+fcall;
    else url+='?fcall='+fcall;
    var ventana=$(this).data('ventana')||'Personal';
    var claseid=ventana+'_<?php echo $idgui; ?>';
    var titulo=$(this).attr('data-titulo')||'';
    titulo=titulo.toString().replace('<br>',' ');     
    if(enmodal=='no'){
      return redir(url);          
    }
    url+='&plt=modal';
    openModal('lg',titulo,url,ventana,claseid); 
  });
  
  $('#ventana_<?php echo $idgui; ?> .table').on('click','.btn-eliminar',function(){
     var id=$(this).attr('data-id');
     $.confirm({
      title: '<?php echo JrTexto::_('Confirm action');?>',
      content: '<?php echo JrTexto::_('It is sure to delete this record ?'); ?>',
      confirmButton: '<?php echo JrTexto::_('Accept');?>',
      cancelButton: '<?php echo JrTexto::_('Cancel');?>',
      confirmButtonClass: 'btn-success',
      cancelButtonClass: 'btn-danger',
      closeIcon: true,
      confirm: function(){             
        var res = xajax__('', 'personal', 'eliminar', id);
        if(res) tabledatos5976271b3962a();
      }
    }); 
  });
});
</script>