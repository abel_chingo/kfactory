<div class="row" id="breadcrumb"> <div class="col-xs-12">
    <ol class="breadcrumb">
        <li><a href="<?php echo $this->documento->getUrlBase();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_('Home'); ?></a></li>
        <?php foreach ($this->breadcrumb as $b) {
        $enlace = '<li>';
        if(!empty($b['link'])){ $enlace .= '<a href="'.$this->documento->getUrlBase().$b['link'].'">'.ucfirst(JrTexto::_($b['texto'])).'</a>'; }
        else{ $enlace .= ucfirst(JrTexto::_($b['texto'])); }
        $enlace .= '</li>';
        echo $enlace;
        } ?>
    </ol>
</div> </div>

<div class="row" id="academico"> <div class="col-xs-12">
    <h2 class="col-xs-12 col-sm-8 color-yellow" style="margin-top: 0;">
        <i class="fa fa-graduation-cap"></i> 
        <?php echo ucfirst(JrTexto::_('Academic')); ?>
    </h2>
    <div class="col-xs-12">
        <ul class="nav nav-pills">
            <li class="active"><a href="#calificaciones" data-toggle="pill"><?php echo ucfirst(JrTexto::_('Grades')); ?></a></li>
            <li><a href="#asistencias" data-toggle="pill"><?php echo ucfirst(JrTexto::_('attendances')); ?></a></li>
        </ul>
    </div>
    <div class="col-xs-12 ">
        <div class="panel border-yellow">
            <div class="panel-body">
                <div class="col-xs-6 select-ctrl-wrapper select-orange">
                    <select name="opcPeriodoAcademico" id="opcPeriodoAcademico" class="form-control select-ctrl">
                        <option value="-1">- <?php echo ucfirst(JrTexto::_('Select academic period')); ?> -</option>
                        <option value="1">Trimestre I</option>
                        <option value="2">Trimestre II</option>
                        <option value="3">Trimestre III</option>
                    </select>
                </div>
            </div>
            <div class="panel-body tab-content" style="max-height: 300px; overflow: auto;">
                <div class="tab-pane fade in active" id="calificaciones">
                    <table class="table table-striped">
                        <tbody>
                            <tr>
                                <td>Calificacion 01</td>
                                <td>12.50</td>
                            </tr>
                            <tr>
                                <td>Calificacion 02</td>
                                <td>12.50</td>
                            </tr>
                            <tr>
                                <td>Calificacion 03</td>
                                <td>12.50</td>
                            </tr>
                        </tbody>
                        <tfoot>
                            <tr class="bolder">
                                <td class="text-right">Promedio</td>
                                <td class="">15.00</td>
                            </tr>
                        </tfoot>
                    </table>
                </div>

                <div class="tab-pane fade" id="asistencias">
                    <table class="table table-striped">
                        <tbody>
                            <tr>
                                <td>01-07-2017</td>
                                <td><i class="fa fa-circle fa-2x color-green"></i></td>
                            </tr>
                            <tr>
                                <td>03-07-2017</td>
                                <td><i class="fa fa-circle fa-2x color-green"></i></td>
                            </tr>
                            <tr>
                                <td>05-07-2017</td>
                                <td><i class="fa fa-circle fa-2x color-red"></i></td>
                            </tr>
                            <tr>
                                <td>08-07-2017</td>
                                <td><i class="fa fa-circle fa-2x color-yellow"></i></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div></div>