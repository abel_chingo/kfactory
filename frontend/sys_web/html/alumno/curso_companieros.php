<div class="row" id="breadcrumb"> <div class="col-xs-12">
    <ol class="breadcrumb">
        <li><a href="<?php echo $this->documento->getUrlBase();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_('Home'); ?></a></li>
        <?php foreach ($this->breadcrumb as $b) {
        $enlace = '<li>';
        if(!empty($b['link'])){ $enlace .= '<a href="'.$this->documento->getUrlBase().$b['link'].'">'.ucfirst(JrTexto::_($b['texto'])).'</a>'; }
        else{ $enlace .= ucfirst(JrTexto::_($b['texto'])); }
        $enlace .= '</li>';
        echo $enlace;
        } ?>
    </ol>
</div> </div>

<div class="row" id="curso-companieros"> <div class="col-xs-12">    
    <h2 class="col-xs-12 col-sm-8 color-blue" style="margin-top: 0;">
        <i class="fa fa-users"></i> 
        <?php echo ucfirst(JrTexto::_('Classmates')); ?>
    </h2>
    <div class="col-xs-12" style="padding: 0;">
    <?php for ($i=1; $i <= 15; $i++) { ?>
        <div class="col-xs-6 col-sm-4 col-md-3">
            <div class="panel border-blue">
                <div class="panel-body"><div class="row">
                    <div class="col-xs-12"><img src="<?php echo $this->documento->getUrlStatic(); ?>/media/usuarios/default_m.png" alt="foto_alumno" class="img-responsive center-block border-blue foto_alumno"></div>
                    <ul class="col-xs-12 list-unstyled informacion">
                        <li class="col-xs-12">
                            <div class="col-xs-2 icono"><i class="fa fa-graduation-cap"></i></div>
                            <div class="col-xs-10 texto_info"><?php echo 'Eder Figueroa Piscoya'; ?></div>
                        </li>
                        <li class="col-xs-12">
                            <div class="col-xs-2 icono"><i class="fa fa-at"></i></div>
                            <div class="col-xs-10 texto_info"><?php echo 'eder.figueroa@outlook.com'; ?></div>
                        </li>
                        <li class="col-xs-12">
                            <div class="col-xs-2 icono"><i class="fa fa-phone"></i></div>
                            <div class="col-xs-10 texto_info"><?php echo '956 123 456'; ?></div>
                        </li>
                    </ul>
                </div></div>
                <!--div class="panel-footer text-center">
                    <a href="<?php echo $this->documento->getUrlBase();?>/docente/perfil" class="btn btn-blue"><?php echo JrTexto::_('View profile'); ?></a>
                </div-->
            </div>
        </div>
        <div class="col-xs-6 col-sm-4 col-md-3">
            <div class="panel border-blue">
                <div class="panel-body"><div class="row">
                    <div class="col-xs-12"><img src="<?php echo $this->documento->getUrlStatic(); ?>/media/usuarios/default_f.png" alt="foto_alumno" class="img-responsive center-block border-blue foto_alumno"></div>
                    <ul class="col-xs-12 list-unstyled informacion">
                        <li class="col-xs-12">
                            <div class="col-xs-2 icono"><i class="fa fa-graduation-cap"></i></div>
                            <div class="col-xs-10 texto_info"><?php echo 'Enny Pe&ntilde;a Flores'; ?></div>
                        </li>
                        <li class="col-xs-12">
                            <div class="col-xs-2 icono"><i class="fa fa-at"></i></div>
                            <div class="col-xs-10 texto_info"><?php echo 'enrufp@gmail.com'; ?></div>
                        </li>
                        <li class="col-xs-12">
                            <div class="col-xs-2 icono"><i class="fa fa-phone"></i></div>
                            <div class="col-xs-10 texto_info"><?php echo '965 321 654'; ?></div>
                        </li>
                    </ul>
                </div></div>
                <!--div class="panel-footer text-center">
                    <a href="<?php echo $this->documento->getUrlBase();?>/docente/perfil" class="btn btn-blue"><?php echo JrTexto::_('View profile'); ?></a>
                </div-->
            </div>
        </div>
    <?php } ?>
    </div>
</div></div>