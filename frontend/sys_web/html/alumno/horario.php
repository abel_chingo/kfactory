<div class="row" id="breadcrumb"> <div class="col-xs-12">
    <ol class="breadcrumb">
        <li><a href="<?php echo $this->documento->getUrlBase();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_('Home'); ?></a></li>
        <?php foreach ($this->breadcrumb as $b) {
        $enlace = '<li>';
        if(!empty($b['link'])){ $enlace .= '<a href="'.$this->documento->getUrlBase().$b['link'].'">'.ucfirst(JrTexto::_($b['texto'])).'</a>'; }
        else{ $enlace .= ucfirst(JrTexto::_($b['texto'])); }
        $enlace .= '</li>';
        echo $enlace;
        } ?>
    </ol>
</div> </div>

<div class="row" id="horario"> <div class="col-xs-12">
    <h2 class="col-xs-12 col-sm-8 color-brown-dark" style="margin-top: 0;">
        <i class="fa fa-calendar-check-o"></i> 
        <?php echo ucfirst(JrTexto::_('Schedule of the course')); ?>
    </h2>
    <a href="#" class="col-xs-12 col-sm-4 btn btn-brown-dark"><?php echo ucfirst(JrTexto::_('See all class schedule')); ?> <i class="fa fa-chevron-right pull-right"></i></a>
    <div class="col-xs-12">
    <?php for ($i=1; $i <= 31; $i++) { 
        $num = (($i<10)?'0':'').$i;
        $fila= '|_'.$num.'_';
        if($i%7==0) { 
            $fila.= '|<br>';
        }
        echo $fila;
    } echo '|'; ?>
    </div>
</div></div>