<div class="container">
    <div class="row">
        <div class="col-xs-12 col-sm-9">
            <div class="panel border-red" id="pnl-cursos">
                <div class="panel-heading bg-red">
                    <h3 class="panel-titulo">
                        <i class="fa fa-bookmark"></i>&nbsp;&nbsp;
                        <?php echo JrTexto::_('Courses'); ?>
                    </h3>
                    <span class="pull-right clickable"><i class="glyphicon glyphicon-chevron-up"></i></span>
                </div>
                <div class="panel-body">
                    <div class="row slider-cursos" style="margin-bottom: 0;">
                        <a href="<?php echo $this->documento->getUrlBase(); ?>/curso/?id=1" class="col-xs-6 col-sm-3 item-curso" title="Biología II">
                            <img src="<?php echo $this->documento->getUrlStatic(); ?>/media/image/N1-U1-A1-20170124040515.jpg" class="img-responsive caratula" alt="cover">
                            <div class="nombre color-turquoise text-center">Biología II</div>
                        </a>
                        <a href="<?php echo $this->documento->getUrlBase(); ?>/curso/?id=1" class="col-xs-6 col-sm-3 item-curso" title="Matemática II">
                            <img src="<?php echo $this->documento->getUrlStatic(); ?>/media/image/N1-U1-A1-20170202120005.jpg" class="img-responsive caratula" alt="cover">
                            <div class="nombre color-turquoise text-center">Matemática II</div>
                        </a>
                        <a href="<?php echo $this->documento->getUrlBase(); ?>/curso/?id=1" class="col-xs-6 col-sm-3 item-curso" title="Razonamiento Lógico II">
                            <img src="<?php echo $this->documento->getUrlStatic(); ?>/media/image/N1-U1-A1-20170218104738.jpg" class="img-responsive caratula" alt="cover">
                            <div class="nombre color-turquoise text-center">Razonamiento Lógico II</div>
                        </a>
                        <a href="<?php echo $this->documento->getUrlBase(); ?>/curso/?id=1" class="col-xs-6 col-sm-3 item-curso" title="Metodología de la Investigación I">
                            <img src="<?php echo $this->documento->getUrlStatic(); ?>/media/image/N1-U1-A1-20170221075548.jpg" class="img-responsive caratula" alt="cover">
                            <div class="nombre color-turquoise text-center">Metodología de la Investigación I</div>
                        </a>
                        <a href="<?php echo $this->documento->getUrlBase(); ?>/curso/?id=1" class="col-xs-6 col-sm-3 item-curso" title="Redactar un nombre demasiado largo para un curso III">
                            <img src="<?php echo $this->documento->getUrlStatic(); ?>/media/image/N1-U1-A1-20170221083810.jpg" class="img-responsive caratula" alt="cover">
                            <div class="nombre color-turquoise text-center">Redactar unambre demasiado largo para un curso III</div>
                        </a>
                        <a href="<?php echo $this->documento->getUrlBase(); ?>/curso/?id=1" class="col-xs-6 col-sm-3 item-curso" title="Ingles Avanzado C1">
                            <img src="<?php echo $this->documento->getUrlStatic(); ?>/media/image/N1-U1-A1-20161226060608.jpg" class="img-responsive caratula" alt="cover">
                            <div class="nombre color-turquoise text-center">Ingles Avanzado C1</div>
                        </a>
                        <a href="<?php echo $this->documento->getUrlBase(); ?>/curso/?id=1" class="col-xs-6 col-sm-3 item-curso" title="Metodología de la Investigación I">
                            <img src="<?php echo $this->documento->getUrlStatic(); ?>/media/image/N1-U1-A1-20170221075548.jpg" class="img-responsive caratula" alt="cover">
                            <div class="nombre color-turquoise text-center">Metodología de la Investigación I</div>
                        </a>
                        <a href="<?php echo $this->documento->getUrlBase(); ?>/curso/?id=1" class="col-xs-6 col-sm-3 item-curso" title="Redactar un nombre demasiado largo para un curso III">
                            <img src="<?php echo $this->documento->getUrlStatic(); ?>/media/image/N1-U1-A1-20170221083810.jpg" class="img-responsive caratula" alt="cover">
                            <div class="nombre color-turquoise text-center">Redactar unambre demasiado largo para un curso III</div>
                        </a>
                        <a href="<?php echo $this->documento->getUrlBase(); ?>/curso/?id=1" class="col-xs-6 col-sm-3 item-curso" title="Ingles Avanzado C1">
                            <img src="<?php echo $this->documento->getUrlStatic(); ?>/media/image/N1-U1-A1-20161226060608.jpg" class="img-responsive caratula" alt="cover">
                            <div class="nombre color-turquoise text-center">Ingles Avanzado C1</div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-3">
            <div class="panel border-brown-dark" id="pnl-agenda">
                <div class="panel-heading bg-brown-dark">
                    <h3 class="panel-titulo">
                        <i class="fa fa-calendar-check-o"></i>&nbsp;&nbsp;
                        <?php echo JrTexto::_('Agenda'); ?>
                    </h3>
                    <a href="#" class="ver-todo" title="<?php echo JrTexto::_('See all'); ?>"><i class="glyphicon glyphicon-chevron-right"></i></a>
                </div>
                <div class="panel-body">
                    <table class="table table-striped" id="tblAgendaHoy">
                        <thead>
                            <tr class="bg-brown">
                                <th class="text-right"><?php echo JrTexto::_('Today'); ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr><td>Tarea</td></tr>
                            <tr><td>Aula virtual</td></tr>
                            <tr><td>Mi nota de agenda</td></tr>
                            <tr><td>Anotacion 2</td></tr>
                        </tbody>
                    </table>
                    <table class="table table-striped" id="tblAgendaManiana">
                        <thead>
                            <tr class="bg-brown">
                                <th class="text-right"><?php echo JrTexto::_('Tommorrow'); ?></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr><td>Tarea</td></tr>
                            <tr><td>Aula virtual</td></tr>
                            <tr><td>Mi nota de agenda</td></tr>
                            <tr><td>Mi nota de agenda</td></tr>
                            <tr><td>Anotacion 2</td></tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12 col-sm-7" style="padding: 0;">
            <div class="col-xs-6 col-lg-4 btn-panel-container panelcont-xs">
                <a href="#" class="btn btn-block btn-red btn-panel">
                    <i class="btn-icon fa fa-university"></i>
                    <?php echo JrTexto::_('Virtual Classroom'); ?>
                    <span class="color-red-dark border-red-dark badge">4</span>
                </a>
            </div>
            <div class="col-xs-6 col-lg-4 btn-panel-container panelcont-xs">
                <a href="<?php echo $this->documento->getUrlBase(); ?>/tarea" class="btn btn-block btn-aqua btn-panel">
                    <i class="btn-icon fa fa-briefcase"></i>
                    <?php echo JrTexto::_('Homework'); ?>
                    <span class="color-aqua-dark border-aqua-dark badge">1</span>
                </a>
            </div>
            <div class="col-xs-6 col-lg-4 btn-panel-container panelcont-xs">
                <a href="#" class="btn btn-block btn-purple btn-panel">
                    <i class="btn-icon fa fa-list"></i>
                    <?php echo JrTexto::_("Assessments").' / '.ucfirst(JrTexto::_("Rubrics")); ?>
                    <span class="color-purple-dark border-purple-dark badge">2</span>
                </a>
            </div>
            <div class="col-xs-6 col-lg-4 btn-panel-container panelcont-xs">
                <a href="#" class="btn btn-block btn-yellow btn-panel">
                    <i class="btn-icon fa fa-graduation-cap"></i>
                    <?php echo JrTexto::_('Academic'); ?>
                    <span class="color-yellow-dark border-yellow-dark badge">5</span>
                </a>
            </div>
            <div class="col-xs-6 col-lg-4 btn-panel-container panelcont-xs">
                <a href="#" class="btn btn-block btn-green btn-panel">
                    <i class="btn-icon fa fa-tachometer"></i>
                    <?php echo JrTexto::_('My Progress Tracking'); ?>
                    <span class="color-green-dark border-green-dark badge">1</span>
                </a>
            </div>
            <div class="col-xs-6 col-lg-4 btn-panel-container panelcont-xs">
                <a href="#" class="btn btn-block btn-blue btn-panel">
                    <i class="btn-icon fa fa-book"></i>
                    <?php echo JrTexto::_('Library'); ?>
                    <span class="color-blue-dark border-blue-dark badge">+9</span>
                </a>
            </div>
        </div>
        <div class="col-xs-12 col-sm-5">
            <div class="panel border-orange" id="pnl-logros">
                <div class="panel-heading bg-orange">
                    <h3 class="panel-titulo">
                        <i class="fa fa-trophy"></i>&nbsp;&nbsp;
                        <?php echo JrTexto::_('Achievements'); ?>
                    </h3>
                    <a href="#" class="ver-todo" title="<?php echo JrTexto::_('See all'); ?>"><i class="glyphicon glyphicon-chevron-right"></i></a>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <?php for($i=1; $i<=6; $i++ ) { ?>
                        <div class="col-xs-6 col-sm-6 col-md-4 item-logro">
                            <img src="<?php echo $this->documento->getUrlStatic(); ?>/media/imagenes/trofeo.png" class="img-responsive" alt="trofeo">
                            <div class="nombre text-center"><?php echo JrTexto::_('Achievements').' '.$i; ?></div>
                        </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>