<div class="row" id="breadcrumb"> <div class="col-xs-12">
    <ol class="breadcrumb">
        <li><a href="<?php echo $this->documento->getUrlBase();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_('Home'); ?></a></li>
        <?php foreach ($this->breadcrumb as $b) {
        $enlace = '<li>';
        if(!empty($b['link'])){ $enlace .= '<a href="'.$this->documento->getUrlBase().$b['link'].'">'.ucfirst(JrTexto::_($b['texto'])).'</a>'; }
        else{ $enlace .= ucfirst(JrTexto::_($b['texto'])); }
        $enlace .= '</li>';
        echo $enlace;
        } ?>
    </ol>
</div> </div>

<div class="row" id="curso-inicio"> <div class="col-xs-12">
    <div class="panel">
        <div class="panel-body"><div class="row">
            <div class="col-xs-12 col-sm-3">
                <img src="<?php echo $this->documento->getUrlStatic(); ?>/media/image/N1-U1-A1-20170124040515.jpg" alt="course_cover" class="img-responsive center-block border-red caratula">
            </div>
            <div class="col-xs-12 col-sm-9">
                <div class="col-xs-12 nombre border-bottom border-red"><h2>Curso con un tituttttlo largo III</h2></div>
                <h3 class="col-xs-12 ultima_actividad">
                    <div title="<?php echo JrTexto::_('Level') ?>">Nombre del Nivel</div>
                    <div title="<?php echo JrTexto::_('Unit') ?>">Nombre de la Unidad</div>
                    <div title="<?php echo JrTexto::_('Activity') ?>">Nombre de la Sesión</div>
                </h3>
                <div class="col-xs-12">
                    <div class="progress">
                        <div class="progress-bar progress-bar-warning progress-bar-striped active" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%">
                            <span class="sr-only">60% Complete (warning)</span>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 text-center">
                    <a href="#" class="btn btn-red  btn-lg"><?php echo JrTexto::_('Continue') ?></a>
                </div>
            </div>
            
        </div></div>
    </div>
</div> </div>