<div class="row" id="breadcrumb"> <div class="col-xs-12">
    <ol class="breadcrumb">
        <li><a href="<?php echo $this->documento->getUrlBase();?>"><i class="fa fa-home"></i>&nbsp;<?php echo JrTexto::_('Home'); ?></a></li>
        <?php foreach ($this->breadcrumb as $b) {
        $enlace = '<li>';
        if(!empty($b['link'])){ $enlace .= '<a href="'.$this->documento->getUrlBase().$b['link'].'">'.ucfirst(JrTexto::_($b['texto'])).'</a>'; }
        else{ $enlace .= ucfirst(JrTexto::_($b['texto'])); }
        $enlace .= '</li>';
        echo $enlace;
        } ?>
    </ol>
</div> </div>

<div class="row" id="curso-info">
    <div class="col-xs-12 col-sm-6 col-md-4">
        <div class="panel border-turquoise">
            <div class="panel-body"><div class="row">
                <div class="col-xs-12"><img src="<?php echo $this->documento->getUrlStatic(); ?>/media/usuarios/user_avatar.jpg" alt="foto_docente" class="img-responsive center-block border-turquoise foto_docente"></div>
                <h4 class="col-xs-offset-3 col-xs-6 border-turquoise color-turquoise text-center titulo"><?php echo JrTexto::_('Teacher'); ?></h4>
                <ul class="col-xs-12 list-unstyled informacion">
                    <li class="col-xs-12">
                        <div class="col-xs-2 icono"><i class="fa fa-graduation-cap"></i></div>
                        <div class="col-xs-10 texto_info"><?php echo 'Lic. Eder Figueroa Piscoya'; ?></div>
                    </li>
                    <li class="col-xs-12">
                        <div class="col-xs-2 icono"><i class="fa fa-at"></i></div>
                        <div class="col-xs-10 texto_info"><?php echo 'eder.figueroa@outlook.com'; ?></div>
                    </li>
                    <li class="col-xs-12">
                        <div class="col-xs-2 icono"><i class="fa fa-phone"></i></div>
                        <div class="col-xs-10 texto_info"><?php echo '956 123 456'; ?></div>
                    </li>
                </ul>
            </div></div>
            <div class="panel-footer text-center">
                <a href="<?php echo $this->documento->getUrlBase();?>/docente/perfil" class="btn btn-turquoise"><?php echo JrTexto::_('View profile'); ?></a>
            </div>
        </div>
    </div>

    <div class="col-xs-12 col-sm-6 col-md-4">
        <div class="panel border-turquoise">
            <div class="panel-body"><div class="row">
                <h4 class="col-xs-offset-3 col-xs-6 border-turquoise color-turquoise text-center titulo"><?php echo JrTexto::_('Schedule'); ?></h4>
                <ul class="col-xs-12 list-unstyled informacion">
                    <li class="col-xs-12">
                        <div class="col-xs-2 icono"><i class="fa fa-calendar-check-o"></i></div>
                        <div class="col-xs-10 texto_info"><?php echo 'Lunes:<br> 08:00 a.m. - 10:00 a.m. (Aula 101)'; ?></div>
                    </li>
                    <li class="col-xs-12">
                        <div class="col-xs-2 icono"><i class="fa fa-calendar-check-o"></i></div>
                        <div class="col-xs-10 texto_info"><?php echo 'Miercoles:<br> 10:00 a.m. - 12:30 p.m. (Aula 105)'; ?></div>
                    </li>
                    <li class="col-xs-12">
                        <div class="col-xs-2 icono"><i class="fa fa-calendar-check-o"></i></div>
                        <div class="col-xs-10 texto_info"><?php echo 'Viernes:<br> 08:00 a.m. - 09:45 a.m. (Lab. 003)'; ?></div>
                    </li>
                </ul>
            </div></div>
            <div class="panel-footer text-center">
                <a href="<?php echo $this->documento->getUrlBase().'/curso/horario/?id='.$this->idCurso;?>" class="btn btn-turquoise"><?php echo JrTexto::_('View graphically'); ?></a>
            </div>
        </div>
    </div>
    
    <div class="col-xs-12 col-sm-6 col-md-4">
        <div class="panel border-turquoise">
            <div class="panel-body"><div class="row">
                <h4 class="col-xs-offset-3 col-xs-6 border-turquoise color-turquoise text-center titulo"><?php echo JrTexto::_('Content'); ?></h4>
                <ul class="col-xs-12 list-unstyled informacion">
                    <li class="col-xs-12">
                        <div class="col-xs-2 icono"><i class="fa fa-commenting-o"></i></div>
                        <div class="col-xs-10 texto_info"><?php echo 'Este cursos esta orientado a público en general abarcando desde temas básicos hasta temas más complejos. Este cursos esta orientado a público en general abarcando desde temas básicos hasta temas más complejos. Lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum básicos hasta temas más complejos. Este cursos esta orientado a público en general abarcando desde temas básicos hasta temas más complejos. Lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum. '; ?></div>
                    </li>
                </ul>
            </div></div>
            <div class="panel-footer text-center">
                <a href="<?php echo $this->documento->getUrlBase().'/curso/contenido/?id='.$this->idCurso;?>" class="btn btn-turquoise"><?php echo JrTexto::_('Full content'); ?></a>
            </div>
        </div>
    </div>
</div>