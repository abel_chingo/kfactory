<?php 
defined("RUTA_BASE") or die(); 
$idgui=uniqid();
?><style type="text/css">
  .panel-body{
    border: 1px solid rgba(90, 137, 248, 0.41);
    padding: 10px;
  }
  .title_left, .small{
    height: auto;
    color: #fff;
  }
  .form-group{
    margin-bottom: 0px; 
  }
  .input-group {
    margin-top: 0px;
  }
  select.select-ctrl, .form-control , .input-group-addon{   
    border: 1px solid #4683af;
    margin-bottom: 1ex;
  }
</style>
<div class="form-view" id="ventana_<?php echo $idgui; ?>" >
  <div class="page-title">
    <div class="title_left">
      <ol class="breadcrumb">
        <li><a href="<?php echo JrAplicacion::getJrUrl(array("academico"));?>"><?php echo JrTexto::_("Academic");?></a></li>
        <li><a href="<?php echo JrAplicacion::getJrUrl(array('persona_experiencialaboral'));?>"><?php echo JrTexto::_('Persona_experiencialaboral'); ?></a></li>
        <li class="active"><?php echo JrTexto::_('list')?></li>
      </ol>
    </div>
  </div>
  <div class="clearfix"></div>
<div class="row">
  <div class="col-md-12">
    <div class="panel" >      
      <div class="panel-body">      
         <div class="row">            
                                        
            <div class="col-xs-6 col-sm-4 col-md-3 select-ctrl-wrapper select-azul">
              <select id="fkcbidpersona" name="fkcbidpersona" class="form-control select-ctrl" >
                <option value=""><?php echo JrTexto::_('Seleccione'); ?></option>
                  <?php 
                          if(!empty($this->fkidpersona))
                            foreach ($this->fkidpersona as $fkidpersona) { ?><option value="<?php echo $fkidpersona["dni"]?>" <?php echo $fkidpersona["dni"]==@$frm["idpersona"]?"selected":""; ?> ><?php echo $fkidpersona["nombre"] ?></option><?php } ?>                        
              </select>
            </div>
                                                                                                                    <div class="col-xs-6 col-sm-4 col-md-3">
                      <div class="form-group">    
                          <div class='input-group date datetimepicker1 border1' >
                            <span class="input-group-addon btn"><span class="fa fa-calendar"></span> <?php echo  ucfirst(JrTexto::_("fechade"))?> </span>
                            <input type='text' class="form-control border0" name="datefechade" id="datefechade" />           
                          </div>
                      </div>
                    </div>
                                                  <div class="col-xs-6 col-sm-4 col-md-3">
                      <div class="form-group">    
                          <div class='input-group date datetimepicker1 border1' >
                            <span class="input-group-addon btn"><span class="fa fa-calendar"></span> <?php echo  ucfirst(JrTexto::_("fechahasta"))?> </span>
                            <input type='text' class="form-control border0" name="datefechahasta" id="datefechahasta" />           
                          </div>
                      </div>
                    </div>
                                                        
                    <div class="col-xs-6 col-sm-4 col-md-3 select-ctrl-wrapper select-azul" >
                      <select name="cbactualmente" id="cbactualmente" class="form-control select-ctrl">
                          <option value=""><?php echo ucfirst(JrTexto::_("All state"))?></option>
                          <option value="1"><?php echo ucfirst(JrTexto::_("Active"))?></option>
                          <option value="0"><?php echo ucfirst(JrTexto::_("Inactive"))?></option>                              
                      </select>
                    </div>
                                                          
                    <div class="col-xs-6 col-sm-4 col-md-3 select-ctrl-wrapper select-azul" >
                      <select name="cbmostrar" id="cbmostrar" class="form-control select-ctrl">
                          <option value=""><?php echo ucfirst(JrTexto::_("All state"))?></option>
                          <option value="1"><?php echo ucfirst(JrTexto::_("Active"))?></option>
                          <option value="0"><?php echo ucfirst(JrTexto::_("Inactive"))?></option>                              
                      </select>
                    </div>
                                
            <div class="col-xs-6 col-sm-6 col-md-6">
              <div class="form-group">
                <div class="input-group">
                  <input type="text" name="texto" id="texto" class="form-control border0" placeholder="<?php echo  ucfirst(JrTexto::_("text to search"))?>">
                  <span class="input-group-addon btn btnbuscar"><?php echo  ucfirst(JrTexto::_("Search"))?> <i class="fa fa-search"></i></span>  
                                    </div>
              </div>
            </div>
            <div class="col-xs-6 col-sm-3 col-md-3 text-center">
               <a class="btn btn-success btnvermodal" data-modal="si" href="<?php echo JrAplicacion::getJrUrl(array("Persona_experiencialaboral", "agregar"));?>" data-titulo="<?php echo JrTexto::_("Persona_experiencialaboral").' - '.JrTexto::_("add"); ?>"><i class="fa fa-plus"></i> <?php echo JrTexto::_('add')?></a>
              </div>
            </div>
          </div>
      </div>
    </div>
	  <div class="col-md-12 col-sm-12 col-xs-12">
	   <div class="panel">         
         <div class="panel-body">
            <table class="table table-striped table-responsive">
              <thead>
                <tr class="headings">
                  <th>#</th>
                  <th><?php echo JrTexto::_("Onal")."(".JrTexto::_("Nombre").")"; ?></th>
                    <th><?php echo JrTexto::_("Empresa") ;?></th>
                    <th><?php echo JrTexto::_("Rubro") ;?></th>
                    <th><?php echo JrTexto::_("Area") ;?></th>
                    <th><?php echo JrTexto::_("Cargo") ;?></th>
                    <th><?php echo JrTexto::_("Funciones") ;?></th>
                    <th><?php echo JrTexto::_("Fechade") ;?></th>
                    <th><?php echo JrTexto::_("Fechahasta") ;?></th>
                    <th><?php echo JrTexto::_("Actualmente") ;?></th>
                    <th><?php echo JrTexto::_("Mostrar") ;?></th>
                    <th class="sorting_disabled"><span class="nobr"><?php echo JrTexto::_('Actions');?></span></th>
                </tr>
              </thead>
              <tbody>
                        </tbody>
        </table>
        </div>
        </div>
      </div>
    </div>
</div>

<script type="text/javascript">
var tabledatos5976235b13fb0='';
function refreshdatos5976235b13fb0(){
    tabledatos5976235b13fb0.ajax.reload();
}
$(document).ready(function(){  
  var estados5976235b13fb0={'1':'<?php echo JrTexto::_("Active") ?>','0':'<?php echo JrTexto::_("Inactive") ?>','C':'<?php echo JrTexto::_("Cancelled") ?>'}
  var tituloedit5976235b13fb0='<?php echo ucfirst(JrTexto::_("persona_experiencialaboral"))." - ".JrTexto::_("edit"); ?>';
  var draw5976235b13fb0=0;

  
  $('#fkcbidpersona').change(function(ev){
    refreshdatos5976235b13fb0();
  });
  $('#datefechade').change(function(ev){
    refreshdatos5976235b13fb0();
  });
  $('#datefechahasta').change(function(ev){
    refreshdatos5976235b13fb0();
  });
  $('#cbactualmente').change(function(ev){
    refreshdatos5976235b13fb0();
  });
  $('#cbmostrar').change(function(ev){
    refreshdatos5976235b13fb0();
  });
  $('.btnbuscar').click(function(ev){
    refreshdatos5976235b13fb0();
  });
  tabledatos5976235b13fb0=$('#ventana_<?php echo $idgui; ?> .table').DataTable(
    { "searching": false,
      "processing": false,
      //"serverSide": true,
      "columns" : [
        {'data': '#'},
        
        {'data': '<?php echo JrTexto::_("Nombre") ;?>'},
            {'data': '<?php echo JrTexto::_("Empresa") ;?>'},
            {'data': '<?php echo JrTexto::_("Rubro") ;?>'},
            {'data': '<?php echo JrTexto::_("Area") ;?>'},
            {'data': '<?php echo JrTexto::_("Cargo") ;?>'},
            {'data': '<?php echo JrTexto::_("Funciones") ;?>'},
            {'data': '<?php echo JrTexto::_("Fechade") ;?>'},
            {'data': '<?php echo JrTexto::_("Fechahasta") ;?>'},
            {'data': '<?php echo JrTexto::_("Actualmente") ;?>'},
            {'data': '<?php echo JrTexto::_("Mostrar") ;?>'},
            
        {'data': '<?php echo JrTexto::_("Actions") ;?>'},
      ],
      "ajax":{
        url:_sysUrlBase_+'/persona_experiencialaboral/buscarjson/?json=true',
        type: "post",                
        data:function(d){
            d.json=true                   
             d.idpersona=$('#fkcbidpersona').val(),
             d.fechade=$('#datefechade').val(),
             d.fechahasta=$('#datefechahasta').val(),
             d.actualmente=$('#cbactualmente').val(),
             d.mostrar=$('#cbmostrar').val(),
             //d.texto=$('#texto').val(),
                        
            draw5976235b13fb0=d.draw;
           // console.log(d);
        },
        "dataSrc":function(json){
          var data=json.data;             
          json.draw = draw5976235b13fb0;
          json.recordsTotal = json.data.length;
          json.recordsFiltered = json.data.length;
          var datainfo = new Array();
          for(var i=0;i< data.length; i++){                   
            datainfo.push({
              '#':(i+1),
              '<?php echo JrTexto::_("nombre") ;?>': data[i].nombre,
                    '<?php echo JrTexto::_("Empresa") ;?>': data[i].empresa,
                '<?php echo JrTexto::_("Rubro") ;?>': data[i].rubro,
                '<?php echo JrTexto::_("Area") ;?>': data[i].area,
              '<?php echo JrTexto::_("Cargo") ;?>': data[i].cargo,
              '<?php echo JrTexto::_("Funciones") ;?>': data[i].funciones,
                '<?php echo JrTexto::_("Fechade") ;?>': data[i].fechade,
              '<?php echo JrTexto::_("Fechahasta") ;?>': data[i].fechahasta,
              '<?php echo JrTexto::_("Actualmente") ;?>': '<a href="javascript:;"  class="btn-chkoption" campo="actualmente"  data-id="'+data[i].idexperiencia+'"> <i class="fa fa'+(data[i].actualmente=='1'?'-check':'')+'-circle-o fa-lg"></i> '+estados5976235b13fb0[data[i].actualmente]+'</a>',
              '<?php echo JrTexto::_("Mostrar") ;?>': '<a href="javascript:;"  class="btn-chkoption" campo="mostrar"  data-id="'+data[i].idexperiencia+'"> <i class="fa fa'+(data[i].mostrar=='1'?'-check':'')+'-circle-o fa-lg"></i> '+estados5976235b13fb0[data[i].mostrar]+'</a>',
              
              '<?php echo JrTexto::_("Actions") ;?>'  :'<a class="btn btn-xs btnvermodal" data-modal="si" href="'+_sysUrlBase_+'/persona_experiencialaboral/editar/?id='+data[i].idexperiencia+'" data-titulo="'+tituloedit5976235b13fb0+'"><i class="fa fa-edit"></i></a><a class="btn-eliminar btn btn-xs" href="javascript:;" data-id="'+data[i].idexperiencia+'" ><i class="fa fa-trash-o"></i>'
            });
          }
          return datainfo }, error: function(d){console.log(d)}
      }
      <?php echo $this->documento->getIdioma()!='EN'?(',"language": { "url": "'.$this->documento->getUrlStatic().'/libs/datatable1.10/idiomas/'.$this->documento->getIdioma().'.json"}'):''?>
    });

  $('#ventana_<?php echo $idgui; ?>').on('click','.btn-chkoption',function(){     
      var id=$(this).attr('data-id');
      var campo=$(this).attr('campo');
      var data=0;
      if($("i",this).hasClass('fa-circle-o')) data=1;
      $.confirm({
        title: '<?php echo JrTexto::_('Confirm action');?>',
        content: '<?php echo JrTexto::_('It is sure to change the status of this record ?'); ?>',
        confirmButton: '<?php echo JrTexto::_('Accept');?>',
        cancelButton: '<?php echo JrTexto::_('Cancel');?>',
        confirmButtonClass: 'btn-success',
        cancelButtonClass: 'btn-danger',
        closeIcon: true,
        confirm: function(){
          var res = xajax__('', 'persona_experiencialaboral', 'setCampo', id,campo,data);
          if(res) tabledatos5976235b13fb0.ajax.reload();
        }
      });
  });

  $('#ventana_<?php echo $idgui; ?>').on('click','.btnvermodal',function(e){
    e.preventDefault();
    e.stopPropagation();
    var enmodal=$(this).attr('data-modal')||'no';
    var fcall=$(this).attr('data-fcall')||'refreshdatos5976235b13fb0';
    var url=$(this).attr('href')
    if(url.indexOf('?')!=-1) url+='&fcall='+fcall;
    else url+='?fcall='+fcall;
    var ventana=$(this).data('ventana')||'Persona_experiencialaboral';
    var claseid=ventana+'_<?php echo $idgui; ?>';
    var titulo=$(this).attr('data-titulo')||'';
    titulo=titulo.toString().replace('<br>',' ');     
    if(enmodal=='no'){
      return redir(url);          
    }
    url+='&plt=modal';
    openModal('lg',titulo,url,ventana,claseid); 
  });
  
  $('#ventana_<?php echo $idgui; ?> .table').on('click','.btn-eliminar',function(){
     var id=$(this).attr('data-id');
     $.confirm({
      title: '<?php echo JrTexto::_('Confirm action');?>',
      content: '<?php echo JrTexto::_('It is sure to delete this record ?'); ?>',
      confirmButton: '<?php echo JrTexto::_('Accept');?>',
      cancelButton: '<?php echo JrTexto::_('Cancel');?>',
      confirmButtonClass: 'btn-success',
      cancelButtonClass: 'btn-danger',
      closeIcon: true,
      confirm: function(){             
        var res = xajax__('', 'persona_experiencialaboral', 'eliminar', id);
        if(res) tabledatos5976235b13fb0.ajax.reload();
      }
    }); 
  });
});
</script>