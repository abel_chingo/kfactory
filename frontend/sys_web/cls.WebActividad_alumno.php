<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		12-04-2017 
 * @copyright	Copyright (C) 12-04-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegActividad_alumno', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegMetodologia_habilidad', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegGrupo_matricula', RUTA_BASE, 'sys_negocio');
class WebActividad_alumno extends JrWeb
{
	private $oNegActividad_alumno;
    protected $oNegMetodologia;
    private $oNegGrupo_matricula;
	public function __construct()
	{
		parent::__construct();
		$this->oNegActividad_alumno = new NegActividad_alumno;
        $this->oNegMetodologia = new NegMetodologia_habilidad;
        $this->oNegGrupo_matricula = new NegGrupo_matricula;
	}
	public function defecto(){
		return $this->listado();
	}

	public function listado()
	{
		try{
			global $aplicacion;			
			if(!NegSesion::tiene_acceso('Actividad_alumno', 'list')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
            $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
            $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
			//$this->documento->script(null, ConfigSitio::get('tema_general') . '/js/datatables/js/jquery.dataTables.js');
			
			
			$this->datos=$this->oNegActividad_alumno->buscar();

						$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Actividad_alumno'), true);
			$this->esquema = 'actividad_alumno-list';			
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}


	public function agregar()
	{
		try {
			global $aplicacion;			
			if(!NegSesion::tiene_acceso('Actividad_alumno', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->frmaccion='Nuevo';
			$this->documento->setTitulo(JrTexto::_('Actividad_alumno').' /'.JrTexto::_('New'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function editar()
	{
		try {
			global $aplicacion;
			
			if(!NegSesion::tiene_acceso('Actividad_alumno', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->frmaccion='Editar';
			$this->oNegActividad_alumno->idactalumno = @$_GET['id'];
			$this->datos = $this->oNegActividad_alumno->dataActividad_alumno;
			$this->pk=@$_GET['id'];
			$this->documento->setTitulo(JrTexto::_('Actividad_alumno').' /'.JrTexto::_('Edit'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function ver(){
		try{
			global $aplicacion;						
			$this->oNegActividad_alumno->idactalumno = @$_GET['id'];
			$this->datos = $this->oNegActividad_alumno->dataActividad_alumno;
									$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Actividad_alumno').' /'.JrTexto::_('see'), true);
			$this->esquema = 'actividad_alumno-see';
			$this->documento->plantilla = $tpl;
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	private function form()
	{
		try {
			global $aplicacion;	
			
			//$this->documento->script(null, 'http://tinymce.cachefly.net/4.2/tinymce.min.js');			
			$this->esquema = 'actividad_alumno-frm';
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function ajax_agregar()
	{
		$this->documento->plantilla = 'returnjson';
		try {
			global $aplicacion;			
			//if(!NegSesion::tiene_acceso('Actividad_alumno', 'add')) {
			//	throw new Exception(JrTexto::_('Restricted access').'!!');
			//}
			$usuarioAct = NegSesion::getUsuario();
		/*
		* Flata chequear si es Insert o Edit
		*/
			$this->oNegActividad_alumno->iddetalleactividad = (int) $_POST['iddetalle_actividad'];
			$this->oNegActividad_alumno->idalumno = $usuarioAct['dni'];
			$this->oNegActividad_alumno->fecha = date('Y-m-d');
			$this->oNegActividad_alumno->porcentajeprogreso = (float) $_POST['progreso'];
			$this->oNegActividad_alumno->habilidades = $_POST['habilidades'];
			$this->oNegActividad_alumno->estado = $_POST['estado'];

			$idactalumno = $this->oNegActividad_alumno->agregar();
			$data=array('code'=>'ok','data'=>['idactalumno'=>$idactalumno]);
            echo json_encode($data);
            return parent::getEsquema();
		} catch(Exception $e) {
			//return $aplicacion->error(JrTexto::_($e->getMessage()));
			$data=array('code'=>'Error','mensaje'=>JrTexto::_($e->getMessage()));
            echo json_encode($data);
		}
	}

	public function progresoxalumno($idalumno=null)
	{
		$this->documento->plantilla = 'returnjson';
		try{
            global $aplicacion;

            if(empty($_POST["id"]) && $idalumno==null) { 
            	throw new Exception(JrTexto::_('Error in filtering')); 
            }
            $filtros=array();
            $filtros["idalumno"]=($idalumno==null)?$_POST["id"]:$idalumno;
            $actividad_alumno=$this->oNegActividad_alumno->buscar($filtros);
            $habilidades=$this->oNegMetodologia->buscar(array('tipo'=>'H'));
            $arrHabilidades = [];
            $arrPorcentajexHab = [];
            foreach ($habilidades as $hab) { 
            	$idHab = $hab['idmetodologia'];
            	$arrHabilidades[] = $idHab;
            	$arrPorcentajexHab[$idHab] = [];
            	$arrPorcentajexHab[$idHab] = [
            		'suma_porcentajes' => 0.0,
    				'total_elem' => 0,
    				'promedio_porcentaje' => 0.0,
            	];
            }
            foreach ($actividad_alumno as $act_alum) {
            	$arrHab_Act = explode ('|', $act_alum['habilidades']);
            	foreach ($arrHab_Act as $h) {
	            	if(in_array($h, $arrHabilidades)){
	            		$arrPorcentajexHab[$h]['suma_porcentajes']+=$act_alum['porcentajeprogreso'];
	            		$arrPorcentajexHab[$h]['total_elem']+=1 ;
	            		$arrPorcentajexHab[$h]['promedio_porcentaje']=$arrPorcentajexHab[$h]['suma_porcentajes']/$arrPorcentajexHab[$h]['total_elem'] ;
	            	}
            	}
            }
            if($idalumno==null){
	            $data=array('code'=>'ok','data'=>$arrPorcentajexHab);
	            echo json_encode($data);
            } else {
            	return $arrPorcentajexHab;
            }
            return parent::getEsquema();
        }catch(Exception $ex){
            $data=array('code'=>'Error','mensaje'=>JrTexto::_($ex->getMessage()));
            echo json_encode($data);
        }
	}

	public function progresoxgrupo()
	{
		$this->documento->plantilla = 'returnjson';
		try {
			global $aplicacion;
			if(empty($_POST["id"])) { 
            	throw new Exception(JrTexto::_('Error in filtering')); 
            }
			$filtros = [];
			$arrPtjesProm = [];
            $filtros['idgrupo']=$_POST['id'];
            $alum_grupo = $this->oNegGrupo_matricula->buscar($filtros);
            foreach ($alum_grupo as $al) {
            	$arrPtjes = $this->progresoxalumno($al['idalumno']);
            	foreach ($arrPtjes as $idHab => $array) {
            		if(!isset($arrPtjesProm[$idHab])){
            			$arrPtjesProm[$idHab] = [];
            			$arrPtjesProm[$idHab] = [
            				'suma_porcentajes' => 0.0,
		    				'total_elem' => 0,
		    				'promedio_porcentaje' => 0.0,
            			];
            		}
            		$arrPtjesProm[$idHab]['suma_porcentajes'] += $array['promedio_porcentaje'];
            		$arrPtjesProm[$idHab]['total_elem'] += 1;
            		$arrPtjesProm[$idHab]['promedio_porcentaje'] = $arrPtjesProm[$idHab]['suma_porcentajes']/$arrPtjesProm[$idHab]['total_elem'];
            	}
            }

            $data=array('code'=>'ok','data'=>$arrPtjesProm);
	        echo json_encode($data);
	        return parent::getEsquema();
		} catch (Exception $ex) {
			
		}
	}

	// ========================== Funciones xajax ========================== //
/*
	public function xSaveActividad_alumno(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$frm = $args[0];
				
				if(!empty($frm['pk'])) {
					$this->oNegActividad_alumno-> = $frm['pk'];
				}
				
				$this->oNegActividad_alumno->__set('idactalumno',@$frm["txtIdactalumno"]);
					$this->oNegActividad_alumno->__set('idactividad',@$frm["txtIdactividad"]);
					$this->oNegActividad_alumno->__set('idalumno',@$frm["txtIdalumno"]);
					$this->oNegActividad_alumno->__set('fecha',@$frm["txtFecha"]);
					$this->oNegActividad_alumno->__set('estado',@$frm["txtEstado"]);
					
				   if(@$frm["accion"]=="Nuevo"){
									    $res=$this->oNegActividad_alumno->agregar();
					}else{
									    $res=$this->oNegActividad_alumno->editar();
				    }
					if(!empty($res)) $oRespAjax->setReturnValue($this->oNegActividad_alumno->);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Failed to save registry')),'warning');
					$oRespAjax->setReturnValue(false);
				}
							
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}

	public function xGetxIDActividad_alumno(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegActividad_alumno->__set('', $pk);
				$this->datos = $this->oNegActividad_alumno->dataActividad_alumno;
				$res=$this->oNegActividad_alumno->getXid();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')),  $this->pasarHtml(JrTexto::_('Get record')), 'warning');
				$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}
	public function xEliminar(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegActividad_alumno->__set('', $pk);
				$res=$this->oNegActividad_alumno->eliminar();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Error').' '.JrTexto::_('Delete Record')), 'warning');
					$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}
*/
	     
}