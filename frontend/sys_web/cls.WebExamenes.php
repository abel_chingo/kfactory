 <?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		03-03-2017 
 * @copyright	Copyright (C) 03-03-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegExamenes', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegNiveles', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegExamen_tipo', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegExamenes_preguntas', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegMetodologia_habilidad', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegExamen_alumno', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegAlumno', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegGrupo_matricula', RUTA_BASE, 'sys_negocio');

JrCargador::clase('sys_negocio::NegTarea_archivos', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegTarea', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegTarea_asignacion', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegTarea_asignacion_alumno', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegTarea_respuesta', RUTA_BASE, 'sys_negocio');

class WebExamenes extends JrWeb
{
	protected $oNegExamenes;
	protected $oNegNiveles;
	protected $oNegExamen_tipo;
	protected $oNegExamenes_preguntas;
    protected $oNegMetodologia;
    protected $oNegExamen_alumno;
    protected $oNegAlumno;
    private $oNegGrupo_matricula;

    private $oNegTarea_archivos;
    private $oNegTarea;  
    private $oNegTarea_asignacion;
    private $oNegTarea_asignacion_alumno;
    private $oNegTarea_respuesta;

	public function __construct()
	{
		parent::__construct();
		$this->oNegNiveles = new NegNiveles;	
		$this->oNegExamenes = new NegExamenes;
		$this->oNegExamen_tipo = new NegExamen_tipo;
		$this->oNegpreguntas = new NegExamenes_preguntas;
        $this->oNegMetodologia = new NegMetodologia_habilidad;
        $this->oNegExamen_alumno = new NegExamen_alumno;
        $this->oNegAlumno = new NegAlumno;
        $this->oNegGrupo_matricula = new NegGrupo_matricula;

        $this->oNegTarea_archivos = new NegTarea_archivos;
        $this->oNegTarea = new NegTarea;    
        $this->oNegTarea_asignacion = new NegTarea_asignacion;
        $this->oNegTarea_asignacion_alumno = new NegTarea_asignacion_alumno;
        $this->oNegTarea_respuesta = new NegTarea_respuesta;
	}

	public function defecto(){
		return $this->publicados();
	}

	public function publicados(){
		global $aplicacion;
		$this->documento->setTitulo(JrTexto::_('Exams'), true);
		$idnivel_=!empty(JrPeticion::getPeticion(2))?JrPeticion::getPeticion(2):0;
		$this->niveles=$this->oNegNiveles->buscar(array('tipo'=>'N'));
        $_idnivel=!empty($this->niveles[0]["idnivel"])?$this->niveles[0]["idnivel"]:0;
        $this->idnivel=!empty($idnivel_)?$idnivel_:$_idnivel;

        $_idunidad=!empty(JrPeticion::getPeticion(3))?JrPeticion::getPeticion(3):0;
        $this->unidades=$this->oNegNiveles->buscar(array('tipo'=>'U','idpadre'=>$this->idnivel));
        $idunidad_=!empty($this->unidades[0]["idnivel"])?$this->unidades[0]["idnivel"]:0;
        $this->idunidad=!empty($_idunidad)?$_idunidad:($idunidad_);

        $_idactividad=!empty(JrPeticion::getPeticion(4))?JrPeticion::getPeticion(4):0;
        $this->actividades=$this->oNegNiveles->buscar(array('tipo'=>'L','idpadre'=>$this->idunidad));
       	$idactividad_=!empty($this->actividades[0]["idnivel"])?$this->actividades[0]["idnivel"]:0;
       	$this->idactividad=!empty($_idactividad)?$_idactividad:($idactividad_);
       	
       	$_idtipo=!empty(JrPeticion::getPeticion(5))?JrPeticion::getPeticion(5):0;
       	$this->tipoexamen=$this->oNegExamen_tipo->buscar(array('estado'=>1));
       	$idtipo_=!empty($this->tipoexamen[0]["idtipo"])?$this->tipoexamen[0]["idtipo"]:0;
       	$this->tipo=!empty($_idtipo)?$_idtipo:($idtipo_);
		$usuarioAct = NegSesion::getUsuario();
		$rol=$usuarioAct["rol"];
		
		$filtros["idnivel"]=$this->idnivel;
		$filtros["idunidad"]=$this->idunidad;
		$filtros["idactividad"]=$this->idactividad;
		$filtros["tipo"]=$this->tipo;

		if($rol=='Alumno'){			
			$this->documento->plantilla = 'examenes/general';
			$filtros["estado"]=1;			
			$this->esquema = 'examenes/alu_publicados';
		}else{
			$this->documento->plantilla = 'examenes/inicio';
			$this->examenes=$this->oNegExamenes->buscar($filtros);
        	$this->esquema = 'examenes/publicados';
		}
		@NegSesion::set('idexamencur', 0, '_examen_');
        return parent::getEsquema();
	}

	public function publicadosjson(){
		try{
			global $aplicacion;
			$this->documento->setTitulo(JrTexto::_('Exams'), true);
			$filtros["idnivel"]=!empty($_POST["nivel"])?$_POST["nivel"]:0;			
	        $filtros["idunidad"]=!empty($_POST["unidad"])?$_POST["unidad"]:0;
	        $filtros["idactividad"]=!empty($_POST["actividad"])?$_POST["actividad"]:0; 
	       	$filtros["tipo"]=!empty($_POST["tipo"])?$_POST["tipo"]:0;
			$usuarioAct = NegSesion::getUsuario();
			$rol=$usuarioAct["rol"];
			$this->documento->plantilla = 'returnjson';		
			if($rol=='alumno')
				//$this->esquema = 'alumno/examenes';
				$examenes=$this->oNegExamenes->buscar($filtros);
			else{
				$filtro["idpersonal"]=$usuarioAct["dni"];
				$examenes=$this->oNegExamenes->buscar($filtros);        	
			}
			$data=array('code'=>'ok','data'=>$examenes);
	        echo json_encode($data);
	        return parent::getEsquema();
        }catch(Exception $ex){
            $data=array('code'=>'Error','mensaje'=>JrTexto::_($ex->getMessage()));
            echo json_encode($data);
        }
	}

	public function ver()
	{
		try {
			global $aplicacion;
            $this->documento->script('jquery.maskedinput.min', '/tema/js/');

			$usuarioAct = NegSesion::getUsuario();
			$rol=$usuarioAct["rol"];
			$filtros=array();
			$idexamen=!empty(NegSesion::get('idexamencur','_examen_'))?NegSesion::get('idexamencur','_examen_'):-1;	
			$this->idexamen=!empty($_GET["idexamen"])?$_GET["idexamen"]:$idexamen;//nuevo o edicion
			$filtros["idexamen"]=$this->idexamen;
			$this->documento->setTitulo(JrTexto::_('Exams').' /'.JrTexto::_('See'), true);
			if($rol=='alumno'){
				$filtros["estado"]=1;
				$this->esquema = 'alumno/examenes';
			}else{
				$filtros["idpersonal"]=$usuarioAct["dni"];
	        	$this->esquema = 'examenes/setting';
			}
			$examen=$this->oNegExamenes->buscar($filtros);
			if(!empty($examen)){
				$this->examen=$examen[0];
				$nivel=$this->examen["idnivel"];
				$unidad=$this->examen["idunidad"];
				$actividad=$this->examen["idactividad"];
				NegSesion::set('idexamencur', $this->examen["idexamen"], '_examen_');
			}else{
				$this->examen=null;
				NegSesion::set('idexamencur', 0, '_examen_');
			}

			$idnivel_=!empty($nivel)?$nivel:(!empty(JrPeticion::getPeticion(2))?JrPeticion::getPeticion(2):0);
			$this->niveles=$this->oNegNiveles->buscar(array('tipo'=>'N'));
	        $_idnivel=!empty($this->niveles[0]["idnivel"])?$this->niveles[0]["idnivel"]:0;
	        $this->idnivel=!empty($idnivel_)?$idnivel_:$_idnivel;

	        $_idunidad=!empty($unidad)?$unidad:(!empty(JrPeticion::getPeticion(3))?JrPeticion::getPeticion(3):0);
	        $this->unidades=$this->oNegNiveles->buscar(array('tipo'=>'U','idpadre'=>$this->idnivel));
	        $idunidad_=!empty($this->unidades[0]["idnivel"])?$this->unidades[0]["idnivel"]:0;
	        $this->idunidad=!empty($_idunidad)?$_idunidad:($idunidad_);

	        $_idactividad=!empty($actividad)?$actividad:(!empty(JrPeticion::getPeticion(4))?JrPeticion::getPeticion(4):0);
	        $this->actividades=$this->oNegNiveles->buscar(array('tipo'=>'L','idpadre'=>$this->idunidad));
	       	$idactividad_=!empty($this->actividades[0]["idnivel"])?$this->actividades[0]["idnivel"]:0;
	       	$this->idactividad=!empty($_idactividad)?$_idactividad:($idactividad_);
	       	$this->tipoexamen=$this->oNegExamen_tipo->buscar(array('estado'=>1));
	       	$this->documento->plantilla = 'examenes/inicio';
	        return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function preguntas()
	{
		try {
			global $aplicacion;	
			$this->documento->setTitulo(JrTexto::_('Exams').' /'.JrTexto::_('preguntas'), true);
			$this->documento->stylesheet('jquery-ui.min', '/tema/css/');
            $this->documento->stylesheet('datetimepicker.min', '/libs/datetimepicker/css/');
            $this->documento->script('jquery.maskedinput.min', '/tema/js/');
            $this->documento->script('cronometro', '/libs/chingo/');
            $this->documento->script('jquery-ui.min', '/tema/js/');
            $this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery.md5', '/tema/js/');
            $this->documento->script('jquery.maskedinput.min', '/tema/js/');

            $this->documento->script('multiplantilla', '/js/examen/');
            $this->documento->script('true_false', '/js/examen/');
            $this->documento->script('order_simple', '/js/examen/');
            $this->documento->script('order_paragraph', '/js/examen/');
            $this->documento->script('join', '/js/examen/');
            $this->documento->script('tag_image', '/js/examen/');
            $this->documento->script('editactividad','/js/new/');
            $this->documento->script('actividad_completar','/js/new/');
            $this->documento->script('funciones','/tema/js/');

			$idnivel_=!empty(JrPeticion::getPeticion(2))?JrPeticion::getPeticion(2):0;
			$this->niveles=$this->oNegNiveles->buscar(array('tipo'=>'N'));
	        $_idnivel=!empty($this->niveles[0]["idnivel"])?$this->niveles[0]["idnivel"]:0;
	        $this->idnivel=!empty($idnivel_)?$idnivel_:$_idnivel;

	        $_idunidad=!empty(JrPeticion::getPeticion(3))?JrPeticion::getPeticion(3):0;
	        $this->unidades=$this->oNegNiveles->buscar(array('tipo'=>'U','idpadre'=>$this->idnivel));
	        $idunidad_=!empty($this->unidades[0]["idnivel"])?$this->unidades[0]["idnivel"]:0;
	        $this->idunidad=!empty($_idunidad)?$_idunidad:($idunidad_);

	        $_idactividad=!empty(JrPeticion::getPeticion(4))?JrPeticion::getPeticion(4):0;
	        $this->actividades=$this->oNegNiveles->buscar(array('tipo'=>'L','idpadre'=>$this->idunidad));
	       	$idactividad_=!empty($this->actividades[0]["idnivel"])?$this->actividades[0]["idnivel"]:0;
	       	$this->idactividad=!empty($_idactividad)?$_idactividad:($idactividad_);
	       	$this->tipoexamen=$this->oNegExamen_tipo->buscar(array('estado'=>1));

	       	$usuarioAct = NegSesion::getUsuario();
			$rol=$usuarioAct["rol"];

            $_idexamen=!empty(NegSesion::get('idexamencur','_examen_'))?NegSesion::get('idexamencur','_examen_'):-1;
	       	$this->idexamen=!empty($_GET["idexamen"])?$_GET["idexamen"]:$_idexamen;
	       	NegSesion::set('idexamencur', $this->idexamen, '_examen_');
	       	$filtros["idexamen"]=$this->idexamen;
	       	if($rol=='alumno'){
				$filtros["estado"]=1;
				$this->esquema = 'alumno/examenes';
			}else{
				$this->esquema = 'examenes/question';
				$filtros["idpersonal"]=$usuarioAct["dni"];				
			}
	       	$exam=$this->oNegExamenes->buscar($filtros);
	       	if(!empty($exam)){
				$this->examen=$exam[0];
				$filtrosp["idexamen"]=$this->idexamen;
        		$filtrosp["orden"]=!empty($this->examen["aleatorio"])?$this->examen["aleatorio"]:0; //0 asc, 1 aleatorio;
        		$this->preguntas=$this->oNegpreguntas->mostrarPreguntas($filtrosp);
			} else {
				throw new Exception(JrTexto::_('Exam does not exist').'!');
			}
			$this->habilidades=$this->oNegMetodologia->buscar(array('tipo'=>'H'));
			$this->documento->plantilla = 'examenes/inicio';			
	        return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function preview()
	{
		try {
			global $aplicacion;
			$this->documento->stylesheet('datetimepicker.min', '/libs/datetimepicker/css/');
            $this->documento->script('jquery.maskedinput.min', '/tema/js/');
            $this->documento->script('cronometro', '/libs/chingo/');
            $this->documento->script('mitimer', '/libs/chingo/');
            $this->documento->script('jquery-ui.min', '/tema/js/');
            $this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery.md5', '/tema/js/');
            $this->documento->stylesheet('circletimer', '/libs/circletimer/');
            $this->documento->script('circletimer.min', '/libs/circletimer/');
            $this->documento->script('multiplantilla', '/js/examen/');
            $this->documento->script('true_false', '/js/examen/');
            $this->documento->script('order_simple', '/js/examen/');
            $this->documento->script('order_paragraph', '/js/examen/');
            $this->documento->script('join', '/js/examen/');
            $this->documento->script('tag_image', '/js/examen/');
            $this->documento->script('editactividad','/js/new/');
            $this->documento->script('actividad_completar','/js/new/');
            $this->documento->script('funciones','/tema/js/');
            $this->documento->script('progressbar.min', '/libs/graficos/progressbar/');
			$this->documento->script('circle', '/libs/graficos/progressbar/');
			$usuarioAct = NegSesion::getUsuario();
			$rol=$usuarioAct["rol"];
			$_idexamen=!empty(NegSesion::get('idexamencur','_examen_'))?NegSesion::get('idexamencur','_examen_'):0;
		    $this->idexamen=!empty($_GET["idexamen"])?$_GET["idexamen"]:$_idexamen;
		   	NegSesion::set('idexamencur', $this->idexamen, '_examen_');
		   	$filtros["idexamen"]=$this->idexamen;
		   	$this->documento->plantilla = 'examenes/inicio';
		   	if($rol=='alumno'){
				$filtros["estado"]=1;
				$this->esquema = 'alumno/examenes';
			}else{
				$filtros["idpersonal"]=$usuarioAct["dni"];
		    	$this->esquema = 'examenes/preview';
			}
			$exam=$this->oNegExamenes->buscar($filtros);
		   	if(!empty($exam)){
				$this->examen=$exam[0];
				$filtrosp["idexamen"]=$this->idexamen;
				$filtrosp["orden"]=!empty($this->examen["aleatorio"])?$this->examen["aleatorio"]:0; //0 asc, 1 aleatorio;
				$this->preguntas=$this->oNegpreguntas->mostrarPreguntas($filtrosp);
				$this->exanivel=$this->oNegNiveles->buscar(array('tipo'=>'N','idnivel'=>$this->examen["idnivel"]));
				$this->exanunidad=$this->oNegNiveles->buscar(array('tipo'=>'U','idnivel'=>$this->examen["idunidad"]));
				$this->exaactividad=$this->oNegNiveles->buscar(array('tipo'=>'L','idnivel'=>$this->examen["idactividad"]));
				$this->exatype=	$this->tipoexamen=$this->oNegExamen_tipo->buscar(array('idtipo'=>$this->examen["tipo"]));
			}else{
				throw new Exception(JrTexto::_('Exam does not exist').'!');
			}
			$this->habilidades=$this->oNegMetodologia->buscar(array('tipo'=>'H'));
            $this->showBtnPrint = true;
			return parent::getEsquema();
		}catch(Exception $e){
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function reportes()
    {
        try {
            global $aplicacion;
            $this->documento->script('progressbar.min', '/libs/graficos/progressbar/');
            $this->documento->script('circle', '/libs/graficos/progressbar/');

            $this->documento->setTitulo(JrTexto::_('Exams').' /'.JrTexto::_('reports'), true);
            $usuarioAct = NegSesion::getUsuario();
            $rol=$usuarioAct["rol"];
            $_idexamen=!empty(NegSesion::get('idexamencur','_examen_'))?NegSesion::get('idexamencur','_examen_'):0;
            $this->idexamen=!empty($_GET["idexamen"])?$_GET["idexamen"]:0;
            if(empty($this->idexamen))$this->idexamen=$_idexamen;
            $filtros["idexamen"]=$this->idexamen; 
            $filtros["idpersonal"]=$usuarioAct["dni"];
            $this->esquema = 'examenes/reports';
            $this->examen=$this->oNegExamenes->buscar($filtros);
            $this->alumnos=$this->oNegAlumno->buscar();
            $this->habilidades=$this->oNegMetodologia->buscar(array('tipo'=>'H'));

            if(!empty($this->examen)){
                $filter['idexamen'] = $this->idexamen;
                $exam_alumnos =$this->oNegExamen_alumno->buscar($filter);
                $this->cantAprob = 0;
                $this->cantDesaprob = 0;
                foreach ($exam_alumnos as $exam_alu) {
                    if($exam_alu['puntaje']>$this->examen[0]['calificacion_min']){
                        $this->cantAprob += 1;
                    }else{
                        $this->cantDesaprob += 1;
                    }
                }
            }

            $this->documento->plantilla = 'examenes/inicio';
            return parent::getEsquema();
        } catch (Exception $e) {
            return $aplicacion->error(JrTexto::_($e->getMessage()));
        }
	}

	public function guardar()
	{
		$this->documento->plantilla = 'returnjson';
        try {
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }

            @extract($_POST);
            global $aplicacion;         
            $usuarioAct = NegSesion::getUsuario(); 			
				
			if(!empty($idexamen)){
				$this->oNegExamenes->idexamen = $idexamen;
				$accion="editar";
			}
			if(@$calificacionen==='A') $txttotal=@$txtAlfanumerico;
			else $txttotal=@$calificaciontotal;
			$txtportada=@str_replace($this->documento->getUrlBase(),'__xRUTABASEx__',$portada);
			$this->oNegExamenes->__set('idnivel',@$nivel);
			$this->oNegExamenes->__set('idunidad',@$unidad);
			$this->oNegExamenes->__set('idactividad',@$actividad);
			$this->oNegExamenes->__set('titulo',@$titulo);
			$this->oNegExamenes->__set('descripcion',@$descripcion);
			$this->oNegExamenes->__set('portada',$txtportada);
			$this->oNegExamenes->__set('fuente',@$fuente);
			$this->oNegExamenes->__set('fuentesize',@$fuentesize);
			$this->oNegExamenes->__set('tipo',@$tipo);
			$this->oNegExamenes->__set('grupo',@$engrupo);
			$this->oNegExamenes->__set('aleatorio',@$aleatorio);
			$this->oNegExamenes->__set('calificacion_por',@$calificacionpor);
			$this->oNegExamenes->__set('calificacion_en',@$calificacionen);			
			$this->oNegExamenes->__set('calificacion_total',$txttotal);
			$this->oNegExamenes->__set('calificacion_min',!empty($calificacionmin)?$calificacionmin:0);
			$this->oNegExamenes->__set('tiempo_por',@$tiempopor);
			$this->oNegExamenes->__set('tiempo_total',@$tiempototal);
			$this->oNegExamenes->__set('estado',!empty($estado)?$estado:0);
			$this->oNegExamenes->__set('nintento',@$nintento);
			$this->oNegExamenes->__set('calificacion',@$calificacion);
			$this->oNegExamenes->__set('idpersonal',@$usuarioAct["dni"]);			
		    if(@$accion=="editar"){
				$res=$this->oNegExamenes->editar();
			}else{
				$res=$this->oNegExamenes->agregar();
		    }
		    @NegSesion::set('idexamencur', $res, '_examen_');
			echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Assessment')).' '.JrTexto::_('saved successfully'),'new'=>$res)); //
            exit(0);				
		}catch(Exception $e) {
			echo json_encode(array('code'=>'Error','msj'=>$e));
            exit(0);
		} 		
	}

	public function guardarPreguntas()
	{
		$this->documento->plantilla = 'returnjson';
        try {

            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }

            @extract($_POST);
            global $aplicacion;         
            $usuarioAct = NegSesion::getUsuario(); 			
				
			if(!empty($idexamen)&&!empty($idpregunta)){			
				$res=$this->oNegpreguntas->idpregunta = $idpregunta;
				$accion="editar";
			}
			$ejercicio=@trim(str_replace($this->documento->getUrlBase(),'__xRUTABASEx__',@$ejercicio));
			$this->oNegpreguntas->__set('idexamen',$idexamen);
			$this->oNegpreguntas->__set('pregunta',@$pregunta);
			$this->oNegpreguntas->__set('descripcion',@$descripcion);
			$this->oNegpreguntas->__set('ejercicio',@$ejercicio);
			$this->oNegpreguntas->__set('idpadre',@$idpadre);
			$this->oNegpreguntas->__set('tiempo',@$tiempo);
			$this->oNegpreguntas->__set('puntaje',@$puntaje);
			$this->oNegpreguntas->__set('idpersonal',@$usuarioAct["dni"]);
			$this->oNegpreguntas->__set('template',@$template);
			$this->oNegpreguntas->__set('habilidad',@$habilidad);
			$this->oNegpreguntas->__set('idcontenedor',@$idcontenedor);

		    if(@$accion=="editar"){
				$res=$this->oNegpreguntas->editar();
			}else{
				$res=$this->oNegpreguntas->agregar();
		    }
			echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Question')).' '.JrTexto::_('saved successfully'),'new'=>$res)); //
            exit(0);				
		}catch(Exception $e) {
			echo json_encode(array('code'=>'Error','msj'=>$e));
            exit(0);
		} 	
	}

	public function buscarpreguntasjson()
	{
		$this->documento->plantilla = 'returnjson';
        try {
        	$filtros["idexamen"]=!empty($_REQUEST["exa"])?$_REQUEST["exa"]:1;
        	$filtros["orden"]=!empty($_REQUEST["orden"])?$_REQUEST["orden"]:0; //0 asc, 1 aleatorio;
        	$preguntas=$this->oNegpreguntas->mostrarPreguntas($filtros);
			echo json_encode(array('code'=>'ok','preguntas'=>$preguntas)); //
            exit(0);				
		}catch(Exception $e) {
			echo json_encode(array('code'=>'Error','msj'=>$e));
            exit(0);
		} 
		try {
            global $aplicacion;
            $this->documento->stylesheet('datetimepicker.min', '/libs/datetimepicker/css/');
            $this->documento->script('jquery.maskedinput.min', '/tema/js/');
            $this->documento->script('cronometro', '/libs/chingo/');
            $this->documento->script('jquery-ui.min', '/tema/js/');
            $this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery.md5', '/tema/js/');
            $this->documento->stylesheet('circletimer', '/libs/circletimer/');
            $this->documento->script('circletimer.min', '/libs/circletimer/');

            $this->documento->script('multiplantilla', '/js/examen/');
            $this->documento->script('true_false', '/js/examen/');
            $this->documento->script('order_simple', '/js/examen/');
            $this->documento->script('order_paragraph', '/js/examen/');
            $this->documento->script('join', '/js/examen/');
            $this->documento->script('tag_image', '/js/examen/');
            $this->documento->script('editactividad','/js/new/');
            $this->documento->script('actividad_completar','/js/new/');
            $this->documento->script('funciones','/tema/js/');
            $this->documento->script('progressbar.min', '/libs/graficos/progressbar/');
            $this->documento->script('circle', '/libs/graficos/progressbar/');
            $usuarioAct = NegSesion::getUsuario();
            $rol=$usuarioAct["rol"];
            $_idexamen=!empty(NegSesion::get('idexamencur','_examen_'))?NegSesion::get('idexamencur','_examen_'):0;
            $this->idexamen=!empty($_GET["idexamen"])?$_GET["idexamen"]:0;
            if(empty($this->idexamen))$this->idexamen=$_idexamen;
            else NegSesion::set('idexamencur', $this->idexamen, '_examen_');
            $filtros["idexamen"]=$this->idexamen;
            $this->documento->plantilla = 'examenes/inicio';
            if($rol=='alumno'){
                $filtros["estado"]=1;
                $this->esquema = 'alumno/examenes';
            }else{
                $filtros["idpersonal"]=$usuarioAct["dni"];              
                $this->esquema = 'examenes/preview';
            }
            $exam=$this->oNegExamenes->buscar($filtros);
            if(!empty($exam)){
                $this->examen=$exam[0];
                $filtrosp["idexamen"]=$this->idexamen;
                $filtrosp["orden"]=!empty($this->examen["aleatorio"])?$this->examen["aleatorio"]:0; //0 asc, 1 aleatorio;
                $this->preguntas=$this->oNegpreguntas->mostrarPreguntas($filtrosp);
                $this->exanivel=$this->oNegNiveles->buscar(array('tipo'=>'N','idnivel'=>$this->examen["idnivel"]));
                $this->exanunidad=$this->oNegNiveles->buscar(array('tipo'=>'U','idnivel'=>$this->examen["idunidad"]));
                $this->exaactividad=$this->oNegNiveles->buscar(array('tipo'=>'L','idnivel'=>$this->examen["idactividad"]));
                $this->exatype= $this->tipoexamen=$this->oNegExamen_tipo->buscar(array('idtipo'=>$this->examen["tipo"]));
            }else{
                throw new Exception(JrTexto::_('Exam does not exist').'!');
            }
            $this->habilidades=$this->oNegMetodologia->buscar(array('tipo'=>'H'));
            return parent::getEsquema();
        }catch(Exception $e){
            return $aplicacion->error(JrTexto::_($e->getMessage()));
        }
	}

	public function imprimir(){
		try {
			global $aplicacion;
			$usuarioAct = NegSesion::getUsuario();
			$rol=$usuarioAct["rol"];
		    $this->idexamen=!empty($_GET["idexamen"])?$_GET["idexamen"]:0;
		    $filtros["idexamen"]=$this->idexamen;
		    if($rol=='alumno'){
				$filtros["estado"]=1;
				$this->esquema = 'alumno/examenes';
			}else{
				$filtros["idpersonal"]=$usuarioAct["dni"];				
		    	$this->esquema = 'examenes/imprimir';
			}
		    $exam=$this->oNegExamenes->buscar($filtros);
		   	if(!empty($exam)){
				$this->examen=$exam[0];
				$filtrosp["idexamen"]=$this->idexamen;
				$filtrosp["orden"]=!empty($this->examen["aleatorio"])?$this->examen["aleatorio"]:0; //0 asc, 1 aleatorio;
				$this->preguntas=$this->oNegpreguntas->mostrarPreguntas($filtrosp);

                $this->exanivel=$this->oNegNiveles->buscar(array('tipo'=>'N','idnivel'=>$this->examen["idnivel"]));
                $this->exanunidad=$this->oNegNiveles->buscar(array('tipo'=>'U','idnivel'=>$this->examen["idunidad"]));
                $this->exaactividad=$this->oNegNiveles->buscar(array('tipo'=>'L','idnivel'=>$this->examen["idactividad"]));
                $this->exatype= $this->tipoexamen=$this->oNegExamen_tipo->buscar(array('idtipo'=>$this->examen["tipo"]));
			}else{
				throw new Exception(JrTexto::_('Exam does not exist').'!');
			}
			$this->documento->plantilla = 'modal';
			return parent::getEsquema();
		} catch (Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

    public function buscar_exams()
    {
        $this->documento->plantilla = 'returnjson';
        try{
            global $aplicacion;
            $usuarioAct = NegSesion::getUsuario();
            $this->documento->setTitulo(JrTexto::_('Exams'), true);
            $filtros=array();
            $filtros["titulo"]=!empty($_POST["txtBuscar"])?$_POST["txtBuscar"]:'';
            $filtros["idnivel"]=!empty($_POST["nivel"])?$_POST["nivel"]:0;
            $filtros["idunidad"]=!empty($_POST["unidad"])?$_POST["unidad"]:0;
            $filtros["idactividad"]=!empty($_POST["actividad"])?$_POST["actividad"]:0;
            $filtros["estado"]=1;
            //$filtros["tipo"]=!empty($_POST["tipo"])?$_POST["tipo"]:0;
            //$filtros["idpersonal"]=$usuarioAct["dni"];
            //print_r($filtros);
            $examenes=$this->oNegExamenes->buscar($filtros);
            $data=array('code'=>'ok','data'=>$examenes);
            echo json_encode($data);
            return parent::getEsquema();
        }catch(Exception $ex){
            $data=array('code'=>'Error','mensaje'=>JrTexto::_($ex->getMessage()));
            echo json_encode($data);
        }
    }

    public function teacherresrc_view()
    {
        try {
            global $aplicacion;
            /* librerias */
             $this->documento->stylesheet('datetimepicker.min', '/libs/datetimepicker/css/');
             $this->documento->script('jquery.maskedinput.min', '/tema/js/');
             $this->documento->script('cronometro', '/libs/chingo/');
             $this->documento->script('mitimer', '/libs/chingo/');
             $this->documento->script('jquery-ui.min', '/tema/js/');
             $this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
             $this->documento->script('jquery-confirm.min', '/libs/alert/');
             $this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
             $this->documento->script('jquery.md5', '/tema/js/');
             $this->documento->stylesheet('circletimer', '/libs/circletimer/');
             $this->documento->script('circletimer.min', '/libs/circletimer/');

             $this->documento->script('multiplantilla', '/js/examen/');
             $this->documento->script('true_false', '/js/examen/');
             $this->documento->script('order_simple', '/js/examen/');
             $this->documento->script('order_paragraph', '/js/examen/');
             $this->documento->script('join', '/js/examen/');
             $this->documento->script('tag_image', '/js/examen/');
             $this->documento->script('editactividad','/js/new/');
             $this->documento->script('actividad_completar','/js/new/');
             $this->documento->script('funciones','/tema/js/');
             $this->documento->script('progressbar.min', '/libs/graficos/progressbar/');
             $this->documento->script('circle', '/libs/graficos/progressbar/');

            $this->idexamen=!empty($_GET["idexamen"])?$_GET["idexamen"]:-1;
            $filtros["idexamen"]=$this->idexamen;

            $usuarioAct = NegSesion::getUsuario();
            $rol=$usuarioAct["rol"];            
            if($rol=='Alumno'){
                $filtros["estado"]=1;  
                $this->documento->plantilla = 'examen/general';
                $this->esquema = 'examenes/alu_preview';
            }else{
                $this->documento->plantilla = 'verblanco';
                //$filtros["idpersonal"]=intVal($usuarioAct["dni"]);
                $this->esquema = 'examenes/preview';
            }
            //var_dump($filtros);
            $exam=$this->oNegExamenes->buscar($filtros);
            
            if(!empty($exam)){
                $this->examen=$exam[0];
                $filtrosp["idexamen"]=$this->idexamen;
                $filtrosp["orden"]=!empty($this->examen["aleatorio"])?$this->examen["aleatorio"]:0; //0 asc, 1 aleatorio;
                $this->preguntas=$this->oNegpreguntas->mostrarPreguntas($filtrosp);
                $this->exanivel=$this->oNegNiveles->buscar(array('tipo'=>'N','idnivel'=>$this->examen["idnivel"]));
                $this->exanunidad=$this->oNegNiveles->buscar(array('tipo'=>'U','idnivel'=>$this->examen["idunidad"]));
                $this->exaactividad=$this->oNegNiveles->buscar(array('tipo'=>'L','idnivel'=>$this->examen["idactividad"]));
                $this->exatype= $this->tipoexamen=$this->oNegExamen_tipo->buscar(array('idtipo'=>$this->examen["tipo"]));
                if(empty($this->preguntas)){
                    throw new Exception(JrTexto::_("There are no question in this exam").".");
                }
            }else{
                throw new Exception(JrTexto::_('Exam does not exist').'!');
            }
            $this->habilidades=$this->oNegMetodologia->buscar(array('tipo'=>'H'));

            $arrDatosTarea = $this->fnTareaArchivo($this->idexamen);
            $this->idTareaArchivo = @$_GET['idtarea_archivos'];
            $this->tarea = @$arrDatosTarea['tarea'];
            $this->tarea_archivo = @$arrDatosTarea['tarea_archivo'];
            $this->tarea_asignacion_alumno = @$arrDatosTarea['tarea_asignacion_alumno'];
            $this->tarea_respuesta = @$arrDatosTarea['tarea_respuesta'];
            if(!empty($this->tarea)){ $this->documento->plantilla = 'verblanco'; }
            if($this->tarea_archivo['tablapadre']=='R'){
                $arrPreg_Exam = json_decode($this->tarea_archivo['texto'], true)[0]['html'];
                foreach ($this->preguntas as $idContenedor => $pregunta) {
                    foreach ($pregunta as $tipoPreg => $ejercicio) {
                        foreach ($ejercicio as $i=>$ej) {
                            foreach ($arrPreg_Exam as $idPreg => $html) {
                                if($ej['idpregunta']==$idPreg){
                                    $this->preguntas[$idContenedor][$tipoPreg][$i]['ejercicio']=$html;
                                }
                            }
                        }
                    }
                }
            }
            $this->showBtnPrint = false;
            return parent::getEsquema();
        }catch(Exception $e){
            return $aplicacion->error(JrTexto::_($e->getMessage()));
        }
    }

    private function fnTareaArchivo($idGame=0)
    {
        try {
            global $aplicacion;
            if(isset($_GET['idtarea_archivos'])){
                if(empty($_GET['idtarea_archivos'])){ throw new Exception(JrTexto::_("Cannot load homework game")."."); }
                $idTareaArchivo = $_GET['idtarea_archivos'];
                $usuarioAct = NegSesion::getUsuario();
                $adjunto=$tarea=$asig_alumno=$respuesta=$archivoAdjunto= array();
                $adjunto = $this->oNegTarea_archivos->buscar(['idtarea_archivos'=>$idTareaArchivo, /*'tablapadre'=> 'T'*/]);
                
                if($usuarioAct['rol']=='Alumno' && empty($_GET['idtarea_asignacion'])){ throw new Exception(JrTexto::_("Cannot load homework game").". ".JrTexto::_("Assignment was not found")."."); }
                
                $filtrosAsig['idtarea_asignacion']=(!empty(@$_GET['idtarea_asignacion']))? @$_GET['idtarea_asignacion']:0;
                $filtrosAsig['idalumno']=($usuarioAct['rol']=='Alumno')? $usuarioAct['dni']:@$_GET['idalumno'];
                $asig_alumno=$this->oNegTarea_asignacion_alumno->buscar($filtrosAsig);
                
                if(!empty($asig_alumno)){
                    $asig_alumno= $asig_alumno[0];
                    $respuesta = $this->oNegTarea_respuesta->buscarConArchivos(['idtarea_asignacion_alumno'=>$asig_alumno['iddetalle']], ['idtarea_archivos_padre'=> $idTareaArchivo]);
                }else{ throw new Exception(JrTexto::_("This attachment is not assigned to you")); }

                if(!empty($respuesta)){ 
                    $respuesta= $respuesta[0];
                    if(!empty($respuesta['respuesta_archivos'])){
                        $adjunto=$respuesta['respuesta_archivos'];
                    }else{
                        $adjunto = $this->oNegTarea_archivos->buscar(['idtarea_archivos'=>$idTareaArchivo, 'tablapadre'=> 'T']);
                    }
                }

                if(!empty($adjunto)){
                    $adjunto = $adjunto[0];
                    $filtrar = ['idtarea'=>-1];
                    if($adjunto['tablapadre']=='T'){ $filtrar['idtarea']=$adjunto['idpadre']; }
                    elseif($adjunto['tablapadre']=='R'){ 
                        $tarea_archivo = $this->oNegTarea_archivos->buscar(['idtarea_archivos'=>$adjunto['idtarea_archivos_padre']]);
                        $filtrar['idtarea']=$tarea_archivo[0]['idpadre'];
                    }
                    if($usuarioAct['rol']!='Alumno'){ $filtrar['iddocente']=$usuarioAct['dni']; }
                    $tarea = $this->oNegTarea->buscar($filtrar);
                }else{ throw new Exception(JrTexto::_("Game not found for this homework")); }

                if(!empty($tarea)){
                    $tarea = $tarea[0];
                    $archivoAdjunto = $adjunto;
                }else{ throw new Exception(JrTexto::_("Game does not belong to your homework")); }

                $resp = ['tarea'=>$tarea, 'tarea_archivo'=>$archivoAdjunto, 'tarea_asignacion_alumno'=>$asig_alumno, 'tarea_respuesta'=>$respuesta];
                return $resp;
            }
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }
    
    public function promedioxalumno($idalumno=null){
        $this->documento->plantilla = 'returnjson';
        try {
            global $aplicacion;
            if(empty($_POST["id"]) && $idalumno==null) { 
                throw new Exception(JrTexto::_('Error in filtering')); 
            }
            $filtros["idalumno"]=($idalumno==null)?$_POST["id"]:$idalumno;
            $promXHab = [];
            $exams_alum=$this->oNegExamen_alumno->buscar($filtros);
            $sumaPuntaje = 0.0;
            foreach ($exams_alum as $e) {
                $this->oNegExamenes->idexamen=$e['idexamen'];
                $exam=$this->oNegExamenes->getXid();
                $jsonHab = json_decode($e['puntajehabilidad'],true);
                foreach ($jsonHab as $idHab=>$ptjeObtenido) {
                    if(!isset($promXHab[$idHab])){
                        $promXHab[$idHab] = [];
                        $promXHab[$idHab] = [
                            'suma'=>0.0,
                            'cant'=>0,
                            'promedio'=>0.0,
                        ];
                    }
                    $promXHab[$idHab]['suma']+=(float) $ptjeObtenido;
                    $promXHab[$idHab]['cant']+=1;
                    $promXHab[$idHab]['promedio']=$promXHab[$idHab]['suma']/$promXHab[$idHab]['cant'];
                }
                $calif_max = (float) $exam['calificacion_total'];
                $pntje_base100 = $e['puntaje']*100/$calif_max;
                $sumaPuntaje +=$pntje_base100;
            }
            $promExamenes = [
                'promPuntaje'=>(count($exams_alum)>0)?$sumaPuntaje/count($exams_alum):0.0,
                'promXHab'=>$promXHab,
            ];
            if($idalumno==null){
                $data=array('code'=>'ok','data'=>$promExamenes);
                echo json_encode($data);
            } else {
                return $promExamenes;
            }
            return parent::getEsquema();
        } catch (Exception $e) {
            $data=array('code'=>'Error','mensaje'=>JrTexto::_($ex->getMessage()));
            echo json_encode($data);
        }
    }

    public function promedioxgrupo()
    {
        try {
            global $aplicacion;
            if(empty($_POST["id"])) { 
                throw new Exception(JrTexto::_('Error in filtering')); 
            }
            $filtros = [];
            $promedioHab = [];
            $sumaPuntaje = 0.0;
            $filtros['idgrupo']=$_POST['id'];
            $alum_grupo = $this->oNegGrupo_matricula->buscar($filtros);
            foreach ($alum_grupo as $al) {
                $arrPtjexHab = $this->promedioxalumno($al['idalumno']);
                foreach ($arrPtjexHab['promXHab'] as $idHab=>$values) {
                    if(!isset($promedioHab[$idHab])){
                        $promedioHab[$idHab] = [];
                        $promedioHab[$idHab] = [
                            'suma'=>0.0,
                            'cant'=>count($alum_grupo),
                            'promedio'=>0.0,
                        ];
                    }
                    $promedioHab[$idHab]['suma']+=(float) $values['promedio'];
                    //$promedioHab[$idHab]['cant']=count($alum_grupo);
                    $promedioHab[$idHab]['promedio']=$promedioHab[$idHab]['suma']/$promedioHab[$idHab]['cant'];
                }
                $sumaPuntaje+=(float) $arrPtjexHab['promPuntaje'];
            }
            $promExamenes = [
                'promPuntaje'=>(count($alum_grupo)>0)?$sumaPuntaje/count($alum_grupo):0.0,
                'promXHab'=>$promedioHab,
            ];
            $data=array('code'=>'ok','data'=>$promExamenes);
            echo json_encode($data);
            return parent::getEsquema();
        } catch (Exception $ex) {
            $data=array('code'=>'Error','mensaje'=>JrTexto::_($ex->getMessage()));
            echo json_encode($data);
        }
    }

	public function xEliminar(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegExamenes->__set('idexamen', $pk);
				$res=$this->oNegExamenes->eliminar();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Error').' '.JrTexto::_('Delete Record')), 'warning');
					$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}

	public function xEliminarxPadre(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$res=$this->oNegpreguntas->eliminarxcontenedor($pk);
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Error').' '.JrTexto::_('Delete Record')), 'warning');
					$oRespAjax->setReturnValue(false);
				}
			}catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}

	public function xEliminarPregunta(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegpreguntas->idpregunta=$pk;
				$res=$this->oNegpreguntas->eliminar();
				if(!empty($res))
					$oRespAjax->setReturnValue($pk);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Error').' '.JrTexto::_('Delete Record')), 'warning');
					$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}
	
	public function xSetCampo(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
               
				if(empty($args[0])) { return;}
				$this->oNegExamenes->setCampo($args[0],$args[1],$args[2]);
				$oRespAjax->setReturnValue(true);
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Atención')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}

	public function xSetCampoPregunta(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {               
				if(empty($args[0])) { return;}
				$this->oNegpreguntas->setCampo($args[0],$args[1],$args[2]);
				$oRespAjax->setReturnValue(true);
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Atención')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}	     
}