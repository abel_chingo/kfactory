<?php
/**
 * @autor		Abel Chingo Tello, ACHT
 * @fecha		2016-06-01
 * @copyright	Copyright (C) 2016. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegAlumno', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegMetodologia_habilidad', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegGrupos', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegLocal', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegNiveles', RUTA_BASE, 'sys_negocio');
class WebDocente extends JrWeb
{	
	
    protected $oNegAlumno;
    protected $oNegMetodologia;
    protected $oNegGrupos;
    protected $oNegLocal;
    protected $oNegNiveles;
    
	public function __construct()
	{
		parent::__construct();
        $this->oNegAlumno = new NegAlumno;
        $this->oNegMetodologia = new NegMetodologia_habilidad;
        $this->oNegGrupos = new NegGrupos;
        $this->oNegLocal = new NegLocal;
        $this->oNegNiveles = new NegNiveles;
	}
	
	public function defecto()
	{
		try {
			global $aplicacion;
			$this->documento->setTitulo(JrTexto::_('docente'), true);
			$this->esquema = 'alumno/tester';
			$this->documento->plantilla = 'general';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function panelcontrol()
	{
		try {
			global $aplicacion;
            if(!NegSesion::tiene_acceso('docente', 'add')) {
                throw new Exception(JrTexto::_('Restricted access').'!!');
            }
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('slick.min', '/libs/sliders/slick/');
            $this->documento->stylesheet('slick', '/libs/sliders/slick/');
            $this->documento->stylesheet('slick-theme', '/libs/sliders/slick/');
            $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
            $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
            $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
            $this->documento->script('loader', '/libs/googlecharts/');
            $this->documento->script('chartist.min', '/libs/chartist/');
            $this->documento->stylesheet('chartist.min', '/libs/chartist/');
            $this->documento->script('progressbar.min', '/libs/graficos/progressbar/');
            $this->documento->script('circle', '/libs/graficos/progressbar/');
            $this->documento->script('jquery.maskedinput.min', '/tema/js/');
            $this->documento->script('html2canvas.min', '/libs/html2canvas/');
            $this->documento->script('rgbcolor', '/libs/svg2canvas/');
            $this->documento->script('StackBlur', '/libs/svg2canvas/');
            $this->documento->script('canvg.min', '/libs/svg2canvas/');
            $this->documento->script('SVG2Bitmap', '/libs/chartist2image/');
            $usuarioAct = NegSesion::getUsuario();
            $filtros = [];
            $filtros['iddocente'] = $usuarioAct["dni"];
            $this->habilidades=$this->oNegMetodologia->buscar(array('tipo'=>'H'));
            $this->grupos=$this->oNegGrupos->buscar($filtros);
            $this->locales=[];
            $arrLocales = [];
            foreach ($this->grupos as $g) {
                if(!in_array($g['idlocal'], $arrLocales)){
                    $this->oNegLocal->idlocal = $g['idlocal'];
                    $this->locales[]=$this->oNegLocal->getXid();
                    $arrLocales[]=$g['idlocal'];
                }
            }
            /*$this->alumnos=$this->oNegAlumno->buscar();*/
            $this->documento->plantilla = 'inicio';
			$this->documento->setTitulo(JrTexto::_('Tracking'), true);
			$this->esquema = 'docente/panelcontrol';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

    public function configuracion()
    {
        try {
            global $aplicacion;
            if(!NegSesion::tiene_acceso('Configuracion_docente', 'add')) {
                throw new Exception(JrTexto::_('Restricted access').'!!');
            }
            $this->documento->script('jquery.maskedinput.min', '/tema/js/');

            $this->niveles=$this->oNegNiveles->buscar(array('tipo'=>'N'));

            $this->documento->plantilla = 'inicio';
            $this->documento->setTitulo(JrTexto::_('Setting'), true);
            $this->esquema = 'docente/configuracion';
            return parent::getEsquema();
        } catch (Exception $e) {
            return $aplicacion->error(JrTexto::_($e->getMessage()));
        }
    }
}