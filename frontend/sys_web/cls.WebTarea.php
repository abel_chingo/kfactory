<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		31-05-2017 
 * @copyright	Copyright (C) 31-05-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegTarea', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegTarea_asignacion', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegTarea_asignacion_alumno', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegTarea_archivos', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegNiveles', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegGrupos', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegLocal', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegTarea_respuesta', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegMetodologia_habilidad', RUTA_BASE, 'sys_negocio');
class WebTarea extends JrWeb
{
	private $oNegTarea;
    private $oNegTarea_asignacion;
	private $oNegTarea_asignacion_alumno;
    protected $oNegNiveles;
    protected $oNegTarea_archivos;
    protected $oNegGrupos;
    protected $oNegLocal;
    private $oNegTarea_respuesta;
    protected $oNegHabilidad;
	public function __construct()
	{
		parent::__construct();		
		$this->oNegTarea = new NegTarea;
        $this->oNegTarea_asignacion = new NegTarea_asignacion;
		$this->oNegTarea_asignacion_alumno = new NegTarea_asignacion_alumno;
        $this->oNegNiveles = new NegNiveles;
        $this->oNegTarea_archivos = new NegTarea_archivos;
        $this->oNegGrupos = new NegGrupos;
        $this->oNegLocal = new NegLocal;
        $this->oNegTarea_respuesta = new NegTarea_respuesta;
        $this->oNegHabilidad = new NegMetodologia_habilidad;
	}

	public function defecto(){
		return $this->listado();
	}

	public function listado()
	{
		try{
			global $aplicacion;
            $usuarioAct = NegSesion::getUsuario();
			if(!NegSesion::tiene_acceso('Tarea', 'list') && $usuarioAct['rol']!='Alumno') {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');

			$this->niveles=$this->oNegNiveles->buscar(array('tipo'=>'N'));
			
            if($usuarioAct['rol']=='Alumno'){
                $pend_final = $this->listarNuevasDevueltasYFinalizadas(true);
                $this->esquema = 'tarea/tarea-list-alum';
            }else{
                $pend_final = $this->listarTareasPendientesYFinalizadas(true);
                $this->todoTareas = $pend_final["todo"];
                $this->esquema = 'tarea/tarea-list';
            }
            $this->tareasPend = $pend_final["pendientes"];
            $this->tareasFin = $pend_final["finalizadas"];

			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'inicio';
			$this->documento->setTitulo(JrTexto::_('Homework'), true);
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function agregar()
	{
		try {
			global $aplicacion;			
			if(!NegSesion::tiene_acceso('Tarea', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}

            /*librerias*/
             $this->documento->script('jquery-ui.min', '/tema/js/');
             $this->documento->script('jquery.md5', '/tema/js/');
             $this->documento->script('jquery.maskedinput.min', '/tema/js/'); 
             $this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
             $this->documento->script('jquery-confirm.min', '/libs/alert/');
            
             $this->documento->script('cronometro', '/libs/chingo/');
			 $this->documento->script('editactividad', '/js/new/');
             $this->documento->script('actividad_completar', '/js/new/');
             $this->documento->script('actividad_ordenar', '/js/new/');
             $this->documento->script('actividad_imgpuntos', '/js/new/');
             $this->documento->script('actividad_verdad_falso', '/js/new/');
             $this->documento->script('actividad_fichas', '/js/new/');
             $this->documento->script('actividad_dialogo', '/js/new/');
             $this->documento->script('manejadores_dby', '/js/new/');
             $this->documento->script('manejadores_practice', '/js/new/');
             $this->documento->script('jquery.md5', '/tema/js/');            
             $this->documento->script('manejadores_practice', '/js/new/');
             $this->documento->script('completar', '/js/new/');

             $this->documento->script('tools_games', '/js/');
             $this->documento->script('plugin_visor', '/js/');
             $this->documento->stylesheet('estilo', '/libs/crusigrama/');
             $this->documento->script('crossword', '/libs/crusigrama/');
             $this->documento->script('micrusigrama', '/libs/crusigrama/');
             $this->documento->script('snap-puzzle', '/libs/rompecabezas/');
             $this->documento->script('wordfind', '/libs/sopaletras/js/');
             $this->documento->script('wordfind.game', '/libs/sopaletras/js/');
             $this->documento->stylesheet('wordfind', '/libs/sopaletras/css/');
             
             $this->documento->script('wavesurfer.min', '/libs/audiorecord/');
             $this->documento->script('audioRecord', '/libs/audiorecord/');
             $this->documento->script('recorderWorker', '/libs/audiorecord/');

			$this->niveles=$this->oNegNiveles->buscar(array('tipo'=>'N'));
			$this->frmaccion='Nuevo';
			$this->breadcrumb=JrTexto::_('Add');
			$this->documento->setTitulo(JrTexto::_('Tarea').' /'.JrTexto::_('New'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function editar()
	{
		try {
			global $aplicacion;
			
			if(!NegSesion::tiene_acceso('Tarea', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
            /*librerias*/
             $this->documento->script('jquery-ui.min', '/tema/js/');
             $this->documento->script('jquery.md5', '/tema/js/');
             $this->documento->script('jquery.maskedinput.min', '/tema/js/');
             $this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
             $this->documento->script('jquery-confirm.min', '/libs/alert/');
            
             $this->documento->script('cronometro', '/libs/chingo/');
			 $this->documento->script('editactividad', '/js/new/');
             $this->documento->script('actividad_completar', '/js/new/');
             $this->documento->script('actividad_ordenar', '/js/new/');
             $this->documento->script('actividad_imgpuntos', '/js/new/');
             $this->documento->script('actividad_verdad_falso', '/js/new/');
             $this->documento->script('actividad_fichas', '/js/new/');
             $this->documento->script('actividad_dialogo', '/js/new/');
             $this->documento->script('manejadores_dby', '/js/new/');
             $this->documento->script('manejadores_practice', '/js/new/');
             $this->documento->script('jquery.md5', '/tema/js/');            
             $this->documento->script('manejadores_practice', '/js/new/');
             $this->documento->script('completar', '/js/new/');

             $this->documento->script('tools_games', '/js/');
             $this->documento->script('plugin_visor', '/js/');
             $this->documento->stylesheet('estilo', '/libs/crusigrama/');
             $this->documento->script('crossword', '/libs/crusigrama/');
             $this->documento->script('micrusigrama', '/libs/crusigrama/');
             $this->documento->script('snap-puzzle', '/libs/rompecabezas/');
             $this->documento->script('wordfind', '/libs/sopaletras/js/');
             $this->documento->script('wordfind.game', '/libs/sopaletras/js/');
             $this->documento->stylesheet('wordfind', '/libs/sopaletras/css/');
             
             $this->documento->script('wavesurfer.min', '/libs/audiorecord/');
             $this->documento->script('audioRecord', '/libs/audiorecord/');
             $this->documento->script('recorderWorker', '/libs/audiorecord/');

			$usuarioAct = NegSesion::getUsuario();
			$this->frmaccion='Editar';
			$this->breadcrumb=JrTexto::_('Edit');
			$this->pk=@$_GET['id'];

			$tareas = $this->oNegTarea->buscar(array('idtarea'=>$_GET['id'], 'iddocente'=>$usuarioAct["dni"]));
			if(empty($tareas)){ throw new Exception(JrTexto::_('Homework not found')); }
			$this->datos = $tareas[0];
			$this->datos['tarea_archivos']=$this->oNegTarea_archivos->buscar(array('tablapadre'=>'T', 'idpadre'=>$this->datos['idtarea']));
            $this->datos['tarea_asignacion']=$this->oNegTarea_asignacion->buscarConDetalle(array('fechahora_mayorigual'=>date('Y-m-d H:i:s'), 'idtarea'=>$this->datos['idtarea']));

			$this->niveles=$this->oNegNiveles->buscar(array('tipo'=>'N'));
			$this->grupos=$this->oNegGrupos->buscar(array('iddocente'=>$usuarioAct["dni"]));
            $this->locales=[];
            $arrLocales = [];
            foreach ($this->grupos as $g) {
                if(!in_array($g['idlocal'], $arrLocales)){
                    $this->oNegLocal->idlocal = $g['idlocal'];
                    $this->locales[]=$this->oNegLocal->getXid();
                    $arrLocales[]=$g['idlocal'];
                }
            }
			
			$this->documento->setTitulo(JrTexto::_('Tarea').' /'.JrTexto::_('Edit'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function ver()
    {
		try{
			global $aplicacion;	
            /*librerias*/
             $this->documento->script('jquery-ui.min', '/tema/js/');
             $this->documento->script('jquery.md5', '/tema/js/');
             $this->documento->script('jquery.maskedinput.min', '/tema/js/');
             $this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
             $this->documento->script('jquery-confirm.min', '/libs/alert/');
            
             $this->documento->script('cronometro', '/libs/chingo/');
             $this->documento->script('editactividad', '/js/new/');
             $this->documento->script('actividad_completar', '/js/new/');
             $this->documento->script('actividad_ordenar', '/js/new/');
             $this->documento->script('actividad_imgpuntos', '/js/new/');
             $this->documento->script('actividad_verdad_falso', '/js/new/');
             $this->documento->script('actividad_fichas', '/js/new/');
             $this->documento->script('actividad_dialogo', '/js/new/');
             $this->documento->script('manejadores_dby', '/js/new/');
             $this->documento->script('manejadores_practice', '/js/new/');
             $this->documento->script('jquery.md5', '/tema/js/');            
             $this->documento->script('manejadores_practice', '/js/new/');
             $this->documento->script('completar', '/js/new/');

             $this->documento->script('tools_games', '/js/');
             $this->documento->stylesheet('estilo', '/libs/crusigrama/');
             $this->documento->script('crossword', '/libs/crusigrama/');
             $this->documento->script('micrusigrama', '/libs/crusigrama/');
             $this->documento->script('snap-puzzle', '/libs/rompecabezas/');
             $this->documento->script('wordfind', '/libs/sopaletras/js/');
             $this->documento->script('wordfind.game', '/libs/sopaletras/js/');
             $this->documento->stylesheet('wordfind', '/libs/sopaletras/css/');
             
             $this->documento->script('wavesurfer.min', '/libs/audiorecord/');
             $this->documento->script('audioRecord', '/libs/audiorecord/');
             $this->documento->script('recorderWorker', '/libs/audiorecord/');

            $usuarioAct = NegSesion::getUsuario();
            $this->breadcrumb=JrTexto::_('View');
            if($usuarioAct['rol']=="Alumno"){
                $this->oNegTarea_asignacion_alumno->iddetalle = @$_GET['id'];
                $asignacion_alumno = $this->oNegTarea_asignacion_alumno->dataTarea_asignacion_alumno;

                $idTarea_asignacion = $asignacion_alumno['idtarea_asignacion'];
            }else{
                $idTarea_asignacion = @$_GET['id'];
            }
            $this->oNegTarea_asignacion->idtarea_asignacion = $idTarea_asignacion;
            $tarea_asignacion = $this->oNegTarea_asignacion->dataTarea_asignacion;
            $filtrosTarAsign = array('idtarea'=>$tarea_asignacion['idtarea']);
            if($usuarioAct['rol']!="Alumno"){  $filtrosTarAsign['iddocente'] = $usuarioAct["dni"]; }

            $tareas = $this->oNegTarea->buscar($filtrosTarAsign);
            if(empty($tareas)){ throw new Exception(JrTexto::_('Homework not found')); }
            $this->datos = array_merge($tareas[0], $tarea_asignacion);
            $this->datos['tarea_archivos']=$this->oNegTarea_archivos->buscar(array('tablapadre'=>'T', 'idpadre'=>$this->datos['idtarea']));

            $filtrosAsignAlum=array();
            if($usuarioAct['rol']=="Alumno"){ $filtrosAsignAlum['idalumno']= $usuarioAct['dni']; }

            $this->datos['tarea_asignacion']=$this->oNegTarea_asignacion->buscarConDetalle(array('idtarea_asignacion'=>$tarea_asignacion['idtarea_asignacion']), $filtrosAsignAlum);
            
            $i=0;
            if(!empty($this->datos['tarea_asignacion'])){
                $this->datos['tarea_asignacion']= $this->datos['tarea_asignacion'][0];
                foreach ($this->datos['tarea_asignacion']['detalle'] as $asig_alum) {
                    $hoy= new DateTime(date('Y-m-d H:i:s'));
                    $fecha_vencimiento= new DateTime($this->datos['fechaentrega'].' '.$this->datos['horaentrega']);
                    if($fecha_vencimiento<$hoy && $this->datos['tarea_asignacion']['detalle'][$i]['estado']!='E'){
                        $this->datos['tarea_asignacion']['detalle'][$i]['estado']='P';
                        $this->oNegTarea_asignacion_alumno->iddetalle=$asig_alum['iddetalle'];
                        $this->oNegTarea_asignacion_alumno->__set('estado', 'P');
                        $this->oNegTarea_asignacion_alumno->editar();
                    }
                    if($asig_alum['estado']=='P' || $asig_alum['estado']=='E' || $usuarioAct['rol']=="Alumno"){
                        $tarea_resp=$this->oNegTarea_respuesta->buscarConArchivos(array('idtarea_asignacion_alumno'=>$asig_alum['iddetalle']));
                        if(!empty($tarea_resp)){
                            $this->datos['tarea_asignacion']['detalle'][$i]=array_merge($this->datos['tarea_asignacion']['detalle'][$i], $tarea_resp[0]);
                        }
                    }
                    $i++;
                }
            }
            $this->datos['nivel']=$this->oNegNiveles->buscar(array('idnivel'=>$this->datos['idnivel']))[0];
            $this->datos['unidad']=$this->oNegNiveles->buscar(array('idnivel'=>$this->datos['idunidad']))[0];
            $this->datos['actividad']=$this->oNegNiveles->buscar(array('idnivel'=>$this->datos['idactividad']))[0];
            $arrHabilidades=$this->oNegHabilidad->buscar(array('tipo'=>'H'));
            $this->habilidades=$this->habilidadColor=array();
            $arrColoresHab = ['#f59440','#337ab7','#5cb85c','#5bc0de','#7e60e0','#d9534f'];
            foreach ($arrHabilidades as $index=>$hab) {
                $this->habilidades[$hab["idmetodologia"]]=$hab["nombre"];
                $this->habilidadColor[$hab["idmetodologia"]]=$arrColoresHab[$index];
            } 

			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Tarea').' /'.JrTexto::_('see'), true);
            if($usuarioAct['rol']=="Alumno"){
                $this->esquema = 'tarea/tarea-see-alum';
            }else{
                $this->esquema = 'tarea/tarea-see';
            }
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	private function form()
	{
		try {
			global $aplicacion;	
			
			//$this->documento->script(null, 'http://tinymce.cachefly.net/4.2/tinymce.min.js');			
			$this->esquema = 'tarea/tarea-frm';
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'inicio';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

    public function listarTareasPendientesYFinalizadas($flag=false)
    {
        $this->documento->plantilla = 'returnjson';
        try {
            global $aplicacion;
            $this->todoTareas=$this->tareasPend=$this->tareasFin=array();
            if(empty($_POST["idnivel"]) && empty($_POST["idunidad"]) && empty($_POST["idactividad"]) && !$flag){
                throw new Exception(JrTexto::_('Error in filtering'));
            }
            $usuarioAct = NegSesion::getUsuario();
            $filtrosTarea = array( 'iddocente'=> $usuarioAct['dni'], 'eliminado'=>0 );
            if(!$flag){
                if($_POST['idnivel']>0) $filtrosTarea["idnivel"] = $_POST['idnivel'];
                if($_POST['idunidad']>0) $filtrosTarea["idunidad"] = $_POST['idunidad'];
                if($_POST['idactividad']>0) $filtrosTarea["idactividad"] = $_POST['idactividad'];
            }
            $this->todoTareas=$this->oNegTarea->buscar($filtrosTarea);
            $x=0;
            foreach ($this->todoTareas as $t) {
                $cant_asiganaciones_activas=0;
                $asignaciones=$this->oNegTarea_asignacion->buscar(array('idtarea' => $t['idtarea']));
                foreach ($asignaciones as $a) {
                    $hoy = new DateTime(date('Y-m-d H:i:s'));
                    $fecha_hora = new DateTime($a['fechaentrega'].' '.$a['horaentrega']);
                    $fechahoy=date('d-m-Y');
                    $fechaentrega=date('d-m-Y', strtotime($a['fechaentrega'])); 
                    $a["fechaentrega"] = ($fechaentrega==$fechahoy)?JrTexto::_("Today"):$fechaentrega;
                    $a["horaentrega"] = date('h:i a', strtotime($a["horaentrega"]));
                    $a['cant_presentados']=count($this->oNegTarea_asignacion_alumno->buscar(array('idtarea_asignacion'=>$a['idtarea_asignacion'], 'estado'=>['P','E'] )));
                    $a['cant_asignaciones']=count($this->oNegTarea_asignacion_alumno->buscar(array('idtarea_asignacion'=>$a['idtarea_asignacion'] )));
                    if($fecha_hora>$hoy){
                        $this->tareasPend[] = array_merge($t,$a);
                        $cant_asiganaciones_activas+=$a['cant_asignaciones'];
                    }else{
                        $this->tareasFin[] = array_merge($t,$a);
                    }
                }
                $this->todoTareas[$x]['cant_asignaciones'] = $cant_asiganaciones_activas;
                if($cant_asiganaciones_activas==0){ 
                    /* ya no hay asignciones, entonces actualizar tarea a "No Asignada"='NA' */
                    $this->oNegTarea->idtarea= $t['idtarea'];
                    $this->oNegTarea->__set('estado', 'NA');
                    $this->oNegTarea->editar();
                }
                $x++;
            }

            $respuesta = array("pendientes"=>$this->tareasPend, "finalizadas"=>$this->tareasFin, "todo"=>$this->todoTareas);
            if($flag){ return $respuesta; }
            $data=array('code'=>'ok','data'=>$respuesta);
            echo json_encode($data);
            return parent::getEsquema();
        } catch (Exception $e) {
            if($flag){ return array("pendientes"=>[], "finalizadas"=>[], "todo"=>[]); }
            $data=array('code'=>'Error','mensaje'=>JrTexto::_($e->getMessage()));
            echo json_encode($data);
        }
    }

	public function listarNuevasDevueltasYFinalizadas($flag=false)
	{
		$this->documento->plantilla = 'returnjson';
		try {
			global $aplicacion;
            $this->todoTareas=$this->tareasPend=$this->tareasFin=array();
			if(empty($_POST["idnivel"]) && empty($_POST["idunidad"]) && empty($_POST["idactividad"]) && !$flag){
				throw new Exception(JrTexto::_('Error in filtering'));
			}
			$usuarioAct = NegSesion::getUsuario();
            $filtrosTarea = array('eliminado'=>0);
            if(!$flag){
                if($_POST['idnivel']>0) $filtrosTarea["idnivel"] = $_POST['idnivel'];
                if($_POST['idunidad']>0) $filtrosTarea["idunidad"] = $_POST['idunidad'];
                if($_POST['idactividad']>0) $filtrosTarea["idactividad"] = $_POST['idactividad'];
            }
            $this->todoTareas=$this->oNegTarea->buscar($filtrosTarea);
            foreach ($this->todoTareas as $t) {
                $cant_asiganaciones=0;
                $asignaciones=$this->oNegTarea_asignacion->buscar(array('idtarea' => $t['idtarea']));
                foreach ($asignaciones as $a) {
                    $hoy = new DateTime(date('Y-m-d H:i:s'));
                    $fecha_hora = new DateTime($a['fechaentrega'].' '.$a['horaentrega']);
                    $fechahoy=date('d-m-Y');
                    $fechaentrega=date('d-m-Y', strtotime($a['fechaentrega'])); 
                    $a["fechaentrega"] = ($fechaentrega==$fechahoy)?JrTexto::_("Today"):$fechaentrega;
                    $a["horaentrega"] = date('h:i a', strtotime($a["horaentrega"]));
                    $asignacion_alumno=$this->oNegTarea_asignacion_alumno->buscar(array('idtarea_asignacion'=>$a['idtarea_asignacion'], 'idalumno'=>$usuarioAct['dni'] , /*'estado'=>['N','D','E','P']*/));
                    if(!empty($asignacion_alumno)){
                        $a['asignacion_alumno'] = $asignacion_alumno[0];
                        if($fecha_hora>$hoy && ($asignacion_alumno[0]['estado']=='N' || $asignacion_alumno[0]['estado']=='D') ){
                            $this->tareasPend[] = array_merge($t,$a);
                        }
                        if($fecha_hora<=$hoy || $asignacion_alumno[0]['estado']=='E' || $asignacion_alumno[0]['estado']=='P'){
                            $this->tareasFin[] = array_merge($t,$a);
                        }
                    }
                }
            }

			$respuesta = array("pendientes"=>$this->tareasPend, "finalizadas"=>$this->tareasFin, );
			if($flag){ return $respuesta; }
			$data=array('code'=>'ok','data'=>$respuesta);
            echo json_encode($data);
            return parent::getEsquema();
		} catch (Exception $e) {
			if($flag){ return array("pendientes"=>[], "finalizadas"=>[], "todo"=>[]); }
			$data=array('code'=>'Error','mensaje'=>JrTexto::_($e->getMessage()));
            echo json_encode($data);
		}
	}

	public function xGuardar()
	{
		$this->documento->plantilla = 'returnjson';
		try {
			global $aplicacion;
			if(empty($_POST)){
				throw new Exception(JrTexto::_('No data to insert'));
			}
			$frm = $_POST;
			if(@$frm["accion"]=="Editar"){
				$this->oNegTarea->idtarea =  @$frm['pkIdtarea'];
			};
			$usuarioAct = NegSesion::getUsuario();
			$this->oNegTarea->__set('iddocente',$usuarioAct['dni']);
			$this->oNegTarea->__set('idnivel',@$frm["opcIdnivel"]);
			$this->oNegTarea->__set('idunidad',@$frm["opcIdunidad"]);
			$this->oNegTarea->__set('idactividad',@$frm["opcIdactividad"]);
			$this->oNegTarea->__set('nombre',@$frm["txtNombre"]);
			$this->oNegTarea->__set('descripcion',@$frm["txtDescripcion"]);
			$this->oNegTarea->__set('foto',str_replace($this->documento->getUrlBase(), '__xRUTABASEx__', @$frm["txtFoto"]));
			$this->oNegTarea->__set('habilidades',@$frm["txtHabilidades"]);
			$this->oNegTarea->__set('puntajemaximo',@$frm["opcPuntajemaximo"]);
			$this->oNegTarea->__set('puntajeminimo',@$frm["opcPuntajeminimo"]);
			$arrIdTarea_archivos = json_decode(@$frm['txtIdTarea_archivos'], true);
			if(@$frm["accion"]=="Nuevo"){
				$idTarea=$this->oNegTarea->agregar();
			}else{
				$idTarea=$this->oNegTarea->editar();
			}
            if(!empty($arrIdTarea_archivos)){
    			foreach ($arrIdTarea_archivos as $idTar_Arch) {
    				$this->oNegTarea_archivos->idtarea_archivos=$idTar_Arch;
    				$this->oNegTarea_archivos->__set('tablapadre','T');
    				$this->oNegTarea_archivos->__set('idpadre',@$idTarea);
    				$res=$this->oNegTarea_archivos->editar();
    			}
            }

            $this->actualizarEstadoTarea($idTarea);

			$data=array('code'=>'ok','data'=>$idTarea);
            echo json_encode($data);
            return parent::getEsquema();
		} catch (Exception $e) {
			$data=array('code'=>'Error','msj'=>JrTexto::_($e->getMessage()));
            echo json_encode($data);
		}
	}

	public function xEliminar_logica()
	{
		$this->documento->plantilla = 'returnjson';
		try {
			global $aplicacion;
			if(empty($_POST['idtarea'])){
				throw new Exception(JrTexto::_('No idtarea to delete'));
			}
			$this->oNegTarea->__set('idtarea', $_POST['idtarea']);
			$res=$this->oNegTarea->eliminar_logica();
			if(empty($res)){
				throw new Exception(JrTexto::_('Error').' '.JrTexto::_('Delete Record'));
			}
			$data=array('code'=>'ok','data'=>$_POST['idtarea']);
            echo json_encode($data);
            return parent::getEsquema();
		} catch (Exception $e) {
			$data=array('code'=>'Error','msj'=>JrTexto::_($e->getMessage()));
            echo json_encode($data);
		}
	}

    private function actualizarEstadoTarea($idtarea=0)
    {
        try {
            if($idtarea<1){ throw new Exception(JrTexto::_('No id_tarea for update estado field')); }
            $tareas_asignadas = $this->oNegTarea_asignacion->buscar(array('idtarea'=>$idtarea, 'fechahora_mayorigual'=>date('Y-m-d H:i:s'), ));
            $this->oNegTarea->idtarea = $idtarea;
            if(empty($tareas_asignadas)){ $this->oNegTarea->__set('estado', 'NA'); }
            else{ $this->oNegTarea->__set('estado', 'A'); }
            $resp=$this->oNegTarea->editar();
        } catch (Exception $e) {
            throw new Exception($e->getMessage());
        }
    }

    public function xGuardarPuntajes()
    {
        $this->documento->plantilla = 'returnjson';
        try {
            global $aplicacion;
            if(empty($_POST['idasignacionalumno']) && empty($_POST['archivos'])){
                throw new Exception(JrTexto::_('No data to update'));
            }
            $this->oNegTarea_asignacion_alumno->iddetalle=$_POST['idasignacionalumno'];
            $this->oNegTarea_asignacion_alumno->__set('notapromedio',@$_POST['notapromedio']);
            $this->oNegTarea_asignacion_alumno->__set('estado', 'E');
            $resp = $this->oNegTarea_asignacion_alumno->editar();
            $arrArchivos=json_decode($_POST['archivos'],true);
            if(!empty($arrArchivos)){
                foreach ($arrArchivos as $arch) {
                    if(empty($arch['habilidades'])){ throw new Exception("Skills are missing in some evaluation"); }
                    $this->oNegTarea_archivos->idtarea_archivos = $arch['idtarea_archivos'];
                    $this->oNegTarea_archivos->__set('puntaje', $arch['puntaje']);
                    $this->oNegTarea_archivos->__set('habilidad', $arch['habilidades']);
                    $this->oNegTarea_archivos->editar();
                }
            }
            $data=array('code'=>'ok','data'=>$_POST['idasignacionalumno'], 'msj'=>JrTexto::_('Scores saved successfully'));
            echo json_encode($data);
            return parent::getEsquema();
        } catch (Exception $e) {
            $data=array('code'=>'Error','msj'=>JrTexto::_($e->getMessage()));
            echo json_encode($data);
        }
    }

    public function xDevolver()
    {
        $this->documento->plantilla = 'returnjson';
        try {
            global $aplicacion;
            if(empty($_POST['idtarea_asignacion_alumno']) && empty($_POST['mensajedevolucion'])){
                throw new Exception(JrTexto::_('No data to update'));
            }
            $this->oNegTarea_asignacion_alumno->iddetalle=$_POST['idtarea_asignacion_alumno'];
            $this->oNegTarea_asignacion_alumno->__set('mensajedevolucion',@$_POST['mensajedevolucion']);
            $this->oNegTarea_asignacion_alumno->__set('estado', 'D');
            $resp = $this->oNegTarea_asignacion_alumno->editar();
            $data=array('code'=>'ok','data'=>$_POST['idtarea_asignacion_alumno'], 'msj'=>JrTexto::_('Homework was returned to student').'.');
            echo json_encode($data);
            return parent::getEsquema();
        } catch (Exception $e) {
            $data=array('code'=>'Error','msj'=>JrTexto::_($e->getMessage()));
            echo json_encode($data);
        }
    }

    /**** Para el módulo "Seguimiento del Estudiante" ****/
    public function xSeguimientoAlumno($idgrupo=null, $idalumno=null)
    {
        $this->documento->plantilla = 'returnjson';
        try{
            global $aplicacion;
            if(empty($_POST["idalumno"]) && empty($_POST["idgrupo"]) && $idalumno==null && $idgrupo==null) { 
                throw new Exception(JrTexto::_('Error in filtering')); 
            }
            $arrNotasxFechas=$filtros=$filtros2=array();
            $presentYear = date('Y');
            $filtros["idgrupo"]=($idgrupo==null)?$_POST["idgrupo"]:$idgrupo;
            $filtros["fechahora_mayorigual"]=$presentYear.'-01-01 00:00:00';
            $asignaciones=$this->oNegTarea_asignacion->buscar($filtros);
            foreach ($asignaciones as $asig) {
                $tarea = $this->oNegTarea->buscar([ 'idtarea'=>$asig['idtarea'] ]);
                $ptjeMax = (float)$tarea[0]['puntajemaximo'];
                $promedioAsig = 0.0;
                $filtros2["idtarea_asignacion"]=$asig['idtarea_asignacion'];
                if ($idalumno!=null){ $filtros2["idalumno"]=$idalumno; }
                elseif (!empty($_POST["idalumno"])) { $filtros2["idalumno"]=$_POST["idalumno"]; }
                $asignaciones_alum = $this->oNegTarea_asignacion_alumno->buscar($filtros2);
                foreach ($asignaciones_alum as $asig_alum) {
                    $puntaje = ((float)$asig_alum['notapromedio']*100)/$ptjeMax;
                    $promedioAsig += $puntaje;
                }
                $promedioAsig = $promedioAsig/count($asignaciones_alum);
                $arrNotasxFechas[] = [
                    'tarea' => $tarea[0],
                    'asignacion' => $asig,
                    'asignaciones_alumno' => $asignaciones_alum,
                    'promedioAsignaciones' => $promedioAsig,
                ];
            }
            if($idgrupo==null && $idalumno==null){
                $data=array('code'=>'ok','data'=>$arrNotasxFechas);
                echo json_encode($data);
            } else {
                return $arrNotasxFechas;
            }
            return parent::getEsquema();
        }catch(Exception $ex){
            $data=array('code'=>'Error','mensaje'=>JrTexto::_($ex->getMessage()));
            echo json_encode($data);
        }
    }

    public function xSeguimientoGrupo()
    {
        $this->documento->plantilla = 'returnjson';
        try{
            global $aplicacion;
            if(empty($_POST["idgrupo"])) { 
                throw new Exception(JrTexto::_('Error in filtering')); 
            }
            $arrNotasxFechas=array();
            $arrNotasxFechas = $this->xSeguimientoAlumno($_POST["idgrupo"]);

            $data=array('code'=>'ok','data'=>$arrNotasxFechas);
            echo json_encode($data);
            return parent::getEsquema();
        }catch(Exception $ex){
            $data=array('code'=>'Error','mensaje'=>JrTexto::_($ex->getMessage()));
            echo json_encode($data);
        }
    }
/*
	// ========================== Funciones xajax ========================== //
	public function xSaveTarea(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$frm = $args[0];
				
				if(!empty($frm['pkIdtarea'])) {
					$this->oNegTarea->idtarea = $frm['pkIdtarea'];
				}
				JrCargador::clase("sys_negocio::NegTools", RUTA_SITIO, "sys_negocio");
				$this->oNegTarea->__set('idnivel',@$frm["opcIdnivel"]);
				$this->oNegTarea->__set('idunidad',@$frm["opcIdunidad"]);
				$this->oNegTarea->__set('idactividad',@$frm["opcIdactividad"]);
				$this->oNegTarea->__set('nombre',@$frm["txtNombre"]);
				$this->oNegTarea->__set('descripcion',@$frm["txtDescripcion"]);
				$this->oNegTarea->__set('habilidades',@$frm["txtHabilidades"]);
				$this->oNegTarea->__set('puntajemaximo',@$frm["txtPuntajemaximo"]);
				$this->oNegTarea->__set('puntajeminimo',@$frm["txtPuntajeminimo"]);
				$this->oNegTarea->__set('eliminado',@$frm["txtEliminado"]);
				
			    if(@$frm["accion"]=="Nuevo"){
					$txtFoto=NegTools::subirImagen("foto_",@$frm["txtFoto"], "tarea",false,100,100 );
					$this->oNegTarea->__set('foto',@$txtFoto);
					$res=$this->oNegTarea->agregar();
				}else{
					$archivo=basename($frm["txtFoto_old"]);
					if(!empty($frm["txtFoto"])) $txtFoto=NegTools::subirImagen("foto_",@$frm["txtFoto"], "tarea",false,100,100 );
					$this->oNegTarea->__set('foto',@$txtFoto);
					@unlink(RUTA_SITIO . SD ."static/media/tarea/".$archivo);
					$res=$this->oNegTarea->editar();
			    }
				if(!empty($res)) $oRespAjax->setReturnValue($this->oNegTarea->idtarea);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Failed to save registry')),'warning');
					$oRespAjax->setReturnValue(false);
				}
							
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}

	public function xGetxIDTarea(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegTarea->__set('idtarea', $pk);
				$this->datos = $this->oNegTarea->dataTarea;
				$res=$this->oNegTarea->getXid();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')),  $this->pasarHtml(JrTexto::_('Get record')), 'warning');
				$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}
	public function xEliminar(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegTarea->__set('idtarea', $pk);
				$res=$this->oNegTarea->eliminar();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Error').' '.JrTexto::_('Delete Record')), 'warning');
					$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}

	public function xSetCampo(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
               
				if(empty($args[0])) { return;}
				$this->oNegTarea->setCampo($args[0],$args[1],$args[2]);
				$oRespAjax->setReturnValue(true);
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Atención')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}
*/	     
}