<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		12-07-2017 
 * @copyright	Copyright (C) 12-07-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegBib_estudio', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegBib_det_estudio_autor', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegBib_detalle_usuario_estudio', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegBib_detalle_subcategoria_estudio', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegBib_idioma', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegBib_setting', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegBib_autor', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegBib_editorial', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegBib_tipo_registro', RUTA_BASE, 'sys_negocio');
class WebBib_estudio extends JrWeb
{
	private $oNegBib_estudio;
	private $oNegBib_idioma;
	private $oNegBib_setting;
	private $oNegBib_autor;
	private $oNegBib_editorial;
	private $oNegBib_tipo_registro;
	private $oNegBib_det_estudio_autor;
	private $oNegBib_detalle_subcategoria_estudio;
	private $oNegBib_detalle_usuario_estudio;
	public function __construct()
	{
		parent::__construct();		
		$this->oNegBib_estudio = new NegBib_estudio;
		$this->oNegBib_det_estudio_autor = new NegBib_det_estudio_autor;
		$this->oNegBib_idioma = new NegBib_idioma;
		$this->oNegBib_setting = new NegBib_setting;
		$this->oNegBib_autor = new NegBib_autor;
		$this->oNegBib_editorial = new NegBib_editorial;
		$this->oNegBib_tipo_registro = new NegBib_tipo_registro;
		$this->oNegBib_detalle_subcategoria_estudio = new NegBib_detalle_subcategoria_estudio;
		$this->oNegBib_detalle_usuario_estudio = new NegBib_detalle_usuario_estudio;
	}

	public function defecto(){
		return $this->listado();
	}

	public function listado()
	{
		try{
			global $aplicacion;			
			/*if(!NegSesion::tiene_acceso('Bib_estudio', 'list')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
            $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
            $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
			//$this->documento->script(null, ConfigSitio::get('tema_general') . '/js/datatables/js/jquery.dataTables.js');
			
			
			$this->datos=$this->oNegBib_estudio->buscar();

						$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Bib_estudio'), true);
			$this->esquema = 'bib_estudio-list';			
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}


	public function agregar()
	{
		try {
			global $aplicacion;			
			/*if(!NegSesion::tiene_acceso('Bib_estudio', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->frmaccion='Nuevo';
			$this->tipo = $this->oNegBib_tipo->buscar();
			$this->autores = $this->oNegBib_autor->buscar();
			$this->documento->setTitulo(JrTexto::_('Bib_estudio').' /'.JrTexto::_('New'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function editar()
	{
		try {
			global $aplicacion;
			
			/*if(!NegSesion::tiene_acceso('Bib_estudio', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}*/
			$this->frmaccion='Editar';
			$this->tipo = $this->oNegBib_tipo->buscar();
			$this->autores = $this->oNegBib_autor->buscar();
			$this->oNegBib_estudio->id_estudio = @$_GET['id'];
			$this->datos = $this->oNegBib_estudio->dataBib_estudio;
			$this->pk=@$_GET['id'];
			$this->documento->setTitulo(JrTexto::_('Bib_estudio').' /'.JrTexto::_('Edit'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}


	public function ver(){
		try{
			global $aplicacion;						
			$this->oNegBib_estudio->id_estudio = @$_GET['id'];
			$this->datos = $this->oNegBib_estudio->dataBib_estudio;
						
				$this->documento->script(null, ConfigSitio::get("tema_general") . "/js/cropping/cropper.min.js");
				$this->documento->script(null, ConfigSitio::get("tema_general") . "/js/cropping/main3.js");
							$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Bib_estudio').' /'.JrTexto::_('see'), true);
			$this->esquema = 'bib_estudio-see';
			$this->documento->plantilla = $tpl;
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	private function form()
	{
		try {
			global $aplicacion;	
			
			//$this->documento->script(null, 'http://tinymce.cachefly.net/4.2/tinymce.min.js');			
			$this->esquema = 'bib_estudio-frm';
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}


	//------------------http angular-----------------------//
	public function xGetEstudio(){

		$this->documento->plantilla = 'returnjson';
		try{
			global $aplicacion;	
			$this->datos=$this->oNegBib_estudio->buscar(array('id_estudio'=>@$_GET['id']));
			if(!empty($this->datos)){$this->datos=$this->datos[0];}
			echo json_encode(array('code'=>'ok','data'=>$this->datos));
		}catch(Exception $e){
			$data=array('code'=>'Error','msj'=>JrTexto::_($e->getMessage()));
            echo json_encode($data);
		}
	}
	public function xLibro(){
		//----
		$postdata = file_get_contents("php://input");
			$request = json_decode($postdata,true);
			$this->oNegBib_estudio->__set('nombre',$request['nombre']);
		//---
		$this->documento->plantilla = 'returnjson';
		try{
			global $aplicacion;	
			$this->datos =$this->oNegBib_estudio->listar();
			echo json_encode(array('code'=>'ok','data'=>$this->datos));
		}catch(Exception $e){
			$data=array('code'=>'Error','msj'=>JrTexto::_($e->getMessage()));
            echo json_encode($data);
		}
	}
	public function xEstudio(){
		$this->documento->plantilla = 'returnjson';
		try{
			global $aplicacion;	
			$this->oNegBib_estudio->setLimite(0,15500);
			$this->datos =$this->oNegBib_estudio->buscar();
			echo json_encode(array('code'=>'ok','data'=>$this->datos));
		}catch(Exception $e){
			$data=array('code'=>'Error','msj'=>JrTexto::_($e->getMessage()));
            echo json_encode($data);
		}
	}

	/*public function xBusca(){
		$this->documento->plantilla = 'returnjson';
		try{
			global $aplicacion;	
			$postdata = file_get_contents("php://input");
			$request = json_decode($postdata,true);
			//$this->datos = '';
			echo json_encode(array('code'=>'ok','data'=>$request['nombre']));
			echo json_encode(array('code'=>'ok','data'=>$request['editorial']));
			echo json_encode(array('code'=>'ok','data'=>$request['idioma']));
			//$this->datos = $this->oNegBib_estudio->agregar();
		}catch(Exception $e){
			$data=array('code'=>'Error','msj'=>JrTexto::_($e->getMessage()));
            echo json_encode($data);
		}
	}*/
   	public function xRegistrarEstudio(){
		$this->documento->plantilla = 'returnjson';
		try{
			global $aplicacion;	
			$postdata = file_get_contents("php://input");
			$request = json_decode($postdata,true);
			//$this->datos = '';
			if(!empty($request['id'])) {
					$this->oNegBib_estudio->id_estudio = $request['id'];
				}
				JrCargador::clase("sys_negocio::NegTools", RUTA_SITIO, "sys_negocio");
					$this->oNegBib_estudio->__set('paginas',$request['paginas']);
					$this->oNegBib_estudio->__set('codigo_tipo',$request['codigo_tipo']);
					$this->oNegBib_estudio->__set('codigo',$request['codigo']);
					$this->oNegBib_estudio->__set('nombre',$request['nombre']);
					$this->oNegBib_estudio->__set('nombre_abreviado',$request['nombre_abreviado']);
					$this->oNegBib_estudio->__set('precio',$request['precio']);
					$this->oNegBib_estudio->__set('edicion',$request['edicion']);
					$this->oNegBib_estudio->__set('fec_publicacion',$request['fec_publicacion']);
					$this->oNegBib_estudio->__set('fec_creacion',$request['fec_creacion']);
					$this->oNegBib_estudio->__set('fec_modificacion',$request['fec_modificacion']);
					$this->oNegBib_estudio->__set('foto',$request['foto']);
					$this->oNegBib_estudio->__set('archivo',$request['archivo']);
					$this->oNegBib_estudio->__set('link',$request['link']);
					$this->oNegBib_estudio->__set('lugar',$request['lugar']);
					$this->oNegBib_estudio->__set('condicion',$request['condicion']);
					$this->oNegBib_estudio->__set('resumen',$request['resumen']);
					$this->oNegBib_estudio->__set('id_idioma',$request['id_idioma']);
					$this->oNegBib_estudio->__set('id_tipo',$request['id_tipo']);
					$this->oNegBib_estudio->__set('id_detalle',$request['id_detalle']);//detalle_grados_areas//
					$this->oNegBib_estudio->__set('id_setting',$request['id_setting']);
					$this->oNegBib_estudio->__set('id_editorial',$request['id_editorial']);
					if(!empty($request['id'])) {
						$res=$this->oNegBib_estudio->editar();
					}else{
						$res=$this->oNegBib_estudio->agregar();	
					}
					$id_estudio = $this->oNegBib_estudio->id_estudio;
					$autores=$request['autor'];
					$det_categoria =$request['detalle'];
					foreach ($autores as $a){
						if ($a['estado']=='true') {
							$this->oNegBib_det_estudio_autor->__set('id_estudio',$id_estudio);
							$this->oNegBib_det_estudio_autor->__set('id_autor',$a['id_autor']);
							$resA=$this->oNegBib_det_estudio_autor->agregar();
						}
					 	
					} 
					 foreach ($det_categoria as $ds){
					 	if ($ds['estado']) {
					 		$this->oNegBib_detalle_subcategoria_estudio->__set('id_subcategoria',$ds['id_subcategoria']);
							$this->oNegBib_detalle_subcategoria_estudio->__set('id_estudio',$id_estudio);
							$res=$this->oNegBib_detalle_subcategoria_estudio->agregar();
					}
					 	
					} 
			echo json_encode(array('code'=>'ok','data'=>$autores));
		}catch(Exception $e){
			$data=array('code'=>'Error','msj'=>JrTexto::_($e->getMessage()));
            echo json_encode($data);
		}
	}
    public function xEliminarEstudio(){
        try{
            $this->documento->plantilla = 'returnjson';
			global $aplicacion;	
            $id=@$_GET['id_estudio'];
            $res=$this->oNegBib_estudio->eliminar($id);
            echo json_encode(array('code'=>'ok','data'=>$res));
            exit;
        }catch(Exception $e){
			$data=array('code'=>'Error','msj'=>JrTexto::_($e->getMessage()));
            echo json_encode($data);
		}
        
    } 
    public function xBuscar($filtros){
        try{
            $this->documento->plantilla = 'returnjson';
			global $aplicacion;	
            $filtros=@$_GET['filtros'];
            $res=$this->oNegBib_estudio->buscar($filtros);
            echo json_encode(array('code'=>'ok','data'=>$res));
            exit;
        }catch(Exception $e){
			$data=array('code'=>'Error','msj'=>JrTexto::_($e->getMessage()));
            echo json_encode($data);
		}
        
    }

    public function xAgregar(){
        try{
            $this->documento->plantilla = 'returnjson';
			global $aplicacion;	

			$postdata = file_get_contents("php://input");
			$request = json_decode($postdata,true);

			$usuarioAct = NegSesion::getUsuario();

            $this->oNegBib_detalle_usuario_estudio->__set('id_estudio',$request['id_estudio']);
			$this->oNegBib_detalle_usuario_estudio->__set('id_personal',$usuarioAct['dni']);
			$res=$this->oNegBib_detalle_usuario_estudio->agregar();
            echo json_encode(array('code'=>'ok','data'=>$res));
        }catch(Exception $e){
			$data=array('code'=>'Error','msj'=>JrTexto::_($e->getMessage()));
            echo json_encode($data);
		}
        
    }    
    public function xEnviarCorreo(){
        try{
            $this->documento->plantilla = 'returnjson';
			global $aplicacion;	

			$postdata = file_get_contents("php://input");
			$request = json_decode($postdata,true);

			$destinatario =$request['nombre'];
			$asunto = $request['asunto'];
			$mensaje = $request['mensaje'];
			echo($destinatario);
			if(mail('pp@pp.com', $asunto, $mensaje)){

			}else{

			}
        }catch(Exception $e){
			$data=array('code'=>'Error al enviar correo','msj'=>JrTexto::_($e->getMessage()));
            echo json_encode($data);
		}
        
    } 

	/*public function uploadFile(){

        $file = $_FILES["file"]["name"];
            $intipo='';
			$tipo=@$_POST["tipo"];
			if($tipo=='image'){
				$intipo=array('jpg','jpeg','png');
			}elseif($tipo=='pdf'){
				$intipo=array('pdf', 'PDF');
			}elseif($tipo=='audio'){
				$intipo=array('mp3', 'aac');
			}
			if(!in_array($ext, $intipo)){
				echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('File type incorrect')));
				exit(0);
			}
        //$time=time();
        $dir_media=RUTA_BASE . 'static' . SD . 'libreria' . SD . $tipo;
        if(!is_dir($dir_media))
            mkdir($dir_media, 0777);
        if($file && move_uploaded_file($_FILES["file"]["tmp_name"], $dir_media. SD .$file))
        {
             
        }
        echo json_encode(array('code'=>'ok','data'=>"../static/library/".$file));
        //return "/files/".$time."_".$file;      
    }*/     

}