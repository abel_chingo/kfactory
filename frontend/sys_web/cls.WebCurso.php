<?php
/**
 * @autor       Abel Chingo Tello, ACHT
 * @fecha       2017-07-31
 * @copyright   Copyright (C) 2016. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
#JrCargador::clase('sys_negocio::Neg...', RUTA_BASE, 'sys_...');
class WebCurso extends JrWeb
{
    #private $oNeg...;

    public function __construct()
    {
        parent::__construct();
        #$this->oNeg... = new Neg...;
    }

    public function defecto()
    {
        try {
            global $aplicacion;
            $this->documento->setTitulo(JrTexto::_('Course'), true);
            $this->usuarioAct = NegSesion::getUsuario();
            $rol=$this->usuarioAct["rol"];
            if(strtolower($rol)=="alumno"){
                $this->defectoAlumno();
                $this->documento->plantilla = 'alumno/curso';
                $this->esquema = 'alumno/curso';
            }else{
                $this->esquema = 'docente/inicio';
                $this->documento->plantilla = 'inicio';
            }
            return parent::getEsquema();
        } catch(Exception $e) {
            return $aplicacion->error(JrTexto::_($e->getMessage()));
        }
    }

    private function defectoAlumno()
    {
        $idCurso = (isset($_GET['id']))?$_GET['id']:0;
        $this->breadcrumb = [
            [ 'texto'=> ucfirst(JrTexto::_('course')), 'link'=> '/curso/?id='.$idCurso ],
            [ 'texto'=> 'Nombre del curso' ],
        ];
    }

    public function informacion()
    {
        try {
            global $aplicacion;
            $this->documento->setTitulo(JrTexto::_('Information'), true);
            $this->idCurso = (isset($_GET['id']))?$_GET['id']:0;
            $this->breadcrumb = [
                [ 'texto'=> ucfirst(JrTexto::_('course')), 'link'=> '/curso/?id='.$this->idCurso ],
                [ 'texto'=> ucfirst(JrTexto::_('information')), 'link'=> '/curso/informacion/?id='.$this->idCurso ],
                [ 'texto'=> 'Nombre del curso' ],
            ];
            $this->documento->plantilla = 'alumno/curso';
            $this->esquema = 'alumno/curso_info';
            return parent::getEsquema();
        } catch (Exception $e) {
            return $aplicacion->error(JrTexto::_($e->getMessage()));
        }
    }

    public function horario()
    {
        try {
            global $aplicacion;
            $this->documento->setTitulo(JrTexto::_('Schedule'), true);
            $this->idCurso = (isset($_GET['id']))?$_GET['id']:0;
            $this->breadcrumb = [
                [ 'texto'=> ucfirst(JrTexto::_('course')), 'link'=> '/curso/?id='.$this->idCurso ],
                [ 'texto'=> ucfirst(JrTexto::_('schedule')), 'link'=> '/curso/horario/?id='.$this->idCurso ],
                [ 'texto'=> 'Nombre del curso' ],
            ];
            $this->documento->plantilla = 'alumno/curso';
            $this->esquema = 'alumno/horario';
            return parent::getEsquema();
        } catch (Exception $e) {
            return $aplicacion->error(JrTexto::_($e->getMessage()));
        }
    }

    public function companieros()
    {
        try {
            global $aplicacion;
            $this->documento->setTitulo(JrTexto::_('Classmates'), true);
            $this->idCurso = (isset($_GET['id']))?$_GET['id']:0;
            $this->breadcrumb = [
                [ 'texto'=> ucfirst(JrTexto::_('course')), 'link'=> '/curso/?id='.$this->idCurso ],
                [ 'texto'=> ucfirst(JrTexto::_('classmates')), 'link'=> '/curso/companieros/?id='.$this->idCurso ],
                [ 'texto'=> 'Nombre del curso' ],
            ];
            $this->documento->plantilla = 'alumno/curso';
            $this->esquema = 'alumno/curso_companieros';
            return parent::getEsquema();
        } catch (Exception $e) {
            return $aplicacion->error(JrTexto::_($e->getMessage()));
        }
    }

    public function academico()
    {
        try {
            global $aplicacion;
            $this->documento->setTitulo(JrTexto::_('Academic'), true);
            $this->idCurso = (isset($_GET['id']))?$_GET['id']:0;
            $this->breadcrumb = [
                [ 'texto'=> ucfirst(JrTexto::_('course')), 'link'=> '/curso/?id='.$this->idCurso ],
                [ 'texto'=> ucfirst(JrTexto::_('academic')), 'link'=> '/curso/academico/?id='.$this->idCurso ],
                [ 'texto'=> 'Nombre del curso' ],
            ];
            $this->documento->plantilla = 'alumno/curso';
            $this->esquema = 'alumno/academico';
            return parent::getEsquema();
        } catch (Exception $e) {
            return $aplicacion->error(JrTexto::_($e->getMessage()));
        }
    }

    public function tarea()
    {
        try {
            global $aplicacion;
            $this->documento->setTitulo(JrTexto::_('Homework'), true);
            $this->idCurso = (isset($_GET['id']))?$_GET['id']:0;

            $this->tareasPend = [];
            $this->tareasFin = [];

            $this->breadcrumb = [
                [ 'texto'=> ucfirst(JrTexto::_('course')), 'link'=> '/curso/?id='.$this->idCurso ],
                [ 'texto'=> ucfirst(JrTexto::_('academic')), 'link'=> '/curso/academico/?id='.$this->idCurso ],
                [ 'texto'=> 'Nombre del curso' ],
            ];
            $this->documento->plantilla = 'alumno/curso';
            $this->esquema = 'tarea/tarea-list-alum';
            return parent::getEsquema();
        } catch (Exception $e) {
            return $aplicacion->error(JrTexto::_($e->getMessage()));
        }
    }

    public function examenes()
    {
        try {
            global $aplicacion;
            $this->documento->setTitulo(JrTexto::_('Homework'), true);
            $this->idCurso = (isset($_GET['id']))?$_GET['id']:0;

            $filtros["estado"]=1;

            $this->breadcrumb = [
                [ 'texto'=> ucfirst(JrTexto::_('course')), 'link'=> '/curso/?id='.$this->idCurso ],
                [ 'texto'=> ucfirst(JrTexto::_('assessments')), 'link'=> '/curso/examenes/?id='.$this->idCurso ],
                [ 'texto'=> 'Nombre del curso' ],
            ];
            $this->documento->plantilla = 'alumno/curso';
            $this->esquema = 'examenes/alu_publicados';
            return parent::getEsquema();
        } catch (Exception $e) {
            return $aplicacion->error(JrTexto::_($e->getMessage()));
        }
    }
}