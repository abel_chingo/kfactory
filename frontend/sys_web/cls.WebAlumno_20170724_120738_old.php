<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		24-03-2017 
 * @copyright	Copyright (C) 24-03-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegAlumno', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegGrupo_matricula', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegActividad_alumno', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegActividad_detalle', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegNiveles', RUTA_BASE, 'sys_negocio');
class WebAlumno extends JrWeb
{
	private $oNegAlumno;
    private $oNegGrupo_matricula;
	private $oNegActividad_alumno;
	private $oNegActividad_detalle;
	private $oNegNiveles;
	public function __construct()
	{
		parent::__construct();		
		$this->oNegAlumno = new NegAlumno;
        $this->oNegGrupo_matricula = new NegGrupo_matricula;
		$this->oNegActividad_alumno = new NegActividad_alumno;
		$this->oNegActividad_detalle = new NegActividad_detalle;
		$this->oNegNiveles = new NegNiveles;
	}

	public function defecto(){
		return $this->listado();
	}

	public function listado()
	{
		try{
			global $aplicacion;	
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
            $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
            $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
			//$this->documento->script(null, ConfigSitio::get('tema_general') . '/js/datatables/js/jquery.dataTables.js');
						
			$this->datos=$this->oNegAlumno->buscar();

			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Alumno'), true);
			$this->esquema = 'alumno-list';			
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}


	public function agregar()
	{
		try {
			global $aplicacion;			
			if(!NegSesion::tiene_acceso('Alumno', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->frmaccion='Nuevo';
			$this->documento->setTitulo(JrTexto::_('Alumno').' /'.JrTexto::_('New'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function editar()
	{
		try {
			global $aplicacion;
			
			if(!NegSesion::tiene_acceso('Alumno', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->frmaccion='Editar';
			$this->oNegAlumno->dni = @$_GET['id'];
			$this->datos = $this->oNegAlumno->dataAlumno;
			$this->pk=@$_GET['id'];
			$this->documento->setTitulo(JrTexto::_('Alumno').' /'.JrTexto::_('Edit'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function ver(){
		try{
			global $aplicacion;						
			$this->oNegAlumno->dni = @$_GET['id'];
			$this->datos = $this->oNegAlumno->dataAlumno;
									$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Alumno').' /'.JrTexto::_('see'), true);
			$this->esquema = 'alumno-see';
			$this->documento->plantilla = $tpl;
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	private function form()
	{
		try {
			global $aplicacion;	
			
			//$this->documento->script(null, 'http://tinymce.cachefly.net/4.2/tinymce.min.js');			
			$this->esquema = 'alumno-frm';
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function json_datosxalumno()
	{
		$this->documento->plantilla = 'returnjson';
        try {
            global $aplicacion;
            if(empty($_POST["id"]) || empty($_POST["tipo"])) { 
                throw new Exception(JrTexto::_('Error in filtering')); 
            }
            $promXHab = [];
            $this->alumnos = [];
            if($_POST['tipo']=='A'){
            	$filtros["idalumno"]=$_POST["id"];
            } elseif ($_POST['tipo']=='G') {
            	$filtros['idgrupo']=$_POST["id"];
            }
            $exams_alum=$this->oNegGrupo_matricula->buscar($filtros);
            foreach ($exams_alum as $al) {
            	$this->oNegAlumno->dni = $al['idalumno'];
            	$alumno=$this->oNegAlumno->getXid();
            	
            	$ultimaActividad=$this->oNegActividad_alumno->ultimaActividadxAlum($al['idalumno']);
            	
            	if($alumno['estado']==1){
            		$new_alum = [];
	            	$new_alum = [
	            		'dni' => $alumno['dni'],
	            		'ape_paterno' => $alumno['ape_paterno'],
	            		'ape_materno' => $alumno['ape_materno'],
	            		'nombre' => $alumno['nombre'],
	            		'email' => $alumno['email'],
	            		'foto' => $alumno['foto'],
	            		'ultima_actividad' => $ultimaActividad,
	            	];
	            	$this->alumnos[] = $new_alum;
            	}
            }
            $data=array('code'=>'ok','data'=>$this->alumnos);
            echo json_encode($data);
            return parent::getEsquema();
        } catch (Exception $e) {
            $data=array('code'=>'Error','mensaje'=>JrTexto::_($ex->getMessage()));
            echo json_encode($data);
        }
	}
	// ========================== Funciones xajax ========================== //
	public function xSaveAlumno(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$frm = $args[0];
				
				if(!empty($frm['pkDni'])) {
					$this->oNegAlumno->dni = $frm['pkDni'];
				}
				
				$this->oNegAlumno->__set('ape_paterno',@$frm["txtApe_paterno"]);
					$this->oNegAlumno->__set('ape_materno',@$frm["txtApe_materno"]);
					$this->oNegAlumno->__set('nombre',@$frm["txtNombre"]);
					$this->oNegAlumno->__set('fechanac',@$frm["txtFechanac"]);
					$this->oNegAlumno->__set('sexo',@$frm["txtSexo"]);
					$this->oNegAlumno->__set('estado_civil',@$frm["txtEstado_civil"]);
					$this->oNegAlumno->__set('ubigeo',@$frm["txtUbigeo"]);
					$this->oNegAlumno->__set('urbanizacion',@$frm["txtUrbanizacion"]);
					$this->oNegAlumno->__set('direccion',@$frm["txtDireccion"]);
					$this->oNegAlumno->__set('telefono',@$frm["txtTelefono"]);
					$this->oNegAlumno->__set('celular',@$frm["txtCelular"]);
					$this->oNegAlumno->__set('email',@$frm["txtEmail"]);
					$this->oNegAlumno->__set('idugel',@$frm["txtIdugel"]);
					$this->oNegAlumno->__set('regusuario',@$frm["txtRegusuario"]);
					$this->oNegAlumno->__set('regfecha',@$frm["txtRegfecha"]);
					$this->oNegAlumno->__set('usuario',@$frm["txtUsuario"]);
					$this->oNegAlumno->__set('clave',@$frm["txtClave"]);
					$this->oNegAlumno->__set('token',@$frm["txtToken"]);
					$this->oNegAlumno->__set('foto',@$frm["txtFoto"]);
					$this->oNegAlumno->__set('estado',@$frm["txtEstado"]);
					$this->oNegAlumno->__set('situacion',@$frm["txtSituacion"]);
					
				   if(@$frm["accion"]=="Nuevo"){
									    $res=$this->oNegAlumno->agregar();
					}else{
									    $res=$this->oNegAlumno->editar();
				    }
					if(!empty($res)) $oRespAjax->setReturnValue($this->oNegAlumno->dni);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Failed to save registry')),'warning');
					$oRespAjax->setReturnValue(false);
				}
							
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}

	public function xGetxIDAlumno(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegAlumno->__set('dni', $pk);
				$this->datos = $this->oNegAlumno->dataAlumno;
				$res=$this->oNegAlumno->getXid();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')),  $this->pasarHtml(JrTexto::_('Get record')), 'warning');
				$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}
	public function xEliminar(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegAlumno->__set('dni', $pk);
				$res=$this->oNegAlumno->eliminar();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Error').' '.JrTexto::_('Delete Record')), 'warning');
					$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}

	     
}