<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		13-01-2017 
 * @copyright	Copyright (C) 13-01-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegAmbiente', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegLocal', RUTA_BASE, 'sys_negocio');
class WebAmbiente extends JrWeb
{
	private $oNegAmbiente;
	public function __construct()
	{
		parent::__construct();		
		$this->oNegAmbiente = new NegAmbiente;
		$this->oNegLocal = new NegLocal;
	}

	public function defecto(){
		return $this->listado();
	}

	public function listado()
	{
		try{
			global $aplicacion;			
			if(!NegSesion::tiene_acceso('Ambiente', 'list')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
            $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
            $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
			//$this->documento->script(null, ConfigSitio::get('tema_general') . '/js/datatables/js/jquery.dataTables.js');
			
			$this->locales=$this->oNegLocal->buscar();
			$this->idlocal=!empty($_GET["txtLocal"])?$_GET["txtLocal"]:"";

			$this->datos=$this->oNegAmbiente->buscar(array('idlocal'=>$this->idlocal));

						$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Ambiente'), true);
			$this->esquema = 'ambiente-list';			
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}


	public function agregar()
	{
		try {
			global $aplicacion;			
			if(!NegSesion::tiene_acceso('Ambiente', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->frmaccion='Nuevo';
			$this->documento->setTitulo(JrTexto::_('Ambiente').' /'.JrTexto::_('New'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function editar()
	{
		try {
			global $aplicacion;
			
			if(!NegSesion::tiene_acceso('Ambiente', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->frmaccion='Editar';
			$this->oNegAmbiente->idambiente = @$_GET['id'];
			$this->datos = $this->oNegAmbiente->dataAmbiente;
			$this->pk=@$_GET['id'];
			$this->documento->setTitulo(JrTexto::_('Ambiente').' /'.JrTexto::_('Edit'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}


	public function ver(){
		try{
			global $aplicacion;						
			$this->oNegAmbiente->idambiente = @$_GET['id'];
			$this->datos = $this->oNegAmbiente->dataAmbiente;
									$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Ambiente').' /'.JrTexto::_('see'), true);
			$this->esquema = 'ambiente-see';
			$this->documento->plantilla = $tpl;
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	private function form()
	{
		try {
			global $aplicacion;	
			
			//$this->documento->script(null, 'http://tinymce.cachefly.net/4.2/tinymce.min.js');			
			$this->esquema = 'ambiente-frm';
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';

			$this->locales=$this->oNegLocal->buscar(array('tipo'=>'C'));

			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function getxlocal()
	{
		$this->documento->plantilla = 'returnjson';
		try{
			global $aplicacion;
			$filtros = [];
			$filtros['idlocal'] = $_POST['idlocal'];
			$this->ambientes=$this->oNegAmbiente->buscar($filtros);
			
			$data=array('code'=>'ok','data'=>$this->ambientes);
            echo json_encode($data);
            return parent::getEsquema();
		}catch(Exception $e) {
			$data=array('code'=>'Error','mensaje'=>JrTexto::_($e->getMessage()));
            echo json_encode($data);
		}
	}

	// ========================== Funciones xajax ========================== //
	public function xSaveAmbiente(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$frm = $args[0];
				
				if(!empty($frm['pkIdambiente'])) {
					$this->oNegAmbiente->idambiente = $frm['pkIdambiente'];
				}
				
				$this->oNegAmbiente->__set('idlocal',@$frm["txtIdlocal"]);
					$this->oNegAmbiente->__set('numero',@$frm["txtNumero"]);
					$this->oNegAmbiente->__set('capacidad',@$frm["txtCapacidad"]);
					$this->oNegAmbiente->__set('tipo',@$frm["txtTipo"]);
					$this->oNegAmbiente->__set('estado',@$frm["txtEstado"]);
					$this->oNegAmbiente->__set('turno',@$frm["txtTurno"]);
					
				   if(@$frm["accion"]=="Nuevo"){
									    $res=$this->oNegAmbiente->agregar();
					}else{
									    $res=$this->oNegAmbiente->editar();
				    }
					if(!empty($res)) $oRespAjax->setReturnValue($this->oNegAmbiente->idambiente);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Failed to save registry')),'warning');
					$oRespAjax->setReturnValue(false);
				}
							
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}

	public function xGetxIDAmbiente(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegAmbiente->__set('idambiente', $pk);
				$this->datos = $this->oNegAmbiente->dataAmbiente;
				$res=$this->oNegAmbiente->getXid();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')),  $this->pasarHtml(JrTexto::_('Get record')), 'warning');
				$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}
	public function xEliminar(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegAmbiente->__set('idambiente', $pk);
				$res=$this->oNegAmbiente->eliminar();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Error').' '.JrTexto::_('Delete Record')), 'warning');
					$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}

	public function xSetCampo(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
               
				if(empty($args[0])) { return;}
				$this->oNegAmbiente->setCampo($args[0],$args[1],$args[2]);
				$oRespAjax->setReturnValue(true);
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Atención')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}
	     
}