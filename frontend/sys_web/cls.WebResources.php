<?php
 /**
 * @autor       Generador Abel Chingo Tello, ACHT
 * @fecha       16-11-2016 
 * @copyright   Copyright (C) 16-11-2016. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegActividades', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegActividad', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegNiveles', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegMetodologia_habilidad', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegRecord', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegResources', RUTA_BASE, 'sys_negocio');
class WebResources extends JrWeb
{
    protected $oNegActividades;
    private $oNegRecord;
    private $oNegResources;
   

    public function __construct()
    {
        parent::__construct();      
        $this->usuarioAct = NegSesion::getUsuario();
        $this->oNegResources = new NegResources;
        $this->oNegRecord = new NegRecord;

    }

    public function defecto(){
        return false;
    }

    public function teacherresources()
    {
        try{
            global $aplicacion;          

            /*$this->documento->script('jquery.min', '/libs/resource/js/dock/');
            $this->documento->script('fisheye-iutil.min', '/libs/resource/js/dock/');
            $this->documento->script('dock-example1', '/libs/resource/js/dock/');
            $this->documento->script('jquery.jqDock.min', '/libs/resource/js/dock/');
            $this->documento->script('stack-1', '/libs/resource/js/dock/');
            $this->documento->script('stack-2', '/libs/resource/js/dock/');*/

            $this->idnivel=!empty($_GET["idnivel"])?$_GET["idnivel"]:0;
            $this->idunidad=!empty($_GET["idunidad"])?$_GET["idunidad"]:0;
            $this->idactividad=!empty($_GET["idactividad"])?$_GET["idactividad"]:0;          
            $this->datos=$this->oNegResources->buscar(Array('idnivel'=>$this->idnivel,'idunidad'=>$this->idunidad,'idactividad'=>$this->idactividad,'tool'=>'R'));
            $this->documento->setTitulo(JrTexto::_('Actividad'), true);
            $this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
            $this->esquema = 'tools/teacherresources';            
            return parent::getEsquema();
        }catch(Exception $e) {
            $aplicacion->encolarMsj($e->getMessage(), false, 'error');
            $aplicacion->redir();
        }
    }

    public function speakinglabs()
    {
        try{
            global $aplicacion;
            $this->documento->setTitulo(JrTexto::_('Actividad'), true);
            $this->idnivel=!empty($_GET["idnivel"])?$_GET["idnivel"]:0;
            $this->idunidad=!empty($_GET["idunidad"])?$_GET["idunidad"]:0;
            $this->idactividad=!empty($_GET["idactividad"])?$_GET["idactividad"]:0;          
            $this->datos=$this->oNegResources->buscar(Array('idnivel'=>$this->idnivel,'idunidad'=>$this->idunidad,'idactividad'=>$this->idactividad,'tool'=>'A'));          

            $this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
            $this->esquema = 'tools/recording';
            return parent::getEsquema();
        }catch(Exception $e) {
            $aplicacion->encolarMsj($e->getMessage(), false, 'error');
            $aplicacion->redir();
        }
    }

    public function workbook()
    {
        try{
            global $aplicacion;         
            $this->idnivel=!empty($_GET["idnivel"])?$_GET["idnivel"]:0;
            $this->idunidad=!empty($_GET["idunidad"])?$_GET["idunidad"]:0;
            $this->idactividad=!empty($_GET["idactividad"])?$_GET["idactividad"]:0;          
            $this->datos=$this->oNegResources->buscar(Array('idnivel'=>$this->idnivel,'idunidad'=>$this->idunidad,'idactividad'=>$this->idactividad,'tool'=>'P'));
            $this->documento->setTitulo(JrTexto::_('Actividad'), true);
            $this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
            $this->esquema = 'tools/workbook';            
            return parent::getEsquema();
        }catch(Exception $e) {
            $aplicacion->encolarMsj($e->getMessage(), false, 'error');
            $aplicacion->redir();
        }
    }

    public function vocabulary()
    {
        try{
            global $aplicacion;          
            $this->idnivel=!empty($_GET["idnivel"])?$_GET["idnivel"]:0;
            $this->idunidad=!empty($_GET["idunidad"])?$_GET["idunidad"]:0;
            $this->idactividad=!empty($_GET["idactividad"])?$_GET["idactividad"]:0;
            $this->datos=$this->oNegResources->buscar(Array('idnivel'=>$this->idnivel,'idunidad'=>$this->idunidad,'idactividad'=>$this->idactividad,'tool'=>'V'));
            $this->documento->setTitulo(JrTexto::_('Actividad'), true);
            $this->documento->script('tinymce.min', '/libs/tinymce/');
            $this->documento->script('editactividad','/js/');
            $this->documento->script('chinput', '/libs/tinymce/plugins/chingo/');
            $this->documento->script('chimage', '/libs/tinymce/plugins/chingo/');
            $this->documento->script('chivideo', '/libs/tinymce/plugins/chingo/');
            $this->documento->script('chiaudio', '/libs/tinymce/plugins/chingo/');
            $this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
            $this->esquema = 'tools/vocabulary';            
            return parent::getEsquema();
        }catch(Exception $e) {
            $aplicacion->encolarMsj($e->getMessage(), false, 'error');
            $aplicacion->redir();
        }
    }
    public function games()
    {
        try{
            global $aplicacion;
          
            $this->idnivel=!empty($_GET["idnivel"])?$_GET["idnivel"]:0;
            $this->idunidad=!empty($_GET["idunidad"])?$_GET["idunidad"]:0;
            $this->idactividad=!empty($_GET["idactividad"])?$_GET["idactividad"]:0;
            $this->documento->setTitulo(JrTexto::_('Game'), true);
            $this->datos=$this->oNegResources->buscar(Array('idnivel'=>$this->idnivel,'idunidad'=>$this->idunidad,'idactividad'=>$this->idactividad,'tool'=>'G'));
            $this->documento->setTitulo(JrTexto::_('Actividad'), true);
            $this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
            $this->esquema = 'tools/games';            
            return parent::getEsquema();
        }catch(Exception $e) {
            $aplicacion->encolarMsj($e->getMessage(), false, 'error');
            $aplicacion->redir();
        }
    }
    public function links()
    {
        try{          
            global $aplicacion;
            $this->idnivel=!empty($_GET["idnivel"])?$_GET["idnivel"]:0;
            $this->idunidad=!empty($_GET["idunidad"])?$_GET["idunidad"]:0;
            $this->idactividad=!empty($_GET["idactividad"])?$_GET["idactividad"]:0;
            $this->documento->setTitulo(JrTexto::_('Actividad'), true);
            $this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
            $this->datos=$this->oNegResources->buscar(Array('idnivel'=>$this->idnivel,'idunidad'=>$this->idunidad,'idactividad'=>$this->idactividad,'tool'=>'L'));
            $this->esquema = 'tools/links';
            return parent::getEsquema();
        }catch(Exception $e) {
            $aplicacion->encolarMsj($e->getMessage(), false, 'error');
            $aplicacion->redir();
        }
    }

//funciones ajax
    public function xSaveResource(&$oRespAjax = null, $args = null)
    {
        if(is_a($oRespAjax, 'xajaxResponse')) {
            try {

                if(empty($args[0])) { return;}
                $frm=$args[0];
                $rutabase=$this->documento->getUrlBase();                
                $texto=@str_replace($rutabase,'__xRUTABASEx__',trim(@$frm["texto"]));
                $pos=@$frm['pos'];
                //var_dump($pos);

                $this->oNegResources->__set('pos',$pos);
                $this->oNegResources->__set('texto',$texto);
                $this->oNegResources->__set('titulo',@$frm['titulo']);
                
                //if(empty($texto)) return;
                $usuarioAct = NegSesion::getUsuario();
                $this->oNegResources->__set('idnivel',@$frm['idNivel']);
                $this->oNegResources->__set('idunidad',@$frm['idUnidad']);
                $this->oNegResources->__set('idactividad',@$frm['idActividad']);
                $this->oNegResources->__set('idpersonal',@$usuarioAct["dni"]);
                $this->oNegResources->__set('publicado',@$frm["publicado"]);
                $this->oNegResources->__set('compartido',@$frm["compartido"]);
                
                //$this->oNegResources->__set('caratula',substr(@$frm['caratula'],-39));
                $caratula=@str_replace($rutabase,'__xRUTABASEx__',trim(@$frm['caratula']));
                $this->oNegResources->__set('caratula',$caratula);
                
                $this->oNegResources->__set('orden',@$frm['orden']);
                $this->oNegResources->__set('tipo',@$frm['tipo']);
                //$this->oNegResources->__set('orden',1);
                $res=$this->oNegResources->agregar();
                
                if(!empty($res)) $oRespAjax->setReturnValue($this->oNegResources->idrecurso);
                else{
                    $oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Failed to save registry')),'warning');
                    $oRespAjax->setReturnValue(false);
                }
                            
            } catch(Exception $e) {
                $oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
            } 
        }
    }

    public function xEliminar(&$oRespAjax = null, $args = null)
    {
        if(is_a($oRespAjax, 'xajaxResponse')) {
            try {
                if(empty($args[0])) { return;}
                $pk = $args[0];
                $this->oNegResources->__set('idtool', $pk);
                $res=$this->oNegResources->eliminar();
                if(!empty($res))
                    $oRespAjax->setReturnValue($res);
                else{
                    $oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Error').' '.JrTexto::_('Delete Record')), 'warning');
                    $oRespAjax->setReturnValue(false);
                }
            } catch(Exception $e) {
                $oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
                $oRespAjax->setReturnValue(false);
            } 
        }
    }

    public function xSetCampo(&$oRespAjax = null, $args = null)
    {
        if(is_a($oRespAjax, 'xajaxResponse')) {
            try {
               
                if(empty($args[0])) { return;}
                $this->oNegResources->setCampo($args[0],$args[1],$args[2]);
                $oRespAjax->setReturnValue(true);
            } catch(Exception $e) {
                $oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Atención')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
            } 
        }
    }   
}