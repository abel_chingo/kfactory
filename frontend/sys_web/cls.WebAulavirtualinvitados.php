<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		24-05-2017 
 * @copyright	Copyright (C) 24-05-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegAulavirtualinvitados', RUTA_BASE, 'sys_negocio');
class WebAulavirtualinvitados extends JrWeb
{
	private $oNegAulavirtualinvitados;
	public function __construct()
	{
		parent::__construct();		
		$this->oNegAulavirtualinvitados = new NegAulavirtualinvitados;
	}

	public function defecto(){
		return $this->listado();
	}

	public function listado()
	{
		try{
			global $aplicacion;			
			if(!NegSesion::tiene_acceso('Aulavirtualinvitados', 'list')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
            $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
            $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
			//$this->documento->script(null, ConfigSitio::get('tema_general') . '/js/datatables/js/jquery.dataTables.js');
			
			
			$this->datos=$this->oNegAulavirtualinvitados->buscar();

						$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Aulavirtualinvitados'), true);
			$this->esquema = 'aulavirtualinvitados-list';			
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}


	public function agregar()
	{
		try {
			global $aplicacion;			
			if(!NegSesion::tiene_acceso('Aulavirtualinvitados', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->frmaccion='Nuevo';
			$this->documento->setTitulo(JrTexto::_('Aulavirtualinvitados').' /'.JrTexto::_('New'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function editar()
	{
		try {
			global $aplicacion;
			
			if(!NegSesion::tiene_acceso('Aulavirtualinvitados', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->frmaccion='Editar';
			$this->oNegAulavirtualinvitados->idinvitado = @$_GET['id'];
			$this->datos = $this->oNegAulavirtualinvitados->dataAulavirtualinvitados;
			$this->pk=@$_GET['id'];
			$this->documento->setTitulo(JrTexto::_('Aulavirtualinvitados').' /'.JrTexto::_('Edit'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}


	public function ver(){
		try{
			global $aplicacion;						
			$this->oNegAulavirtualinvitados->idinvitado = @$_GET['id'];
			$this->datos = $this->oNegAulavirtualinvitados->dataAulavirtualinvitados;
									$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Aulavirtualinvitados').' /'.JrTexto::_('see'), true);
			$this->esquema = 'aulavirtualinvitados-see';
			$this->documento->plantilla = $tpl;
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	private function form()
	{
		try {
			global $aplicacion;	
			
			//$this->documento->script(null, 'http://tinymce.cachefly.net/4.2/tinymce.min.js');			
			$this->esquema = 'aulavirtualinvitados-frm';
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}


	// ========================== Funciones xajax ========================== //
	public function xSaveAulavirtualinvitados(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$frm = $args[0];
				
				if(!empty($frm['pkIdinvitado'])) {
					$this->oNegAulavirtualinvitados->idinvitado = $frm['pkIdinvitado'];
				}
				
				$this->oNegAulavirtualinvitados->__set('idaula',@$frm["txtIdaula"]);
					$this->oNegAulavirtualinvitados->__set('dni',@$frm["txtDni"]);
					$this->oNegAulavirtualinvitados->__set('email',@$frm["txtEmail"]);
					$this->oNegAulavirtualinvitados->__set('asistio',@$frm["txtAsistio"]);
					$this->oNegAulavirtualinvitados->__set('como',@$frm["txtComo"]);
					$this->oNegAulavirtualinvitados->__set('usuario',@$frm["txtUsuario"]);
					
				   if(@$frm["accion"]=="Nuevo"){
									    $res=$this->oNegAulavirtualinvitados->agregar();
					}else{
									    $res=$this->oNegAulavirtualinvitados->editar();
				    }
					if(!empty($res)) $oRespAjax->setReturnValue($this->oNegAulavirtualinvitados->idinvitado);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Failed to save registry')),'warning');
					$oRespAjax->setReturnValue(false);
				}
							
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}

	public function xGetxIDAulavirtualinvitados(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegAulavirtualinvitados->__set('idinvitado', $pk);
				$this->datos = $this->oNegAulavirtualinvitados->dataAulavirtualinvitados;
				$res=$this->oNegAulavirtualinvitados->getXid();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')),  $this->pasarHtml(JrTexto::_('Get record')), 'warning');
				$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}
	public function xEliminar(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegAulavirtualinvitados->__set('idinvitado', $pk);
				$res=$this->oNegAulavirtualinvitados->eliminar();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Error').' '.JrTexto::_('Delete Record')), 'warning');
					$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}

	     
}