<?php
/**
 * @autor		Abel Chingo Tello, ACHT
 * @fecha		2016-06-01
 * @copyright	Copyright (C) 2016. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
class WebAdministrador extends JrWeb
{	
	
	public function __construct()
	{
		parent::__construct();
	}
	
	public function defecto()
	{
		try {
			global $aplicacion;
			$this->documento->setTitulo(JrTexto::_('Dashboard'), true);
			$this->documento->plantilla = 'mantenimientos';
			$this->esquema = 'menu_administrador';			
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}
}