<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		19-07-2017 
 * @copyright	Copyright (C) 19-07-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegUbigeo', RUTA_BASE, 'sys_negocio');
class WebUbigeo extends JrWeb
{
	private $oNegUbigeo;
		
	public function __construct()
	{
		parent::__construct();		
		$this->oNegUbigeo = new NegUbigeo;	
	}

	public function defecto(){
		return $this->listado();
	}

	public function listado()
	{
		try{
			global $aplicacion;			
			if(!NegSesion::tiene_acceso('Ubigeo', 'list')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
            $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
            $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
			//$this->documento->script(null, ConfigSitio::get('tema_general') . '/js/datatables/js/jquery.dataTables.js');
			$filtros=array();
			if(isset($_REQUEST["id_ubigeo"])&&@$_REQUEST["id_ubigeo"]!='')$filtros["id_ubigeo"]=$_REQUEST["id_ubigeo"];
			if(isset($_REQUEST["pais"])&&@$_REQUEST["pais"]!='')$filtros["pais"]=$_REQUEST["pais"];
			if(isset($_REQUEST["departamento"])&&@$_REQUEST["departamento"]!='')$filtros["departamento"]=$_REQUEST["departamento"];
			if(isset($_REQUEST["provincia"])&&@$_REQUEST["provincia"]!='')$filtros["provincia"]=$_REQUEST["provincia"];
			if(isset($_REQUEST["distrito"])&&@$_REQUEST["distrito"]!='')$filtros["distrito"]=$_REQUEST["distrito"];
			if(isset($_REQUEST["ciudad"])&&@$_REQUEST["ciudad"]!='')$filtros["ciudad"]=$_REQUEST["ciudad"];
			$this->oNegUbigeo->setLimite(0,1000000);
			$this->datos=$this->oNegUbigeo->buscar($filtros);
			$filtrospais=$filtrosdepartamentos=$filtrosprovincias=$filtrosdistritos=array();
			$fkpais=$fkdepartamento=$fkprovincia=$fkdistrito=array();
			foreach ($this->datos as $paises) {				
				if($paises["departamento"]=='00'&&$paises["provincia"]=='00'&&$paises["distrito"]=='00')
					array_push($fkpais,$paises);
				if(!isset($fkdepartamento[$paises["pais"]]))$fkdepartamento[$paises["pais"]]=array();
				if(!isset($fkprovincia[$paises["pais"]][$paises["departamento"]]))$fkprovincia[$paises["pais"]][$paises["departamento"]]=array();
				if(!isset($fkdistrito[$paises["pais"]][$paises["departamento"]][$paises["provincia"]]))$fkdistrito[$paises["pais"]][$paises["departamento"]][$paises["provincia"]]=array();
				if($paises["departamento"]!='00'&&$paises["provincia"]=='00'&&$paises["distrito"]=='00')
					array_push($fkdepartamento[$paises["pais"]],$paises);
				if($paises["provincia"]!='00'&&$paises["distrito"]=='00')
					array_push($fkprovincia[$paises["pais"]][$paises["departamento"]],$paises);
				if($paises["distrito"]!='00')
					array_push($fkdistrito[$paises["pais"]][$paises["departamento"]][$paises["provincia"]],$paises);
			}
			sort($fkpais);
			$paises=$fkpais;
			$this->paisid=isset($filtros["pais"])?$filtros["pais"]:$paises[0]["pais"];	
			$departamentos=$fkdepartamento[$this->paisid];
			$this->departamentoid=isset($filtros["departamento"])?$filtros["departamento"]:$departamentos[0]["departamento"];
			$provincias=$fkprovincia[$this->paisid][$this->departamentoid];
			$this->provinciasid=isset($filtros["provincia"])?$filtros["provincia"]:$provincias[0]["provincia"];
			$distritos=$fkdistrito[$this->paisid][$this->departamentoid][$this->provinciasid];
			$this->distritoid=isset($filtros["distrito"])?$filtros["distrito"]:$distritos[0]["distrito"];
			$this->fkpais=$fkpais;
			$this->fkdepartamento=$departamentos;
			$this->fkprovincia=$provincias;
			$this->fkdistrito=$distritos;

			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Ubigeo'), true);
			$this->esquema = 'ubigeo-list';			
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}


	public function agregar()
	{
		try {
			global $aplicacion;			
			if(!NegSesion::tiene_acceso('Ubigeo', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->frmaccion='Nuevo';
			$this->documento->setTitulo(JrTexto::_('Ubigeo').' /'.JrTexto::_('New'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function editar()
	{
		try {
			global $aplicacion;
			
			if(!NegSesion::tiene_acceso('Ubigeo', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->frmaccion='Editar';
			$this->oNegUbigeo->id_ubigeo = @$_GET['id'];
			$this->datos = $this->oNegUbigeo->dataUbigeo;
			$this->pk=@$_GET['id'];
			$this->documento->setTitulo(JrTexto::_('Ubigeo').' /'.JrTexto::_('Edit'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	private function form()
	{
		try {
			global $aplicacion;	
			$this->fkpais=$this->oNegUbigeo->buscar();
			$this->fkdepartamento=$this->oNegUbigeo->buscar();
			$this->fkprovincia=$this->oNegUbigeo->buscar();
			$this->fkdistrito=$this->oNegUbigeo->buscar();
			
			//$this->documento->script(null, 'http://tinymce.cachefly.net/4.2/tinymce.min.js');			
			$this->esquema = 'ubigeo-frm';
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	// ========================== Funciones ajax ========================== //

	public function buscarjson(){
		$this->documento->plantilla = 'blanco';
		try{
			global $aplicacion;			
			if(!NegSesion::tiene_acceso('Ubigeo', 'list')) {
				echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('Restricted access').'!!'));
				exit(0);
			}
			$filtros=array();
			$filtrospais=$filtrosdepartamentos=$filtrosprovincias=$filtrosdistritos=array();
			$fkpais=$fkdepartamento=$fkprovincia=$fkdistrito=array();
			$strfkpais=$strfkdepartamento=$strfkprovincia=$strfkdistrito='';
			if(isset($_REQUEST["id_ubigeo"])&&@$_REQUEST["id_ubigeo"]!='')$filtros["id_ubigeo"]=$_REQUEST["id_ubigeo"];
			if(isset($_REQUEST["pais"])&&@$_REQUEST["pais"]!='')$filtros["pais"]=$_REQUEST["pais"];
			if(isset($_REQUEST["departamento"])&&@$_REQUEST["departamento"]!='')$filtros["departamento"]=$_REQUEST["departamento"];
			if(isset($_REQUEST["provincia"])&&@$_REQUEST["provincia"]!='')$filtros["provincia"]=$_REQUEST["provincia"];
			if(isset($_REQUEST["distrito"])&&@$_REQUEST["distrito"]!='')$filtros["distrito"]=$_REQUEST["distrito"];
			if(isset($_REQUEST["ciudad"])&&@$_REQUEST["ciudad"]!='')$filtros["ciudad"]=$_REQUEST["ciudad"];
			$return=(isset($_REQUEST["return"])&&@$_REQUEST["return"]!='')?$_REQUEST["return"]:'all';
			//var_dump($filtros);
			$this->oNegUbigeo->setLimite(0,1000000);
			$this->datos=$this->oNegUbigeo->buscar($filtros);
			$newdatos=$returnnewdatos=array();
			foreach ($this->datos as $paises){
				if(!isset($fkdepartamento[$paises["pais"]])){
					$fkdepartamento[$paises["pais"]]=array();
					$strfkpais[$paises["pais"]]='';					
				}
				if(!isset($fkprovincia[$paises["pais"].$paises["departamento"]])){
					$fkprovincia[$paises["pais"].$paises["departamento"]]=array();
					$strfkdepartamento[$paises["pais"].$paises["departamento"]]=array();					
				}
				if(!isset($fkdistrito[$paises["pais"].$paises["departamento"].$paises["provincia"]])){
					$fkdistrito[$paises["pais"].$paises["departamento"].$paises["provincia"]]=array();
					$strfkprovincia[$paises["pais"].$paises["departamento"].$paises["provincia"]]='';
					//$strfkdistrito[$paises["pais"].$paises["departamento"].$paises["provincia"]]=array();
				}
				if($paises["departamento"]=='00'&&$paises["provincia"]=='00'&&$paises["distrito"]=='00'){
					array_push($fkpais,$paises);
					$strfkpais[$paises["pais"]]=$paises["ciudad"];
				}
				if($paises["departamento"]!='00'&&$paises["provincia"]=='00'&&$paises["distrito"]=='00'){
					array_push($fkdepartamento[$paises["pais"]],$paises);
					$strfkdepartamento[$paises["pais"].$paises["departamento"]]=$paises["ciudad"];
				}
				if($paises["provincia"]!='00'&&$paises["distrito"]=='00'){
					array_push($fkprovincia[$paises["pais"].$paises["departamento"]],$paises);
					$strfkprovincia[$paises["pais"].$paises["departamento"].$paises["provincia"]]=$paises["ciudad"];
				}
				if($paises["distrito"]!='00'){
					array_push($newdatos,$paises);					
					array_push($fkdistrito[$paises["pais"].$paises["departamento"].$paises["provincia"]],$paises);
					if(!isset($strfkdistrito[$paises["pais"].$paises["id_ubigeo"]])) $strfkdistrito[$paises["pais"].$paises["id_ubigeo"]]='';
					$strfkdistrito[$paises["pais"].$paises["id_ubigeo"]]=$paises["ciudad"];
				}
			}

			if($return=='pais'){
				echo json_encode(array('code'=>'ok','data'=>$fkpais));
			}elseif($return=='departamento'){
				echo json_encode(array('code'=>'ok','data'=>$fkdepartamento));
			}elseif($return=='provincia'){
				echo json_encode(array('code'=>'ok','data'=>$fkprovincia));
			}elseif($return=='distrito'){
				echo json_encode(array('code'=>'ok','data'=>$fkdistrito));
			}elseif($return=='all'){
				foreach ($newdatos as $dt){
					if(empty($strfkpais[$dt["pais"]])){
						$pais=$this->oNegUbigeo->buscar(array('pais'=>$dt["pais"],'departamento'=>'00','provincia'=>'00'));
						if(!empty($pais[0])) $strfkpais[$dt["pais"]]=$pais[0]["ciudad"];
					}
					if(empty($strfkdepartamento[$dt["pais"].$dt["departamento"]])){
						$departamento=$this->oNegUbigeo->buscar(array('pais'=>$dt["pais"],'departamento'=>$dt["departamento"],'provincia'=>'00'));
						if(!empty($departamento[0])) $strfkdepartamento[$dt["pais"].$dt["departamento"]]=$departamento[0]["ciudad"];
					}
					if(empty($strfkdepartamento[$dt["pais"].$dt["departamento"].$dt["provincia"]])){
						$provincia=$this->oNegUbigeo->buscar(array('pais'=>$dt["pais"],'departamento'=>$dt["departamento"],'provincia'=>$dt["provincia"]));
						if(!empty($provincia[0])) $strfkprovincia[$dt["pais"].$dt["departamento"].$dt["provincia"]]=$provincia[0]["ciudad"];
					}

					array_push($returnnewdatos,
						array(
							'id_ubigeo'=>$dt["id_ubigeo"],
							'pais'=>$dt["pais"],
							'strpais'=>@$strfkpais[$dt["pais"]],
							'departamento'=>$dt["departamento"],
							'strdepartamento'=>@$strfkdepartamento[$dt["pais"].$dt["departamento"]],
							'provincia'=>$dt["provincia"],
							'strprovincia'=>@$strfkprovincia[$dt["pais"].$dt["departamento"].$dt["provincia"]],
							'distrito'=>$dt["distrito"],
							'strdistrito'=>@$strfkdistrito[$dt["pais"].$dt["id_ubigeo"]],
							'ciudad'=>$dt["ciudad"]
							)
					);
				}			
				echo json_encode(array('code'=>'ok','data'=>$returnnewdatos));
			}
		 	exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>$e));
            exit(0);
        }
	}

	public function guardarUbigeo(){
		$this->documento->plantilla = 'blanco';
		try {
            if(empty($_POST)){
                echo json_encode(array('code'=>'Error','msj'=>JrTexto::_('data incomplete')));
                exit(0);            
            }
            $accion='_add';
            if(!empty(@$pkId_ubigeo)) {
				$this->oNegUbigeo->id_ubigeo = $frm['pkId_ubigeo'];
				$accion='_edit';
			}

            	global $aplicacion;         
            	$usuarioAct = NegSesion::getUsuario();
            	@extract($_POST);
            	
				$this->oNegUbigeo->pais=@$txtPais;
				$this->oNegUbigeo->departamento=@$txtDepartamento;
				$this->oNegUbigeo->provincia=@$txtProvincia;
				$this->oNegUbigeo->distrito=@$txtDistrito;
				$this->oNegUbigeo->ciudad=@$txtCiudad;
					
            if($accion=='_add') {
            	$res=$this->oNegUbigeo->agregar();
            	 echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Ubigeo')).' '.JrTexto::_('saved successfully'),'newid'=>$res)); 
            }else{
            	$res=$this->oNegUbigeo->editar();
            	echo json_encode(array('code'=>'ok','msj'=>ucfirst(JrTexto::_('Ubigeo')).' '.JrTexto::_('update successfully'),'newid'=>$res)); 
            }
            exit(0);
        }catch(Exception $e) {
            echo json_encode(array('code'=>'Error','msj'=>$e));
            exit(0);
        }
	}

	
	// ========================== Funciones xajax ========================== //
	public function xSaveUbigeo(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$frm = $args[0];
				
				if(!empty($frm['pkId_ubigeo'])) {
					$this->oNegUbigeo->id_ubigeo = $frm['pkId_ubigeo'];
				}
				
				$this->oNegUbigeo->pais=@$frm["txtPais"];
				$this->oNegUbigeo->departamento=@$frm["txtDepartamento"];
				$this->oNegUbigeo->provincia=@$frm["txtProvincia"];
				$this->oNegUbigeo->distrito=@$frm["txtDistrito"];
				$this->oNegUbigeo->ciudad=@$frm["txtCiudad"];
					
				if(@$frm["accion"]=="Nuevo"){
					$res=$this->oNegUbigeo->agregar();
				}else{
					$res=$this->oNegUbigeo->editar();
				}
				if(!empty($res)) $oRespAjax->setReturnValue($this->oNegUbigeo->id_ubigeo);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Failed to save registry')),'warning');
					$oRespAjax->setReturnValue(false);
				}
							
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}

	public function xGetxIDUbigeo(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegUbigeo->__set('id_ubigeo', $pk);
				$this->datos = $this->oNegUbigeo->dataUbigeo;
				$res=$this->oNegUbigeo->getXid();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')),  $this->pasarHtml(JrTexto::_('Get record')), 'warning');
				$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}

	public function xEliminar(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegUbigeo->__set('id_ubigeo', $pk);
				$res=$this->oNegUbigeo->eliminar();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Error').' '.JrTexto::_('Delete Record')), 'warning');
					$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}
	public function xSetCampo(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
               
				if(empty($args[0])) { return;}
				$this->oNegUbigeo->setCampo($args[0],$args[1],$args[2]);
				$oRespAjax->setReturnValue(true);
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Atención')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}	     
}