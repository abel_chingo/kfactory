<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		14-11-2016 
 * @copyright	Copyright (C) 14-11-2016. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegNiveles', RUTA_BASE, 'sys_negocio');
class WebNiveles extends JrWeb
{
	private $oNegNiveles;
	public function __construct()
	{
		parent::__construct();
		$this->oNegNiveles = new NegNiveles;		
	}

	public function defecto(){
		return $this->listado();
	}

	public function listado()
	{
		try{
			global $aplicacion;

			if(!NegSesion::tiene_acceso('Niveles', 'list')) {
                throw new Exception(JrTexto::_('Restricted access').'!!');
            }
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
            $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
            $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
			$this->datos=$this->oNegNiveles->buscar(array('tipo'=>'N'));
			$this->documento->plantilla = 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('level'), true);
			$this->esquema = 'niveles-list';			
			return parent::getEsquema();
		}catch(Exception $e) {
			 return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function todos(){
		try{
            global $aplicacion;  
            $idnivel_=!empty(JrPeticion::getPeticion(2))?JrPeticion::getPeticion(2):0;
            $this->niveles=$this->oNegNiveles->buscar(array('tipo'=>'N'));
            $_idnivel=!empty($this->niveles[0]["idnivel"])?$this->niveles[0]["idnivel"]:0;
            $this->idnivel=!empty($idnivel_)?$idnivel_:$_idnivel;

            $idunidad_=!empty(JrPeticion::getPeticion(3))?JrPeticion::getPeticion(3):0;
            $this->unidades=$this->oNegNiveles->buscar(array('tipo'=>'U','idpadre'=>$this->idnivel));
            $_idunidad=!empty($this->unidades[0]["idnivel"])?$this->unidades[0]["idnivel"]:0;
            $this->idunidad=!empty($idunidad_)?$idunidad_:($_idunidad);  

            $this->sesiones=$this->oNegNiveles->buscar(array('tipo'=>'L','idpadre'=>$this->idunidad));
            $_idsesion=!empty($this->sesiones[0]["idnivel"])?$this->sesiones[0]["idnivel"]:0;
           
            $this->documento->script('slick.min', '/libs/sliders/slick/');
			$this->documento->stylesheet('slick', '/libs/sliders/slick/');
			$this->documento->stylesheet('slick-theme', '/libs/sliders/slick/');
            $this->documento->setTitulo(JrTexto::_('All Activities'), true);            
            $this->esquema = 'alumno/sesiones';
            return parent::getEsquema();
        }catch(Exception $e) {
           return $aplicacion->error(JrTexto::_($e->getMessage()));
        }
	}	

	public function getxPadrejson($filtro)
	{
		try {               
			$datos=$this->oNegNiveles->buscar($filtro);		
			$this->documento->plantilla='returnjson';
			echo json_encode($datos);
			return parent::getEsquema();
		} catch(Exception $e) {
			$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Atención')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
		} 

	}

	public function agregar()
	{
		try {
			global $aplicacion;			
	
			$this->frmaccion='New';
			$this->documento->setTitulo(JrTexto::_('Niveles').' /'.JrTexto::_('New'), true);
			return $this->form();
		} catch(Exception $e) {
			 return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function editar()
	{
		try {
			global $aplicacion;
			
			$this->frmaccion='Edit';
			$this->oNegNiveles->idnivel = @$_GET['id'];
			$this->datos = $this->oNegNiveles->dataNiveles;
			$this->pk=@$_GET['id'];
			$this->documento->setTitulo(JrTexto::_('Niveles').' /'.JrTexto::_('Edit'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}


	public function ver(){
		try{
			global $aplicacion;						
			$this->oNegNiveles->idnivel = @$_GET['id'];
			$this->datos = $this->oNegNiveles->dataNiveles;
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Niveles').' /'.JrTexto::_('see'), true);
			$this->documento->plantilla = 'mantenimientos';
			$this->esquema = 'niveles-see';
			$this->documento->plantilla = $tpl;
			return parent::getEsquema();
		}catch(Exception $e) {
			$aplicacion->encolarMsj($e->getMessage(), false, 'error');			
		}
	}

	private function form()
	{
		try {
			global $aplicacion;	
			$this->esquema = 'niveles-frm';
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}


	// ========================== Funciones xajax ========================== //
	public function xSaveNiveles(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$frm = $args[0];
				
				if(!empty($frm['pkIdnivel'])) {
					$this->oNegNiveles->idnivel = $frm['pkIdnivel'];
				}
					$usuarioAct = NegSesion::getUsuario();
					$imagen=str_replace($this->documento->getUrlBase(),'__xRUTABASEx__',@$frm["imagen"]);
					$this->oNegNiveles->__set('nombre',@$frm["txtNombre"]);
					$this->oNegNiveles->__set('tipo',@$frm["txtTipo"]);
					$this->oNegNiveles->__set('idpadre',@$frm["txtIdpadre"]);
					$this->oNegNiveles->__set('idpersonal',@$usuarioAct["dni"]);
					$this->oNegNiveles->__set('estado',@$frm["txtEstado"]);
					$this->oNegNiveles->__set('orden',@$frm["txtOrden"]);
					$this->oNegNiveles->__set('imagen',$imagen);
					
				   if(@$frm["accion"]=="New"){
						$res=$this->oNegNiveles->agregar();
					}else{
						$res=$this->oNegNiveles->editar();
				    }
					if(!empty($res)) $oRespAjax->setReturnValue($this->oNegNiveles->idnivel);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Failed to save registry')),'warning');
					$oRespAjax->setReturnValue(false);
				}
							
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}

	public function xGetxIDNiveles(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegNiveles->__set('idnivel', $pk);
				$this->datos = $this->oNegNiveles->dataNiveles;
				$res=$this->oNegNiveles->getXid();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')),  $this->pasarHtml(JrTexto::_('Get record')), 'warning');
				$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}
	public function xEliminar(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegNiveles->__set('idnivel', $pk);
				$res=$this->oNegNiveles->eliminar();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Error').' '.JrTexto::_('Delete Record')), 'warning');
					$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}
	public function xSetCampo(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
               
				if(empty($args[0])) { return;}
				$this->oNegNiveles->setCampo($args[0],$args[1],$args[2]);
				$oRespAjax->setReturnValue(true);
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Atención')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}

	public function xGetxPadre(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {               
				if(empty($args[0])) { return;}
				$filtro=$args[0];
				$datos=$this->oNegNiveles->buscar($filtro);				
				$oRespAjax->setReturnValue($datos);
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Atención')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}

	public function xOrdenar(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {               
				if(empty($args[0])) { return;}
				$frm=$args[0];
				$res=$this->oNegNiveles->ordenar($frm);
				$oRespAjax->setReturnValue($res);
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Atención')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}  
}