<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		11-01-2017 
 * @copyright	Copyright (C) 11-01-2017. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegLocal', RUTA_BASE, 'sys_negocio');
JrCargador::clase('sys_negocio::NegUgel', RUTA_BASE, 'sys_negocio');
class WebLocal extends JrWeb
{
	private $oNegLocal;
	public function __construct()
	{
		parent::__construct();		
		$this->oNegLocal = new NegLocal;
		$this->oNegUgel = new NegUgel;
	}

	public function defecto(){
		return $this->listado();
	}

	public function listado()
	{
		try{
			global $aplicacion;			
			if(!NegSesion::tiene_acceso('Local', 'list')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
            $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
            $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
			//$this->documento->script(null, ConfigSitio::get('tema_general') . '/js/datatables/js/jquery.dataTables.js');
			
			
            
            $this->tipo1=!empty($_GET["txtTipo1"])?$_GET["txtTipo1"]:'T';

            $this->ugeles=$this->oNegUgel->buscar();
            $this->idugel=!empty($_GET["txtUgel"])?$_GET["txtUgel"]:"";


            //var_dump($this->tipo1);


			$this->datos=$this->oNegLocal->buscar(array('tipo'=>$this->tipo1,'idugel'=>$this->idugel));
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';

			$this->documento->setTitulo(JrTexto::_('Local'), true);
			$this->esquema = 'local-list';			
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}


	public function agregar()
	{
		try {
			global $aplicacion;			
			if(!NegSesion::tiene_acceso('Local', 'add')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->frmaccion='Nuevo';
			$this->documento->setTitulo(JrTexto::_('Local').' /'.JrTexto::_('New'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function editar()
	{
		try {
			global $aplicacion;
			
			if(!NegSesion::tiene_acceso('Local', 'edit')) {
				throw new Exception(JrTexto::_('Restricted access').'!!');
			}
			$this->frmaccion='Editar';
			$this->oNegLocal->idlocal = @$_GET['id'];
			$this->datos = $this->oNegLocal->dataLocal;
			$this->pk=@$_GET['id'];
			//var_dump($this->datos);

			$_iddep=$this->datos['id_ubigeo'];			
			$this->provincias=$this->oNegUgel->ubigeo(array('idpadre'=>$_iddep));
			$this->distritos=$this->oNegUgel->ubigeo(array('idpadre1'=>$_iddep));

			$this->documento->setTitulo(JrTexto::_('Local').' /'.JrTexto::_('Edit'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}


	public function ver(){
		try{
			global $aplicacion;						
			$this->oNegLocal->idlocal = @$_GET['id'];
			$this->datos = $this->oNegLocal->dataLocal;
									$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Local').' /'.JrTexto::_('see'), true);
			$this->esquema = 'local-see';
			$this->documento->plantilla = $tpl;
			return parent::getEsquema();
		}catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	private function form()
	{
		try {
			global $aplicacion;	
			
			//$this->documento->script(null, 'http://tinymce.cachefly.net/4.2/tinymce.min.js');			
			$this->esquema = 'local-frm';

			$this->departamentos=$this->oNegUgel->ubigeo(array('iddep'=>'OK'));
			

			$this->ugeles=$this->oNegUgel->buscar(array('iddep'=>'OK'));

			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}


	// ========================== Funciones xajax ========================== //
	public function xSaveLocal(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$frm = $args[0];
				
				if(!empty($frm['pkIdlocal'])) {
					$this->oNegLocal->idlocal = $frm['pkIdlocal'];
				}
				
				$this->oNegLocal->__set('nombre',@$frm["txtNombre"]);
					$this->oNegLocal->__set('direccion',@$frm["txtDireccion"]);
					$this->oNegLocal->__set('id_ubigeo',@$frm["txtId_ubigeo"]);
					$this->oNegLocal->__set('tipo',@$frm["txtTipo"]);
					$this->oNegLocal->__set('vacantes',@$frm["txtVacantes"]);
					$this->oNegLocal->__set('idugel',@$frm["txtIdugel"]);
					
				   if(@$frm["accion"]=="Nuevo"){
									    $res=$this->oNegLocal->agregar();
					}else{
									    $res=$this->oNegLocal->editar();
				    }
					if(!empty($res)) $oRespAjax->setReturnValue($this->oNegLocal->idlocal);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Failed to save registry')),'warning');
					$oRespAjax->setReturnValue(false);
				}
							
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}

	public function xGetxIDLocal(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegLocal->__set('idlocal', $pk);
				$this->datos = $this->oNegLocal->dataLocal;
				$res=$this->oNegLocal->getXid();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')),  $this->pasarHtml(JrTexto::_('Get record')), 'warning');
				$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}
	public function xEliminar(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {
				if(empty($args[0])) { return;}
				$pk = $args[0];
				$this->oNegLocal->__set('idlocal', $pk);
				$res=$this->oNegLocal->eliminar();
				if(!empty($res))
					$oRespAjax->setReturnValue($res);
				else{
					$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_('Error').' '.JrTexto::_('Delete Record')), 'warning');
					$oRespAjax->setReturnValue(false);
				}
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Attention')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
				$oRespAjax->setReturnValue(false);
			} 
		}
	}
	

	public function xGetxPadre(&$oRespAjax = null, $args = null)
	{
		if(is_a($oRespAjax, 'xajaxResponse')) {
			try {               
				if(empty($args[0])) { return;}
				$filtro=$args[0];
				$datos=$this->oNegUgel->ubigeo($filtro);				
				$oRespAjax->setReturnValue($datos);
			} catch(Exception $e) {
				$oRespAjax->call('mostrar_notificacion', $this->pasarHtml(JrTexto::_('Atención')), $this->pasarHtml(JrTexto::_($e->getMessage())), 'warning');
			} 
		}
	}	    
	     
}