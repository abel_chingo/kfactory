<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		26-12-2016 
 * @copyright	Copyright (C) 26-12-2016. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegNiveles', RUTA_BASE, 'sys_negocio');
class WebCursos extends JrWeb
{
	private $oNegNiveles;

	public function __construct()
	{
		parent::__construct();
		$this->oNegNiveles = new NegNiveles;
	}

	public function defecto(){
		return $this->listado();
		//return $this->versetting();
	}

	public function versetting(){
		global $aplicacion;
		$this->documento->setTitulo(JrTexto::_('Setting'), true);
        $this->esquema = 'doc_cursos';            
        return parent::getEsquema();
	} 

	public function listado()
	{
		try {
            global $aplicacion;
            $this->documento->setTitulo(JrTexto::_('Courses'), true);
            $this->idCurso = (isset($_GET['id']))?$_GET['id']:0;
            
	        
			$this->niveles=$this->oNegNiveles->buscarNiveles(array('tipo'=>'N'));

            $this->documento->plantilla = 'inicio';
            $this->esquema = 'cursos/listar';
            return parent::getEsquema();
        } catch (Exception $e) {
            return $aplicacion->error(JrTexto::_($e->getMessage()));
        }
	}
	public function contenido()
	{
		try {
            global $aplicacion;
            $this->documento->setTitulo(JrTexto::_('Course'), true);
            $this->idCurso = (isset($_GET['id']))?$_GET['id']:0;
            
	        
			$this->niveles=$this->oNegNiveles->buscarNiveles(array('tipo'=>'N'));

            $this->documento->plantilla = 'full_width';
            $this->esquema = 'cursos/contenido';
            return parent::getEsquema();
        } catch (Exception $e) {
            return $aplicacion->error(JrTexto::_($e->getMessage()));
        }
	}
}