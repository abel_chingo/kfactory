<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		14-11-2016 
 * @copyright	Copyright (C) 14-11-2016. Todos los derechos reservados.
 */
defined('RUTA_BASE') or die();
JrCargador::clase('sys_negocio::NegNiveles', RUTA_BASE, 'sys_negocio');
class WebLeccion extends JrWeb
{
	private $oNegNiveles;
	public function __construct()
	{
		parent::__construct();
		$this->oNegNiveles = new NegNiveles;		
	}

	public function defecto(){
		return $this->listado();
	}

	public function listado()
	{
		try{
			global $aplicacion;	
			$this->documento->stylesheet('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery-confirm.min', '/libs/alert/');
            $this->documento->script('jquery.dataTables.min', '/libs/datatable1.10/media/js/');
            $this->documento->stylesheet('jquery.dataTables.min', '/libs/datatable1.10/media/css/');
            $this->documento->stylesheet('buttons.dataTables.min', '/libs/datatable1.10/extensions/Buttons/css/');
           
            $this->niveles=$this->oNegNiveles->buscar(array('tipo'=>'N'));
            $_idnivel=!empty($this->niveles[0]["idnivel"])?$this->niveles[0]["idnivel"]:0;
            $this->idnivel=!empty($_GET["txtNivel"])?$_GET["txtNivel"]:$_idnivel;
           
            $this->unidades=$this->oNegNiveles->buscar(array('tipo'=>'U','idpadre'=>$this->idnivel));
            $_idunidad=!empty($this->unidades[0]["idnivel"])?$this->unidades[0]["idnivel"]:0;
            $this->idunidad=!empty($_GET["txtUnidad"])?$_GET["txtUnidad"]:($_idunidad);

			$this->datos=$this->oNegNiveles->buscar(array('tipo'=>'L','idpadre'=>$this->idunidad));	
			$this->documento->plantilla = 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Unit'), true);
			$this->esquema = 'leccion-list';			
			return parent::getEsquema();
		}catch(Exception $e) {
			$aplicacion->encolarMsj($e->getMessage(), false, 'error');
			$aplicacion->redir();
		}
	}


	public function agregar()
	{
		try {
			global $aplicacion;

			$this->niveles=$this->oNegNiveles->buscar(array('tipo'=>'N'));
			$_idnivel=!empty($this->niveles[0]["idnivel"])?$this->niveles[0]["idnivel"]:0;
            $this->idnivel=!empty($_GET["idnivel"])?$_GET["idnivel"]:$_idnivel;

            $this->unidades=$this->oNegNiveles->buscar(array('tipo'=>'U','idpadre'=>$this->idnivel));
            $_idunidad=!empty($this->unidades[0]["idnivel"])?$this->unidades[0]["idnivel"]:0;
            $this->idunidad=!empty($_GET["idunidad"])?$_GET["idunidad"]:($_idunidad);


			$this->frmaccion='New';
			$this->documento->setTitulo(JrTexto::_('Lesson').' /'.JrTexto::_('New'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}

	public function editar()
	{
		try {
			global $aplicacion;
			$this->oNegNiveles->idnivel = @$_GET['id'];
			$this->datos = $this->oNegNiveles->dataNiveles;
			
			$this->niveles=$this->oNegNiveles->buscar(array('tipo'=>'N'));
			$_idnivel=!empty($this->niveles[0]["idnivel"])?$this->niveles[0]["idnivel"]:0;
			$this->idnivel=!empty($_GET["idnivel"])?$_GET["idnivel"]:$_idnivel;

			$this->unidades=$this->oNegNiveles->buscar(array('tipo'=>'U','idpadre'=>$this->idnivel));		    
            $_idunidad=!empty($this->unidades[0]["idnivel"])?$this->unidades[0]["idnivel"]:0;
            $this->idunidad=!empty($_GET["idunidad"])?$_GET["idunidad"]:($_idunidad);
			$this->frmaccion='Edit';			
			$this->pk=@$_GET['id'];
			$this->documento->setTitulo(JrTexto::_('Lesson').' /'.JrTexto::_('Edit'), true);
			return $this->form();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}


	public function ver(){
		try{
			global $aplicacion;						
			$this->oNegNiveles->idnivel = @$_GET['id'];
			$this->datos = $this->oNegNiveles->dataNiveles;
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			$this->documento->setTitulo(JrTexto::_('Lesson').' /'.JrTexto::_('see'), true);

			$this->documento->plantilla = 'mantenimientos';
			$this->esquema = 'leccion-ver';
			$this->documento->plantilla = $tpl;
			return parent::getEsquema();
		}catch(Exception $e) {
			$aplicacion->encolarMsj($e->getMessage(), false, 'error');
			$aplicacion->redir();
		}
	}

	private function form()
	{
		try {
			global $aplicacion;
			$this->esquema = 'leccion-frm';
			$this->documento->plantilla = !empty($_GET['plt']) ? $_GET['plt'] : 'mantenimientos';
			return parent::getEsquema();
		} catch(Exception $e) {
			return $aplicacion->error(JrTexto::_($e->getMessage()));
		}
	}
 
}