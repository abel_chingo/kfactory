<?php 
    $SD=DIRECTORY_SEPARATOR;
    $ruta=dirname(dirname(dirname(__FILE__))).$SD;
    require_once($ruta.'ini_app.php');
    defined('RUTA_BASE') or die();
    $Sitio=new Sitio();
    $Sitio->iniciar();
    $documento =&JrInstancia::getDocumento();
    $met=empty($_GET["met"])?exit(0):$_GET["met"];
    $orden=empty($_GET["ord"])?1:$_GET["ord"];
    $idgui = uniqid();
    $usuarioAct = NegSesion::getUsuario();
    $rolActivo=$usuarioAct["rol"];
?>
<input type="hidden" name="orden[<?php echo $met ?>][<?php echo $orden ?>]" value="<?php echo $orden; ?>">
<input type="hidden" name="det_tipo[<?php echo $met ?>][<?php echo $orden ?>]" value="Look">
<input type="hidden" name="tipo_desarrollo[<?php echo $met ?>][<?php echo $orden ?>]" value="Video">
<input type="hidden" name="tipo_actividad[<?php echo $met ?>][<?php echo $orden ?>]" value="A">
<input type="hidden" id="video<?php echo $idgui ?>" name="url[<?php echo $met ?>]" value="">
<input type="hidden" name="idgui" id="idgui" value="<?php echo $idgui; ?>">
<link rel="stylesheet" href="<?php echo $documento->getUrlTema()?>/css/actividad_video.css">
<div class="plantilla plantilla_video" data-idgui="<?php echo $idgui ?>">
  <div class="tmp_video v<?php echo $idgui ?>">
    <div class="row">
      <div class="col-xs-12 video-loader">
        <div class="btnselectedfile btn btn-primary nopreview istooltip" title="<?php echo ucfirst(JrTexto::_('select a video from our multimedia library or upload your own video')) ?>" data-tipo="video" data-url=".vid_1<?php echo $met."_".$idgui ?>">
          <i class="fa fa-video-camera"></i>
          <?php echo ucfirst(JrTexto::_('select')) ?> video
        </div>
      </div>
      <div class="col-xs-12">              
        <div class="embed-responsive embed-responsive-16by9 mascara_video">
          <video controls="true" class="valvideo embed-responsive-item vid_1<?php echo $met."_".$idgui ?>" src="" style="display: none"></video>
          <img src="<?php echo $documento->getUrlStatic(); ?>/media/web/novideo.png" class="img-responsive img-thumbnail embed-responsive-item nopreview">
        </div>
      </div>
    </div>
  </div>    
 </div>