<?php defined('_BOOT_') or die(''); 
$idCurso = (isset($_GET['id']))?$_GET['id']:0;
$ruta_completa=$_SERVER['HTTP_HOST'].$_SERVER["REQUEST_URI"];
$rutaBase = str_replace(array('http://', 'https://'), '', $documento->getUrlBase());
$ruta_sin_UrlBase = str_replace($rutaBase,'',$ruta_completa);
$ruta_sin_UrlBase = explode('?', $ruta_sin_UrlBase)[0];
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="<?php echo $documento->getUrlStatic()?>/tema/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo $documento->getUrlStatic()?>/tema/css/font-awesome.min.css" rel="stylesheet">
    <link href="<?php echo $documento->getUrlStatic()?>/tema/css/animate.min.css" rel="stylesheet">
    <link href="<?php echo $documento->getUrlStatic()?>/tema/css/hover.min.css" rel="stylesheet">
    <link href="<?php echo $documento->getUrlStatic()?>/tema/css/pnotify.min.css" rel="stylesheet">
    <link href="<?php echo $documento->getUrlStatic()?>/libs/datepicker/datetimepicker.css" rel="stylesheet">
    <!-- Custom styling plus plugins -->
    <link href="<?php echo $documento->getUrlTema()?>/css/inicio.css" rel="stylesheet">
    <link href="<?php echo $documento->getUrlTema()?>/css/colores_flat.css" rel="stylesheet">
    <link href="<?php echo $documento->getUrlTema()?>/alumno/general.css" rel="stylesheet">
    <script type="text/javascript" src="<?php echo $documento->getUrlStatic()?>/tema/js/jquery.min.js"></script>
    
    <!--[if lt IE 9]>
        <script src="../assets/js/ie8-responsive-file-warning.js"></script>
        <![endif]-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    <!-- PNotify -->
    <script type="text/javascript" src="<?php echo $documento->getUrlStatic()?>/tema/js/pnotify.min.js"></script>
    <script type="text/javascript" src="<?php echo $documento->getUrlStatic()?>/tema/js/funciones.js"></script>
    <script type="text/javascript" src="<?php echo $documento->getUrlStatic()?>/js/inicio.js"></script>
    <script> var _sysUrlBase_ = '<?php echo $documento->getUrlBase()?>'; 
            var sitio_url_base = '<?php echo $documento->getUrlSitio()?>';
            var _sysUrlStatic_ = '<?php echo $documento->getUrlStatic()?>';</script>
    <jrdoc:incluir tipo="cabecera" />
    <script src="<?php echo $documento->getUrlStatic()?>/tema/js/bootstrap.min.js"></script>
</head>
<body>
<jrdoc:incluir tipo="modulo" nombre="top" /></jrdoc:incluir>
<jrdoc:incluir tipo="modulo" nombre="toolbar" /></jrdoc:incluir>
<jrdoc:incluir tipo="modulo" nombre="sidebar_right" /></jrdoc:incluir>
<div class="">
<div class="menu-right pull-right"></div>
<div class="container">
<div class="row">
    <div class="col-xs-12 col-sm-2 col-md-1" id="menu-curso">
        <a href="<?php echo $documento->getUrlBase().'/curso/?id='.$idCurso; ?>" class="btn btn-red <?php if($ruta_sin_UrlBase=='/curso/'){echo 'activo';} ?>" title="<?php echo JrTexto::_('Course'); ?>"><i class="fa fa-bookmark fa-2x"></i></a>
        <a href="<?php echo $documento->getUrlBase().'/curso/informacion/?id='.$idCurso; ?>" class="btn btn-turquoise <?php if($ruta_sin_UrlBase=='/curso/informacion/'){echo 'activo';} ?>" title="<?php echo JrTexto::_('Information'); ?>"><i class="fa fa-info-circle fa-2x"></i></a>
        <a href="<?php echo $documento->getUrlBase().'/curso/horario/?id='.$idCurso; ?>" class="btn btn-brown-dark <?php if($ruta_sin_UrlBase=='/curso/horario/'){echo 'activo';} ?>" title="<?php echo JrTexto::_('Schedule'); ?>"><i class="fa fa-calendar-check-o fa-2x"></i></a>
        <a href="<?php echo $documento->getUrlBase().'/curso/companieros/?id='.$idCurso; ?>" class="btn btn-blue <?php if($ruta_sin_UrlBase=='/curso/companieros/'){echo 'activo';} ?>" title="<?php echo JrTexto::_('Classmates'); ?>"><i class="fa fa-users fa-2x"></i></a>
        <a href="<?php echo $documento->getUrlBase().'/curso/academico/?id='.$idCurso; ?>" class="btn btn-yellow <?php if($ruta_sin_UrlBase=='/curso/academico/'){echo 'activo';} ?>" title="<?php echo JrTexto::_('Academic'); ?>"><i class="fa fa-graduation-cap fa-2x"></i></a>
        <a href="<?php echo $documento->getUrlBase().'/curso/tarea/?id='.$idCurso; ?>" class="btn btn-aqua <?php if($ruta_sin_UrlBase=='/curso/tarea/'){echo 'activo';} ?>" title="<?php echo JrTexto::_('Homework'); ?>"><i class="fa fa-briefcase fa-2x"></i></a>
        <a href="<?php echo $documento->getUrlBase().'/curso/examenes/?id='.$idCurso; ?>" class="btn btn-purple <?php if($ruta_sin_UrlBase=='/curso/examenes/'){echo 'activo';} ?>" title="<?php echo JrTexto::_('Assessment'); ?>"><i class="fa fa-list fa-2x"></i></a>
    </div>
    <div class="col-xs-12 col-sm-10 col-md-11">
        <jrdoc:incluir tipo="mensaje" />
        <jrdoc:incluir tipo="recurso" />
    </div>
</div>
</div>

<jrdoc:incluir tipo="modulo" nombre="footer"/></jrdoc:incluir>
</div>
<?php
$configs = JrConfiguracion::get_();
require_once(RUTA_SITIO . 'ServerxAjax.php');
echo $xajax->getJavascript($documento->getUrlStatic() . '/libs/xajax/');
?>
<script src="<?php echo $documento->getUrlStatic()?>/libs/moment/moment.js"></script>
<script src="<?php echo $documento->getUrlStatic()?>/libs/datepicker/datetimepicker.js"></script>
<jrdoc:incluir tipo="docsJs" />
<jrdoc:incluir tipo="docsCss" />
</body>
</html>