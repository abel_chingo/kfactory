<?php 
$menu = array(
	array('nombre' => ucfirst(JrTexto::_('Profile')), 'link'=>'/perfil', 'icon'=>'fa-user ', 'descripcion'=>''),
	array('nombre' => ucfirst(JrTexto::_('Courses')), 'link'=>'/cursos', 'icon'=>'fa-file', 'descripcion'=>''),
	array('nombre' => ucfirst(JrTexto::_('Homework')), 'link'=>'/tarea', 'icon'=>'fa-briefcase', 'descripcion'=>''),
	array('nombre' => ucfirst(JrTexto::_('Assessments')), 'link'=>'/examenes', 'icon'=>'fa-list', 'descripcion'=>''),
	array('nombre' => ucfirst(JrTexto::_('Tracking to students')), 'link'=>'/docente/panelcontrol', 'icon'=>'fa-tachometer', 'descripcion'=>''),
	array('nombre' => ucfirst(JrTexto::_('Reports')), 'link'=>'/reportes', 'icon'=>'fa-bar-chart', 'descripcion'=>''),
	array('nombre' => ucfirst(JrTexto::_('Log out')), 'link'=>'/sesion/salir', 'icon'=>'fa-power-off', 'descripcion'=>''),
);
$ruta_completa=$_SERVER['HTTP_HOST'].$_SERVER["REQUEST_URI"];
$rutaBase = str_replace(array('http://', 'https://'), '', $this->documento->getUrlBase());
$ruta_sin_UrlBase = str_replace($rutaBase,'',$ruta_completa);
$ruta_sin_UrlBase = explode('?', $ruta_sin_UrlBase)[0];
?>
<div class="list-group" id="sidebar-nav">
	<?php foreach ($menu as $m) { 
		$active = ($ruta_sin_UrlBase==$m['link'])?'active':'';
	?>
		<a class="list-group-item <?php echo $active; ?>" href="<?php echo $this->documento->getUrlSitio().$m['link']; ?>">
			<h5 class="list-group-item-heading">
				<i class="fa <?php echo $m['icon']; ?> fa-fw"></i> <?php echo $m['nombre']; ?>
			</h5>
			<?php if(!empty($m['descripcion'])) { ?>
			<p class="list-group-item-text">$m['descripcion']</p>
			<?php } ?>
		</a>
	<?php }
	?>
</div>
<script type="text/javascript">	
var calcularAncho = function () {
	var ventanaW = $(window).outerWidth();
	if(ventanaW<=768){ $('#menu-left').hide(); }
	else{ $('#menu-left').show(); }
};

$(window).resize(calcularAncho);

$(document).ready(function() {
	calcularAncho();
	$('header .navbar-header button.navbar-toggle').click(function() {
		var estaMenuVisible = $('#menu-left #sidebar-nav').is(':visible');
		if(estaMenuVisible){ $('#menu-left').hide('fast'); }
		else{ $('#menu-left').show('fast'); }
	});
	
});

</script>