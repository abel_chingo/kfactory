<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		24-07-2017  
  * @copyright	Copyright (C) 2017. Todos los derechos reservados.
 */ 
class DatPersona_apoderado extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Persona_apoderado").": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros=null)
	{
		try {
			$sql = "SELECT COUNT(*) FROM persona_apoderado";
			
			$cond = array();		
			
			if(isset($filtros["idapoderado"])) {
					$cond[] = "idapoderado = " . $this->oBD->escapar($filtros["idapoderado"]);
			}
			if(isset($filtros["idpersona"])) {
					$cond[] = "idpersona = " . $this->oBD->escapar($filtros["idpersona"]);
			}
			if(isset($filtros["nombres"])) {
					$cond[] = "nombres = " . $this->oBD->escapar($filtros["nombres"]);
			}
			if(isset($filtros["apellidopaterno"])) {
					$cond[] = "apellidopaterno = " . $this->oBD->escapar($filtros["apellidopaterno"]);
			}
			if(isset($filtros["apellidomaterno"])) {
					$cond[] = "apellidomaterno = " . $this->oBD->escapar($filtros["apellidomaterno"]);
			}
			if(isset($filtros["correo"])) {
					$cond[] = "correo = " . $this->oBD->escapar($filtros["correo"]);
			}
			if(isset($filtros["sexo"])) {
					$cond[] = "sexo = " . $this->oBD->escapar($filtros["sexo"]);
			}
			if(isset($filtros["tipodoc"])) {
					$cond[] = "tipodoc = " . $this->oBD->escapar($filtros["tipodoc"]);
			}
			if(isset($filtros["ndoc"])) {
					$cond[] = "ndoc = " . $this->oBD->escapar($filtros["ndoc"]);
			}
			if(isset($filtros["parentesco"])) {
					$cond[] = "parentesco = " . $this->oBD->escapar($filtros["parentesco"]);
			}
			if(isset($filtros["telefono"])) {
					$cond[] = "telefono = " . $this->oBD->escapar($filtros["telefono"]);
			}
			if(isset($filtros["celular"])) {
					$cond[] = "celular = " . $this->oBD->escapar($filtros["celular"]);
			}
			if(isset($filtros["mostrar"])) {
					$cond[] = "mostrar = " . $this->oBD->escapar($filtros["mostrar"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			return $this->oBD->consultarEscalarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("Persona_apoderado").": " . $e->getMessage());
		}
	}
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT * FROM persona_apoderado";
			$cond = array();
			if(isset($filtros["idapoderado"])) {
					$cond[] = "idapoderado = " . $this->oBD->escapar($filtros["idapoderado"]);
			}
			if(isset($filtros["idpersona"])) {
					$cond[] = "idpersona = " . $this->oBD->escapar($filtros["idpersona"]);
			}
			if(isset($filtros["nombres"])) {
					$cond[] = "nombres = " . $this->oBD->escapar($filtros["nombres"]);
			}
			if(isset($filtros["apellidopaterno"])) {
					$cond[] = "apellidopaterno = " . $this->oBD->escapar($filtros["apellidopaterno"]);
			}
			if(isset($filtros["apellidomaterno"])) {
					$cond[] = "apellidomaterno = " . $this->oBD->escapar($filtros["apellidomaterno"]);
			}
			if(isset($filtros["correo"])) {
					$cond[] = "correo = " . $this->oBD->escapar($filtros["correo"]);
			}
			if(isset($filtros["sexo"])) {
					$cond[] = "sexo = " . $this->oBD->escapar($filtros["sexo"]);
			}
			if(isset($filtros["tipodoc"])) {
					$cond[] = "tipodoc = " . $this->oBD->escapar($filtros["tipodoc"]);
			}
			if(isset($filtros["ndoc"])) {
					$cond[] = "ndoc = " . $this->oBD->escapar($filtros["ndoc"]);
			}
			if(isset($filtros["parentesco"])) {
					$cond[] = "parentesco = " . $this->oBD->escapar($filtros["parentesco"]);
			}
			if(isset($filtros["telefono"])) {
					$cond[] = "telefono = " . $this->oBD->escapar($filtros["telefono"]);
			}
			if(isset($filtros["celular"])) {
					$cond[] = "celular = " . $this->oBD->escapar($filtros["celular"]);
			}
			if(isset($filtros["mostrar"])) {
					$cond[] = "mostrar = " . $this->oBD->escapar($filtros["mostrar"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			//$sql .= " ORDER BY fecha_creado ASC";
			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Persona_apoderado").": " . $e->getMessage());
		}
	}
	
	
	public function insertar($idpersona,$nombres,$apellidopaterno,$apellidomaterno,$correo,$sexo,$tipodoc,$ndoc,$parentesco,$telefono,$celular,$mostrar)
	{
		try {
			
			$this->iniciarTransaccion('dat_persona_apoderado_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idapoderado) FROM persona_apoderado");
			++$id;
			
			$estados = array('idapoderado' => $id							
							,'idpersona'=>$idpersona
							,'nombres'=>$nombres
							,'apellidopaterno'=>$apellidopaterno
							,'apellidomaterno'=>$apellidomaterno
							,'correo'=>$correo
							,'sexo'=>$sexo
							,'tipodoc'=>$tipodoc
							,'ndoc'=>$ndoc
							,'parentesco'=>$parentesco
							,'telefono'=>$telefono
							,'celular'=>$celular
							,'mostrar'=>$mostrar							
							);
			
			$this->oBD->insert('persona_apoderado', $estados);			
			$this->terminarTransaccion('dat_persona_apoderado_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_persona_apoderado_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Persona_apoderado").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $idpersona,$nombres,$apellidopaterno,$apellidomaterno,$correo,$sexo,$tipodoc,$ndoc,$parentesco,$telefono,$celular,$mostrar)
	{
		try {
			$this->iniciarTransaccion('dat_persona_apoderado_update');
			$estados = array('idpersona'=>$idpersona
							,'nombres'=>$nombres
							,'apellidopaterno'=>$apellidopaterno
							,'apellidomaterno'=>$apellidomaterno
							,'correo'=>$correo
							,'sexo'=>$sexo
							,'tipodoc'=>$tipodoc
							,'ndoc'=>$ndoc
							,'parentesco'=>$parentesco
							,'telefono'=>$telefono
							,'celular'=>$celular
							,'mostrar'=>$mostrar								
							);
			
			$this->oBD->update('persona_apoderado ', $estados, array('idapoderado' => $id));
		    $this->terminarTransaccion('dat_persona_apoderado_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Persona_apoderado").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT *  FROM persona_apoderado "
					. " WHERE idapoderado = " . $this->oBD->escapar($id);			
			$res = $this->oBD->consultarSQL($sql);			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Persona_apoderado").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('persona_apoderado', array('idapoderado' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Persona_apoderado").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('persona_apoderado', array($propiedad => $valor), array('idapoderado' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Persona_apoderado").": " . $e->getMessage());
		}
	}
   
		
}