<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		24-07-2017  
  * @copyright	Copyright (C) 2017. Todos los derechos reservados.
 */ 
class DatPersona_educacion extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Persona_educacion").": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros=null)
	{
		try {
			$sql = "SELECT COUNT(*) FROM persona_educacion";
			
			$cond = array();		
			
			if(isset($filtros["ideducacion"])) {
					$cond[] = "ideducacion = " . $this->oBD->escapar($filtros["ideducacion"]);
			}
			if(isset($filtros["idpersona"])) {
					$cond[] = "idpersona = " . $this->oBD->escapar($filtros["idpersona"]);
			}
			if(isset($filtros["centroestudios"])) {
					$cond[] = "centroestudios = " . $this->oBD->escapar($filtros["centroestudios"]);
			}
			if(isset($filtros["nivelestudios"])) {
					$cond[] = "nivelestudios = " . $this->oBD->escapar($filtros["nivelestudios"]);
			}
			if(isset($filtros["situacion"])) {
					$cond[] = "situacion = " . $this->oBD->escapar($filtros["situacion"]);
			}
			if(isset($filtros["fechade"])) {
					$cond[] = "fechade = " . $this->oBD->escapar($filtros["fechade"]);
			}
			if(isset($filtros["fechahasta"])) {
					$cond[] = "fechahasta = " . $this->oBD->escapar($filtros["fechahasta"]);
			}
			if(isset($filtros["actualmente"])) {
					$cond[] = "actualmente = " . $this->oBD->escapar($filtros["actualmente"]);
			}
			if(isset($filtros["mostrar"])) {
					$cond[] = "mostrar = " . $this->oBD->escapar($filtros["mostrar"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			return $this->oBD->consultarEscalarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("Persona_educacion").": " . $e->getMessage());
		}
	}
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT * FROM persona_educacion";			
			
			$cond = array();		
					
			
			if(isset($filtros["ideducacion"])) {
					$cond[] = "ideducacion = " . $this->oBD->escapar($filtros["ideducacion"]);
			}
			if(isset($filtros["idpersona"])) {
					$cond[] = "idpersona = " . $this->oBD->escapar($filtros["idpersona"]);
			}
			if(isset($filtros["centroestudios"])) {
					$cond[] = "centroestudios = " . $this->oBD->escapar($filtros["centroestudios"]);
			}
			if(isset($filtros["nivelestudios"])) {
					$cond[] = "nivelestudios = " . $this->oBD->escapar($filtros["nivelestudios"]);
			}
			if(isset($filtros["situacion"])) {
					$cond[] = "situacion = " . $this->oBD->escapar($filtros["situacion"]);
			}
			if(isset($filtros["fechade"])) {
					$cond[] = "fechade = " . $this->oBD->escapar($filtros["fechade"]);
			}
			if(isset($filtros["fechahasta"])) {
					$cond[] = "fechahasta = " . $this->oBD->escapar($filtros["fechahasta"]);
			}
			if(isset($filtros["actualmente"])) {
					$cond[] = "actualmente = " . $this->oBD->escapar($filtros["actualmente"]);
			}
			if(isset($filtros["mostrar"])) {
					$cond[] = "mostrar = " . $this->oBD->escapar($filtros["mostrar"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			//$sql .= " ORDER BY fecha_creado ASC";
			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Persona_educacion").": " . $e->getMessage());
		}
	}
	
	
	public function insertar($idpersona,$centroestudios,$nivelestudios,$situacion,$fechade,$fechahasta,$actualmente,$mostrar)
	{
		try {
			
			$this->iniciarTransaccion('dat_persona_educacion_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(ideducacion) FROM persona_educacion");
			++$id;
			
			$estados = array('ideducacion' => $id
							
							,'idpersona'=>$idpersona
							,'centroestudios'=>$centroestudios
							,'nivelestudios'=>$nivelestudios
							,'situacion'=>$situacion
							,'fechade'=>$fechade
							,'fechahasta'=>$fechahasta
							,'actualmente'=>$actualmente
							,'mostrar'=>$mostrar							
							);
			
			$this->oBD->insert('persona_educacion', $estados);			
			$this->terminarTransaccion('dat_persona_educacion_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_persona_educacion_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Persona_educacion").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $idpersona,$centroestudios,$nivelestudios,$situacion,$fechade,$fechahasta,$actualmente,$mostrar)
	{
		try {
			$this->iniciarTransaccion('dat_persona_educacion_update');
			$estados = array('idpersona'=>$idpersona
							,'centroestudios'=>$centroestudios
							,'nivelestudios'=>$nivelestudios
							,'situacion'=>$situacion
							,'fechade'=>$fechade
							,'fechahasta'=>$fechahasta
							,'actualmente'=>$actualmente
							,'mostrar'=>$mostrar								
							);
			
			$this->oBD->update('persona_educacion ', $estados, array('ideducacion' => $id));
		    $this->terminarTransaccion('dat_persona_educacion_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Persona_educacion").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT tb1.*,tb2.ape_paterno AS _ape_paterno  FROM persona_educacion tb1 LEFT JOIN personal tb2 ON tb1.idpersona=tb2.dni  "
					. " WHERE tb1.ideducacion = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Persona_educacion").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('persona_educacion', array('ideducacion' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Persona_educacion").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('persona_educacion', array($propiedad => $valor), array('ideducacion' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Persona_educacion").": " . $e->getMessage());
		}
	}
   
		
}