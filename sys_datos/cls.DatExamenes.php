<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		03-03-2017  
  * @copyright	Copyright (C) 2017. Todos los derechos reservados.
 */ 
class DatExamenes extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Examenes").": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros=null)
	{
		try {
			$sql = "SELECT COUNT(*) FROM examenes";
			
			$cond = array();		
			
			if(!empty($filtros["idexamen"])) {
					$cond[] = "idexamen = " . $this->oBD->escapar($filtros["idexamen"]);
			}
			if(!empty($filtros["idnivel"])) {
					$cond[] = "idnivel = " . $this->oBD->escapar($filtros["idnivel"]);
			}
			if(!empty($filtros["idunidad"])) {
					$cond[] = "idunidad = " . $this->oBD->escapar($filtros["idunidad"]);
			}
			if(!empty($filtros["idactividad"])) {
					$cond[] = "idactividad = " . $this->oBD->escapar($filtros["idactividad"]);
			}
			if(!empty($filtros["titulo"])) {
					$cond[] = "titulo = " . $this->oBD->escapar($filtros["titulo"]);
			}
			
			if(!empty($filtros["tipo"])) {
					$cond[] = "tipo = " . $this->oBD->escapar($filtros["tipo"]);
			}
			if(!empty($filtros["grupo"])) {
					$cond[] = "grupo = " . $this->oBD->escapar($filtros["grupo"]);
			}		
			
			if(!empty($filtros["estado"])) {
					$cond[] = "estado = " . $this->oBD->escapar($filtros["estado"]);
			}
			if(!empty($filtros["idpersonal"])) {
					$cond[] = "idpersonal = " . $this->oBD->escapar($filtros["idpersonal"]);
			}
			/*if(!empty($filtros["nintento"])) {
					$cond[] = "nintento = " . $this->oBD->escapar($filtros["nintento"]);
			}
			if(!empty($filtros["calificacion"])) {
					$cond[] = "calificacion = " . $this->oBD->escapar($filtros["calificacion"]);
			}*/
			if(!empty($filtros["fecharegistro"])) {
					$cond[] = "fecharegistro = " . $this->oBD->escapar($filtros["fecharegistro"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			return $this->oBD->consultarEscalarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("Examenes").": " . $e->getMessage());
		}
	}
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT * FROM examenes";			
			
			$cond = array();

			if(!empty($filtros["idexamen"])) {
					$cond[] = "idexamen = " . $this->oBD->escapar($filtros["idexamen"]);
			}
			if(!empty($filtros["idnivel"])) {
					$cond[] = "idnivel = " . $this->oBD->escapar($filtros["idnivel"]);
			}
			if(!empty($filtros["idunidad"])) {
					$cond[] = "idunidad = " . $this->oBD->escapar($filtros["idunidad"]);
			}
			if(!empty($filtros["idactividad"])) {
					$cond[] = "idactividad = " . $this->oBD->escapar($filtros["idactividad"]);
			}
			if(!empty($filtros["titulo"])) {
					$cond[] = "titulo LIKE '" . $filtros["titulo"]."%'";
			}
			
			if(!empty($filtros["tipo"])) {
					$cond[] = "tipo = " . $this->oBD->escapar($filtros["tipo"]);
			}
			if(!empty($filtros["grupo"])) {
					$cond[] = "grupo = " . $this->oBD->escapar($filtros["grupo"]);
			}		
			
			if(!empty($filtros["estado"])) {
					$cond[] = "estado = " . $this->oBD->escapar($filtros["estado"]);
			}
			if(!empty($filtros["idpersonal"])) {
					$cond[] = "idpersonal = " . $this->oBD->escapar($filtros["idpersonal"]);
			}
			if(!empty($filtros["fecharegistro"])) {
					$cond[] = "fecharegistro = " . $this->oBD->escapar($filtros["fecharegistro"]);
			}
			/*if(!empty($filtros["nintento"])) {
					$cond[] = "nintento = " . $this->oBD->escapar($filtros["nintento"]);
			}
			if(!empty($filtros["calificacion"])) {
					$cond[] = "calificacion = " . $this->oBD->escapar($filtros["calificacion"]);
			}*/			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			//$sql .= " ORDER BY fecha_creado ASC";
			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Examenes").": " . $e->getMessage());
		}
	}

	
	public function insertar($idnivel,$idunidad,$idactividad,$titulo,$descripcion,$portada,$fuente,$fuentesize,$tipo,$grupo,$aleatorio,$calificacion_por,$calificacion_en,$calificacion_total,$calificacion_min,$tiempo_por,$tiempo_total,$estado,$idpersonal,$nintento=1,$calificacion='U')
	{
		try {
			
			$this->iniciarTransaccion('dat_examenes_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idexamen) FROM examenes");
			++$id;
			
			$estados = array('idexamen' => $id							
							,'idnivel'=>$idnivel
							,'idunidad'=>$idunidad
							,'idactividad'=>$idactividad
							,'titulo'=>$titulo
							,'descripcion'=>$descripcion
							,'portada'=>$portada
							,'fuente'=>$fuente
							,'fuentesize'=>$fuentesize
							,'tipo'=>$tipo
							,'grupo'=>$grupo
							,'aleatorio'=>$aleatorio
							,'calificacion_por'=>$calificacion_por
							,'calificacion_en'=>$calificacion_en
							,'calificacion_total'=>$calificacion_total
							,'calificacion_min'=>$calificacion_min
							,'tiempo_por'=>$tiempo_por
							,'tiempo_total'=>$tiempo_total
							,'estado'=>$estado
							,'idpersonal'=>$idpersonal
							,'fecharegistro'=>date('Y-m-d')	
												
							);/*,'nintento'=>$nintento
							,'calificacion'=>$calificacion	*/
			
			$this->oBD->insert('examenes', $estados);			
			$this->terminarTransaccion('dat_examenes_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_examenes_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Examenes").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $idnivel,$idunidad,$idactividad,$titulo,$descripcion,$portada,$fuente,$fuentesize,$tipo,$grupo,$aleatorio,$calificacion_por,$calificacion_en,$calificacion_total,$calificacion_min,$tiempo_por,$tiempo_total,$estado,$idpersonal,$nintento=1,$calificacion='U')
	{
		try {
			$this->iniciarTransaccion('dat_examenes_update');
			$estados = array('idnivel'=>$idnivel
							,'idunidad'=>$idunidad
							,'idactividad'=>$idactividad
							,'titulo'=>$titulo
							,'descripcion'=>$descripcion
							,'portada'=>$portada
							,'fuente'=>$fuente
							,'fuentesize'=>$fuentesize
							,'tipo'=>$tipo
							,'grupo'=>$grupo
							,'aleatorio'=>$aleatorio
							,'calificacion_por'=>$calificacion_por
							,'calificacion_en'=>$calificacion_en
							,'calificacion_total'=>$calificacion_total
							,'calificacion_min'=>$calificacion_min
							,'tiempo_por'=>$tiempo_por
							,'tiempo_total'=>$tiempo_total
							,'estado'=>$estado
							,'idpersonal'=>$idpersonal
							,'fecharegistro'=>date('Y-m-d')	
														
							);/*,'nintento'=>$nintento
							,'calificacion'=>$calificacion	*/
			$this->oBD->update('examenes ', $estados, array('idexamen' => $id));
		    $this->terminarTransaccion('dat_examenes_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Examenes").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  *  FROM examenes  "
					. " WHERE idexamen = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Examenes").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			$this->iniciarTransaccion('dat_examenes_eliminar');
				$this->oBD->delete('examenes_preguntas', array('idexamen' => $id));
				$this->oBD->delete('examenes', array('idexamen' => $id));
			$this->terminarTransaccion('dat_examenes_eliminar');
			return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Examenes").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('examenes', array($propiedad => $valor), array('idexamen' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Examenes").": " . $e->getMessage());
		}
	}
   
		
}