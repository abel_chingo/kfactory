<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		31-05-2017  
  * @copyright	Copyright (C) 2017. Todos los derechos reservados.
 */ 
class DatTarea_archivos extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Tarea_archivos").": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros=null)
	{
		try {
			$sql = "SELECT COUNT(*) FROM tarea_archivos";
			
			$cond = array();		
			
			if(!empty($filtros["idtarea_archivos"])) {
					$cond[] = "idtarea_archivos = " . $this->oBD->escapar($filtros["idtarea_archivos"]);
			}
			if(!empty($filtros["tablapadre"])) {
					$cond[] = "tablapadre = " . $this->oBD->escapar($filtros["tablapadre"]);
			}
			if(!empty($filtros["idpadre"])) {
					$cond[] = "idpadre = " . $this->oBD->escapar($filtros["idpadre"]);
			}
			if(!empty($filtros["nombre"])) {
					$cond[] = "nombre = " . $this->oBD->escapar($filtros["nombre"]);
			}
			if(!empty($filtros["ruta"])) {
					$cond[] = "ruta = " . $this->oBD->escapar($filtros["ruta"]);
			}
			if(!empty($filtros["tipo"])) {
					$cond[] = "tipo = " . $this->oBD->escapar($filtros["tipo"]);
			}
			if(!empty($filtros["puntaje"])) {
					$cond[] = "puntaje = " . $this->oBD->escapar($filtros["puntaje"]);
			}
			if(!empty($filtros["habilidad"])) {
					$cond[] = "habilidad = " . $this->oBD->escapar($filtros["habilidad"]);
			}
			if(!empty($filtros["texto"])) {
					$cond[] = "texto = " . $this->oBD->escapar($filtros["texto"]);
			}
			if(!empty($filtros["idtarea_archivos_padre"])) {
					$cond[] = "idtarea_archivos_padre = " . $this->oBD->escapar($filtros["idtarea_archivos_padre"]);
			}
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			return $this->oBD->consultarEscalarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("Tarea_archivos").": " . $e->getMessage());
		}
	}
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT * FROM tarea_archivos";			
			
			$cond = array();		
					
			
			if(!empty($filtros["idtarea_archivos"])) {
					$cond[] = "idtarea_archivos = " . $this->oBD->escapar($filtros["idtarea_archivos"]);
			}
			if(!empty($filtros["tablapadre"])) {
					$cond[] = "tablapadre = " . $this->oBD->escapar($filtros["tablapadre"]);
			}
			if(!empty($filtros["idpadre"])) {
					$cond[] = "idpadre = " . $this->oBD->escapar($filtros["idpadre"]);
			}
			if(!empty($filtros["nombre"])) {
					$cond[] = "nombre = " . $this->oBD->escapar($filtros["nombre"]);
			}
			if(!empty($filtros["ruta"])) {
					$cond[] = "ruta = " . $this->oBD->escapar($filtros["ruta"]);
			}
			if(!empty($filtros["tipo"])) {
					$cond[] = "tipo = " . $this->oBD->escapar($filtros["tipo"]);
			}
			if(!empty($filtros["puntaje"])) {
					$cond[] = "puntaje = " . $this->oBD->escapar($filtros["puntaje"]);
			}
			if(!empty($filtros["habilidad"])) {
					$cond[] = "habilidad = " . $this->oBD->escapar($filtros["habilidad"]);
			}
			if(!empty($filtros["texto"])) {
					$cond[] = "texto = " . $this->oBD->escapar($filtros["texto"]);
			}	
			if(!empty($filtros["idtarea_archivos_padre"])) {
					$cond[] = "idtarea_archivos_padre = " . $this->oBD->escapar($filtros["idtarea_archivos_padre"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			$sql .= " ORDER BY idtarea_archivos ASC";
			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Tarea_archivos").": " . $e->getMessage());
		}
	}
	public function listarall()
	{
		try {
			$sql = "SELECT  *  FROM tarea_archivos  ";
			
			$res = $this->oBD->consultarSQL($sql);			
			return empty($res) ? null : $res;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("List all")." ".JrTexto::_("Tarea_archivos").": " . $e->getMessage());
		}
	}
	
	public function insertar($tablapadre,$idpadre,$nombre,$ruta,$tipo,$puntaje,$habilidad,$texto,$idtarea_archivos_padre)
	{
		try {
			
			$this->iniciarTransaccion('dat_tarea_archivos_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idtarea_archivos) FROM tarea_archivos");
			++$id;
			
			$estados = array('idtarea_archivos' => $id
							,'nombre'=>$nombre
							,'ruta'=>$ruta
							,'tipo'=>$tipo
							);
			if(!empty($idpadre) && $idpadre!=''){
				$estados["idpadre"] = $idpadre;
			}
			if(!empty($tablapadre) && $tablapadre!=''){
				$estados["tablapadre"] = $tablapadre;
			}
			if(!empty($puntaje) && $puntaje!=''){
				$estados["puntaje"] = $puntaje;
			}
			if(!empty($habilidad)){
				$estados["habilidad"] = $habilidad;
			}
			if(!empty($texto)){
				$estados["texto"] = $texto;
			}
			if(!empty($idtarea_archivos_padre)){
				$estados["idtarea_archivos_padre"] = $idtarea_archivos_padre;
			}
			
			$this->oBD->insert('tarea_archivos', $estados);			
			$this->terminarTransaccion('dat_tarea_archivos_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_tarea_archivos_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Tarea_archivos").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $tablapadre,$idpadre,$nombre,$ruta,$tipo,$puntaje,$habilidad,$texto,$idtarea_archivos_padre)
	{
		try {
			$this->iniciarTransaccion('dat_tarea_archivos_update');
			$estados = array('nombre'=>$nombre
							,'ruta'=>$ruta
							,'tipo'=>$tipo								
							);
			if(!empty($idpadre) && $idpadre!=''){
				$estados["idpadre"] = $idpadre;
			}
			if(!empty($tablapadre) && $tablapadre!=''){
				$estados["tablapadre"] = $tablapadre;
			}
			if(!empty($puntaje) && $puntaje!=''){
				$estados["puntaje"] = $puntaje;
			}
			if(!empty($habilidad)){
				$estados["habilidad"] = $habilidad;
			}
			if(!empty($texto)){
				$estados["texto"] = $texto;
			}
			if(!empty($idtarea_archivos_padre)){
				$estados["idtarea_archivos_padre"] = $idtarea_archivos_padre;
			}
			$this->oBD->update('tarea_archivos ', $estados, array('idtarea_archivos' => $id));
		    $this->terminarTransaccion('dat_tarea_archivos_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Tarea_archivos").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  *  FROM tarea_archivos  "
					. " WHERE idtarea_archivos = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Tarea_archivos").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('tarea_archivos', array('idtarea_archivos' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Tarea_archivos").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('tarea_archivos', array($propiedad => $valor), array('idtarea_archivos' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Tarea_archivos").": " . $e->getMessage());
		}
	}
   
		
}