<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		12-07-2017  
  * @copyright	Copyright (C) 2017. Todos los derechos reservados.
 */ 
class DatBib_tipo extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Bib_tipo").": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros=null)
	{
		try {
			$sql = "SELECT COUNT(*) FROM bib_tipo";
			
			$cond = array();		
			
			if(!empty($filtros["id_tipo"])) {
					$cond[] = "id_tipo = " . $this->oBD->escapar($filtros["id_tipo"]);
			}
			if(!empty($filtros["nombre"])) {
					$cond[] = "nombre = " . $this->oBD->escapar($filtros["nombre"]);
			}
			if(!empty($filtros["filtro"])) {
					$cond[] = "filtro = " . $this->oBD->escapar($filtros["filtro"]);
			}
			if(!empty($filtros["extension"])) {
					$cond[] = "extension = " . $this->oBD->escapar($filtros["extension"]);
			}	
			if(!empty($filtros["id_registro"])) {
					$cond[] = "id_registro = " . $this->oBD->escapar($filtros["id_registro"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			return $this->oBD->consultarEscalarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("Bib_tipo").": " . $e->getMessage());
		}
	}
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT * FROM bib_tipo";			
			
			$cond = array();		
					
			
			if(!empty($filtros["id_tipo"])) {
					$cond[] = "id_tipo = " . $this->oBD->escapar($filtros["id_tipo"]);
			}
			if(!empty($filtros["nombre"])) {
					$cond[] = "nombre = " . $this->oBD->escapar($filtros["nombre"]);
			}
			if(!empty($filtros["filtro"])) {
					$cond[] = "filtro = " . $this->oBD->escapar($filtros["filtro"]);
			}
			if(!empty($filtros["extension"])) {
					$cond[] = "extension = " . $this->oBD->escapar($filtros["extension"]);
			}
			if(!empty($filtros["id_registro"])) {
					$cond[] = "id_registro = " . $this->oBD->escapar($filtros["id_registro"]);
			}				
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			//$sql .= " ORDER BY fecha_creado ASC";
			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Bib_tipo").": " . $e->getMessage());
		}
	}
	public function listarall()
	{
		try {
			$sql = "SELECT  *  FROM bib_tipo  ";
			
			$res = $this->oBD->consultarSQL($sql);			
			return empty($res) ? null : $res;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("List all")." ".JrTexto::_("Bib_tipo").": " . $e->getMessage());
		}
	}
	
	public function insertar($nombre,$filtro,$extension)
	{
		try {
			
			$this->iniciarTransaccion('dat_bib_tipo_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(id_tipo) FROM bib_tipo");
			++$id;
			
			$estados = array('id_tipo' => $id
							
							,'nombre'=>$nombre
							,'filtro'=>$filtro
							,'extension'=>$extension
							,'id_registro'=>$id_registro							
							);
			
			$this->oBD->insert('bib_tipo', $estados);			
			$this->terminarTransaccion('dat_bib_tipo_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_bib_tipo_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Bib_tipo").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $nombre,$filtro,$extension)
	{
		try {
			$this->iniciarTransaccion('dat_bib_tipo_update');
			$estados = array('nombre'=>$nombre
							,'filtro'=>$filtro
							,'extension'=>$extension	
							,'id_registro'=>$id_registro							
							);
			
			$this->oBD->update('bib_tipo ', $estados, array('id_tipo' => $id));
		    $this->terminarTransaccion('dat_bib_tipo_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Bib_tipo").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  *  FROM bib_tipo  "
					. " WHERE id_tipo = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Bib_tipo").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('bib_tipo', array('id_tipo' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Bib_tipo").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('bib_tipo', array($propiedad => $valor), array('id_tipo' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Bib_tipo").": " . $e->getMessage());
		}
	}
   
		
}