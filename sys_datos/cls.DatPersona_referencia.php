<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		24-07-2017  
  * @copyright	Copyright (C) 2017. Todos los derechos reservados.
 */ 
class DatPersona_referencia extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Persona_referencia").": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros=null)
	{
		try {
			$sql = "SELECT COUNT(*) FROM persona_referencia";
			
			$cond = array();		
			
			if(isset($filtros["idreferencia"])) {
					$cond[] = "idreferencia = " . $this->oBD->escapar($filtros["idreferencia"]);
			}
			if(isset($filtros["idpersona"])) {
					$cond[] = "idpersona = " . $this->oBD->escapar($filtros["idpersona"]);
			}
			if(isset($filtros["nombre"])) {
					$cond[] = "nombre = " . $this->oBD->escapar($filtros["nombre"]);
			}
			if(isset($filtros["cargo"])) {
					$cond[] = "cargo = " . $this->oBD->escapar($filtros["cargo"]);
			}
			if(isset($filtros["relacion"])) {
					$cond[] = "relacion = " . $this->oBD->escapar($filtros["relacion"]);
			}
			if(isset($filtros["correo"])) {
					$cond[] = "correo = " . $this->oBD->escapar($filtros["correo"]);
			}
			if(isset($filtros["telefono"])) {
					$cond[] = "telefono = " . $this->oBD->escapar($filtros["telefono"]);
			}
			if(isset($filtros["mostrar"])) {
					$cond[] = "mostrar = " . $this->oBD->escapar($filtros["mostrar"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			return $this->oBD->consultarEscalarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("Persona_referencia").": " . $e->getMessage());
		}
	}
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT * FROM persona_referencia";			
			
			$cond = array();		
					
			
			if(isset($filtros["idreferencia"])) {
					$cond[] = "idreferencia = " . $this->oBD->escapar($filtros["idreferencia"]);
			}
			if(isset($filtros["idpersona"])) {
					$cond[] = "idpersona = " . $this->oBD->escapar($filtros["idpersona"]);
			}
			if(isset($filtros["nombre"])) {
					$cond[] = "nombre = " . $this->oBD->escapar($filtros["nombre"]);
			}
			if(isset($filtros["cargo"])) {
					$cond[] = "cargo = " . $this->oBD->escapar($filtros["cargo"]);
			}
			if(isset($filtros["relacion"])) {
					$cond[] = "relacion = " . $this->oBD->escapar($filtros["relacion"]);
			}
			if(isset($filtros["correo"])) {
					$cond[] = "correo = " . $this->oBD->escapar($filtros["correo"]);
			}
			if(isset($filtros["telefono"])) {
					$cond[] = "telefono = " . $this->oBD->escapar($filtros["telefono"]);
			}
			if(isset($filtros["mostrar"])) {
					$cond[] = "mostrar = " . $this->oBD->escapar($filtros["mostrar"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			//$sql .= " ORDER BY fecha_creado ASC";
			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Persona_referencia").": " . $e->getMessage());
		}
	}
	
	
	public function insertar($idpersona,$nombre,$cargo,$relacion,$correo,$telefono,$mostrar)
	{
		try {
			
			$this->iniciarTransaccion('dat_persona_referencia_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idreferencia) FROM persona_referencia");
			++$id;
			
			$estados = array('idreferencia' => $id
							
							,'idpersona'=>$idpersona
							,'nombre'=>$nombre
							,'cargo'=>$cargo
							,'relacion'=>$relacion
							,'correo'=>$correo
							,'telefono'=>$telefono
							,'mostrar'=>$mostrar							
							);
			
			$this->oBD->insert('persona_referencia', $estados);			
			$this->terminarTransaccion('dat_persona_referencia_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_persona_referencia_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Persona_referencia").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $idpersona,$nombre,$cargo,$relacion,$correo,$telefono,$mostrar)
	{
		try {
			$this->iniciarTransaccion('dat_persona_referencia_update');
			$estados = array('idpersona'=>$idpersona
							,'nombre'=>$nombre
							,'cargo'=>$cargo
							,'relacion'=>$relacion
							,'correo'=>$correo
							,'telefono'=>$telefono
							,'mostrar'=>$mostrar								
							);
			
			$this->oBD->update('persona_referencia ', $estados, array('idreferencia' => $id));
		    $this->terminarTransaccion('dat_persona_referencia_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Persona_referencia").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT tb1.*,tb2.ape_paterno AS _ape_paterno  FROM persona_referencia tb1 LEFT JOIN personal tb2 ON tb1.idpersona=tb2.dni  "
					. " WHERE tb1.idreferencia = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Persona_referencia").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('persona_referencia', array('idreferencia' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Persona_referencia").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('persona_referencia', array($propiedad => $valor), array('idreferencia' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Persona_referencia").": " . $e->getMessage());
		}
	}
   
		
}