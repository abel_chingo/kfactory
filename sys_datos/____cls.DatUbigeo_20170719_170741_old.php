<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		18-11-2016  
  * @copyright	Copyright (C) 2016. Todos los derechos reservados.
 */ 
class DatUbigeo extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Ubigeo").": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros=null)
	{
		try {
			$sql = "SELECT COUNT(*) FROM ubigeo";
			
			$cond = array();		
			
			if(!empty($filtros["id_ubigeo"])) {
					$cond[] = "id_ubigeo = " . $this->oBD->escapar($filtros["id_ubigeo"]);
			}
			if(!empty($filtros["pais"])) {
					$cond[] = "pais = " . $this->oBD->escapar($filtros["pais"]);
			}
			if(!empty($filtros["departamento"])) {
					$cond[] = "departamento = " . $this->oBD->escapar($filtros["departamento"]);
			}
			if(!empty($filtros["provincia"])) {
					$cond[] = "provincia = " . $this->oBD->escapar($filtros["provincia"]);
			}
			if(!empty($filtros["distrito"])) {
					$cond[] = "distrito = " . $this->oBD->escapar($filtros["distrito"]);
			}
			if(!empty($filtros["ciudad"])) {
					$cond[] = "ciudad = " . $this->oBD->escapar($filtros["ciudad"]);
			}			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			return $this->oBD->consultarEscalarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("Ubigeo").": " . $e->getMessage());
		}
	}
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT * FROM ubigeo";			
			
			$cond = array();		
					
			
			if(!empty($filtros["id_ubigeo"])) {
					$cond[] = "id_ubigeo = " . $this->oBD->escapar($filtros["id_ubigeo"]);
			}
			if(!empty($filtros["pais"])) {
					$cond[] = "pais = " . $this->oBD->escapar($filtros["pais"]);
			}
			if(!empty($filtros["departamento"])) {
					$cond[] = "departamento = " . $this->oBD->escapar($filtros["departamento"]);
			}
			if(!empty($filtros["provincia"])) {
					$cond[] = "provincia = " . $this->oBD->escapar($filtros["provincia"]);
			}
			if(!empty($filtros["distrito"])) {
					$cond[] = "distrito = " . $this->oBD->escapar($filtros["distrito"]);
			}
			if(!empty($filtros["ciudad"])) {
					$cond[] = "ciudad = " . $this->oBD->escapar($filtros["ciudad"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			//$sql .= " ORDER BY fecha_creado ASC";
			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Ubigeo").": " . $e->getMessage());
		}
	}
	public function listarall()
	{
		try {
			$sql = "SELECT  *  FROM ubigeo  ";
			
			$res = $this->oBD->consultarSQL($sql);			
			return empty($res) ? null : $res;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("List all")." ".JrTexto::_("Ubigeo").": " . $e->getMessage());
		}
	}
	
	public function insertar($id,$pais,$departamento,$provincia,$distrito,$ciudad)
	{
		try {
			
			$this->iniciarTransaccion('dat_ubigeo_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(id_ubigeo) FROM ubigeo");
			++$id;
			
			$estados = array('id_ubigeo' => $id							
							,'pais'=>$pais
							,'departamento'=>$departamento
							,'provincia'=>$provincia
							,'distrito'=>$distrito
							,'ciudad'=>$ciudad							
							);
			
			$this->oBD->insert('ubigeo', $estados);			
			$this->terminarTransaccion('dat_ubigeo_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_ubigeo_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Ubigeo").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $pais,$departamento,$provincia,$distrito,$ciudad)
	{
		try {
			$this->iniciarTransaccion('dat_ubigeo_update');
			$estados = array('pais'=>$pais
							,'departamento'=>$departamento
							,'provincia'=>$provincia
							,'distrito'=>$distrito
							,'ciudad'=>$ciudad								
							);
			
			$this->oBD->update('ubigeo ', $estados, array('id_ubigeo' => $id));
		    $this->terminarTransaccion('dat_ubigeo_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Ubigeo").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  *  FROM ubigeo  "
					. " WHERE id_ubigeo = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Ubigeo").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('ubigeo', array('id_ubigeo' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Ubigeo").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('ubigeo', array($propiedad => $valor), array('id_ubigeo' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Ubigeo").": " . $e->getMessage());
		}
	}
   
		
}