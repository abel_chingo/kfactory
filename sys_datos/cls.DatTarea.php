<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		31-05-2017  
  * @copyright	Copyright (C) 2017. Todos los derechos reservados.
 */ 
class DatTarea extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Tarea").": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros=null)
	{
		try {
			$sql = "SELECT COUNT(*) FROM tarea";
			
			$cond = array();		
			
			if(!empty($filtros["idtarea"])) {
					$cond[] = "idtarea = " . $this->oBD->escapar($filtros["idtarea"]);
			}
			if(!empty($filtros["idnivel"])) {
					$cond[] = "idnivel = " . $this->oBD->escapar($filtros["idnivel"]);
			}
			if(!empty($filtros["idunidad"])) {
					$cond[] = "idunidad = " . $this->oBD->escapar($filtros["idunidad"]);
			}
			if(!empty($filtros["idactividad"])) {
					$cond[] = "idactividad = " . $this->oBD->escapar($filtros["idactividad"]);
			}
			if(!empty($filtros["iddocente"])) {
					$cond[] = "iddocente = " . $this->oBD->escapar($filtros["iddocente"]);
			}
			if(!empty($filtros["nombre"])) {
					$cond[] = "nombre = " . $this->oBD->escapar($filtros["nombre"]);
			}
			if(!empty($filtros["descripcion"])) {
					$cond[] = "descripcion = " . $this->oBD->escapar($filtros["descripcion"]);
			}
			if(!empty($filtros["foto"])) {
					$cond[] = "foto = " . $this->oBD->escapar($filtros["foto"]);
			}
			if(!empty($filtros["habilidades"])) {
					$cond[] = "habilidades = " . $this->oBD->escapar($filtros["habilidades"]);
			}
			if(!empty($filtros["puntajemaximo"])) {
					$cond[] = "puntajemaximo = " . $this->oBD->escapar($filtros["puntajemaximo"]);
			}
			if(!empty($filtros["puntajeminimo"])) {
					$cond[] = "puntajeminimo = " . $this->oBD->escapar($filtros["puntajeminimo"]);
			}
			if(!empty($filtros["estado"])) {
					$cond[] = "estado = " . $this->oBD->escapar($filtros["estado"]);
			}
			if(!empty($filtros["eliminado"])) {
					$cond[] = "eliminado = " . $this->oBD->escapar($filtros["eliminado"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			return $this->oBD->consultarEscalarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("Tarea").": " . $e->getMessage());
		}
	}
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT * FROM tarea";			
			
			$cond = array();		
					
			if(!empty($filtros["idtarea"])) {
					$cond[] = "idtarea = " . $this->oBD->escapar($filtros["idtarea"]);
			}
			if(!empty($filtros["idnivel"])) {
					$cond[] = "idnivel = " . $this->oBD->escapar($filtros["idnivel"]);
			}
			if(!empty($filtros["idunidad"])) {
					$cond[] = "idunidad = " . $this->oBD->escapar($filtros["idunidad"]);
			}
			if(!empty($filtros["idactividad"])) {
					$cond[] = "idactividad = " . $this->oBD->escapar($filtros["idactividad"]);
			}
			if(!empty($filtros["iddocente"])) {
					$cond[] = "iddocente = " . $this->oBD->escapar($filtros["iddocente"]);
			}
			if(!empty($filtros["nombre"])) {
					$cond[] = "nombre = " . $this->oBD->escapar($filtros["nombre"]);
			}
			if(!empty($filtros["descripcion"])) {
					$cond[] = "descripcion = " . $this->oBD->escapar($filtros["descripcion"]);
			}
			if(!empty($filtros["foto"])) {
					$cond[] = "foto = " . $this->oBD->escapar($filtros["foto"]);
			}
			if(!empty($filtros["habilidades"])) {
					$cond[] = "habilidades = " . $this->oBD->escapar($filtros["habilidades"]);
			}
			if(!empty($filtros["puntajemaximo"])) {
					$cond[] = "puntajemaximo = " . $this->oBD->escapar($filtros["puntajemaximo"]);
			}
			if(!empty($filtros["puntajeminimo"])) {
					$cond[] = "puntajeminimo = " . $this->oBD->escapar($filtros["puntajeminimo"]);
			}
			if(!empty($filtros["estado"])) {
					$cond[] = "estado = " . $this->oBD->escapar($filtros["estado"]);
			}
			if(isset($filtros["eliminado"])) {
					$cond[] = "eliminado = " . $this->oBD->escapar($filtros["eliminado"]);
			}			
			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			//$sql .= " ORDER BY fecha_creado ASC";
			//echo $sql;
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Tarea").": " . $e->getMessage());
		}
	}
	public function listarall()
	{
		try {
			$sql = "SELECT  *  FROM tarea  ";
			
			$res = $this->oBD->consultarSQL($sql);			
			return empty($res) ? null : $res;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("List all")." ".JrTexto::_("Tarea").": " . $e->getMessage());
		}
	}
	
	public function insertar($idnivel,$idunidad,$idactividad,$iddocente,$nombre,$descripcion,$foto,$habilidades,$puntajemaximo,$puntajeminimo,$estado,$eliminado)
	{
		try {
			
			$this->iniciarTransaccion('dat_tarea_insert');
			
			$id = $this->oBD->consultarEscalarSQL("SELECT MAX(idtarea) FROM tarea");
			++$id;
			
			$estados = array('idtarea' => $id
							
							,'idnivel'=>$idnivel
							,'idunidad'=>$idunidad
							,'idactividad'=>$idactividad
							,'iddocente'=>$iddocente
							,'nombre'=>$nombre
							,'descripcion'=>$descripcion
							,'foto'=>$foto
							,'habilidades'=>$habilidades
							,'puntajemaximo'=>$puntajemaximo
							,'puntajeminimo'=>$puntajeminimo
							,'estado'=>$estado
							,'eliminado'=>$eliminado
							);
			
			$this->oBD->insert('tarea', $estados);			
			$this->terminarTransaccion('dat_tarea_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_tarea_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Tarea").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $idnivel,$idunidad,$idactividad,$iddocente,$nombre,$descripcion,$foto,$habilidades,$puntajemaximo,$puntajeminimo,$estado,$eliminado)
	{
		try {
			$this->iniciarTransaccion('dat_tarea_update');
			$estados = array('idnivel'=>$idnivel
							,'idunidad'=>$idunidad
							,'idactividad'=>$idactividad
							,'iddocente'=>$iddocente
							,'nombre'=>$nombre
							,'descripcion'=>$descripcion
							,'foto'=>$foto
							,'habilidades'=>$habilidades
							,'puntajemaximo'=>$puntajemaximo
							,'puntajeminimo'=>$puntajeminimo
							,'estado'=>$estado
							,'eliminado'=>$eliminado
							);
			
			$this->oBD->update('tarea ', $estados, array('idtarea' => $id));
		    $this->terminarTransaccion('dat_tarea_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Tarea").": " . $e->getMessage());
		}
	}
	public function get($id)
	{
		try {
			$sql = "SELECT  *  FROM tarea  "
					. " WHERE idtarea = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Tarea").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('tarea', array('idtarea' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Tarea").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			$this->oBD->update('tarea', array($propiedad => $valor), array('idtarea' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Tarea").": " . $e->getMessage());
		}
	}
   
		
}