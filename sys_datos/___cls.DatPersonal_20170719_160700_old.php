<?php
 /**
 * @autor		Generador Abel Chingo Tello, ACHT
 * @fecha		16-11-2016  
  * @copyright	Copyright (C) 2016. Todos los derechos reservados.
 */ 
class DatPersonal extends DatBase
{
	public function __construct()
	{
		try {
			parent::conectar();
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("connect")." ".JrTexto::_("Personal").": " . $e->getMessage());
		}
	}
	public function getNumRegistros($filtros=null)
	{
		try {
			$sql = "SELECT COUNT(*) FROM personal";
			
			$cond = array();		
			
			if(!empty($filtros["dni"])) {
					$cond[] = "dni = " . $this->oBD->escapar($filtros["dni"]);
			}
			if(!empty($filtros["ape_paterno"])) {
					$cond[] = "ape_paterno = " . $this->oBD->like($filtros["ape_paterno"]);
			}
			if(!empty($filtros["ape_materno"])) {
					$cond[] = "ape_materno = " . $this->oBD->like($filtros["ape_materno"]);
			}
			if(!empty($filtros["nombre"])) {
					$cond[] = "nombre = " . $this->oBD->like($filtros["nombre"]);
			}
			if(!empty($filtros["fechanac"])) {
					$cond[] = "fechanac = " . $this->oBD->escapar($filtros["fechanac"]);
			}
			if(!empty($filtros["sexo"])) {
					$cond[] = "sexo = " . $this->oBD->escapar($filtros["sexo"]);
			}
			if(!empty($filtros["estado_civil"])) {
					$cond[] = "estado_civil = " . $this->oBD->escapar($filtros["estado_civil"]);
			}
			if(!empty($filtros["ubigeo"])) {
					$cond[] = "ubigeo = " . $this->oBD->escapar($filtros["ubigeo"]);
			}
						
			if(!empty($filtros["email"])) {
					$cond[] = "email = " . $this->oBD->escapar($filtros["email"]);
			}
			if(!empty($filtros["idugel"])) {
					$cond[] = "idugel = " . $this->oBD->escapar($filtros["idugel"]);
			}
			if(!empty($filtros["regusuario"])) {
					$cond[] = "regusuario = " . $this->oBD->escapar($filtros["regusuario"]);
			}
			if(!empty($filtros["regfecha"])) {
					$cond[] = "regfecha = " . $this->oBD->escapar($filtros["regfecha"]);
			}
			if(!empty($filtros["usuario"])) {
					$cond[] = "usuario = " . $this->oBD->escapar($filtros["usuario"]);
			}
			
			if(!empty($filtros["token"])) {
					$cond[] = "token = " . $this->oBD->escapar($filtros["token"]);
			}
			
			if(isset($filtros["estado"])) {
					$cond[] = "estado = " . $this->oBD->escapar($filtros["estado"]);
			}
			if(isset($filtros["situacion"])) {					
					$cond[] = "situacion = " . $this->oBD->escapar($filtros["situacion"]);
			}			

			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			return $this->oBD->consultarEscalarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("count")." ".JrTexto::_("Personal").": " . $e->getMessage());
		}
	}
	public function buscar($filtros=null)
	{
		try {
			$sql = "SELECT * FROM personal";			
			
			$cond = array();		
					
			
			if(!empty($filtros["dni"])) {
					$cond[] = "dni = " . $this->oBD->escapar($filtros["dni"]);
			}
			if(!empty($filtros["ape_paterno"])) {
					$cond[] = "ape_paterno = " . $this->oBD->like($filtros["ape_paterno"]);
			}
			if(!empty($filtros["ape_materno"])) {
					$cond[] = "ape_materno = " . $this->oBD->like($filtros["ape_materno"]);
			}
			if(!empty($filtros["nombre"])) {
					$cond[] = "nombre = " . $this->oBD->like($filtros["nombre"]);
			}
			if(!empty($filtros["fechanac"])) {
					$cond[] = "fechanac = " . $this->oBD->escapar($filtros["fechanac"]);
			}
			if(!empty($filtros["sexo"])) {
					$cond[] = "sexo = " . $this->oBD->escapar($filtros["sexo"]);
			}
			if(!empty($filtros["estado_civil"])) {
					$cond[] = "estado_civil = " . $this->oBD->escapar($filtros["estado_civil"]);
			}
			if(!empty($filtros["ubigeo"])) {
					$cond[] = "ubigeo = " . $this->oBD->escapar($filtros["ubigeo"]);
			}
						
			if(!empty($filtros["email"])) {
					$cond[] = "email = " . $this->oBD->escapar($filtros["email"]);
			}
			if(!empty($filtros["idugel"])) {
					$cond[] = "idugel = " . $this->oBD->escapar($filtros["idugel"]);
			}
			if(!empty($filtros["regusuario"])) {
					$cond[] = "regusuario = " . $this->oBD->escapar($filtros["regusuario"]);
			}
			if(!empty($filtros["regfecha"])) {
					$cond[] = "regfecha = " . $this->oBD->escapar($filtros["regfecha"]);
			}
			if(!empty($filtros["usuario"])) {
					$cond[] = "usuario = " . $this->oBD->escapar($filtros["usuario"]);
			}
			
			if(!empty($filtros["token"])) {
					$cond[] = "token = " . $this->oBD->escapar($filtros["token"]);
			}
			
			if(isset($filtros["estado"])) {
					$cond[] = "estado = " . $this->oBD->escapar($filtros["estado"]);
			}
			if(isset($filtros["situacion"])) {					
					$cond[] = "situacion = " . $this->oBD->escapar($filtros["situacion"]);
			}

			if(!empty($cond)) {
				$sql .= " WHERE " . implode(' AND ', $cond);
			}
			
			//$sql .= " ORDER BY fecha_creado ASC";
			
			return $this->oBD->consultarSQL($sql);
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Search")." ".JrTexto::_("Personal").": " . $e->getMessage());
		}
	}

	public function getxCredencial($usuario, $clave)
	{
		try {
			$sql = "SELECT * FROM personal "
					. " WHERE (usuario = " . $this->oBD->escapar($usuario)." OR email=". $this->oBD->escapar($usuario).") "
					. " AND clave = '" . md5($clave) . "' AND estado=1";
			$res = $this->oBD->consultarSQL($sql);
			
			return !empty($res) ? $res[0] : null;
		} catch(Exception $e) {
			throw new Exception(JrTexto::_('Error')."\n ".JrTexto::_('Validate').' '.JrTexto::_('enter'). $e->getMessage());
		}
	}

	public function getxusuarioemail($usuema)
	{
		try {
			$sql = "SELECT *  FROM personal "
					. " WHERE usuario = " . $this->oBD->escapar($usuema)." OR email = " . $this->oBD->escapar($usuema);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return !empty($res) ? $res[0] : null;
		} catch(Exception $e) {
			throw new Exception(JrTexto::_('Error')."\n ".JrTexto::_('Get').' '.JrTexto::_('user'). $e->getMessage());
		}
	}
	public function getxusuario($usuario)
	{
		try {
			$sql = "SELECT *  FROM personal  "
					. " WHERE usuario = " . $this->oBD->escapar($usuario);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return !empty($res) ? $res[0] : null;
		} catch(Exception $e) {
			throw new Exception(JrTexto::_('Error')."\n ".JrTexto::_('Get').' '.JrTexto::_('users'). $e->getMessage());
		}
	}

	public function getxtoken($token)
	{
		try {
			$sql = "SELECT *  FROM personal "
					. " WHERE token = " . $this->oBD->escapar($token);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return !empty($res) ? $res[0] : null;
		} catch(Exception $e) {
			throw new Exception(JrTexto::_('Error')."\n ".JrTexto::_('Get').' '.JrTexto::_('user').$e->getMessage());
		}
	}
	
	public function insertar($dni,$ape_paterno,$ape_materno,$nombre,$fechanac,$sexo,$estado_civil,$ubigeo,$urbanizacion,$direccion,$telefono,$celular,$email,$idugel,$regusuario,$regfecha,$usuario,$clave,$token,$rol,$foto,$estado,$situacion)
	{
		try {
			
			$this->iniciarTransaccion('dat_personal_insert');		
			if(!$estado)$estado=0;
			$estados = array('dni' => $dni							
							,'ape_paterno'=>$ape_paterno
							,'ape_materno'=>$ape_materno
							,'nombre'=>$nombre
							,'fechanac'=>$fechanac
							,'sexo'=>$sexo
							,'estado_civil'=>$estado_civil
							,'ubigeo'=>$ubigeo
							,'urbanizacion'=>$urbanizacion
							,'direccion'=>$direccion
							,'telefono'=>$telefono
							,'celular'=>$celular
							,'email'=>$email
							,'idugel'=>$idugel
							,'regusuario'=>$regusuario
							,'regfecha'=>$regfecha
							,'usuario'=>$usuario
							,'clave'=>$clave
							,'token'=>$token
							,'rol'=>$rol
							,'foto'=>$foto
							,'estado'=>$estado
							,'situacion'=>$situacion							
							);
			
			$this->oBD->insert('personal', $estados);			
			$this->terminarTransaccion('dat_personal_insert');			
			return $id;

		} catch(Exception $e) {
			$this->cancelarTransaccion('dat_personal_insert');
			throw new Exception("ERROR\n".JrTexto::_("Insert")." ".JrTexto::_("Personal").": " . $e->getMessage());
		}
	}
	public function actualizar($id, $ape_paterno,$ape_materno,$nombre,$fechanac,$sexo,$estado_civil,$ubigeo,$urbanizacion,$direccion,$telefono,$celular,$email,$idugel,$regusuario,$regfecha,$usuario,$clave,$token,$rol,$foto,$estado,$situacion)
	{
		try {
			$this->iniciarTransaccion('dat_personal_update');
			$estados = array('ape_paterno'=>$ape_paterno
							,'ape_materno'=>$ape_materno
							,'nombre'=>$nombre
							,'fechanac'=>$fechanac
							,'sexo'=>$sexo
							,'estado_civil'=>$estado_civil
							,'ubigeo'=>$ubigeo
							,'urbanizacion'=>$urbanizacion
							,'direccion'=>$direccion
							,'telefono'=>$telefono
							,'celular'=>$celular
							,'email'=>$email
							,'idugel'=>$idugel
							,'regusuario'=>$regusuario
							,'regfecha'=>$regfecha
							,'usuario'=>$usuario
							,'clave'=>$clave
							,'token'=>$token
							,'rol'=>$rol
							,'foto'=>$foto
							,'estado'=>$estado
							,'situacion'=>$situacion								
							);
			
			$this->oBD->update('personal ', $estados, array('dni' => $dni));
		    $this->terminarTransaccion('dat_personal_update');
		    return $id;
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Personal").": " . $e->getMessage());
		}
	}

	public function get($id)
	{
		try {
			$sql = "SELECT  *  FROM personal  "
					. " WHERE dni = " . $this->oBD->escapar($id);
			
			$res = $this->oBD->consultarSQL($sql);
			
			return empty($res) ? null : $res[0];
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Get")." ".JrTexto::_("Personal").": " . $e->getMessage());
		}
	}

	public function eliminar($id)
	{
		try {
			return $this->oBD->delete('personal', array('dni' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Delete")." ".JrTexto::_("Personal").": " . $e->getMessage());
		}
	}

	public function set($id, $propiedad, $valor)
	{//02.01.13
		try {
			return $this->oBD->update('personal', array($propiedad => $valor), array('dni' => $id));
		} catch(Exception $e) {
			throw new Exception("ERROR\n".JrTexto::_("Update")." ".JrTexto::_("Personal").": " . $e->getMessage());
		}
	}
   
		
}